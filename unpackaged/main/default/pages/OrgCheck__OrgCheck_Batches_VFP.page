<apex:page >
    <apex:composition template="OrgCheck__OrgCheck_PageTemplate_VFT">
        <apex:define name="html_additional_scripttags" />
        <apex:define name="text_page_title">
            {!$Label.orgcheck__OrgCheck_Batches_Title_CL}
        </apex:define>
        <apex:define name="text_page_subtitle">
            {!$Label.orgcheck__OrgCheck_Batches_Subtitle_CL}
        </apex:define>
        <apex:define name="text_page_description">
            <apex:outputText escape="false" value="{!$Label.orgcheck__OrgCheck_Batches_Description_CL}" />
        </apex:define>
        <apex:define name="html_actions">
            <div class="slds-page-header__control">
                <a href="/01p" target="_blank" rel="external noopener noreferrer">
                    <button id="compile-all-button" class="slds-button slds-button_icon slds-button_icon-border-filled" title="{!$Label.OrgCheck_CompileAll_CL}all">
                        <svg class="slds-button__icon" aria-hidden="true">
                            <use href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#apex_plugin')}"></use>
                        </svg>
                        <span class="slds-assistive-text">{!$Label.OrgCheck_CompileAll_CL}</span>
                    </button>
                </a>
            </div>
        </apex:define>
        <apex:define name="html_content_core">
            <div id="datatable" />

            <div class="slds-tabs_default">
                <ul class="slds-tabs_default__nav" role="tablist">
                    <li class="slds-tabs_default__item slds-is-active" title="Schedulable classes not yet scheduled" role="presentation">
                        <a class="slds-tabs_default__link" href="javascript:void(0);" role="tab" tabindex="0" aria-selected="true" aria-controls="tab-default-notscheduledjobs" id="tab-default-notscheduledjobs__item">Schedulable classes not yet scheduled</a>
                    </li>
                    <li class="slds-tabs_default__item" title="Apex Jobs" role="presentation">
                        <a class="slds-tabs_default__link" href="javascript:void(0);" role="tab" tabindex="-1" aria-selected="false" aria-controls="tab-default-apexjobs" id="tab-default-apexjobs__item">Apex Jobs</a>
                    </li>
                    <li class="slds-tabs_default__item" title="Scheduled Jobs" role="presentation">
                        <a class="slds-tabs_default__link" href="javascript:void(0);" role="tab" tabindex="-2" aria-selected="false" aria-controls="tab-default-scheduledjobs" id="tab-default-scheduledjobs__item">Scheduled Jobs</a>
                    </li>
                </ul>
                <div id="tab-default-notscheduledjobs" class="slds-tabs_default__content slds-show" role="tabpanel" aria-labelledby="tab-default-notscheduledjobs__item">
                    <div id="datatable-notscheduledjobs" />
                </div>
                <div id="tab-default-apexjobs" class="slds-tabs_default__content slds-hide" role="tabpanel" aria-labelledby="tab-default-apexjobs__item">
                    <div id="datatable-apexjobs" />
                </div>
                <div id="tab-default-scheduledjobs" class="slds-tabs_default__content slds-hide" role="tabpanel" aria-labelledby="tab-default-scheduledjobs__item">
                    <div id="datatable-scheduledjobs" />
                </div>
            </div>
        </apex:define>
        <apex:define name="html_start_definition_script">
            <script>
                function start2(controller, helper) {

                    // Initialize TABS bindings
                    helper.html.tabs.initialize('slds-tabs_default__item', 'slds-tabs_default__content', 'slds-button');

                    // RUN CONTROLLER
                    controller.run({
                        datasets: [ 'batches', 'users', 'apexClasses' ],
                        onRecords: function(map) { 

                            // Render the data in a table for failed Apex Jobs
                            helper.html.datatable.create({
                                element: 'datatable-apexjobs',
                                columns: [
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Type_CL)}', property: 'type' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_ApexContext_CL)}', property: 'context' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Score_CL)}', type: 'numeric', property: '##score##' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Status_CL)}', property: 'status',
                                        scoreFormula: (r) => { 
                                            if (r.status === 'Failed') return 1; 
                                        }
                                    },
                                    { name: 'Message', property: 'message' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Count_CL)}', type: 'numeric', property: 'numIds' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Errors_CL)}', type: 'numeric', property: 'numErrors' }
                                ],
                                data: map.batches,
                                sorting: { name: '{!JSENCODE($Label.orgcheck__OrgCheck_ApexContext_CL)}', order: 'asc' },
                                filtering: { formula: (r) => { return r.nature === 'AsyncApexJob'; }},
                                showSearch: true,
                                showStatistics: true,
                                showLineCount: true
                            });

                            // Render the data in a table for Scheduled Jobs
                            helper.html.datatable.create({
                                element: 'datatable-scheduledjobs',
                                columns: [
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Name_CL)}', property: 'name' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Type_CL)}', property: 'type' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Score_CL)}', type: 'numeric', property: '##score##' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Status_CL)}', property: 'status',
                                        scoreFormula: (r) => { 
                                            if (r.status !== 'WAITING') return 1; 
                                        }
                                    },
                                    { name: 'user', formula: (r) => { 
                                        return helper.html.render.link(
                                            '/'+r.userid, 
                                            helper.html.render.escape(map.users[r.userid]?.name || r.userid)
                                        ); 
                                    }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Starts_CL)}', property: 'start' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Ends_CL)}', property: 'end' },
                                    { name: 'timezone', property: 'timezone' }
                                ],
                                data: map.batches,
                                sorting: { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Score_CL)}', order: 'asc' },
                                filtering: { formula: (r) => { return r.nature === 'ScheduledJob'; }},
                                showSearch: true,
                                showStatistics: true,
                                showLineCount: true
                            });

                            let cntClsToCompile = 0;

                            // Render the data in a table for Not Scheduled Jobs
                            helper.html.datatable.create({
                                element: 'datatable-notscheduledjobs',
                                columns: [
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Name_CL)}', formula: (r) => { 
                                        return helper.html.render.link(
                                            '/'+r.id, 
                                            helper.html.render.escape(r.name)
                                        ); 
                                    }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Score_CL)}', type: 'numeric', property: '##score##' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Status_CL)}', 
                                        formula: (r) => {
                                            if (r.needsRecompilation === true) {
                                                cntClsToCompile++;
                                                return 'Recompile!';
                                            }
                                        },
                                        scoreFormula: (r) => { 
                                            if (r.needsRecompilation === true) return 1; 
                                        }
                                    },
                                    { name: 'Is Scheduled?', 
                                        formula: (r) => { return helper.html.render.checkbox(r.isScheduled === true); },
                                        scoreFormula: (r) => { if (r.isScheduled !== true) return 1; }
                                    },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Package_CL)}', property: 'namespace' }
                                ],
                                data: map.apexClasses,
                                sorting: { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Score_CL)}', order: 'desc' },
                                filtering: { formula: (r) => { return r.needsRecompilation === true || (r.interfaces && r.interfaces.includes('System.Schedulable')); }},
                                showSearch: true,
                                showStatistics: true,
                                showLineCount: true
                            });

                            // Show alert about recompilation
                            if (cntClsToCompile > 0) {
                                let html = 'You have '+cntClsToCompile+' Apex class'+(cntClsToCompile>1?'es':'')+' that need'+(cntClsToCompile>1?'':'s')+' to be <b>recompiled</b>.<br />';
                                html += 'Please make sure you click on the <b>plug</b> button in this page to go to the setup menu and recompile all classes in this org.<br />';
                                html += 'If some classes still need recompilation after this, you need to check them for sure.<br />';
                                //helper.html.modal.show('Action needed on Apex classes in the Org!', html);
                                helper.html.message.show(html);
                                helper.html.element.addClass('compile-all-button', ['slds-theme_warning']);
                            }
                        },
                        actions: {  
                            clearCache: { 
                                show: true 
                            }
                        }
                    });
                }
            </script>
        </apex:define>
    </apex:composition>
</apex:page>