<apex:page >
    <apex:composition template="OrgCheck__OrgCheck_PageTemplate_VFT">
        <apex:define name="text_page_title">
            {!$Label.orgcheck__OrgCheck_ProfilePermSets_Title_CL}
        </apex:define>
        <apex:define name="text_page_subtitle">
            {!$Label.orgcheck__OrgCheck_ProfilePermSets_Subtitle_CL}
        </apex:define>
        <apex:define name="text_page_description">
            <apex:outputText escape="false" value="{!$Label.orgcheck__OrgCheck_ProfilePermSets_Description1_CL}" />
            <apex:outputText escape="false" value="{!$Label.orgcheck__OrgCheck_ProfilePermSets_Description2_CL}" />
            <apex:outputText escape="false" value="{!$Label.orgcheck__OrgCheck_ProfilePermSets_Description3_CL}" />
        </apex:define>
        <apex:define name="html_actions">
        </apex:define>
        <apex:define name="html_content_core">
            <div class="slds-tabs_default">
                <ul class="slds-tabs_default__nav" role="tablist">
                    <li class="slds-tabs_default__item slds-is-active" title="Profiles" role="presentation">
                        <a class="slds-tabs_default__link" href="javascript:void(0);" role="tab" tabindex="0" aria-selected="true" aria-controls="tab-default-profiles" id="tab-default-profiles__item">Profiles</a>
                    </li>
                    <li class="slds-tabs_default__item" title="Permission Sets" role="presentation">
                        <a class="slds-tabs_default__link" href="javascript:void(0);" role="tab" tabindex="-1" aria-selected="false" aria-controls="tab-default-permsets" id="tab-default-permsets__item">Permission Sets</a>
                    </li>
                    <li class="slds-tabs_default__item" title="Login Hours Restrictions" role="presentation">
                        <a class="slds-tabs_default__link" href="javascript:void(0);" role="tab" tabindex="-1" aria-selected="false" aria-controls="tab-default-loginhours" id="tab-default-loginhours__item">Login Hours Restrictions</a>
                    </li>
                    <li class="slds-tabs_default__item" title="IP Ranges Restrictions" role="presentation">
                        <a class="slds-tabs_default__link" href="javascript:void(0);" role="tab" tabindex="-1" aria-selected="false" aria-controls="tab-default-ipranges" id="tab-default-ipranges__item">IP Range Restrictions</a>
                    </li>
                    <li class="slds-tabs_default__item" title="Object CRUDs" role="presentation">
                        <a class="slds-tabs_default__link" href="javascript:void(0);" role="tab" tabindex="-1" aria-selected="false" aria-controls="tab-default-objcruds" id="tab-default-objcruds__item">Object CRUDs</a>
                    </li>
                </ul>
                <div id="tab-default-profiles" class="slds-tabs_default__content slds-show" role="tabpanel" aria-labelledby="tab-default-profiles__item">
                    <div id="datatable-profiles" />
                </div>
                <div id="tab-default-permsets" class="slds-tabs_default__content slds-hide" role="tabpanel" aria-labelledby="tab-default-permsets__item">
                    <div id="datatable-permissionSets" />
                </div>
                <div id="tab-default-loginhours" class="slds-tabs_default__content slds-hide" role="tabpanel" aria-labelledby="tab-default-loginhours__item">
                    <div id="datatable-profileLoginHours" />
                </div>
                <div id="tab-default-ipranges" class="slds-tabs_default__content slds-hide" role="tabpanel" aria-labelledby="tab-default-ipranges__item">
                    <div id="datatable-profileRangeIPs" />
                </div>
                <div id="tab-default-objcruds" class="slds-tabs_default__content slds-hide" role="tabpanel" aria-labelledby="tab-default-objcruds__item">
                    <div id="legend-objectCRUDs">
                        Legend: <u><b>C</b></u>reate, <u><b>R</b></u>ead, <u><b>U</b></u>pdate, <u><b>D</b></u>elete, <u><b>V</b></u>iew all and <u><b>M</b></u>odify all.<br />
                    </div>
                    <div id="datatable-objectCRUDs" />
                </div>
            </div>
        </apex:define>
        <apex:define name="html_start_definition_script">
            <script>
                function start2(controller, helper) {

                    const PREF_HIDE_CRUDS = helper.preferences.get('filter.HideCRUDsProfilesPermSets');

                    const profileDatasets = [ 'profiles', 'permissionSets', 'permissionSetAssignments' ];
                    if (PREF_HIDE_CRUDS === false) profileDatasets.push('objectCRUDs');

                    // Initialize TABS bindings
                    helper.html.tabs.initialize('slds-tabs_default__item', 'slds-tabs_default__content', 'slds-button');

                    // =================================
                    // RUN CONTROLLER
                    // =================================
                    controller.run({
                        datasets: profileDatasets,
                        onRecords: function(map) { 

                            // -------------------------------------------
                            // PROFILES
                            // -------------------------------------------
                            helper.html.datatable.create({
                                element: 'datatable-profiles',
                                columns: [
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Name_CL)}', formula: (r) => { 
                                        return helper.html.render.link(
                                            '/'+r.id, 
                                            helper.html.render.escape(r.name)
                                        ); 
                                    }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Score_CL)}', type: 'numeric', property: '##score##' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_IsCustom_CL)}', property: 'isCustom', formula: (r) => { 
                                        return helper.html.render.checkbox(r.isCustom); 
                                    }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_License_CL)}', property: 'license' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_UserType_CL)}', property: 'userType' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Package_CL)}', property: 'package' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_NbActiveUsers_CL)}', 
                                        formula: (r) => { 
                                            if (r.isUnusedCustom === true) return '{!JSENCODE($Label.orgcheck__OrgCheck_NoUserForThisProfile_CL)}'; 
                                            if (r.membersCount < 101) return helper.html.render.format('{!JSENCODE($Label.orgcheck__OrgCheck_XUsers_CL)}', r.membersCount);
                                            return '{!JSENCODE($Label.orgcheck__OrgCheck_More100Users_CL)}';
                                        },
                                        scoreFormula: (r) => { 
                                            if (r.isUnusedCustom === true) return 1;
                                        }
                                    },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Description_CL)}', 
                                        formula: (r) => {
                                            if (r.isUndescribedCustom === true) return '{!JSENCODE($Label.orgcheck__OrgCheck_SetADescription_CL)}';
                                            return helper.html.render.escape(r.description);
                                        },
                                        scoreFormula: (r) => { 
                                            if (r.isUndescribedCustom === true) return 1;
                                        }
                                    },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_CreatedDate_CL)}', type: 'datetime', property: 'createdDate' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_ModifiedDate_CL)}', type: 'datetime', property: 'lastModifiedDate' }                                    
                                ],
                                data: map.profiles,
                                sorting: { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Score_CL)}', order: 'desc' },
                                showSearch: true,
                                showStatistics: true,
                                showLineCount: true
                            });

                            // -------------------------------------------
                            // PROFILE LOGIN RESTRICTIONS
                            // -------------------------------------------
                            helper.html.datatable.create({
                                element: 'datatable-profileLoginHours',
                                columns: [
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Profile_CL)}', formula: (r) => { 
                                        return helper.html.render.link(
                                            '/'+r.id, 
                                            helper.html.render.escape(r.name)
                                        );
                                    }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Monday_CL)}', formula: (r) => { return r.loginHours.monday ? helper.html.render.format('{!JSENCODE($Label.orgcheck__OrgCheck_XtoY_CL)}', r.loginHours.monday.from, r.loginHours.monday.to) : '' }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Tuesday_CL)}', formula: (r) => { return r.loginHours.tuesday ? helper.html.render.format('{!JSENCODE($Label.orgcheck__OrgCheck_XtoY_CL)}', r.loginHours.tuesday.from,  r.loginHours.tuesday.to) : '' }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Wednesday_CL)}', formula: (r) => { return r.loginHours.wednesday ? helper.html.render.format('{!JSENCODE($Label.orgcheck__OrgCheck_XtoY_CL)}', r.loginHours.wednesday.from,  r.loginHours.wednesday.to) : '' }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Thursday_CL)}', formula: (r) => { return r.loginHours.thursday ? helper.html.render.format('{!JSENCODE($Label.orgcheck__OrgCheck_XtoY_CL)}', r.loginHours.thursday.from,  r.loginHours.thursday.to) : '' }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Friday_CL)}', formula: (r) => { return r.loginHours.friday ? helper.html.render.format('{!JSENCODE($Label.orgcheck__OrgCheck_XtoY_CL)}', r.loginHours.friday.from, r.loginHours.friday.to) : '' }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Saturday_CL)}', formula: (r) => { return r.loginHours.saturday ? helper.html.render.format('{!JSENCODE($Label.orgcheck__OrgCheck_XtoY_CL)}', r.loginHours.saturday.from,  r.loginHours.saturday.to) : '' }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Sunday_CL)}', formula: (r) => { return r.loginHours.sunday ? helper.html.render.format('{!JSENCODE($Label.orgcheck__OrgCheck_XtoY_CL)}', r.loginHours.sunday.from,  r.loginHours.sunday.to) : '' }},
                                ],
                                data: map.profiles,
                                sorting: { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Profile_CL)}', order: 'asc' },
                                filtering: { formula: (r) => { return r.loginHours ? true : false; }},
                                showSearch: false,
                                showStatistics: false,
                                showLineCount: true
                            });

                            // -------------------------------------------
                            // PROFILE RANGE IP
                            // -------------------------------------------
                            helper.html.datatable.create({
                                element: 'datatable-profileRangeIPs',
                                columns: [
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Profile_CL)}', formula: (r) => { 
                                        return helper.html.render.link(
                                            '/'+r.id, 
                                            helper.html.render.escape(r.name)
                                        ); 
                                    }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Starts_CL)}', formula: (r) => { 
                                        let html = '';
                                        r.loginIpRanges.forEach(i => html += i.startAddress + '<br />');
                                        return html;
                                    }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Ends_CL)}', formula: (r) => { 
                                        let html = '';
                                        r.loginIpRanges.forEach(i => html += i.endAddress + '<br />');
                                        return html;
                                    }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Description_CL)}', formula: (r) => { 
                                        let html = '';
                                        r.loginIpRanges.forEach(i => html += helper.html.render.escape(i.description) + '<br />');
                                        return html;
                                    }}
                                ],
                                data: map.profiles,
                                sorting: { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Profile_CL)}', order: 'asc' },
                                filtering: { formula: (r) => { return r.loginIpRanges && r.loginIpRanges.length > 0 ? true : false; }},
                                showSearch: false,
                                showStatistics: false,
                                showLineCount: true
                            });

                            // -------------------------------------------
                            // PERMISSION SETS
                            // -------------------------------------------
                            const assignmentsPerPS = {};
                            helper.map.iterate2(map.permissionSetAssignments, function(p, i, s) {
                                let assignments = assignmentsPerPS[p.permissionSetId];
                                if (!assignments) {
                                    assignments = assignmentsPerPS[p.permissionSetId] = { assignees: [], uniqueProfiles: {} };
                                }
                                assignments.assignees.push(p.assigneeId);
                                if (!assignments.uniqueProfiles[p.assigneeProfileId]) {
                                    assignments.uniqueProfiles[p.assigneeProfileId] = { count: 1 };
                                } else {
                                    assignments.uniqueProfiles[p.assigneeProfileId].count++;
                                }
                            });
                            helper.html.datatable.create({
                                element: 'datatable-permissionSets',
                                columns: [
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Name_CL)}', 
                                        formula: (r) => { 
                                            return helper.html.render.link(
                                                r.isGroup ? ('/'+r.groupId) : ('/'+r.id), 
                                                helper.html.render.escape(r.name)
                                            ); 
                                        }
                                    },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_IsGroup_CL)}', property: 'isGroup', formula: (r) => { 
                                        return helper.html.render.checkbox(r.isGroup); 
                                    }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Score_CL)}', type: 'numeric', property: '##score##' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_IsCustom_CL)}', property: 'isCustom', formula: (r) => { 
                                        return helper.html.render.checkbox(r.isCustom); 
                                    }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_License_CL)}', property: 'license' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Package_CL)}', property: 'package' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_NbActiveUsers_CL)}', 
                                        formula: (r) => { 
                                            if (r.isUnusedCustom === true) return 'No user for this custom permission set!'; 
                                            const membersCount = assignmentsPerPS[r.id]?.assignees?.length || 0;
                                            if (membersCount < 101) return helper.html.render.format('{!JSENCODE($Label.orgcheck__OrgCheck_XUsers_CL)}', membersCount);                                            
                                            return '{!JSENCODE($Label.orgcheck__OrgCheck_More100Users_CL)}';
                                        },
                                        scoreFormula: (r) => { 
                                            if (r.isUnusedCustom === true) return 1;
                                        }
                                    },

                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_AssignedUsersProfiles_CL)}', formula: (r) => { 
                                        let html = '';
                                        helper.map.iterate2(assignmentsPerPS[r.id]?.uniqueProfiles, function(v, i, l, pid) {
                                            html += helper.html.render.link(
                                                '/'+pid, 
                                                helper.html.render.escape(map.profiles[pid]?.name)) + ' (' + v.count + ')<br />';
                                        });
                                        return html;
                                    }},
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Description_CL)}', 
                                        formula: (r) => {
                                            if (r.isUndescribedCustom === true) return '{!JSENCODE($Label.orgcheck__OrgCheck_SetADescription_CL)}';
                                            return helper.html.render.escape(r.description);
                                        },
                                        scoreFormula: (r) => { 
                                            if (r.isUndescribedCustom === true) return 1;
                                        }
                                    },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_CreatedDate_CL)}', type: 'datetime', property: 'createdDate' },
                                    { name: '{!JSENCODE($Label.orgcheck__OrgCheck_ModifiedDate_CL)}', type: 'datetime', property: 'lastModifiedDate' }                                    
                                ],
                                data: map.permissionSets,
                                sorting: { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Score_CL)}', order: 'desc' },
                                showSearch: true,
                                showStatistics: true,
                                showLineCount: true
                            });

                            // -------------------------------------------
                            // PROFILE OBJECT CRUDs
                            // -------------------------------------------
                            if (PREF_HIDE_CRUDS === true) {
                                const divCRUDs = helper.html.element.get('legend-objectCRUDs');
                                divCRUDs.innerHTML = '{!JSENCODE($Label.orgcheck__OrgCheck_CRUDMessage_CL)}';
                            } else {
                                helper.html.datatable.create({
                                    element: 'datatable-objectCRUDs',
                                    columns: [
                                        { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Parent_CL)}', formula: (r) => { 
                                            return helper.html.render.link(
                                                '/'+r.parent.id, 
                                                helper.html.render.escape(r.parent.name)
                                            ); 
                                        }},
                                        { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Object_CL)}', property: 'sobject' },
                                        { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Type_CL)}', property: 'type' },
                                        { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Permissions_CL)}', formula: (r) => { 
                                            let html = '';
                                            if (r.permissions.create === true) html += 'C';
                                            if (r.permissions.read === true) html += 'R';
                                            if (r.permissions.update === true) html += 'U';
                                            if (r.permissions.delete === true) html += 'D';
                                            if (r.permissions.viewAll === true) html += ' V';
                                            if (r.permissions.modifyAll === true) html += ' M';
                                            return html;
                                        }}
                                    ],
                                    data: map.objectCRUDs,
                                    sorting: { name: '{!JSENCODE($Label.orgcheck__OrgCheck_Object_CL)}', order: 'asc' },
                                    showSearch: true,
                                    showStatistics: true,
                                    showLineCount: true
                                });
                            }

                        },
                        actions: {  
                            clearCache: { 
                                show: true 
                            },
                            exportTable: [{
                                table: 'datatable-profiles',
                                visibleTab: 'tab-default-profiles__item',
                                filename: 'Profiles'
                            }, {
                                table: 'datatable-permissionSets',
                                visibleTab: 'tab-default-permsets__item',
                                filename: 'PermissionSets'
                            }, {
                                table: 'datatable-profileLoginHours',
                                visibleTab: 'tab-default-loginhours__item',
                                filename: 'LoginHoursRestrictions'
                            }, {
                                table: 'datatable-profileRangeIPs',
                                visibleTab: 'tab-default-ipranges__item',
                                filename: 'IPRangesRestrictions'
                            }]
                        }
                    });
                }
            </script>
        </apex:define>
    </apex:composition>
</apex:page>