<table border="0" cellpadding="5" width="550" cellspacing="5" height="400" >
<tr valign="top" height="400" >
<td tEditID="c1r1" style=" background-color:#FFFFFF; bEditID:r3st1; color:#000000; bLabel:main; font-size:12pt; font-family:arial;" aEditID="c1r1" locked="0" >
<![CDATA[<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><font face="Arial, sans-serif" color="#666666">Dear {!Case.Name_Title__c} {!Case.Contact_Last_Name__c},</font></p><p style="color: rgb(0, 0, 0); font-family: arial;"><span style="background-color: white;"><font face="Arial, sans-serif" style="" color="#666666"><br></font></span></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">Thank you for taking the time to write
to us regarding your recent experience.<br><br>Virgin Australia adheres to the Public
Health advice from both the Commonwealth and relevant State/Territory health
authorities, as well as aviation-specific peak bodies such as the International
Air Transport Association (IATA) and the International Civil Aviation
Organisation (ICAO), when putting in place changes to usual operating practices
due to the COVID-19 pandemic.<br><br>We have a multi-layered approach to
minimising risk of infectious transmission for both the travelling public and
our crew, which encompasses all parts of the passenger journey and is part of
the Federal Health endorsed “COVID-safe flying protocols”.<br><br>This includes but is not limited to;</font></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">&nbsp;</font></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">• Health declarations at the time of
check-in;<o:p></o:p></font></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">• Medical clearance requirements for
those who have symptoms and/or have traveled&nbsp;from identified areas of high
community transmission;<o:p></o:p></font></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">• Rigorous cleaning practices including
daily deep-cleans and reapplication of antimicrobial products to high touch
areas on all aircraft turnarounds;<o:p></o:p></font></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">• Social distancing strategies as far
as is practicable including seat separation where passenger loads allow;<o:p></o:p></font></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">• Masks and hand sanitiser available to
all guests on all flights;<o:p></o:p></font></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">• Health screening on departure from
and/or arrival in to airports carried out by the relevant State/Territory
Public Health authorities or by Australian Border Force biosecurity teams where
required;<o:p></o:p></font></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">• Briefing and training for all staff
in COVID-safe working practices;<o:p></o:p></font></p><p class="MsoPlainText" style=""><font face="Arial, sans-serif" color="#666666" style=""><font face="arial">• Adherence to robust COVID-safe plans
following the national workplace COVID-safe principles.</font><br><br><font face="arial">For flights departing from areas of
known high community transmission where additional public health measures have
been implemented, mask wearing is required and supplies are given to guests
before and at the time of boarding.<br></font></font><span style="color: rgb(102, 102, 102); font-family: Arial, sans-serif; font-size: 12pt;"><br>For flights departing from areas of low
community transmission, where public health advice does not mandate the use of
masks, guests are able to access these supplies on request (as detailed in our
PA announcements on board). This is true for both guests and our crew. Virgin
Australia crew on the ground and in the air use personal protective equipment
as recommended by the Group Medical and Workplace Health and Safety teams.</span></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">&nbsp;</font></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666"><span style="background: white;">In regards to social distancing on board, it's important to
take into account there are no government or medical directives which state
social distancing on commercial aircraft is required.&nbsp; To observe the
standard social distancing requirements on an aircraft would mean we could only
carry approximately 20 guests on a 170 seat aircraft which is simply not a
feasible option.&nbsp; When availability allows, we will certainly look to
leave the middle seat empty on all flights for guest peace of mind. The air
filters on the air conditioning units are the same as those used in hospital
operating theatres and filter out 99.9% of microbes in the air.</span><o:p></o:p></font></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">&nbsp;</font></p><p class="MsoPlainText" style="color: rgb(0, 0, 0); font-family: arial;"><font face="Arial, sans-serif" color="#666666">The current medical evidence suggests
that the risk of in flight&nbsp;transmission is low due to a number of protective
factors onboard the aircraft. However, we encourage all Guests to follow public
health guidance and industry recommendations for a COVID-safe journey.<br></font><span style="color: rgb(102, 102, 102); font-family: Arial, sans-serif; background-color: white; font-size: 12pt;"><br>Please be advised the relevant health department will directly contact any
passengers who they deem to be in close contact with confirmed Coronavirus
cases.</span></p><p style="color: rgb(0, 0, 0); font-family: arial; margin: 0cm 0cm 0.0001pt;"><span style="background-color: white; color: rgb(102, 102, 102); font-family: Arial, sans-serif; font-size: 12pt;"><br>If you require
further information about Coronavirus, please call Coronavirus Health
Information Line on 1800 020 080.<br></span><span style="background-color: white; color: rgb(102, 102, 102); font-family: Arial, sans-serif; font-size: 12pt;"><br>If you are
experiencing flu like symptoms and are concerned about your health, please call
the Healthdirect hotline on 1800 022 222.</span></p><p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="color: rgb(102, 102, 102); font-family: Arial, sans-serif;"><br></span></p><p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="color: rgb(102, 102, 102); font-family: Arial, sans-serif;">Kind regards,</span></p>
<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;">
<table style="WIDTH: 100%; mso-cellspacing: 0cm; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 0cm 0cm 0cm" class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes">
<td style="BORDER-BOTTOM: #ece9d8; BORDER-LEFT: #ece9d8; PADDING-BOTTOM: 0cm; PADDING-LEFT: 7.5pt; WIDTH: 142.5pt; PADDING-RIGHT: 14.25pt; BACKGROUND: white; BORDER-TOP: #ece9d8; BORDER-RIGHT: #ece9d8; PADDING-TOP: 0cm" width="190"><font color="#1f497d" size="2" face="Arial, sans-serif"><br><img border="0" alt="" align="left" src="https://virginaustralia--c.ap1.content.force.com/servlet/servlet.ImageServer?id=01590000001W9Z2&amp;oid=00D90000000KkT5"><br></font><br><br></td></tr>
<tr style="mso-yfti-irow: 1">
<td style="BORDER-BOTTOM: #ece9d8; BORDER-LEFT: #ece9d8; PADDING-BOTTOM: 0cm; PADDING-LEFT: 0cm; WIDTH: 100%; PADDING-RIGHT: 0cm; BACKGROUND: white; BORDER-TOP: #ece9d8; BORDER-RIGHT: #ece9d8; PADDING-TOP: 0cm" width="100%">
<p style=""><font face="Arial,Sans-Serif" style="color: rgb(130, 138, 143); font-family: Arial, sans-serif; font-size: 10pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#ff0000"><b>{!Case.OwnerFullName}</b>&nbsp;</font>|&nbsp;<o:p></o:p></font><font color="#828a8f" face="Arial, sans-serif"><span style="font-size: 13.3333px;">{!User.Title}</span></font></p>
<p style="color: rgb(130, 138, 143); font-family: Arial, sans-serif; font-size: 10pt; line-height: 11.25pt; margin: 0cm 0cm 0pt 26.25pt;" class="MsoNormal"><span style="FONT-FAMILY: 'Arial','sans-serif'; COLOR: #828a8f; FONT-SIZE: 10pt; mso-fareast-language: EN-AU"><font face="Arial,Sans-Serif">PO Box 1034 Spring Hill QLD Australia 4004</font></span></p>
<p style="color: rgb(130, 138, 143); font-family: Arial, sans-serif; font-size: 10pt; line-height: 11.25pt; margin: 0cm 0cm 0pt 26.25pt;" class="MsoNormal"><font face="Arial,Sans-Serif"><b><span style="FONT-FAMILY: 'Arial','sans-serif'; COLOR: #828a8f; FONT-SIZE: 10pt; mso-fareast-language: EN-AU">E</span></b><span style="FONT-FAMILY: 'Arial','sans-serif'; COLOR: #828a8f; FONT-SIZE: 10pt; mso-fareast-language: EN-AU"> <a href="mailto:VirginAustraliaCustomerCare@virginaustralia.com">VirginAustraliaCustomerCare@virginaustralia.com</a></span></font></p></td></tr>
<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes">
<td style="BORDER-BOTTOM: #ece9d8; BORDER-LEFT: #ece9d8; PADDING-BOTTOM: 0cm; BACKGROUND-COLOR: transparent; PADDING-LEFT: 0cm; PADDING-RIGHT: 0cm; BORDER-TOP: #ece9d8; BORDER-RIGHT: #ece9d8; PADDING-TOP: 0cm">
<p style="LINE-HEIGHT: 11.25pt; MARGIN: 0cm 0cm 0pt 26.25pt" class="MsoNormal"><span style="FONT-FAMILY: 'Arial','sans-serif'; COLOR: #999999; FONT-SIZE: 7.5pt; mso-fareast-language: EN-AU"><font face="Arial,Sans-Serif"><font size="2">Please consider the environment before printing this email.<o:p></o:p></font></font></span></p><p style="LINE-HEIGHT: 11.25pt; MARGIN: 0cm 0cm 0pt 26.25pt" class="MsoNormal"><span style="FONT-FAMILY: 'Arial','sans-serif'; COLOR: #999999; FONT-SIZE: 7.5pt; mso-fareast-language: EN-AU"><font face="Arial,Sans-Serif"><font size="2"><br></font></font></span></p><p style="LINE-HEIGHT: 11.25pt; MARGIN: 0cm 0cm 0pt 26.25pt" class="MsoNormal"><i style="font-size: 12pt;"><span style="font-size: 7.5pt; font-family: Arial, sans-serif; color: rgb(102, 102, 102);"><b>If you need to reply to us, please ensure to include the reference code at the bottom of this email in your response.</b></span></i></p></td></tr></tbody></table></p>
<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><br></p><p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="color: rgb(255, 255, 255); background-color: rgb(255, 255, 255); font-size: 12pt;">{!Case.Thread_Id}</span></p>]]></td>
</tr>
</table>