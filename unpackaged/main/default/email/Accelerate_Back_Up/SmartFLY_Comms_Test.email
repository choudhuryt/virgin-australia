<html style="overflow-y: hidden;">
<head><meta content="text/html; charset=utf-8"><meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1">
	<title>Virgin Australia</title>
	<style type="text/css">body{
		width: 100%;
		margin:0;
		padding:0;
		border:0;
		font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif;
		background-color: #D9D9D9;
		line-height: 120%;
		}
	table{
		border-collapse: collapse;
  		table-layout: auto;
  		width: 600px;
  	  	margin-left: auto;
  		margin-right: auto;
  		border:none;
	}
	td{
		padding:22.5px;
		padding-bottom: 25.5px;
	}
	h1{
		color:#512698;
		font-size:12pt;
		font-weight:600;
		padding:0 0 10 0;
	}
	h2{
		color:#FFFFFF;
		font-size:10pt;
		font-weight:bold;
		line-height:150%
	}
	img{
		width:600px;
		height:auto;
		display:inline-flex;
		background-color:#D9D9D9;
		padding:0;	
	}
	#footer{
		background-color:#D6083B;
		font-size:6pt;
		color: #FFFFFF;
		line-height: 100%;
	}
	#header_content{
		border-collapse:collapse;
	}
	#header_date{
		color: #BFBFBF;
		background-color:#FFFFFF;		
		font-size: 12pt;
		font-weight: 500;
		line-height: 90%;
		text-align: right;
		border-top: solid #7F7F7F 6pt;
	}
	#header_image{
		padding:0;
		display:inline-flex;
	}
	#header_title{
		background-color:#512698;
		font-size: 16pt;
		font-weight: 600;
		color:#FFFFFF;
	}
	#module_with_heading, #module_without_heading, #module_with_image, #module_bulletpoint,#module_with_side_image{
		background-color:#ffffff;
		font-size:10pt;
		color: #36424A;
	}
	a{
		color: #FFFFFF;
    	text-decoration: none;
    	font-weight: bold;
		text-decoration: none;
	}
	#footer{
		line-height: 130%;
		border-bottom: solid #7F7F7F 6pt;
	}
	.module_image{
		background-color:#D9D9D9;
		padding:0;
		display:inline-flex;
	}
	.module_side_image{
		background-color:#D9D9D9;
		width:200px;
		height:auto;
		padding:0;
		display:inline-flex;
	}
	@media only screen and (max-device-width:720px) {
	@import url(https://fonts.googleapis.com/css?family='Montserrat':500,600,700);
	</style>
</head>
<body style="height: auto; min-height: auto;">
<table border="0" cellpadding="10" width="100%">
	<tbody>
		<tr>
			<td>
			<table id="header_content">
				<tbody>
					<tr>
						<td id="header_date">&nbsp;</td>
					</tr>
					<tr>
						<td id="header_image"><img alt="Accelerate banner" src="https://virginaustralia--c.ap12.content.force.com/file-asset-public/Accelerate_banner?oid=00D90000000KkT5" style="max-width: 681.15px" title="Accelerate banner" /></td>
					</tr>
					<tr>
						<td id="header_title">Enjoy 7,800 Velocity Frequent Flyer Points^</td>
					</tr>
				</tbody>
			</table>
			&nbsp;

			<table id="module_with_heading">
				<tbody>
					<tr>
						<td>
						<h1>&nbsp;</h1>
						<span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a">Hello {!Account.Accelerate_Key_Contact_First_Name}<b>,</b></span></span></span></span></span>

						<h1><span style="font-size:16px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="color:#512698"><span style="font-weight:bold">Making more of our Virgin Australia <i>accelerate</i> program</span></span></span></span></span></h1>

						<p><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a">As a valued Virgin Australia customer, you&#39;ll already be familiar with the wide range of benefits offered in our <i>accelerate</i> program.</span></span></span></span></span></p>

						<p><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a">While we&#39;ve previously engaged with you through Flight Centre Travel group&#39;s Corporate Traveller business, as they&#39;ve informed you as of 9 November 2021 we&#39;ll no longer be the&nbsp;exclusive partner of their SmartFLY program.</span></span></span></span></span></p>

						<p><span style="font-size:11pt"><span style="line-height:120%"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="line-height:120%"><span style="font-family:Montserrat"><span style="color:#36424a"><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;">From now on, we&#39;re pleased to communicate directly on your business travel account.</span></span>&nbsp;</span></span></span></span><br />
						<br />
						<span style="font-size:16px;"><span style="font-family:Arial,Helvetica,sans-serif;"><b><span style="line-height:120%"><span style="color:#512698">What this means for you and your business</span></span></b></span></span></span></span></span></p>

						<p><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a">When your business signed up to SmartFLY, you also joined the Virgin Australia <i>accelerate</i> program. This means you&#39;ve automatically transitioned to <i>accelerate</i> and your current agent managing your company travel has all the information needed to continue tracking your business&#39;s spend and bookings on your&nbsp;behalf.</span></span></span></span></span></p>

						<h1><span style="font-size:16px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="color:#512698"><span style="font-weight:bold">We&#39;d like to say thanks</span></span></span></span></span></h1>

						<p><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a">As the nominated key contact for {!Account.Name} we appreciate your support as we make this transition. To say thank you, we&#39;d like to offer you 7,800 bonus Velocity Frequent Flyer Points.</span></span></span><br />
						<span style="color:#36424a">These Points are redeemable for one complimentary flight on one travel sector when travelling for your business. To claim these Points, you must&nbsp;book an eligible flight in the <i>accelerate</i> portal or via your TMC by 31 December 2021 and travel by 28 February 2022**. Please refer to the terms and conditions provided at the end of this email.&nbsp;</span></span></span></p>
						</td>
					</tr>
				</tbody>
			</table>
			&nbsp;&nbsp;

			<table id="module_with_image">
				<tbody>
					<tr>
						<td class="module_image"><b><span style="font-family:Arial,Helvetica,sans-serif;"><span style="font-size:12px;"><img alt="Welcome onboard (1)" src="https://virginaustralia--c.ap12.content.force.com/file-asset-public/Welcome_onboard_1?oid=00D90000000KkT5" style="max-width: 681.15px" title="Welcome onboard (1)" /></span></span></b></td>
					</tr>
					<tr>
						<td>
						<h1><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><b><span style="color: rgb(54, 66, 74);">Our </span><i style="color: rgb(54, 66, 74); font-family: Montserrat; font-size: 10pt;">accelerate </i><span style="color: rgb(54, 66, 74);">program gives your business exclusive access to a range of exclusive services and offers. This Includes:</span></b></span></span></h1>

						<p><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a"><b>Competitive fares: </b>In addition to our great value, you&#39;ll receive up to 6% off Flexi and Business Class fares.</span></span></span></span></span></p>

						<p><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><b><span style="line-height:120%"><span style="color:#36424a">Contemporary comfort: </span></span></b><span style="line-height:120%"><span style="color:#36424a">From the ground to the sky, we&#39;ll offer you and your team premium comfort, great value and great service all the way. Plus, you&#39;ll enjoy discounted lounge memberships for you and your employees*.</span></span></span></span></span></p>

						<p><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><b><span style="line-height:120%"><span style="color:#36424a">Here for you:</span></span></b><span style="line-height:120%"><span style="color:#36424a"> Our dedicated account management team are on hand to support you and help keep your business travel spend on track.</span></span></span><br />
						<br />
						<span style="line-height:120%"><b><span style="line-height:120%"><span style="color:#36424a">Award-winning frequent flyer program</span></span></b><span style="line-height:120%"><span style="color:#36424a">: Traveller on your account who are Velocity Frequent Flyer members will earn Points and Status Credits on eligible flights. Our Velocity Frequent Flyer program is completely free to join and you can store a traveller&#39;s Velocity membership number on their traveller profile.&nbsp;</span></span></span></span></span><br />
						<br />
						<span style="font-size:11pt"><span style="line-height:120%"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="line-height:120%"><span style="font-family:Montserrat"><span style="color:#36424a"><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;">You&#39;ll also also continue to receive exclusive promotions and month progress reports from Virgin Australia regarding your <i>accelerate</i> account.&nbsp;</span></span><br />
						&nbsp;</span></span></span></span></span></span></span></p>
						</td>
					</tr>
				</tbody>
			</table>
			&nbsp;

			<table id="module_bulletpoint">
				<tbody>
					<tr>
						<td>
						<h1><span style="font-size:16px;"><span style="font-family:Arial,Helvetica,sans-serif;">Next steps &amp; additional support&nbsp;</span></span></h1>

						<p><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a">To make sure we have the correct information for your business, we&#39;d appreciate it if you could review your account name and ABN/ACN below and advise us of any changes as soon as possible.<br />
						<br />
						<b>Account name</b>: {!Account.Name}</span></span></span><br />
						<span style="line-height:120%"><b><span style="line-height:120%"><span style="color:#36424a">ABN/ACN:</span></span></b><span style="line-height:120%"><span style="color:#36424a"> {!Account.Business_number_C}</span></span></span></span></span></p>

						<p><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a">We&#39;d also recommend you check we have the most up to date information on file for you nominated key account contact as well as your traveller profiles, including their Velocity Frequent Flyer number. When your business starts flying again you&#39;ll be ready for take-off.&nbsp;</span></span></span><br />
						<br />
						<span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a">If you&#39;d like more information on this transition, we&#39;ve prepared some <u>FAQ&#39;s</u> which can be found</span></span> <span style="line-height:120%"><span style="color:#36424a"><a href="https://aus01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fwww.virginaustralia.com%2Fcontent%2Fdam%2Fvaa%2Fdocs%2Faccelerate%2FVirgin_Australia_Accelerate_FAQ%27s_SmartFLY.pdf&data=04%7C01%7Cashleigh.wedd%40virginaustralia.com%7C7ae2120cd67642a7ee7708d9a57d8b53%7C0f289d43bbef44469cec57d0419a15c2%7C0%7C0%7C637722780937455706%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&sdata=485n4d3Sx3SCn2fWukuSIhPhJLlBYExA3mlMLq2K%2BoA%3D&reserved=0" style="color:white; font-weight:bold; text-decoration:none"><b><span style="color:black">here</span></b></a>.</span></span></span><br />
						<span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a">Should&nbsp;you need further assistance with your <i>accelerate</i> account, please call 1300 246 497 or email us </span><span style="color:#000000;">at </span><a href="mailto:accelerate@virginaustralia.com" style="color:white; font-weight:bold; text-decoration:none"><span style="color:#000000;"><b>accelerate@virginaustralia.com</b></span></a><span style="color:#000000;">.</span></span></span></span></span></p>

						<p><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a">We look forward to welcoming you onboard again soon.&nbsp;</span></span></span></span></span></p>

						<p><br />
						<span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:120%"><span style="line-height:120%"><span style="color:#36424a">Kind Regards,&nbsp;</span></span></span><br />
						<span style="color:#36424a">The Virgin Australia <i>accelerate</i> team.</span></span></span></p>
						</td>
					</tr>
				</tbody>
			</table>
			&nbsp;

			<table id="footer">
				<tbody>
					<tr>
						<td>
						<h2><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;"><span style="line-height:150%"><span style="color:white">Virgin Australia <i>accelerate</i> |&nbsp;<a href="https://aus01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fwww.virginaustralia.com%2Fau%2Fen%2Ftravel-info%2Fflying-with-us%2Fcorporate-travel%2Faccelerate-program%2F&data=04%7C01%7Cashleigh.wedd%40virginaustralia.com%7C7ae2120cd67642a7ee7708d9a57d8b53%7C0f289d43bbef44469cec57d0419a15c2%7C0%7C0%7C637722780937465661%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&sdata=qJftaEKmFQnSyBmqQb7%2FQyuDaKjC%2FQCND8PPJfAmI9o%3D&reserved=0" style="color:white; font-weight:bold; text-decoration:none">Terms &amp; conditions</a> |&nbsp; <a href="https://aus01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fwww.virginaustralia.com%2Fau%2Fen%2Fhelp%2Fcontact-us%2F&data=04%7C01%7Cashleigh.wedd%40virginaustralia.com%7C7ae2120cd67642a7ee7708d9a57d8b53%7C0f289d43bbef44469cec57d0419a15c2%7C0%7C0%7C637722780937465661%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&sdata=jbiWVfRC7nR5Zo9IexCtNCx1KpAuPZE1MXe%2BEd1zUZE%3D&reserved=0" style="color:white; font-weight:bold; text-decoration:none" target="_blank">Contact us</a>&nbsp;&nbsp;|&nbsp; <a href="https://aus01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fwww.virginaustralia.com%2Fau%2Fen%2Fabout-us%2Fpolicies%2Fprivacy%2F&data=04%7C01%7Cashleigh.wedd%40virginaustralia.com%7C7ae2120cd67642a7ee7708d9a57d8b53%7C0f289d43bbef44469cec57d0419a15c2%7C0%7C0%7C637722780937475620%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&sdata=YHk4bqTHQZW2Zo2%2F2Y25tFnrcXYX6DJ6fzNTw2jCcrA%3D&reserved=0" style="color:white; font-weight:bold; text-decoration:none" target="_blank">Privacy</a>&nbsp;| <a href="https://aus01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fvirginaustralia.secure.force.com%2Fsales%2FUnsubscribeContact%3Ft%3D3%26cid%3D0039000001hK1Fq&data=04%7C01%7Cashleigh.wedd%40virginaustralia.com%7C7ae2120cd67642a7ee7708d9a57d8b53%7C0f289d43bbef44469cec57d0419a15c2%7C0%7C0%7C637722780937475620%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&sdata=q1wdsR7s2NfQcXUryIm32lHyJFeaZdu12q4EP7Y%2F9kc%3D&reserved=0" style="color:white; font-weight:bold; text-decoration:none">Unsubscribe</a></span></span></span></span></h2>

						<h2><span style="font-size:12px;"><span style="font-family:Arial,Helvetica,sans-serif;">Velocity Frequent Flyer | <a href="https://aus01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fexperience.velocityfrequentflyer.com%2Fmember-support%2Fterms-conditions&data=04%7C01%7Cashleigh.wedd%40virginaustralia.com%7C7ae2120cd67642a7ee7708d9a57d8b53%7C0f289d43bbef44469cec57d0419a15c2%7C0%7C0%7C637722780937485581%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&sdata=x2aKKSGX9hI%2F3FaFjWJ4UBDdQgXYFJKuVpj9HQfFZOI%3D&reserved=0" style="color:white; font-weight:bold; text-decoration:none">Terms &amp; conditions</a> | <a href="https://aus01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fexperience.velocityfrequentflyer.com%2Fmember-support%2Fprivacy&data=04%7C01%7Cashleigh.wedd%40virginaustralia.com%7C7ae2120cd67642a7ee7708d9a57d8b53%7C0f289d43bbef44469cec57d0419a15c2%7C0%7C0%7C637722780937485581%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&sdata=F9IwStZVWc6fyjSqu1KbTdFyPbn%2B%2FsuAmEli0dgUXNA%3D&reserved=0" style="color:white; font-weight:bold; text-decoration:none">Privacy</a></span></span></h2>
						</td>
					</tr>
				</tbody>
			</table>

			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:7.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">**Terms and conditions -&nbsp;Virgin Australia 7,800 Bonus Velocity Points offer&nbsp;</span></span></b><br />
			<span style="font-size:7.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">To be eligible for the 7,800 bonus Velocity Points promotion you must:&nbsp;&nbsp;<br />
			(1) be the Key Contact of an Accelerate business account with your Velocity membership number registered in your Accelerate account between 12.01am AEST 9 November 2021 and 11.59pm 31 December inclusive (Promotion Period);&nbsp;&nbsp;<br />
			(2) book a ticket for an eligible flight in the Accelerate Portal or via your travel agent during the Promotion Period and enter a valid Velocity membership number into the booking, at the time of booking; and&nbsp;&nbsp;<br />
			(3) complete travel on an Eligible Flight. An Eligible Flight is a flight marketed and operated by Virgin Australia, between 12.01am AEST 9 November 2021 and 11.59pm 28 February 2022 inclusive and booked and ticketed in a fare class that normally accrues Points. Bonus points cannot be earned on Economy LITE fares or fares booked via virginaustralia.com.au. Codeshare services marketed or operated by partner airlines are not eligible for this offer.&nbsp;&nbsp;<br />
			4) Bonus Velocity Points will be earned in addition to base Velocity Points earned. Bonus Points will be applied to the originally purchased fare class and any upgrades (other than when you paid the full commercial fare) will not attract bonus Points as a result of this offer. This offer can only be redeemed once per Accelerate Account. This offer is only available to the Key Contact of an Accelerate business account. Velocity Points offer cannot be used in conjunction with any other offer or promotion. You should allow up to 8 weeks after completion of travel for the bonus Velocity Points to be allocated.&nbsp;^According to current government border restrictions.&nbsp;*Velocity terms and conditions apply.&nbsp;</span></span></span></span><br />
			<span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:7.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">Accelerate program Terms and conditions</span></span></b></span></span><br />
			<span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><sup><span style="font-size:7.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">1</span></span></sup><span style="font-size:7.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;Fare Advantage Terms and Conditions: Fare Advantage discounts are available to all registered Virgin Australia accelerate clients in selected booking classes (see&nbsp; &ldquo;Savings&rdquo; tab on this page for further information regarding eligibility and specific Fare Advantage Discounts). All flights must be booked via the Virgin Australia Corporate Portal, the Virgin Australia Guest Contact Centre or a registered self-ticketing Travel Management Company and are not available via the Virgin Australia public website. For information on how to register your travel management company, please contact the Virgin Australia accelerate team a<span style="color:black">t&nbsp;</span><a href="mailto:accelerate@virginaustralia.com" style="color:white; font-weight:bold; text-decoration:none"><b><span style="color:black">accelerate@virginaustralia.com</span></b></a><span style="color:black">. </span>Fare advantage discounts apply to Eligible Services sold with the &lsquo;VA&rsquo; designator and which are operated by Virgin Australia.&nbsp; See accelerate terms and conditions for more information. Fare Advantage discounts do not apply to any bookings made via the Virgin Australia conference and group travel area and cannot be combined with any other Unpublished Fares and Promotional Fares. Fare Advantage discounts are available on all domestic flights, Trans Tasman and international short haul flights using Virgin Australia only. Fare Advantage discounts will not be applied to sale fares or other tactical fares Virgin Australia offers from time to time. If you have any questions regarding sale fares, tactical fares or the application of Fare Advantage discounts, please contact the Virgin Australia accelerate team.</span></span></span></span><br />
			<span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><sup><span style="font-size:7.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">2</span></span></sup><span style="font-size:7.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;For further detail regarding the Virgin Australia lounge benefits, please see Schedule 2, section (C) &lsquo;Virgin Australia Lounge Benefits&rsquo; in the accelerate terms and conditions. International short haul discounts exclude flights between Australia and New Zealand. For further information please refer to the accelerate terms and conditions.&nbsp;&nbsp;</span></span></span></span><br />
			<span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><sup><span style="font-size:7.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">3</span></span></sup><span style="font-size:7.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;Status Credits cannot be earned on all fare classes. Status Credits can only be earned on eligible flights, marketed and operated by Virgin Australia and/or a Velocity airline partner. Find out which fare classes earn Status Credits . Status Credits cannot be earned in other fare classes, unless advised otherwise. Status Credits accrual is based on single flight segments that consist of individual flight numbers. An itinerary that involves a change in aircraft but retains the same flight number is considered a single flight segment. An itinerary that consists of a change in flight numbers, even if the same aircraft is used for the entire itinerary, is considered two flight segments. Members travelling on upgraded fares purchased with Points or any other subsidised means of upgrades on Virgin Australia or our partner airlines will earn Status Credits based on original purchased fare class</span></span></span></span><br />
			&nbsp;</p>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>