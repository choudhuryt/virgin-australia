<html><head>
	<title></title>
</head>
<body style="height: auto; min-height: auto;"><br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:black">Dear (name of organisation),</span></span></span></span></span></span><br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></span><br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:black">Thank you for your email and sorry for the delay in replying to you.</span></span></span></span></span></span><br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></span><br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:black">Please note that Virgin Australia does not have an “approved list” of trainers or training organisations.&nbsp;</span></span></span></span></span></span><br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></span><br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:black">There are organisations which are&nbsp;accredited under section 9 of the Disability Discrimination Act (Cth) (<b>Act</b>), being those:</span></span></span></span></span></span>
<ul>
	<li><span style="font-size:11pt"><span style="color:black"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">9(2)(a) - accredited under a law of a State or Territory that provides for the accreditation of animals trained to assist a person with a disability to alleviate the effect of the disability; or</span></span></span></span></span></span></span></li>
	<li><span style="font-size:11pt"><span style="color:black"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">9(2)(b) - accredited by an animal training organisation prescribed by the regulations for the purposes of section 9(2).</span></span></span></span></span></span></span></li>
</ul>
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></span><br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:black">In assessing dogs for carriage in the aircraft cabin, Virgin Australia is guided by the accredited training organisation and trainer lists published by Assistance Dogs International and the relevant State or Territory government departments where the department has assessed and determined those organisations and trainers as accredited under the Act. These lists are publicly available. A trainer or training organisation can apply to Assistance Dogs International or relevant State or Territory government department to become accredited for the purposes of the Act under section 9(2)(a) or (b).</span></span></span></span></span></span><br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></span><br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:black">Where a trainer or training organisation is not accredited under&nbsp;9(2)(a) or (b) of the Act, Virgin Australia will assess the dog’s suitability to be carried in the aircraft cabin under section 9(2)(c) of the Act to assess whether the dog is:</span></span></span></span></span></span>

<ul>
	<li><span style="font-size:11pt"><span style="color:black"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">Trained:</span></span></span></span></span></span></span>

	<ul style="list-style-type:circle">
		<li><span style="font-size:11pt"><span style="color:black"><span style="line-height:normal"><span style="tab-stops:list 72.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">(i)&nbsp; to assist a person with a disability to alleviate the effect of the disability; and</span></span></span></span></span></span></span></li>
		<li><span style="font-size:11pt"><span style="color:black"><span style="line-height:normal"><span style="tab-stops:list 72.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">(ii)&nbsp; to meet standards of hygiene and behaviour that are appropriate for an animal in a public place.</span></span></span></span></span></span></span></li>
	</ul>
	</li>
</ul>
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></span><br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:black">To make this assessment, we ask the handler for the following to evidence that the assistance dog meets the requirements of section 9(2)(c):</span></span></span></span></span></span>

<ul>
	<li><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">A letter from a Medical Professional stating that you have a disability within the meaning of the DDA, and the Guide, Hearing and Assistance Dogs Act 2009, and the dog is specifically trained to assist in alleviating the effects of this disability.</span></span></span></span></span></span></li>
	<li><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">A certificate or written statement from the training organisation or trainer which includes evidence (for example training records) demonstrating that the dog:</span></span></span></span></span></span>
	<ul style="list-style-type:circle">
		<li><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list 72.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">has been trained to assist you with your disability to alleviate the effect of the disability; and</span></span></span></span></span></span></li>
		<li><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list 72.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">has been trained to meet the standards of hygiene and behaviour that are appropriate for an animal in a public place (for example a copy of the Public Access Test (PAT) certificate could be provided as evidence of this).</span></span></span></span></span></span></li>
	</ul>
	</li>
	<li><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">A Handler identification card for the dog issued by a State Authority or Statutory Body and identification for the dog including a recent photo of the dog.</span></span></span></span></span></span></li>
	<li><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">A coat/harness/badge that clearly identifies the dog and its assistance dog status.</span></span></span></span></span></span></li>
	<li><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">Documentation showing that the dog is registered with the local Council or a government affiliated animal organisation.</span></span></span></span></span></span><br />
	&nbsp;</li>
</ul>
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">The following further information is not necessary but will be useful to assess a dog’s suitability to travel in the aircraft cabin:</span></span></span></span></span>

<ul>
	<li><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">rail/transport pass;</span></span></span></span></span></span></li>
	<li><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">obedience certificates; or</span></span></span></span></span></span></li>
	<li><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">a letter from your dog’s regular Veterinarian verifying the dog's details and temperament.</span></span></span></span></span></span></li>
</ul>
<br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">I hope the above information has provided clarification on this matter for you.</span></span></span></span></span><br />
<br />
<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif">Kind regards,</span></span></span></span></span><br />
<br />
<br />
&nbsp;</body></html>