<html><head>
	<title></title>
</head>
<body style="height: auto; min-height: auto;">Dear <span style="color:#e74c3c;">{Name}</span>,<br />
<br />
Thank you for your continued patience.&nbsp; We've reviewed your request for a refund; however, because your booking was made on or before 20 April 2020, we're unable to provide you with a refund under our usual terms and conditions.&nbsp; However, we are pleased to let you know that we can provide you with a Future Flight credit for the value of your booking (less non-refundable fees and charges).<br />
<br />
You can use this credit to book an Economy or Business Class seat on Virgin Australia operated flights in the fare class available for Future Flight credits, as well as other Virgin Australia flight extras like Economy X or pre-paid baggage.<br />
<br />
Your new Future Flight credit is for the same value as your unused credit and can be used for bookings anytime up until 31 July 2022, for travel valid until 30 June 2023.&nbsp; We're providing seats for Future Flight credit use on all Virgin Australia operated flights (<b>Future Credit fares</b>) but please book early as these are subject to availability.<br />
<br />
<b>Your Travel Bank account</b><br />
<br />
A new Travel Bank has been created for your Future Flight credit.&nbsp; You can view your credit balance online by visiting <a href="https://travelbank.virginaustralia.com/tbank/user/login.html">Travel Bank</a>&nbsp;and you can find the Travel Bank Terms and Conditions <a href="https://www.virginaustralia.com/au/en/terms-conditions/travel-bank/">here</a>.&nbsp; Please note if you have a Velocity accounts, you will not be able to access your Future Flight credit through this account.<br />
<br />
Below you will find your Travel Bank account number.&nbsp; Please visit the Travel Bank website to create your password.&nbsp; You will need to enter your Travel Bank username and follow the instructions to access your Travel Bank online.<br />
<br />
<u><b>Travel Bank Account number</b></u><b>:</b>&nbsp;<span style="color:#e74c3c;">{acc number}</span><br />
<b><u>Travel Bank Account username</u>:</b> <span style="color:#e74c3c;">{acc username}</span><br />
<br />
If you have a travel credit that was issued for a booking made from 21 April 2020 onwards, this will continue to be held in a separate Travel bank.<br />
<br />
<b>Using your Future Flight credit</b><br />
<br />
To use your credit, you can:
<ul>
	<li>Virgin <a href="https://www.virginaustralia.com/au/">www.virginaustralia.com</a>, search for your flight and select the option to pay using Travel Bank before you continue to available flights.&nbsp; The website will display the fares available for booking using your Future Flight credit.</li>
	<li>Visit Manage My Booking to purchase Virgin Australia flight extras after a booking is made.</li>
	<li>Contact the Guest Contact Centre on 13 67 89 (if calling from within Australia) or +61 7 3295 2296 (if calling from outside of Australia).</li>
</ul>
Use of your Future Flight credit is subject to the Future Flight Credit Terms of Use.&nbsp; You can find these Terms of Use and FAQs about your Future Flight credit <a href="https://travel.virginaustralia.com/au/coronavirus-update/travel-bank">here</a>.<br />
<br />
Thank you for your patience and we look forward to seeing you onboard again soon.<br />
<br />
Kind regards,</body></html>