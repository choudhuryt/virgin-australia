/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
* @updatedBy         : Cloudwerx
*/
@isTest
private class TestScheduleAccelerateAccountBatch {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        testAccountObj.Accelerate_Batch_Update__c = false;
        testAccountObj.Account_Lifecycle__c = 'Opportunity';
        testAccountObj.Market_Segment__c = 'Accelerate';
        testAccountObj.Potential_Duplicate__c = false;
        testAccountObj.OwnerId = System.Label.Virgin_Australia_Business_Flyer_User_Id;
        testAccountObj.Business_Number__c = '233';
        INSERT testAccountObj;
        
    }
    
    @isTest
    private static void testScheduleAccelerateAccountBatch() {
        Test.startTest();
        ScheduleAccelerateAccountBatch scheduleAccelerateAccountBatchObj = new ScheduleAccelerateAccountBatch();
        String cronTime = '0 0 23 * * ?'; 
        system.schedule('scheduleAccelerateAccountBatch' + System.now(), cronTime, scheduleAccelerateAccountBatchObj);
        Test.stopTest();
        // Asserts
        Account updatedAccountObj = [SELECT Id, Sales_Matrix_Owner__c, RecordTypeId,
                                     Billing_Country_Code__c, Market_Segment__c FROM Account LIMIT 1];
        // Asserts
        System.assertEquals('Accelerate', updatedAccountObj.Sales_Matrix_Owner__c);
        System.assertEquals(Utilities.getRecordTypeId('Account', 'Accelerate'), updatedAccountObj.RecordTypeId);
        System.assertEquals('AU', updatedAccountObj.Billing_Country_Code__c);
        System.assertEquals('Accelerate', updatedAccountObj.Market_Segment__c);
    }
}