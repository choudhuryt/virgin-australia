/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCheckATierController {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account account = new Account();
        CommonObjectsForTest commonOBJ = new CommonObjectsForTest();
        account = commonOBJ.CreateAccountObject(0);
        insert account;
        
        Velocity_Status_Match__c vsm = new Velocity_Status_Match__c();
        vsm.Account__c = account.id;
        vsm.Approval_Status__c = 'Draft';
        vsm.Within_Contract__c = 'no';
        vsm.Status_Match_or_Upgrade__c = 'Gold';
        vsm.Passenger_Name__c = 'Test';
        vsm.Passenger_Email__c ='andy@test.com';
        vsm.Position_in_Company__c = 'tester';
        vsm.Passenger_Velocity_Number__c = '9530011641';
        vsm.Delivery_To__c = 'Member';
        vsm.Justification__c = 'test';
        vsm.Date_Requested__c = Date.today();
        //Added for failing test classes start 28/11/2019
        vsm.PA_Email__c = 'test@test.com.au';
     	vsm.PA_name__c = 'Sarita Gaur';
     	vsm.PA_Number__c = '1013143410';
     
       
        //Added for failing test classes stop 28/11/2019
		insert vsm;
		
		PageReference  ref1 = Page.CheckATier;
        ref1.getParameters().put('id', vsm.Id);
        Test.setCurrentPage(ref1);
        ApexPages.StandardController conL = new ApexPages.StandardController(vsm);
        CheckATierController lController = new CheckATierController(conL);
        
        //instantiate the DomesticDiscountController 
        ApexPages.StandardController conX = new ApexPages.StandardController(vsm);
        CheckATierController xController = new CheckATierController(conX);
        
        Test.startTest();
        //PageReference  ref1 = Page.DomesticDiscountPage;
     
        
       
        ref1 = lController.initCheckATier();
        ref1 = lController.cancel();
		Test.stopTest();
		        
    }
}