/*
 * Updated by Cloudwerx: Refactored the below code.
 * 
 */
public class SmartflyContactUpdate {
    
    @InvocableMethod
    public static void UpdateContactID(List<Id> ContactIds) {
        List<contact> contactsToUpdate = new List<Contact>(); 
        Set<Id> accountIds = new Set<Id>();
        List<Contact> contacts = [SELECT Id, AccountId, Account_ID__c FROM Contact WHERE Id IN:ContactIds AND AccountId = NULL];
        for (Contact contactObj :contacts) {
            accountIds.add(contactObj.Account_ID__c);
        }
        
        Map<Id, PartnerNetworkRecordConnection> partnerRecordIdsMap = new Map<Id, PartnerNetworkRecordConnection>();
        for (PartnerNetworkRecordConnection partnerNetworkRecordConnectionObj : [SELECT Id, PartnerRecordId, LocalRecordId FROM PartnerNetworkRecordConnection WHERE PartnerRecordId IN :accountIds]) {
            partnerRecordIdsMap.put(partnerNetworkRecordConnectionObj.PartnerRecordId, partnerNetworkRecordConnectionObj);
        }
        
        for(Contact contactObj :contacts){
            contactObj.AccountId  = (partnerRecordIdsMap != null && partnerRecordIdsMap.containsKey(contactObj.Account_ID__c)) ? partnerRecordIdsMap.get(contactObj.Account_ID__c)?.LocalRecordId : null;           
            contactsToUpdate.add(contactObj);
        }
        
        UPDATE contactsToUpdate; 
    } 
}