public class GuestCaseBaggageInfo{
    public VelocityDetail getVelocityDetails(String PIRNumber,String RecordType){
        if(String.isEmpty(PIRNumber) || String.isEmpty(RecordType)){
            return null;
        }
        SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService HisDetailsService = new SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService();
        SalesForceHistoricalDetailModel.GetLostBaggageRSType response_x = HisDetailsService.GetLostBaggage(PIRNumber, RecordType);
        if(response_x == null){
            return null;
        }
        VelocityDetail velocityDetail = new VelocityDetail();
        velocityDetail.PIRNumber = response_x.PIRNumber;
        velocityDetail.FirstName = response_x.GuestName;
        
        return velocityDetail;
    }
}