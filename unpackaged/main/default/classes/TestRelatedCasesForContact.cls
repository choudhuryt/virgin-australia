@isTest
private class TestRelatedCasesForContact 
{
 static testMethod void myUnitTest1() 
 {
     
      Test.startTest();
     
      Case c1 = new Case(SuppliedEmail='jdoe_test_test@doe.com',
                            SuppliedName='John Doe',
                            Velocity_Member__c = false,
                            Subject='Feedback - Something1',
                           Contact_First_Name__c='Tom',
                            Contact_Last_name__c = 'Johns');
     
     insert c1;
     
      Case c2 = new Case(SuppliedEmail='jdoe_test_test@doe.com',
                            SuppliedName='John Doe',
                            Velocity_Member__c = false,
                            Subject='Feedback - Something2',
                           Contact_First_Name__c='Tom',
                            Contact_Last_name__c = 'Johns');
     
      insert c2 ;
     
      Case c3 = new Case(SuppliedEmail='jdoe_test_test@doe.com',
                            SuppliedName='John Doe',
                            Velocity_Member__c = false,
                            Subject='Feedback - Something3',
                           Contact_First_Name__c='Tom',
                            Contact_Last_name__c = 'Johns');
     
      insert c3;
     
     
     PageReference pref = Page.RelatedCasesForContact;
     pref.getParameters().put('id', c1.id);
     Test.setCurrentPage(pref);
     
      ApexPages.StandardController conL = new ApexPages.StandardController(c1);
      RelatedCasesForContact lController = new RelatedCasesForContact(conL);
     
     
   
     
      PageReference  ref1 = Page.RelatedCasesForContact;
      test.stopTest();
        
 }

 static testMethod void myUnitTest2() 
 {
    List<RecordType> recordTypeGRContact = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Contact'
										and IsActive = true
										and DeveloperName = 'GR_Contact' LIMIT 1];
     
      Test.startTest();
     
     Contact cnt1 = new Contact(FirstName = 'John',
	                                LastName = 'Doe',
	                                Email='jdoe_test_test@doe.com',
	                                recordTypeId=recordTypeGRContact[0].id);
		
     insert cnt1;
     
      Case c1 = new Case(  SuppliedName='John Doe',
                            Velocity_Member__c = false,
                            Subject='Feedback - Something1',
                           Contact_First_Name__c='Tom',
                            Contact_Last_name__c = 'Johns',
                            Contactid = cnt1.id );
     
     insert c1;
     
      Case c2 = new Case( Contactid = cnt1.id,
                            SuppliedName='John Doe',
                            Velocity_Member__c = false,
                            Subject='Feedback - Something2',
                           Contact_First_Name__c='Tom',
                            Contact_Last_name__c = 'Johns');
     
      insert c2 ;
     
      Case c3 = new Case( Contactid = cnt1.id ,
                            SuppliedName='John Doe',
                            Velocity_Member__c = false,
                            Subject='Feedback - Something3',
                           Contact_First_Name__c='Tom',
                            Contact_Last_name__c = 'Johns');
     
      insert c3;
     
     
     PageReference pref = Page.RelatedCasesForContact;
     pref.getParameters().put('id', c1.id);
     Test.setCurrentPage(pref);
     
      ApexPages.StandardController conL = new ApexPages.StandardController(c1);
      RelatedCasesForContact lController = new RelatedCasesForContact(conL);
     
     
   
     
      PageReference  ref1 = Page.RelatedCasesForContact;
      test.stopTest();
        
 }
    
    
}