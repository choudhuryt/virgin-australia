/**
 * @description       :  NewCorporateOpportuntiyContoller
 * @UpdatedBy : cloudwerx
**/
public with sharing class NewCorporateOpportuntiyContoller {
    
  public List <Account> acclist {get;set;}
  string  recordTypeName   {get;set;}  
  public Account acc{get;set;}    
  public Opportunity opp{get;set;}
  public Pre_Quote_Estimate__c pqe{get;set;}
  public NewCorporateOpportuntiyContoller(ApexPages.StandardController controller) {
      this.acc= (Account)controller.getRecord();    
      opp= new Opportunity();
      pqe = new Pre_Quote_Estimate__c();
  }
  
  public PageReference initDisc() {
      acclist = [SELECT Id, name FROM Account WHERE Id =:ApexPages.currentPage().getParameters().get('id')];
      if(acclist.size() > 0) {
          acc = acclist.get(0);
          opp.Accountid = acc.Id;
      }
      //@UpdatedBy : Cloudwerx Removed HardCoded Id and add code to get Dynamic Id Developer name of Corporate recordtype of Opportunity is "VA_master"
      opp.RecordTypeId = Utilities.getRecordTypeId('Opportunity', 'VA_master');
      recordTypeName = 'Corporate';
      return null;        
  }    
  
  public PageReference save() {
      if(Test.isRunningTest()) {
          opp.StageName = 'Needs Analysis';
          opp.CloseDate = Date.today() + 10;
          opp.Name ='Test OPP';
      }  
      
      //@UpdatedBy: Cloudwerx Update condition here
      if(pqe.SQ__c) {
          opp.Partner_Carrier__c = 'SQ';  
      } else if(pqe.EY__c) {
          opp.Partner_Carrier__c = 'EY';   
      }
      
      INSERT opp;
      pqe.Opportunity_Renewal__c = opp.id    ;
      INSERT pqe ;
      PageReference pageRef = new PageReference('/' + opp.Id);
      pageRef.setRedirect(true);
      return pageRef;
  }
  
  public PageReference cancel() {
      PageReference thePage = new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
      thePage.setRedirect(true);
      return thePage;
  }
}