public with sharing class CreateMeetingNotes
{
    
public Meeting_Notes__c newmeetnote{get;set;}
    
public Boolean addnote {get;set;}     
public List <Event> evenlist {get;set;}    
public Event even{get;set;}    

    
public CreateMeetingNotes(ApexPages.StandardController controller)
{
    this.even= (Event)controller.getRecord(); 
    newmeetnote = new Meeting_Notes__c();
    
    addnote = false ;
}
    
public PageReference initDisc() 
{
  evenlist = [SELECT  id, Meeting_Notes__c , Account__c FROM Event WHERE Id =:ApexPages.currentPage().getParameters().get('id')]; 
 
    if(evenlist.size() >  0 && evenlist[0].Meeting_Notes__c == null) 
    {
     addnote = true ; 
     return null ;   
    }else
    {
    ApexPages.addMessage( new ApexPages.message( ApexPages.severity.ERROR,'There are meeting notes available. Please edit the same.' ));
          return null;   
    }    
 
}
    
public PageReference Save() 
{
     try     
  { 
    
     
     insert newmeetnote;
     even.Meeting_Notes__c = newmeetnote.Id ;
     update even ;    
     
     if(newmeetnote.Market_Intelligence_Type__c <> null && newmeetnote.Market_Intelligence_Details__c <> null  && evenlist[0].Account__c <> null  )
     {
        Data_Smart_Research__c  marInt = new Data_Smart_Research__c();
        marInt.Account__c = evenlist[0].Account__c ;
        marInt.Subject__c = newmeetnote.Market_Intelligence_Type__c ;
        marInt.Sub_Category__c =  newmeetnote.Market_Intelligence_Sub_Type__c ;
        marInt.Detail__c = newmeetnote.Market_Intelligence_Details__c  ;
         insert  marInt ;
     }  
    PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
    thePage.setRedirect(true);
    return thePage;
  }
  catch(Exception e) {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getMessage()));
            return NULL;
  }
}  

public PageReference Cancel() 
{
  return null ; 
}     
}