public class UpdateRelatedAccount 
{
  @InvocableMethod
    
   public static void UpdateRelatedAccountdetails(List<Id> EventIds )   
    {
        callupdateAccountInfo(EventIds );
    }    
    
    @future
    public static void callupdateAccountInfo(List<Id> EventIds )   
    {
        List<Event> lsteventUpdate = new List<Event>();
        id Relatedaccountid ;
        List<Event> eventlist =  [SELECT AccountId,Account__c,Id,WhatCount,WhatId,WhoCount,WhoId,Who__c FROM Event where id =   :EventIds ];
        
        if (eventlist.size() > 0  && eventlist[0].WhoID <> NULL )
        {
            List<Contact> conlist =  [Select Id, AccountId ,Email,RecordtypeId  from Contact where id =   :eventlist[0].WhoID];
            
            system.debug('The contact details' + conlist) ;
            
            if(conlist.size() > 0 &&  conlist[0].recordtypeid == '012900000007krgAAA')
            {
               Relatedaccountid = conlist[0].AccountId ; 
            }else
            {
              List<Contact> salesconlist =  [Select Id, AccountId ,Email,RecordtypeId  from Contact where Email = :conlist[0].Email
                                             and  recordtypeid = '012900000007krgAAA' order by createddate desc limit 1];  
                
               system.debug('The sales contact details' + salesconlist  + 'The accountid'+ salesconlist[0].AccountID ); 
                
                
              if(salesconlist.size() > 0 && salesconlist[0].AccountID <> NULL )  
              {
                Relatedaccountid =  salesconlist[0].AccountID ;
              }
            }    
                
        }
        
        IF(RelatedAccountid <> null )
            
        {
            system.debug('the id' + RelatedAccountid);
            
             for(Event ev : eventlist)
             {
                 ev.Account__c = relatedAccountid ;
                 ev.WhatId = relatedAccountid ;
                 lsteventUpdate.add(ev);
             }
            
        }   update   lsteventUpdate ;
            
    }
}