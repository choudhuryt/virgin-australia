public class AccelerateCaseAssignment {
    
@InvocableMethod
public static void AccelerateCaseLink( List<Id> CaseIds )
  {
      string BusinessNumber ;
      string AccountIDName ;
      List<Case> caseUpdate = new List<Case>(); 
      List<Account> accList = new List<Account>();  
      List<Case> smfcaselist = new List<Case>();   
      List<Case> accappcaselist = new List<Case>();
      List<Case> ebouncecaselist = new List<Case>(); 
      
      smfcaselist    =  [select id ,Subject,accountid,ACC_Primary_Category__c,
                        ACC_Secondary_Category__c,ACC_Tertiary_Category__c 
                        from case 
                        where id=:caseIds and subject like 'Pilot Gold Request | ABN: %' 
                        ];
      
      accappcaselist =  [select id ,Subject,Description,accountid,ACC_Primary_Category__c,
                         ACC_Secondary_Category__c, ACC_Tertiary_Category__c 
                         from case where id=:caseIds and subject like 'ACCELERATE Application%' 
                         and SuppliedEmail = 'no-reply@inteflow.com.au'
                        ];
      
      ebouncecaselist =  [select id ,Subject,Description,accountid,ACC_Primary_Category__c,
                         ACC_Secondary_Category__c, ACC_Tertiary_Category__c 
                         from case where id=:caseIds and subject like 'ACCOUNT MANAGER ALERT%' 
                        ];
      
     if (smfcaselist.size() > 0)
     {
       BusinessNumber =    smfcaselist[0].Subject.substringAfter('ABN:').substring(1);  
         
       if(BusinessNumber.length() == 11 ||  BusinessNumber.length() == 9 )  
       {    
         accList  =  [select id , Business_Number__c from Account 
                      where Business_Number__c =:BusinessNumber 
                      and recordtypeid = '012900000009HrP'  limit 1
                     ];    
       
       }
    
      for(Case c: smfcaselist)
       { 
       if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
       }
         c.ACC_Primary_Category__c = 'Client' ;
         c.ACC_Secondary_Category__c=  'Pilot Gold' ;
         c.ACC_Tertiary_Category__c = 'Action Sales Support';
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     }
      
      if (accappcaselist.size() > 0)
     {
         
       AccountIDName =    accappcaselist[0].Description.substringAfter('Application ID:').substring(1,21).replaceAll( '\\s+', '');
       system.debug('This is the description and id' + AccountIDName ) ; 
       accList  =  [select id , Business_Number__c,X18_Char_ID__c,Booking_Type__c
                    from Account 
                    where  X18_Char_ID__c = : AccountIDName  limit 1
                   ]; 
         
       for(Case c: accappcaselist)
       { 
       c.ACC_Primary_Category__c = 'Accounts/NAR' ;  
           
       if (c.Subject.contains('Approved')  )
       {
         c.ACC_Secondary_Category__c=  'Approvals' ;   
       }else if (c.Subject.contains('Decline') || c.Subject.contains('Withdraw'))  
       {
         c.ACC_Secondary_Category__c=  'Application Denied' ;      
       }
           
       if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
            if(acclist[0].Booking_Type__c == 'DirectB2B')
            {
             c.ACC_Tertiary_Category__c =  'Direct Approval' ;    
            }else
            {
             c.ACC_Tertiary_Category__c =  'TMC Approval' ;       
            }
       }
       
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     } 
    
      if (ebouncecaselist.size() > 0)
     {
      AccountIDName =    ebouncecaselist[0].Description.substringAfter('Account ID : ').substring(0,15).replaceAll( '\\s+', '');
      
      accList  =  [select id 
                    from Account 
                    where  id = : AccountIDName  limit 1
                   ]; 
    
      for(Case c: ebouncecaselist)
       { 
       if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
       }
         c.ACC_Primary_Category__c = 'Client' ;
         c.ACC_Secondary_Category__c = 'Contact Details Update' ; 
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     }
      
  }
}