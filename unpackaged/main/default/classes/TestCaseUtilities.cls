/**
* @description       : Test class for CaseUtilities
* @createdBy         : Cloudwerx
* @Updated By        : Cloudwerx
**/
@isTest
private with sharing class TestCaseUtilities {

    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        INSERT testCaseObj;
        
        Attachment testAttachmentObj = TestDataFactory.createTestAttachment(testCaseObj.Id, 'Test Attachment for Parent', 'Test Attachment for Parent');
        INSERT testAttachmentObj;
    }
    
    @isTest
    private static void testCaseUtilitiesInsertSolution() {
        Case testCaseObj = [SELECT Id, Subject, Description FROM Case LIMIT 1];
        Test.startTest();
        String convertedtStr = CaseUtilities.convertToSolution(testCaseObj.Id);
        Test.stopTest();
        // Asserts
        Solution insertedSolutionObj = [SELECT Id, SolutionName, SolutionNote FROM Solution WHERE CaseID__c =:testCaseObj.Id LIMIT 1];
        // Asserts
        System.assertEquals(testCaseObj.Subject, insertedSolutionObj.SolutionName);
        System.assertEquals(testCaseObj.Description, insertedSolutionObj.SolutionNote);
        System.assertEquals('Case has been converted to Solution successfully.', convertedtStr);
    }
    
    @isTest
    private static void testCaseUtilitiesWithExistingSolution() {
        Case testCaseObj = [SELECT Id, Subject, Description FROM Case LIMIT 1];
        Solution testSolutionObj = TestDataFactory.createTestSolution(testCaseObj.Id, testCaseObj.Subject, testCaseObj.Description);
        INSERT testSolutionObj;
        Test.startTest();
        String convertedtStr = CaseUtilities.convertToSolution(testCaseObj.Id);
        Test.stopTest();
        // Asserts
        System.assertEquals('Case is already converted to Solution successfully.', convertedtStr);
    }
}