public with sharing class LandingPageController {

	private String[] months = new String[]{'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'};
	
	public String getCurrentDate(){
		
		Date d = System.today();
		Datetime dt = (DateTime)d;
		String dayOfWeek = dt.format('EEEE');

		return dayOfWeek + ', ' + ' ' + d.day() + ' ' + months[d.month()-1] + ' ' + d.year();
	}

	public ChatterSettings__c getChatterSetting(){
		
		List<ChatterSettings__c> cs = ChatterSettings__c.getall().values();
		if(cs.size()>0)
			return cs[0];
		else
			return null;
		
	}
		


	
}