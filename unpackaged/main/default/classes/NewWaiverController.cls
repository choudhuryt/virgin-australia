public class NewWaiverController 
{

    
public Waiver_Favour__c waiver{get;set;}
public List <Case> caselist {get;set;}    
public Case c{get;set;}    
   

public NewWaiverController(ApexPages.StandardController controller)
{
    this.c= (Case)controller.getRecord();    
    waiver = new Waiver_Favour__c();
}

public PageReference initDisc() 
{
  caselist = [SELECT id   FROM Case WHERE Id =:ApexPages.currentPage().getParameters().get('id')];
  c = caselist.get(0);
  waiver.Case__c = c.Id;  
  waiver.RecordTypeId = '01290000000uaQq' ;
  waiver.Waiver_Category__c= 'Cancellations & Refunds';
  waiver.Waiver_Type__c	 = 'Refund'	 ;
    
  return null;        
}    

    
public PageReference save() {
    
    waiver.Waiver_Group_del__c = 	waiver.FFC_Waiver_Group__c;   
    insert waiver;
    
    PageReference pageRef = new PageReference('/'+waiver.Id);
    pageRef.setRedirect(true);
    return pageRef;
}
    
public PageReference cancel() {
 
   PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
   thePage.setRedirect(true);
   return thePage;
   
  }
}