/**
 * @description       : Test class for createDVBatch
 * @UpdatedBy         : Cloudwerx
**/
@isTest
private class createDVBatchTest {
    
    @testSetup static void setup() {
        // Create common test accounts
        List<Account> testAccts = new List<Account>();
        for (Integer i = 0; i < 20; i++) {
            Account testAccountObj = TestDataFactory.createTestAccount('TEST'+i, 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
            testAccountObj.bulk_updated__c = true; 
            testAccountObj.Update_DV__c = false;
            testAccts.add(testAccountObj);
        }
        INSERT testAccts;  
    }
    
    @isTest
    public static void testCreateDVBatch() {
        Test.startTest();
        Id jobid= Database.executeBatch(new createDVBatch() , 20);
        new createDVBatch().execute(null, null);
        Test.stopTest();
        Integer accountSize = [SELECT COUNT() FROM ACCOUNT WHERE Update_DV__c = TRUE];
        // Asserts
        system.assertEquals(20, accountSize);
    }
}