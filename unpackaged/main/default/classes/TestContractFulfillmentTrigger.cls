/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * 
 * HISTORY
 * 
 * 20.May.2016  Warjie Malibago (Accenture CloudFirst); see 200516WBM
*/
@isTest
private class TestContractFulfillmentTrigger {
    //200516WBM Start
   
  static testMethod void tourcodeupdatetest(){
        User u = TestUtilityClass.createTestUser();     
        insert u;
        
        system.runAs(u){
            Test.startTest();
            Account acc = TestUtilityClass.createTestAccount();
            insert acc;
            
            Contract ct = TestUtilityClass.createTestContract(acc.Id);
            insert ct;
            
            Contract_Fulfilment__c cf = TestUtilityClass.createTestContractFulfilment(acc.Id, ct.Id);
            insert cf;
            
            cf.Tour_Code__c = '12321321';
            update cf;
            
            //system.assertEquals('Corp Tourcode', [SELECT Tourcode_Purpose__c FROM Tourcodes__c WHERE Account__c =: acc.Id].Tourcode_Purpose__c);
            
            Test.stopTest();
            
           
        }
    }
  
 
}