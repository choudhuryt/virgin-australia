/**
 * @description       : AccountInteflowCallout
**/
global class AccountInteflowCallout {
    
   @future (callout=true) 
   public static void callrequesttoInteflow(String  accid) { 
       string CreditType;
       string AccountId;
       sTRING Name;
       string ABN;
       string Website;
       string BillingStreet ;
       string BillingCity ;
       string Billingstate ;
       string BillingPC ;
       string IndustryCode;
       String FirstName;
       String LastName; 
       String Email;
       String Title;
       String Phone;
       String BusinessActivity;   
       
       List<Account> accList =   [SELECT Id, Business_Number__c, name, InteflowIndustryCode__c,
                                  website, BillingState, BillingStreet, BillingCity, Business_Activity__c,
                                  BillingPostalCode, Account_Manager_Email_Address__c 
                                  FROM Account WHERE Id =:accid  LIMIT 1];
       
       List<Contact> KeyConlist = [SELECT Id, Key_Contact__c, Email, title, Phone, FirstName, LastName
                                   FROM Contact WHERE AccountId =:accid AND Key_Contact__c = true LIMIT 1]; 
       
       if  (acclist.size() > 0 && KeyConlist.size() > 0) {
           if (acclist[0].Account_Manager_Email_Address__c !=null ) {
               CreditType = 'TMCGDS' ;
           } else {
               CreditType = 'DIRECTB2B' ;                    
           } 
           if (acclist[0].Business_Activity__c !=null )  {
               BusinessActivity = acclist[0].Business_Activity__c.replaceall('&' ,'&amp;') ;   
           }else {
               BusinessActivity = '  ';   
           }    
           AccountId = acclist[0].Id;
           ABN  = acclist[0].Business_Number__c;
           Name = acclist[0].Name.replaceall('&' ,'&amp;') ; 
           title =  KeyConlist[0].title.replaceall('&' ,'&amp;') ;   
           BillingStreet = acclist[0].BillingStreet.replaceall('&' ,'&amp;') ; 
           BillingCity = acclist[0].BillingCity;
           BillingState = acclist[0].BillingState;
           BillingPC = acclist[0].BillingPostalCode;
           Website =  acclist[0].Website.replaceall('&' ,'&amp;') ;  
           IndustryCode  =  acclist[0].InteflowIndustryCode__c ;       
           FirstName = KeyConlist[0].FirstName;
           LastName = KeyConlist[0].LastName;     
           email = KeyConlist[0].Email;
           phone = KeyConlist[0].Phone;   
           
           //@UpdatedBy: Cloudwerx Removed Test IsRunning check
           Inteflowapexcallout.invokeExternalWs(AccountId, String.valueOf(AccountId),abn,name,
                                                Website,BillingStreet,BillingCity,
                                                BillingState,BillingPC,
                                                IndustryCode,
                                                FirstName,LastName,
                                                title,Email,
                                                Phone,credittype,BusinessActivity) ;
       }
   }
}