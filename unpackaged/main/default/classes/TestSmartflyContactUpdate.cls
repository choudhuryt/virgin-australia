/**
* @description       : Test class for SmartflyContactUpdate
* @Updated By        : Cloudwerx
**/
@isTest
private  with sharing class TestSmartflyContactUpdate {
    
    @testSetup static void setup() {
        Contact testContactObj = TestDataFactory.createTestContact(null, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        INSERT testContactObj;
    }
    
    @isTest
    private static void testSmartflyContactUpdate() {
        List<Id> contactIds = new List<Id>(new Map<Id,Contact>([SELECT Id FROM Contact]).keySet());   
        Test.startTest();
        SmartflyContactUpdate.UpdateContactID(contactIds);
        Test.stopTest();
		// We can't add asserts here because they are using "PartnerNetworkRecordConnection" setup object to add asserts either we have to setup the configuration or we have to create record in salesforce but we can't create test records
    }
}