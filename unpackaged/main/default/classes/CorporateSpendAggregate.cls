/*
 *  Updated by Cloudwerx : Removed hardcoded references for recordtypeIds
 * 
 * 
 */
global class  CorporateSpendAggregate implements Database.Batchable<SObject>
{
global CorporateSpendAggregate(){}
    
    public Static Id accountCorporateRecordTypeId = Utilities.getRecordTypeId('Account', 'Corporate');
    public Static Id accountGovernmentRecordTypeId = Utilities.getRecordTypeId('Account', 'Government');
    
    public Static Set<Id> accountRecTypeIds = new Set<Id>{accountCorporateRecordTypeId,accountGovernmentRecordTypeId};    

    global String gstrQuery = 'SELECT Id '+' FROM Account WHERE '+ 'recordtypeid IN :accountRecTypeIds AND Account_Lifecycle__c = \'Contract\'  ';

    //Loading and running the query string
    global Database.QueryLocator start(Database.BatchableContext BC){        
        return Database.getQueryLocator(gstrQuery);          
    }   

    /** Running The execute Method **/
    global void execute(Database.BatchableContext BC,List<sObject> scope)
    {
          // Cast the incoming list of Objects into Account objects.  
        List<Account> accList = new List <Account>();
                
        for (Sobject s : scope)
        {
           Account a = (Account)s;
            accList.add(a);
        }
        
        for(Integer x =0; x<accList.size(); x++)
        {

            Account  account = accList.get(x);

            // Look up an existing accounts with the same Business Number
            //     NOTE:  The logic below assumes this query looks for Accelerate and SmartFly accounts ONLY!!
            //            If you change that then review the DELETE account command below.
            List<Market__c> marketList = [SELECT Id FROM Market__c WHERE Contract__c in (select Id from  Contract where AccountId =:account.id  and Status =: 'Activated' ) ];
           
         
           
            if(marketList.size() > 0 )
           {
               processMarketData(account);
           }

        }
    }
    
     public void processMarketData(Account account)         
         
     {
         account.Total_Domestic_Spend_All_Airlines__c =   0 ;
         account.Total_VA_Domestic_Spend__c =   0 ;
         account.Total_VA_Domestic_MarketShare__c =  0 ;
         account.Total_International_Spend_All_Airlines__c =   0;
         account.Total_VA_International_Spend__c =   0 ;
         account.Total_VA_Intl_Market_Share__c =  0 ;
         
         
         List<aggregateResult> totdomresults =[SELECT SUM(Total_Spend__c) totspend , SUM(VA_Revenue_Commitment__c) vaspend  
                                            from Market__c
                                            where Contract__c in (select Id from  Contract where AccountId =:account.id  and Status =: 'Activated' )
                                            and   DOM_INT__c= 'DOMESTIC'	
                                            group by Contract__c
                                          ];  
         
         if (totdomresults.size() > 0)
       {    
         for(AggregateResult ar1 : totdomresults)
         {
           account.Total_Domestic_Spend_All_Airlines__c =   (decimal)ar1.get('totspend')==null?0:(decimal)ar1.get('totspend');
           account.Total_VA_Domestic_Spend__c =   (decimal)ar1.get('vaspend')==null?0:(decimal)ar1.get('vaspend');
           if (account.Total_Domestic_Spend_All_Airlines__c > 0) 
           {
           account.Total_VA_Domestic_MarketShare__c =  ((account.Total_VA_Domestic_Spend__c/ account.Total_Domestic_Spend_All_Airlines__c)*100).setScale(0);
           }
        }
       }
         
          List<aggregateResult> totintresults =[SELECT SUM(Total_Spend__c) totspend , SUM(VA_Revenue_Commitment__c) vaspend  
                                            from Market__c
                                            where Contract__c in (select Id from  Contract where AccountId =:account.id  and Status =: 'Activated' )
                                            and   DOM_INT__c= 'INTERNATIONAL'	
                                            group by Contract__c
                                          ]; 
         
         if (totintresults.size() > 0)
       {    
         for(AggregateResult ar2 : totintresults)
         {
           account.Total_International_Spend_All_Airlines__c =   (decimal)ar2.get('totspend')==null?0:(decimal)ar2.get('totspend');
           account.Total_VA_International_Spend__c =   (decimal)ar2.get('vaspend')==null?0:(decimal)ar2.get('vaspend');
           if ( account.Total_International_Spend_All_Airlines__c > 0)
           {
            account.Total_VA_Intl_Market_Share__c =  ((account.Total_VA_International_Spend__c/ account.Total_International_Spend_All_Airlines__c)*100).setScale(0);
           }

        }
       }
      
       try{
            update account ;          

        }
        catch(Exception e)
        {
            System.debug(Logginglevel.INFO,'** Failed ** ');
            //VirginsutilitiesClass.sendEmailError(e);
        }
  
     }
     global void finish(Database.BatchableContext BC){

    }
}