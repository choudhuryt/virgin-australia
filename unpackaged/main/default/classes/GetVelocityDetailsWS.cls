/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com) and Naveen V(naveenkumar.vankadhara@virginaustralia.com)
**Created Date: 22.03.2022
**Description: Webservice class to fetch comms details from ALMS
*/
public class GetVelocityDetailsWS {
    public static string getMethod = System.Label.GET_Method;
    public static string endpoint = System.Label.GET_endpoint;
    public static string authorization = System.Label.Authorization;
    public static string bearer = System.Label.Bearer;
    public static string x_APIKey = System.Label.X_APIKey;
    public static string apiKey_Value = System.Label.APIKey_Value;
    public static string clientChannel = System.Label.Client_Channel;
    public static string internal = System.Label.INTERNAL;
    public static string membershipId = System.Label.Membership_Id;
    public static string success = System.label.Success;
    public static string failed = System.label.Failed;
    
    public static Map<String, String> getVelDetFromAlms(String velocityNumber){
        List<Comms_Log__c> logList = new List<Comms_Log__c>();        
        Map<String, Object> responseMap = new Map<String, Object>();
        String status;
        String levelTier;
        String firstName;
        String lastName;
        String title;
        String email;
        String phoneNumber;
        String landLineNumber;
        String statusCode;
        String statusDetails;
        String errorDetail;
        String exeDetails;
        String address = 'Address Lines: ';
        Integer pointBalance;
        String birthDate;
        String gender;
        HttpResponse res;
        Map<String, String> velocityDetailsMap = new Map<String, String>();
        
        Integer attempt;
        
        if(velocityNumber != NULL){
            String accessToken = GetAccessToken.getToken();
            
            Http http = new Http();
            HttpRequest request = new HttpRequest();        
            request.setEndpoint(endpoint);
            request.setMethod(getMethod);
            request.setHeader(authorization, bearer + ' ' + accessToken);    
            request.setHeader(x_APIKey, apiKey_Value);
            request.setHeader(clientChannel, internal);
            request.setHeader(membershipId, velocityNumber);
            
            
            try{
                res= http.send(request); 
                attempt = 1;
            }catch(System.CalloutException exp){
                attempt = 1; 
                exeDetails = String.valueOf(exp);
                System.debug('Exception--'+exp);
                try{
                    res= http.send(request);
                    attempt = 2;
                }catch(System.CalloutException exp1){
                    attempt = 2; exeDetails = String.valueOf(exp1);
                    System.debug('Exception1--'+exp1);
                    try{
                        res= http.send(request);
                        attempt = 3;
                    }catch(System.CalloutException exp2){
                        attempt = 3; exeDetails = String.valueOf(exp2);
                        System.debug('Exception2--'+exp2);                    
                        try{
                            res= http.send(request);                        
                            attempt = 4;
                        }catch(System.CalloutException exp3){
                            attempt = 4; exeDetails = String.valueOf(exp3);
                            System.debug('Exception3--'+exp3);
                        }
                    }
                }
            } 
                        
            //System.debug('Full Response--'+res+' '+res.getBody());
            if(res != NULL){
                if(!res.getBody().startsWith('<')){
                    responseMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                }                
                statusCode = String.valueOf(res.getStatusCode()); 
                statusDetails = String.valueOf(res.getStatus());
            }            
            System.debug(statusCode+'--statusCode--'+statusDetails);
            
            if(statusCode != NULL && statusCode.startsWith('2')){
                status = success;                                 
                Map<String, Object> dataMap = (Map<String, Object>)responseMap.get('data');
                pointBalance = (Integer)dataMap.get('pointBalance');
                Map<String, Object> tierMap = (Map<String, Object>)dataMap.get('mainTier');
                levelTier = (String)tierMap.get('level');
                Map<String, Object> individualMap = (Map<String, Object>)dataMap.get('individual');
                if(individualMap !=NULL){
                   Map<String, Object> identityMap = (Map<String, Object>)individualMap.get('identity');
                     if(identityMap !=NULL){
                        firstName = (String)identityMap.get('firstName');
                        lastName = (String)identityMap.get('lastName');
                        title = (String)identityMap.get('title');
                        birthDate = (String)identityMap.get('birthDate');
                        gender = (String)identityMap.get('gender');
                      }   
                    Map<String, Object> contactMap = (Map<String, Object>)individualMap.get('contact');
                   
                    List<Object> emailList = (List<Object>)contactMap.get('emails');
                    if(emailList !=NULL){
                       Map<String, Object> emailsMap = (Map<String, Object>)emailList[0];
                       email = (String)emailsMap.get('address'); 
                     }
                 
                    List<Object> phoneList = (List<Object>)contactMap.get('phones');
                     if(phoneList !=NULL){
                       for(integer i=0; i<phoneList.size(); i++){
                          map<String, Object> phoneMap = (Map<String, Object>)phoneList[i];
                          String deviceType = (String)phoneMap.get('deviceType');
                          String phNumber = (String)phoneMap.get('number');
                          String countryCallingCode = (String)phoneMap.get('countryCallingCode');
                        if(deviceType == 'MOBILE'){
                          phoneNumber = '(+'+countryCallingCode+')-'+phNumber;
                        }else if(deviceType == 'LANDLINE'){
                         String areaCode = (String)phoneMap.get('areaCode');
                         landLineNumber = '(+'+countryCallingCode+')-'+areaCode+'-'+phNumber;
                      }
                    }    
                  }
                  List<Object> addressList = (List<Object>)contactMap.get('addresses');
                    if(addressList !=NULL ){ 
                      for(integer i=0; i<1; i++){
                        Map<String, Object> addressesMap = (Map<String, Object>)addressList[i];
                        List<Object> linesList = (List<Object>)addressesMap.get('lines'); System.debug(linesList.size()+'--linesList--'+linesList[0]);
                        for(integer j=0; j<linesList.size(); j++){
                        address = address + ', ' + (String)linesList[j];
                     }
                       address = address + ', City Name: ' + (String)addressesMap.get('cityName');
                       address = address + ', State Code: ' + (String)addressesMap.get('stateCode');
                       address = address + ', Country Code: ' + (String)addressesMap.get('countryCode');
                       address = address + ', Postal Code: ' + (String)addressesMap.get('postalCode');
                   }
                  }
                }    
                velocityDetailsMap.put('velocityNumber',velocityNumber); 
                if(levelTier != NULL){
                    velocityDetailsMap.put('Tier',levelTier);
                }
                if(firstName != NULL){
                    velocityDetailsMap.put('FirstName',firstName);
                }
                if(lastName != NULL){
                    velocityDetailsMap.put('LastName',lastName);
                }
                if(title != NULL){
                    velocityDetailsMap.put('Title',title);
                }
                if(email != NULL){
                    velocityDetailsMap.put('Email',email);
                }
                if(birthDate != NULL){
                    velocityDetailsMap.put('BirthDate',birthDate);
                }
                if(gender != NULL){
                    velocityDetailsMap.put('Gender',gender);
                }
                if(phoneNumber != NULL){
                    velocityDetailsMap.put('PhoneNumber',phoneNumber);
                }else if(phoneNumber == NULL && landLineNumber != NULL){
                    velocityDetailsMap.put('PhoneNumber',landLineNumber);
                }
                if(pointBalance != NULL){
                    velocityDetailsMap.put('PointBalance',String.valueOf(pointBalance));
                }
                if(address != NULL){
                    velocityDetailsMap.put('Address',address);
                }
            }else if(exeDetails != NULL){
                status = failed;
                errorDetail = exeDetails;                
            }else if(statusCode != NULL && !statusCode.startsWith('2')){
                status = failed;
                errorDetail = statusCode+'-'+statusDetails;
                if(String.isNotBlank(String.valueOf(responseMap.get('detail')))){
                    errorDetail = errorDetail+'-'+String.valueOf(responseMap.get('status'))+'-'+String.valueOf(responseMap.get('detail'));
                }else if(String.isNotBlank(String.valueOf(responseMap.get('description')))){
                    errorDetail = errorDetail+'-'+String.valueOf(responseMap.get('status'))+'-'+String.valueOf(responseMap.get('description'));
                }                                                
            }            
        }
        
        if(status != NULL){
            velocityDetailsMap.put('Status',status);
            velocityDetailsMap.put('Attempt',String.valueOf(attempt));
            if(status == failed){
                velocityDetailsMap.put('ErrorDetails',errorDetail);
            }
        }
        
        System.debug(velocityDetailsMap+'--Map & Tier--'+levelTier);
        return velocityDetailsMap;
    } 
}