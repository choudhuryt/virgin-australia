public with sharing class RelatedCasesForContact
{
  public Boolean isvalid {get;set;}   
  private Case c;  
  public List< Case> caselist {get;set;} 
     
    public RelatedCasesForContact(ApexPages.standardController std)
    {
        isvalid = false ;
        
        c=(Case)std.getrecord();  
                
         List<Case> orgcaselist = [SELECT Id,CaseNumber,SuppliedEmail,ContactId  FROM Case where id = :c.id ]; 
        
        if(orgcaselist.size() > 0  && orgcaselist[0].SuppliedEmail <> null  )
        {
                    
          
                
            caselist =[SELECT id,CaseNumber,CreatedDate,Origin,Owner.Name,Priority,RecordType.Name ,Status,Subject,SuppliedEmail FROM Case WHERE SuppliedEmail=:orgcaselist[0].SuppliedEmail and id <> :c.id ];
            if (caselist.size() == 0 )
                
            {
                  ApexPages.addMessage( new ApexPages.message( ApexPages.severity.INFO,  'There are no cases assoicated to this contact' ));
            } else if (caselist.size() > 0)
            {
               isvalid =true; 
            }
        }else if(orgcaselist.size() > 0  && orgcaselist[0].ContactId <> null )
        {
             caselist =[SELECT id,CaseNumber,CreatedDate,Origin,Owner.Name,Priority,RecordType.Name ,Status,Subject,SuppliedEmail FROM Case WHERE
                           ContactId=: orgcaselist[0].ContactId and id <> :c.id ];
            if (caselist.size() == 0 )
                
            {
                  ApexPages.addMessage( new ApexPages.message( ApexPages.severity.INFO,  'There are no cases assoicated to this contact' ));
            } else if (caselist.size() > 0)
            {
               isvalid =true; 
            }
        }
        else
        {
           
                  ApexPages.addMessage( new ApexPages.message( ApexPages.severity.INFO,  'There are no cases assoicated to this contact' ));
             
        }    
            
    }

}