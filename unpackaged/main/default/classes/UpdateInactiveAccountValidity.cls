global class UpdateInactiveAccountValidity {
@future


public static void UpdateTheAccount(Id l) {
	
                Date d= Date.today();
                List<Account_Data_Validity__c> advList = new List<Account_Data_Validity__c>();
				List<Account_Data_Validity__c> advListToUpdate = new List<Account_Data_Validity__c>();
                List<Account_Data_Validity__c> advListToInsert = new List<Account_Data_Validity__c>();
				advList = [select id,To_Date__c,Account_Owner__c,From_Date__c,Account_Record_Type__c,From_Account__c,
                                Market_Segment__c,Parent_Account__c,Sales_Matrix_Owner__c,Account_Record_Type_Picklist__c
                               from Account_Data_Validity__c where From_Account__c =:l  and To_Date__c = null and Soft_Delete__c = false ];
                
				system.debug('The adv list' + advList) ;
				for(Integer x =0; x< advList.size();x++)
                {
					Account_Data_Validity__c ad = new Account_Data_Validity__c();
                        ad = advList.get(x);
						ad.To_Date__c =d;
						advListToUpdate.add(ad);
                   
				}
				
				system.debug('The update list' +  advListToUpdate );
				system.debug('The insert list' +  advListToInsert  + advListToInsert.size()  );
    

	
	        /*   if(advListToInsert.size() > 0){
					
					insert advListToInsert;
					
				}	*/
	           
	          				if(advListToUpdate.size() > 0){
					
					update advListToUpdate;
					
				}
}

public static void UpdateTheContract(Id accountId) {
	
	
				Date d= Date.today();
				
				
				List<Opportunity> opportunityList = new List<Opportunity>();
				List<Opportunity> opportunityListToUpdate = new List<Opportunity>();
				
				opportunityList =[select id,StageName,Status__c from Opportunity where Accountid = :accountId and StageName=:'Open' ];
				
				for(Integer z =0; z < opportunityList.size(); z++){
					Opportunity opportunity = new Opportunity();
					 opportunity = opportunityList.get(z);
					 opportunity.Status__c = 'Account Closed';
					 opportunity.StageName = 'Closed Lost';
					 opportunity.Lost_Reason__c ='Account Closed';
					 
					 opportunityListToUpdate.add(opportunity);
				}
					
						if(opportunityListToUpdate.size() > 0){
							update opportunityListToUpdate;
						}			
				
				List<Contact> contactList = new List<Contact>();
				List<Contact> contactListUpdate = new List<Contact>();
				
				contactList = [select id, Status__c from Contact where Accountid =:accountId];
				
				for(Integer y =0; y < contactList.size(); y ++){
					Contact contact = new Contact();
					contact = contactList.get(y);
					contact.status__c = 'Inactive';
					contactListUpdate.add(contact);
					
				}
				
				if(contactListUpdate.size() > 0){
					update contactListUpdate;
				}
				
				//put logic here
				List<Contract> contractList = new List<Contract>();
				List<Contract> contractListToUpdate = new List<Contract>();
				contractList = [select id,recordtypeid,EndDate,Reason_for_Termination__c, status from Contract where Accountid=:accountId and status <>'Draft' ];
				
				
					for(Integer x = 0; x< contractList.size(); x++){
		
			       	Contract contract = contractList.get(x);
		
					if(contract.Status.equals('Activated') )
                    {
					    contract.Status = 'Terminated';
						contract.Reason_for_Termination__c = 'Closed Account';
						contract.EndDate = d;
                        contractListToUpdate.add(contract);
						
					}
					
					
					}
	
	               if(contractListToUpdate.size() > 0){
				update contractListToUpdate;
		         	}

			}
			
			

}