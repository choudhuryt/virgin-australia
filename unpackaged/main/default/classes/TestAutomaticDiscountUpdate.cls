/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAutomaticDiscountUpdate {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Account accountToTest = new Account();
        CommonObjectsForTest objForTest = new CommonObjectsForTest();
        
        accountToTest= objForTest.CreateAccountObject(0);
        //accountToTest.Velocity_Gold_Status_Match_Available__c = null;
       // accountToTest.Velocity_Platinum_Status_Match_Available__c = null;
        accountToTest.Account_Type__c = 'Corporate';
        insert accountToTest;
        
        Contract contractForTEST = new Contract();
        contractForTEST = objForTest.CreateOldStandardContract(0);
      	contractForTEST.AccountId = accountToTest.id;
      	contractForTEST.Gold__c= 10;
      	contractForTEST.Platinum__c =1;
      	contractForTEST.Pilot_Gold__c =1;
      	contractForTEST.Domestic_Short_Haul_Tier__c ='1';
      	contractForTEST.VA_DL_Requested_Tier__c ='1';
      	contractForTEST.VA_NZ_Requested_Tier__c='1';
      	contractForTEST.Cos_Version__c='11.2';
      	contractForTEST.Saver_Discounts__c ='Yes';
      	contractForTEST.Domestic_TT_VA_Market_Share_Target__c = 10;
      	contractForTEST.Virgin_Australia_Amount__c =100000;
      	insert contractForTEST;
      	contractForTEST.Status ='Activated';
      	//update contractForTEST;
      	
      	Contract contractForTEST1 = new Contract();
        contractForTEST1 = objForTest.CreateOldStandardContract(0);
      	contractForTEST1.AccountId = accountToTest.id;
      	contractForTEST1.Gold__c= 10;
      	contractForTEST1.Platinum__c =1;
      	contractForTEST1.Pilot_Gold__c =1;
      	contractForTEST1.Domestic_Short_Haul_Tier__c ='1';
      	contractForTEST1.VA_DL_Requested_Tier__c ='1';
      	contractForTEST1.VA_NZ_Requested_Tier__c='1';
      	contractForTEST1.Cos_Version__c='11.2';
      	contractForTEST1.Saver_Discounts__c ='Yes';
      	contractForTEST1.Domestic_and_Trans_Tasman_VA_Rev_Target__c =10000;
      	insert contractForTEST1;
      	contractForTEST1.Status ='Activated';
      	//update contractForTEST1;
      	
      	Domestic_Discounts__c dd = new Domestic_Discounts__c();
      	dd.Is_Template__c = true;
      	dd.Tier__c ='1';
      	dd.Cos_Version__c ='11.2';
      	dd.Contract__c = contractForTEST.id;
      	dd.Domestic_off_Published_Fare__c = 10 ;
        dd.Domestic_off_Published_Fare7__c =10;
        dd.Domestic_off_Published_Fare8__c =10;
                 //Economy Flexi Discount
        dd.Domestic_off_Published_Fare_2__c =10;
        dd.Domestic_off_Published_Fare_3__c =10;
        dd.Domestic_off_Published_Fare_4__c =10;
        dd.Domestic_off_Published_Fare_5__c =10;
        dd.Domestic_off_Published_Fare9__c =10;
                   //Economy Saver Discount
        dd.Domestic_off_Published_Fare10__c=10;
        dd.Domestic_off_Published_Fare_6__c =10;
        dd.Domestic_off_Published_Fare11__c=10;
        dd.Domestic_off_Published_Fare_12__c=10;
        dd.Domestic_off_Published_Fare_13__c=10;
      	insert dd;
      	
      	Domestic_Discounts__c dd1 = new Domestic_Discounts__c();
      	//dd1.Is_Template__c = true;
      	//dd1.Tier__c ='1';
      	//dd1.Cos_Version__c ='11.2';
      				dd1.Domestic_off_Published_Fare__c = 10 ;
                    dd1.Domestic_off_Published_Fare7__c =10;
                    dd1.Domestic_off_Published_Fare8__c =10;
                    //Economy Flexi Discount
                    dd1.Domestic_off_Published_Fare_2__c =10;
                    dd1.Domestic_off_Published_Fare_3__c =10;
                    dd1.Domestic_off_Published_Fare_4__c =10;
                    dd1.Domestic_off_Published_Fare_5__c =10;
                    dd1.Domestic_off_Published_Fare9__c =10;
                    //Economy Saver Discount
                    dd1.Domestic_off_Published_Fare10__c=10;
                    dd1.Domestic_off_Published_Fare_6__c =10;
                    dd1.Domestic_off_Published_Fare11__c=10;
                    dd1.Domestic_off_Published_Fare_12__c=10;
                    dd1.Domestic_off_Published_Fare_13__c=10;
      				dd1.Contract__c = contractForTEST1.id;
      	insert dd1;
      	
      	Intra_WA_Discounts__c dr = new Intra_WA_Discounts__c();
      	dr.Is_Template__c = true;
      	dr.Tier__c ='1';
      	dr.Cos_Version__c ='11.2';
      	dr.Intra_WA_Discount_off_Pub_Fare10_c__c =10.0;
      	dr.IntraWALookupToContract__c = contractForTEST.id;
      	insert dr;
      	
      	Intra_WA_Discounts__c dr1 = new Intra_WA_Discounts__c();
      	dr1.Intra_WA_Discount_off_Pub_Fare10_c__c =10.0;
      	dr1.IntraWALookupToContract__c = contractForTEST1.id;
      	insert dr1;
      	
      	Trans_Tasman_Discounts__c tt = new Trans_Tasman_Discounts__c();
      	tt.Is_Template__c = true;
      	tt.Tier__c ='1';
      	tt.Cos_Version__c ='11.2';
      	tt.Trans_Tasman_off_Published_Fare8__c =10.0;
      	tt.Trans_Tasman_Lookup_to_Contract__c = contractForTEST.id;
      	insert tt;
      	
      	Trans_Tasman_Discounts__c tt1 = new Trans_Tasman_Discounts__c();
      	
      	tt1.Trans_Tasman_off_Published_Fare8__c =10.0;
      	tt1.Trans_Tasman_Lookup_to_Contract__c = contractForTEST1.id;
      	insert tt1;
      	
      	International_Discounts_USA_Canada__c idelta = new International_Discounts_USA_Canada__c();
      	idelta.Is_Template__c = true;
      	idelta.Tier__c ='1';
      	idelta.Cos_Version__c ='11.2';
      	idelta.Intl_Discount_USA_CAN_off_PublishedFare3__c =10.0;
      	idelta.Int_DiscountUSACANLookupToContract__c = contractForTEST.id;
      	
      	insert idelta;
      	
      	International_Discounts_USA_Canada__c idelta1 = new International_Discounts_USA_Canada__c();
      	
      	idelta1.Intl_Discount_USA_CAN_off_PublishedFare3__c =10.0;
      	idelta1.Int_DiscountUSACANLookupToContract__c = contractForTEST1.id;
      	
      	insert idelta1;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel = new Contract_Calculation_Discount_Levels__c();
      	discountlevel.Business_Perentage_C_Class__c = 2.2;
      	discountlevel.Business_Perentage_J_Class__c = 2.2;
      	discountlevel.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel.Carrier__c = 'VA';
      	discountlevel.Category__c ='Trans Tasman';
      	discountlevel.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel.Premium_Economy_W_Class__c =5.0;
      	discountlevel.Premium_Saver_R_Class__c = 3.0;
      	discountlevel.Saver_Percentage_E_Class__c =2.0;
      	discountlevel.Saver_Percentage_N_Class__c =3.0;
      	discountlevel.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel1 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel1.Business_Perentage_C_Class__c = 2.2;
      	discountlevel1.Business_Perentage_J_Class__c = 2.2;
      	discountlevel1.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel1.Carrier__c = 'VA';
      	discountlevel1.Category__c ='Domestic';
      	discountlevel1.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel1.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel1.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel1.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel1.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel1.Premium_Economy_W_Class__c =5.0;
      	discountlevel1.Premium_Saver_R_Class__c = 3.0;
      	discountlevel1.Saver_Percentage_E_Class__c =2.0;
      	discountlevel1.Saver_Percentage_N_Class__c =3.0;
      	discountlevel1.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel1.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel1.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel1;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel2 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel2.Business_Perentage_C_Class__c = 2.2;
      	discountlevel2.Business_Perentage_J_Class__c = 2.2;
      	discountlevel2.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel2.Carrier__c = 'VA';
      	discountlevel2.Category__c ='International North America';
      	discountlevel2.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel2.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel2.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel2.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel2.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel2.Premium_Economy_W_Class__c =5.0;
      	discountlevel2.Premium_Saver_R_Class__c = 3.0;
      	discountlevel2.Saver_Percentage_E_Class__c =2.0;
      	discountlevel2.Saver_Percentage_N_Class__c =3.0;
      	discountlevel2.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel2.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel2.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel2;
      	
      	AutomaticDiscountUpdate adu = new AutomaticDiscountUpdate();
      	//testing with values
      	 
      	Test.startTest();
      	
      	adu.updateDomestic(contractForTEST1);
      	adu.updateDomesticRegional(contractForTEST1);
      	adu.updateTransTasman(contractForTEST1);
      	adu.updateDelta(contractForTEST1);
      	
      	test.stopTest();
        
    }
	
	
	static testMethod void test2() {
        // TO DO: implement unit test
        
        Account accountToTest = new Account();
        CommonObjectsForTest objForTest = new CommonObjectsForTest();
        
        accountToTest= objForTest.CreateAccountObject(0);
        //accountToTest.Velocity_Gold_Status_Match_Available__c = null;
       // accountToTest.Velocity_Platinum_Status_Match_Available__c = null;
        accountToTest.Account_Type__c = 'Corporate';
        insert accountToTest;
        
        Contract contractForTEST = new Contract();
        contractForTEST = objForTest.CreateOldStandardContract(0);
      	contractForTEST.AccountId = accountToTest.id;
      	contractForTEST.Gold__c= 10;
      	contractForTEST.Platinum__c =1;
      	contractForTEST.Pilot_Gold__c =1;
      	contractForTEST.Domestic_Short_Haul_Tier__c ='1';
      	contractForTEST.VA_DL_Requested_Tier__c ='1';
      	contractForTEST.VA_NZ_Requested_Tier__c='1';
      	contractForTEST.Cos_Version__c='11.2';
      	contractForTEST.Saver_Discounts__c ='Yes';
      	contractForTEST.Domestic_TT_VA_Market_Share_Target__c = 10;
      	contractForTEST.Virgin_Australia_Amount__c =100000;
		contractForTEST.North_America__c=10.0;
		contractForTEST.TT_Revenue_target__c=10.0;
      	insert contractForTEST;
      	contractForTEST.Status ='Activated';
      	//update contractForTEST;
      	
      	Contract contractForTEST1 = new Contract();
        contractForTEST1 = objForTest.CreateOldStandardContract(0);
      	contractForTEST1.AccountId = accountToTest.id;
      	contractForTEST1.Gold__c= 10;
      	contractForTEST1.Platinum__c =1;
      	contractForTEST1.Pilot_Gold__c =1;
      	contractForTEST1.Domestic_Short_Haul_Tier__c ='1';
      	contractForTEST1.VA_DL_Requested_Tier__c ='1';
      	contractForTEST1.VA_NZ_Requested_Tier__c='1';
      	contractForTEST1.Cos_Version__c='11.2';
      	contractForTEST1.Saver_Discounts__c ='Yes';
      	contractForTEST1.Domestic_and_Trans_Tasman_VA_Rev_Target__c =10000;
      	contractForTEST1.TT_Market_Share_target__c=10.0;
		contractForTEST1.Trans_Tasman_Amount__c=10.0;
		contractForTEST1.North_America_MS__c=10.0;
		contractForTEST1.USA_Canada_Amount__c=10.0;
      	insert contractForTEST1;
      	contractForTEST1.Status ='Activated';
      	//update contractForTEST1;
      	
      	Domestic_Discounts__c dd = new Domestic_Discounts__c();
      	dd.Is_Template__c = true;
      	dd.Tier__c ='1';
      	dd.Cos_Version__c ='11.2';
      	dd.Contract__c = contractForTEST.id;
      	dd.Domestic_off_Published_Fare__c = 10 ;
        dd.Domestic_off_Published_Fare7__c =10;
        dd.Domestic_off_Published_Fare8__c =10;
                 //Economy Flexi Discount
        dd.Domestic_off_Published_Fare_2__c =10;
        dd.Domestic_off_Published_Fare_3__c =10;
        dd.Domestic_off_Published_Fare_4__c =10;
        dd.Domestic_off_Published_Fare_5__c =10;
        dd.Domestic_off_Published_Fare9__c =10;
                   //Economy Saver Discount
        dd.Domestic_off_Published_Fare10__c=10;
        dd.Domestic_off_Published_Fare_6__c =10;
        dd.Domestic_off_Published_Fare11__c=10;
        dd.Domestic_off_Published_Fare_12__c=10;
        dd.Domestic_off_Published_Fare_13__c=10;
      	insert dd;
      	
      	Domestic_Discounts__c dd1 = new Domestic_Discounts__c();
      	//dd1.Is_Template__c = true;
      	//dd1.Tier__c ='1';
      	//dd1.Cos_Version__c ='11.2';
      				dd1.Domestic_off_Published_Fare__c = 10 ;
                    dd1.Domestic_off_Published_Fare7__c =10;
                    dd1.Domestic_off_Published_Fare8__c =10;
                    //Economy Flexi Discount
                    dd1.Domestic_off_Published_Fare_2__c =10;
                    dd1.Domestic_off_Published_Fare_3__c =10;
                    dd1.Domestic_off_Published_Fare_4__c =10;
                    dd1.Domestic_off_Published_Fare_5__c =10;
                    dd1.Domestic_off_Published_Fare9__c =10;
                    //Economy Saver Discount
                    dd1.Domestic_off_Published_Fare10__c=10;
                    dd1.Domestic_off_Published_Fare_6__c =10;
                    dd1.Domestic_off_Published_Fare11__c=10;
                    dd1.Domestic_off_Published_Fare_12__c=10;
                    dd1.Domestic_off_Published_Fare_13__c=10;
      				dd1.Contract__c = contractForTEST1.id;
      	insert dd1;
      	
      	Intra_WA_Discounts__c dr = new Intra_WA_Discounts__c();
      	dr.Is_Template__c = true;
      	dr.Tier__c ='1';
      	dr.Cos_Version__c ='11.2';
      	dr.Intra_WA_Discount_off_Pub_Fare10_c__c =10.0;
      	dr.IntraWALookupToContract__c = contractForTEST.id;
      	insert dr;
      	
      	Intra_WA_Discounts__c dr1 = new Intra_WA_Discounts__c();
      	dr1.Intra_WA_Discount_off_Pub_Fare10_c__c =10.0;
      	dr1.IntraWALookupToContract__c = contractForTEST1.id;
      	insert dr1;
      	
      	Trans_Tasman_Discounts__c tt = new Trans_Tasman_Discounts__c();
      	tt.Is_Template__c = true;
      	tt.Tier__c ='1';
      	tt.Cos_Version__c ='11.2';
		tt.Trans_Tasman_off_Published_Fare__c =10.0;
		tt.Trans_Tasman_off_Published_Fare2__c =10.0;
      	tt.Trans_Tasman_off_Published_Fare3__c =10.0;
		tt.Trans_Tasman_off_Published_Fare4__c =10.0;
      	tt.Trans_Tasman_off_Published_Fare5__c =10.0;
		tt.Trans_Tasman_off_Published_Fare6__c =10.0;
      	tt.Trans_Tasman_off_Published_Fare7__c =10.0;
		tt.Trans_Tasman_off_Published_Fare8__c =10.0;
      	tt.Trans_Tasman_off_Published_Fare9__c =10.0;
      	tt.Trans_Tasman_Lookup_to_Contract__c = contractForTEST.id;
      	insert tt;
      	
      	Trans_Tasman_Discounts__c tt1 = new Trans_Tasman_Discounts__c();
      	
      	tt1.Trans_Tasman_off_Published_Fare__c =10.0;
		tt1.Trans_Tasman_off_Published_Fare2__c =10.0;
      	tt1.Trans_Tasman_off_Published_Fare3__c =10.0;
		tt1.Trans_Tasman_off_Published_Fare4__c =10.0;
      	tt1.Trans_Tasman_off_Published_Fare5__c =10.0;
		tt1.Trans_Tasman_off_Published_Fare6__c =10.0;
      	tt1.Trans_Tasman_off_Published_Fare7__c =10.0;
		tt1.Trans_Tasman_off_Published_Fare8__c =10.0;
      	tt1.Trans_Tasman_off_Published_Fare9__c =10.0;
      	tt1.Trans_Tasman_Lookup_to_Contract__c = contractForTEST1.id;
      	insert tt1;
      	
      	International_Discounts_USA_Canada__c idelta = new International_Discounts_USA_Canada__c();
      	idelta.Is_Template__c = true;
      	idelta.Tier__c ='1';
      	idelta.Cos_Version__c ='11.2';
		idelta.Intl_Discount_USA_CAN_off_Published_Fare__c =10.0;
		idelta.Intl_Discount_USA_CAN_off_PublishedFare2__c =10.0;
      	idelta.Intl_Discount_USA_CAN_off_PublishedFare3__c =10.0;
		idelta.Intl_Discount_USA_CAN_off_PublishedFare4__c =10.0;
      	idelta.Intl_Discount_USA_CAN_off_PublishedFare5__c =10.0;
		idelta.Intl_Discount_USA_CAN_off_PublishedFare6__c =10.0;
      	idelta.Intl_Discount_USA_CAN_off_PublishedFare7__c =10.0;
		idelta.Intl_Discount_USA_CAN_off_PublishedFare8__c =10.0;
      	idelta.Intl_Discount_USA_CAN_off_PublishedFare9__c =10.0;		
      	idelta.Int_DiscountUSACANLookupToContract__c = contractForTEST.id;
      	
      	insert idelta;
      	
      	International_Discounts_USA_Canada__c idelta1 = new International_Discounts_USA_Canada__c();
      	
      	idelta1.Intl_Discount_USA_CAN_off_Published_Fare__c =10.0;
		idelta1.Intl_Discount_USA_CAN_off_PublishedFare2__c =10.0;
      	idelta1.Intl_Discount_USA_CAN_off_PublishedFare3__c =10.0;
		idelta1.Intl_Discount_USA_CAN_off_PublishedFare4__c =10.0;
      	idelta1.Intl_Discount_USA_CAN_off_PublishedFare5__c =10.0;
		idelta1.Intl_Discount_USA_CAN_off_PublishedFare6__c =10.0;
      	idelta1.Intl_Discount_USA_CAN_off_PublishedFare7__c =10.0;
		idelta1.Intl_Discount_USA_CAN_off_PublishedFare8__c =10.0;
      	idelta1.Intl_Discount_USA_CAN_off_PublishedFare9__c =10.0;	
      	idelta1.Int_DiscountUSACANLookupToContract__c = contractForTEST1.id;
      	
      	insert idelta1;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel = new Contract_Calculation_Discount_Levels__c();
      	discountlevel.Business_Perentage_C_Class__c = 2.2;
      	discountlevel.Business_Perentage_J_Class__c = 2.2;
      	discountlevel.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel.Carrier__c = 'VA';
      	discountlevel.Category__c ='Trans Tasman';
      	discountlevel.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel.Premium_Economy_W_Class__c =5.0;
      	discountlevel.Premium_Saver_R_Class__c = 3.0;
      	discountlevel.Saver_Percentage_E_Class__c =2.0;
      	discountlevel.Saver_Percentage_N_Class__c =3.0;
      	discountlevel.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel1 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel1.Business_Perentage_C_Class__c = 2.2;
      	discountlevel1.Business_Perentage_J_Class__c = 2.2;
      	discountlevel1.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel1.Carrier__c = 'VA';
      	discountlevel1.Category__c ='Domestic';
      	discountlevel1.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel1.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel1.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel1.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel1.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel1.Premium_Economy_W_Class__c =5.0;
      	discountlevel1.Premium_Saver_R_Class__c = 3.0;
      	discountlevel1.Saver_Percentage_E_Class__c =2.0;
      	discountlevel1.Saver_Percentage_N_Class__c =3.0;
      	discountlevel1.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel1.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel1.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel1;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel2 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel2.Business_Perentage_C_Class__c = 2.2;
      	discountlevel2.Business_Perentage_J_Class__c = 2.2;
      	discountlevel2.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel2.Carrier__c = 'VA';
      	discountlevel2.Category__c ='International North America';
      	discountlevel2.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel2.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel2.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel2.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel2.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel2.Premium_Economy_W_Class__c =5.0;
      	discountlevel2.Premium_Saver_R_Class__c = 3.0;
      	discountlevel2.Saver_Percentage_E_Class__c =2.0;
      	discountlevel2.Saver_Percentage_N_Class__c =3.0;
      	discountlevel2.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel2.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel2.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel2;
      	
      	AutomaticDiscountUpdate adu = new AutomaticDiscountUpdate();
      	//testing with values
      	 
      	Test.startTest();
      	
		adu.UpdateCostOFSaleRecordForDL(25.0,25.0,contractForTEST1,25.0);
		adu.UpdateContractPercentagesForContractCalculationForDL(contractForTEST,idelta);
		adu.UpdateCostOFSaleRecordForTrans(25.0,25.0,contractForTEST1,25.0);
		adu.UpdateContractPercentagesForContractCalculationForTrans(contractForTEST,tt);
		adu.UpdateContractPercentagesForContractCalculationForTrans(contractForTEST1,tt1);
		adu.UpdateContractPercentagesForContractCalculation(contractForTEST,dd);
      	
      	test.stopTest();        
    }
	
}