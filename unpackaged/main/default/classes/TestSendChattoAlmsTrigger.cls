/**
* @description       : Test class for SendChattoAlmsTrigger
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestSendChattoAlmsTrigger {
    
    @testSetup 
    private static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '0000360500', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        INSERT testCaseObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'abc@gmail.com', 'Test', 'User', 'Testing','345943201', 'Active', 'First Nominee', '0004563210');
        INSERT testContactObj;
    }
    
    @isTest 
    private static void testUpdateStatusSendChattoAlmsTrigger(){
        createTestLiveChatTranscript();
        Account testAccountObj = [SELECT Id, Name FROM Account LIMIT 1];
        LiveChatTranscript testLiveChatTranscriptObj= [SELECT Id, Status, Body FROM LiveChatTranscript 
                                                       WHERE AccountId =:testAccountObj.Id LIMIT 1];
        Test.startTest();
        testLiveChatTranscriptObj.Status = 'Completed';
        testLiveChatTranscriptObj.Body = 'It is body of test class';
        UPDATE testLiveChatTranscriptObj;
        Test.stopTest();
        //Asserts
        System.assertEquals('Completed', testLiveChatTranscriptObj.Status);
    }
    
    @isTest 
    private static void testUpdateContactSendChattoAlmsTrigger() {
        createTestLiveChatTranscript();
        Account testAccountObj = [SELECT Id, Name FROM Account LIMIT 1];
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'abc@gmail.com', 'Test', 'User', 'Testing','345943201', 'Active', 'First Nominee', '0004563210');
        INSERT testContactObj;
        
        LiveChatTranscript testLiveChatTranscriptObj = [SELECT Id, Body, ContactId FROM LiveChatTranscript LIMIT 1];
        Test.startTest();
        testLiveChatTranscriptObj.Body = 'It is body of test class';
        testLiveChatTranscriptObj.ContactId = testContactObj.Id;
        UPDATE testLiveChatTranscriptObj;
        Test.stopTest();
        //Asserts
        System.assertEquals('It is body of test class', testLiveChatTranscriptObj.Body);
    }
    
    @isTest 
    private static void testUpdateCaseSendChattoAlmsTrigger() {
        createTestLiveChatTranscript();
        Account testAccountObj = [SELECT Id, Name FROM Account LIMIT 1];
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '0000360500', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        INSERT testCaseObj;
        
        LiveChatTranscript testLiveChatTranscriptObj = [SELECT Id, Body, ContactId FROM LiveChatTranscript LIMIT 1];
        Test.startTest();
        testLiveChatTranscriptObj.Body = 'It is body of test class';
        testLiveChatTranscriptObj.CaseId = testCaseObj.Id;
        UPDATE testLiveChatTranscriptObj;
        Test.stopTest();
        //Asserts
        System.assertEquals('It is body of test class', testLiveChatTranscriptObj.Body);
    }
    
    @isTest 
    private static void testUpdateSendChattoAlmsTrigger() {
        createTestLiveChatTranscript();
        LiveChatTranscript testLiveChatTranscriptObj = [SELECT Id, Body, Velocity_Number__c, Status FROM LiveChatTranscript LIMIT 1];
        Test.startTest();
        testLiveChatTranscriptObj.Body = 'It is body of test class';
        testLiveChatTranscriptObj.Velocity_Number__c = '0123456789';
        UPDATE testLiveChatTranscriptObj;
        Test.stopTest();
        //Asserts
        System.assertEquals('It is body of test class', testLiveChatTranscriptObj.Body);
    }
    
    private static void createTestLiveChatTranscript() {
        Account testAccountObj = [SELECT Id, Name FROM Account LIMIT 1];
        Contact testContactObj = [SELECT Id FROM Contact LIMIT 1];
        Case testCaseObj = [SELECT Id FROM Case LIMIT 1];
        
        LiveChatVisitor testLiveChatVisitorObj = new LiveChatVisitor();
        INSERT testLiveChatVisitorObj;
        
        LiveChatTranscript testLiveChatTranscriptObj = TestDataFactory.createLiveChatTranscript(testLiveChatVisitorObj.Id, testAccountObj.Id, testCaseObj.Id, testContactObj.Id);
        INSERT testLiveChatTranscriptObj;
        
        LiveChatTranscriptEvent testLiveChatTranscriptEventPbj = TestDataFactory.createLiveChatTranscriptEvent(testLiveChatTranscriptObj.Id, 'ChatRequest');
        testLiveChatTranscriptEventPbj.time = system.now();
        INSERT testLiveChatTranscriptEventPbj;
    }
}