@isTest(SeeAllData=true)
public class TestCompensationScorecardController
{
    
     static testMethod void myUnitTest1() 
    {
        
	Test.startTest();	
        
     Case c = new Case(SuppliedEmail='jdoe_test_test@doe.com',
                                  SuppliedName='John Doe',
                                 Velocity_Member__c = false,
                                 Subject='Feedback - Something',
                                 Contact_First_Name__c='Tom',
                               Contact_Last_name__c = 'Johns');
        insert c ;
        
      Compensation_Scorecard__c ccard1 = new Compensation_Scorecard__c(Case__c =c.id,
                                Question__c='Have we done the wrong thing and or Inconvenienced the Guest?',
                               Answer__c= 'Yes',
                               Valid_Answer__c ='Yes',
                               Categoty__c='Facts', 
                              Score__c = 30 ,
                             Total_Points_per_Question__c = 'es = 30p'          );
        insert ccard1 ;
        
        Compensation_Scorecard__c ccard2 = new Compensation_Scorecard__c(Case__c =c.id,
                                Question__c='Have we done the wrong thing and or Inconvenienced the Guest?',
                               Answer__c= 'Yes',
                               Valid_Answer__c ='Yes',
                               Categoty__c='Communication', 
                              Score__c = 30 ,
                             Total_Points_per_Question__c = 'es = 30p'          );
        insert ccard2 ;
        
        Compensation_Scorecard__c ccard3 = new Compensation_Scorecard__c(Case__c =c.id,
                                Question__c='Have we done the wrong thing and or Inconvenienced the Guest?',
                               Answer__c= 'Yes',
                               Valid_Answer__c ='Yes',
                               Categoty__c='Customer', 
                              Score__c = 30 ,
                             Total_Points_per_Question__c = 'es = 30p'          );
        insert ccard3 ;
        
        Compensation_Scorecard__c ccard4 = new Compensation_Scorecard__c(Case__c =c.id,
                                Question__c='Have we done the wrong thing and or Inconvenienced the Guest?',
                               Answer__c= 'Yes',
                               Valid_Answer__c ='Yes',
                               Categoty__c='Channel', 
                              Score__c = 30 ,
                             Total_Points_per_Question__c = 'es = 30p'          );
        insert ccard4 ;
        
        Compensation_Scorecard__c ccard5 = new Compensation_Scorecard__c(Case__c =c.id,
                                Question__c='Have we done the wrong thing and or Inconvenienced the Guest?',
                               Answer__c= 'Yes',
                               Valid_Answer__c ='Yes',
                               Categoty__c='Circumstances', 
                              Score__c = 30 ,
                             Total_Points_per_Question__c = 'es = 30p'          );
        insert ccard5 ;
        
         PageReference  ref1 = Page.CompensationScorecardPage;
        ref1.getParameters().put('id', c.Id);
        Test.setCurrentPage(ref1);
        ApexPages.StandardController conL = new ApexPages.StandardController(c);
        CompensationScorecardController lController = new CompensationScorecardController(conL);       
        ref1 = lController.initDisc();
       ref1 = lController.cancel();
        ref1 = lController.save();
       
        Test.stopTest();
        
    }
    
      static testMethod void myUnitTest2() 
    {
        
	
        
        
      Case case1 = new Case(SuppliedEmail='jdoe_test_test@doe.com',
	                            SuppliedName='John Doe',
	                            Subject='Feedback - Something',
	                            Contact_First_Name__c='Tom',
	                            Contact_Last_name__c = 'Johns' ); 
        insert case1 ;
       
     Test.startTest();	
        PageReference  ref = Page.CompensationScorecardPage;
        ref.getParameters().put('id', case1.Id);
        Test.setCurrentPage(ref);
       ApexPages.StandardController conL = new ApexPages.StandardController(case1);
        CompensationScorecardController lController = new CompensationScorecardController(conL);       
        ref = lController.initDisc();
       ref = lController.cancel();
        ref = lController.save();
       
        Test.stopTest();

        
    }
}