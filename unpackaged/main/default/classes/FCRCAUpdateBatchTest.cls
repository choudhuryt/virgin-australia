/**
* @description       : Test class for FCRCAUpdateBatch
* @Updated By         : Cloudwerx
**/
@isTest
private class FCRCAUpdateBatchTest {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        
        Contract_Addendum__c testContractAddendumObj = TestDataFactory.createTestContractAddendum(testContractObj.Id, '16.0', 'Tier 1', 'Tier 1', true, 'SYDMEL', 'PERKTH', 'Draft');
        INSERT testContractAddendumObj;
        
        Proposed_Discount_Tables__c testProposedDiscountTablesObj = TestDataFactory.createTestProposedDiscountTables('DOM Mainline', testContractAddendumObj.Id, 10, 'Business', 'J');
        INSERT testProposedDiscountTablesObj;
    }
    
    @isTest
    private static void testFCRCAUpdateBatch() {
        Id contractAddendumId = [SELECT Id FROM Contract_Addendum__c LIMIT 1]?.Id;
        Test.startTest();
        FCRCAUpdateBatch fcrCAUpdateBatch= new FCRCAUpdateBatch();
        Id jobid= Database.executeBatch(fcrCAUpdateBatch, 1);
        Test.stopTest();
        //Asserts
        List<Proposed_Discount_Tables__c> populatedProposedDiscountTables = [SELECT Id, FCR_Contract_Amendment_Back_Up__c, FCR_Processed__c, 
                                                                             FCR_For_Deletion__c, FCR_Remarks__c 
                                                                             FROM Proposed_Discount_Tables__c];
        System.assertEquals(1, populatedProposedDiscountTables.size());
        System.assertEquals(contractAddendumId, populatedProposedDiscountTables[0].FCR_Contract_Amendment_Back_Up__c);
        System.assertEquals(true, populatedProposedDiscountTables[0].FCR_Processed__c);
        System.assertEquals(true, populatedProposedDiscountTables[0].FCR_For_Deletion__c);
        System.assertEquals('Old DOM Record marked for Deletion', populatedProposedDiscountTables[0].FCR_Remarks__c);
    }
}