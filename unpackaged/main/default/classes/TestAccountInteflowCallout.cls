/**
 * @description       : Test class for AccountInteflowCallout
 * @Created By         : Cloudwerx
 * @Updated By         : Cloudwerx
**/
@isTest
private class TestAccountInteflowCallout {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        testAccountObj.AccountSource = 'Smartfly';
        testAccountObj.Business_Number__c = 'ABC';
        testAccountObj.FC_Store_Name__c = 'fcStore';
        testAccountObj.Website = 'www.salesforce.com';
        INSERT testAccountObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        testContactObj.Key_Contact__c = true;
        INSERT testContactObj;	        
    }
    
    @isTest
    private static void testAccountInteflowCallout() {
        Id accId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, getMockObject());
        AccountInteflowCallout.callrequesttoInteflow(accId);
        Test.stopTest();
        //Asserts
        Account updatedAccountObj = [SELECT Id, Inteflow_Request__c, Inteflow_Response__c FROM Account WHERE Id =:accId];
        System.assertEquals(true, String.isNotBlank(updatedAccountObj.Inteflow_Request__c));
        System.assertEquals(true, String.isNotBlank(updatedAccountObj.Inteflow_Response__c));
    }
    
    @isTest
    private static void testAccountInteflowCalloutNextCriteria() {
        Account testAccountObj = [SELECT Id, Account_Manager_Email_Address__c, Business_Activity__c FROM Account LIMIT 1];
        testAccountObj.Account_Manager_Email_Address__c = 'accountmanager@yahoo.com';
        testAccountObj.Business_Activity__c = 'businessActivity';
        UPDATE testAccountObj;
        
        Test.startTest();      
        Test.setMock(HttpCalloutMock.class, getMockObject());
        AccountInteflowCallout.callrequesttoInteflow(testAccountObj.Id);
        Test.stopTest();
        //Asserts
        Account updatedAccountObj = [SELECT Id, Inteflow_Request__c, Inteflow_Response__c FROM Account WHERE Id =:testAccountObj.Id];
        System.assertEquals(true, String.isNotBlank(updatedAccountObj.Inteflow_Request__c));
        System.assertEquals(true, String.isNotBlank(updatedAccountObj.Inteflow_Response__c));
    }
    
    private static StaticResourceCalloutMock getMockObject() {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TestInteflowResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
        return mock;
    }
}