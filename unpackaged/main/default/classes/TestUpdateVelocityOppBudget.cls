/**
* @description       : Test class for UpdateOpportunityBudget
* @Updated By          : Cloudwerx
**/
@isTest
private class TestUpdateVelocityOppBudget {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;  
        
        Opportunity testOpportunityObj = TestDataFactory.createTestOpportunity('testOpp1', 'Sales Opportunity Analysis', testAccountObj.Id, Date.today(), 1.00);
        INSERT testOpportunityObj;
        
        Budget__c testBudgetObj = TestDataFactory.createTestBudget(testAccountObj.Id, 1000.0, String.valueOf(Date.today().year()), String.valueOf(Date.today().year()), String.valueOf(Date.today().month()), 'Velocity', 'Velocity' + system.now(), Date.today());
        INSERT testBudgetObj;
    }
    
    @isTest
    private static void testUpdateOpportunityBudget() {
        Id oppId = [SELECT Id FROM Opportunity LIMIT 1]?.Id;
        Id budgetId = [SELECT Id FROM Budget__c LIMIT 1]?.Id;
        Test.startTest();
        UpdateOpportunityBudget.UpdateVelocityOpportunityBudeget(new List<Id>{oppId}) ;
        Test.stopTest();
        //Asserts
        Opportunity updatedOpp = [SELECT Id, Budget__c FROM Opportunity WHERE Id =:oppId];
        system.assertEquals(budgetId, updatedOpp.Budget__c);
    }
}