/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 

 
@isTest 
private class TestLoyaltyAPI {

	static testMethod void myUnitTest() {

	}
	
public static testMethod void testTest() {
	
		/** Setup some objects for the test **/
		// Create a default Case object
		//Case caseobj = new Case(Status='New');
	
	    // Reference a static XML resource for Mock WS callouts.
		// i.e. the XML in the resource is what WS calls will return.
		//StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		//mock.setStaticResource('loyaltyTier_G');
		//mock.setStatusCode(200);
		//mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
		//mock.setHeader('Content-Type', 'application/json');        
		//Test.setMock(HttpCalloutMock.class, mock);

		/** Test the test.  i.e. lets make sure this mock callout is working as expected...  **/
		//HttpRequest req = new HttpRequest();  
		//req.setMethod('POST');  
		//req.setEndpoint('https://soa.virginblue.com.au/LoyaltyTierMediatorOSB/proxy/v1/LoyaltyTierMediator');  
		//req.setMethod('POST');  
		//req.setHeader('Content-Type', 'text/xml; charset=utf-8');  
		//req.setHeader('SOAPAction', 'https://api.authorize.net/soap/v1/CreateCustomerProfile');//
		//Http http = new Http();  

		//Execute web service call
		//HTTPResponse res;
		//res = http.send(req);
		//XmlStreamReader reader = res.getXmlStreamReader();

		//Integer safetycount = 0;
		//while(reader.hasNext() && safetycount < 200) {
			//safetycount++;
			//if(reader.getEventType() == XmlTag.START_ELEMENT){
				//if(reader.getLocalName().equals('loyaltyTier')){ 			
					//reader.next();			
					//System.debug('Value_Test:' +  reader.getText());
					//break;
				//}
			//}

			//reader.next();
		//}
}
	
	
public static testMethod void testLoyaltyAPI_G() {
	
	    /** Setup some objects for the test **/
		// Create a default Case object
		Case caseobj = new Case(Status='New');
	
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('loyaltyTier_G');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
		mock.setHeader('Content-Type', 'application/json');        
		Test.setMock(HttpCalloutMock.class, mock);
		
        test.startTest();
		LoyaltyAPI.invokeExternalWs(caseobj.id, '5420003303');
		test.stopTest();
}
	
	
public static testMethod void testLoyaltyAPI_P() {
	
	    /** Setup some objects for the test **/
		// Create a default Case object
		Case caseobj = new Case(Status='New');
	
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('loyaltyTier_P');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
		mock.setHeader('Content-Type', 'application/json');        
		Test.setMock(HttpCalloutMock.class, mock);
		
		test.startTest();
		LoyaltyAPI.invokeExternalWs(caseobj.id, '5420003303');
		test.stopTest();
		
}	

public static testMethod void testLoyaltyAPI_R() {
	
	    /** Setup some objects for the test **/
		// Create a default Case object
		Case caseobj = new Case(Status='New');
	
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('loyaltyTier_R');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
		mock.setHeader('Content-Type', 'application/json');        
		Test.setMock(HttpCalloutMock.class, mock);
		
		test.startTest();
		LoyaltyAPI.invokeExternalWs(caseobj.id, '5420003303');
		test.stopTest();
}

public static testMethod void testLoyaltyAPI_S() {
	
	    /** Setup some objects for the test **/
		// Create a default Case object
		Case caseobj = new Case(Status='New');
	
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('loyaltyTier_S');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
		mock.setHeader('Content-Type', 'application/json');        
		Test.setMock(HttpCalloutMock.class, mock);
		
		test.startTest();
		LoyaltyAPI.invokeExternalWs(caseobj.id, '5420003303');
		test.stopTest();
}

public static testMethod void testLoyaltyAPI_V() {
	
	    /** Setup some objects for the test **/
		// Create a default Case object
		Case caseobj = new Case(Status='New');
	
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('loyaltyTier_V');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
		mock.setHeader('Content-Type', 'application/json');        
		Test.setMock(HttpCalloutMock.class, mock);
		
		test.startTest();
		LoyaltyAPI.invokeExternalWs(caseobj.id, '5420003303');
		test.stopTest();
}

public static testMethod void testLoyaltyAPI_Error() {
	
	    /** Setup some objects for the test **/
		// Create a default Case object
		Case caseobj = new Case(Status='New');
	
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('loyaltyTier_Error');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
		mock.setHeader('Content-Type', 'application/json');        
		Test.setMock(HttpCalloutMock.class, mock);
		
		test.startTest();
		LoyaltyAPI.invokeExternalWs(caseobj.id, '5420003303');
		test.stopTest();
}

	
}