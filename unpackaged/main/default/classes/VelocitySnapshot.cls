/** 
*  File Name       : VelocitySnapshot 
*
*  Description     : Represents a virtual Velocity card.  The data is fetched from Crane 
*                      
*                      
*  Copyright       : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
*  @author         : Steve Kerr
*  Date            : 10th November 2015
*
*  Notes           : 
* UpdatedBy: cloudwerx (Did indentation)
**/ 
/*
**Modified By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com) and Naveen V(naveenkumar.vankadhara@virginaustralia.com)
**Modified Date: 22.03.2022
**Description: Modified to enable ALMS
*/

public with sharing class VelocitySnapshot {
    
    public String velocityNumber {get; set;}
    public String tierName {get; set;}
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String title {get; set;}
    public String cardImageName {get; set;} // Name of the card image, inside the zip file.
    public String dis {get; set;}  // This is used to hide the picture (when set to 'none')
    public String err {get; set;}  // Error message that will appear at the top.
    public String message {get; set;}  // Message that will appear at the top.
    public static string almsActivated = system.Label.ALMS_Integration;
    String tier;
        
    /*
    * Constructor 
    */
    public VelocitySnapshot(String velNum, String name){
        velocityNumber = velNum;
        tierName = '';
        firstName = '** Not Found ***';
        lastName = '';
        title = '';
        cardImageName = '';
        dis = 'block';
        err = '';
        
        if(velocityNumber.length() > 0){
            fetchDetails(name);
        } else {
            dis = 'none';
        }
    }
    
    /*
    * Fetch the data from Crane
    */
    private void fetchDetails(String name){
        Boolean testAlmsActivated = false;
        List<ALMS_Integration__c> flagList = [SELECT Activate_GET__c FROM ALMS_Integration__c WHERE Name =:'Activate ALMS'];
        if(flagList.size() > 0){ 
            testAlmsActivated = flagList[0].Activate_GET__c; 
        } 
        
        if(testAlmsActivated || almsActivated == 'Inactive') {
            GuestCaseVelocityInfoModel.ProfileInformationType profileInfo;
            GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType velocityDetails;
            GuestCaseVelocityInfoModel.MemberInformationType memberInfo;
            GuestCaseVelocityInfoModel.PersonType person;
            
            // Callout setup
            GuestCaseVelocityInfo.SalesforceLoyaltyService service = new GuestCaseVelocityInfo.SalesforceLoyaltyService();
            Apex_Callouts__c apexCallouts = Apex_Callouts__c.getValues('VelocityDetails');
            service.endpoint_x = apexCallouts.Endpoint_URL__c;
            service.timeout_x = Integer.valueOf(apexCallouts.Timeout_ms__c);
            service.clientCertName_x = apexCallouts.Certificate__c;
            
            // Fetch details
            try {
                velocityDetails = service.GetLoyaltyDetails(velocityNumber);
                // Extract details
                profileInfo = velocityDetails.ProfileInformation;                          
                tier = profileInfo.TierLevel;                 
                memberInfo = velocityDetails.MemberInformation;
                person = memberInfo.Person;                
                firstName = person.GivenName;
                lastName = person.Surname;
                title = person.Title;
                if(tier.equals('R')) {
                    tierName = 'Red';
                    cardImageName = 'Red.jpg';
                } else if(tier.equals('S')) {
                    tierName = 'Silver';
                    cardImageName = 'Silver.jpg';
                } else if(tier.equals('G')) {
                    tierName = 'Gold';
                    cardImageName = 'Gold.jpg';
                } else if(tier.equals('P')) {
                    tierName = 'Platinum';
                    cardImageName = 'Platium.jpg';
                } else if(tier.equals('V')) {
                    tierName = 'VIP';
                    cardImageName = '';
                    dis = 'none';
                    message = 'VIP Club Member, ' 
                        + title + ' ' + firstName + ' ' + lastName + ', ' 
                        + velocityNumber;
                }
                // We got this far, excellent.  Now we check if the name matches
                String cardName = firstName + ' ' + lastName;
                if(!name.toLowerCase().equals(cardName.toLowerCase())){
                    err = '*** WARNING: The name on the card doesn\'t match the name given. ***';
                }
            } catch(Exception e){
                dis = 'none';
                // Check if not found			
                if(e.getMessage().contains('does not correspond to a current Member')){
                    err = 'Please check the number.  Record not found';
                } else {
                    err = e.getMessage();	
                }
            }
        } else if(almsActivated == 'Active'){
            String cardName;
            String status;
            String errorDetails;
            Integer attempt;
            Map<String, String> velDetailsMap = GetVelocityDetailsWS.getVelDetFromAlms(velocityNumber); 
            System.debug(velocityNumber+'--velDetailsMap--'+velDetailsMap);
            
            if(velDetailsMap.size() > 0){
                if(velDetailsMap.containsKey('Tier') && velDetailsMap.get('Tier') != null) {
                    tier = velDetailsMap.get('Tier');                     
                } else {
                    tier = null;
                }
                if(velDetailsMap.containsKey('FirstName') && velDetailsMap.get('FirstName') != null){
                    firstName = velDetailsMap.get('FirstName');
                    cardName = firstName;
                } else {
                    firstName = null;
                }
                if(velDetailsMap.containsKey('LastName') && velDetailsMap.get('LastName') != null) {
                    lastName = velDetailsMap.get('LastName');
                    cardName = cardName + ' ' + lastName;
                } else {
                    lastName = null;
                }
                if(velDetailsMap.containsKey('Title') && velDetailsMap.get('Title') != null) {
                    title = velDetailsMap.get('Title');
                } else {
                    title = null;
                }
                if(velDetailsMap.containsKey('Attempt') && velDetailsMap.get('Attempt') != null) {
                    attempt = Integer.valueOf(velDetailsMap.get('Attempt'));
                } 
                if(velDetailsMap.containsKey('Status') && velDetailsMap.get('Status') != null) {
                    status = velDetailsMap.get('Status');
                } 
                if(velDetailsMap.containsKey('ErrorDetails') && velDetailsMap.get('ErrorDetails') != null) {
                    errorDetails = velDetailsMap.get('ErrorDetails');
                }
            } else {
                dis = 'none';
                err = 'Please check the number.  Record not found';
            }

            if(tier != null){
                if(tier.equalsIgnoreCase('Red') || tier.equalsIgnoreCase('R')) {
                    tierName = 'Red';
                    cardImageName = 'Red.jpg';            
                } else if(tier.equalsIgnoreCase('Silver') || tier.equalsIgnoreCase('S')){
                    tierName = 'Silver';
                    cardImageName = 'Silver.jpg';                        
                } else if(tier.equalsIgnoreCase('Gold') || tier.equalsIgnoreCase('G')) {
                    tierName = 'Gold';
                    cardImageName = 'Gold.jpg';                        
                } else if(tier.equalsIgnoreCase('Platinum') || tier.equalsIgnoreCase('P')) {
                    tierName = 'Platinum';
                    cardImageName = 'Platium.jpg';                        
                } else if(tier.equalsIgnoreCase('VIP') || tier.equalsIgnoreCase('V')) {
                    tierName = 'VIP';
                    cardImageName = '';
                    dis = 'none';
                    message = 'VIP Club Member, ';
                    if(title != null){
                        message = message+title+' ';
                    }
                    if(firstName != null){
                        message = message+firstName+' ';
                    }
                    if(lastName != null){
                        message = message+lastName;
                    }
                    message = message+', '+velocityNumber;
                }
            }

            if(cardName != null && !name.equalsIgnoreCase(cardName)){
                err = '*** WARNING: The name on the card doesn\'t match the name given. ***';
            } 
            
            //List<Comms_Log__c> logList = new List<Comms_Log__c>();
            /*Comms_Log__c lg = new Comms_Log__c();
            lg.Type__c = 'Visualforce Page';
            lg.Attempts__c = attempt;
            lg.Status__c = status;
            lg.Request_Type__c = 'GET';
            lg.Velocity_Number__c = velocityNumber;
            if(errorDetails != null){
                lg.Error__c = errorDetails;
            }
            System.debug('VF Log--'+lg);
            insert lg;*/
        }                        
    }    
}