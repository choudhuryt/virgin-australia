global class PaymentBatchAdhoc implements Database.Batchable<SObject>
{
    
   global PaymentBatchAdhoc(){}

    global Integer month = system.now().month();
 
  global String gstrQuery = 'Select Id,ContractNumber,AccountId,StartDate from Contract where Record_Type__c = \'Accelerate POS/Rebate\' and Valid_For_Payment_Processing__c = true' ;
 //global String gstrQuery = 'Select Id,ContractNumber,AccountId,StartDate from Contract where Record_Type__c = \'Accelerate POS/Rebate\' and Anniversary_Month__c = \'April\'  ' ;
    global Integer howManyrecordsUpdated = 0;
   
    
    //Loading and running the query string
    global Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator(gstrQuery);
    }   

    //Running The execute Method
    global void execute(Database.BatchableContext BC,List<sObject> scope)
    {
        List<Contract> contractList = new List <Contract>();	
		for (Sobject s : scope){
			Contract c = (Contract)s;
			contractList.add(c);

		}  

      
       for(Integer x =0; x<contractList.size(); x++)
        {

            Contract  con = contractList.get(x);

            // Look up an existing accounts with the same Business Number
            //     NOTE:  The logic below assumes this query looks for Accelerate and SmartFly accounts ONLY!!
            //            If you change that then review the DELETE account command below.
            List<Spend__c> spendList = [SELECT Account__c,Contract__c,Id FROM Spend__c WHERE Account__c = :con.AccountId and Eligible_For_Payment_Processing__c =  true and Contract_Type__c = 'Accelerate POS/Rebate' ];
           
         
           
            if(spendList.size() > 0 )
           {
               processPaymentData(con);
           }

        }
    }
    
    
   public void processPaymentData(Contract con)
    {
        
        
        Account acc = new Account();

        acc = (Account) [select id,RecordType.Name,Corporate_ID__c,Corporate_Travel_Bank_Account_Number__c from Account where id =:con.accountid  limit 1  ];
 
        
        Date toDate = date.today();
        Date startDate = con.StartDate ;
        Integer numberOfDays = Date.daysInMonth(toDate.year(), toDate.addMonths(-1).month());
        Payments__c payrec = new Payments__c();
        payrec.Account__c  = acc.Id;
        payrec.Contract__c = con.id ;
        payrec.Start_Date__c   =  Date.newInstance(toDate.addYears(-1).year(),startDate.month() ,1);
        payrec.End_Date__c   =  Date.newInstance(toDate.year(),startDate.addMonths(-1).month() ,numberOfDays);
                  
             
        if (acc.Corporate_ID__c <> null  &&  acc.RecordType.Name == 'Accelerate')
        {
        payrec.Reward_Travel_Bank_User_Name__c =acc.Corporate_ID__c ;
        }else
        {
      /*   List<Payments__c> payList = new List<Payments__c>();
         payList =  [Select id,Reward_Travel_Bank_User_Name__c from Payments__c where Account__c =:con.Accountid and Reward_Travel_Bank_User_Name__c <> null order by createdDate desc  limit 1];
         if (paylist.size() > 0 )
         {
           payrec.Reward_Travel_Bank_User_Name__c = payList[0].Reward_Travel_Bank_User_Name__c ;  
         }else
         {*/
           payrec.Reward_Travel_Bank_User_Name__c  = '' ;  
         }
         
           
        
        if (acc.Corporate_Travel_Bank_Account_Number__c <> null &&  acc.RecordType.Name == 'Accelerate' )
        {
        payrec.Reward_Travel_Bank_Number__c = acc.Corporate_Travel_Bank_Account_Number__c ;
        }else
        {
         /*List<Payments__c> payList = new List<Payments__c>();
         payList =  [Select id,Reward_Travel_Bank_Number__c from Payments__c where Account__c =:con.AccountId and Reward_Travel_Bank_Number__c <> null order by createdDate desc  limit 1];
         if (paylist.size() > 0 )
         {
           payrec.Reward_Travel_Bank_Number__c = payList[0].Reward_Travel_Bank_Number__c ;  
         }else
         {*/
           payrec.Reward_Travel_Bank_Number__c = '' ;  
         
         
        }
       
       
        
        
        payrec.Total_Expenditure__c  =   0;
        payrec.Total_Eligible_Expenditure__c =   0;
        payrec.Domestic_Eligible_Expenditure__c  =0;
        payrec.Intl_Eligible_Expenditure__c  = 0 ;
        payrec.TT_Eligible_Expenditure__c  = 0 ;
        payrec.Payment_Earned_On_Domestic__c = 0;
        payrec.Payment_Earned_On_Intl__c = 0 ;
        payrec.GST__c = 0 ;
        payrec.SmartFLY_Dom_Rebate__c = 0 ;
        payrec.SmartFly_TT_Eligible__c = 0 ;
        payrec.FCTG_Dom_Rebate_Recovery__c = 0 ;
        payrec.FCTG_TT_Rebate_Recovery__c = 0 ;
        payrec.SmartFly_Dom_Eligible__c  =0 ;
        payrec.SmartFly_tt_Eligible__c  =0;
        
        List<aggregateResult> totresults =[SELECT SUM(Eligible_Spend__c) totespend , SUM(Spend__c) totspend , Account__c 
                                             FROM Spend__c
                                             where Account__c = :con.AccountId
                                             and  Contract_Type__c in ( 'Accelerate POS/Rebate')
                                             and  Eligible_For_Payment_Processing__c =  true
                                             group by Account__c
                                          ];   
        
        
      
        
         if (totresults.size() > 0)
       { 
           
         for(AggregateResult ar1 : totresults)
         {
           payrec.Total_Expenditure__c  =   (decimal)ar1.get('totspend')==null?0:(decimal)ar1.get('totspend');
           payrec.Total_Eligible_Expenditure__c =   (decimal)ar1.get('totespend')==null?0:(decimal)ar1.get('totespend');

         }
       }
        
       if (  payrec.Total_Expenditure__c  >= 20000 )            
       {
           payrec.Rebate_Percentage__c = 0 ;
           
           if ( payrec.Total_Expenditure__c  >= 20000  && payrec.Total_Expenditure__c <= 49999.99) 
           {
               payrec.Rebate_Percentage__c = 0.02 ;
           }else if ( payrec.Total_Expenditure__c  >= 50000  && payrec.Total_Expenditure__c <= 99999.99) 
           {
               payrec.Rebate_Percentage__c = 0.03 ;
           }
           else if ( payrec.Total_Expenditure__c  >= 100000  && payrec.Total_Expenditure__c <= 149999.99) 
           {
               payrec.Rebate_Percentage__c = 0.035 ;
           }
           else if ( payrec.Total_Expenditure__c  >= 150000  && payrec.Total_Expenditure__c <= 199999.99) 
           {
               payrec.Rebate_Percentage__c = 0.04 ;
           }else if ( payrec.Total_Expenditure__c  >= 200000  && payrec.Total_Expenditure__c <= 249999.99) 
           {
               payrec.Rebate_Percentage__c = 0.045 ;
           }else if ( payrec.Total_Expenditure__c  >= 250000 ) 
           {
               payrec.Rebate_Percentage__c = 0.05 ;
           }
                  
            List<aggregateResult> domresults =[SELECT SUM(Eligible_Spend__c) domespend , Account__c 
                                             FROM Spend__c
                                             where Account__c = :con.AccountId
                                             and  Contract_Type__c in ( 'Accelerate POS/Rebate')
                                             and  Eligible_For_Payment_Processing__c =  true  
                                             and  Category__c = 'D'
                                             group by Account__c
                                          ]; 
        
            List<aggregateResult> intresults =[SELECT SUM(Eligible_Spend__c) intespend ,  Account__c 
                                             FROM Spend__c
                                             where Account__c = :con.AccountId
                                             and  Contract_Type__c in ( 'Accelerate POS/Rebate')
                                             and  Eligible_For_Payment_Processing__c =  true  
                                             and  Category__c in ( 'L','S')
                                             group by Account__c
                                          ]; 
        
        
            List<aggregateResult> ttresults =[SELECT SUM(Eligible_Spend__c) ttespend ,  Account__c 
                                             FROM Spend__c
                                             where Account__c = :con.AccountId
                                             and  Contract_Type__c in ( 'Accelerate POS/Rebate')
                                             and  Eligible_For_Payment_Processing__c =  true  
                                             and  Category__c in ( 'T')
                                             group by Account__c
                                          ]; 
        
        
           
               
            if (domresults.size() > 0)
           { 
           
              for(AggregateResult ar2 : domresults)
              {
               payrec.Domestic_Eligible_Expenditure__c  =   (decimal)ar2.get('domespend')==null?0:(decimal)ar2.get('domespend');
              }
            }
        
           
             if (intresults.size() > 0)
            { 
           
             for(AggregateResult ar3 : intresults)
            {
               payrec.Intl_Eligible_Expenditure__c  =   (decimal)ar3.get('intespend')==null?0:(decimal)ar3.get('intespend');

             }
           }
        
            if (ttresults.size() > 0)
            { 
           
             for(AggregateResult ar6 : ttresults)
            {
               payrec.TT_Eligible_Expenditure__c  =   (decimal)ar6.get('ttespend')==null?0:(decimal)ar6.get('ttespend');

             }
           }
           
             if (acc.RecordType.Name == 'Accelerate')
        {
         payrec.Payment_Type__c = 'Sales Payment';  
         payrec.GST__c =    (payrec.Domestic_Eligible_Expenditure__c * payrec.Rebate_Percentage__c) /11 ;
         payrec.Payment_Earned_On_Domestic__c =   (payrec.Domestic_Eligible_Expenditure__c * payrec.Rebate_Percentage__c)- payrec.GST__c;
         payrec.Payment_Earned_On_Intl__c = (payrec.Intl_Eligible_Expenditure__c + payrec.TT_Eligible_Expenditure__c) *payrec.Rebate_Percentage__c ; 
         payrec.Payment_Amount__c = payrec.Payment_Earned_On_Domestic__c +  payrec.Payment_Earned_On_Intl__c ;
         if ( ( payrec.Payment_Amount__c + payrec.GST__c ) >  15000)
         {
          payrec.Payment_Amount__c = 15000 - payrec.GST__c  ;   
         }
            
        }else if (acc.RecordType.Name == 'SmartFly')
        {
         payrec.Payment_Type__c = 'SmartFLY Rebate' ;
         
            
        Contract smfcon = new Contract();

         smfcon= (Contract) [select Id,ContractNumber,AccountId,StartDate from Contract where accountid =:con.accountid and Record_Type__c = 'SmartFly' and Status = 'Activated' limit 1]  ;  
            
         List<aggregateResult> smfdomresults =[SELECT SUM(Eligible_Spend__c) domespend , Account__c 
                                             FROM Spend__c
                                             where Account__c = :con.AccountId
                                             and  Contract_Type__c in ( 'Accelerate POS/Rebate')
                                             and  Eligible_For_Payment_Processing__c =  true
                                             and  Spend_Start_Date__c >= :smfcon.StartDate   
                                             and  Category__c = 'D'
                                             group by Account__c
                                          ]; 
            
          List<aggregateResult> smfttresults =[SELECT SUM(Eligible_Spend__c) ttespend , Account__c 
                                             FROM Spend__c
                                             where Account__c = :con.AccountId
                                             and  Contract_Type__c in ( 'Accelerate POS/Rebate')
                                             and  Eligible_For_Payment_Processing__c =  true
                                             and  Spend_Start_Date__c >= :smfcon.StartDate   
                                             and  Category__c = 'T'
                                             group by Account__c
                                          ]; 
            
            
           if (smfdomresults.size() > 0)
           { 
           
              for(AggregateResult ar4 : smfdomresults)
              {
               payrec.SmartFly_Dom_Eligible__c  =   (decimal)ar4.get('domespend')==null?0:(decimal)ar4.get('domespend');
              }
            }   
            
          if (smfttresults.size() > 0)
           { 
           
              for(AggregateResult ar5 : smfttresults)
              {
               payrec.SmartFly_tt_Eligible__c  =   (decimal)ar5.get('ttespend')==null?0:(decimal)ar5.get('ttespend');
              }
            } 
            
         payrec.GST__c =    (payrec.Domestic_Eligible_Expenditure__c * (payrec.Rebate_Percentage__c + 0.02 )) /11 ;
         payrec.Payment_Earned_On_Domestic__c =   (payrec.Domestic_Eligible_Expenditure__c * (payrec.Rebate_Percentage__c + 0.02 ))- payrec.GST__c;
         payrec.Payment_Earned_On_Intl__c = (payrec.Intl_Eligible_Expenditure__c * payrec.Rebate_Percentage__c)  + (payrec.TT_Eligible_Expenditure__c * (payrec.Rebate_Percentage__c + 0.02 )) ; 
         payrec.Payment_Amount__c = payrec.Payment_Earned_On_Domestic__c +  payrec.Payment_Earned_On_Intl__c ; 
         payrec.SmartFLY_Dom_Rebate__c =  payrec.SmartFly_Dom_Eligible__c * (payrec.Rebate_Percentage__c + 0.02 );
         payrec.SmartFLY_TT_Rebate__c =    payrec.SmartFly_tt_Eligible__c  * (payrec.Rebate_Percentage__c + 0.02 );
         payrec.FCTG_Dom_Rebate_Recovery__c = (payrec.SmartFly_Dom_Eligible__c *.02)/2 ;
         payrec.FCTG_TT_Rebate_Recovery__c  = (payrec.SmartFly_TT_Eligible__c *.02)/2 ;
        }
           
           payrec.Upsert_ID__c =    acc.Id + '_' + con.id + '_' + payrec.Payment_Type__c + '_' + payrec.Start_Date__c + '_' +payrec.End_Date__c ;
           
          try{
               insert payrec ;
             }catch(Exception e){
            //VirginsutilitiesClass.sendEmailError(e);
            }

           
       }           
           
           
           
           
    }
          

     
       

  

    //Finishing the batch  (Or though this is not executiong code this methos needs to exsist for the batch class to run!!)  
    global void finish(Database.BatchableContext BC){
        
        

    }
 
    

}