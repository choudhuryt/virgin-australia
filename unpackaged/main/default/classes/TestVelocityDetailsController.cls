@isTest
public class TestVelocityDetailsController{

    static testMethod void testConstructor() {
    	Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());	
        VelocityDetailsController testObject = new VelocityDetailsController();
    }
    
    static testMethod void testPrepareSeatingPreferences(){
        Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());	
        VelocityDetailsController testObject = new VelocityDetailsController();
        GuestCaseVelocityInfoModel.PreferencesType preference = new GuestCaseVelocityInfoModel.PreferencesType();
        GuestCaseVelocityInfoModel.SeatingPreferenceType[] seatingPrefs = new List<GuestCaseVelocityInfoModel.SeatingPreferenceType>();
        GuestCaseVelocityInfoModel.SeatingPreferenceType result = testObject.prepareSeatingPreferences(seatingPrefs);
        System.assertEquals(null, result.SeatArea);
        
        GuestCaseVelocityInfoModel.SeatingPreferenceType seatingPref = new GuestCaseVelocityInfoModel.SeatingPreferenceType();
        seatingPref.SeatArea = 'A16';
        seatingPref.SeatingPreferenceOrder = 'Order';
        seatingPrefs.add(seatingPref);
        result = testObject.prepareSeatingPreferences(seatingPrefs);
        System.assertEquals('A16', result.SeatArea);
    }
    
    static testMethod void testSaveVelocity() {
        Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());	
        VelocityDetailsController testObject = new VelocityDetailsController();
        testObject.SaveVelocity();
    }
    
    static testMethod void testPrepareContactEmail() {
        Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());	
        VelocityDetailsController testObject = new VelocityDetailsController();
        List<String> emails = new List<String>();
        System.assertEquals('', testObject.prepareContactEmail(emails));
        String email1 = 'abcd@smsmt.com';
        emails.add(email1);
        System.assertEquals(email1, testObject.prepareContactEmail(emails));
    }
}