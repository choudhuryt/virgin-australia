@isTest
private class TestSipHeaderTriggerHandler
{

     static testMethod void myUnitTest()
    {
        // TO DO: implement unit test
        
        
        //setup account
        CommonObjectsForTest coft = new CommonObjectsForTest();
        Account account = new Account();
        account = coft.CreateAccountObject(0);
        insert account;
        
        
        Contact contact = new Contact();
        contact.AccountId = account.id;
        contact.FirstName= 'Test';
        contact.LastName = 'Test';
        contact.Title ='Test';
        insert contact;
        
        Contract contract = new Contract();
        contract.AccountId = account.Id;
        contract.Name ='Test Industry Contract';
        contract.Status = 'Draft';
        contract.Type__c = 'Retail';
        contract.Industry_Cos__c = 'A';
        contract.Short_Haul_LY__c = 10000;
        contract.CustomerSignedId = contact.id;
        contract.Customer_Signed_By_1__c =contact.id;
        contract.RecordTypeId = '01290000000u8Kc';
        contract.Fixed_Annual_SLA_Amount__c =100;
        contract.Dom_SH_SLA__c = 2;
        contract.Ticket_Fund__c =10;
        contract.Fixed_Amount__c =1000;        
        insert contract;
        
        contract.Override_Commisions__c =true;
        update contract;
        SIP_Header__c sipheader = new SIP_Header__c();
        sipheader.Contract__c = contract.id;
        sipheader.Full_Last_Year_Actual_Revenue__c = 10000;
        sipheader.Est_Current_Full_Year_Revenue__c	= 10000 ;
        sipheader.SIP_Percentage__c = 2;
        insert sipheader;
        
 		SIP_Rebate__c sipRebate = new SIP_Rebate__c(); 		
 		sipRebate.SIP_Header__c = sipheader.id;
 		sipRebate.Upper_Percent__c =2.0;
 		sipRebate.Lower_Percent__c =1.0;
 		sipRebate.Upper_Percent__c=10;
        sipRebate.SIP_Percent__c = 3;
  	    insert  sipRebate;
        
         sipheader.Est_Current_Full_Year_Revenue__c	= 20000 ;
         update sipheader;
    }
}