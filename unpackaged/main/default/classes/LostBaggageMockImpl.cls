@isTest
global class LostBaggageMockImpl implements WebServiceMock{
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService services = new SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService();
       SalesForceHistoricalDetailModel.GetLostBaggageRSType response_x = services.GetLostBaggageDemo();
       
       response.put('response_x', response_x); 
   }
}