/*------------------------------------------------------------------------
Author:     Serge Lauriou
Company:    Tquila ANZ
Description:   Inbound Email Handler for processing Fare Promise Web form to email service. 

History
<Date>     <Authors Name>     <Brief Description of Change>
23-Apr-2019  Serge Lauriou     Created
12-Jun-2019  Serge Lauriou	   Updated and tested from Test Website
----------------------------------------------------------------------------*/
global class GCCInboundEmailFarePromise implements Messaging.InboundEmailHandler{
    
    global Messaging.InboundEmailResult handleInboundEmail(
        	Messaging.InboundEmail email,
        	Messaging.InboundEnvelope envelope){
            
            Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
                
            String stripedHtml = email.htmlBody.stripHtmlTags();
            system.debug('stripedHtml **** ' + stripedHtml);
            system.debug('email.subject **** ' + email.subject);
            system.debug('email.htmlBody **** ' + email.htmlBody);
                //string subject = email.plainTextBody
            
            // find queue and record type
            Group[] FPqueues = [select Id, Name from Group where Name = 'Fare Promise'];
            String farePromiseQueueId = FPqueues[0].Id;
            string FPRecordTypeId = [SELECT id from RecordType where Name ='GCC Fare Promise'].Id;
            
            // get values from the html web form
            String bodyFirstName = extractBodyValue(stripedHtml, 'First Name:', 'Last Name:');
            system.debug('bodyFirstName **** ' + bodyFirstName);
            String bodyLastName = extractBodyValue(stripedHtml, 'Last Name:', 'Email Address:');
            String bodyEmail = extractBodyValue(stripedHtml, 'Email Address:', 'Phone No:');
            //bodyEmail = bodyEmail.left(bodyEmail.indexOf('<'));
            
            String bodyPhone = extractBodyValue(stripedHtml, 'Phone No:', 'Velocity Frequent Flyer No');
            String bodyVelocity = extractBodyValue(stripedHtml, 'Velocity Frequent Flyer No:', 'Your Virgin Australia Itinerary');
            String bodyBookingReference = extractBodyValue(stripedHtml, 'Virgin Australia Booking Reference:', 'Date booking was made:');
            String bodyDateBooking = extractBodyValue(stripedHtml, 'Date booking was made:', 'Was your booking made via the Virgin Australia Website:');
            String bodyBookingMadeVA = extractBodyValue(stripedHtml, 'Was your booking made via the Virgin Australia Website:', 'Is your booking solely a domestic Australian Itinerary');
            String bodyBookingDomestic = extractBodyValue(stripedHtml, 'Is your booking solely a domestic Australian Itinerary', 'Eligible Lower Fare Details');
            String bodyWebsiteAddress = extractBodyValue(stripedHtml, 'Website Address:', 'Date the fare was viewed:');
            String bodyFareDate = extractBodyValue(stripedHtml, 'Date the fare was viewed:', 'Time the fare was viewed:');
            String bodyFareTime = extractBodyValue(stripedHtml, 'Time the fare was viewed:', 'Total booking amount of all passengers');
            String bodyBookingAmount = extractBodyValue(stripedHtml, 'Total booking amount of all passengers \\(inclusive of mandatory fees,taxes and surcharges excluding credit card fees and commission\\):', 'The content of this');
            
            // search for contact for parentid
            String contactId;
                if(bodyEmail != null &&  bodyEmail.contains('@')){
            		List<contact> contactList = new List<Contact>([Select Id, email from Contact where email =: bodyEmail]);
                		if(contactList.size()>0 ){
                			contactId = contactList[0].Id;
                	}
                }
                
            try{
                
                Case newCase = new Case();
                newCase.Description =  'Fare Promise';
                newCase.HTML_Email__c =  email.htmlBody;
                newCase.Subject = email.subject;
                newCase.Origin = 'Web';                
                newCase.status  = 'New';
                newCase.Ownerid = farePromiseQueueId;
                newCase.RecordTypeId = FPRecordTypeId;
                if(contactID <> ''){
                    newCase.ContactId = contactID;
                }
                
                newCase.First_Name__c = bodyFirstName;
                newCase.Last_Name__c = bodyLastName;
                newCase.Contact_Email__c = bodyEmail;
                newCase.Contact_Phone__c = bodyPhone;
                newCase.Velocity_Number__c = bodyVelocity.trim();
                newCase.Virgin_Australia_Booking_Reference__c = bodyBookingReference;
                newCase.Time_the_fare_was_viewed_AEST__c = bodyFareDate + ' - ' + bodyFareTime;
                newCase.Was_your_booking_made_via_the_Virgin_Aus__c = bodyBookingMadeVA.trim();
                newCase.Is_your_booking_solely_a_domestic_Austra__c = bodyBookingDomestic.trim();
                newCase.Total_booking_amount_for_all_passengers__c = bodyBookingAmount;
                newCase.Website_Address__c = bodyWebsiteAddress;
                newCase.SuppliedEmail = bodyEmail;
                
                //perform case record insert operation.
                insert newCase;
                System.debug('Successfully inserted case id: ' + newCase.Id);
                
                // insert email message to the case
                EmailMessage emailMsgObj = new EmailMessage();
                emailMsgObj.HtmlBody =  email.htmlBody;
                emailMsgObj.TextBody = email.plainTextBody;
                emailMsgObj.FromAddress = email.fromAddress;                
                emailMsgObj.FromName = email.fromName;
                emailMsgObj.Subject = email.subject;
                emailMsgObj.Incoming = true;
                emailMsgObj.ParentId = newCase.Id;
                
                insert emailMsgObj;
                
                
                // attachment
                List<Attachment> lstAttachment = new List<Attachment>();
                
                if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
                    for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
                        Attachment attachment = new Attachment();
                        attachment.ParentId = newCase.Id;
                        attachment.Name = email.binaryAttachments[i].filename;
                        attachment.Body = email.binaryAttachments[i].body;
                        lstAttachment.add(attachment);
                    }
                    
                    insert lstAttachment;
                } 
                
            }
            catch (Exception e) {
                //handle it properly.
                result.success = false;
                return result;
            }
            
            result.success = true;
            return result;       
        }
    
    // regex to find value from the from after the fieldName:
    private String extractBodyValue(String emailBody, String firstString, String lastString){
        
        //example String PatternString string between 2 strings = First Name:(.*)Last Name:
        
        String PatternString = firstString + '(.*)' + lastString;
        if(firstString == 'Phone No:'){
        	//PatternString = firstString + '\\s*([^\\n\\r]*)';
        	PatternString ='(\\w+)\\s+Velocity Frequent Flyer No';
        }

        Pattern p = Pattern.compile(PatternString);
        Matcher pm = p.matcher(emailBody);
        String resp;
        if (pm.find()){
            resp = pm.group(1);
            System.debug('**RESP***' +resp);
        } else {
            System.debug('No match');
        }
        return resp;
    }
}