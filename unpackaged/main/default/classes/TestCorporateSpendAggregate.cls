/*
 *  Updated By Cloudwerx : Removed hardcoded references for RecordTypeId
 * 
 */ 
@isTest
public class TestCorporateSpendAggregate {
    
    static testMethod void myUnitTestContract1() 
    {
        Id accountRecTypeId = Utilities.getRecordTypeId('Account', 'Corporate');
        
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';
        account.Account_Lifecycle__c = 'Contract';
        account.RecordTypeId = accountRecTypeId;	
        insert account;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.StageName = 'Sales Opportunity Analysis'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1; 
        
        Test.StartTest();
        
        Contract con = new Contract();
        con = commx.CreateOldStandardContract(0);
        con.AccountId = account.id;
        con.Opportunity__c = opp1.id ;
        con.SQ_Discount_Type__c = 'Specific OD Pairs';        
        insert con;
        
        con.Status = 'Activated';
        
        update con ;
        
        Market__c market1 = new Market__c();
        market1.Opportunity__c = opp1.id ;
        market1.Contract__c = con.id ;
        market1.Name = 'DOM Mainline';
        market1.VA_Revenue_Commitment__c = 100;
        market1.Total_Spend__c = 150;
        insert market1;
        
        Market__c market2 = new Market__c();
        market2.Opportunity__c = opp1.id ;
        market2.Contract__c = con.id ;
        market2.Name = 'Trans Tasman TT';
        market2.VA_Revenue_Commitment__c = 100; 
        market2.Total_Spend__c = 150;
        insert market2; 
                
        CorporateSpendAggregate testBatch = new CorporateSpendAggregate();
        Database.executeBatch(testBatch, 1); 
        
        Test.StopTest();
    }
    
}