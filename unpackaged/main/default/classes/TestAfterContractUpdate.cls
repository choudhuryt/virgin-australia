@isTest
private class TestAfterContractUpdate
{

    static testMethod void myUnitTestOpp() 
    {
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';
        insert account;
        
        Test.startTest();
        Contract ct = TestUtilityClass.createTestContract(account.Id);
        insert ct;
        
        Contract_Fulfilment__c cf = TestUtilityClass.createTestContractFulfilment(account.Id, ct.Id);
        insert cf;
        
        Id oppRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.Type = 'Revision';
        opp1.StageName = 'Awaiting BDM Allocation'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        //opp1.RecordTypeId = '01290000000so61' ;
        opp1.RecordTypeId = oppRecTypeId;        
        opp1.Existing_SF_Contract__c = ct.id ; 
        insert opp1;
        opp1.StageName = 'Closed Won'  ;    
        update opp1;       
        
        Market__c market1 = new Market__c();  
        market1.Opportunity__c = opp1.id ;
        market1.Name = 'DOM Mainline';
        market1.VA_Revenue_Commitment__c = 100; 
        insert market1;
        
        
        Additional_Benefits__c addben1 = new  Additional_Benefits__c();
        addben1.Opportunity__c = opp1.id ;
        addben1.Name = opp1.Name ;
        insert addben1;
        
        Proposal_Table__c  prop1 = new  Proposal_Table__c();
        prop1.Opportunity__c = opp1.id ;
        prop1.Name = 'DOM Mainline'; 
        insert prop1;
        
        Red_Circle_Discounts__c  red1 = new  Red_Circle_Discounts__c();
        red1.Opportunity__c = opp1.id ;
        red1.Name = 'DOM Mainline'; 
        insert red1;  
        
        Contract ct1 = TestUtilityClass.createTestContract(account.Id);
        ct1.Contract_Type__c= 'Revision';
        ct1.Opportunity__c = opp1.id ;
        ct1.Parent_Contract__c =ct.id ;
        insert ct1;               
        
        market1.Contract__c  = ct1.id ;
        
        update market1 ;
        
        Test.stopTest();
        
    }      
    
}