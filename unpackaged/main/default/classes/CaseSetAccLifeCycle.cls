public class CaseSetAccLifeCycle {
    
    // Updates Account Life Cycle to "Follow-UP" if it is in "Opportunity" and   
    // the Case subject line contains "FOLLOWUP#" and record type is Accelerate'
    
    public static void CaseSetAccLifeCycle1(list<Case> cse)
    {
        system.debug('CaseSetAccLifeCycle Trigger started');
        list<Account> accList = new List<Account>();
        Set<ID> deleteCaseSet = new Set<ID>();
        list<Case> caseList = new List<Case>();
        list<CaseComment> caseCommList = new list<CaseComment>();
        
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosById();
        
        for(case cs : cse)
        {   
            String subjAccID = ' ';
            boolean subjFollUp = False;
            boolean subjRe = False;
            boolean subjInfoReq = False;
            
            //****************************************************************************************************************************
            //  Process to handle FollowUp email from new Account team to Accelrate team to be sent to customer.                         *
            //****************************************************************************************************************************              
            if(cs.Subject != null)
                subjFollUp = cs.Subject.contains('FOLLOWUP#');
            
            String Rectype = rtMap.get(cs.RecordTypeId).getName();
            
            if(subjFollUp && Rectype == 'Accelerate')
            {
                subjAccID = cs.Subject.substringBetween('[', ']');    
                system.debug('in accel');
                
                // get key contact id from account
                string keyContactID;
                for(Contact c: [select id, Name from contact where accountid=:subjAccID and Key_Contact__c = true]){
                    keyContactID = c.id;
                }
                
                for(Account acc : [select name from account where Account_Lifecycle__c = 'Opportunity' and ID = :subjAccID])
                {
                    Account accObj = new account();
                    accObj.ID = acc.ID;
                    accObj.Account_Lifecycle__c = 'Follow-up';
                    AccList.add(accObj);
                    
                    case caseObj = new case();
                    caseObj.AccountId  = subjAccID;
                    caseObj.ID  = cs.ID;
                    caseObj.ACC_Primary_Category__c='Accounts/NAR';
                    caseObj.ACC_Secondary_Category__c ='Chasing Further Info';
                    caseObj.Accelerate_Case_Category__c='Accelerate';
                    caseObj.contactid = keyContactID;
                    caseObj.origin='E2C';
                    caseList.add(caseObj);                
                    
                    system.debug('after case details');
                    
                }
            }else {
                //****************************************************************************************************************************
                //  MOVED TO WORKFLOW RULES --- Process to handle Response email from Customer to Accelrate team to be forwared to NewAccounts Team.                *
                //****************************************************************************************************************************              
                
            }
        }
        
        if(accList.size() > 0){   
            system.debug('trying to update Account to follow-up');
            update accList;
        }
        
        if(caseList.size() > 0){   
            update caseList;
            system.debug('Trying to update  Case to follow-up');
        }
        
        if(caseCommList.size() > 0) 
        {    system.debug('in caseCommList ' + caseCommList);
         Database.SaveResult[] srList1 = Database.Insert(caseCommList, false);
        }
        
    }
}