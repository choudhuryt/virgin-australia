/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 28.10.2021
**Description: Batch Class to delete Attachments for Email Messages and Contact Documents for Cases closed One year ago.
**             For running test classes on the batch,the cases closed for Today is considered.
** UpdatedBy : cloudwerx
*/
global class CaseAttachmentDeletionBatch implements Database.Batchable<sObject> {
    //UpdatedBy: cloudwerx While running the test class, the value of the custom label should be 0
    @testvisible Integer labelDate = Integer.valueOf(System.Label.Case_Attachment_Expiry_Date); 
    List<Id> caseIdList = new List<Id>();
    List<Id> msgIdList = new List<Id>();
    List<Id> contentDocList = new List<Id>();
    String rtName = 'Velocity Member Support';
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query;
        Date expDate = System.today().addDays(-labelDate); System.debug('expDate--'+expDate);
        if(labelDate == 0){
            query = 'Select Id, ClosedDate from Case where ClosedDate >=:expDate AND RecordType.Name =:rtName';
        } else {            
            query = (Test.isRunningTest()) ? 'Select Id, ClosedDate from Case where RecordType.Name =:rtName' : 'Select Id, ClosedDate from Case where ClosedDate <=:expDate AND RecordType.Name =:rtName';
        }
        //String query = 'Select Id, ClosedDate from Case where ClosedDate <=:expDate AND RecordType.Name =:rtName';
        return Database.getQueryLocator(query);
    }  
    
    global void execute (Database.BatchableContext BC, List<Case> caseList){
        
        for(Case cs: caseList){
            caseIdList.add(cs.Id);
        } 

        List<ContentDocumentLink> cList = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN: caseIdList];
        for(ContentDocumentLink cd:cList){
            contentDocList.add(cd.ContentDocumentId);
        }

        List<ContentDocument> conDocList= [SELECT Id FROM ContentDocument WHERE Id IN: contentDocList];
        
        List<EmailMessage> emailList = [SELECT Id FROM EmailMessage WHERE RelatedToId IN:caseIdList];
        for(EmailMessage em: emailList){
            msgIdList.add(em.Id);
        }

        List<Attachment> attList = [SELECT Id FROM Attachment WHERE ParentId IN:msgIdList];
        if(conDocList.Size()>0){
            DELETE conDocList;
        }
        if(attList.Size()>0){
            DELETE attList;
        }
    }

    global void finish (Database.BatchableContext BC){
        
    }
}