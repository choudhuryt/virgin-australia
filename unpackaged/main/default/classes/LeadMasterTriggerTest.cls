@isTest
public class LeadMasterTriggerTest {
    static testMethod void gccWebChatTest(){
        User adminUser = [SELECT Id FROM User WHERE Alias = 'SADMIN' LIMIT 1];
        Lead ld = new Lead(Company = 'GCC Webchat',
                           LastName = 'Lead Last Name');
        test.startTest();
        insert ld;
        test.stopTest();
        Lead ld2 = [SELECT OwnerId FROM Lead WHERE Company = 'GCC Webchat' LIMIT 1];
        System.assertEquals(adminUser.Id, ld2.OwnerId);
    }
}