global class AccLastResDateBatch implements Database.Batchable<sObject> {
	global Database.QueryLocator start(Database.BatchableContext BC){
        Date expDate = System.today().addDays(-15);
        Date tExpDate = System.today().addDays(0);
        String rtName = 'Accelerate';
        String query;
        query = 'Select Id,CreatedDate,Last_Response_Date__c,Status from Case where RecordType.Name =:rtName AND Status != \'Closed\' AND Last_Response_Date__c =NULL';
      
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Case> caseList){
        System.debug('caseList--'+caseList);
        List<Case> upCaseList = new List<Case>();
        List<Id> csIdList = new List<Id>();
        for(Case c: caseList){
            csIdList.add(c.Id);
        }

        Map<Id, DateTime> createdDtMap = new Map<Id, DateTime>();
        System.debug('csIdList--'+csIdList);
        List<EmailMessage> mailList = [Select Id,RelatedToId,CreatedDate from EmailMessage where RelatedToId IN:csIdList AND Incoming =:true];
        System.debug('mailList--'+mailList);
        
        for(EmailMessage mail: mailList){            
            if(createdDtMap.containskey(mail.RelatedToId)){
                if(mail.CreatedDate > createdDtMap.get(mail.RelatedToId)){
                    createdDtMap.put(mail.RelatedToId, mail.CreatedDate);
                }
            }else{
                createdDtMap.put(mail.RelatedToId, mail.CreatedDate);
            }
        }		
        System.debug('createdDtMap--'+createdDtMap);
        
        for(Id cId: createdDtMap.keyset()){
            Case cas = new Case();
            cas.Id = cId;
            cas.Last_Response_Date__c = createdDtMap.get(cId);
            upCaseList.add(cas);
        }
        
        for(Case cs: caseList){
            System.debug('Case--'+cs.Id);
            if(!createdDtMap.containskey(cs.Id)){
                Case cas = new Case();
            	cas.Id = cs.Id;
            	cas.Last_Response_Date__c = cs.CreatedDate;
            	upCaseList.add(cas);
            }
        }
        
        if(upCaseList.size()>0){
            update upCaseList;
        }
    }
    global void finish(Database.BatchableContext BC){
        
    }
}