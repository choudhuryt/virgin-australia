@isTest
private class  TestAccelerateCaseAssignment
{
    static testMethod void myUnitTest1()
    {
    
    Test.StartTest();   
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrP';
    account.Sales_Matrix_Owner__c = 'Accelerate';
    account.OwnerId = '00590000000LbNz'; 
    account.Business_Number__c = '11111111111';
    insert account;
     
    Case c = new Case() ;
    c.SuppliedEmail='jdoe_test_test@doe.com';
    c.SuppliedName='John Doe';
    c.Velocity_Member__c = false;
    c.Subject='Pilot Gold Request | ABN: 11111111111';
    c.Contact_First_Name__c='Tom';
    c.Contact_Last_name__c = 'Johns';
    insert c;
        
    List<Id> caseid = new List<Id>();        
    caseid.add(c.Id); 
        
    AccelerateCaseAssignment.AccelerateCaseLink(caseid)  ;  
        
    Test.stopTest(); 
        
    }

    
    static testMethod void myUnitTest2()
    {
    
    Test.StartTest();   
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrP';
    account.Sales_Matrix_Owner__c = 'Accelerate';
    account.OwnerId = '00590000000LbNz'; 
    account.Business_Number__c = '11111111111';
    insert account;
     
    Case c = new Case() ;
    c.SuppliedEmail='no-reply@inteflow.com.au';
    c.SuppliedName='John Doe';
    c.Velocity_Member__c = false;
    c.Subject='ACCELERATE Application Approved';
    c.Contact_First_Name__c='Tom';
    c.Contact_Last_name__c = 'Johns';
    c.Description = 'Application ID: ' + '    0016F00001gSbuPQAS' ;
    system.debug('the description' + c.Description ) ;   
    insert c;
        
    List<Id> caseid = new List<Id>();        
    caseid.add(c.Id); 
        
    AccelerateCaseAssignment.AccelerateCaseLink(caseid)  ;  
        
    Test.stopTest(); 
        
    }
    
    static testMethod void myUnitTest3()
    {
    
    Test.StartTest();   
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrP';
    account.Sales_Matrix_Owner__c = 'Accelerate';
    account.OwnerId = '00590000000LbNz';
    account.Booking_Type__c = 'Both';
    account.Business_Number__c = '11111111111';
    insert account;
     
    Case c = new Case() ;
    c.SuppliedEmail='no-reply@inteflow.com.au';
    c.SuppliedName='John Doe';
    c.Velocity_Member__c = false;
    c.Subject='ACCELERATE Application Decline';
    c.Contact_First_Name__c='Tom';
    c.Contact_Last_name__c = 'Johns';
    c.Description = 'Application ID: ' + '  0016F00001gSbuPQAS' ;
    system.debug('the description' + c.Description ) ;   
    insert c;
        
    List<Id> caseid = new List<Id>();        
    caseid.add(c.Id); 
        
    AccelerateCaseAssignment.AccelerateCaseLink(caseid)  ;  
        
    Test.stopTest(); 
        
    }
    
     static testMethod void myUnitTest4()
    {
    
    Test.StartTest();   
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrP';
    account.Sales_Matrix_Owner__c = 'Accelerate';
    account.OwnerId = '00590000000LbNz';
    account.Booking_Type__c = 'Both';
    account.Business_Number__c = '11111111111';
    insert account;
     
    Case c = new Case() ;
    c.SuppliedEmail='jdoe_test_test@doe.com';
    c.SuppliedName='John Doe';
    c.Velocity_Member__c = false;
    c.Subject='ACCOUNT MANAGER ALERT – Invalid email address';
    c.Contact_First_Name__c='Tom';
    c.Contact_Last_name__c = 'Johns';
    c.Description = 'Account ID : ' + '0016F00001gSbuP' ;
    system.debug('the description' + c.Description ) ;   
    insert c;
        
    List<Id> caseid = new List<Id>();        
    caseid.add(c.Id); 
        
    AccelerateCaseAssignment.AccelerateCaseLink(caseid)  ;  
        
    Test.stopTest(); 
        
    }
    
    static testMethod void myUnitTest5()
    {
    
    Test.StartTest();   
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrP';
    account.Sales_Matrix_Owner__c = 'Accelerate';
    account.OwnerId = '00590000000LbNz';
    account.Booking_Type__c = 'Both';
    account.Business_Number__c = '11111111111';
    insert account;
     
    Case c = new Case() ;
    Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Sales Support').getRecordTypeId();    
    c.RecordTypeId = recordTypeId ;    
    c.Origin   = 'E2C';
    c.SuppliedEmail='salesforce.admin@virginaustralia.com';
    c.SuppliedName='John Doe';
    c.Velocity_Member__c = false;
    c.Subject='Please Process the following FOC Ticket Request';
    c.Contact_First_Name__c='Tom';
    c.Contact_Last_name__c = 'Johns';
    c.Description = 'Account ID: 0016F0000344152' + 'Due Date : 31/10/2019' ;
    system.debug('the description' + c.Description ) ;   
    insert c;
        
    List<Id> caseid = new List<Id>();        
    caseid.add(c.Id); 
        
    SalesSupportCaseAssignment.SalesSupportCaseLink(caseid)  ;  
        
    Test.stopTest(); 
        
    }
    
    static testMethod void myUnitTest6()
    {
    
    Test.StartTest();   
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrP';
    account.Sales_Matrix_Owner__c = 'Accelerate';
    account.OwnerId = '00590000000LbNz';
    account.Booking_Type__c = 'Both';
    account.Business_Number__c = '11111111111';
    insert account;
     
    Case c = new Case() ;
    Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Sales Support').getRecordTypeId();    
    c.RecordTypeId = recordTypeId ;    
    c.Origin   = 'E2C';
    c.SuppliedEmail='salesforce.admin@virginaustralia.com';
    c.SuppliedName='John Doe';
    c.Velocity_Member__c = false;
    c.Subject='Waiver & Favour Request for Test Approved';
    c.Contact_First_Name__c='Tom';
    c.Contact_Last_name__c = 'Johns';
    c.Description = 'Account ID: 0016F0000344152' + 'Due Date : 31/10/2019' ;
    system.debug('the description' + c.Description ) ;   
    insert c;
        
    List<Id> caseid = new List<Id>();        
    caseid.add(c.Id); 
        
    SalesSupportCaseAssignment.SalesSupportCaseLink(caseid)  ;  
        
    Test.stopTest(); 
        
    }
    
      static testMethod void myUnitTest7()
    {
    
    Test.StartTest();   
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrP';
    account.Sales_Matrix_Owner__c = 'Accelerate';
    account.OwnerId = '00590000000LbNz';
    account.Booking_Type__c = 'Both';
    account.Business_Number__c = '11111111111';
    insert account;
     
    Case c = new Case() ;
    Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Sales Support').getRecordTypeId();    
    c.RecordTypeId = recordTypeId ;    
    c.Origin   = 'E2C';
    c.SuppliedEmail='salesforce.admin@virginaustralia.com';
    c.SuppliedName='John Doe';
    c.Velocity_Member__c = false;
    c.Subject='Velocity Status Match/Upgrade Request Approved';
    c.Contact_First_Name__c='Tom';
    c.Contact_Last_name__c = 'Johns';
    c.Description = 'Account ID: 0016F0000344152' + 'Due Date : 31/10/2019' + 'VSM Record: a051e000000MOC0/ V-163788' ;
    system.debug('the description' + c.Description ) ;   
    insert c;
        
    List<Id> caseid = new List<Id>();        
    caseid.add(c.Id); 
        
    SalesSupportCaseAssignment.SalesSupportCaseLink(caseid)  ;  
        
    Test.stopTest(); 
        
    }
    
      static testMethod void myUnitTest8()
    {
    
    Test.StartTest();   
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrP';
    account.Sales_Matrix_Owner__c = 'Accelerate';
    account.OwnerId = '00590000000LbNz';
    account.Booking_Type__c = 'Both';
    account.Business_Number__c = '11111111111';
    insert account;
     
    Case c = new Case() ;
    Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Sales Support').getRecordTypeId();    
    c.RecordTypeId = recordTypeId ;    
    c.Origin   = 'E2C';
    c.SuppliedEmail='salesforce.admin@virginaustralia.com';
    c.SuppliedName='John Doe';
    c.Velocity_Member__c = false;
    c.Subject='Please process the following Agent Rate Request';
    c.Contact_First_Name__c='Tom';
    c.Contact_Last_name__c = 'Johns';
    c.Description = 'Account ID: 0016F0000344152' + 'Due Date : 31/10/2019' ;
    system.debug('the description' + c.Description ) ;   
    insert c;
        
    List<Id> caseid = new List<Id>();        
    caseid.add(c.Id); 
        
    SalesSupportCaseAssignment.SalesSupportCaseLink(caseid)  ;  
        
    Test.stopTest(); 
        
    }
}