/**
* @description       : Test class for FCRCAUtilities
* @CreatedBy         : Cloudwerx
* @Updated By         : Cloudwerx
**/
@isTest
private class TestFCRCAUtilities {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        
        Contract_Addendum__c testContractAddendumObj = TestDataFactory.createTestContractAddendum(testContractObj.Id, '16.0', 'Tier 1', 'Tier 1', true, 'SYDMEL', 'PERKTH', 'Draft');
        INSERT testContractAddendumObj;
        
        Proposed_Discount_Tables__c testProposedDiscountTablesObj = TestDataFactory.createTestProposedDiscountTables('DOM Mainline', testContractAddendumObj.Id, 10, 'Business', 'J');
        INSERT testProposedDiscountTablesObj;
        User vaSystemAdminUser = TestUtilityClass.createTestUser();
        INSERT vaSystemAdminUser;
        system.runAs(vaSystemAdminUser) {
            FCR_Proposal_Tier__c testFCRProposalTierObj = TestDataFactory.createTestFCRProposalTier('DOM Mainline', 10, 'Booking CLass', 'fare Type', 1, 'Tier 1', 'Tier 1', 'VA Booking Class');
            INSERT testFCRProposalTierObj;
        }
    }
    
    @isTest
    private static void testFCRCAUtilities() {
        Id contractAddendumId = [SELECT Id FROM Contract_Addendum__c LIMIT 1]?.Id;
        Test.startTest();
        FCRCAUtilities.removeDOMRegional(new Set<Id>{contractAddendumId});
        Test.stopTest();
        List<Proposed_Discount_Tables__c> populatedProposedDiscountTables = [SELECT Id, FCR_Contract_Amendment_Back_Up__c, FCR_Processed__c, 
                                                                             FCR_For_Deletion__c, FCR_Remarks__c 
                                                                             FROM Proposed_Discount_Tables__c];
        System.assertEquals(2, populatedProposedDiscountTables.size());
        System.assertEquals(contractAddendumId, populatedProposedDiscountTables[0].FCR_Contract_Amendment_Back_Up__c);
        System.assertEquals(true, populatedProposedDiscountTables[0].FCR_Processed__c);
        System.assertEquals(true, populatedProposedDiscountTables[0].FCR_For_Deletion__c);
        System.assertEquals('Old DOM Record marked for Deletion', populatedProposedDiscountTables[0].FCR_Remarks__c);
        System.assertEquals('DOM Record created as per new Tier Table', populatedProposedDiscountTables[1].FCR_Remarks__c);
    }
}