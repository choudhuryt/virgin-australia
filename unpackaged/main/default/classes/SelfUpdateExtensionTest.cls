@isTest
private class SelfUpdateExtensionTest {

/*	@isTest static void clickYesTest() {
        //Test.startTest();
		String RECORDTYPE_ACCELERATE = 'Accelerate';

		Ignore_Email_Domains__c ignoreDomain = new Ignore_Email_Domains__c();
		ignoreDomain.Name = 'yahoo.com';

		insert ignoreDomain;
        // These accounts are for testing of duplicates
        Id accelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_ACCELERATE ).getRecordTypeId();

        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<3;i++) {
            testAccts.add(new Account(Name = 'TestDuplicateAcct',
            						  Business_Number__c = '11122233345',
            						  Administrator_Contact_Email__c = 'TestAdmin@testsalesforce.com',
            						  RecordTypeId = accelerateRecTypeId,
            //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                      BillingStreet = 'Bliss',
                                      BillingCity = 'Malolos',
                                      BillingState = 'BUL',
                                      BillingPostalCode = '3000',
                                      BillingCountry = 'PH',
            						  Billing_Country_Code__c = 'AU',
            						  Industry_Type__c = 'Engineering',
            						  Account_Lifecycle__c = 'Offer',
            //MOD-END CILAG INS CASE# 00129600 09.23.16
            						  Administrator_Contact_Last_Name__c = 'Test ' + i));
        }
        insert testAccts; 

        Account acct = new Account(Name = 'TestDuplicateAcct1',
           						   Business_Number__c = '11122233344',
           						   Administrator_Contact_Email__c = 'TestAdmin@testsalesforce1.com',
           						   RecordTypeId = accelerateRecTypeId,
        //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                   BillingStreet = 'Bliss',
                                   BillingCity = 'Malolos',
                                   BillingState = 'BUL',
                                   BillingPostalCode = '3000',
                                   BillingCountry = 'PH',
            					   Billing_Country_Code__c = 'AU',
            					   Industry_Type__c = 'Engineering',
            					   Account_Lifecycle__c = 'Offer',
        //MOD-END CILAG INS CASE# 00129600 09.23.16
           						   Administrator_Contact_Last_Name__c = 'Test last');

        insert acct;

        //Select Id, Name, AccountId, Status__c, Key_Contact__c from Contact

        Contact con = new Contact(LastName ='Test Contact',
        						  AccountId = acct.Id,
        						  Status__c = 'Active',
        						  Key_Contact__c = true);

        insert con;


        Mailer_Expiry__c mExpiry = new Mailer_Expiry__c(Expiry_Date__c = Date.today().addDays(1));
        insert mExpiry;


		PageReference pageRef = Page.SelfUpdate;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('aid',acct.Id);		
		ApexPages.currentPage().getParameters().put('eId',mExpiry.Id);


		ApexPages.standardController controller = new ApexPages.standardController(acct);
		SelfUpdateController selfUpdate = new SelfUpdateController(controller);			
		PageReference pageRefExpected = Page.SelfUpdateSuccessful;
		PageReference pageRefActual = selfUpdate.clickYes();

		List<Account_Self_Update__c> acctSelfUpdateList = [Select Id, Account__c FROM Account_Self_Update__c WHERE Account__c =: acct.Id];

		System.assertEquals(pageRefExpected.getURL(), pageRefActual.getURL());
		System.assert(!acctSelfUpdateList.isEmpty());
		//Test.stopTest();
    }

	@isTest static void clickSaveTest() {
		String RECORDTYPE_ACCELERATE = 'Accelerate';

		Ignore_Email_Domains__c ignoreDomain = new Ignore_Email_Domains__c();
		ignoreDomain.Name = 'yahoo.com';

		insert ignoreDomain;
        // These accounts are for testing of duplicates
        Id accelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_ACCELERATE ).getRecordTypeId();

        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<3;i++) {
            testAccts.add(new Account(Name = 'TestDuplicateAcct',
            						  Business_Number__c = '11122233345',
            						  Administrator_Contact_Email__c = 'TestAdmin@testsalesforce.com',
            						  RecordTypeId = accelerateRecTypeId,
            //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                      BillingStreet = 'Bliss',
                                      BillingCity = 'Malolos',
                                      BillingState = 'BUL',
                                      BillingPostalCode = '3000',
                                      BillingCountry = 'PH',
            						  Billing_Country_Code__c = 'AU',
            						  Industry_Type__c = 'Engineering',
            						  Account_Lifecycle__c = 'Offer',
            //MOD-END CILAG INS CASE# 00129600 09.23.16
            						  Administrator_Contact_Last_Name__c = 'Test ' + i));
        }
        insert testAccts; 

        Account acct = new Account(Name = 'TestDuplicateAcct1',
           						   Business_Number__c = '11122233344',
           						   Administrator_Contact_Email__c = 'TestAdmin@testsalesforce1.com',
           						   RecordTypeId = accelerateRecTypeId,
        //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                   BillingStreet = 'Bliss',
                                   BillingCity = 'Malolos',
                                   BillingState = 'BUL',
                                   BillingPostalCode = '3000',
                                   BillingCountry = 'PH',
            					   Billing_Country_Code__c = 'AU',
            					   Industry_Type__c = 'Engineering',
            					   Account_Lifecycle__c = 'Offer',
        //MOD-END CILAG INS CASE# 00129600 09.23.16
           						   Administrator_Contact_Last_Name__c = 'Test last');

        insert acct;

        //Select Id, Name, AccountId, Status__c, Key_Contact__c from Contact

        Contact con = new Contact(LastName ='Test Contact',
        						  AccountId = acct.Id,
        						  Status__c = 'Active',
        						  Key_Contact__c = true);

        insert con;

        Mailer_Expiry__c mExpiry = new Mailer_Expiry__c(Expiry_Date__c = Date.today().addDays(1));
        insert mExpiry;

		PageReference pageRef = Page.SelfUpdate;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('aid',acct.Id);	
		ApexPages.currentPage().getParameters().put('eId',mExpiry.Id);

		ApexPages.standardController controller = new ApexPages.standardController(acct);
		SelfUpdateController selfUpdate = new SelfUpdateController(controller);	
        
		PageReference pageRefActual = selfUpdate.clickNo();

		List<Account_Self_Update__c> acctSelfUpdateList = [Select Id, Account__c FROM Account_Self_Update__c WHERE Account__c =: acct.Id AND Updated__c = false];
		PageReference pageRefExpected = Page.SelfUpdateEdit;
		pageRefExpected.getParameters().put('aid',acct.Id);		
		pageRefExpected.getParameters().put('eId',mExpiry.Id);

		Test.setCurrentPage(pageRefExpected);
		ApexPages.currentPage().getParameters().put('aid',acct.Id);
		ApexPages.currentPage().getParameters().put('eId',mExpiry.Id);
		ApexPages.standardController controller2 = new ApexPages.standardController(acct);
		SelfUpdateExtension selfUpdateExt2 = new SelfUpdateExtension(controller2);	
		selfUpdateExt2.redirectIfInvalid();
		selfUpdateExt2.newAcctSelfUpdate.Account_Name__c = 'Test';
		selfUpdateExt2.newAcctSelfUpdate.Billing_Street__c = '56 Edmondstone Rd';
		selfUpdateExt2.newAcctSelfUpdate.Business_Number__c = '11122233311';
		selfUpdateExt2.newAcctSelfUpdate.Billing_City__c = 'Bowen Hills';
		selfUpdateExt2.newAcctSelfUpdate.Annual_DOM_Spend_ALL_Carriers__c = 10;
		selfUpdateExt2.newAcctSelfUpdate.Billing_State_Province__c = 'QLD';
		selfUpdateExt2.newAcctSelfUpdate.Annual_INT_Spend_ALL_Carriers__c = 20;
		selfUpdateExt2.newAcctSelfUpdate.Billing_Post_Code__c = '4006';
		selfUpdateExt2.newAcctSelfUpdate.Please_Contact_Me__c = true;
		selfUpdateExt2.newAcctSelfUpdate.Notes__c = 'Test';

		PageReference pageRefSave = selfUpdateExt2.clickSave();
		
		PageReference expectedPageRef = Page.SelfUpdateSuccessful;

		List<Account_Self_Update__c> acctSelfUpdatedList = [Select Id, Account__c FROM Account_Self_Update__c WHERE Account__c =: acct.Id AND Updated__c = true];

		System.assertEquals(expectedPageRef.getURL(), pageRefSave.getURL());
		System.assert(!acctSelfUpdatedList.isEmpty());
	}

	@isTest static void clickCancelTest() {
		String RECORDTYPE_ACCELERATE = 'Accelerate';

		Ignore_Email_Domains__c ignoreDomain = new Ignore_Email_Domains__c();
		ignoreDomain.Name = 'yahoo.com';

		insert ignoreDomain;
        // These accounts are for testing of duplicates
        Id accelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_ACCELERATE ).getRecordTypeId();

        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<3;i++) {
            testAccts.add(new Account(Name = 'TestDuplicateAcct',
            						  Business_Number__c = '11122233345',
            						  Administrator_Contact_Email__c = 'TestAdmin@testsalesforce.com',
            						  RecordTypeId = accelerateRecTypeId,
             //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                      BillingStreet = 'Bliss',
                                      BillingCity = 'Malolos',
                                      BillingState = 'BUL',
                                      BillingPostalCode = '3000',
                                      BillingCountry = 'PH',
            					      Billing_Country_Code__c = 'AU',
            					      Industry_Type__c = 'Engineering',
            					      Account_Lifecycle__c = 'Offer',
             //MOD-END CILAG INS CASE# 00129600 09.23.16
            						  Administrator_Contact_Last_Name__c = 'Test ' + i));
        }
        insert testAccts; 

        Account acct = new Account(Name = 'TestDuplicateAcct1',
           						   Business_Number__c = '11122233344',
           						   Administrator_Contact_Email__c = 'TestAdmin@testsalesforce1.com',
           						   RecordTypeId = accelerateRecTypeId,
        //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                   BillingStreet = 'Bliss',
                                   BillingCity = 'Malolos',
                                   BillingState = 'BUL',
                                   BillingPostalCode = '3000',
                                   BillingCountry = 'PH',
            					   Billing_Country_Code__c = 'AU',
            					   Industry_Type__c = 'Engineering',
            					   Account_Lifecycle__c = 'Offer',
        //MOD-END CILAG INS CASE# 00129600 09.23.16
           						   Administrator_Contact_Last_Name__c = 'Test last');

        insert acct;

        //Select Id, Name, AccountId, Status__c, Key_Contact__c from Contact

        Contact con = new Contact(LastName ='Test Contact',
        						  AccountId = acct.Id,
        						  Status__c = 'Active',
        						  Key_Contact__c = true);

        insert con;

        Mailer_Expiry__c mExpiry = new Mailer_Expiry__c(Expiry_Date__c = Date.today().addDays(1));
        insert mExpiry;

		PageReference pageRef = Page.SelfUpdate;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('aid',acct.Id);	
		ApexPages.currentPage().getParameters().put('eId',mExpiry.Id);

		ApexPages.standardController controller = new ApexPages.standardController(acct);
		SelfUpdateController selfUpdate = new SelfUpdateController(controller);			
		

		PageReference pageRefActual = selfUpdate.clickNo();

		List<Account_Self_Update__c> acctSelfUpdateList = [Select Id, Account__c FROM Account_Self_Update__c WHERE Account__c =: acct.Id AND Updated__c = false];
		PageReference pageRefExpected = Page.SelfUpdateEdit;
		pageRefExpected.getParameters().put('aid',acct.Id);
		pageRefExpected.getParameters().put('cid',con.Id);
		pageRefExpected.getParameters().put('asuid',acctSelfUpdateList[0].Id);
		ApexPages.currentPage().getParameters().put('eId',mExpiry.Id);
		Test.setCurrentPage(pageRefExpected);
		ApexPages.currentPage().getParameters().put('aid',acct.Id);
		ApexPages.currentPage().getParameters().put('cid',con.Id);
		ApexPages.currentPage().getParameters().put('asuid',acctSelfUpdateList[0].Id);
		ApexPages.currentPage().getParameters().put('eId',mExpiry.Id);
		ApexPages.standardController controller2 = new ApexPages.standardController(acct);
		SelfUpdateExtension selfUpdateExt2 = new SelfUpdateExtension(controller2);	
		selfUpdateExt2.redirectIfInvalid();
		PageReference pageRefCancel = selfUpdateExt2.clickCancel();
		
		PageReference expectedPageRef = Page.SelfUpdate;
		expectedPageRef.getParameters().put('aid',acct.Id);
		expectedPageRef.getParameters().put('cid',con.Id);
		expectedPageRef.getParameters().put('eId',mExpiry.Id);
		System.assertEquals(expectedPageRef.getURL(), pageRefCancel.getURL());
		System.assert(!acctSelfUpdateList.isEmpty());
	}

	@isTest static void clickNoTest() {
		String RECORDTYPE_ACCELERATE = 'Accelerate';

		Ignore_Email_Domains__c ignoreDomain = new Ignore_Email_Domains__c();
		ignoreDomain.Name = 'yahoo.com';

		insert ignoreDomain;
        // These accounts are for testing of duplicates
        Id accelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_ACCELERATE ).getRecordTypeId();

        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<3;i++) {
            testAccts.add(new Account(Name = 'TestDuplicateAcct',
            						  Business_Number__c = '11122233345',
            						  Administrator_Contact_Email__c = 'TestAdmin@testsalesforce.com',
            						  RecordTypeId = accelerateRecTypeId,
            //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                      BillingStreet = 'Bliss',
                                      BillingCity = 'Malolos',
                                      BillingState = 'BUL',
                                      BillingPostalCode = '3000',
                                      BillingCountry = 'PH',
            					      Billing_Country_Code__c = 'AU',
            					      Industry_Type__c = 'Engineering',
            					      Account_Lifecycle__c = 'Offer',
            //MOD-END CILAG INS CASE# 00129600 09.23.16
            						  Administrator_Contact_Last_Name__c = 'Test ' + i));
        }
        insert testAccts; 

        Account acct = new Account(Name = 'TestDuplicateAcct1',
           						   Business_Number__c = '11122233344',
           						   Administrator_Contact_Email__c = 'TestAdmin@testsalesforce1.com',
           						   RecordTypeId = accelerateRecTypeId,
        //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                   BillingStreet = 'Bliss',
                                   BillingCity = 'Malolos',
                                   BillingState = 'BUL',
                                   BillingPostalCode = '3000',
                                   BillingCountry = 'PH',
            					   Billing_Country_Code__c = 'AU',
            					   Industry_Type__c = 'Engineering',
            					   Account_Lifecycle__c = 'Offer',
        //MOD-END CILAG INS CASE# 00129600 09.23.16
           						   Administrator_Contact_Last_Name__c = 'Test last');

        insert acct;

        //Select Id, Name, AccountId, Status__c, Key_Contact__c from Contact

        Contact con = new Contact(LastName ='Test Contact',
        						  AccountId = acct.Id,
        						  Status__c = 'Active',
        						  Key_Contact__c = true);

        insert con;

        Mailer_Expiry__c mExpiry = new Mailer_Expiry__c(Expiry_Date__c = Date.today().addDays(1));
        insert mExpiry;

		PageReference pageRef = Page.SelfUpdate;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('aid',acct.Id);
		ApexPages.currentPage().getParameters().put('cid',con.Id);
		ApexPages.currentPage().getParameters().put('eId',mExpiry.Id);

		ApexPages.standardController controller = new ApexPages.standardController(acct);
        SelfUpdateController selfUpdate = new SelfUpdateController(controller);	
		SelfUpdateExtension selfUpdateExt = new SelfUpdateExtension(controller);	
		selfUpdateExt.redirectIfInvalid();
		
		
		PageReference pageRefActual = selfUpdate.clickNo();

		List<Account_Self_Update__c> acctSelfUpdateList = [Select Id, Account__c FROM Account_Self_Update__c WHERE Account__c =: acct.Id AND Updated__c = false];
		PageReference pageRefExpected = Page.SelfUpdateEdit;
		pageRefExpected.getParameters().put('aid',acct.Id);		
		pageRefExpected.getParameters().put('eId',mExpiry.Id);

		System.assertEquals(pageRefExpected.getURL(), pageRefActual.getURL());
		System.assert(!acctSelfUpdateList.isEmpty());
	}*/
}