global class ScheduleAccelerateFlagUpdate implements Schedulable
{
    
	global void execute(SchedulableContext sc)
    {
	
	AccelerateStatementSendUpdate accflagupdate = new AccelerateStatementSendUpdate();
    Database.executeBatch(accflagupdate, 50); 
	    
	} 

}