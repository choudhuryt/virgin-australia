/**
 * @description       : Handler class for DataValidityTrigger
 * @CreatedBy         : CloudWerx
 * @UpdatedBy         : CloudWerx
**/
public class DataValidityTriggerHandler {
    //@UpdatedBy : cloudwerx : Added code to get record type Ids dynamically
    public static Id dataValidityIATAValidityRecordTypeId = Utilities.getRecordTypeId('Data_Validity__c', 'IATA_Validity');
    public static Id dataValidityPCCRecordTypeId = Utilities.getRecordTypeId('Data_Validity__c', 'PCC');
    public static Id dataValidityAGYRecordTypeId = Utilities.getRecordTypeId('Data_Validity__c', 'AGY');
    public static Id dataValidityTIDSRecordTypeId = Utilities.getRecordTypeId('Data_Validity__c', 'TIDS');
    public static Id dataValidityTourCodeRecordTypeId = Utilities.getRecordTypeId('Data_Validity__c', 'Tour_Code');
    //@UpdatedBy : cloudwerx : Added code to get record type Ids dynamically
    
    public static void dataValidityActivity(List<Data_Validity__c> dataValidities) {
        Set<Id> iataIds = new Set<Id>();
        Set<Id> pccIds = new Set<Id>();
        Set<Id> agyIds = new Set<Id>();
        Set<Id> tidsIds = new Set<Id>();
        Set<Id> tourCodesIds = new Set<Id>();
        for(Data_Validity__c dataValidityObj :dataValidities) {
            if(dataValidityObj.recordtypeId == dataValidityIATAValidityRecordTypeId && dataValidityObj.IATA__c != null) {
                iataIds.add(dataValidityObj.IATA__c);
            } else if(dataValidityObj.recordtypeId == dataValidityPCCRecordTypeId && dataValidityObj.PCC__c != null) {
                pccIds.add(dataValidityObj.PCC__c);
            } else if(dataValidityObj.recordtypeId == dataValidityAGYRecordTypeId && dataValidityObj.AGY__c != null) {
                agyIds.add(dataValidityObj.AGY__c);
            } else if(dataValidityObj.recordtypeId == dataValidityTIDSRecordTypeId && dataValidityObj.TIDS__c != null) {
                tidsIds.add(dataValidityObj.TIDS__c);
            } else if(dataValidityObj.recordtypeId == dataValidityTourCodeRecordTypeId && dataValidityObj.Tourcode__c != null) {
                tourCodesIds.add(dataValidityObj.Tourcode__c);
            }
        }
        
        manageExistingDataValidityRecordsAndUpdateRelatedSObject(dataValidities, 'IATA__c', 'IATA_Number__c', iataIds);
        manageExistingDataValidityRecordsAndUpdateRelatedSObject(dataValidities, 'PCC__c', 'PCC__c', pccIds);
        manageExistingDataValidityRecordsAndUpdateRelatedSObject(dataValidities, 'AGY__c', 'AGYLogin__c', agyIds);
        manageExistingDataValidityRecordsAndUpdateRelatedSObject(dataValidities, 'TIDS__c', 'TIDS__c', tidsIds);
        manageExistingDataValidityRecordsAndUpdateRelatedSObject(dataValidities, 'Tourcode__c', 'Tourcodes__c', tourCodesIds);
    }
    
    private static void manageExistingDataValidityRecordsAndUpdateRelatedSObject(List<Data_Validity__c> dataValidities, String FieldName, String sObjectName, Set<Id> parentIds) {
        if(String.isBlank(FieldName) || parentIds.size() == 0 || String.isBlank(sObjectName)) {
            return;
        }
        
        List<SObject> sObjectListToUpdate = new List<SObject>();
        String sobjectSOQL = 'SELECT Id, Account__c FROM ' + sObjectName + ' WHERE Id IN:parentIds';
        Map<Id, Sobject> sObjectMap = new Map<Id, SObject>(Database.query(sobjectSOQL));
        
        String dataValidityObjSOQL = 'SELECT Id, ' + FieldName + ', From_Date__c, To_Date__c, From_Account__c, Soft_Delete__c FROM Data_Validity__c WHERE ' + FieldName + ' IN :parentIds';
        List<Data_Validity__c> existingDataValidities = Database.query(dataValidityObjSOQL);
        for(Data_Validity__c dataValidityObj :dataValidities) {
            for(Data_Validity__c existingDataValidityObj :existingDataValidities) {
                if(dataValidityObj.id != existingDataValidityObj.Id && dataValidityObj.From_Date__c <= existingDataValidityObj.To_Date__c && !existingDataValidityObj.Soft_Delete__c) {
                    Date dInputDate = existingDataValidityObj.To_Date__c;
                    DateTime dtValue = DateTime.newInstance(dInputDate.year(), dInputDate.month(), dInputDate.day());
                    string sFormattedDate = dtValue.format('dd-MM-YYYY');
                    dataValidityObj.name.addError('The Start Date Of the new ' + FieldName.replace('__c', '')+ ' cannot be before the end date of the previous Data Validity record, the end date of the previous Data Validity record is ' + sFormattedDate);
                }
            }
            SObject sObj = (sObjectMap != null && sObjectMap.containsKey((Id)dataValidityObj.get(FieldName))) ? sObjectMap.get((Id)dataValidityObj.get(FieldName)) : null;
            if (sObj != null && sObj.Id != null && (Id)sObj.get('Account__c') != dataValidityObj.From_Account__c) {
                sObj.put('Account__c', dataValidityObj.From_Account__c);
                sObjectListToUpdate.add(sObj);
            }
        }
        UPDATE sObjectListToUpdate;
    }
}