public with sharing class BeyondContractValueController
{
private ApexPages.StandardController controller {get; set;}
public String message {get;set;}
private Account a;
public Account  acctotals {get;set;}
    
    public BeyondContractValueController(ApexPages.StandardController myController) 
	{
		a=(Account)myController.getrecord();
		
		
		
		// Get all active contracts
		List<Account> acc = [ SELECT Id,Total_Economy_X_Indicative_Prev_FY__c,Total_Economy_X_Indicative_This_FY__c,
                                          Total_Economy_X_Prev_FY__c,Total_Economy_X_This_FY__c,Total_FlyForwards_Prev_FY__c,
                                          Total_FlyForwards_This_FY__c,Total_FlyForward_Indicative_Prev_FY__c,
                                          Total_FlyForward_Indicative_ThisFY__c,Velocity_PG_Out_of_Contract_Prev_FY__c,
                                          Velocity_PG_Out_of_Contract__c,VelocityPlatinum_Out_Of_Contract_Prev_FY__c,
                                          Velocity_Platinum_Out_of_Contract__c,Velocity_Silver_Out_of_Contract_Prev_FY__c,
                                          Velocity_Silver_Out_of_Contract__c,Velocity_Gold_Out_of_Contract_Prev_FY__c,
                                          Velocity_Gold_Out_of_Contract__c,Velocity_Acceleration_Indicative_PrevFY__c,
                                          Velocity_Acceleration_Indicative__c FROM Account
                                          where Id = :a.id
                            ];
        
          acctotals = acc.get(0);
        if(acc.size() == 0)
        {
        	message = '***   No details available.   ***';
        }

	}
    
    	//pre-processing prior to Page load
	public PageReference initDisc() 
	{
		return null;
	}

}