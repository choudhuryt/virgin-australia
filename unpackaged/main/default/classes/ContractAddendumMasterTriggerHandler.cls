/*
 * Updated by tapashree.choudhury@virginaustralia.com
 * Updated on 08.08.2022
 * Description: For Airline additions in Project Willis
*/
public class ContractAddendumMasterTriggerHandler{
    
    public static string domMainline = System.Label.DOM_Mainline;
    public static string domRegional = System.Label.DOM_Regional;
    public static string intShortHaul = System.Label.INT_Short_Haul;
    public static string transTasman = System.Label.Trans_Tasman;
    public static string northAmerica = System.Label.North_America;
    public static string africa = System.Label.Africa;
    public static string middleEast = System.Label.Middle_East;
    public static string ukEuropeQR = System.Label.UK_Europe_QR;
    public static string ukEuropeSQ = System.Label.UK_Europe_SQ;
    public static List<String> airlineList = new List<String>{domMainline,domRegional,intShortHaul,transTasman,northAmerica,africa,middleEast,ukEuropeQR,ukEuropeSQ};
        
    public static void  SumbitExtensionForApproval (Boolean isDelete, List<Contract_Addendum__c> CAInTrigger,Map<Id, Contract_Addendum__c> CAOldInTrigger)
    {
        Set<Id> ContractExtnIDs  = new Set<Id>();
        Set<Id> ContractaddIDs  = new Set<Id>();
        List<Contract_Extension__c> affectedCE = new List<Contract_Extension__c>();
        
        for(Contract_Addendum__c CATrigger : CAInTrigger)  
            
        { 
            Contract_Addendum__c oldCA = CAOldInTrigger.get(CATrigger.Id);        
            if((oldCA.Status__c != CATrigger.Status__c) &&  CATrigger.Status__c == 'Approved By VA' && CATrigger.Contract_Extension__c <> NULL )
            {       
                ContractaddIDs.add(CATrigger.Id); 
                ContractExtnIDs.add(CATrigger.Contract_Extension__c);         
            }
        }
        
        affectedCE = [SELECT Account__c,Contract__c,Id,Name,Status__c FROM Contract_Extension__c where Id= :ContractExtnIDs and Status__c = 'Draft'];
                
        if(affectedCE.size() > 0)     
        {      
            Approval.ProcessSubmitRequest req1 =    new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(affectedCE[0].id);
            req1.setProcessDefinitionNameOrId('Contract_Extension_Auto_Approved');   
            req1.setSkipEntryCriteria(true);
            Approval.ProcessResult result = Approval.process(req1);    
        }     
    }
    
    public static void  UpdateDiscountOnContract (Boolean isDelete, List<Contract_Addendum__c> CAInTrigger,Map<Id, Contract_Addendum__c> CAOldInTrigger)
    {
        Set<Id> ContractIDs  = new Set<Id>();
        Set<Id> ContractaddIDs  = new Set<Id>();
        List<Contract_Addendum__c> conaddllist = new List<Contract_Addendum__c>();  
        List<Additional_Benefits__c> lstaddbenUpdate = new List<Additional_Benefits__c>();
        List<Market__c> lstdommarketUpdate = new List<Market__c>();
        List<Market__c> lstregmarketUpdate = new List<Market__c>();
        List<Proposal_Table__c> proposallist = new List<Proposal_Table__c>();
        List<Red_Circle_Discounts__c> redcirclelist = new List<Red_Circle_Discounts__c>();
        List<Proposed_Discount_Tables__c> newdiscountlist = new List<Proposed_Discount_Tables__c>();
        List<Proposed_Red_Tables__c> newrcdiscountlist = new List<Proposed_Red_Tables__c>();
        List<Proposal_Table_Backup__c> tempproptlist = new List<Proposal_Table_Backup__c>();
        List<Proposal_Table__c> plist = new List<Proposal_Table__c>();
        List<Additional_Benefits__c> addlist = new List<Additional_Benefits__c>();    
        List<Market__c> dommarlist = new List<Market__c>(); 
        List<Market__c> regmarlist   = new List<Market__c>();     
        List<Red_Circle_Discounts_BackUp__c> temprclist = new List<Red_Circle_Discounts_BackUp__c>();
        List<Red_Circle_Discounts__c> rlist = new List<Red_Circle_Discounts__c>();
        
        List<Market__c> marketlist = new List<Market__c>();
        List<Market__c> updateMarketList = new List<Market__c>();
            
        for(Contract_Addendum__c CATrigger : CAInTrigger)  
            
        { 
            
            Contract_Addendum__c oldCA = CAOldInTrigger.get(CATrigger.Id);  
            
            if((oldCA.Update_Discounts_On_Contract__c != CATrigger.Update_Discounts_On_Contract__c) &&  CATrigger.Update_Discounts_On_Contract__c== 'Yes')
            {       
                ContractIDs.add(CATrigger.Contract__c);  
                ContractaddIDs.add(CATrigger.id);
            }
        }
        
        conaddllist = [SELECT Contract__c,Cos_Version__c,Domestic_Guideline_Tier__c,Domestic_Requested_Tier__c,Id,Lounge_Membership_Annual_Fee__c,Lounge_Scheme_Selected__c,Regional_Guideline_Tier__c,
                       Regional_Requested_Tier__c, Africa_requested_Tier__c, INT_short_Haul_Requested_Tier__c, Middle_East_Requested_Tier__c, North_America_Requested_Tier__c, Trans_Tasman_Requested_Tier__c, UK_Europe_Requested_Tier_QR__c, UK_Europe_Requested_Tier_SQ__c 
                       FROM Contract_Addendum__c
                       where id =:ContractaddIDs];   
        
        proposallist = [SELECT Contract__c,DISCOUNT_OFF_PUBLISHED_FARE__c,Eligible_Booking_Class__c,
                        ELIGIBLE_FARE_TYPE__c,Id,Name,VA_ELIGIBLE_BOOKING_CLASS__c 
                        FROM Proposal_Table__c where Contract__c = :ContractIDs
                        and Name in :airlineList /*('DOM Mainline','DOM Regional')*/ 
                        order by  Name,Sort_Order__c ];
        
        newdiscountlist=[ SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,
                         Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
                         where Contract__c = :ContractIDs
                         and Contract_Addendum__c = :ContractaddIDs
                         and Name in :airlineList /*('DOM Mainline','DOM Regional') */
                         order by  Name,Sort_Order__c ] ;
                
        addlist=[SELECT Id,Joining_Fee__c,LoungeMembership__c,Lounge_Membership_Tier__c,Lounge_Membership__c,Membership_Fee__c 
                 FROM Additional_Benefits__c 
                 where Contract__c = :ContractIDs
                 and Membership_Fee__c  > 0
                ];
        
        /*dommarlist =[ SELECT Contract__c,Guideline_Tier__c,Id,Market_Type__c,Market__c,Name,Requested_Tier__c 
                     FROM Market__c
                     where Contract__c = :ContractIDs
                     and Name= 'DOM Mainline'
                    ];
        
        regmarlist =[ SELECT Contract__c,Guideline_Tier__c,Id,Market_Type__c,Market__c,Name,Requested_Tier__c 
                     FROM Market__c
                     where Contract__c = :ContractIDs
                     and Name= 'DOM Regional'
                    ];*/
        
        marketlist =[SELECT Contract__c,Guideline_Tier__c,Id,Market_Type__c,Market__c,Name,Requested_Tier__c 
                     FROM Market__c
                     WHERE Contract__c = :ContractIDs
                     AND Name IN: airlineList
                    ];
        
        /*if(dommarlist.size() > 0  &&  conaddllist.size() > 0 && conaddllist[0].Domestic_Requested_Tier__c <>dommarlist[0].Requested_Tier__c )     
        {
            for(Market__c  dm:  dommarlist)
            {
                dm.Requested_Tier__c   = conaddllist[0].Domestic_Requested_Tier__c ;
                lstdommarketUpdate.add(dm);   
            }
            update lstdommarketUpdate;
            
        }
        
        if(regmarlist.size() > 0  &&  conaddllist.size() > 0 && conaddllist[0].Regional_Requested_Tier__c <>regmarlist[0].Requested_Tier__c )     
        {
            for(Market__c  regm:  regmarlist)
            {
                regm.Requested_Tier__c   = conaddllist[0].Regional_Requested_Tier__c ;
                lstregmarketUpdate.add(regm);   
            }
            update lstregmarketUpdate;
            
        }*/
        
        if(marketlist.size() > 0  &&  conaddllist.size() > 0){
            for(Market__c mar: marketlist){
                if(mar.Name == domMainline && conaddllist[0].Domestic_Requested_Tier__c <> mar.Requested_Tier__c){
                    mar.Requested_Tier__c   = conaddllist[0].Domestic_Requested_Tier__c ;
                    updateMarketList.add(mar);
                }else if(mar.Name == domRegional && conaddllist[0].Regional_Requested_Tier__c <> mar.Requested_Tier__c){
                    mar.Requested_Tier__c   = conaddllist[0].Regional_Requested_Tier__c ;
                    updateMarketList.add(mar);
                }else if(mar.Name == africa && conaddllist[0].Africa_requested_Tier__c <> mar.Requested_Tier__c){
                    mar.Requested_Tier__c   = conaddllist[0].Africa_requested_Tier__c ;
                    updateMarketList.add(mar);
                }else if(mar.Name == intShortHaul && conaddllist[0].INT_short_Haul_Requested_Tier__c <> mar.Requested_Tier__c){
                    mar.Requested_Tier__c   = conaddllist[0].INT_short_Haul_Requested_Tier__c ;
                    updateMarketList.add(mar);
                }else if(mar.Name == transTasman && conaddllist[0].Trans_Tasman_Requested_Tier__c <> mar.Requested_Tier__c){
                    mar.Requested_Tier__c   = conaddllist[0].Trans_Tasman_Requested_Tier__c ;
                    updateMarketList.add(mar);
                }else if(mar.Name == northAmerica && conaddllist[0].North_America_Requested_Tier__c <> mar.Requested_Tier__c){
                    mar.Requested_Tier__c   = conaddllist[0].North_America_Requested_Tier__c ;
                    updateMarketList.add(mar);
                }else if(mar.Name == middleEast && conaddllist[0].Middle_East_Requested_Tier__c <> mar.Requested_Tier__c){
                    mar.Requested_Tier__c   = conaddllist[0].Middle_East_Requested_Tier__c ;
                    updateMarketList.add(mar);
                }else if(mar.Name == ukEuropeQR && conaddllist[0].UK_Europe_Requested_Tier_QR__c <> mar.Requested_Tier__c){
                    mar.Requested_Tier__c   = conaddllist[0].UK_Europe_Requested_Tier_QR__c ;
                    updateMarketList.add(mar);
                }else if(mar.Name == ukEuropeSQ && conaddllist[0].UK_Europe_Requested_Tier_SQ__c <> mar.Requested_Tier__c){
                    mar.Requested_Tier__c   = conaddllist[0].UK_Europe_Requested_Tier_SQ__c ;
                    updateMarketList.add(mar);
                }             
            }            
        }
        
        if(updateMarketList.size()>0){
            update updateMarketList;
        }
        
        if(addlist.size() > 0  &&  conaddllist.size() > 0 && conaddllist[0].Lounge_Scheme_Selected__c && conaddllist[0].Lounge_Membership_Annual_Fee__c > 0 )     
        {
            for(Additional_Benefits__c  ab: addlist)
            {
                ab.Membership_Fee__c  = conaddllist[0].Lounge_Membership_Annual_Fee__c ;
                ab.Joining_Fee__c = 0 ;
                lstaddbenUpdate.add(ab);   
            }
            
            update lstaddbenUpdate;
            
        }
        
        if ( newdiscountlist.size() > 0)
        {
            if(proposallist.size() > 0)
            {    
                for(Integer x =0; x < proposallist.size(); x++ )
                {
                    
                    Proposal_Table_Backup__c pback = new  Proposal_Table_Backup__c();
                    pback.Contract__c  =  proposallist[x].Contract__c;
                    pback.Discount_Off_Published_Fare__c  =  proposallist[x].Discount_Off_Published_Fare__c;
                    pback.Eligible_Fare_Type__c  =  proposallist[x].Eligible_Fare_Type__c;
                    pback.VA_Eligible_Booking_Class__c =  proposallist[x].VA_Eligible_Booking_Class__c;
                    pback.Name =      proposallist[x].Name;
                    tempproptlist.add(pback);        
                }
                
                try
                {
                    insert  tempproptlist ;
                    delete proposallist;    
                }
                catch(Exception e){
                    
                }
            }
            for(Integer x =0; x < newdiscountlist.size(); x++ )
            {
                
                Proposal_Table__c ptable = new  Proposal_Table__c ();
                ptable.Contract__c  = newdiscountlist[x].Contract__c;
                ptable.Discount_Off_Published_Fare__c  = newdiscountlist[x].Discount_Off_Published_Fare__c;
                ptable.Eligible_Fare_Type__c  = newdiscountlist[x].Eligible_Fare_Type__c;
                ptable.VA_Eligible_Booking_Class__c = newdiscountlist[x].VA_Eligible_Booking_Class__c;
                ptable.Name =      newdiscountlist[x].Name;
                plist.add(ptable);        
            }
            try
            {
                insert  plist ; 
            }
            catch(Exception e){
                
            }
            
        }
                
        redcirclelist = [SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,Applicable_Routes__c,
                         ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,Contract__c,
                         Partner_Eligible_Booking_Class__c ,Fixed_Base_Fare__c
                         FROM Red_Circle_Discounts__c where Contract__c = :ContractIDs
                         and Name in :airlineList /*('DOM Mainline','DOM Regional')*/
                         order by  Name,Sort_Order__c ];
                        
        newrcdiscountlist=[ SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,
                           Id,Name,VA_Eligible_Booking_Class__c,Applicable_Routes__c,Fixed_Base_Fare__c,
                           Domestic_Red_Circle_Requested_Tier__c
                           FROM Proposed_Red_Tables__c
                           where Contract__c = :ContractIDs
                           and Contract_Addendum__c = :ContractaddIDs
                           and Name in :airlineList /*('DOM Mainline','DOM Regional')*/ 
                           order by  Name,Sort_Order__c ] ;        
        
        system.debug('This is proposed red circle discout size'  + newrcdiscountlist.size()   );    
        if ( newrcdiscountlist.size() > 0)
        {
            if (redcirclelist.size() > 0 )
            {     
                for(Integer x =0; x < redcirclelist.size(); x++ )
                {                    
                    Red_Circle_Discounts_BackUp__c rcback = new  Red_Circle_Discounts_BackUp__c();
                    rcback.Contract__c  =  redcirclelist[x].Contract__c;
                    rcback.Discount_Off_Published_Fare__c  =  redcirclelist[x].Discount_Off_Published_Fare__c;
                    rcback.Eligible_Fare_Type__c  =  redcirclelist[x].Eligible_Fare_Type__c;
                    rcback.VA_Eligible_Booking_Class__c =  redcirclelist[x].VA_Eligible_Booking_Class__c;
                    rcback.Applicable_Routes__c = redcirclelist[x].Applicable_Routes__c;
                    rcback.Fixed_Base_Fare__c = redcirclelist[x].Fixed_Base_Fare__c;
                    rcback.Name =      redcirclelist[x].Name;
                    temprclist.add(rcback);        
                }                
                try
                {
                    insert  temprclist ;
                    delete redcirclelist;    
                }
                catch(Exception e){
                    
                }
            }
            
            for(Integer x =0; x < newrcdiscountlist.size(); x++ )
            {
                system.debug('Inside the for loop')     ;
                Red_Circle_Discounts__c rctable = new  Red_Circle_Discounts__c ();
                rctable.Contract__c  = newrcdiscountlist[x].Contract__c;
                rctable.Discount_Off_Published_Fare__c  = newrcdiscountlist[x].Discount_Off_Published_Fare__c;
                rctable.Eligible_Fare_Type__c  = newrcdiscountlist[x].Eligible_Fare_Type__c;
                rctable.VA_Eligible_Booking_Class__c = newrcdiscountlist[x].VA_Eligible_Booking_Class__c;
                rctable.Applicable_Routes__c = newrcdiscountlist[x].Applicable_Routes__c;
                rctable.Fixed_Base_Fare__c = newrcdiscountlist[x].Fixed_Base_Fare__c;
                rctable.Name =      newrcdiscountlist[x].Name;
                system.debug('Inside the for loop' + rctable)     ;
                rlist.add(rctable);
                
            }
            try
            {
                insert  rlist ; 
            }
            catch(Exception e){
                
            }
            
        }               
    }
}