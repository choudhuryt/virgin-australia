/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PRISMDataReviewControllerUnitTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account acc= CreateTestAccount();
        CreateAllPRISMKeyRoute(acc);
        PRISMDataReviewController PRISMDataReviewController = new PRISMDataReviewController();
				PageReference opptyPage = new PageReference('/apex/PRISMDataReview?acid='+acc.Id);
				Test.setCurrentPage(opptyPage);
				CreatePOSRevenue(acc);
				System.assertNotEquals(null,PRISMDataReviewController.CheckAccount());
				PRISMDataReviewController.CurrentAccountId=acc.Id;
				System.assertEquals(null,PRISMDataReviewController.CheckAccount());
				System.assertNotEquals(null,PRISMDataReviewController.GetAllPOSRevenueItem());
				System.assertNotEquals(null,PRISMDataReviewController.ViewDetail());
				System.assertNotEquals(null,PRISMDataReviewController.GetAllPRISMKeyRoute());
				System.assertNotEquals(null,PRISMDataReviewController.viewAsPDF());
				//delid
				//'International'
			
    }
   	private static Account CreateTestAccount()
		{
			Account account 				= new Account();
			CommonObjectsForTest commonObjectsFortest = new CommonObjectsForTest();
			account = commonObjectsFortest.CreateAccountObject(0);
  			
  			insert account;
			return account;
		}
		private static void CreatePOSRevenue(Account acc)
		{
			POS_Revenue__c p = new POS_Revenue__c();
			for(Integer i = 0 ; i < 10; i ++)
			{
				p = new POS_Revenue__c();
				p.Year__c='';
				p.YY_YTD_Total_Amount__c=89741.0; 
				p.YY_YTD_Count__c=14144; 
				p.YY_YTD_Count_Share__c=4567; 
				p.YY_YTD_Amount_Share__c=1040;
				p.VA_YTD_Total_Amount__c=5410; 
				p.VA_YTD_Count__c=1010; 
				p.VA_YTD_Count_Share__c=1444; 
				p.VA_YTD_Amount_Share__c=4545;
				
				if( i==0 || i==1 || i==3)
				{
					p.Type__c='Domestic'; 
					p.Revenue_Type__c='Annual';
					p.Start_Date__c=date.newInstance(2012, 1, 1);
					p.End_Date__c=date.newInstance(2013, 1, 1);
				}
				else if( i==5 || i==7 )
				{
					p.Type__c='International'; 
					p.Revenue_Type__c='Annual';
					p.Start_Date__c=date.newInstance(2012, 1, 1);
					p.End_Date__c=date.newInstance(2013, 1, 1);
				}				
				if( i==2 || i==4 || i==6)
				{
					p.Type__c='Domestic'; 
					p.Revenue_Type__c='Monthly';
					p.Start_Date__c=date.newInstance(2010, 1, 1);
					p.End_Date__c=date.newInstance(2011, 1, 1);
				}
				else if( i==9 || i==8 )
				{
					p.Type__c='International'; 
					p.Revenue_Type__c='Monthly';
					p.Start_Date__c=date.newInstance(2010, 1, 1);
					p.End_Date__c=date.newInstance(2011, 1, 1);
				}
				p.Contracted_Revenue_Percentage__c=0.0;
				p.Account__c=acc.Id; 
				p.ALL_YTD_Total_Count__c=10400; 
				p.ALL_YTD_Total_Amount__c=500;
				p.PRISM_Record_ID__c='CTT01'+i;
				
				insert p;
			}
		}
		private static void CreateAllPRISMKeyRoute(Account acc)
		{
			PRISM_Key_Routes__c p = new PRISM_Key_Routes__c();
			for(Integer i = 0 ; i < 10; i ++)
			{
				p = new PRISM_Key_Routes__c();
					p.VA_Share_of_Count_Previous_Year__c=10;
					p.VA_Share_of_Count_Current_Year__c=10; 
					p.VA_Share_of_Amount_Previous_Year__c=10; 
					p.VA_Share_of_Amount_Current_Year__c=100; 
					p.VA_Count_Previous_Year__c=145477; 
					p.VA_Count_Current_Year__c=7455; 
					p.VA_Amount_Previous_Year__c=545; 
					p.VA_Amount_Current_Year__c=5454;
						if( i==2 || i==4 || i==6)
					{
						p.Type__c='Domestic'; 
						p.Start_Date__c=date.newInstance(2010, 1, 1);
					p.End_Date__c=date.newInstance(2011, 1, 1);
					}
					else
					{
						p.Type__c='International';
						p.Start_Date__c=date.newInstance(2011, 1, 1);
						p.End_Date__c=date.newInstance(2012, 1, 1);
					}
					p.PRISM_Record_ID__c=String.valueOf(i); 
					p.Origin__c='ORI' + i; 
					p.Origin_Full__c='ORIF' + i; 
					p.Order_Number__c=i;
					p.Destination__c='Destination__c'; 
					p.Destination_Full__c='Destination__c';
					p.Account__c=acc.Id;
					upsert p;
			}
		}
}