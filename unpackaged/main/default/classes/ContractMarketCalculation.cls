public class ContractMarketCalculation {
    
    @InvocableMethod
    public static void UpdateRelatedContract(List<Id> ContractIds)   
    {
        List<Contract> lstContractUpdate = new List<Contract>();   
        
        List<Contract> conlist =  [Select Id ,Virgin_Australia_Amount__c,Domestic_and_Trans_Tasman_VA_Rev_Target__c,Domestic_TT_VA_Market_Share_Target__c,
                                   DOM_Regional_Spend_ALL_Carriers__c, Domestic_Regional_Revenue_Target__c, Domestic_Revenue_Target__c,
                                   Trans_Tasman_Amount__c,TT_Revenue_target__c,	TT_Market_Share_target__c,
                                   Hong_Kong__c,HK_Renevue_Target__c,HK_Market_Share_Target__c,
                                   London__c,London_Revenue_Target__c,London_Market_Share_Target__c,
                                   China__c,China_Market_Share_Target__c,China_Revenue_Target__c,
                                   International_S_H_ALL__c,International_S_H_Revenue_Target__c,International_S_H_market_Share_target__c,
                                   USA_Canada_Amount__c,North_America__c,	North_America_MS__c,
                                   UK_Europe_Amount__c,UK_Europe_Revenue_Target__c,UK_Europe_MS__c,
                                   Middle_East_Amount__c,	Middle_East_Revenue_Target__c,Middle_East_MS__c,
                                   AFRICA_Spend__c,Africa_revenue_Target__c,Africa_MS__c,
                                   Asia_Expenditure__c	,Asia_Revenue_Target__c,Asia_MS__c,
                                   Domestic_Contracted_Revenue_Target__c,S_H_Annual_Contracted_Revenue_Target__c,
                                   L_H_Annual_Contracted_Revenue_Target__c,Opportunity__c
                                   from Contract where id =:ContractIds ]; 
        
        List<Market__c> dommarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c
                                          from Market__c  where Contract__c =:ContractIds and name LIKE '%DOM%Mainline%' 
                                          and  VA_Revenue_Commitment__c > 0 ]; 
        
        List<Market__c> regmarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c
                                          from Market__c  where Contract__c =:ContractIds and  name LIKE  '%DOM%Regional%' 
                                          and  VA_Revenue_Commitment__c > 0 ]; 
        
        List<Market__c> ttmarketlist =   [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c
                                          from Market__c  where Contract__c =:ContractIds and name LIKE 'Trans%Tasman%TT%' 
                                          and  VA_Revenue_Commitment__c > 0 ];  
        
        List<Market__c> ishmarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c
                                          from Market__c  where Contract__c =:ContractIds and name LIKE 'INT%Short%Haul%' 
                                          and  VA_Revenue_Commitment__c > 0 ];  
        
        List<Market__c> hkmarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c
                                         from Market__c  where Contract__c =:ContractIds and name LIKE '%Hong%Kong%' 
                                         and  VA_Revenue_Commitment__c > 0  ];  
        
        List<Market__c> lonmarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c
                                          from Market__c  where Contract__c =:ContractIds and name LIKE '%London%' 
                                          and  VA_Revenue_Commitment__c > 0  ];   
        
        List<Market__c> cnmarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c
                                         from Market__c  where Contract__c =:ContractIds and name LIKE  '%China%' 
                                         and  VA_Revenue_Commitment__c > 0  ];
        
        List<Market__c> namarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c
                                         from Market__c  where Contract__c =:ContractIds and name LIKE '%North America%' 
                                         and  VA_Revenue_Commitment__c > 0  ]; 
        
        List<Market__c> eumarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c,Name
                                         from Market__c  where Contract__c =:ContractIds and name LIKE '%UK/Europe%' 
                                         and  VA_Revenue_Commitment__c > 0 ];
        
        List<Market__c> memarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c,Name
                                         from Market__c  where Contract__c =:ContractIds and name LIKE '%Middle East%' 
                                         and  VA_Revenue_Commitment__c > 0 ]; 
        
        List<Market__c> afmarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c,Name
                                         from Market__c  where Contract__c =:ContractIds and name LIKE '%Africa%' 
                                         and  VA_Revenue_Commitment__c > 0  ]; 
        List<Market__c> asmarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c,Name
                                         from Market__c  where Contract__c =:ContractIds and name LIKE '%Asia%' 
                                         and  VA_Revenue_Commitment__c > 0  ];
        
        List<Market__c> jpmarketlist =  [Select VA_Serviceable_Routes_p_a__c , VA_Revenue_Commitment__c,Revenue_M_S__c,Name
                                         from Market__c  where Contract__c =:ContractIds and name LIKE '%Japan%' 
                                         and  VA_Revenue_Commitment__c > 0  ];
                        
        List<aggregateResult> totresults =[SELECT SUM(Total_Spend__c) totspend , SUM(VA_Revenue_Commitment__c) totcomm ,Contract__c
                                           FROM Market__c
                                           where Contract__c = :ContractIds
                                           group by Contract__c
                                          ]; 
        
        for(Contract c: conlist)
        {
            if(conlist[0].Opportunity__c <> null    )
            {     
                c.Domestic_and_Trans_Tasman_VA_Rev_Target__c = 0 ;
                c.Domestic_Regional_Revenue_Target__c = 0;
                c.TT_Revenue_target__c = 0 ;
                c.International_S_H_Revenue_Target__c = 0;       
                c.Asia_Revenue_Target__c  = 0 ;
                c.Africa_revenue_Target__c = 0 ;
                c.Middle_East_Revenue_Target__c = 0 ;
                c.London_Revenue_Target__c = 0 ;
                c.China_Revenue_Target__c = 0 ;
                c.North_America__c = 0 ;
                c.HK_Renevue_Target__c = 0 ;
                c.UK_Europe_Revenue_Target__c = 0 ; 
                c.S_H_Annual_Contracted_Revenue_Target__c = 0;
                c.L_H_Annual_Contracted_Revenue_Target__c = 0; 
                c.Domestic_Contracted_Revenue_Target__c= 0; 
                                
                if(dommarketlist.size() > 0 )  
                {  
                    c.Virgin_Australia_Amount__c = dommarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.Domestic_and_Trans_Tasman_VA_Rev_Target__c = dommarketlist[0].VA_Revenue_Commitment__c ;
                    c.Domestic_TT_VA_Market_Share_Target__c = dommarketlist[0].Revenue_M_S__c ;  
                    
                }
                
                if(regmarketlist.size() > 0 )  
                {  
                    c.DOM_Regional_Spend_ALL_Carriers__c = regmarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.Domestic_Regional_Revenue_Target__c = regmarketlist[0].VA_Revenue_Commitment__c ;
                    c.Domestic_Revenue_Target__c = regmarketlist[0].Revenue_M_S__c ;            
                }
                
                if(ttmarketlist.size() > 0 )  
                {  
                    c.Trans_Tasman_Amount__c = ttmarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.TT_Revenue_target__c = ttmarketlist[0].VA_Revenue_Commitment__c ;
                    c.TT_Market_Share_target__c = ttmarketlist[0].Revenue_M_S__c ;     
                    c.TTDiscountAvailable__c = true;
                }  
                
                if(ishmarketlist.size() > 0 )  
                {  
                    c.International_S_H_ALL__c = ishmarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.International_S_H_Revenue_Target__c =ishmarketlist[0].VA_Revenue_Commitment__c ;
                    c.International_S_H_market_Share_target__c = ishmarketlist[0].Revenue_M_S__c ;    
                    c.SHDiscountAvailable__c = true;
                }  
                
                if(hkmarketlist.size() > 0 )  
                {  
                    c.Hong_Kong__c = hkmarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.HK_Renevue_Target__c = hkmarketlist[0].VA_Revenue_Commitment__c ;
                    c.HK_Market_Share_Target__c = hkmarketlist[0].Revenue_M_S__c ;   
                    c.HKDiscountAvailable__c = true;
                } 
                if(jpmarketlist.size() > 0 )   
                {  
                    c.Japan__c = jpmarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.JP_Revenue_Traget__c = jpmarketlist[0].VA_Revenue_Commitment__c ;
                    c.Japan_Market_Share_Target__c = jpmarketlist[0].Revenue_M_S__c ;   
                    c.JPDiscountsAvailable__c = true ;
                }  
                if(lonmarketlist.size() > 0 )  
                {  
                    c.London__c = lonmarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.London_Revenue_Target__c=lonmarketlist[0].VA_Revenue_Commitment__c ;
                    c.London_Market_Share_Target__c = lonmarketlist[0].Revenue_M_S__c ;
                    c.LONDiscountAvailable__c = true;
                }
                if(cnmarketlist.size() > 0 )  
                {  
                    c.China__c = cnmarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.China_Revenue_Target__c =cnmarketlist[0].VA_Revenue_Commitment__c ;
                    c.China_Market_Share_Target__c = cnmarketlist[0].Revenue_M_S__c ;  
                    c.CNDiscountAvailable__c = true ;
                }
                
                if(namarketlist.size() > 0 )  
                {  
                    c.USA_Canada_Amount__c = namarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.North_America__c =namarketlist[0].VA_Revenue_Commitment__c ;
                    c.North_America_MS__c = namarketlist[0].Revenue_M_S__c ;    
                    c.DLDiscountAvailable__c = true ;
                }
                
                if(eumarketlist.size() > 0 )  
                {  
                    c.UK_Europe_Amount__c = eumarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.UK_Europe_Revenue_Target__c =eumarketlist[0].VA_Revenue_Commitment__c ;
                    c.UK_Europe_MS__c = eumarketlist[0].Revenue_M_S__c ; 
                    if (eumarketlist[0].Name == 'UK/Europe SQ') 
                    {
                        c.SQEUDiscountAvailable__c   = true ;
                    }else if (eumarketlist[0].Name == 'UK/Europe EY') 
                    {
                        c.EYEUDiscountAvailable__c = true ;
                    }                                        
                }
                
                if(memarketlist.size() > 0 )  
                {  
                    c.Middle_East_Amount__c = memarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.Middle_East_Revenue_Target__c = memarketlist[0].VA_Revenue_Commitment__c ;
                    c.Middle_East_MS__c = memarketlist[0].Revenue_M_S__c ; 
                    c.EYMEDiscountAvailable__c = true ;
                }
                                                
                if(afmarketlist.size() > 0 )  
                {  
                    c.AFRICA_Spend__c = afmarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.Africa_revenue_Target__c = afmarketlist[0].VA_Revenue_Commitment__c ;
                    c.Africa_MS__c = afmarketlist[0].Revenue_M_S__c ; 
                    if (afmarketlist[0].Name == 'Africa SQ') 
                    {
                        c.SQAfDiscountAvailable__c   = true ;
                    } 
                    else if (afmarketlist[0].Name == 'Africa EY') 
                    {
                        c.EYAfDiscountAvailable__c = true ;
                    }
                    
                }
                if(asmarketlist.size() > 0 )  
                {  
                    c.Asia_Expenditure__c = asmarketlist[0].VA_Serviceable_Routes_p_a__c ;
                    c.Asia_Revenue_Target__c = asmarketlist[0].VA_Revenue_Commitment__c ;
                    c.Asia_MS__c = asmarketlist[0].Revenue_M_S__c ;
                    c.SQAsDiscountAvailable__c = true ;
                    
                } 
                
                c.Domestic_Contracted_Revenue_Target__c   = c.Domestic_and_Trans_Tasman_VA_Rev_Target__c +  c.Domestic_Regional_Revenue_Target__c;
                c.S_H_Annual_Contracted_Revenue_Target__c  =  c.TT_Revenue_target__c + c.International_S_H_Revenue_Target__c ;           
                c.L_H_Annual_Contracted_Revenue_Target__c  =    c.Asia_Revenue_Target__c + c.Africa_revenue_Target__c + c.Middle_East_Revenue_Target__c+
                    c.London_Revenue_Target__c+ c.China_Revenue_Target__c + c.North_America__c + 
                    c.HK_Renevue_Target__c +c.UK_Europe_Revenue_Target__c;                                
            }  
            
            
            if (totresults.size() > 0)
            {    
                for(AggregateResult ar1 : totresults)
                {
                    c.Total_Market_Spend__c =   (decimal)ar1.get('totspend')==null?0:(decimal)ar1.get('totspend');
                    c.Total_Market_Commitment__c =   (decimal)ar1.get('totcomm')==null?0:(decimal)ar1.get('totcomm');
                    
                }
            }                        
            
            lstContractUpdate.add(c);  
        }         
        update lstContractUpdate;         
    }
    
}