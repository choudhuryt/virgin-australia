global class CheckATier {

	public String invokeExternalWs(String LoyaltyNumber)  
	{   
		// check data quality
		String currentTier='';
		
		if(LoyaltyNumber <> null && LoyaltyNumber.length() >= 10){

			HttpRequest req = new HttpRequest();  
			String Value2 ='';
			req.setMethod('POST');  
			req.setEndpoint('https://soa.virginblue.com.au/LoyaltyTierMediatorOSB/proxy/v1/LoyaltyTierMediator');  
			req.setMethod('POST');  
			req.setHeader('Content-Type', 'text/xml; charset=utf-8');  
			req.setHeader('SOAPAction', 'https://api.authorize.net/soap/v1/CreateCustomerProfile');//  
			string b =   '<?xml version="1.0" encoding="utf-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:www.virginaustralia.com.au:service:partner:loyalty:LoyaltyTierMediatorMsg:v10">'+  
					'<soapenv:Header><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><wsse:UsernameToken><wsse:Username>GCCTEL</wsse:Username><wsse:Password>gcctel11</wsse:Password></wsse:UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><urn:getRQ><urn:memberID>'+LoyaltyNumber.trim() +'</urn:memberID></urn:getRQ></soapenv:Body></soapenv:Envelope>';  
			req.setBody(b);  
			Http http = new Http();  
			HTTPResponse res;
			try{
				res = http.send(req);

				XmlStreamReader reader = res.getXmlStreamReader();
				System.debug(reader.nextTag());

				//Extracting the value of the xml
				Integer safetycount = 0;
				Boolean found = false;
				while(reader.hasNext() && !found) {
					safetycount++;

					if(reader.getEventType() == XmlTag.START_ELEMENT){
						if(reader.getLocalName().equals('loyaltyTier')){

							reader.next();			
							value2 = reader.getText().trim().toUpperCase();
							System.debug('Value:' +  value2);

							if(value2.equals('R')){
								currentTier = 'Red';
								
							}   
							else if(value2.equals('S')){
								currentTier = 'Silver';
								
							}
							else if(value2.equals('G')){
								currentTier = 'Gold';
								
							}
							else if(value2.equals('P')){
								currentTier = 'Platinum';
								
							}
							else if(value2.equals('V')){
								currentTier = 'VIP';
								
							}
							
						}
					}

					// Check if we are in an infinite loop.
					if(safetycount >= 200){

						System.debug('Error: The WS response could not be parsed or too long.');
									
					}

					reader.next();
				}
			}catch(Exception e){
				//No action required (the case will remain standard)
				System.debug('Error: Could not perform Loyalty WS call');
				System.debug(e.getmessage());
			}
		}
		if(currentTier.equals('')){
			currentTier = 'Your Results have not returned a tier, Please confirm the Velocity Number is correct';
		}
	return currentTier;		
	}
	
}