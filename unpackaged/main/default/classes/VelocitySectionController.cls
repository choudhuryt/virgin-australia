/**
* This class is controller for Velocity Section vf page
* @author Son Pham
* UpdatedBy: cloudwerx (Did indentation) 
*/
public class VelocitySectionController {
    public Boolean showVelocityDetailsLink {get; set;}
    public Case caseRecord {get;set;}
    public Velocity__c velocityRecord {get;set;}
    public List<Velocity__c> listVelocity {get; set;}
    public VelocitySnapshot Snapshot {get; set;}    
    public VelocitySectionController(){}
    public Velocity__c velocitydetails{get;set;}
    /**
	* Contructs the controller with standard controller
	* @param controller CaseDetail controller
    */
    public VelocitySectionController(ApexPages.StandardController controller) {
        this.caseRecord  = (Case)controller.getRecord();
        // reload case record
        this.caseRecord = loadCaseRecord(this.caseRecord.id);
        // show Velocity Details link
        this.showVelocityDetailsLink = determineToShowVelocityDetailsLink(this.caseRecord.Velocity_Number__c);
        // query latest velocity object
        this.velocityRecord = loadLatestLinkedVelocity(this.caseRecord.Velocity_Number__c );
        // get historical velocity list
        this.listVelocity = loadSearchHistory(this.caseRecord.Velocity_Number__c, this.caseRecord.id);
    }
    
    /**
	* Loads a case record by caseId
	* @param caseId case id to be loaded
	* @return a case with Id and velocity number
	*/
    public Case loadCaseRecord(String caseId) {
        List<Case> listCase = [SELECT Id, Velocity_Number__c FROM Case WHERE Id = :caseId];
        if(listCase != null && listCase.size() > 0) {
            return listCase.get(0);
        }
        return null;
    }
    
    /**
	* Determines to show Velocity Details link or not
	* @param velocityNumber velocity number
	* @return true if velocity number exists, false otherwise
	*/
    public Boolean determineToShowVelocityDetailsLink(String velocityNumber) {
        if (velocityNumber == null || velocityNumber == '') {
            return false;
        } 
        return true;
    }
    
    /**
	* Loads the latest velocity
	* @param velocityNumber used to load velocity
	* @return Velocity__c record if any, null otherwise
	*/
    public Velocity__c loadLatestLinkedVelocity(String velocityNumber) {
        String velocityNo = '';
        String name = '';
        
        if (this.caseRecord != null){
            List<Velocity__c> listVelocity = [SELECT Velocity_Number__c, First_Name__c, Surname__c, Tier__c, Phone_Number__c,
                                              Email__c, Travel_Bank_Number__c, Point_Balance__c, CreatedDate, CreatedById 
                                              FROM Velocity__c 
                                              WHERE Case__c =:this.caseRecord.Id AND Velocity_Number__c = :velocityNumber 
                                              ORDER BY LastModifiedDate DESC LIMIT 5];
            
            // select the latest velocity
            if (listVelocity != null && listVelocity.size() > 0) {
                velocitydetails = listVelocity.get(0);                
                velocityNo = velocitydetails.Velocity_Number__c ;
                name = velocitydetails.First_Name__c + ' ' + velocitydetails.Surname__c; 
                Snapshot = new VelocitySnapshot(velocityNo, name);
                return listVelocity.get(0);
            }
        }
        return null;
    }
    
    /*
	* Loads velocity list linked to a case
	* @param velocityNumber velocity number
	* @param caseId case id which the velocity records are linking to
	* @retun a velocity list if any, an empty list otherwise
	*/
    public List<Velocity__c> loadSearchHistory(String velocityNumber, String caseId) {
        List<Velocity__c> velocityList = new List<Velocity__c>();
        velocityList = [SELECT Velocity_Number__c,Name,CreatedDate,CreatedById
                        FROM Velocity__c WHERE Case__c = :caseId ORDER BY CreatedDate DESC];
        return velocityList;
    }
    
    /**
	* Reloads velocity section when closing velocity detail popup window to make sure data is refreshed
	*/
	/*
    public void reloadVelocitySection() {
        this.caseRecord = loadCaseRecord(this.caseRecord.id);
        this.velocityRecord = loadLatestLinkedVelocity(this.caseRecord.Velocity_Number__c );
        this.loadSearchHistory(this.velocityRecord.Id, this.caseRecord.id);
    }*/
    public void reloadVelocitySection() {
        try{
            this.caseRecord = loadCaseRecord(this.caseRecord.id);
            this.velocityRecord = loadLatestLinkedVelocity(this.caseRecord.Velocity_Number__c );
            if ((this.velocityRecord != null) && (this.caseRecord != null)) {
                this.listVelocity = this.loadSearchHistory(this.velocityRecord.Velocity_Number__c, this.caseRecord.id);
            }
        } catch(Exception e) {
            String error = 'There is an exception when loading Velocity Section::' + e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
        }
    } 
}