/*
* 	Updated by Cloudwerx : removed  hardcoded references for userId & recordTypeId by referencing Utilities class
* 
* 
*/

@isTest
private class TestSMEToCorporate
{
    static testMethod void myUnitTest()
    {        
        
        Id accRecTypeId = Utilities.getRecordTypeId('Account','SmartFly');         
        Id contractRecTypeId = Utilities.getRecordTypeId('Contract','SmartFly');         
        Id caseRecTypeId = Utilities.getRecordTypeId('Case','Account Ownership/SMO/Market Segment/Change Case');       
        Id accOwnerId = Utilities.getOwnerIdByName('Virgin Australia Business Flyer');         
        Id caseOwnerId = Utilities.getOwnerIdByName('Systems Administrator');         
        
        Account account = new Account();
        CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
        account = commonObjectsForTest.CreateAccountObject(0);  
        //account.RecordTypeId ='012900000009HrPAAU';
        account.RecordTypeId = accRecTypeId;	
        account.Sales_Matrix_Owner__c = 'Accelerate';
        account.Market_Segment__c = 'SmartFly';
        //account.OwnerId = '00590000000LbNz'; 		
        account.OwnerId = accOwnerId;			
        insert account; 
        
        Test.StartTest();
        
        Contract contract = New Contract();
        contract.name ='PUTCONTRACTNAMEHERE';
        contract.AccountId =account.id;
        contract.Status ='Draft';
        contract.StartDate = Date.newInstance(2018,01,01);
        //contract.RecordTypeId ='012900000009HrU'; 
        contract.RecordTypeId = contractRecTypeId;
        insert contract;
        
        contract.Status ='Activated';
        update contract;               
        
        Case c1 = new Case(RecordTypeID = caseRecTypeId,
                           Accountid = account.id,
                           Movement_Type__c = 'Accelerate/Smartfly-Corporate',
                           New_Account_Owner1__c= caseOwnerId,
                           NEW_Market_Segment1__c='Mid Market',
                           NEW_Sales_Matrix_Owner_SMO1__c = 'NSW',
                           Change_From_Date1__c = date.Today());              
        insert c1;
        
        
        List<Id> cid = new List<Id>();        
        cid.add(c1.Id);
        SMEToCorporate.MovetoCorporate(cid);
        Test.stopTest();
        
    }        
}