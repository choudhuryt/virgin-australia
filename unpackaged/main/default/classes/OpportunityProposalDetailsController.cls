public class OpportunityProposalDetailsController 
{
    public String CurrentoppId{set;get;}  
    public String message {get;set;}
    public String oppname {get;set;}    
    public List<Market__c> marketdata {get;set;} 
    public Additional_Benefits__c addben {get;set;} 
    public List <Opportunity> opp {get;set;} 
    public List <Opportunity> Opports {get;set;}
    
    public OpportunityProposalDetailsController() 
    {
        if(CurrentoppId == null)
        {
            CurrentoppId= ApexPages.currentPage().getParameters().get('id');
        }
    }
    
    public PageReference initDisc() 
    {  
        
        if(CurrentoppID == null)
        {
            CurrentoppId= ApexPages.currentPage().getParameters().get('id');
        }      
        Opports =  [Select JAPAN__c,LondonJapan__c,SQs__c from Opportunity where
                    id = :ApexPages.currentPage().getParameters().get('id')
                   ];
        
        opp =         [Select Name from Opportunity where
                       id = :ApexPages.currentPage().getParameters().get('id')
                      ];
        if(opp.size() > 0)
        {
            oppname = opp[0].name ;  
        }
        marketdata = [
            SELECT Carrier__c,Comments__c,Contract__c,Guideline_Tier__c,
            Id,Name,Requested_Tier__c,Revenue_M_S__c,VA_Revenue_Commitment__c,
            VA_Serviceable_Routes_p_a__c,Market__c
            FROM Market__c
            where Opportunity__c =:ApexPages.currentPage().getParameters().get('id')
        ];
        
        
        List<Additional_Benefits__c> addbendetails = [
            SELECT Brand_Connections_Payment_Frequency__c,Brand_Connections_Payment_Type__c,
            Brand_Connections__c,Contract__c,Gold__c,Id,Joining_Fee__c, Virgin_Australia_Beyond__c,
            Lounge_joining_fee_waived_for_60_days__c,Lounge_Membership_Tier__c,Lounge_Membership__c,
            Membership_Fee__c,Name,Opportunity__c,Pilot_GD_Campaign_Offered__c,
            Pilot_GD_Campaign_Term__c,Pilot_Gold__c,Platinum__c,Silver__c,Status_Match_Offered__c,
            Status_Match_Term__c,Ticket_Fund_Payment_Frequency__c,
            Ticket_Fund_Payment_Type__c,Ticket_Fund__c,Velocity_Accelerations_Offered__c,
            Velocity_Acceleration_Frequency__c,Ticket_Fund_Perf_Based__c,
            Ticket_Fund_Perf_Based_Frequecy__c,Ticket_Fund_Sign_On_Bonus__c,
            Ticket_Fund_Sign_On_Bonus_Frequency__c,Additional_Notes__c,Red_Circle_Notes__c
            FROM Additional_Benefits__c
            where Opportunity__c =:ApexPages.currentPage().getParameters().get('id')
        ];
        
        
        
        if(addbendetails.size() == 0)
        {
            message = '***   No details available.   ***';
        }else{
            addben = addbendetails.get(0);
        }
        
        
        return null;
    }
    
    public PageReference DOM() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.OpportunityWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentoppId);  
        newPage.getParameters().put('Region', 'DOMREG'); 
        return newPage ;
    } 
    
    public PageReference INSH() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.OpportunityWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentoppId);  
        newPage.getParameters().put('Region', 'INTSH'); 
        return newPage ;
    } 
    
    public PageReference NA() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.OpportunityWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentoppId);  
        newPage.getParameters().put('Region', 'NA'); 
        return newPage ;
    } 
    public PageReference LON() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.OpportunityWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentoppId);  
        newPage.getParameters().put('Region', 'LONDON'); 
        return newPage ;
    } 
    public PageReference EY() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.OpportunityWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentoppId);  
        newPage.getParameters().put('Region', 'EY'); 
        return newPage ;
    } 
    public PageReference SQ() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.OpportunityWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentoppId);  
        newPage.getParameters().put('Region', 'SQ'); 
        return newPage ;
    }
    //Willis_13.09.2022: Added for values UA and QR
    public PageReference UA(){
        PageReference newPage;
        newPage = Page.OpportunityWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentoppId);  
        newPage.getParameters().put('Region', 'UA');
        return newPage;        
    }    
    public PageReference QR(){
        PageReference newPage;
        newPage = Page.OpportunityWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentoppId);  
        newPage.getParameters().put('Region', 'QR');
        return newPage;
    }
    
    public PageReference RED() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.OpportunityWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentoppId);  
        newPage.getParameters().put('Region', 'RED'); 
        return newPage ;
    } 
    
    public PageReference cancel() {
        // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
        PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
        //
        thePage.setRedirect(true);
        //
        return thePage;
        
        
        
    }
}