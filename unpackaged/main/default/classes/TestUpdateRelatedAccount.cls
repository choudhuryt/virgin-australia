@isTest
public class TestUpdateRelatedAccount 
{
public static testMethod void testUpdateRelatedAccountEvent ()
    {
        Test.startTest();
        List<Account> acctList = new List<Account>();
        Account accountToTest = new Account();
        accountToTest.Account_Type__c = 'Industry';
        accountToTest.Name = 'Test Name';
        accountToTest.Billing_Country_Code__c = 'AU';
        accountToTest.BillingStreet = 'Test Street';
        accountToTest.BillingState = 'AU';
        accountToTest.BillingPostalCode = '12345';
        accountToTest.BillingCountry = 'USA';
        accountToTest.BillingCity = 'Test City';
        accountToTest.Industry_Type__c = 'Hospitality & Tourism';
        accountToTest.Market_Segment__c = 'Accelerate';
        accountToTest.Lounge__c = TRUE;
        accountToTest.Pilot_Gold_Email__c = 'testgoldmail@yahoo.com';
        accountToTest.Pilot_Gold_Email_Second_Nominee__c = 'testgoldmailsec@yahoo.com';
        acctList.add(accountToTest);
        insert acctList;
        
        
        
        Contact contactTest = new Contact();
        contactTest.LastName = 'TestLast Contact';
        contactTest.FirstName  = 'TestFirst Contact';
        contactTest.Key_Contact__c  = TRUE;
        contactTest.Email = 'testContact@yahoo.com';        
        contactTest.Title = 'Test Title';
        contactTest.Phone = '123456';
        contactTest.Velocity_Number__c = '2105327372';
        contactTest.RecordTypeId = '012900000007krgAAA';
        contactTest.AccountId = accountToTest.Id;
        insert contactTest; 
        
        
        Event even = new Event() ;
        even.Who__c = contacttest.id;
        even.WhoId = contacttest.id;
        even.RecordTypeId = '0126F00000174ThQAI';
        even.Subject = 'TEST';
        even.DurationInMinutes = 10 ;
        even.ActivityDateTime = System.now() ;
        insert even ;
        
        List<Id> eid = new List<Id>();        
        eid.add(even.Id);
        UpdateRelatedAccount.UpdateRelatedAccountdetails(eId);
        
        Test.stopTest();
        
    }
}