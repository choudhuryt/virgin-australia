public class SmartFlyTourcodeUpdateOnContact
{ 
@InvocableMethod
public static void UpdateSmartFlyTourcodeOnContact(List<Id> Accids ) 
{
    
    List<Account> accountlist =  [Select Id , Business_Number__c from Account where id =:accIds ]; 
    
    Contract contractSmartFly= new Contract();

    contractSmartFly = (Contract) [select id,Status from Contract where Accountid = :Accids  and  contract.RecordTypeId = '012900000009HrUAAU' limit 1];
        
         Tourcodes__c SmartFlyCode = new Tourcodes__c();
         SmartFlyCode.Account__c  = accountlist[0].id;
         SmartFlyCode.Contract__c = contractSmartFly.id;
         SmartFlyCode.Status__c   = 'Active';
         SmartFlyCode.Tourcode__c = accountlist[0].Business_Number__c ;
         SmartFlyCode.Tourcode_Purpose__c = 'Smartfly Tourcode';
         SmartFlyCode.Tourcode_Effective_Date_From__c = Date.today();
         insert SmartFlyCode;


}
}