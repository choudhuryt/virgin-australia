/*
 * Updated by Cloudwerx : Removed references to hardcoded recordtypeids & user record id 
 * 
 * 
 * 
 */
@isTest
private class TestParentAccountChange
{
    static testMethod void myUnitTest()
    {
        User userRec = [SELECT Id, Name FROM User WHERE FirstName = 'Systems' AND LastName = 'Administrator'];
        Id accountCorporateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id caseAccChangeRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Account Hierarchy /Parent Account Change Case').getRecordTypeId();
        

        Test.StartTest();
        
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.Sales_Matrix_Owner__c = 'VIC';
        account.Market_Segment__c = 'Large Market';
        account.OwnerId = userRec.Id; 
        account.Account_Lifecycle__c = 'Contract';
        account.RecordTypeId = accountCorporateRecTypeId;	
        insert account;
        
        Account paccount = new Account();
        CommonObjectsForTest commp = new CommonObjectsForTest();
        paccount = commp.CreateAccountObject(0);
        paccount.Sales_Matrix_Owner__c = 'VIC';
        paccount.Market_Segment__c = 'Large Market';
        paccount.OwnerId = userRec.Id; 
        paccount.Account_Lifecycle__c = 'Contract';
        paccount.RecordTypeId = accountCorporateRecTypeId;	
        insert paccount;
        
        Case c1 = new Case(RecordTypeID = caseAccChangeRecTypeId,
                           Accountid = account.id ,
                           NEW_Parent_Account1__c = paccount.id,
                           Change_From_Date1__c = date.Today()) ;    
        
        insert c1;
        
        
        List<Id> cid = new List<Id>();        
        cid.add(c1.Id);
        ParentAccountChange.ParentAccountUpdate(cid) ;
        
        Test.StopTest();    
        
    }
}