public with sharing class PrismContractDomDataController {
public String CurrentContractId{set;get;}  
public String CurrentCPId{set;get;} 
public List<Contract_Performance__c> domregsearchResults {get;set;}
public List<Contract_Performance__c> cplist = new List<Contract_Performance__c>();

    
private Contract a;  
    
   public PrismContractDomDataController() 
    {
        if(CurrentContractId == null)
		{
			CurrentContractId= ApexPages.currentPage().getParameters().get('cid');
		}
		if(CurrentCPId == null)
		{
			CurrentCPId= ApexPages.currentPage().getParameters().get('cpid');
		}
        
    }
   public PageReference initDisc() 
     {  
      if(CurrentContractId == null)
		{
			CurrentContractId= ApexPages.currentPage().getParameters().get('cid');
		}   
        if(CurrentCPId == null)
		{
			CurrentCPId= ApexPages.currentPage().getParameters().get('cpid');
		} 
      
         Contract_Performance__c cp = [SELECT Id, Contract_Term__c,Contract__c, Measure__c FROM Contract_Performance__c  WHERE id = : CurrentCPId];
       
         system.debug('Inside the prism init' + CurrentContractId +  cp.Contract_Term__c );
         
        cplist = [ SELECT id,Contract_Term__c,Profile_Month__c,To_Date_Amount_Share__c,
                                To_Date_Market_Share__c,To_Date_Performace_Amount__c,Requirement__c,
                             	Measure__c,Variance__c,Fulfilled__c,Reporting_Period__c,Current_Month_Amount_Share__c,
                                Current_Month_Market_Share__c,Current_Month_Performance_Amount__c 
                                    FROM Contract_Performance__c where 
                                    Contract__c = :CurrentContractId
                                    and  Contract_Term__c  = :cp.Contract_Term__c
                                   and Start_Date__c <= TODAY 
                       order by Profile_Month__c
                         ]   ;
         
       system.debug('Inside the prismlist' +cplist );
         
         if (cplist.size() > 0 )
        {
         domregsearchResults =  cplist ;   
        }
         
      
          return null ; 
     }

    
     public List<Data> getData() {
         
        system.debug('Inside the prism chart' + CurrentCPId );
        if (CurrentCPId <> null)
        {    
        return PrismContractDomDataController.getDomChartData();
       system.debug('The data from get data' +PrismContractDomDataController.getDomChartData()  );    
        }
        
        return null ; 
         
    }

    // The actual chart data; needs to be static to be
    // called by a @RemoteAction method
    // 
    
   public static List<Data> getDomChartData() 
   
   {
         string CurrentContractId= ApexPages.currentPage().getParameters().get('cid');
         string CurrentCPId= ApexPages.currentPage().getParameters().get('cpid');
         Contract_Performance__c cp = [SELECT Id, Contract_Term__c,Contract__c, Measure__c FROM Contract_Performance__c  WHERE id = : CurrentCPId];
       
         Contract_Performance__c conperf = new Contract_Performance__c();
         
             
         List<Data> data = new List<Data>(); 
        
       if (cp.Contract_Term__c <> null &&  cp.Measure__c == 'Amount')
        {  
            
    
            
        List<aggregateResult>   searchResultsdom = [
                            select  Profile_Month__c,AVG(Current_Month_Performance_Amount__c)cmpa,AVG(To_Date_Performace_Amount__c)tdpa,AVG(Requirement_Amount__c)req	
                            from Contract_Performance__c where
                            Contract__c =:CurrentContractId and 
                            Contract_Term__c =: cp.Contract_Term__c
                            and Start_Date__c <= TODAY 
                           GROUP BY Profile_Month__c
                            order by Profile_Month__c ASC
                             ];
            
            
            for(AggregateResult ar1 : searchResultsdom)
            {    
            data.add(new Data(String.valueOf(ar1.get('Profile_Month__c')),(decimal)ar1.get('req'),(decimal)ar1.get('cmpa'),(decimal)ar1.get('tdpa')) );
            }
     
            
        } else if (cp.Contract_Term__c <> null && cp.Measure__c == 'Share of Amount')
            
            
        {
            
           List<aggregateResult>   searchResultsdom = [
                            select  Profile_Month__c,AVG(Current_Month_Amount_Share__c)cmpa,AVG(To_Date_Amount_Share__c)tdpa,AVG(Requirement_Percentage__c)req	
                            from Contract_Performance__c where
                            Contract__c =:CurrentContractId and 
                            Contract_Term__c =: cp.Contract_Term__c
                            and Start_Date__c <= TODAY 
                           GROUP BY Profile_Month__c
                            order by Profile_Month__c ASC
                             ];
            
            
            for(AggregateResult ar1 : searchResultsdom)
            {    
            data.add(new Data(String.valueOf(ar1.get('Profile_Month__c')),(decimal)ar1.get('req'),(decimal)ar1.get('cmpa'),(decimal)ar1.get('tdpa')) );
            } 
        }else if (cp.Contract_Term__c <> null && cp.Measure__c == 'No Requirement')
            
            
        {
            
           List<aggregateResult>   searchResultsdom = [
                            select  Profile_Month__c,AVG(Current_Month_Amount_Share__c)cmpa,AVG(To_Date_Amount_Share__c)tdpa,AVG(Requirement_Percentage__c)req	
                            from Contract_Performance__c where
                            Contract__c =:CurrentContractId and 
                            Contract_Term__c =: cp.Contract_Term__c
                            and Start_Date__c <= TODAY 
                           GROUP BY Profile_Month__c
                            order by Profile_Month__c ASC
                             ];
            
            
            for(AggregateResult ar1 : searchResultsdom)
            {    
            data.add(new Data(String.valueOf(ar1.get('Profile_Month__c')),(decimal)ar1.get('req'),(decimal)ar1.get('cmpa'),(decimal)ar1.get('tdpa')) );
            }    
            
            
        }    
            
            
        
        system.debug('The data returned' + data);
        return data;       
    }
    
    
      
   
    
        
   
    
    
   public class Data {
        public String name { get; set; }      
        public decimal Target { get; set; }
        public decimal CurrentActual { get; set; }
        public decimal ToDateActual { get; set; }
       
      public Data(String name, DECIMAL Target,DECIMAL CurrentActual,DECIMAL ToDateActual) {
            this.name = name;           
            this.Target = Target;
            this.CurrentActual = CurrentActual;  
           this.ToDateActual = ToDateActual;  
        }
    }
    
       public PageReference cancel() {
   // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
      PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('cid'));
                  //
                  thePage.setRedirect(true);
                  //
                  return thePage;
   
  
   
  }
     public PageReference cancel1() {
      PageReference newPage;
     newPage = Page.PRISMContractDataView;
     newPage.getParameters().put('cid', ApexPages.currentPage().getParameters().get('cid')); 
     newPage.setRedirect(true);
     return newPage;  
   
  }

}