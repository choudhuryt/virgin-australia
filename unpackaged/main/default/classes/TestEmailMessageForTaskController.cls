@isTest
public class TestEmailMessageForTaskController 
{
    
 static testMethod void myUnitTest1() 
    {
        
	Test.startTest();	
        
         Case c = new Case(SuppliedEmail='jdoe_test_test@doe.com',
                                  SuppliedName='John Doe',
                                 Velocity_Member__c = false,
                                 Subject='Feedback - Something',
                                 Contact_First_Name__c='Tom',
                               Contact_Last_name__c = 'Johns');
        insert c ;
            
        
     Task t = new Task(Subject ='Email Test',
                                WhatId = c.id,
                                Recordtypeid  = '012900000007fzkAAA' ,
                               TaskSubtype='Email');
        insert t ;
        
        
       EmailMessage newEmail = new EmailMessage(
            FromName = 'testfrom@gmail.com',
            ToAddress = 'testto@gmail.com',
            Subject = 'Test Email',
            TextBody = 'Test',
            ActivityId = t.id,
            ParentId = c.Id); 
        
            insert newEmail; 
        
        
        PageReference  ref1 = Page.EmailMessageForTask;
        ref1.getParameters().put('id', t.Id);
        Test.setCurrentPage(ref1);
        ApexPages.StandardController conL = new ApexPages.StandardController(t);
        EmailMessageForTaskController lController = new EmailMessageForTaskController(conL);
        Test.stopTest();
        
    }
    
    
    
}