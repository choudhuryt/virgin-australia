//NOTE: Some of the following code is copied from existing code that is not being modified. It has some major issues
// such as not being bulkified, running soql and dml inside loops, unneccessary casting and assignments,
// hardcoded IDs and self querying to update self etc
//
// We have only added the duplicate checker
// to allow our code to run properly. The rest is simply consolidated from multiple triggers into a single
// Account master trigger.
// 
// Updated by Cloudwerx : 
// 	Removed hardcoded references for RecordTypeId & ownerId. Also, Removed query from For loop
// 


public class AccountTriggerHandler 
{
    
    public static final String OPEN_BRACKETS = '[';
    public static final String CLOSE_BRACKETS = ']. ';
    public static final String SEPARATOR = ' : ';
    
    public static final String RECORDTYPE_CORPORATE = 'Corporate';
    public static final String RECORDTYPE_ACCELERATE = 'Accelerate';
    public static final String RECORDTYPE_SMARTFLY = 'SmartFly';
    public static final String RECORDTYPE_VA_ACC = 'VA Account';
    
    public static final String MSG_DUPLICATE_ABN = 'Duplicate ABN ';
    public static final String MSG_DUPLICATE_NAME = 'Duplicate Account Name / Company ';
    public static final String MSG_DUPLICATE_DOMAIN = 'Duplicate Domain of Administrator Contact Email ';
    public static final String MSG_DUPLICATE_WEBSITE = 'Duplicate Website';
    
    
    //9/12/2015 COpied from SetAccountAccountOwner
    //No optimisations/improvements have been done due to time constraints
    public static void setWaiverParentPoints( Boolean isInsert, List<Account> accountsInTrigger, Map<Id, Account> oldAccountMap )
    {                
        Set<Id> wpaccIds  = new Set<Id>(); 
        
        List<Account> wpAccUpdate = new List<Account>(); 
        
        for( Account accountItr: accountsInTrigger ) 
        {
            Account oldAccount = oldAccountMap.get(accountItr.ID);
            
            if   (accountItr.Waiver_Parent_Account__c != null)
            {
                wpaccIds.add(accountItr.Waiver_Parent_Account__c);
            }            
        }
        
        List<Account> wpaccList =  [select id , Waiver_Points_Allocated__c, Waiver_Points_Used__c  from  Account where Id =:wpaccIds];    
        
        if (wpaccList.size() > 0)
        {
            for(Account wpacc: wpaccList)
            { 
                
                AggregateResult[] wpwaiverpointallocated = [SELECT SUM(Waiver_Points_Allocated__c) allocsum FROM Account
                                                            where Waiver_Parent_Account__c= :wpacc.id  ];
                
                
                AggregateResult[] wpwaiverpointused = [SELECT SUM(Waiver_Points_Used__c) usedsum FROM Account
                                                       where Waiver_Parent_Account__c = :wpacc.id  ];  
                
                AggregateResult[] wpwaiverpointpending = [SELECT SUM(Waiver_Points_Pending_Approval__c) pendsum FROM Account
                                                          where Waiver_Parent_Account__c = :wpacc.id  ]; 
                
                wpacc.Waiver_Points_Allocated__c = (decimal)wpwaiverpointallocated[0].get('allocsum')   ; 
                wpacc.Waiver_Points_Used__c = (decimal)wpwaiverpointused[0].get('usedsum')   ; 
                wpacc.Waiver_Points_Pending_Approval__c   = (decimal)wpwaiverpointpending[0].get('pendsum')   ; 
                
                wpAccUpdate.add(wpacc);    
            }     
            update   wpAccUpdate ;
            
        }    
        
    }
    
    public static void updateEmpowerPassword( Boolean isInsert, List<Account> accountsInTrigger, Map<Id, Account> oldAccountMap )
    {   
        for( Account accountItr: accountsInTrigger ) 
        { 
            if( accountItr.Empower_Password__c != NULL)   
            {
                accountItr.Password__c =    accountItr.Empower_Password__c ;
            }   
        }
    } 
    
    public static void updateoldABN( Boolean isInsert, List<Account> accountsInTrigger, Map<Id, Account> oldAccountMap )
    {   
        Set<Id> accIds  = new Set<Id>();
        
        Id accountAccelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accelerate').getRecordTypeId();
        Id ContractAcceleratePOSRecTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Accelerate POS/Rebate').getRecordTypeId();
        
        Id accountSmartFlyRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('SmartFly').getRecordTypeId();
        Id ContractSmartFlyRecTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('SmartFly').getRecordTypeId();
        
        Id accountFlyPlusRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('FlyPlus').getRecordTypeId();        
        Id ContractFlyPlusRecTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('FlyPlus').getRecordTypeId();        
        
        String businessnum ='';
        
        if( !isInsert )   
        {
            for( Account accountItr: accountsInTrigger ) 
            {                 
                Account oldAccount = oldAccountMap.get(accountItr.ID);  
                
                if( oldAccount.Business_Number__c <> accountItr.Business_Number__c) 
                {
                    if (oldAccount.Business_Number__c != NULL )
                    {
                        accountItr.Old_Business_Number__c = oldAccount.Business_Number__c ;
                        
                        if ( accountItr.recordtypeid == accountAccelerateRecTypeId || 
                            accountItr.recordtypeid == accountSmartFlyRecTypeId || 
                            accountItr.recordtypeid == accountFlyPlusRecTypeId )
                        {
                            accIds.add(accountItr.Id);
                            businessnum= accountItr.Business_Number__c ;
                        }
                    }
                    else
                    {
                        accountItr.Old_Business_Number__c = ' '  ;  
                    } 
                }    
            }
            
            List<Account> accList = [select id , Business_Number__c,recordtypeid from  Account where  id  =:accIds]; 
            List<Tourcodes__c> accountTCList = new List<Tourcodes__c>();
            List<contact> lstConUpdate = new List<Contact>(); 
            boolean tourcodecreated = false; 
            List<Contract> contractfortourcode = new List<Contract>(); 
            
            if(acclist.size() > 0  &&  tourcodecreated== false)                
            {
                String tourcodetype ='';
                
                if(acclist[0].recordtypeid == accountAccelerateRecTypeId)
                {
                    tourcodetype ='ABN Number';
                    contractfortourcode =  [select id,Status from Contract where Accountid = :Accids and contract.RecordTypeId =: ContractAcceleratePOSRecTypeId limit 1];                    
                }else if ( acclist[0].recordtypeid == accountSmartFlyRecTypeId)
                {
                    tourcodetype ='Smartfly Tourcode';
                    contractfortourcode =  [select id,Status from Contract where Accountid = :Accids  and  contract.RecordTypeId =: ContractSmartFlyRecTypeId limit 1];
                }else if ( acclist[0].recordtypeid == accountFlyPlusRecTypeId)
                {
                    tourcodetype ='FlyPlus Tourcode';
                    contractfortourcode = [select id,Status from Contract where Accountid = :Accids  and contract.RecordTypeId =: ContractFlyPlusRecTypeId limit 1];
                }
                
                Tourcodes__c accCode = new Tourcodes__c();   
                accCode.Account__c  = acclist[0].id;
                accCode.Status__c   = 'Active';
                accCode.Tourcode__c = businessnum ;
                accCode.Tourcode_Purpose__c =  tourcodetype ;
                accCode.Tourcode_Effective_Date_From__c = Date.today();   
                if(contractfortourcode.size() > 0)    
                {
                    accCode.Contract__c = contractfortourcode[0].id;
                }
                accountTCList.add(accCode);  
                tourcodecreated = true;
            }
            
            if(accountTCList.size()>0)
            {
                insert accountTCList;
            }
            
            if(acclist.size() > 0 && (acclist[0].recordtypeid == accountSmartFlyRecTypeId || 
                                      acclist[0].recordtypeid == accountFlyPlusRecTypeId)) 
            {
                List<Contact> conlist =  [Select Id, Account_ID__c,SmarftFly_Tourcode__c from Contact where  AccountId = :acclist[0].id  and SmartFly_Contact_Status__c  includes ('Key Contact') order by createddate desc limit 1 ];
                if(conlist.size() > 0)
                {
                    for(Contact con: conlist)
                    {
                        con.SmarftFly_Tourcode__c = businessnum ;
                        lstConUpdate.add(con);
                    }
                    update lstConUpdate;  
                }     
            }         
        }   
    }  
    
    public static void setrelatedtmcparent(  List<Account> accountsInTrigger, Map<Id, Account> oldAccountMap )
        
    {  
        if (!triggerupdatehelper.hasAlreadyupdatedtrigger()) 
        {    
            Set<Id> accIds  = new Set<Id>(); 
            Set<Id> tmcIds  = new Set<Id>(); 
            List<Account> lstAccUpdate = new List<Account>(); 
            
            for( Account accountItr: accountsInTrigger ) 
            {
                
                {
                    tmcIds.add(accountItr.Main_Corporate_TMC__c) ;   
                    accIds.add(accountItr.ID)  ; 
                }
                
            }  
            List<Account> accList    =  [select id , Related_Travel_Agent_Ultimate_Parent__c  from Account where Id =:accIds  limit 1];  
            List<Account> tmcaccList =  [select id , Ultimate_Parent_Account_Name__c  from Account where Id =:tmcIds limit 1 ]; 
            
            if  (acclist.size() > 0 )
            {
                for(Account acc: acclist)
                {
                    if(tmcaccList.size()>0)
                    {  
                        acc.Related_Travel_Agent_Ultimate_Parent__c  = tmcaccList[0].Ultimate_Parent_Account_Name__c;           
                        
                    } else
                    {  
                        acc.Related_Travel_Agent_Ultimate_Parent__c  = '';           
                        
                    }     
                    
                    lstAccUpdate.add(acc);
                } 
                triggerupdatehelper.settriggerupdated(); 
                update lstAccUpdate;    
            }      
        }    
    } 
    
    public static void setAccountOwner( Boolean isInsert, List<Account> accountsInTrigger, Map<Id, Account> oldAccountMap )
    {
        // Detect Accounts with a changed ownerID and copy the ownerID over to  
        // Account_Owner__c.
        Boolean accountChanged=false;
        
        User userRec = [SELECT Id, Name FROM User WHERE FirstName = 'Virgin Australia' AND LastName = 'Business Flyer'];
        
        for( Account accountItr: accountsInTrigger ) 
        {
            accountChanged=false;
            
            if( !isInsert ) 
            {  
                Account oldAccount = oldAccountMap.get(accountItr.ID);
                if( accountItr.Account_Lifecycle__c != null && accountItr.Account_Lifecycle__c.equals('Inactive') 
                   && (!oldAccount.Account_Lifecycle__c.equals('Inactive')) )
                {
                    Date d= Date.today();
                    
                    if(!accountItr.Name.contains('CLOSED'))
                    {
                        accountItr.Name = accountItr.Name + ' ***CLOSED***';  
                    }
                    
                    if(accountItr.PRISM_ID__c <> null)
                    {
                        accountItr.PRISM_ID__c = '';
                    }
                    
                    List<Data_Validity__c> dvList = new List<Data_Validity__c>();
                    List<Data_Validity__c> dvListToUpdate = new List<Data_Validity__c>();
                    List<Data_Validity__c> dvListToinsert = new List<Data_Validity__c>();
                    
                    dvList = [select id,To_Date__c,RecordTypeid,AGY__c,From_Account__c,From_Date__c,
                              Tourcode__c,TIDS__c,PCC__c,IATA__c,Soft_Delete__c from 
                              Data_Validity__c where To_Date__c =null and From_Account__c=:accountItr.id and Soft_Delete__c = false  ];
                    
                    for (Integer g =0; g < dvList.size();g++ )
                    {
                        Data_Validity__c dv = new Data_Validity__c();
                        dv = dvList.get(g);
                        dv.To_Date__c = d;
                        dvListToUpdate.add(dv);
                        Data_Validity__c dvi = new Data_Validity__c();  
                        dvi.From_Account__c  =  dvList[g].From_Account__c ;
                        dvi.From_Date__c  =  d.AddDays(1); 
                        dvi.RecordTypeid  =  dvList[g].RecordTypeid ;
                        dvi.AGY__c  =  dvList[g].AGY__c ;
                        dvi.Tourcode__c  =  dvList[g].Tourcode__c ;
                        dvi.IATA__c  =  dvList[g].IATA__c ;
                        dvi.TIDS__c  =  dvList[g].TIDS__c ;
                        dvi.PCC__c =  dvList[g].PCC__c ;
                        dvi.To_Date__c =  null ;  
                        dvi.Soft_Delete__c = true ;
                        dvListToinsert.add(dvi) ;
                    }
                    
                    if(dvListToUpdate.size() > 0)
                    {
                        update dvListToUpdate;
                    }
                    
                    if(dvListToinsert.size()  > 0)
                    {
                        insert dvListToinsert;
                    }
                    
                    UpdateInactiveAccountValidity.UpdateTheAccount(accountItr.id);	
                    UpdateInactiveAccountValidity.UpdateTheContract(accountItr.id);
                }
                
                //Stevie babys code
                if (accountItr.ownerID != oldAccount.ownerID) 
                {
                    accountChanged=true;
                }
            } 
            else 
            {               
                //Need an if to deterine if this is converted from a lead
                //If the account is converted from the lead,
                
                if(accountItr.Agreed_To_Terms_And_Conditions__c == true  && accountItr.Accelerate_Batch_Update__c ==false)
                {     
                    accountItr.Sales_Matrix_Owner__c = 'Accelerate';
                    accountItr.OwnerId = userRec.Id;
                    accountItr.Account_Owner__c = userRec.Id;
                    accountItr.Velocity_Pilot_Gold_Available__c = 2;
                    accountItr.New_Accelerate_Account__c = true;
                    accountItr.Sales_Support_Group__c = userRec.Id;
                    //AccelerateUpdateAccount.UpdateTheAccount(accountItr.Name);
                }    
                //
                accountChanged=true;            
            }           
            // Copy field.
            if(accountChanged) 
            {
                accountItr.Account_Owner__c = accountItr.ownerID;
            }
        }
    }
    
    //9/12/2015 COpied from CreateDataValidityTrigger
    //No optimisations/improvements have been done due to time constraints
    public static void createDataValidity( List<Account> accountsInTrigger )
    {
        
        Date d = Date.newinstance(2001, 12, 31);
        
        Id vaAccountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('VA Account').getRecordTypeId();
        Id accelerateAccountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accelerate').getRecordTypeId();
        
        List<Account_Data_Validity__c> accountDVList = new List<Account_Data_Validity__c>();
        String SMO = '';
        String MarketSegment ='';
        
        Set<Id> ownerIds = new Set<Id>();
        List<User> userList = new List<User>();
        
        for(Account accObj : accountsInTrigger){
            ownerIds.add(accObj.ownerId);
        }
        
        userList = [Select Id, SMO__C from User where id IN: ownerIds];
        
        for( Account account : accountsInTrigger )
        {
            SMO ='';
            MarketSegment = '';
            if( account.recordtypeid == vaAccountRecTypeId || account.recordtypeid == accelerateAccountRecTypeId)
            {
                SMO ='Accelerate';
                MarketSegment ='Accelerate';
            }
            else
            {
                // Removed beow query from from loop & added before the for loop
                //User u = new User();
                //u = [Select id,SMO__C from User where id =:account.ownerid];
                
                for(User u : userList){
                    if(u.SMO__C != null)
                    {
                        SMO = u.smo__c;
                    }
                    else
                    {
                        SMO = account.billingstate;
                    }
                }                
            }
            
            Account_Data_Validity__c accountDV = new Account_Data_Validity__c();   
            accountDV.Account_Owner__c = account.ownerid;
            accountDV.Account_Record_Type__c = account.recordtypeid;
            accountDV.From_Account__c = account.id; 
            accountDV.Parent_Account__c = account.Parentid;
            accountDV.From_Date__c = d;
            
            if( account.Sales_Matrix_Owner__c != null )
            {
                accountDV.Sales_Matrix_Owner__c = account.Sales_Matrix_Owner__c;  
            }
            else
            {
                accountDV.Sales_Matrix_Owner__c = SMO;  
            }
            
            if( account.Market_Segment__c != null )
            {
                accountDV.Market_Segment__c = account.Market_Segment__c;
            }
            else
            {
                accountDV.Market_Segment__c =MarketSegment;  
            }
            
            accountDVList.add(accountDV);
        }
        if(accountDVList.size()>0)
        {
            insert accountDVList;
        }
    }
    
    public static void markDuplicatesV2( List<Account> accountList ) {
        Set<String> abnSet = new Set<String>();
        Set<String> nameSet = new Set<String>();
        Set<String> emailDomainSet = new Set<String>();
        Set<String> webDomainSet = new Set<String>();
        
        String[] RT_LIST = new String[] {'Corporate', 'Accelerate', 'VA_Account', 'SmartFly', 'Government'};
            Set<String> sfdcIgnoreEmailDomainSet = new Set<String>();
        List<Ignore_Email_Domains__c> emailDomains = Ignore_Email_Domains__c.getAll().values();
        for (Ignore_Email_Domains__c emailDomain : emailDomains) {
            sfdcIgnoreEmailDomainSet.add(emailDomain.Name);
        }
        
        for(Account acc: accountList) {
            acc.Potential_Duplicate__c = false;
            acc.Duplicate_Notes__c = '';
            
            if(String.isNotEmpty(acc.Business_Number__c)) {
                if(abnSet.contains(acc.Business_Number__c)) {
                    acc.Potential_Duplicate__c = true;
                    acc.Duplicate_Notes__c += MSG_DUPLICATE_ABN + OPEN_BRACKETS + acc.Business_Number__c + CLOSE_BRACKETS + '\n';
                } else {
                    abnSet.add(acc.Business_Number__c);
                }
            }
            
            if(String.isNotEmpty(acc.Name)) {
                if(nameSet.contains(acc.Name.trim())) {
                    acc.Potential_Duplicate__c = true;
                    acc.Duplicate_Notes__c += MSG_DUPLICATE_NAME + OPEN_BRACKETS + acc.Name.trim() + CLOSE_BRACKETS + '\n';
                } else {
                    nameSet.add(acc.Name.toUpperCase().trim());
                    nameSet.add(acc.Name.toUpperCase().trim() + ' ***CLOSED***');
                }
            }
            
            if(String.isNotEmpty(acc.Email_Domain__c)) {
                if(!sfdcIgnoreEmailDomainSet.contains(acc.Email_Domain__c)) {
                    if(emailDomainSet.contains(acc.Email_Domain__c)) {
                        acc.Potential_Duplicate__c = true;
                        acc.Duplicate_Notes__c += MSG_DUPLICATE_DOMAIN + OPEN_BRACKETS + acc.Email_Domain__c + CLOSE_BRACKETS + '\n';
                    } else {
                        emailDomainSet.add(acc.Email_Domain__c);
                    }
                }
            }
            
            if(String.isNotEmpty(acc.Web_Domain__c)) {
                if(!'NO'.equalsIgnoreCase(acc.Web_Domain__c.trim()) && !'NA'.equalsIgnoreCase(acc.Web_Domain__c.trim())) {
                    if(webDomainSet.contains(acc.Web_Domain__c)) {
                        acc.Potential_Duplicate__c = true;
                        acc.Duplicate_Notes__c += MSG_DUPLICATE_WEBSITE + OPEN_BRACKETS + acc.Web_Domain__c + CLOSE_BRACKETS + '\n';
                    } else {
                        webDomainSet.add(acc.Web_Domain__c);
                    }
                }
            }
        }
        
        Set<String> abnDSet = new Set<String>();
        Set<String> nameDSet = new Set<String>();
        Set<String> emailDomainDSet = new Set<String>();
        Set<String> webDomainDSet = new Set<String>();
        
        for(Account account: [SELECT Id, Name, Business_Number__c, Email_Domain__c, Web_Domain__c
                              FROM Account
                              WHERE RecordType.DeveloperName IN :RT_LIST
                              AND (Name IN :nameSet OR Business_Number__c IN :abnSet OR Email_Domain__c IN :emailDomainSet OR Web_Domain__c IN : webDomainSet)]) {
                                  abnDSet.add(account.Business_Number__c);
                                  nameDSet.add(account.Name.removeEnd(' ***CLOSED***'));
                                  emailDomainDSet.add(account.Email_Domain__c);
                                  webDomainDSet.add(account.Web_Domain__c);
                              }
        
        for(Account acc: accountList) {
            if(String.isNotEmpty(acc.Business_Number__c)) {
                if(abnSet.contains(acc.Business_Number__c) && abnDSet.contains(acc.Business_Number__c)) {
                    acc.Potential_Duplicate__c = true;
                    acc.Duplicate_Notes__c += MSG_DUPLICATE_ABN + OPEN_BRACKETS + acc.Business_Number__c + CLOSE_BRACKETS + '\n';
                }
            }
            
            if(String.isNotEmpty(acc.Name)) {
                if(nameSet.contains(acc.Name.toUpperCase().trim()) && nameDSet.contains(acc.Name.toUpperCase().trim())) {
                    acc.Potential_Duplicate__c = true;
                    acc.Duplicate_Notes__c += MSG_DUPLICATE_NAME + OPEN_BRACKETS + acc.Name.trim() + CLOSE_BRACKETS + '\n';
                }
            }
            
            if(String.isNotEmpty(acc.Email_Domain__c)) {
                if(emailDomainSet.contains(acc.Email_Domain__c) && emailDomainDSet.contains(acc.Email_Domain__c)) {
                    acc.Potential_Duplicate__c = true;
                    acc.Duplicate_Notes__c += MSG_DUPLICATE_DOMAIN + OPEN_BRACKETS + acc.Email_Domain__c + CLOSE_BRACKETS + '\n';
                }
            }
            
            if(String.isNotEmpty(acc.Web_Domain__c)) {
                if(webDomainSet.contains(acc.Web_Domain__c) && webDomainDSet.contains(acc.Web_Domain__c)) {
                    acc.Potential_Duplicate__c = true;
                    acc.Duplicate_Notes__c += MSG_DUPLICATE_WEBSITE + OPEN_BRACKETS + acc.Web_Domain__c + CLOSE_BRACKETS + '\n';
                }
            }
        }
    }
    
    public static void markDuplicates( List<Account> accountList )
    {
        // Account RecordTypes to check
        Id corporateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_CORPORATE ).getRecordTypeId();
        Id accelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_ACCELERATE ).getRecordTypeId();
        Id vaAccountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_VA_ACC ).getRecordTypeId();
        Id smartFlyRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_SMARTFLY ).getRecordTypeId();
        
        Set<Id> recordTypeIds = new Set<Id>();
        recordTypeIds.add(corporateRecTypeId);
        recordTypeIds.add(accelerateRecTypeId);
        recordTypeIds.add(vaAccountRecTypeId);
        recordTypeIds.add(smartFlyRecTypeId);
        
        // Duplicate Checking
        // Get Sets of ABN, Account Name and Domain
        //Set<String> sfdcABNSet = new Set<String>();
        //Set<String> sfdcCompanyNameSet = new Set<String>();
        //Set<String> sfdcAccelerateKeyContactEmailDomainSet = new Set<String>();
        
        Map<String, Account> sfdcABNMap = new Map<String, Account>();
        Map<String, Account> sfdcCompanyNameMap = new Map<String, Account>();
        Map<String, Account> sfdcAccelerateKeyContactEmailDomainMap = new Map<String, Account>();
        Map<String, Account> sfdcAccountWebsiteDomainMap = new Map<String, Account>();
        
        Set<String> sfdcIgnoreEmailDomainSet = new Set<String>();
        
        // list
        List<Ignore_Email_Domains__c> emailDomains = Ignore_Email_Domains__c.getall().values();
        for( Ignore_Email_Domains__c emailDomain : emailDomains)
        {
            sfdcIgnoreEmailDomainSet.add( emailDomain.Name );
        }
        
        Set<Id> accountIds = new Set<Id>();
        for( Account acct : accountList )
        {
            accountIds.add(acct.Id);
        }
        
        //Run seperate queries to reduce risk of hitting 50000 SOQL row limit
        List<Account> sfdcAccountCorp = [SELECT Id, Name, Business_Number__c, Administrator_Contact_Email__c, Website FROM Account 
                                         WHERE Id Not IN :accountIds 
                                         ANd RecordTypeId =: corporateRecTypeId
                                         AND Potential_Duplicate__c = false 
                                         AND  Account_Lifecycle__c != 'Inactive'
                                         limit 50000];  
        
        List<Account> sfdcAccountAcc = [SELECT Id, Name, Business_Number__c, Administrator_Contact_Email__c, Website FROM Account 
                                        WHERE Id Not IN :accountIds 
                                        ANd RecordTypeId =: accelerateRecTypeId
                                        AND Potential_Duplicate__c = false
                                        AND  Account_Lifecycle__c != 'Inactive' limit 40000] ;  
        
        List<Account> sfdcAccountVA = [SELECT Id, Name, Business_Number__c, Administrator_Contact_Email__c, Website FROM Account 
                                       WHERE Id Not IN :accountIds 
                                       ANd RecordTypeId =: vaAccountRecTypeId
                                       AND Potential_Duplicate__c = false
                                       AND  Account_Lifecycle__c != 'Inactive'  limit 40000];  
        
        List<Account> sfdcAccountSF = [SELECT Id, Name, Business_Number__c, Administrator_Contact_Email__c, Website FROM Account 
                                       WHERE Id Not IN :accountIds 
                                       ANd RecordTypeId =:smartFlyRecTypeId                     
                                       AND Potential_Duplicate__c = false 
                                       AND  Account_Lifecycle__c != 'Inactive'  
                                       limit 40000];  
        
        List<Account> sfdcAccountList = new List<Account>();
        sfdcAccountList.addAll( sfdcAccountCorp );
        sfdcAccountList.addAll( sfdcAccountAcc );
        sfdcAccountList.addAll( sfdcAccountVA );
        sfdcAccountList.addAll( sfdcAccountSF );
        
        String parsedDomain = '';
        String subDomainHTTP = 'http://';
        String subDomainWWW ='www.';
        
        for( Account sfdcAcct : sfdcAccountList )
        {
            parsedDomain = '';
            
            if( !sfdcABNMap.containsKey( sfdcAcct.Business_Number__c ) && !String.isEmpty( sfdcAcct.Business_Number__c ) )
            {
                sfdcABNMap.put( sfdcAcct.Business_Number__c.trim(), sfdcAcct );
            }
            
            if( !sfdcCompanyNameMap.containsKey( sfdcAcct.Name ) && !String.isEmpty( sfdcAcct.Name ) )
            {
                sfdcCompanyNameMap.put( sfdcAcct.Name.trim(), sfdcAcct );
            }
            
            if( !String.isEmpty( sfdcAcct.Administrator_Contact_Email__c ) && sfdcAcct.Administrator_Contact_Email__c.contains('@') )
            {
                parsedDomain = sfdcAcct.Administrator_Contact_Email__c.substring( sfdcAcct.Administrator_Contact_Email__c.indexOf('@') + 1 ).trim();
                if( !sfdcIgnoreEmailDomainSet.contains( parsedDomain ) && !sfdcAccelerateKeyContactEmailDomainMap.containsKey( parsedDomain ) )
                {
                    sfdcAccelerateKeyContactEmailDomainMap.put( parsedDomain, sfdcAcct );
                }        
            }
            // Website
            if( !String.isEmpty( String.valueOf( sfdcAcct.Website ) ) )
            {
                //URL tempUrl = new URL(sfdcAcct.Website.trim());
                parsedDomain = sfdcAcct.Website.trim();//tempUrl.getHost();
                if( parsedDomain.startsWithIgnoreCase( subDomainHTTP ) )
                {
                    parsedDomain = parsedDomain.substring( subDomainHTTP.length() );
                }
                if( parsedDomain.startsWithIgnoreCase( subDomainWWW ) )
                {
                    parsedDomain = parsedDomain.substring( subDomainWWW.length() );
                }
                
                if( !sfdcAccountWebsiteDomainMap.containsKey( parsedDomain ) )
                {
                    sfdcAccountWebsiteDomainMap.put( parsedDomain, sfdcAcct );
                }        
            }
        }
        
        for( Account account: accountList ) 
        {
            if( recordTypeIds.contains( account.RecordTypeId ) && !account.Potential_Duplicate__c )
            {
                // check account duplicates
                String duplicateNotes = '';
                String accountDuplicate = '';
                parsedDomain = '';
                
                //Ignore blank ABNs
                if( !String.isEmpty( account.Business_Number__c ) && sfdcABNMap.containsKey( account.Business_Number__c.trim() ) )
                {
                    account.Potential_Duplicate__c = true;
                    accountDuplicate = sfdcABNMap.get( account.Business_Number__c.trim() ).Name + SEPARATOR + sfdcABNMap.get( account.Business_Number__c.trim()).Id; 
                    duplicateNotes += MSG_DUPLICATE_ABN + OPEN_BRACKETS + accountDuplicate + CLOSE_BRACKETS;
                }
                else if( !String.isEmpty( account.Business_Number__c ) )
                {
                    sfdcABNMap.put( account.Business_Number__c.trim(), account );
                }
                
                if( sfdcCompanyNameMap.containsKey( account.Name ) && !String.isEmpty( account.Name ) )
                {
                    account.Potential_Duplicate__c = true;
                    accountDuplicate = sfdcCompanyNameMap.get( account.Name.trim() ).Name + SEPARATOR + sfdcCompanyNameMap.get( account.Name.trim() ).Id; 
                    duplicateNotes +=  MSG_DUPLICATE_NAME + OPEN_BRACKETS + accountDuplicate + CLOSE_BRACKETS;
                }
                else if( !String.isEmpty( account.Name ) )
                {
                    sfdcCompanyNameMap.put( account.Name.trim(), account );
                }
                
                if( !String.isEmpty( account.Administrator_Contact_Email__c ) && account.Administrator_Contact_Email__c.contains('@') )
                {
                    parsedDomain = account.Administrator_Contact_Email__c.substring( account.Administrator_Contact_Email__c.indexOf('@') + 1 ).trim();
                    if( !sfdcIgnoreEmailDomainSet.contains( parsedDomain ) )
                    {
                        if( sfdcAccelerateKeyContactEmailDomainMap.containsKey( parsedDomain ) )
                        {
                            account.Potential_Duplicate__c = true;
                            accountDuplicate = sfdcAccelerateKeyContactEmailDomainMap.get( parsedDomain ).Name + SEPARATOR + 
                                
                                sfdcAccelerateKeyContactEmailDomainMap.get( parsedDomain ).Id; 
                            duplicateNotes += MSG_DUPLICATE_DOMAIN + OPEN_BRACKETS + accountDuplicate + CLOSE_BRACKETS;
                        }
                        else
                        {
                            sfdcAccelerateKeyContactEmailDomainMap.put( parsedDomain, account );
                        }
                    }
                }
                
                //
                if( !String.isEmpty( String.valueOf( account.Website ) ) )
                {
                    //URL tempUrl = new URL(account.Website.trim());
                    parsedDomain = account.Website.trim();//tempUrl.getHost();
                    if( parsedDomain.startsWithIgnoreCase( subDomainHTTP ) )
                    {
                        parsedDomain = parsedDomain.substring( subDomainHTTP.length() );
                    }
                    if( parsedDomain.startsWithIgnoreCase( subDomainWWW ) )
                    {
                        parsedDomain = parsedDomain.substring( subDomainWWW.length() );
                    }
                    
                    //account.Internal_Notes__c = parsedDomain;
                    
                    if( sfdcAccountWebsiteDomainMap.containsKey( parsedDomain ) &&  
                       (parsedDomain != 'NO' && parsedDomain != 'NA') ) 
                    {
                        account.Potential_Duplicate__c = true;
                        accountDuplicate = sfdcAccountWebsiteDomainMap.get( parsedDomain ).Name + SEPARATOR + sfdcAccountWebsiteDomainMap.get( parsedDomain ).Id; 
                        duplicateNotes += MSG_DUPLICATE_WEBSITE + OPEN_BRACKETS + accountDuplicate + CLOSE_BRACKETS;
                    }
                    else
                    {
                        sfdcAccountWebsiteDomainMap.put( parsedDomain, account );
                    }
                    
                }
    
                if( account.Potential_Duplicate__c )
                {
                    account.Duplicate_Notes__c = duplicateNotes;
                }    
            }
        }
    }
    
}