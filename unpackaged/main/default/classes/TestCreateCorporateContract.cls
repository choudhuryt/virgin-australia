@isTest
private class TestCreateCorporateContract 
{
    
    static testMethod void myUnitTestOpp() 
    {
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';
        
        insert account;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.Type = 'Acquisition';
        opp1.StageName = 'Awaiting BDM Allocation'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1;
        
        
        
        Market__c market1 = new Market__c();
        market1.Opportunity__c = opp1.id ;
        market1.Name = 'DOM Mainline';
        market1.VA_Revenue_Commitment__c = 100; 
        insert market1;
        
        
        Additional_Benefits__c addben1 = new  Additional_Benefits__c();
        addben1.Opportunity__c = opp1.id ;
        addben1.Name = opp1.Name ;
        insert addben1;
        
        Proposal_Table__c  prop1 = new  Proposal_Table__c();
        prop1.Opportunity__c = opp1.id ;
        prop1.Name = 'DOM Mainline'; 
        insert prop1;
        
        Red_Circle_Discounts__c  red1 = new  Red_Circle_Discounts__c();
        red1.Opportunity__c = opp1.id ;
        red1.Name = 'DOM Mainline'; 
        insert red1;  
        
        List<Id> oppid = new List<Id>();        
        oppid.add(opp1.Id); 
        Test.startTest(); 
        CreateCorporateContract.CreateRelatedContract(oppid)  ;  
        
        Test.stopTest(); 
    }
    
}