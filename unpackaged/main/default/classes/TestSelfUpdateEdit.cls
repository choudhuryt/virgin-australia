@isTest
private class TestSelfUpdateEdit 
{

    static testMethod void myUnitTest() 
    {
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
  		account.BillingCountry ='AU';
  			
 		insert account;
        
        PageReference  ref1 = Page.SelfUpdateEdit;
        ref1.getParameters().put('id',account.Id);
        Test.setCurrentPage(ref1);
        ApexPages.StandardController conL = new ApexPages.StandardController(account);
        SelfUpdateExtension lController = new SelfUpdateExtension(conL);
        
             
        Test.startTest();
        
         ref1 = lController.redirectIfInvalid();
         ref1 = lController.clickSave();

         Test.stopTest();        
    }
}