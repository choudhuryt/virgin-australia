@isTest
private class TestHistoricalDetailsService {
    
    static testMethod void TestMockServices() {
        Test.setMock(WebServiceMock.class, new HistoricalDetailsServiceMockImpl());
        SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService services = 
            new SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService();
        
        SalesForceHistoricalDetailModel.GetFlightDetailsRSType response_1 = 
            services.GetFlightDetails('111', '111', '12/12/2012');
        System.assertEquals('codeaaaaa', response_1.AirlineCode);
        System.assertNotEquals(null, response_1.FlightDate);
    }
    
    static testMethod void TestSalesForceHistoricalDetailUtil() {
        SalesForceHistoricalDetailUtil.AnyElementType anyElementType = new SalesForceHistoricalDetailUtil.AnyElementType();
        SalesForceHistoricalDetailUtil.FaultType faultType = new SalesForceHistoricalDetailUtil.FaultType();
        SalesForceHistoricalDetailUtil.FaultEventType faultEventType = new SalesForceHistoricalDetailUtil.FaultEventType();
        SalesForceHistoricalDetailUtil.PropertySetType propertySetType = new SalesForceHistoricalDetailUtil.PropertySetType();
        SalesForceHistoricalDetailUtil.payload_element payloadElement = new SalesForceHistoricalDetailUtil.payload_element();
    }
}