public with sharing class CheckATierController {

 private ApexPages.StandardController controller {get; set;}
 private  Velocity_Status_Match__c vsm;
 public String Tier {get;set;}
 public CheckATierController(ApexPages.StandardController myController) 
    {
        vsm=(Velocity_Status_Match__c)myController.getrecord();
        
     
    }

 public PageReference initCheckATier() 
     {
		
		Velocity_Status_Match__c vsmMatch = new Velocity_Status_Match__c();
		
		vsmMatch =[select id,Passenger_Velocity_Number__c from Velocity_Status_Match__c where id =:vsm.id];
		CheckATier checkATier = new CheckATier();
		Tier = checkATier.invokeExternalWs(vsmMatch.Passenger_Velocity_Number__c);
		
		return null;
     }
     
    
       public PageReference cancel() {
   // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
  				  PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
                  //
                  thePage.setRedirect(true);
                  //
                  return thePage;
   
  }
}