@isTest
global class HistoricalDetailsServiceMockImpl implements WebServiceMock{
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService services = 
           new SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService();
       SalesForceHistoricalDetailModel.GetFlightDetailsRSType response_x = services.GetFlightDetailsDemo();
       
       response.put('response_x', response_x); 
   }
}