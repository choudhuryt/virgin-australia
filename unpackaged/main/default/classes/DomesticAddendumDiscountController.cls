public  with sharing class DomesticAddendumDiscountController 
{
private ApexPages.StandardController controller {get; set;}

   public List<Proposed_Discount_Tables__c> searchResults {get;set;}
   public List<Proposed_Discount_Tables__c>  searchResults1 {get;set;}   
   public List <Contract_Addendum__c> contractadd {get;set;}
   public Contract_Addendum__c conlist{get;set;}
   public Boolean iseditable {get;set;}
   public integer domFlag {get; set;} 
   public integer regFlag {get; set;}
   private Contract_Addendum__c  a;

 public DomesticAddendumDiscountController(ApexPages.StandardController myController) 
    {
        a=(Contract_Addendum__c)myController.getrecord();
        domFlag=0;
       regFlag=0;
       
      //  transFlag=0;
        //Id conid = 
    }

public PageReference initDisc() 
{
    
    
    
      contractadd=[SELECT Contract__c,Cos_Version__c,Domestic_Guideline_Tier__c,Domestic_Requested_Tier__c,
                                      Red_Circle__c,Regional_Guideline_Tier__c ,Regional_Requested_Tier__c,Status__c,IsEditable__c
                                      FROM Contract_Addendum__c 
                           where id =:ApexPages.currentPage().getParameters().get('id')];
    
      conlist = contractadd.get(0);
    
     if(conlist.IsEditable__c ==true  )
       {
        iseditable = true;
        
       }else{
        iseditable = false;
       }
    
     
    
      searchResults= [SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
                              where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')and  Name = 'DOM Mainline'
                               ORDER BY Sort_Order__c];
    
   
      
      if(searchResults.size()>0)
      {
        domFlag=1;
      }
     
       searchResults1= [
       SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
                              where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')and  Name = 'DOM Regional'
                             ORDER BY Sort_Order__c];
      if(searchResults1.size()>0)
      {
        regFlag=1;
      }
    
    
            
             
      return null;
  
   }
    
    
    public PageReference qsave() {
      upsert searchResults;
      upsert searchResults1; 
      PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount?id=' + a.Id);
      thePage.setRedirect(true);
      return thePage;
    }
    
   public PageReference save() {
   
      upsert searchResults;
      upsert searchResults1; 
       
     
      PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id')); 
      thePage.setRedirect(true);
      return thePage;
  }
    
   public PageReference newDom() 
  {
      
   
  
   
    if (searchResults.size()<1)
      {
         List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>(); 

         templateDomesticDiscountList =
              [
               SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
               ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
               Partner_Eligible_Booking_Class__c 
               FROM Proposal_Table__c WHERE
               Name ='DOM Mainline'
               AND IsTemplate__c = true
               AND Tier__c = : conlist.Domestic_Requested_Tier__c
               AND Cos_Version__c = :conlist.Cos_Version__c
               ORDER BY Sort_Order__c
             ];
        
        
        if(templateDomesticDiscountList.size() > 0 )
            {
               
               for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) 
               {   
                Proposed_Discount_Tables__c newprop = new Proposed_Discount_Tables__c();     
               newprop.Contract__c = conlist.Contract__c ;
               newprop.Contract_Addendum__c = conlist.id;
               newprop.Name = tempdisc.Name;
               newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
               newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
               newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                searchResults.add(newprop);    
               }               
            }
         
                return null;
     }
    else{
  
             PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount?id=' + a.Id);
             thePage.setRedirect(true);
             return thePage;
         }
  }
    
    
    public PageReference newReg() 
  {
      
   
  
   
    if (searchResults1.size()<1)
      {
         List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>(); 

         templateDomesticDiscountList =
              [
               SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
               ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
               Partner_Eligible_Booking_Class__c 
               FROM Proposal_Table__c WHERE
               Name ='DOM Regional'
               AND IsTemplate__c = true
               AND Tier__c = : conlist.Domestic_Requested_Tier__c
               AND Cos_Version__c = :conlist.Cos_Version__c
              ORDER BY Sort_Order__c
             ];
        
        
        if(templateDomesticDiscountList.size() > 0 )
            {
               
               for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) 
               {   
                Proposed_Discount_Tables__c newprop = new Proposed_Discount_Tables__c();     
               newprop.Contract__c = conlist.Contract__c ;
               newprop.Contract_Addendum__c = conlist.id;
               newprop.Name = tempdisc.Name;
               newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
               newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
               newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                searchResults1.add(newprop);    
               }               
            }
         
                return null;
     }
    else{
  
             PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount?id=' + a.Id);
             thePage.setRedirect(true);
             return thePage;
         }
  }
    
     public PageReference remdom()
     {
         Proposed_Discount_Tables__c toDelete = searchResults[searchResults.size()-1];            
         delete toDelete;        
         PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount?id=' + a.Id);
         thePage.setRedirect(true);
         return thePage;
    }
    
    
     public PageReference remreg()
     {
         Proposed_Discount_Tables__c toDelete = searchResults1[searchResults1.size()-1];            
         delete toDelete;        
         PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount?id=' + a.Id);
         thePage.setRedirect(true);
         return thePage;
    }
     public PageReference cancel() {
   // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
   PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
                  //
                  thePage.setRedirect(true);
                  //
                  return thePage;
     }
    
    
    
}