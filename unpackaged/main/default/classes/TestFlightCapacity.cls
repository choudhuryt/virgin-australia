/*
*  Test class for FlightCapacity
*/
@isTest
private class TestFlightCapacity{

    static testMethod void myTest() {
        // setup mock service
        Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());
        
        // prepare test data
      	Case tmpCase = new Case(Flight_Number__c = 'JEST123', Flight_Date__c = System.today(),
                                Velocity_Number__c = '1000019636');
        insert tmpCase;
        
        Reservation__c reservation = new Reservation__c(Case__c = tmpCase.Id, Flight_Number__c = 'JEST123', Flight_Date__c = System.today());
        insert reservation;
        
        // create test page + controller + call search function
        Test.startTest();
        
        PageReference flightSearchPage = new PageReference('/apex/FlightCapacity?id='+tmpCase.Id);
        Test.setCurrentPage(flightSearchPage);
        
        FlightCapacityController customFlightController = new FlightCapacityController();
        FlightCapacityController.FlightCapacity tmp = new FlightCapacityController.FlightCapacity();
        customFlightController.searchCapacity();
        
        Test.stopTest();
        
        // verify search result
        System.assert(customFlightController.flightCapacityList.size()>0);
    }
}