public with sharing class CompensationScorecardController {
      
    
   //Set&Get Lists
   private ApexPages.StandardController controller {get; set;}
   public List<Compensation_Scorecard__c> factsearchResults {get;set;}
     public List<Compensation_Scorecard__c> commsearchResults {get;set;}
     public List<Compensation_Scorecard__c> customersearchResults {get;set;}
     public List<Compensation_Scorecard__c> channelssearchResults {get;set;}
     public List<Compensation_Scorecard__c> circumstancessearchResults {get;set;}
   public List<Compensation_Scorecard__c> templatesearchResults {get;set;}
   public List <Case> caselist {get;set;}
   public Case originalCase {get;set;}
   private Case a;
   public Boolean hasscorecard {get;set;}
   
    // Constructor
    
    public CompensationScorecardController(ApexPages.StandardController myController) 
    {
        a=(Case)myController.getrecord();
           }
 
    //pre-processing prior to Page load
     public PageReference initDisc() 
     {
    

    
      factsearchResults= [
               SELECT Answer__c,Case__c,Categoty__c,Id,Name,Question__c,Score__c,Status__c,Valid_Answer__c,Total_Points_per_Question__c
               FROM  Compensation_Scorecard__c
               where Case__c = :ApexPages.currentPage().getParameters().get('id')
               and  Categoty__c = 'Facts'
               ORDER BY CreatedDate];
         
      commsearchResults = [
               SELECT Answer__c,Case__c,Categoty__c,Id,Name,Question__c,Score__c,Status__c,Valid_Answer__c,Total_Points_per_Question__c
               FROM  Compensation_Scorecard__c
               where Case__c = :ApexPages.currentPage().getParameters().get('id')
               and  Categoty__c = 'Communication'
               ORDER BY CreatedDate];
         
      customersearchResults = [
               SELECT Answer__c,Case__c,Categoty__c,Id,Name,Question__c,Score__c,Status__c,Valid_Answer__c,Total_Points_per_Question__c
               FROM  Compensation_Scorecard__c
               where Case__c = :ApexPages.currentPage().getParameters().get('id')
               and  Categoty__c = 'Customer'
               ORDER BY CreatedDate];
         
         channelssearchResults = [
               SELECT Answer__c,Case__c,Categoty__c,Id,Name,Question__c,Score__c,Status__c,Valid_Answer__c,Total_Points_per_Question__c
               FROM  Compensation_Scorecard__c
               where Case__c = :ApexPages.currentPage().getParameters().get('id')
               and  Categoty__c = 'Channel'
               ORDER BY CreatedDate];
         
         circumstancessearchResults = [
               SELECT Answer__c,Case__c,Categoty__c,Id,Name,Question__c,Score__c,Status__c,Valid_Answer__c,Total_Points_per_Question__c
               FROM  Compensation_Scorecard__c
               where Case__c = :ApexPages.currentPage().getParameters().get('id')
               and  Categoty__c = 'Circumstances'
               ORDER BY CreatedDate];
         
      if(factsearchResults.size()>0 &&  circumstancessearchResults.size() > 0  && channelssearchResults.size() > 0 &&   customersearchResults.size() > 0 && commsearchResults.size() > 0 )
      {
        hasscorecard = true ;
      return null; 
      }else
      {
          
     
      templatesearchResults= [
               SELECT Answer__c,Case__c,Categoty__c,Id,Name,Question__c,Score__c,Status__c,Valid_Answer__c,Total_Points_per_Question__c
               FROM  Compensation_Scorecard__c
               where  IsTemplate__c = true
                and Status__c = 'Active'
               ORDER BY CreatedDate];
          
    for(Integer x =0; x< templatesearchResults.size();x++)
                {
					Compensation_Scorecard__c compscorecard = new Compensation_Scorecard__c();
                    compscorecard.Question__c =  templatesearchResults[x].Question__c;
                    compscorecard.Categoty__c =  templatesearchResults[x].Categoty__c ;
                    compscorecard.Score__c =  templatesearchResults[x].Score__c ;
                    compscorecard.Total_Points_per_Question__c =   templatesearchResults[x].Total_Points_per_Question__c ;
                    compscorecard.Valid_Answer__c =  templatesearchResults[x].Valid_Answer__c ;
					compscorecard.Case__c = ApexPages.currentPage().getParameters().get('id') ;
                    if ( templatesearchResults[x].Categoty__c == 'Facts')
                    {
					  factsearchResults.add( compscorecard);   
                    }
                    
                     if ( templatesearchResults[x].Categoty__c == 'Communication')
                    {
					  commsearchResults.add( compscorecard);   
                    }
                    
                    if ( templatesearchResults[x].Categoty__c == 'Customer')
                    {
					  customersearchResults.add( compscorecard);   
                    }

                    if ( templatesearchResults[x].Categoty__c == 'Channel')
                    {
					 channelssearchResults.add( compscorecard);   
                    }

                    if ( templatesearchResults[x].Categoty__c == 'Circumstances')
                    {
					  circumstancessearchResults.add( compscorecard);   
                    }

				}          
     return null; 
      }     
      
     
   }
   

 

  public PageReference save() {
      
    
      upsert factsearchResults ;
      upsert commsearchResults ;
      upsert  channelssearchResults ;
      upsert circumstancessearchResults ;
      upsert  customersearchResults ;
      
    PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id')); 
                  //
                  thePage.setRedirect(true);
                  //
                  return thePage;
  }
  
  
  public PageReference cancel() {
   // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
   PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
                  //
                  thePage.setRedirect(true);
                  //
                  return thePage;
   
  }
  
 
  
  
}