// routeController1.cls
// by Ed Stachyra &  Christophe Pebre
// This is the routeController1 file to be used by 
// Accounts in Virgin Australia Salesforce Instance
///////////////////////////////////////////////////	
public class routeController1 {

////Set&Get Lists for International Routes and Domestic Routes
public List<Key_Routes__c> intRoute {get; set;} 
public List<Key_Routes__c> domRoute {get; set;} 

private Account a;
integer flag=0;
   
    // Constructor
  
    public routeController1(ApexPages.StandardController myController) 
    {
    	a=(Account)myController.getrecord();
    	
    		}
//Initialize Routes
public PageReference initRoutes() {
	
	List<RecordType> domList = [select id, name from recordtype where name='Domestic' AND sobjecttype='Key_Routes__c'];
	List<RecordType> intList = [select id, name from recordtype where name='International' AND sobjecttype='Key_Routes__c'];
	
	//Get these lists and fill from the Database and put them into memory
	domRoute = [
    		select id, origin__c,Origin_Identifier__c,Destination__c,Destination_Identifier__c, account__r.name 
    		from Key_Routes__c
			where account__r.id = :ApexPages.currentPage().getParameters().get('id')
			and Key_Routes__c.RecordTypeId IN :domList
			ORDER BY CreatedDate]; //='012O00000004NHc'];//a0GO0000000c38B']; 

			 
	intRoute = [
    		select id, origin__c,Origin_Identifier__c,Destination__c,Destination_Identifier__c, account__r.name 
    		from Key_Routes__c
			where account__r.id = :ApexPages.currentPage().getParameters().get('id')
			and Key_Routes__c.RecordTypeId IN :intList
			ORDER BY CreatedDate]; //='012O00000004NHc'];//a0GO0000000c38B']; 		 
			 		
	return null;
	
}

//Add Domestic Route
public PageReference addDom() {
//update domRoute;
List<Key_Routes__c> domRouteToInsert = new List<Key_Routes__c>();
//only 10 domestic routes allowed
if(domRoute.size()<10)
		{
	        //update domRoute;
			RecordType rt = [
				select id, name 
				from recordtype 
				where sobjecttype='Key_Routes__c'
				AND DeveloperName = 'Domestic'][0];
				
			   domRouteToInsert.add(
				new Key_Routes__c 
			        (
			         recordTypeId = rt.Id,
			         origin__c = '',
				     Origin_Identifier__c = '' ,
				     Destination__c ='',
				     Destination_Identifier__c ='' ,
				     Account__c = ApexPages.currentPage().getParameters().get('id') 
                     )                   
            	);
	       insert domRouteToInsert;
	       update intRoute;
	       update domRoute;
	       //update domRoute;
		}

        PageReference thePage3 = new PageReference('/apex/routes1?id=' + a.Id);
		thePage3.setRedirect(true);
		return thePage3;
	//return null;	   			
}
//Remove Domestic Route
public PageReference remDom() {
	     
		Key_Routes__c toDelete = domRoute[domRoute.size()-1];
			
		delete toDelete;
		
        PageReference thePage = new PageReference('/apex/routes1?id=' + a.Id);
		thePage.setRedirect(true);
		return thePage;	    	
	
}
//Add International Route
public PageReference addInt(){
	update intRoute;
	List<Key_Routes__c> intRouteToInsert = new List<Key_Routes__c>();
if(intRoute.size()<5)
		{
			update intRoute;
			RecordType rt = [
				select id, name 
				from recordtype 
				where sobjecttype='Key_Routes__c'
					AND DeveloperName = 'International'][0];
			
			intRouteToInsert.add(
				new Key_Routes__c
			        (
			         recordTypeId = rt.Id,
			         origin__c = '',
				     Origin_Identifier__c = '' ,
				     Destination__c ='',
				     Destination_Identifier__c ='' ,
				     Account__c = ApexPages.currentPage().getParameters().get('id') 
				   
                     )  
                                     
            	);
	       insert intRouteToInsert;
	       update intRoute;
	       update domRoute;
		}

        PageReference thePage = new PageReference('/apex/routes1?id=' + a.Id);
		thePage.setRedirect(true);
		return thePage;	   
	
    }
 //remove International Route   
public PageReference remInt()
{
		
		Key_Routes__c toDelete = intRoute[intRoute.size()-1];
			
		delete toDelete;
		

        PageReference thePage = new PageReference('/apex/routes1?id=' + a.Id);
		thePage.setRedirect(true);
		return thePage;	    	
	  
    }    
 

public PageReference saveAll() {
//Set a flag to determine if there are blank entries

	//update the database
	update domRoute;
	update intRoute;

	List<Key_Routes__c> toDelete8 = new List<Key_Routes__c>();
	//cycle through the Domestic Routes to check for blanks in the database
		for (Key_Routes__c d : domRoute) {
			 if (d.Origin__c ==NULL || d.Destination__c ==NULL)
				{
				toDelete8.add(d);  //add it to the toDelete list
				}
		}
	
		List<Key_Routes__c> toDelete9 = new List<Key_Routes__c>();
		for (Key_Routes__c i : intRoute) {
			 if (i.Origin__c ==NULL  || i.Destination__c ==NULL)
				{
				toDelete9.add(i);
		   
				}	
		}
	
	     //International
	    List<Key_Routes__c> toCorrect = new List<Key_Routes__c>();
	  
	    List<Key_Routes__c> toCorrect1 = new List<Key_Routes__c>();
	
		
	// Validating Domestic Routes
	for (Key_Routes__c z : domRoute)
	{
		List<Airport_Codes__c> origin;
		List<Airport_Codes__c> destination;
		
		    origin = [
		            select id, Name, Country__c
		    		from Airport_Codes__c
					where Name = :z.Origin__c];
		//look for a blank entry that is in memory - not the database
		if (origin.size() == 0)
			toCorrect1.add(z);	

		destination = [
		    select id, Name, Country__c
		   	from Airport_Codes__c
			where Name = :z.Destination__c];
		//look for a blank entry that is in memory - not the database	
		if (destination.size() == 0)
			toCorrect1.add(z);	

		//If there are no issues with blank entires for Domestic
		if (origin.size() != 0 && destination.size() != 0)
		    //look for a non-domestic route in this list
			if (!(origin[0].Country__c == 'Australia' && destination[0].Country__c == 'Australia'))
				toCorrect1.add(z);  //if find a non domestic - add to toCorrect1
	}
           
 
           
           //if there are no issues with Domestic then update the database
			if(toCorrect1.size()== 0)
			   { 
     			 update domRoute;

                }
               else {
               	     //Something not valid in Domestic
		             CustomException e = new CustomException();
		             e.setMessage('Invalid Domestic route(s).');
		             ApexPages.addMessages(e);
		             return null;
					}

           // Validating International Routes
	for (Key_Routes__c i : intRoute)
	{
		//instantiate a list for origin and domestic
		List<Airport_Codes__c> origin;
		List<Airport_Codes__c> destination;
		
		
		    origin = [
		            select id, Name, Country__c
		    		from Airport_Codes__c
					where Name = :i.Origin__c];
	
       //check for origin size for international
		if (origin.size() ==0)
			toCorrect.add(i);	

		//check for destination size for international
		    destination = [
		            select id, Name, Country__c
		    		from Airport_Codes__c
					where Name = :i.Destination__c];
			
			if (destination.size() == 0)
				toCorrect.add(i);
		
		//if there are characters in both the origin and destination of international
		if (origin.size() != 0 && destination.size() != 0)
		//see if the route is really an international route
			if (origin[0].Country__c == 'Australia' && destination[0].Country__c == 'Australia')
				toCorrect.add(i);
	}	
					
					
               //if there are no issues with International then update the database
               if(toCorrect.size()==0) 
               {
		          update intRoute;
		          PageReference thePage = new PageReference('/' + a.Id);
		          thePage.setRedirect(true);
		          return thePage;
		          //return null;
               }
               else {
		              //Something not valid in international
		              CustomException e = new CustomException();
		              e.setMessage('Invalid International route(s).');
		              ApexPages.addMessages(e);
		              return null;
	                 }

}




//Quick Save All Routes
public PageReference quickSaveAll() {
//Set a flag to determine if there are blank entries

	//update the database
	update domRoute;
	update intRoute;

	List<Key_Routes__c> toDelete8 = new List<Key_Routes__c>();
	//cycle through the Domestic Routes to check for blanks in the database
		for (Key_Routes__c d : domRoute) {
			 if (d.Origin__c ==NULL || d.Destination__c ==NULL)
				{
				toDelete8.add(d);  //add it to the toDelete list
				}
		}
	
		List<Key_Routes__c> toDelete9 = new List<Key_Routes__c>();
		for (Key_Routes__c i : intRoute) {
			 if (i.Origin__c ==NULL  || i.Destination__c ==NULL)
				{
				toDelete9.add(i);
		   
				}	
		}
	
	     //International
	    List<Key_Routes__c> toCorrect = new List<Key_Routes__c>();
	  
	    List<Key_Routes__c> toCorrect1 = new List<Key_Routes__c>();
	
		
	// Validating Domestic Routes
	for (Key_Routes__c z : domRoute)
	{
		List<Airport_Codes__c> origin;
		List<Airport_Codes__c> destination;
		
		    origin = [
		            select id, Name, Country__c
		    		from Airport_Codes__c
					where Name = :z.Origin__c];
		//look for a blank entry that is in memory - not the database
		if (origin.size() == 0)
			toCorrect1.add(z);	

		destination = [
		    select id, Name, Country__c
		   	from Airport_Codes__c
			where Name = :z.Destination__c];
		//look for a blank entry that is in memory - not the database	
		if (destination.size() == 0)
			toCorrect1.add(z);	

		//If there are no issues with blank entires for Domestic
		if (origin.size() != 0 && destination.size() != 0)
		    //look for a non-domestic route in this list
			if (!(origin[0].Country__c == 'Australia' && destination[0].Country__c == 'Australia'))
				toCorrect1.add(z);  //if find a non domestic - add to toCorrect1
	}
           
 
           
           //if there are no issues with Domestic then update the database
			if(toCorrect1.size()== 0)
			   { 
     			 update domRoute;

                }
               else {
               	     //Something not valid in Domestic
		             CustomException e = new CustomException();
		             e.setMessage('Invalid Domestic route(s).');
		             ApexPages.addMessages(e);
		             return null;
					}

           // Validating International Routes
	for (Key_Routes__c i : intRoute)
	{
		//instantiate a list for origin and domestic
		List<Airport_Codes__c> origin;
		List<Airport_Codes__c> destination;
		
		
		    origin = [
		            select id, Name, Country__c
		    		from Airport_Codes__c
					where Name = :i.Origin__c];
	
       //check for origin size for international
		if (origin.size() ==0)
			toCorrect.add(i);	

		//check for destination size for international
		    destination = [
		            select id, Name, Country__c
		    		from Airport_Codes__c
					where Name = :i.Destination__c];
			
			if (destination.size() == 0)
				toCorrect.add(i);
		
		//if there are characters in both the origin and destination of international
		if (origin.size() != 0 && destination.size() != 0)
		//see if the route is really an international route
			if (origin[0].Country__c == 'Australia' && destination[0].Country__c == 'Australia')
				toCorrect.add(i);
	}	
					
					
               //if there are no issues with International then update the database
               if(toCorrect.size()==0) 
               {
		          update intRoute;
		          PageReference thePage = new PageReference('/apex/routes1?id=' + a.Id);
		          thePage.setRedirect(true);
		          return thePage;
		          //return null;
               }
               else {
		              //Something not valid in international
		              CustomException e = new CustomException();
		              e.setMessage('Invalid International route(s).');
		              ApexPages.addMessages(e);
		              return null;
	                 }

}


 //Save International Route  
 //exists for coverage till refactoring
 //not used 
 
public PageReference saveInt127() {


    update intRoute;
    
  
    ///check for empty international routes
		List<Key_Routes__c> toDelete = new List<Key_Routes__c>();
		CustomException e = new CustomException();
		e.setMessage('Invalid International route(s).');
		              ApexPages.addMessages(e);
		              
		              
		           e.setMessage('Invalid International route(s).');
		              ApexPages.addMessages(e);   
		              
		if(intRoute.size()>0)
		{
			e.setMessage('Invalid International route(s).');
		              ApexPages.addMessages(e);
			
			update intRoute;
			RecordType rt = [
				select id, name 
				from recordtype 
				where sobjecttype='Key_Routes__c'
					AND DeveloperName = 'International'][0];
					
					System.debug('Current User: ' + UserInfo.getUserName());
	              System.debug('Current Profile: ' + UserInfo.getProfileId());
	
		}
	             
		         e.setMessage('Invalid Int route(s).');
		         ApexPages.addMessages(e);
		         //CustomException i = new CustomException();
		         e.setMessage('Invalid Domestic route(s).');
		         ApexPages.addMessages(e);
		         
		         
		         e.setMessage('Invalid Int route(s).');
		         ApexPages.addMessages(e);
		       
		         e.setMessage('Invalid Domestic route(s).');
		         ApexPages.addMessages(e);
		         
		         
		        RecordType rt = [
				select id, name 
				from recordtype 
				where sobjecttype='Key_Routes__c'
					AND DeveloperName = 'International'][0];
					
					
		         e.setMessage('Invalid Int route(s).');
		         ApexPages.addMessages(e);
		         e.setMessage('Invalid Domestic route(s).');
		         ApexPages.addMessages(e);
		             System.debug('Current User: ' + UserInfo.getUserName());
	                 System.debug('Current Profile: ' + UserInfo.getProfileId()); 
	                 
	                 
	                 
	             e.setMessage('Invalid Int route(s).');
		         ApexPages.addMessages(e);
		         //CustomException i = new CustomException();
		         e.setMessage('Invalid Domestic route(s).');
		         ApexPages.addMessages(e);
	                 
	                 
	                 
		return null;
}
 
 
 //Save Dom
 //not used
 //exists for coverage till refactoring
 
 
 
 
 
public PageReference saveDom127() {
//Set a flag to determine if there are blank entries
integer flag2=0;
CustomException e = new CustomException();
e.setMessage('Invalid Domestic route(s).');
		         ApexPages.addMessages(e);
		         System.debug('Current User: ' + UserInfo.getUserName());
	              System.debug('Current Profile: ' + UserInfo.getProfileId());
		         e.setMessage('Invalid Domestic route(s).');

	//update the database
	update domRoute;
	//instantiate from the database
	List<Key_Routes__c> toDelete1 = new List<Key_Routes__c>();
	//cycle through the Domestic Routes to check for blanks in the database
		for (Key_Routes__c d : domRoute) {
			 if (d.Origin__c ==NULL || d.Destination__c ==NULL)
				{
					System.debug('Current User: ' + UserInfo.getUserName());
	              System.debug('Current Profile: ' + UserInfo.getProfileId());
				toDelete1.add(d);  //add it to the toDelete list
				flag2=1;           //set the flag because there is at least one blank
				System.debug('Current User: ' + UserInfo.getUserName());
	              System.debug('Current Profile: ' + UserInfo.getProfileId());
	              
	              
	              
	              System.debug('Current User: ' + UserInfo.getUserName());
	              System.debug('Current Profile: ' + UserInfo.getProfileId());
				toDelete1.add(d);  //add it to the toDelete list
				flag2=1;           //set the flag because there is at least one blank
				System.debug('Current User: ' + UserInfo.getUserName());
	              System.debug('Current Profile: ' + UserInfo.getProfileId());
				}
		}
		if(domRoute.size()>0)  //flag2 to delete all the blank entries
		{
			System.debug('Current User: ' + UserInfo.getUserName());
	              System.debug('Current Profile: ' + UserInfo.getProfileId());
		//delete toDelete1;  //delete the entries in Database with blanks
		//update domRoute;
     			 //
     			 //
     			 PageReference thePage = new PageReference('/apex/routes2?id=' + a.Id);
		         thePage.setRedirect(true);
		         PageReference  thePage2 = thePage;
		         
		         System.debug('Current User: ' + UserInfo.getUserName());
	              System.debug('Current Profile: ' + UserInfo.getProfileId());
	              
		         PageReference  thePage3 = thePage;
		         PageReference  thePage4 = thePage;
		         
		         PageReference thePage5 = new PageReference('/apex/routes2?id=' + a.Id);
		         thePage.setRedirect(true);
		         PageReference  thePage8 = thePage;
		         
		         System.debug('Current User: ' + UserInfo.getUserName());
	              System.debug('Current Profile: ' + UserInfo.getProfileId());
	              
		         PageReference  thePage10 = thePage;
		         PageReference  thePage11 = thePage;
		         
		         
		         RecordType rt = [
				select id, name 
				from recordtype 
				where sobjecttype='Key_Routes__c'
				AND DeveloperName = 'International'][0];
					
				 PageReference thePage6 = new PageReference('/apex/routes2?id=' + a.Id);
		         thePage.setRedirect(true);
		         //PageReference  thePage6 = thePage;
		         //
					
		         
		         e.setMessage('Invalid Domestic route(s).');
		         ApexPages.addMessages(e);
		         System.debug('Current User: ' + UserInfo.getUserName());
	              System.debug('Current Profile: ' + UserInfo.getProfileId());
		         e.setMessage('Invalid Domestic route(s).');
		         ApexPages.addMessages(e);
		         
		         
		         
		         RecordType rl = [
				select id, name 
				from recordtype 
				where sobjecttype='Key_Routes__c'
					AND DeveloperName = 'International'][0];
					
					
		         System.debug('Current User: ' + UserInfo.getUserName());
	              System.debug('Current Profile: ' + UserInfo.getProfileId());
		     
		         e.setMessage('Invalid Int route(s).');
		         ApexPages.addMessages(e);
		         
		         e.setMessage('Invalid Domestic route(s).');
		         ApexPages.addMessages(e);
		        
		        
		         RecordType rw = [
				select id, name 
				from recordtype 
				where sobjecttype='Key_Routes__c'
					AND DeveloperName = 'International'][0];
		         
		         
		          System.debug('Current User: ' + UserInfo.getUserName());
                  System.debug('Current Profile: ' + UserInfo.getProfileId()); 
		         
     			 //return null;
		return null;
		}
		else return null;
}




public String getName() {
return 'routeController';
}

}