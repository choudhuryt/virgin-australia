global class ScheduleCorporateSpendAggregate implements Schedulable{

	global void execute(SchedulableContext sc) {
	
	CorporateSpendAggregate corpspendBatch = new CorporateSpendAggregate();
    Database.executeBatch(corpspendBatch, 1); 
	    
	} 
}