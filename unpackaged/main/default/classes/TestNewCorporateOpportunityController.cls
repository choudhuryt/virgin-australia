/**
 * @description       : Test class for NewCorporateOpportuntiyContoller
 * @UpdatedBy         : Cloudwerx
**/
@isTest
private class TestNewCorporateOpportunityController {
    public static Id opportunityVAMasterRecordTypeId = Utilities.getRecordTypeId('Opportunity', 'VA_master');
    @testSetup static void setup() {
        Account testAcctObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAcctObj;
        
        Opportunity testOppObj = TestDataFactory.createTestOpportunity('testOpp1', 'Sales Opportunity Analysis', testAcctObj.Id, Date.today(), 1.00);
        INSERT testOppObj;
    }
    
    @isTest
    private static void testNewCorporateOpportuntiyContoller() {
        Account accountObj = [SELECT Id FROM Account LIMIT 1];
        Test.StartTest(); 
        PageReference pageRef1 = Page.NewCorporateOpportunity; 
        pageRef1.getParameters().put('id', accountObj.Id);
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController conL = new ApexPages.StandardController(accountObj);
        NewCorporateOpportuntiyContoller dController = new NewCorporateOpportuntiyContoller(conL);
        dController.pqe = TestDataFactory.createTestPreQuoteEstimate(false, true);
        dController.initDisc();
        dController.save();
        dController.cancel();
        Test.StopTest();
        // Asserts
        Opportunity opp = [SELECT Id, Name, RecordTypeId, Partner_Carrier__c 
                           FROM Opportunity 
                           WHERE AccountId =:accountObj.Id AND Partner_Carrier__c = 'SQ'];
        // Asserts
        System.assertEquals(true, opp != null);
        System.assertEquals(opportunityVAMasterRecordTypeId, opp.RecordTypeId);
        System.assertEquals('SQ', opp.Partner_Carrier__c);
        System.assertEquals('Test OPP', opp.Name);
    }
    
    @isTest
    private static void testNewCorporateOpportuntiyContollerEY() {
        Account accountObj = [SELECT Id FROM Account LIMIT 1];
        Test.StartTest(); 
        PageReference pageRef1 = Page.NewCorporateOpportunity; 
        pageRef1.getParameters().put('id', accountObj.Id);
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController conL = new ApexPages.StandardController(accountObj);
        NewCorporateOpportuntiyContoller dController = new NewCorporateOpportuntiyContoller(conL);
        dController.pqe = TestDataFactory.createTestPreQuoteEstimate(true, false);
        dController.initDisc();
        dController.save();
        dController.cancel();
        Test.StopTest();
        // Asserts
        Opportunity opp = [SELECT Id, Name, RecordTypeId, Partner_Carrier__c 
                           FROM Opportunity 
                           WHERE AccountId =:accountObj.Id AND Partner_Carrier__c = 'EY'];
        // Asserts
        System.assertEquals(true, opp != null);
        System.assertEquals(opportunityVAMasterRecordTypeId, opp.RecordTypeId);
        System.assertEquals('EY', opp.Partner_Carrier__c);
        System.assertEquals('Test OPP', opp.Name);
    }
}