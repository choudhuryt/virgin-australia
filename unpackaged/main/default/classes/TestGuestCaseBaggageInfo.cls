/**
* @description       : Test class for GuestCaseBaggageInfo
* @createdBy         : Cloudwerx
* @Updated By        : Cloudwerx
**/
@isTest
private class TestGuestCaseBaggageInfo {
    @isTest
    private static void testGetVelocityDetails(){
        Test.setMock(WebServiceMock.class, new LostBaggageMockImpl());
        Test.startTest();
        VelocityDetail velocityDetailObj = new GuestCaseBaggageInfo().getVelocityDetails('23', 'recordType');
        Test.stopTest();
        // Asserts
        system.assertEquals('111', velocityDetailObj.PIRNumber);
        system.assertEquals('Vu holmes', velocityDetailObj.FirstName);
    }
    
    @isTest
    private static void testGetVelocityDetailsWithNoNumber(){
        Test.setMock(WebServiceMock.class, new LostBaggageMockImpl());
        Test.startTest();
        VelocityDetail velocityDetailObj = new GuestCaseBaggageInfo().getVelocityDetails('', '');
        Test.stopTest();
        // Asserts
        system.assertEquals(true, velocityDetailObj == null);
    }
}