@isTest
global class LoyaltyServiceMockImpl implements WebServiceMock {
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       GuestCaseVelocityInfo.SalesforceLoyaltyService services = new GuestCaseVelocityInfo.SalesforceLoyaltyService();
       GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType response_x = services.getDemoData();
       
       response.put('response_x', response_x); 
   }
}