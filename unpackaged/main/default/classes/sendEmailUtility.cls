/*
 *  Updated by Cloudwerx
 *  Refactored the code as the query for fetching OrgWideEmailAddress was inside loop
 * 
 */
global class sendEmailUtility {
    global class emailDetails {
        @invocablevariable(required = true) 
        global id TargetObjectId;
        @invocablevariable 
        global id TemplateId;
        @invocablevariable 
        global list<string> toAddressList = new list<string>();
        @invocablevariable 
        global id whatId;
        @invocablevariable 
        global string owdName;
    }    
    
    @InvocableMethod(label = 'Send Apex Email)')
    global static void sendEmail(emailDetails[] EDList) {
        try {     
            Set<String> owdDisplayName = new Set<String>();
            Map<String, OrgWideEmailAddress> owdObjMap = new Map<String, OrgWideEmailAddress>();
            List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            for(emailDetails emailDetailObj : EDList) {                
                owdDisplayName.add(emailDetailObj.owdName);
            }
            
            for(OrgWideEmailAddress orgWideEmailAddressObj :[SELECT Id, DisplayName, Address FROM OrgWideEmailAddress 
                                                             WHERE DisplayName IN :owdDisplayName]) {
                owdObjMap.put(orgWideEmailAddressObj.DisplayName, orgWideEmailAddressObj);
            }
            
            for(EmailDetails emailDetailsObj :EDList) {
                mail.setTemplateID(emailDetailsObj.TemplateId); 
                mail.setTargetObjectId(emailDetailsObj.TargetObjectId);
                mail.setWhatId(emailDetailsObj.whatId);
                mail.setSaveAsActivity(true);
                if(owdObjMap != null && owdObjMap.containsKey(emailDetailsObj.owdName) && owdObjMap.get(emailDetailsObj.owdName).Id != null) {
                    mail.setOrgWideEmailAddressId(owdObjMap.get(emailDetailsObj.owdName).Id);
                }
                if(emailDetailsObj.toAddressList != null && emailDetailsObj.toAddressList.size() > 0) {
                    mail.setToAddresses(emailDetailsObj.toAddressList);
                }
                allmsg.add(mail);
            }                               
            Messaging.sendEmail(allmsg, false);
        } catch(exception e) {
            system.debug(e);
        }            
    }            
}