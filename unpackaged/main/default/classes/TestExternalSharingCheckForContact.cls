/**
* @description       : Test class for ExternalSharingCheckForContact
* @Updated By         : Cloudwerx
**/
@isTest
private class TestExternalSharingCheckForContact {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        INSERT testContactObj;
    }
    
     @isTest
    private static void testExternalSharingCheckForContact()  {
        Contact testContactObj = [SELECT Id FROM Contact LIMIT 1];
        Test.startTest();
        PageReference pageRef = Page.ExternalSharingContact;
        pageRef.getParameters().put('c.id', testContactObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContactObj);
        ExternalSharingCheckForContact externalSharingForContactController  = new ExternalSharingCheckForContact(conL);
        PageReference initDiscPageReference = externalSharingForContactController.initDisc();
        PageReference stopSharingPageReference = externalSharingForContactController.stopsharing();
        Test.stopTest();
        //Asserts
        system.assertEquals('/' + testContactObj.Id, stopSharingPageReference.getUrl());
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.INFO));
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('This contact is  not externally shared.', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.INFO, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testExternalStopSharingForContactCheck()  {
        Contact testContactObj = [SELECT Id FROM Contact LIMIT 1];
        PartnerNetworkRecordConnection testPartnerNetworkRecordConnectionObj = TestDataFactory.createTestPartnerNetworkRecordConnection(null, testContactObj.Id);
        Test.startTest();
        PageReference pageRef = Page.ExternalSharingContact;
        Test.setCurrentPage(pageRef);
        ExternalSharingCheckForContact.isCallFromTestClass = true;
        ApexPages.StandardController conL = new ApexPages.StandardController(testContactObj);
        ExternalSharingCheckForContact externalSharingForContactController  = new ExternalSharingCheckForContact(conL);
        externalSharingForContactController.searchResults = new List<PartnerNetworkRecordConnection>{testPartnerNetworkRecordConnectionObj};
        PageReference stopSharingPageReference = externalSharingForContactController.stopsharing();
        Test.stopTest();
        //Asserts
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.INFO));
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals(true, msg.getSummary().containsIgnoreCase('This contact is externally shared and was received from Flight Centre Travel Group since '));
            System.assertEquals(ApexPages.SEVERITY.INFO, msg.getSeverity());
        }
    }
}