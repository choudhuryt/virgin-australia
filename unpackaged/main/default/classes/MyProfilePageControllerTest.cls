/**
* An apex class that updates details of a portal user.
  Guest users are never able to access this page.
* @UpdateBy : cloudwerx
*/
@IsTest 
private with sharing class MyProfilePageControllerTest {
    
    @isTest
    private static void testMyProfileControllerForGuest() {
        Test.startTest();
        User guestUserObj = [SELECT Id FROM User WHERE userType = 'Guest' LIMIT 1];
        String errorMessage = '';
        system.runAs(guestUserObj) {
            try {
                MyProfilePageController controller = new MyProfilePageController();
            } catch (NoAccessException exp) {
                errorMessage = exp.getMessage();
            }
        }
        Test.stopTest();
        system.assertEquals(true, String.isNotBlank(errorMessage));
    }
    
    @isTest
    private static void testMyProfileController() {
        User currentUser = [select id, title, firstname, lastname, email, phone, mobilephone, fax, street, city, state, postalcode, country
                            FROM User WHERE id =: UserInfo.getUserId()];
        Test.startTest();
        MyProfilePageController controller = new MyProfilePageController();
        //Asserts
        System.assertEquals(currentUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
        System.assert(controller.getIsEdit() == false, 'isEdit should default to false');
        controller.edit();
        //Asserts
        System.assert(controller.getIsEdit() == true);
        controller.cancel();
        //Asserts
        System.assert(controller.getIsEdit() == false);
        System.assert(Page.ChangePassword.getUrl().equals(controller.changePassword().getUrl()));
        controller.save();
        Test.stopTest();
    }
}