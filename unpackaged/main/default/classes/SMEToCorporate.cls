/*
* 	Updated by Cloudwerx : Removed hardcoded references to RecordTypeIds
* 
* 
* 
*/ 

public class SMEToCorporate
{
    @InvocableMethod
    public static void MovetoCorporate(List<Id> Caseids ) 
    {
        List<Contract> conList = new List<Contract>(); 
        List<Contract> lstContractUpdate = new List<Contract>(); 
        List<Account>  lstAccountUpdate = new List<Account>(); 
        
        Id Relatedaccountid ;
        
        List<Case> Caselist = [SELECT Id,CaseNumber,AccountId,Movement_Type__c,New_Account_Owner1__c,NEW_Market_Segment1__c,
                               NEW_Sales_Matrix_Owner_SMO1__c,Change_From_Date1__c  FROM Case where id =:CaseIds];  
        
        
        List<PartnerNetworkRecordConnection> accsharingconnection = new List<PartnerNetworkRecordConnection>();     
        List<PartnerNetworkRecordConnection> consharingconnection = new List<PartnerNetworkRecordConnection>(); 
        List<Account_Data_Validity__c> advList = new List<Account_Data_Validity__c>();
        
        if(Caselist.size() > 0 )
        {
            
            List<Account> accountlist =  [Select Id      from Account where id =:Caselist[0].AccountId    and recordtypeid in (select Id from RecordType where SobjectType = 'Account' and Name in ('Accelerate', 'SmartFly','FlyPlus')) ]; 
            
            
            if(accountlist.size() > 0 )
            {   
                
                
                for(Account acc: accountlist) 
                { 
                    acc.Sales_Support_Group__c = null ;
                    lstAccountUpdate.add(acc) ;
                }
                
                update  lstAccountUpdate;
                
                Relatedaccountid = accountlist[0].id   ;
                accsharingconnection =
                    [SELECT EndDate,Id,LocalRecordId,StartDate,Status FROM PartnerNetworkRecordConnection
                     WHERE LocalRecordId = :Relatedaccountid
                     and Status = 'Received'
                     and EndDate = null
                    ];      
                
                if (accsharingconnection.size() > 0)
                {
                    for(PartnerNetworkRecordConnection  accrecordConn :  accsharingconnection)
                    {
                        delete accrecordConn;               
                    }
                    
                }
                
                consharingconnection = [SELECT EndDate,Id,LocalRecordId,StartDate,Status
                                        FROM PartnerNetworkRecordConnection
                                        WHERE LocalRecordId  in (SELECT id  FROM Contact  WHERE    AccountId = :Relatedaccountid and Key_Contact__c = true)                     
                                        and Status = 'Received'
                                        and EndDate = null
                                       ];      
                
                if (consharingconnection.size() > 0)
                {
                    for(PartnerNetworkRecordConnection  conrecordConn :  consharingconnection)
                    {
                        delete conrecordConn;
                    }
                }
                
                Date dadv;
                
                advList = [SELECT Account_Owner__c,Account_Record_Type__c,From_Account__c,From_Date__c,Id,
                           Market_Segment__c,Name,Record_Type__c,Sales_Matrix_Owner__c,To_Date__c FROM
                           Account_Data_Validity__c   where   From_Account__c =:Relatedaccountid 
                           and Market_Segment__c in ( 'SmartFly' ,'Accelerate','FlyPlus')  and  To_Date__c = null ];
                
                if(advList.size()>0)
                { 
                    dadv = Caselist[0].Change_From_Date1__c;
                    
                    if(dadv.day() >= 19   )  
                    {    
                        dadv = dadv.addMonths(1).toStartofMonth().addDays(-1);
                    }else
                    {
                        dadv = dadv.toStartofMonth().addDays(-1);      
                    }
                    
                    advList[0].To_Date__c = dadv ;
                    
                    update advList;
                    
                    Id accCorporateRecTypeId = Utilities.getRecordTypeId('Account','Corporate');         
                    
                    Account_Data_Validity__c advNew = new Account_Data_Validity__c();
                    advNew.Account_Owner__c = Caselist[0].New_Account_Owner1__c;
                    advNew.Account_Record_Type__c = accCorporateRecTypeId;
                    advNew.From_Account__c = accountlist[0].id ;
                    advNew.From_Date__c = dadv.AddDays(1);
                    advNew.Market_Segment__c = Caselist[0].NEW_Market_Segment1__c;
                    advNew.Sales_Matrix_Owner__c = Caselist[0].NEW_Sales_Matrix_Owner_SMO1__c;            
                    advNew.Account_Record_Type_Picklist__c = 'Corporate' ;
                    insert advNew;
                    
                    
                    List<ContentNote> nte = new List<ContentNote>();
                    List<ContentDocumentLink> lnk = new List<ContentDocumentLink>();
                    ContentNote cnt = new ContentNote();
                    cnt.Content = Blob.valueof(Caselist[0].CaseNumber);
                    cnt.Title = 'Case Number';
                    nte.add(cnt);
                    
                    if(nte.size()>0)
                    {
                        insert nte;
                    }
                    
                    ContentDocumentLink clnk = new ContentDocumentLink();
                    clnk.LinkedEntityId = advNew.Id;
                    clnk.ContentDocumentId = nte[0].Id;
                    clnk.ShareType = 'I';
                    lnk.add(clnk);   
                    
                    if(nte.size()>0){
                        insert lnk;
                    }
                } 
                
                Set<Id> contractRecTypeSet = new Set<Id>();
                
                Id contractPOSRecTypeId = Utilities.getRecordTypeId('Contract','Accelerate POS/Rebate');
                Id contractSmartFlyRecTypeId = Utilities.getRecordTypeId('Contract','SmartFly');
                Id contractFlyPlusRecTypeId = Utilities.getRecordTypeId('Contract','FlyPlus');
                
                contractRecTypeSet.add(contractPOSRecTypeId);
                contractRecTypeSet.add(contractSmartFlyRecTypeId);
                contractRecTypeSet.add(contractFlyPlusRecTypeId);
                
                conList = [SELECT id,EndDate
                           FROM Contract where
                           accountid = :accountlist[0].id 
                           and Status = 'Activated'  
                           and RecordTypeId IN: contractRecTypeSet
                          ];
                
                
                if(conlist.size() > 0 )
                {    
                    
                    for(Contract c: conlist) 
                    {
                        
                        c.EndDate = dadv;
                        if(dadv <= Date.Today())  
                        {
                            c.Status = 'Terminated';
                        }
                        c.Reason_for_Termination__c ='Moving to Mid/Large Market.' ;
                        
                        lstContractUpdate.add(c);  
                    }
                    
                    update lstContractUpdate; 
                }                        
                
            }   
            
        }
        
    }
}