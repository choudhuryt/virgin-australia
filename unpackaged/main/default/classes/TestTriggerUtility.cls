/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 * @updatedBy : cloudwerx
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestTriggerUtility {
	
    public static String velocityNumber = '0000360491';
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        INSERT testCaseObj;
        
        Apex_Callouts__c testApexCalloutObj = TestDataFactory.createTestApexCallouts('TravelBankDetails', 'https://services-mssl.virginaustralia.com/service/partner/salesforce/1.0/SalesforceLoyalty', 90000, 'client_mssl_virginaustralia2018_com');
        INSERT testApexCalloutObj;
        
        Apex_Callouts__c testApexCalloutObj1 = TestDataFactory.createTestApexCallouts('VelocityDetails', 'https://services-mssl.virginaustralia.com/service/partner/salesforce/1.0/SalesforceLoyalty', 90000, 'client_mssl_virginaustralia2018_com');
        INSERT testApexCalloutObj1;
        
        ALMS_Integration__c testALMSIntegrationObj = TestDataFactory.createTestALMSIntegration('Activate ALMS', false);
        INSERT testALMSIntegrationObj;
    }
    
    @isTest
    static void testVelocitySnapShotRestVipSuccess(){
        Id caseId = [SELECT Id FROM Case LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new VelocitySnapshot_Test.WebServiceMockImplV());
        Test.startTest();            
        updateALMSIntegrationRecord();
        TriggerUtility.UpdateVelocity(new List<String>{caseId}, new List<String>{velocityNumber});
        Test.stopTest();  
        //Asserts
		Case updatedCaseObj = [SELECT Id, Travel_Bank_Account__c, Velocity_Type__c FROM Case WHERE Id =:caseId];
        system.assertEquals(null, updatedCaseObj.Travel_Bank_Account__c);
        system.assertEquals('VIP', updatedCaseObj.Velocity_Type__c);
    }
    
    @isTest
    static void testVelocitySnapShotRestVipSuccess2(){
        Id caseId = [SELECT Id FROM Case LIMIT 1]?.Id;
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "VIP", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        HTTPCalloutRequestMock fakeResponse = new HTTPCalloutRequestMock(200, 'Ok', body, null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        Test.startTest();            
        TriggerUtility.almsActivated = 'Active';
        TriggerUtility.UpdateVelocity(new List<String>{caseId}, new List<String>{velocityNumber});
        Test.stopTest();   
        //Asserts
		Comms_Log__c commLogObj = [SELECT Id, Type__c FROM Comms_Log__c WHERE Case__c =:caseId LIMIT 1];
        system.assertEquals('Case', commLogObj.Type__c);
    }
    
    private static void updateALMSIntegrationRecord() {
        ALMS_Integration__c testALMSIntegrationObj = [SELECT Id, Activate_GET__c FROM ALMS_Integration__c LIMIT 1];
        testALMSIntegrationObj.Activate_GET__c = true;
        UPDATE testALMSIntegrationObj;
    }
}