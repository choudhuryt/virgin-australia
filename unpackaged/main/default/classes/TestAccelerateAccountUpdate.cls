/**
* @description       : Test class for AccelerateUpdateAccount
* @Updated By          : Cloudwerx
**/
@isTest
private class TestAccelerateAccountUpdate {
    
    public static Id contractAcceleratePOSRebateRecordTypeId = Utilities.getRecordTypeId('Contract', 'Accelerate_POS_Rebate');
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        testAccountObj.Industry_Type__c = 'Hospitality & Tourism';
        testAccountObj.Market_Segment__c = 'Accelerate';
        testAccountObj.Lounge__c = TRUE;
        testAccountObj.Pilot_Gold_Email__c = 'testgoldmail@yahoo.com';
        testAccountObj.Pilot_Gold_Position__c = '3';
        testAccountObj.Pilot_Gold_Email_Second_Nominee__c = 'testgoldmailsec@yahoo.com';
        INSERT testAccountObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'super@gmail.com', 'Fry', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        INSERT testContactObj;
        
        Subscriptions__c testSubscriptionObj = TestDataFactory.createTestSubscription(testContactObj.Id, false, false);
        INSERT testSubscriptionObj;
        
        Lead testLeadObj = TestDataFactory.createTestLead('Fry', 'Qantas Fry And Sons', 'super@test.com', 'Web', 'Unqualified', 'Corporate', 'Hot', 'Travel', 'Agency', true, 'test@test.com', 'andy@test.com', 'andy@test.com', '1111111111', '3333333333', 'BLI', 'MAL', 'BUL', '3000', 'AU', 'Australia');
        INSERT testLeadObj;
    }
    
    @isTest
    private static void testAccelerateUpdateAccount() {
        Lead testLeadObj = [SELECT Id FROM Lead];
        Account testAccountObj = [SELECT Id FROM Account];
        Contact testContactObj = [SELECT Id FROM Contact];
        Test.startTest();
        Database.LeadConvert leadconvt = new Database.LeadConvert();
        leadconvt.setLeadId(testLeadObj.Id);
        leadconvt.setAccountId(testAccountObj.Id);
        leadconvt.setContactId(testContactObj.Id);
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        leadconvt.setConvertedStatus(convertStatus.MasterLabel);
        Database.LeadConvertResult lcr = Database.convertLead(leadconvt);
        System.assert(lcr.isSuccess());
        AccelerateUpdateAccount.UpdateTheAccount(testLeadObj.Id);
        Test.stopTest();
        //Asserts
        Account updatedAccountObj = [SELECT Id, Sales_Matrix_Owner__c, Account_Lifecycle__c, Account_Owner__c, 
                                     Sales_Support_Group__c, market_segment__c, recordtypeId 
                                     FROM Account WHERE Id =:testAccountObj.Id];
        System.assertEquals('Accelerate', updatedAccountObj.Sales_Matrix_Owner__c);
        System.assertEquals('Acceptance', updatedAccountObj.Account_Lifecycle__c);
        System.assertEquals(AccelerateAccountUpdate.vaUserId, updatedAccountObj.Account_Owner__c);
        System.assertEquals(AccelerateAccountUpdate.vaUserId, updatedAccountObj.Sales_Support_Group__c);
        System.assertEquals('Accelerate', updatedAccountObj.market_segment__c);
        System.assertEquals(AccelerateUpdateAccount.accounAccelerateRecordTypeId, updatedAccountObj.recordtypeId);
        
        Account_Data_Validity__c updatedADVObj = [SELECT Id, Market_Segment__c, Sales_Matrix_Owner__c, Account_Record_Type__c 
                                                  FROM Account_Data_Validity__c WHERE From_Account__c =:testAccountObj.Id LIMIT 1];
        System.assertEquals('Accelerate', updatedADVObj.Market_Segment__c);
        System.assertEquals('Accelerate', updatedADVObj.Sales_Matrix_Owner__c);
        System.assertEquals(AccelerateUpdateAccount.accounAccelerateRecordTypeId, updatedADVObj.Account_Record_Type__c);
        
        Contact updatedContactObj = [SELECT Id, OwnerId, Key_Contact__c 
                                     FROM Contact WHERE AccountID =:testAccountObj.Id];
        System.assertEquals(AccelerateAccountUpdate.vaUserId, updatedContactObj.OwnerId);
        System.assertEquals(true, updatedContactObj.Key_Contact__c);
        
        Subscriptions__c updatedSubscriptionObj = [SELECT Id, Business_News__c, Accelerate_EDM__c 
                                                   FROM Subscriptions__c LIMIT 1];
        System.assertEquals(true, updatedSubscriptionObj.Business_News__c);
        System.assertEquals(true, updatedSubscriptionObj.Accelerate_EDM__c);
        
        Task insertedTaskObj = [SELECT Id, Subject, Status, Priority, Notify_Creator__c, Description 
                                FROM Task WHERE WhatId =:testAccountObj.Id LIMIT 1];
        System.assertEquals('Please Process the following Accelerate Lounge Menmber', insertedTaskObj.Subject);
        System.assertEquals('Not Started', insertedTaskObj.Status);
        System.assertEquals('Normal', insertedTaskObj.Priority);
        System.assertEquals(true, insertedTaskObj.Notify_Creator__c);
        System.assertEquals(true, insertedTaskObj.Description.containsIgnoreCase('Contract Name:'));
        
        Contract insertedContractObj = [SELECT Id, RecordTypeId, Status, Type__c, Contracting_Entity__c,
                                        OwnerId, Sales_Basis__c, StartDate 
                                        FROM Contract WHERE AccountId =:testAccountObj.Id];
        System.assertEquals(contractAcceleratePOSRebateRecordTypeId, insertedContractObj.RecordTypeId);
        System.assertEquals('Draft', insertedContractObj.Status);
        System.assertEquals('Rebate', insertedContractObj.Type__c);
        System.assertEquals('Virgin Australia', insertedContractObj.Contracting_Entity__c);
        System.assertEquals(AccelerateAccountUpdate.vaUserId, insertedContractObj.OwnerId);
        System.assertEquals('Flown', insertedContractObj.Sales_Basis__c);
        System.assertEquals(Date.today(), insertedContractObj.StartDate);
    }
}