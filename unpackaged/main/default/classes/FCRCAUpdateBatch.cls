global class FCRCAUpdateBatch implements Database.Batchable<sObject> 
{
    List<contract_addendum__c> CAListGlobal = new List<contract_addendum__c>();
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        // collect the batches of records or objects to be passed to execute
        string BatchLimit = Label.FCR_Batch_Limit; 
        String query = 'SELECT Id from contract_addendum__c where fcr_processed__c = false and status__c not in (\'deactivated\',\'rejected\') and Domestic_Requested_Tier__c != null limit '+BatchLimit;
        //String query = 'SELECT Id from contract_addendum__c where id = \''+label.FCR_CAId+'\'';
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<contract_addendum__c> CAList) 
    {
        try
        {
            
            for(contract_addendum__c CA : CAList) 
            {        
                set<id> contractIdSet = new set<Id>();
                contractIdSet.add(CA.id);
                FCRCAUtilities.removeDOMRegional(contractIdSet);
            }
        }
    	catch(Exception e) 
        {
            System.debug(e);
        }
         
    }   
     
    global void finish(Database.BatchableContext BC) 
    {
        system.debug('Inside Finish');
        // execute any post-processing operations like sending email
        if(!Test.isRunningTest())  
        {
  			System.scheduleBatch(new FCRCAUpdateBatch(), 'FCR CA Update Job', 1, 1);
        }   
    }
}