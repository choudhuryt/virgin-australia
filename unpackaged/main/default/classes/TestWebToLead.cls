/**
 * @description       : Test class for AccelerateWebToLead
 * @Updated By          : Cloudwerx
**/
@isTest
public with sharing class TestWebToLead {
    
    @testSetup static void setup() {
        // Create common test leads
        List<Lead> testLeads = new List<Lead>();
        testLeads.add(TestDataFactory.createTestLead('Fry', 'Qantas Fry And Sons', 'super@test.com', 'Web', 'Unqualified', 'Corporate', 'Hot', 'Travel', 'Agency', true, 'test@test.com', 'andy@test.com', 'andy@test.com', '1111111111', '3333333333', 'BLI', 'MAL', 'BUL', '3000', 'AU', 'Australia'));
        testLeads.add(TestDataFactory.createTestLead('Fry2', 'Fry And Sons2', 'test2@test.com', 'Web', 'Unqualified', 'Corporate', 'eeee', 'ggg', 'LeadCompany', true, 'test2@test.com', 'andy2@test.com', 'andy2@test.com', '1111111111', '3333333333', 'BLI', 'MAL', 'BUL', '3000', 'AU', 'Australia'));
        INSERT testLeads;
    }
    
    static testMethod void testLeadConversion() {
        Test.startTest();
        lead leadrec = [select id from lead where Email ='super@test.com'];
        AccelerateWebToLead awl = new AccelerateWebToLead(leadrec.id); 
        System.enqueueJob(awl);
        
        lead leadrec2 = [select id from lead where Email ='test2@test.com'];
        AccelerateWebToLead awl2 = new AccelerateWebToLead(leadrec2.id); 
        System.enqueueJob(awl2);
        
        lead leadrec3 = [select id from lead where Email ='test2@test.com'];
        AccelerateWebToLead awl3 = new AccelerateWebToLead(leadrec3.id); 
        System.enqueueJob(awl3);
        Test.stopTest();
        // Asserts
        Lead leadObjConverted = [SELECT Id, Potential_Duplicate__c, Duplicate_Notes__c FROM Lead WHERE Id =:leadrec3.Id];
        System.assertEquals(true, leadObjConverted.Potential_Duplicate__c);
        System.assertEquals(true, String.isNotBlank(leadObjConverted.Duplicate_Notes__c));
    }
}