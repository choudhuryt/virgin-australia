@isTest
private  class TestNewVSMController
{
static testMethod void testNewVSMPage() 
{
    
   
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);
    account.RecordTypeId ='01290000000tSR0';
    account.Sales_Matrix_Owner__c = 'Accelerate';
	account.OwnerId = '00590000000LbNz';
	account.Account_Owner__c = '00590000000LbNz';
	account.Sales_Support_Group__c = '00590000000LbNz'; 		
    account.RecordTypeId ='01290000000tSR0';	
    account.Billing_Country_Code__c ='AU';
    account.Market_Segment__c = 'Accelerate';
    account.Business_Number__c =  '49113973087' ; 
    account.Accelerate_Key_Contact_Email__c= 'test123456789@test.com';
    insert account; 
    
    Contract contract = New Contract();
    contract.name ='PUTCONTRACTNAMEHERE';
    contract.AccountId =account.id;
    contract.Status ='Draft';
    contract.RecordTypeId ='012900000007oTOAAY';
    
    // ** VSMs are now only stored on contracts **
    contract.Pilot_Gold__c = 10;
    
    // Set dates
    contract.startDate = Date.today();
    contract.endDate = Date.today().addDays(365);
    
    insert contract;
    
    // Activate Contract
    contract.Status ='Activated';
    update contract;
    
    Velocity_Status_Match__c vsm = new Velocity_Status_Match__c();
    
    PageReference  ref1 = Page.NewVSMPage;
    ref1.getParameters().put('id', contract.Id);
    Test.setCurrentPage(ref1);
    ApexPages.StandardController conL = new ApexPages.StandardController(contract);
    NewVSMController lController = new NewVSMController(conL);
    Test.startTest();
       
           ref1 = lController.initDisc();
           ref1 = lController.save(); 
           ref1 = lController.cancel(); 
   Test.stopTest();
    

}
    
}