/**
* @description       : Test class for WaiverPointPageController
* @UpdatedBy         : Cloudwerx
**/
@isTest
private class TestWaiverPointPageController {
    
    @testSetup static void setup() {
        User u = TestUtilityClass.createTestUser();
        INSERT u;      
        system.runAs(u) {
            Account testAccountObj = TestUtilityClass.createTestAccount();      
            INSERT testAccountObj;    
            testAccountObj.Waiver_Points_Allocated__c = 10;       
            UPDATE testAccountObj ;     
            
            Waiver_Point__c testWaiverPointObj =   TestUtilityClass.createTestHeadOfficeLevelWaiverPoint(testAccountObj.Id) ; 
            INSERT testWaiverPointObj;
            
            List<Waiver_Favour__c> testWaiverFavours = new List<Waiver_Favour__c>();
            testWaiverFavours.add(TestUtilityClass.createTestHeadOfficePointWaiverFavour(testAccountObj.Id, u.Id));
            testWaiverFavours.add(TestUtilityClass.createTestHeadOfficePointWaiverFavour(testAccountObj.Id, u.Id)); 
            INSERT testWaiverFavours; 
            List<Waiver_Favour__c> updateWaiverFavours = new List<Waiver_Favour__c>();
            List<Waiver_Ticket_Details__c> testWaiverTicketDetails = new List<Waiver_Ticket_Details__c>();
            for (Waiver_Favour__c testWaiverFavourObj :testWaiverFavours) {
                testWaiverFavourObj.Status__c = 'Approved';
                updateWaiverFavours.add(testWaiverFavourObj);
                testWaiverTicketDetails.add(TestUtilityClass.createTestWaiverTicket(testWaiverFavourObj.Id));
            }
            UPDATE updateWaiverFavours;
            INSERT testWaiverTicketDetails; 
        }
    }
    
    @isTest
    private static void TestWaiverHeadOfficeLevelPointCheck()  {
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1];
        Test.startTest(); 
        WaiverPointPageController lController = new WaiverPointPageController();
        PageReference wpPage = new PageReference('/apex/WaiverPointPage?cid=' + testAccountObj.Id);
        Test.setCurrentPage(wpPage);
        wpPage = lController.initDisc();
        Test.stopTest();
    }
    
    @isTest
    private static void TestWaiverStoreLevelPointCheck()  {
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1];  
        Test.startTest(); 
        WaiverPointPageController lController = new WaiverPointPageController();
        PageReference wpPage = new PageReference('/apex/WaiverPointPage?cid=' + testAccountObj.Id);
        Test.setCurrentPage(wpPage);
        wpPage = lController.initDisc();
        Test.stopTest();
    }
    
    @isTest
    private static void TestWaiverStoreLevelPointCheckIsWavierParentAccount()  {
        Account testAccountObj = [SELECT Id, IsWavierParentAccount__c FROM Account LIMIT 1];
        testAccountObj.IsWavierParentAccount__c = true;
        UPDATE testAccountObj;
        Test.startTest(); 
        WaiverPointPageController lController = new WaiverPointPageController();
        PageReference wpPage = new PageReference('/apex/WaiverPointPage?cid=' + testAccountObj.Id);
        Test.setCurrentPage(wpPage);
        wpPage = lController.initDisc();
        Test.stopTest();
    }
    
    @isTest
    private static void TestWaiverStoreLevelPointCheckNoSearchResults()  {
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1]; 
        DELETE [SELECT Id FROM Waiver_Point__c];
        Test.startTest(); 
        WaiverPointPageController lController = new WaiverPointPageController();
        PageReference wpPage = new PageReference('/apex/WaiverPointPage?cid=' + testAccountObj.Id);
        Test.setCurrentPage(wpPage);
        wpPage = lController.initDisc();
        list<AggregateResult> agg = lController.rolluplist;
        String accountName = lController.CurrentAccountDetails;
        lController.sumallocated = 10;
        Test.stopTest();
        // Asserts
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
    }
    
    @isTest
    private static void TestWaiverStoreLevelPointCheckNoAccId()  {
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1]; 
        DELETE [SELECT Id FROM Waiver_Point__c];
        Test.startTest(); 
        WaiverPointPageController lController = new WaiverPointPageController();
        PageReference wpPage = new PageReference('/apex/WaiverPointPage?cid=' + testAccountObj.Id);
        Test.setCurrentPage(wpPage);
        wpPage = lController.initDisc();
        lController.CurrentAccountId = null;
        String accountName = lController.CurrentAccountDetails;
        Test.stopTest();
        // Asserts
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
    }
}