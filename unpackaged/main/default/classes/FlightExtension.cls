/*
*	Extension controller for FlightSection
*/
public class FlightExtension{
    private Case caseObj{get;set;}
    public Reservation__c reserObj{get; set;}
    
    public FlightExtension(ApexPages.StandardController stdController) {
        this.caseObj = (Case)stdController.getRecord();
        loadReservation();
    }
    
    // get Reservation__c from database
    public void loadReservation(){
        try {
            List<Reservation__c> res = [select Id, Flight_Number__c, Flight_Date__c, Origin__c, Destination__c, 
                        Ticket_Number__c, Reservation_Date__c, Reservation_Number__c
                        from Reservation__c where Case__c = :caseObj.Id order by LastModifiedDate DESC ];
            if (res!=null){
                reserObj = res.get(0);
            }else{
                reserObj = new Reservation__c();
            }
        } catch (Exception ex){
            reserObj = new Reservation__c();
        }
        System.debug(reserObj);
    }
}