@isTest
global class VelocityAndTravelBankDispatcherMock implements WebServiceMock{
    global void doInvoke(Object stub,  
             Object request,  
             Map<String, object> response,  
             String endpoint,  
             String soapAction,  
             String requestName,  
             String responseNS,  
             String responseName,  
             String responseType) {
                 System.debug('requestName: ' + requestName);
                //call demo function
                 if (stub instanceof SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService) {
                     new HistoricalDetailsServiceMockImpl().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
                 } 
                 else if (stub instanceof TravelBank.SalesforceTravelBankService) {
                     if (requestName.equals('GetTravelBankAccountRQ'))
                     	new GetTravelBankServiceMockImpl().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
                     else if (requestName.equals('UpdateTravelBankAccountBalanceRQ'))
                        new TravelBankServiceMockImpl().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
                 } 
                 else if (stub instanceof GuestCaseVelocityInfo.SalesforceLoyaltyService) {
                     new LoyaltyServiceMockImpl().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
                 }
                 else if (stub instanceof GuestCaseReservationInfo.SalesforceReservationService) {
                     if (requestName.equals('GetReservationDetailsRQ')) {
                     	new ReservationServiceMockImpl().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
                     } else if (requestName.equals('FindReservationDetailsRQ')){
                        new ReservationServiceMockImpl2().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType); 
                     }
                        
                 }
    }
}