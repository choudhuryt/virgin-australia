public with sharing class EmailMessageForTaskController
{
    public List<EmailMessage> messagelist {get;set;}
     private Task  a;
     public EmailMessageForTaskController(ApexPages.StandardController controller) {
      a = (Task)controller.getRecord();
    
        messagelist= [
                                   SELECT FromAddress,FromName,Subject,TextBody,ToAddress FROM EmailMessage
                                WHERE ActivityId = :ApexPages.currentPage().getParameters().get('id')
                      ];  
    }


}