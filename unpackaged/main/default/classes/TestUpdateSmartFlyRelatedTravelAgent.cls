/*
 * 	Udpdated  by Cloudwerx : Removed hardcoded RecordTypeIds & UserId references from the code
 * 
 */
@isTest
public with sharing class TestUpdateSmartFlyRelatedTravelAgent
{
    
    public static testMethod void testRelatedTravelAgent ()
    {
        Profile userProfile = [SELECT Id FROM Profile WHERE Name = 'Contract Implementation Support'];
        User userRec = [SELECT Id FROM User WHERE profileId =:userProfile.Id AND IsActive = True LIMIT 1];
        
        Test.startTest();    
        Account acc = new Account();
        CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
        acc = commonObjectsForTest.CreateAccountObject(0);
        acc.RecordTypeid = Utilities.getRecordTypeId('Account', 'SmartFly');
        acc.Sales_Matrix_Owner__c = 'Accelerate';
        acc.OwnerId = Utilities.getOwnerIdByName('Virgin Australia Business Flyer');
        acc.Business_Number__c ='111';
        acc.Account_Owner__c = Utilities.getOwnerIdByName('Virgin Australia Business Flyer');
        acc.Sales_Support_Group__c = userRec.Id;         
        acc.Billing_Country_Code__c ='AU';
        acc.Market_Segment__c = 'Smartfly'; 
        acc.Total_Domestic_Air_Travel_Expenditure__c =100;
        acc.Lounge__c =true;
        acc.AccountSource = 'Smartfly';
        
        insert acc; 
        
        Account Indacc = new Account();
        CommonObjectsForTest commonObjectsForTest1 = new CommonObjectsForTest();
        Indacc = commonObjectsForTest1.CreateAccountObject(0);
        Indacc.FC_Store_Name__c = 'TEST QLD 111' ;   
        insert Indacc ;  
        
        Contact newContact1 = new Contact(); 
        newContact1.FirstName ='Andy';
        newContact1.LastName ='C';
        //newContact.Name ='TEST456';
        newContact1.AccountId = acc.id;
        newContact1.Email ='test@test.com';
        newContact1.SmartFly_Contact_Status__c = 'Key Contact';  
        newContact1.Booking_Office_Name__c = 'TEST QLD 111' ; 
        insert newContact1;
        
        List<Id> conid = new List<Id>();        
        conid.add(newContact1.Id); 
        
        UpdateSmartFlyRelatedTravelAgent.UpdateRelatedAgentID(conid)  ;  
        
        Test.stopTest(); 
        
        //system.assertEquals(Indacc.Id, [SELECT Main_Corporate_TMC__c FROM Account WHERE Id =: acc.Id].Main_Corporate_TMC__c);         
    }
}