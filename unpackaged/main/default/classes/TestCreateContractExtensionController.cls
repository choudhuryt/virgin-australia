@isTest
private class TestCreateContractExtensionController
{
    static testMethod void myUnitTest() 
    {
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        insert account;
        
        Contract contractOld = new Contract();        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.Cos_Version__c ='16.0';
        contractOld.Red_Circle__c = true;
        insert contractOld;
        
        Contract_Addendum__c conadd = new Contract_Addendum__c();
        
        conadd.Contract__c = contractOld.Id ;
        conadd.Cos_Version__c = '16.0';
        conadd.Domestic_Requested_Tier__c = 'Tier 1';
        conadd.Regional_Requested_Tier__c = 'Tier 1';
        conadd.Red_Circle__c =false ;   
        conadd.Is_the_contract_being_extended__c = 'Yes' ; 
        conadd.Status__c = 'Draft' ;
        insert conadd;       
        
        
        PageReference  ref1 = Page.CreateContractExtension;
        ref1.getParameters().put('id', conadd.Id);
        Test.setCurrentPage(ref1);
        ApexPages.StandardController conL = new ApexPages.StandardController(conadd);
        CreateContractExtensionController lController = new CreateContractExtensionController(conL);
        
        Test.startTest();
        
        ref1 = lController.initDisc();
        ref1 = lController.save();
        
        ref1 = lController.cancel();
        
        Test.stopTest();
        
        
    }
}