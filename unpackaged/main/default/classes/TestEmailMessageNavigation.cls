@isTest
private class TestEmailMessageNavigation
{
 static testMethod void myUnitTest() 
    {
        
        
      List<RecordType> recordTypeGRCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
									                    		r.Description, r.BusinessProcessId From RecordType r
									                        	where SobjectType = 'Case'
										                        and IsActive = true
										                        and DeveloperName = 'GR_Case' LIMIT 1];
       
        Case cm = new Case(SuppliedEmail='smitha.nair@inmail.com',
                            SuppliedName='John Doe',
                            Velocity_Member__c = false,
                            Subject='Feedback - Something',
                            Contact_First_Name__c='Tom',
                            Contact_Last_name__c = 'Johns',
                            Compliance_Status__c = 'Compliance form NOT required',
                            Category_Number__c = 'a1w6F000004JD9b',
                            Pre_Filter_Primary_Category__c = 'Alliances',
                            recordTypeId=recordTypeGRCase[0].id);
    
    insert cm ;

        //Insert emailmessage for case
        //
        
        EmailMessage email0 = new EmailMessage();
        email0.FromAddress = 'test@abc.org';
        email0.Incoming = True;
        email0.ToAddress= 'test@xyz.org';
        email0.Subject = 'Test email0';
        email0.HtmlBody = 'Test email body0';
        email0.ParentId = cm.Id; 
        insert email0;
        
        EmailMessage email = new EmailMessage();
        email.FromAddress = 'test@abc.org';
        email.Incoming = True;
        email.ToAddress= 'test@xyz.org';
        email.Subject = 'Test email';
        email.HtmlBody = 'Test email body';
        email.ParentId = cm.Id; 
        insert email;
        
          EmailMessage email1 = new EmailMessage();
        email1.FromAddress = 'test@abc.org';
        email1.Incoming = True;
        email1.ToAddress= 'test@xyz.org';
        email1.Subject = 'Test email1';
        email1.HtmlBody = 'Test email body1';
        email1.ParentId = cm.Id; 
        insert email1;
        
         PageReference  ref1 = Page.NextEmailMessage;
         ref1.getParameters().put('id', email.Id);
        Test.setCurrentPage(ref1);
        ApexPages.StandardController conL = new ApexPages.StandardController(email);
        EmailMessageNavigation lController = new EmailMessageNavigation(conL);
        
         Test.startTest();
        
          lController.getPreviousNextEMsgIds() ;
        
          ref1 = lController.nextMsg();
          ref1 = lController.emailMsgList();
          ref1 = lController.previousMsg();
         
         Test.stopTest();
        
    }
}