/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
 
@isTest
private class TestResolvePrismKeyRoutes {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test

       Test.startTest();
       
       //create a user that is both in prod and sandbox to clone from - leads need to be created by a user
      
      	CommonObjectsForTest commonobjectsForTest = new CommonObjectsForTest();
      	
      
        //User initUser = [SELECT Id, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey FROM User  limit 1];
		//User u1 = new User();
        //Create a User ui
        User u1 = new User();
        u1 = commonobjectsForTest.CreateNewUser(0);
              
        insert u1;
   
   		Account newAccount = new Account ();
   		newAccount = commonobjectsForTest.CreateAccountObject(0);
   		
   			insert newAccount;
   
   
   		 
         List<PRISM_Key_Routes__c> prismKeyRoutesList = new List<PRISM_Key_Routes__c>();
        	
        for (Integer i =0; i<3; i++){
        	PRISM_Key_Routes__c pkr1 = new PRISM_Key_Routes__c();
        	pkr1 = commonobjectsForTest.CreatePrismKeyRoutes(i);
        	pkr1.Account__c = newAccount.Id;
        	prismKeyRoutesList.add(pkr1);
        }	
        
        insert prismKeyRoutesList;
        
        update prismKeyRoutesList;
   
   
        // Create a PRISM_Key_Routes__c pkr1
    /*   PRISM_Key_Routes__c pkr1 = new PRISM_Key_Routes__c();
         List<PRISM_Key_Routes__c> c = new List<PRISM_Key_Routes__c>();
         pkr1.Destination__c = 'SYD';
         pkr1.Origin__c = 'BNE';
         pkr1.Account__c = newAccount.id;
         //Set<String> yo = new Set<String>();
        // TestUtilities.createAccounts(1, yo);
         
         
         
        // List <Account> x = [ Select id from account where name = 'TEST' limit 1 ];
         
       //  pkr1.Account__c = 
         c.add(pkr1);
         
         insert pkr1;
         
         
         PRISM_Key_Routes__c pkrupd = new PRISM_Key_Routes__c();
         List<PRISM_Key_Routes__c> cupd = new List<PRISM_Key_Routes__c>();
         pkrupd.Destination__c = 'SYD';
         pkrupd.Origin__c = 'BNE';
         pkrupd.Account__c = newAccount.id;
         //Set<String> yo = new Set<String>();
        // TestUtilities.createAccounts(1, yo);
         
         
         
        // List <Account> x = [ Select id from account where name = 'TEST' limit 1 ];
         
       //  pkr1.Account__c = 
         cupd.add(pkrupd);
         
         insert pkrupd;
         
       */  
         
         
         Test.stopTest();
        
    }
}