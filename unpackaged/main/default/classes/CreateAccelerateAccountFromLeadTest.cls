@isTest
public class CreateAccelerateAccountFromLeadTest {
	
    static testMethod void myUnitTest() {
        
        Lead lead = new Lead(LastName = 'Fry', Company='Qantas Fry And Sons ',Email ='Super@test.com');
        lead.LeadSource = 'Web';
        lead.Status = 'Unqualified';
        lead.Lead_Type__c = 'Corporate';
        lead.Agreed_To_Terms_And_Conditions__c = true;
        lead.Rating = 'Hot';
        lead.Business_Number__c = '2345678765';
        lead.Company_Trading_Name__c = 'Travel';
        lead.Company_Type__c = 'Agency';
        lead.Lounge__c = true;
        lead.Account_Manager_Email_Address__c ='test@test.com';
        lead.Pilot_Gold_Email__c = 'andy@test.com';
        lead.Pilot_Gold_Email_Second_Nominee__c ='andy@test.com';
        lead.Pilot_Gold_Velocity_Gold_Second_Nominee__c ='1111111111';
        lead.Pilot_Gold_Velocity_Number__c ='3333333333'; 
        //MOD-BEGIN CILAG INS CASE# 129600 09.23.16
        lead.Street  = 'BLI';
        lead.City    = 'MAL';
        lead.State   = 'BUL';
        lead.PostalCode = '3000';
        lead.Billing_Country_Code__c = 'AU';
        lead.Country = 'Australia';
        
        Test.startTest();
        insert lead;
        Test.stopTest();
    }
}