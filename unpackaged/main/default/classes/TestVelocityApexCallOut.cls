/**
* @description       : Test class for VelocityApexCallOut
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
public class TestVelocityApexCallOut {
    private static String velocityNumber = 'L111345678';
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('FlyPlus', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        testContactObj.Velocity_Number__c = velocityNumber;
        INSERT testContactObj;
        
        Apex_Callouts__c testApexCalloutObj = TestDataFactory.createTestApexCallouts('VelocityDetails', 'https://services-mssl.virginaustralia.com/service/partner/salesforce/1.0/SalesforceLoyalty', 90000, 'client_mssl_virginaustralia2018_com');
        INSERT testApexCalloutObj;
    }
    
    public class WebServiceMockImplException extends Exception implements WebServiceMock {
        public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
                throw new TestVelocityApexCallOutException('does not correspond to a current Member');
            }
    }
    
    @isTest
    private static void testVelocityApexCalloutContactVelocityStatusRed() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new VelocitySnapshot_Test.WebServiceMockImplR());
        Test.startTest();
        VelocityApexCallOut.apexcallout(velocityNumber, contactId);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, Velocity_Status__c FROM Contact LIMIT 1];
        system.assertEquals('Red', updatedContact.Velocity_Status__c);
    }
    
    @isTest
    private static void testVelocityApexCalloutContactVelocityStatusSilver() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new VelocitySnapshot_Test.WebServiceMockImplS());
        Test.startTest();
        VelocityApexCallOut.apexcallout(velocityNumber, contactId);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, Velocity_Status__c FROM Contact LIMIT 1];
        system.assertEquals('Silver', updatedContact.Velocity_Status__c);
    }
    
    @isTest
    private static void testVelocityApexCalloutContactVelocityStatusGold() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new VelocitySnapshot_Test.WebServiceMockImplG());
        Test.startTest();
        VelocityApexCallOut.apexcallout(velocityNumber, contactId);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, Velocity_Status__c FROM Contact LIMIT 1];
        system.assertEquals('Gold', updatedContact.Velocity_Status__c);
    }
    
    @isTest
    private static void testVelocityApexCalloutContactVelocityStatusVIP() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new VelocitySnapshot_Test.WebServiceMockImplV());
        Test.startTest();
        VelocityApexCallOut.apexcallout(velocityNumber, contactId);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, Velocity_Status__c FROM Contact LIMIT 1];
        system.assertEquals('VIP', updatedContact.Velocity_Status__c);
    }
    
    @isTest
    private static void testVelocityApexCalloutContactVelocityStatusPlatinum() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new VelocitySnapshot_Test.WebServiceMockImplP());
        Test.startTest();
        VelocityApexCallOut.apexcallout(velocityNumber, contactId);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, Velocity_Status__c FROM Contact LIMIT 1];
        system.assertEquals('Platinum', updatedContact.Velocity_Status__c);
    }
    
    @isTest
    private static void testVelocityApexCalloutException() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new WebServiceMockImplException());
        Test.startTest();
        VelocityApexCallOut.apexcallout(velocityNumber, contactId);   
        Test.stopTest();
    }
    
    @TestVisible private class TestVelocityApexCallOutException extends Exception{}
}