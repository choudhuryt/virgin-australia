/**
 * @description       : NewRevisonOppController
 * UpdatedBy : cloudwerx
**/
public with sharing class NewRevisonOppController {
    
    @testvisible private ApexPages.StandardController controller {get; set;}
    public String contractId;
    public Contract originalContract{get;set;}
    public ID conIdAB ;
    public List <Contract> oldContractVersion {get;set;}
    public Id newOppId {get; set;}
    string quotenum;
    @testvisible private string revisondetails {get; set;}
    
    public  NewRevisonOppController(ApexPages.StandardController controller) {
        
        contractId = ApexPages.currentPage().getParameters().get('contractId');       
        oldContractVersion = [SELECT AccountId,Revision_Details__c,AFZ1ONDSelected__c,AFZ2ONDSelected__c,B2B_Request__c,BillingCity,StartDate,Name,Contract_Purpose__c,
                              PRISM_Participant__c, BillingCountry,BillingPostalCode,Status, BillingState,BillingStreet,Company_Signed_By__c,CustomerSignedId,Customer_Signed_By_1__c,
                              ContractNumber,ContractTerm,Contract_Type__c,Contract_Version__c,Cos_Version__c,Date_JFF_Amendment_Letter_Recieved__c,
                              Date_JFF_Amendment_Letter_Sent__c,Description,EndDate,End_Date__c,EUZ1ONDSelected__c,EUZ2ONDSelected__c,
                              EUZ3ONDSelected__c,MEZ1ONDSelected__c,MEZ2ONDSelected__c,MEZ3ONDSelected__c,MEZ4ONDSelected__c,Internal_Notes__c,
                              JFF_Amendment_Letter_Status__c,JFF_Amendment_Rejection_Reason__c,JFF_Comments__c,JFF_DL_Contract_Summary_Form_Received__c,
                              JFF_DL_Contract_Summary_Form_Sent__c,JFF_DL_Contract_Summary_Form_Status__c,JFF_EY_Contract_Summary_Form_Received__c,
                              JFF_EY_Contract_Summary_Form_Sent__c,JFF_EY_Contract_Summary_Form_Status__c,JFF_NZ_Contract_Summary_Form_Received__c,
                              JFF_NZ_Contract_Summary_Form_Sent__c,JFF_NZ_Contract_Summary_Form_Status__c,JFF_SQ_Contract_Summary_Form_Received__c,
                              JFF_SQ_Contract_Summary_Form_Sent__c,JFF_SQ_Contract_Summary_Form_Status__c,Opportunity__c,OwnerExpirationNotice,
                              OwnerId,Parent_Contract__c,SQNAOND__c,SQSEAOND__c,SQEUOND__c,SQWAAOND__c ,TTDiscountAvailable__c, CNDiscountAvailable__c ,
                              EYEUDiscountAvailable__c,EYMEDiscountAvailable__c,EYAFDiscountAvailable__c,DLDiscountAvailable__c,HKDiscountAvailable__c,
                              LONDiscountAvailable__c,SHDiscountAvailable__c,SQAfDiscountAvailable__c,SQASDiscountAvailable__c,SQEUDiscountAvailable__c,
                              exNZ__c,exHKG__c, Contract_Term_Months__c FROM Contract WHERE Id = :contractId];
        originalContract = oldContractVersion.get(0);
        conIdAB = contractId;
    }  
    
    
    public Pagereference actionButtonYes() {
        if(originalContract.Status != 'Activated') {
            ApexPages.addMessage( new ApexPages.message( ApexPages.severity.ERROR, 
                                                        'Only activated contracts can be versioned!' ));
            return null;
        }
        try {
            Opportunity  newRevisionOpp = new Opportunity();
            system.debug ( 'Create new Opp' + originalContract +  originalContract.Internal_Notes__c ) ; 
            newRevisionOpp.Accountid = originalContract.AccountId;
            newRevisionOpp.Existing_SF_Contract__c = originalContract.Id ;             
            newRevisionOpp.StageName = 'Awaiting BDM Allocation';
            newRevisionOpp.Proposed_Contract_Start_Date__c = originalContract.StartDate ;
            newRevisionOpp.Proposed_Contract_Duration__c =  originalContract.Contract_Term_Months__c ;
            newRevisionOpp.B2B_Request__c =  originalContract.B2B_Request__c;
            newRevisionOpp.exHKG__c =  originalContract.exHKG__c; 
            newRevisionOpp.exNZ__c =  originalContract.exNZ__c ; 
            newRevisionOpp.Type =  'Revision';
            //@UpdatedBy: Cloudwerx Here we added code to get user id and record type ids dynamically
            newRevisionOpp.RecordTypeId = Utilities.getRecordTypeId('Opportunity', 'VA_master');
            newRevisionOpp.Name = originalContract.name ; 
            String input = originalContract.Revision_Details__c;
            input = input.replace('<br>','%br%');
            input = input.replaceAll('<[/a-zAZ0-9]*>','');
            input = input.replace('%br%','<br>'); //replace back 
            newRevisionOpp.Revision_Details__c = input; 
            newRevisionOpp.CloseDate   = system.today() ;
            INSERT newRevisionOpp;
            newOppId = newRevisionOpp.id;              
            //for testing purposes 
            system.debug ( 'The new opp' + newOppId ) ;
           
         /*  List<Quote__c> quotedetails = new List<Quote__c>(); 
           quotedetails = getQuotedetails(newRevisionOpp.id,conIdAB);
             
            if (quotedetails.size() > 0)
            {
                insert quotedetails;
            } 
             
             
           List<Market__c> Marketdetails = new List<Market__c>(); 
           Marketdetails = getMarketdetails(newRevisionOpp.id,conIdAB);
            if (Marketdetails.size() > 0)
            {     
               for( Market__c m :Marketdetails) 
               {
                   m.Contract__c = null;
               }    
                insert Marketdetails;
            }
             
            List<Proposal_Table__c> proposaldetails = new List<Proposal_Table__c>(); 
            proposaldetails = getproposaldetails(newRevisionOpp.id,conIdAB);
            if (proposaldetails.size() > 0){
                  for( Proposal_Table__c p :proposaldetails) 
               {
                   p.Contract__c = null;
               }  
                insert proposaldetails;
            }  
           
            List<Additional_Benefits__c> additionalbenefits = new List<Additional_Benefits__c>(); 
            additionalbenefits = getAddBenefits(newRevisionOpp.id,conIdAB);
            if (additionalbenefits.size() > 0){
               for( Additional_Benefits__c add :additionalbenefits) 
               {
                   add.Contract__c = null;
               }  
                insert additionalbenefits;
            }*/
            
            return new PageReference('/'+ newRevisionOpp.Id);
        }  catch (Exception e) {
            // roll everything back in case of error
            ApexPages.addMessages(e);
            newOppId = null;
            return null;
        }      
    }

    
    /*    private List<Quote__c> getQuoteDetails(Id newOppId, Id oldContractId)
        {
            
        List <Quote__c> qte = new List<Quote__c>();
            
        
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Market__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        
        String theQuery = 'SELECT ';
      
        
        // Finalize query string
        theQuery += ' ID FROM Quote__c WHERE Contract__c = \'' + oldContractId + '\'' + 'and Primary__c = true '  ;
        
          List<aggregateResult> qname = [ SELECT max(Name) maxname FROM Quote__c ]; 
          if (qname.size() > 0)
         {    
         for(AggregateResult ar1 : qname)
          {
              quotenum = String.valueOf(ar1.get('maxname'));        

          }
        }  
            
       
            
        List<Sobject> s = new List<Sobject>();
        s = VirginsutilitiesClass.getData(theQuery);
         //s = Database.query(theQuery);
        for (Integer i = 0; i < s.size(); i++)
            {
        Quote__c oldquote = new Quote__c();
        Quote__c newquote = new Quote__c();    
        oldquote = (Quote__c)s.get(i);    
        newquote = oldquote.clone(false, true, true, true);
        newquote.Name =   String.valueOf(quotenum+ 1 )  ;    
        newquote.Opportunity__c= newOppId;
        qte.add(newquote);
            }
        
        
        
        return qte; 
        
        
    }
    
    
    
        private List<Market__c> getMarketDetails(Id newOppId, Id oldContractId){
        
        List <Market__c> mar = new List<Market__c>();
            
        
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Market__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        
        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues)
        {
           String theName = s.getDescribe().getName();
           theQuery += theName + ',';
        }
        
        // Trim last comma
        theQuery = theQuery.subString(0, theQuery.length() - 1);
        
        // Finalize query string
        theQuery += ' FROM Market__c WHERE Contract__c = \'' + oldContractId + '\'';
        
        List<Sobject> s = new List<Sobject>();
        s = VirginsutilitiesClass.getData(theQuery);
         //s = Database.query(theQuery);
        for (Integer i = 0; i < s.size(); i++)
            {
        Market__c oldmarket = new Market__c();
        Market__c newmarket = new Market__c();    
        oldmarket = (Market__c)s.get(i);    
        newmarket = oldmarket.clone(false, true, true, true);
        newmarket.Opportunity__c= newOppId;
        mar.add(newmarket);
            }
        
        
        
        return mar; 
        
    }
    
    private List<Proposal_Table__c> getProposalDetails(Id newOppId, Id oldContractId){
        
        List <Proposal_Table__c> proposal = new List<Proposal_Table__c>();
            
        
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Proposal_Table__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        
        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues)
        {
           String theName = s.getDescribe().getName();
           theQuery += theName + ',';
        }
        
        // Trim last comma
        theQuery = theQuery.subString(0, theQuery.length() - 1);
        
        // Finalize query string
        theQuery += ' FROM Proposal_Table__c WHERE Contract__c = \'' + oldContractId + '\'';
        
        
        
        List<Sobject> s = new List<Sobject>();
        s = VirginsutilitiesClass.getData(theQuery);
         //s = Database.query(theQuery);
        for (Integer i = 0; i < s.size(); i++)
            {
        Proposal_Table__c oldproposal = new Proposal_Table__c();
        Proposal_Table__c newproposal = new Proposal_Table__c();    
        oldproposal = (Proposal_Table__c)s.get(i);      
        newproposal = oldproposal.clone(false, true, true, true);
        newproposal.Opportunity__c= newOppId;
        proposal.add(newproposal);
            }
        
        
        
        return proposal; 
        
    }
    
       private List<Additional_Benefits__c> getAddBenefits(Id newOppId, Id oldContractId){
        
        List <Additional_Benefits__c> addben = new List<Additional_Benefits__c>();
        
        
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Additional_Benefits__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        
        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues)
        {
           String theName = s.getDescribe().getName();
           theQuery += theName + ',';
        }
        
        // Trim last comma
        theQuery = theQuery.subString(0, theQuery.length() - 1);
        
        // Finalize query string
        theQuery += ' FROM Additional_Benefits__c WHERE Contract__c = \'' + oldContractId + '\'';
        
        
        
        List<Sobject> s = new List<Sobject>();
        s = VirginsutilitiesClass.getData(theQuery);
         //s = Database.query(theQuery);
        for (Integer i = 0; i < s.size(); i++)
            {
        Additional_Benefits__c oldaddben = new Additional_Benefits__c();
        Additional_Benefits__c newaddben= new Additional_Benefits__c();   
        oldaddben = (Additional_Benefits__c)s.get(i);   
        newaddben = oldaddben.clone(false, true, true, true);
        newaddben.Opportunity__c= newOppId;
        addben.add(newaddben);
            }
        
        return addben; 
        
    }
   */
    public Pagereference actionButtonNo() {
        return new PageReference('/'+ contractId);
    }
}