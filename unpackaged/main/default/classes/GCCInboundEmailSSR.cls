/*------------------------------------------------------------------------
Author:     Serge Lauriou
Company:    Tquila ANZ
Description:   Inbound Email Handler for processing SSR (Special Service Request) Web form to email service. 

History
<Date>     <Authors Name>     	<Brief Description of Change>
23-Apr-2019  Serge Lauriou		Created
4-Nov-2019	 Serge Lauriou		updated regex
18 Nov 2019  Serge Lauriou		update regex for last name and additional information as form in prod different format than from Sombra
----------------------------------------------------------------------------*/
global class GCCInboundEmailSSR implements Messaging.InboundEmailHandler{
    
    global Messaging.InboundEmailResult handleInboundEmail(
        	Messaging.InboundEmail email,
        	Messaging.InboundEnvelope envelope){
            
            Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
             
            String stripedHtml = email.htmlBody.stripHtmlTags();
            // String stripedHtml = email.htmlBody.stripHtmlTags().normalizeSpace();
            // String stripedHtml = email.plaintextbody;
            //stripedHtml = stripedHtml.replace('*','');
            //stripedHtml = stripedHtml.replace('The content of this e-mail','\\r\\n The content of this e-mail');
            
            // find queue and record type
            Group[] SSRqueues = [select Id, Name from Group where Name = 'SSR'];
            String SSRQueueId = SSRqueues[0].Id;
            string SSRRecordTypeId = [SELECT id from RecordType where Name ='GCC SSR'].Id;
            
            // get values from the html web form
            system.debug('stripedHtml **** ' + stripedHtml);
            
            // extract values from the email body between 2 sentences - User info
            String bodyBookingReference = extractBodyValue(stripedHtml, 'Reservation Number','Title' );
            String bodyFirstName = extractBodyValue(stripedHtml, 'First Name ', ' Last Name');
            system.debug('bodyFirstName **** ' + bodyFirstName);
            // String bodyLastName = extractBodyValue(stripedHtml, 'Last Name', 'Contact Phone Number' ); // for sombra only!
            String bodyLastName = extractBodyValue(stripedHtml, 'Last Name', 'Phone Number' );
            String bodyEmail = extractBodyValue(stripedHtml, 'Email Address', ' ');
            String bodyPhone = extractBodyValue(stripedHtml, 'PhoneNumber', 'na');
            String bodyCountryCode = extractBodyValue(stripedHtml, 'Please enter Country Code, Area Code, Number', 'Phone Number');
            String bodyPhoneType = extractBodyValue(stripedHtml, 'Phone Number', 'Please enter Country Code');
            
            // Vision Impairment Details
            String bodyVisionImpairedNoAssistance = extractBodyValue(stripedHtml, 'I am vision impaired but do not require assistance', 'I am vision impaired and require assistance');
            String bodyVisionImpairedRequireAssistance = extractBodyValue(stripedHtml, 'I am vision impaired and require assistance', 'I am travelling with a service dog in the cabin');
            String bodyVisionImpairedServiceDoginCabin = extractBodyValue(stripedHtml, 'I am travelling with a service dog in the cabin', 'Additional Information');
            String bodyVisionImpairedAdditionalInformation = '';
            if(stripedHtml.contains('Vision Impairment Information')){
            	bodyVisionImpairedAdditionalInformation = extractBodyValue(stripedHtml, 'Additional Information', 'End Of Line');
            }
            
            // Hearing Impairment Details
            String bodyHearingImpairedNoAssistance = extractBodyValue(stripedHtml, 'I am hearing impaired but do not require assistance', 'I am hearing impaired and require assistance');
            String bodyHearingImpairedRequireAssistance = extractBodyValue(stripedHtml, 'I am hearing impaired and require assistance', 'I am travelling with a service dog in the cabin');
            String bodyHearingImpairedServiceDoginCabin = extractBodyValue(stripedHtml, 'I am travelling with a service dog in the cabin', 'Additional Information');
            String bodyHearingImpairedAdditionalInformation = '';
            if(stripedHtml.contains('Hearing Impairment Information')){
          		bodyHearingImpairedAdditionalInformation = extractBodyValue(stripedHtml, 'Additional Information', 'End Of Line');
            }
            
            // Mobility Impairment Information
            String bodyCanUseStairsandWalktoSeat = extractBodyValue(stripedHtml, 'Wheelchair requested as unable to walk long distance but can use stairs and walk to seat', 'Wheelchair requested as unable to walk long distance or use stairs but can walk to seat');
			String bodyUnabletoUseStairsCanWalktoSeat = extractBodyValue(stripedHtml, 'Wheelchair requested as unable to walk long distance or use stairs but can walk to seat', 'Wheelchair requested as immobile but can self-transfer to seat');
			String bodyImmobileCanSelfTransfertoSeat = extractBodyValue(stripedHtml, 'Wheelchair requested as immobile but can self-transfer to seat', 'Wheelchair requested as immobile and cannot self-transfer to seat');
			String bodyImmobileCannotSelfTransfertoSeat = extractBodyValue(stripedHtml, 'Wheelchair requested as immobile and cannot self-transfer to seat', 'Travelling with own electric wheelchair in hold powered by a dry cell battery');
			String bodyOwnElectricWCinHoldDryCell = extractBodyValue(stripedHtml, 'Travelling with own electric wheelchair in hold powered by a dry cell battery', 'Travelling with own electric wheelchair in hold powered by a lithium battery');
			String bodyOwnElectricWCinHoldLithium = extractBodyValue(stripedHtml, 'Travelling with own electric wheelchair in hold powered by a lithium battery','Upper torso harness also required');
			String bodyUpperTorsoHarnessRequired = extractBodyValue(stripedHtml, 'Upper torso harness also required', 'Wheelchair is required onboard');
			String bodyWheelchairRequiredOnboard = extractBodyValue(stripedHtml, 'Wheelchair is required onboard','Dimensions of mobility aid');
			String bodyDimensionsofMobilityAid = extractBodyValue(stripedHtml, 'Dimensions of mobility aid','Additional Information');
			String bodyAdditionalInformationonAssistanceReq = '';
			if(stripedHtml.contains('Mobility Impairment Information')){
          		bodyAdditionalInformationonAssistanceReq = extractBodyValue(stripedHtml, 'Additional Information', 'End Of Line');
            }         
            
            
            // Travelling with a Carer or Providing an Assistance Person
            String bodyImmobileandTravellingwithCarer = extractBodyValue(stripedHtml, 'Immobile and travelling with a carer','Immobile and providing an assistance person');
            String bodyImmobileandProvidingAssistancePerson = extractBodyValue(stripedHtml, 'Immobile and providing an assistance person','Travelling with a carer for other reasons');
            String bodyTravellingWithCarerOther = extractBodyValue(stripedHtml, 'Travelling with a carer for other reasons','Providing an assistance person for other reasons');
            String bodyProvidingAssistancePersonOther = extractBodyValue(stripedHtml, 'Providing an assistance person for other reasons','Additional Information');
            
            String bodyAdditionalInformationAboutAssistance = '';
            
            String stDepartureAssistance = '';
            String bodyDepartureAssistancePersonTitle = '';
            String bodyDepartureAssistancePersonFirstName = '';
            String bodyDepartureAssistancePersonLastName = '';
            String bodyDepartureAssistancePersonCountryCode = '';
            String bodyDepartureAssistancePersonPhoneNumber = '';
            
            String stArrivalAssistance = '';
            String bodyArrivalAssistancePersonTitle = '';
            String bodyArrivalAssistancePersonFirstName = '';
            String bodyArrivalAssistancePersonLastName = '';
            String bodyArrivalAssistancePersonCountryCode = '';
            String bodyArrivalAssistancePersonPhoneNumber = '';
            
            String stCarer = '';
            String bodyCarerSameVirginBooking = '';
            String bodyCarerVirginBookingReference = '';
            String bodyCarerTitle = '';
            String bodyCarerFirstName = '';
            String bodyCarerLastName = '';
            String bodyCarerCountryCode = '';
            String bodyCarerPhoneNumber = '';
            
            if(stripedHtml.contains('Carer Details')){
            	bodyAdditionalInformationAboutAssistance = extractBodyValue(stripedHtml, 'Additional Information','Carer Details');
            	stCarer = stripedHtml.substringAfter('Carer Details');
            	// replace Carer Phone Number by Carer Phone type
            	stCarer = stCarer.replace('Carer Phone Number','Carer Phone Type');
            	bodyCarerSameVirginBooking = extractBodyValue(stCarer, 'Same reservation number as above?','Reservation Number');
            	bodyCarerVirginBookingReference = extractBodyValue(stCarer, 'Reservation Number','Title');
            	bodyCarerTitle = extractBodyValue(stCarer, 'Title','First Name');
            	bodyCarerFirstName = extractBodyValue(stCarer, 'First Name','Last Name');
            	bodyCarerLastName = extractBodyValue(stCarer, 'Last Name','Carer Phone Type');
            	bodyCarerCountryCode = extractBodyValue(stCarer, 'Country Code','Phone Number');
            	bodyCarerPhoneNumber = extractBodyValue(stCarer, 'Phone Number','End Of Line');       
              	
            }
            if(stripedHtml.contains('Assistance Person Details')){
              	bodyAdditionalInformationAboutAssistance = extractBodyValue(stripedHtml, 'Additional Information','Assistance Person Details');
              	// departure assistance values
              	// replace Carer Phone Number by Carer Phone type
            	stripedHtml = stripedHtml.replace('Carer Phone Number','Carer Phone Type');
              	stDepartureAssistance = stripedHtml.substringAfter('Departure Assistance Person');
              	stDepartureAssistance = stDepartureAssistance.substringBefore('rival Assistance Person');
              	
              	bodyDepartureAssistancePersonTitle = extractBodyValue(stDepartureAssistance, 'Title','First Name');
              	bodyDepartureAssistancePersonFirstName = extractBodyValue(stDepartureAssistance, 'First Name','Last Name');
              	bodyDepartureAssistancePersonLastName = extractBodyValue(stDepartureAssistance, 'Last Name','Carer Phone Type');
              	bodyDepartureAssistancePersonCountryCode = extractBodyValue(stDepartureAssistance, 'Country Code','Phone Number');
              	bodyDepartureAssistancePersonPhoneNumber = extractBodyValue(stDepartureAssistance, 'Phone Number','Ar');
              	// Arrival assistance values
              	stArrivalAssistance = stripedHtml.substringAfter('Arrival Assistance Person');
             	bodyArrivalAssistancePersonTitle = extractBodyValue(stArrivalAssistance, 'Title','First Name');
             	bodyArrivalAssistancePersonFirstName = extractBodyValue(stArrivalAssistance, 'First Name','Last Name');
             	bodyArrivalAssistancePersonLastName = extractBodyValue(stArrivalAssistance, 'Last Name','Carer Phone Type');
             	bodyArrivalAssistancePersonCountryCode = extractBodyValue(stArrivalAssistance, 'Country Code','Phone Number');
             	bodyArrivalAssistancePersonPhoneNumber = extractBodyValue(stArrivalAssistance, 'Phone Number','End Of Line');
              	
            }
            
            // Medical Condition That May Require Medical Clearance
            String bodyMedicalConditionMayRequireClearance ='';
            String bodyAdditionalInformationOnCondition = '';
            if(stripedHtml.contains('Medical Condition Information')){
            	bodyMedicalConditionMayRequireClearance = extractBodyValue(stripedHtml, 'Travelling with a medical condition that may require medical clearance','Additional Information');
            	bodyAdditionalInformationOnCondition = extractBodyValue(stripedHtml, 'Additional Information','End Of Line');
            }

            // Hidden Disability Details
            String bodyHiddenDisabilityDetails ='';
            
            if(stripedHtml.contains('Hidden Disability')){
            	bodyHiddenDisabilityDetails = extractBodyValue(stripedHtml, 'Hidden Disability - Details Additional Information','End Of Line');
            }
             
            // Travelling with Powered Medical Equipment for use on board
            String bodyCPAPMachine = '';
            String bodyPersonalPortableOxygenConcentrator = '';
            String bodySupplementaryOxygenBottle = '';
            String bodyOtherPoweredMedicalEquipment = '';
            String bodyAdditionalMedicalEquipmentInformation = '';
           	if(stripedHtml.contains('Travelling with Powered Medical Equipment')){
            	bodyCPAPMachine = extractBodyValue(stripedHtml, 'Travelling with a CPAP machine for use onboard','Travelling with a personal portable oxygen concentrator for use onboard');
            	bodyPersonalPortableOxygenConcentrator = extractBodyValue(stripedHtml, 'Travelling with a personal portable oxygen concentrator for use onboard','Travelling with supplementary oxygen bottle');
                bodySupplementaryOxygenBottle = extractBodyValue(stripedHtml, 'Travelling with supplementary oxygen bottle','Travelling with other powered medical equipment for use onboard');
                bodyOtherPoweredMedicalEquipment = extractBodyValue(stripedHtml, 'Travelling with other powered medical equipment for use onboard','Attach Personal Medical Equipment form');
                bodyAdditionalMedicalEquipmentInformation = extractBodyValue(stripedHtml, 'Additional Information','End Of Line');
            }
            
            // Other Assistance & Additional Information
            String bodyOtherAssistanceAdditionalInformation ='';
            if(stripedHtml.contains('Other Assistance') && !stripedHtml.contains('Hidden Disability') ){
          		bodyOtherAssistanceAdditionalInformation = extractBodyValue(stripedHtml, 'Other Assistance Additional Information','End Of Line');
            }
            if(stripedHtml.contains('Other Assistance') && stripedHtml.contains('Hidden Disability') ){
          		bodyOtherAssistanceAdditionalInformation = extractBodyValue(stripedHtml, 'Other Assistance Additional Information','Hidden Disability');
            }
            
            
            // search for contact for parentid
            String contactId;
            String velocityTier;
                if(bodyEmail != null &&  bodyEmail.contains('@')){
            		List<contact> contactList = new List<Contact>([Select Id, email, Velocity_Status__c from Contact where email =: bodyEmail]);
                		if(contactList.size()>0 ){
                			contactId = contactList[0].Id;
                            velocityTier = contactList[0].Velocity_Status__c;
                	}
                }
                
            //try{
                
                Case newCase = new Case();
                newCase.Description =  'Special Request Service';
                newCase.HTML_Email__c =  email.htmlBody;
                newCase.Subject = email.subject;
                newCase.Origin = 'Web';                
                newCase.status  = 'New';
                newCase.Priority = 'Low';
                newCase.Ownerid = SSRQueueId;
                newCase.RecordTypeId = SSRRecordTypeId;
                newCase.Virgin_Australia_Booking_Reference__c = bodyBookingReference;
                newCase.Email_Address__c = bodyEmail;
                newCase.Contact_Email__c = bodyEmail;
                newCase.Country_Code__c = bodyCountryCode;
                newCase.Contact_Phone__c = bodyPhone;
                newCase.First_Name__c = bodyFirstName;
                newCase.Last_Name__c = bodyLastName;

                //newCase.Velocity_Number__c = bodyVelocity;
                
                // Vision impairment
                
                if(bodyVisionImpairedNoAssistance != null){
                	if(bodyVisionImpairedNoAssistance.contains('Yes')){
                		newCase.Vision_Impaired_No_Assistance__c = true;
                	}                	
                }
                if(bodyVisionImpairedRequireAssistance != null){
                	if(bodyVisionImpairedRequireAssistance.contains('Yes')){
                		newCase.Vision_Impaired_Require_Assistance__c = true;
                	}
                }
                if(bodyVisionImpairedServiceDoginCabin != null){
                	if(bodyVisionImpairedServiceDoginCabin.contains('Yes')){
                		newCase.Vision_Impaired_Service_Dog_in_Cabin__c = true;
                	}
                }
                newCase.Vision_Impaired_Additional_Information__c = bodyVisionImpairedAdditionalInformation;
            
            	// Hearing Impairment Details
            	
            	if(bodyHearingImpairedNoAssistance != null){
                	if(bodyHearingImpairedNoAssistance.contains('Yes')){
                		newCase.Hearing_Impaired_No_Assistance__c = true;
                	}                	
                }
                if(bodyHearingImpairedRequireAssistance != null){
                	if(bodyHearingImpairedRequireAssistance.contains('Yes')){
                		newCase.Hearing_Impaired_Require_Assistance__c = true;
                	}
                }
                if(bodyHearingImpairedServiceDoginCabin != null){
                	if(bodyHearingImpairedServiceDoginCabin.contains('Yes')){
                		newCase.Hearing_Impaired_Service_Dog_in_Cabin__c = true;
                	}
                }
                newCase.Hearing_Impaired_Additional_Informatio__c = bodyHearingImpairedAdditionalInformation;
            	
            	
            	// Wheelchair Assistance
            	
            	//Wheelchair requested as unable to walk long distance but can use stairs and walk to seat
            	if(bodyCanUseStairsandWalktoSeat != null){
                	if(bodyCanUseStairsandWalktoSeat.contains('Yes')){
                		newCase.Can_Use_Stairs_and_Walk_to_Seat__c = true;
                	}                	
                }
            	//Wheelchair requested as unable to walk long distance or use stairs but can walk to seat
            	if(bodyUnabletoUseStairsCanWalktoSeat != null){
                	if(bodyUnabletoUseStairsCanWalktoSeat.contains('Yes')){
                		newCase.Unable_to_Use_Stairs_Can_Walk_to_Seat__c = true;
                	}                	
                }
            	//Wheelchair requested as immobile but can self-transfer to seat
            	if(bodyImmobileCanSelfTransfertoSeat != null){
                	if(bodyImmobileCanSelfTransfertoSeat.contains('Yes')){
                		newCase.Immobile_Can_Self_Transfer_to_Seat__c = true;
                	}                	
                }
            	//Wheelchair requested as immobile and cannot self-transfer to seat	
            	if(bodyImmobileCannotSelfTransfertoSeat != null){
                	if(bodyImmobileCannotSelfTransfertoSeat.contains('Yes')){
                		newCase.Immobile_Cannot_Self_Transfer_to_Seat__c = true;
                	}                	
                }
            	//Travelling with own electric wheelchair in hold powered by a dry cell battery
            	if(bodyOwnElectricWCinHoldDryCell != null){
                	if(bodyOwnElectricWCinHoldDryCell.contains('Yes')){
                		newCase.Own_Electric_WC_in_Hold_Dry_Cell__c = true;
                	}                	
                }
            	//Travelling with own electric wheelchair in hold powered by a lithium battery 
            	if(bodyOwnElectricWCinHoldLithium != null){
                	if(bodyOwnElectricWCinHoldLithium.contains('Yes')){
                		newCase.Own_Electric_WC_in_Hold_Lithium__c = true;
                	}                	
                }     	
            	//Upper torso harness also required
            	if(bodyUpperTorsoHarnessRequired != null){
                	if(bodyUpperTorsoHarnessRequired.contains('Yes')){
                		newCase.Upper_Torso_Harness_Required__c = true;
                	}                	
                } 
            	//Wheelchair is required onboard
            	if(bodyWheelchairRequiredOnboard != null){
                	if(bodyWheelchairRequiredOnboard.contains('Yes')){
                		newCase.Wheelchair_Required_Onboard__c = true;
                	}                	
                } 
            	//Dimensions of mobility aid
            	newCase.Dimensions_of_Mobility_Aid__c = bodyDimensionsofMobilityAid;
            	//Additional Information on Assistance Req
            	newCase.Additional_Information_on_Assistance_Req__c = bodyAdditionalInformationonAssistanceReq;
            	
            	
            	// Travelling with Carer //
            	if(bodyImmobileandTravellingwithCarer != null){
                	if(bodyImmobileandTravellingwithCarer.contains('Yes')){
                		newCase.Immobile_and_Travelling_with_Carer__c = true;
                	}                	
                } 
                if(bodyImmobileandProvidingAssistancePerson != null){
                	if(bodyImmobileandProvidingAssistancePerson.contains('Yes')){
                		newCase.Immobile_and_Providing_Assistance_Person__c = true;
                	}                	
                } 
                if(bodyTravellingWithCarerOther != null){
                	if(bodyTravellingWithCarerOther.contains('Yes')){
                		newCase.Travelling_with_Carer_Other__c = true;
                	}                	
                } 
                if(bodyProvidingAssistancePersonOther != null){
                	if(bodyProvidingAssistancePersonOther.contains('Yes')){
                		newCase.Providing_Assistance_Person_other__c = true;
                	}                	
                } 
                newCase.Additional_Information_about_Assistance__c = bodyAdditionalInformationAboutAssistance;
            	
            	// Assistance Person //
            	newCase.Departure_Assistance_Person_Title__c = bodyDepartureAssistancePersonTitle;
            	newCase.Departure_Assistance_Person_First_Name__c = bodyDepartureAssistancePersonFirstName;
            	newCase.Departure_Assistance_Person_Last_Name__c = bodyDepartureAssistancePersonLastName;
            	newCase.Departure_Assistance_Person_Country_Code__c = bodyDepartureAssistancePersonCountryCode;
            	newCase.Departure_Assistance_Person_Phone_Number__c = bodyDepartureAssistancePersonPhoneNumber;
            	newCase.Arrival_Assistance_Person_Title__c = bodyArrivalAssistancePersonTitle;
            	newCase.Arrival_Assistance_Person_First_Name__c = bodyArrivalAssistancePersonFirstName;
            	newCase.Arrival_Assistance_Person_Last_Name__c = bodyArrivalAssistancePersonLastName;
            	newCase.Arrival_Assistance_Person_Country_Code__c = bodyArrivalAssistancePersonCountryCode;
            	newCase.Arrival_Assistance_Person_Phone_Number__c = bodyArrivalAssistancePersonPhoneNumber;
                        	
            	// Carer Info //
            	if(bodyCarerSameVirginBooking != null){
                	if(bodyCarerSameVirginBooking.contains('Yes')){
                		newCase.Carer_Same_virgin_Booking__c = true;
                	}                	
                } 
            	newCase.Carer_Virgin_Booking_Reference__c = bodyCarerVirginBookingReference;
            	newCase.Carer_Title__c = bodyCarerTitle;
            	newCase.Carer_First_Name__c = bodyCarerFirstName;
            	newCase.Carer_Last_Name__c = bodyCarerLastName;
            	newCase.Carer_Country_Code__c = bodyCarerCountryCode;
            	newCase.Carer_Phone_Number__c = bodyCarerPhoneNumber;
            	
            	// Medical Condition Information
            	if(bodyMedicalConditionMayRequireClearance != null){
                	if(bodyMedicalConditionMayRequireClearance.contains('Yes')){
                		newCase.Medical_Condition_May_require_Clearance__c = true;
                	}                	
                }
                
                // Hidden Disability Details
                newCase.Hidden_Disability_Details__c = bodyHiddenDisabilityDetails;
                
                // Travelling with Powered Medical Equipment
                
                if(bodyCPAPMachine != null){
                	if(bodyCPAPMachine.contains('Yes')){
                		newCase.CPAP_machine__c = true;
                	}                	
                }
                if(bodyPersonalPortableOxygenConcentrator != null){
                	if(bodyPersonalPortableOxygenConcentrator.contains('Yes')){
                		newCase.Personal_Portable_Oxygen_Concentrator__c = true;
                	}                	
                }
                if(bodySupplementaryOxygenBottle != null){
                	if(bodySupplementaryOxygenBottle.contains('Yes')){
                		newCase.Supplementary_Oxygen_Bottle__c = true;
                	}                	
                }
                if(bodyOtherPoweredMedicalEquipment != null){
                	if(bodyOtherPoweredMedicalEquipment.contains('Yes')){
                		newCase.Other_Powered_Medical_Equipment__c = true;
                	}                	
                }
                newCase.Additional_Medical_Equipment_Information__c = bodyAdditionalMedicalEquipmentInformation;
            	
            	
				// Other Assistance & Additional Information
            	newCase.Other_Assistance_Additional_Information__c = bodyOtherAssistanceAdditionalInformation;
				// Additional Information testing medical condition
				newCase.Additional_Information_on_Condition__c = bodyAdditionalInformationOnCondition;
                
                if(contactID <> ''){
                    newCase.ContactId = contactID;
                    newCase.Velocity_Tier__c = velocityTier;
                }
                
                
                //perform case record insert operation.
                insert newCase;
                System.debug('Successfully inserted case id: ' + newCase.Id);
                
                // insert email message to the case
                EmailMessage emailMsgObj = new EmailMessage();
                emailMsgObj.HtmlBody =  email.htmlBody;
                emailMsgObj.TextBody = email.plainTextBody;
                emailMsgObj.FromAddress = email.fromAddress;                
                emailMsgObj.FromName = email.fromName;
                emailMsgObj.Subject = email.subject;
                emailMsgObj.Incoming = true;
                emailMsgObj.ParentId = newCase.Id;
                
                insert emailMsgObj;
                
                
                // attachment
                List<Attachment> lstAttachment = new List<Attachment>();
                
                if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
                    for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
                        Attachment attachment = new Attachment();
                        attachment.ParentId = newCase.Id;
                        attachment.Name = email.binaryAttachments[i].filename;
                        attachment.Body = email.binaryAttachments[i].body;
                        lstAttachment.add(attachment);
                    }
                    
                    insert lstAttachment;
                } 
                
            //}
            //catch (Exception e) {
                //handle it properly.
            //    result.success = false;
            //    return result;
            //}
            
            result.success = true;
            return result;       
        }
    
    // regex to find value from the from after the fieldName:
    private String extractBodyValue(String emailBody, String bodyField, String upToString){
        
        // example String PatternString = [\n\r].*First Name\s*([^\n\r]*)
        // example between 2 sentences = (?<= Virgin Australia Booking Reference )(.*)(?= Title )
        // String PatternString = '[\\n\\r].*' + bodyField + '\\s*([^\\n\\r]*)';
        // Email Address (?<=\s)(.*?)(?=\s)
        // word before string (\w+)\s+Email Address
        
        String PatternString = bodyField + '(.*?)' + upToString;
        if(bodyField == 'Email Address'){
        	PatternString = 'Email Address (?<=\\s)(.*?)(?=\\s)';
        }
        if(bodyField == 'PhoneNumber'){
        	PatternString = '(\\w+)\\s+Email Address';
        }
        // adding for prod form as no "The content of this", use line return
        if(upToString == 'End Of Line'){
            PatternString = bodyField + '(.*)';
        }
        
        Pattern p = Pattern.compile(PatternString);
        Matcher pm = p.matcher(emailBody);
        String resp;
        if (pm.find()){
            resp = pm.group(1);
            System.debug('**RESP***' +resp);
        } else {
            System.debug('No match');
        }
        return resp;
    }
}