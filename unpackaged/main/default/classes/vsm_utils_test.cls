/**
 * @description       : Test class for VSM_Utills
 * @UpdatedBy         : Cloudwerx
**/
@isTest
public class vsm_utils_test {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;  
        
        Contract testContractObj = TestDataFactory.createTestContract('TestContract', testAccountObj.Id, 'Draft', '15', null, true, Date.today());
        INSERT testContractObj;
        testContractObj.Status = 'Activated';
        testContractObj.EndDate = Date.today().addDays(10);
        UPDATE testContractObj;
        
        Velocity_Status_Match__c testVelocityStatusMatchObj = TestDataFactory.createTestVelocityStatusMatch(testAccountObj.Id, testContractObj.Id, 'Yes', 'Activated', Date.today(), 'some', 'Velocity Status Upgrade');
        testVelocityStatusMatchObj.RecordTypeId = Utilities.getRecordTypeId('Velocity_Status_Match__c', 'Velocity_Upgrade');
        testVelocityStatusMatchObj.Passenger_Velocity_Number__c = '9878786756';
        INSERT testVelocityStatusMatchObj;
        
        Velocity_Acceleration__c testVelocityAccelerationObj = TestDataFactory.createTestVelocityAcceleration('TestVelocityAccelaration', testContractObj.Id, 1, 2, 3, 4, Date.today().addDays(-1), 5);
        testVelocityAccelerationObj.End_Date__c = Date.today().addDays(1);
        INSERT testVelocityAccelerationObj;
    }
    
    @isTest public static void testgetVSMTotals() {
        Contract contractObj = [SELECT ID, EndDate FROM Contract LIMIT 1];
        Test.startTest();
        VSM_Utills.VSM_Totals totals = VSM_Utills.getVSM_Totals(contractObj.id);
        Test.stopTest();
        // Asserts
        system.assert(totals != null);
        system.assertEquals(1, totals.getAllocated('some'));
    }
    
    @isTest public static void testgetVSMTotalsWithAnnual() {
        Contract contractObj = [SELECT ID, Velocity_Acceleration_Frequency__c FROM Contract LIMIT 1];
        contractObj.Velocity_Acceleration_Frequency__c = 'Annual';
        UPDATE contractObj;
        Test.startTest();
        VSM_Utills.VSM_Totals totals = VSM_Utills.getVSM_Totals(contractObj.id);
        Test.stopTest();
        // Asserts
        system.assert(totals != null);
        system.assertEquals(4, totals.getAvailable('Silver'));
        system.assertEquals(2, totals.getAvailable('Pilot Gold'));
        system.assertEquals(1, totals.getAvailable('Gold'));
        system.assertEquals(3, totals.getAvailable('Platinum'));
        system.assertEquals(5, totals.getAvailable(label.Virgin_Australia_Beyond));
        system.assertEquals(1, totals.getAllocated('some'));
    }
    
    @isTest public static void VSMGetTotalsTestAnnual() {
        Test.StartTest(); 
        VSM_Utills.VSM_Totals VSMT = new VSM_Utills.VSM_Totals();
        VSMT.getSilverAvailable();
        VSMT.getSilverUsed();
        VSMT.getSilverRemaining();
        VSMT.getPilotGoldUsed();
        VSMT.getPilotGoldRemaining();
        VSMT.getPilotGoldAvailable();
        VSMT.getGoldAvailable();
        VSMT.getGoldUsed();
        VSMT.getGoldRemaining();
        VSMT.getBeyondAvailable();
        VSMT.getBeyondUsed();
        VSMT.getBeyondRemaining();
        VSMT.getContractNumber();
        VSMT.getContractId();
        VSMT.getPlatinumAvailable();
        VSMT.getPlatinumUsed();
        VSMT.getPlatinumRemaining();
        VSMT.setAvailable('testAvailable', 2);
        VSMT.incrementAllocated('testAllocated');
        Integer availbleValue = VSMT.getAvailable('testAvailable');
        Integer allocatedValue = VSMT.getAllocated('testAllocated');
        Test.StopTest(); 
        // Asserts
        system.assert(availbleValue == 2);
        system.assert(allocatedValue == 1);
    }
}