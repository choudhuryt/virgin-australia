/**
* @description       : Test class for UpdateOwnerManager
* @UpdatedBy         : Cloudwerx
**/
@IsTest
private class TestUpdateOwnerManager {
    public static String orgId = UserInfo.getOrganizationId();
    public static Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1]?.Id;
    
    @testSetup static void setup() {
        List<User> testUsers = new List<User>();
        User testUserObj = TestDataFactory.createTestUser('tuser1', 'Test First Name' + System.now(), 'Test Last Name', 'test@email.com', 'Name1' + '@test' + orgId + '.org', 3, 'Nickname1' + System.now(), profileId, 'Australia/Sydney', 'en_US', 'UTF-8', 'en_US');
        User testUserObj1 = TestDataFactory.createTestUser('tuser2', 'Test First Name' + System.now(), 'Test Last Name', 'test@email.com', 'Name2' + '@test' + orgId + '.org', 3, 'Nickname2' + System.now(), profileId, 'Australia/Sydney', 'en_US', 'UTF-8', 'en_US');
        testUsers.add(testUserObj);
        testUsers.add(testUserObj1);
        INSERT testUsers;
        testUserObj.Level__c = 2;
        testUserObj.contract_approver__c = testUserObj1.Id;
        UPDATE testUserObj;
        testUserObj1.Level__c = 3;
        UPDATE testUserObj1;
        
        Account testAccountObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        testAccountObj.OwnerId = testUserObj.id;
        INSERT testAccountObj;  
        
        Contract testContractObj = TestDataFactory.createTestContract('TestContract', testAccountObj.Id, 'Draft', '15', null, true, Date.today());
        INSERT testContractObj; 
        
        Contract_Termination__c testContractTerminationObj = TestUtilityClass.createTestContractTermination(testAccountObj.Id, testContractObj.Id);
        INSERT testContractTerminationObj; 
        
        Contract_Extension__c testContractExtensionObj = TestUtilityClass.createTestContractExtension(testAccountObj.Id, testContractObj.Id);
        INSERT testContractExtensionObj; 
        
        Opportunity testOppObj = TestDataFactory.createTestOpportunity('testOpp1', 'Sales Opportunity Analysis', testAccountObj.Id, Date.today(), 1.00);
        INSERT testOppObj;
        testOppObj.OwnerId = testUserObj.id;
        UPDATE testOppObj;
        
        Global_Sales_Activity__c testGlobalSalesActivityObj = TestUtilityClass.createGlobalSalesActivity();
        testGlobalSalesActivityObj.ownerid  = testUserObj.id;  
        INSERT testGlobalSalesActivityObj;
    }
    
    @isTest 
    private static void testUpdateOwnerManager() {
        List<Id> userIds = new List<Id>(new Map<Id, User>([SELECT Id FROM User WHERE alias IN ('tuser1', 'tuser2')]).keySet());
        Test.startTest();
        UpdateOwnerManager.UpdateL3OwnerManager(userIds);
        Test.stopTest();
        
        //Asserts
        Opportunity updatedOpportunityObj = [SELECT Id, Opportunity_Owner_Manager__c, Level3_Manager__c FROM Opportunity LIMIT 1];
        System.assertEquals(userIds[1], updatedOpportunityObj.Opportunity_Owner_Manager__c);
        System.assertEquals(userIds[1], updatedOpportunityObj.Level3_Manager__c);
        
        Contract_Extension__c updatedContractExtensionObj = [SELECT Id, Level3_Manager__c FROM Contract_Extension__c LIMIT 1];
        System.assertEquals(userIds[1], updatedContractExtensionObj.Level3_Manager__c);
        
        Contract_Termination__c updatedContractTerminationObj = [SELECT Id, Level3_Manager__c FROM Contract_Termination__c LIMIT 1];
        System.assertEquals(userIds[1], updatedContractTerminationObj.Level3_Manager__c);
        
        Global_Sales_Activity__c updatedGlobalSalesActivityObj = [SELECT Id, Associated_Level3__c FROM Global_Sales_Activity__c LIMIT 1];
        System.assertEquals(userIds[1], updatedGlobalSalesActivityObj.Associated_Level3__c);
    }
    
    @isTest 
    private static void testUpdateOwnerManagerContractLevel2() {
        User testUserObj = TestDataFactory.createTestUser('tuser3', 'Test First Name' + System.now(), 'Test Last Name', 'test@email.com', 'Name3' + '@test' + orgId + '.org', 2, 'Nickname3' + System.now(), profileId, 'Australia/Sydney', 'en_US', 'UTF-8', 'en_US');
        INSERT testUserObj;
        
        Opportunity testOppObj = [SELECT Id, OwnerId FROM Opportunity LIMIT 1];
        testOppObj.OwnerId = testUserObj.Id;
        UPDATE testOppObj;
        
        Account testAccountbj = [SELECT Id, OwnerId FROM Account LIMIT 1];
        testAccountbj.OwnerId = testUserObj.Id;
        UPDATE testAccountbj; 
        
        Global_Sales_Activity__c testGlobalSalesActivityObj = [SELECT Id, OwnerId FROM Global_Sales_Activity__c LIMIT 1];
        testGlobalSalesActivityObj.OwnerId = testUserObj.Id;
        UPDATE testGlobalSalesActivityObj;
        
        List<User> testUsers = [SELECT Id, contract_approver__c, Level__c FROM User WHERE alias IN ('tuser1', 'tuser2')];
        testUserObj.Level__c = 2;
        testUserObj.contract_approver__c = testUsers[0].Id;
        UPDATE testUserObj;
        
        Test.startTest();
        UpdateOwnerManager.UpdateL3OwnerManager(new List<Id>{testUserObj.Id});
        Test.stopTest();
        
        //Asserts
        Opportunity updatedOpportunityObj = [SELECT Id, Opportunity_Owner_Manager__c, Level3_Manager__c FROM Opportunity LIMIT 1];
        System.assertEquals(testUsers[0].Id, updatedOpportunityObj.Opportunity_Owner_Manager__c);
    }
}