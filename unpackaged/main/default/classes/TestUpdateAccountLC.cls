/**
* @description       : Test class for updateAccountLC
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestUpdateAccountLC {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        INSERT testCaseObj;
    }
    
    @isTest
    private static void testUpdatedAccountLC() {
        Case testCaseObj = [SELECT Id, status, ACC_Secondary_Category__c, subject FROM Case LIMIT 1];
        Test.startTest();
        testCaseObj.ACC_Secondary_Category__c = 'Received Further Info';
        testCaseObj.Status = 'Working';
        testCaseObj.subject = 'FOLLOWUP#[';
        UPDATE testCaseObj;
        Test.stopTest();
        //Asserts
        Account updatedAccountObj = [SELECT Id, Account_Lifecycle__c FROM Account LIMIT 1];
        system.assertEquals('Opportunity', updatedAccountObj.Account_Lifecycle__c);
    }
}