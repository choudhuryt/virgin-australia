public class AccountOwnerBulkUpdate
{
@InvocableMethod
public static void OwnerUpdateForAccount(List<Id> Accountids ) 
{
     Date dadv ;
     List<Account>  lstAccountUpdate = new List<Account>(); 
     id Relatedaccountid ;
     List<Account> acclist = [SELECT Id,Nominated_Account_Owner__c,OwnerId FROM Account where id =:Accountids ];  
    
      List<Account_Data_Validity__c> advList = new List<Account_Data_Validity__c>();
    
     if(acclist.size() > 0 )
     {
          List<Account> accountlist =  [Select Id ,Nominated_Account_Owner__c,OwnerId     from Account where id =:acclist[0].Id    ]; 
         
       
           
          advList = [SELECT Account_Owner__c,Account_Record_Type__c,From_Account__c,From_Date__c,Id,
                        Market_Segment__c,Name,Record_Type__c,Sales_Matrix_Owner__c,To_Date__c,
                        Parent_Account__c,Account_Record_Type_Picklist__c
                        FROM
                        Account_Data_Validity__c   where   From_Account__c =: accountlist[0].id 
                        and  To_Date__c = null 
                        and Soft_Delete__c = false 
                        ];
         
         
           if(advList.size()>0)
         {  
               if(  advList[0].From_Date__c >= system.today().toStartofMonth().addDays(-1))
              {
               dadv = system.today();
               dadv = dadv.addMonths(1).toStartofMonth().addDays(-1);   
              }else   
              {dadv = system.today();
               dadv = dadv.toStartofMonth().addDays(-1);
              }
               advList[0].To_Date__c = dadv ;
              try 
              {
                 update advList;
               }catch(DmlException e) 
              {
                System.debug('The following exception has occurred: ' + e.getMessage());
             }
             Account_Data_Validity__c advNew = new Account_Data_Validity__c();            
             advNew.Account_Record_Type__c = advList[0].Account_Record_Type__c ;
             advNew.From_Account__c = accountlist[0].id ;
             advNew.From_Date__c = dadv.AddDays(1);
              if( acclist[0].Nominated_Account_Owner__c <> null )
             {
              advNew.Account_Owner__c =  acclist[0].Nominated_Account_Owner__c;
             }
            
              advNew.Market_Segment__c = advList[0].Market_Segment__c ;
              advNew.Sales_Matrix_Owner__c = advList[0].Sales_Matrix_Owner__c ; 
              advNew.Parent_Account__c =  advList[0].Parent_Account__c ;
              advNew.Account_Record_Type__c = advList[0].Account_Record_Type__c;
              advNew.Account_Record_Type_Picklist__c = advList[0].Account_Record_Type_Picklist__c ;
             
             try 
            {    
            insert advNew;
             }catch(DmlException e) 
           {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }
         }        
     }        
}
}