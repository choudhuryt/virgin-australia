public with sharing class PRISMDataReviewController {	
	
	//private List<DynamicSObject> dynamicSObjects;
    public String dynamicQuery { get; set;}
	
	public String CurrentAccountId{set;get;}
	public Boolean PRISMKeyRoute{get;set;}
	
	public String FooterContent{get
	{
		return 'hello hello new';
	}} 
	
	//keep all  POS_Revenue__c item
	private list<Type> TypeList{set; get;}
	private list<AnnualAndMonthly> AnnualAndMonthlyAllObject{get;set;}
	
	//keep POSRevenue total value
	private Decimal VAYTDAmount = 0; 
	private Decimal VAYTDCount = 0;
	private Decimal YYYTDTotalAmount = 0;
	private Decimal YYYTDCount=0;

	//keep PRISM Key Route total value
	private Decimal VAAmountCurrentYear = 0;
	private Decimal VACountCurrentYear = 0;
	private Decimal VAAmountPreviousYear = 0;
	private Decimal VACountPreviousYear = 0;

	private List<AggregateResult> groupedStartAndEndDates;
	private list<POS_Revenue__c> POSRevenueObjectListMain;
	
	//PRISMKeyRoutesType
	private list<PRISMKeyRoutesWrapper> PRISMKeyRoutesWrapperList{set;get;}
	
	//constructor
	public PRISMDataReviewController()
	{
		dynamicQuery='';
		PRISMKeyRoute = false;
		if(CurrentAccountId == null)
		{
			CurrentAccountId = ApexPages.currentPage().getParameters().get('acid');
		}
		
		if(PRISMKeyRoutesWrapperList == null || PRISMKeyRoutesWrapperList.size()==0)
		{
			this.GetAllPRISMKeyRoute();
			if(PRISMKeyRoutesWrapperList.size()> 0)
			{
				PRISMKeyRoute = true;
			}
		}
	}
	
	//check the url account id is available or not
	public PageReference CheckAccount()
	{
		PageReference url = null;
		if(CurrentAccountId == null || CurrentAccountId=='' )
		{
			url = new PageReference('/home/home.jsp');
			url.setRedirect(true);  
		}
		return url;
	}
	
	public PageReference ViewDetail()
	{
		String Id = ApexPages.currentPage().getParameters().get('currentID');
		PageReference nextPage = new PageReference('/'+Id);
		nextPage.setRedirect(true);
    return nextPage;
	}
	public list<AnnualAndMonthly> GetAllPOSRevenueItem()
	{
		
		if(AnnualAndMonthlyAllObject == null 
		|| AnnualAndMonthlyAllObject.size()==0)
		{
			List<AggregateResult> RevenueTypeList
			 = [Select p.Revenue_Type__c 
				From POS_Revenue__c p 
				Where  Account__c = :CurrentAccountId 
				group by p.Revenue_Type__c];
			if(RevenueTypeList != null)
			{
				if(AnnualAndMonthlyAllObject == null)
				{
					AnnualAndMonthlyAllObject = new list<AnnualAndMonthly>();
				}
				
				//I have two Revenue_Type__c values 1.Annual Revenue 2.Monthly Revenue
				for(AggregateResult ar: RevenueTypeList)
				{
					String RevenueTypeC =  String.valueOf(ar.get('Revenue_Type__c'));
					TypeList = null;
					
					//Call get value function
					GetAllType(RevenueTypeC);
					AnnualAndMonthly AnnualAndMonthlyItem = new AnnualAndMonthly();
					if(TypeList!=null)
					{
						AnnualAndMonthlyItem.RevenueTypeName = RevenueTypeC;
						AnnualAndMonthlyItem.TypeObjectList = TypeList;
						if(AnnualAndMonthlyItem.TypeObjectList !=null && 
						AnnualAndMonthlyItem.TypeObjectList.size()>0)
						{
							AnnualAndMonthlyAllObject.add(AnnualAndMonthlyItem);
						}
					}
				}
			}
		}
		DeleteCollapseItem();
		return AnnualAndMonthlyAllObject;
	}
	
	private void GetAllType(String RevenueTypeC)
	{
		if(groupedStartAndEndDates == null 
			|| groupedStartAndEndDates.size()== 0)
		{
			/*groupedStartAndEndDates = 
			[Select p.Type__c,p.Year__c , p.Start_Date__c, p.End_Date__c 
			From POS_Revenue__c p 
			where	p.Account__c =:CurrentAccountId
			and p.Type__c in ('Domestic','International')
			and p.Revenue_Type__c in ('Annual','Monthly')
			group by p.Start_Date__c, p.End_Date__c,p.Type__c,p.Year__c
			order by p.Type__c asc, p.Start_Date__c desc];*/
			groupedStartAndEndDates = 
			[Select p.Type__c,p.Year__c , p.Start_Date__c, p.End_Date__c 
			From POS_Revenue__c p 
			where	p.Account__c =:CurrentAccountId
			and p.Type__c in ('Domestic','International')
			and p.Revenue_Type__c in ('Annual','Monthly')
			group by p.Start_Date__c, p.End_Date__c,p.Type__c,p.Year__c
			order by p.Type__c asc, Year__c, p.Start_Date__c desc];
			if(groupedStartAndEndDates != null 
			&& groupedStartAndEndDates.size()>0)
			{
				list<Date> StartDateList = new list<Date>();
				list<Date> EndDateList = new list<Date>();
				for(AggregateResult ar : groupedStartAndEndDates)
				{
					StartDateList.add(Date.valueOf(ar.get('Start_Date__c')));
					EndDateList.add(Date.valueOf(ar.get('End_Date__c')));
				}
				POSRevenueObjectListMain  = 
				[Select p.Year__c, p.YY_YTD_Total_Amount__c, 
				p.YY_YTD_Count__c, p.YY_YTD_Count_Share__c, 
				p.YY_YTD_Amount_Share__c, p.VA_YTD_Total_Amount__c, 
				p.VA_YTD_Count__c, p.VA_YTD_Count_Share__c, 
				p.VA_YTD_Amount_Share__c, p.Type__c, 
				p.Total_Estimated_Revenue__c, p.SystemModstamp, 
				p.Start_Date__c, p.PRISM_Record_ID__c, p.POS__c, p.OwnerId, 
				p.Opportunity__c, p.Name, p.Lead__c, p.LastModifiedDate, 
				p.LastModifiedById, p.IsDeleted, p.International_Revenue__c, p.Id, 
				p.End_Date__c, p.Domestic_Revenue__c, p.Date_Range__c, p.CreatedDate, 
				p.CreatedById, p.Contracted_Revenue_Percentage__c, 
				p.Contracted_Revenue_Objective__c, 
				p.Contract__c, p.Account__c, p.ALL_YTD_Total_Count__c, 
				p.ALL_YTD_Total_Amount__c,
				p.Revenue_Type__c,
				p.Month__c 
				From POS_Revenue__c p
				where p.Start_Date__c in :StartDateList
				and p.End_Date__c in :EndDateList
				and p.Account__c = :CurrentAccountId
				and p.Type__c in ('Domestic','International')
				and p.Revenue_Type__c in ('Annual','Monthly')
				order by p.Type__c asc, Start_Date__c desc];
			}
		}
		SearchValue(groupedStartAndEndDates,POSRevenueObjectListMain, RevenueTypeC);
	}
	
	private void SearchValue(list<AggregateResult> groupedStartAndEndDates,
							 list<POS_Revenue__c> POSRevenueObjectListSearchValue,
							 String RevenueTypeC)
	{
		String CurrentType = null;
		Type TypeItem = new Type();
		list<POSRevenueWrapper> POSRevenueWrapperList = new list<POSRevenueWrapper>();
		Boolean needAdd = false;
		for (AggregateResult ar : groupedStartAndEndDates)
		{			
			VAYTDAmount = 0;
			VAYTDCount = 0;
			YYYTDTotalAmount = 0;
			YYYTDCount = 0;
			
			String POSType =  String.valueOf(ar.get('Type__c'));
			if(CurrentType == null)
			{
				CurrentType = POSType;
				TypeItem.TypeName=CurrentType;
				needAdd = true;
			}
			if(CurrentType != POStype) 
			{
				TypeItem = new Type();
				CurrentType = POSType;
				TypeItem.TypeName=CurrentType;
				POSRevenueWrapperList = new list<POSRevenueWrapper>();
				needAdd = true;
			}
			Date StartDate = Date.valueOf(ar.get('Start_Date__c'));
			Date EndDate = Date.valueOf(ar.get('End_Date__c'));
			String YearC = String.valueOf(ar.get('Year__c'));
			
			//assign date range
			POS_Revenue__c POSRevenue = new POS_Revenue__c();
			POSRevenue.Type__c =POSType; 
			POSRevenue.Start_Date__c = StartDate; 
			POSRevenue.End_Date__c = EndDate;
			POSRevenue.Year__c = YearC;
			POSRevenueWrapper POSRevenueWrapperItem = new POSRevenueWrapper();
			
			//ExtractDataFromList to get all list belong to current header
			list<POS_Revenue__c> POSRevenueListTmp =   
								ExtractDataFromList(StartDate,
								EndDate, 
								POSType,
								POSRevenueObjectListSearchValue,
								RevenueTypeC,yearC);
			
			//pass revenue to header 
			POSRevenueWrapperItem.POSRevenueHeader = POSRevenue;
			
			//pass total values for current list 
			POSRevenueWrapperItem.VAYTDTotalAmount = VAYTDAmount;
			POSRevenueWrapperItem.VAYTDCount = VAYTDCount;
			POSRevenueWrapperItem.YYYTDTotalAmount = YYYTDTotalAmount;
			POSRevenueWrapperItem.YYYTDCount = YYYTDCount;
			
			//pass list revenue items
			POSRevenueWrapperItem.POSRevenueList  = POSRevenueListTmp;
			
			if(POSRevenueListTmp !=null && POSRevenueListTmp.size()>0)
			{
				POSRevenueWrapperList.add(POSRevenueWrapperItem);
			}
			
			TypeItem.POSRevenueWrapperList = POSRevenueWrapperList;
					
			if(TypeList == null )
			{
				TypeList = new list<Type>();
			}
			if(needAdd)
			{
				/*
					replace the following code by those
					TypeList.add(TypeItem);
					TypeItem = new Type();
					needAdd= false;
					if any error
				*/
				//just for test, can be removed if any errors.
				if(TypeItem.POSRevenueWrapperList != null 
				&& TypeItem.POSRevenueWrapperList.size() > 0)
				{
					TypeList.add(TypeItem);
					TypeItem = new Type();
					needAdd= false;
				}
			}
		}
	}
	private list<POS_Revenue__c> ExtractDataFromList(Date StartDate, 
				Date EndDate,
				String POStype,
				list<POS_Revenue__c> POSRevenueObjectListIn,
				String RevenueTypeC, String yearC)
	{
		list<POS_Revenue__c> POSRevenueTmpList = new list<POS_Revenue__c>();
		for(Integer i = POSRevenueObjectListIn.size()-1; i>=0 ; i--)
		{
			if(RevenueTypeC=='Annual')
			{
				if(
					POSRevenueObjectListIn[i].Start_Date__c == StartDate 
					&& POSRevenueObjectListIn[i].End_Date__c == EndDate 
					&& POSRevenueObjectListIn[i].Type__c == POStype
					&& POSRevenueObjectListIn[i].Revenue_Type__c == RevenueTypeC )
				{
					POSRevenueTmpList.add(POSRevenueObjectListIn[i]);
					VAYTDAmount = VAYTDAmount + POSRevenueObjectListIn[i].VA_YTD_Total_Amount__c;  
					VAYTDCount = VAYTDCount + POSRevenueObjectListIn[i].VA_YTD_Count__c;
					YYYTDTotalAmount =  YYYTDTotalAmount + POSRevenueObjectListIn[i].YY_YTD_Total_Amount__c;
					YYYTDCount = YYYTDCount   +  POSRevenueObjectListIn[i].YY_YTD_Count__c;
					POSRevenueObjectListIn.remove(i);
				}
			}
			else if(RevenueTypeC=='Monthly')
			{
				if(POSRevenueObjectListIn[i].Type__c == POStype
					&& POSRevenueObjectListIn[i].Revenue_Type__c == RevenueTypeC
					&& POSRevenueObjectListIn[i].Year__c ==YearC)
				{
					POSRevenueTmpList.add(POSRevenueObjectListIn[i]);
					VAYTDAmount = VAYTDAmount + POSRevenueObjectListIn[i].VA_YTD_Total_Amount__c;  
					VAYTDCount = VAYTDCount + POSRevenueObjectListIn[i].VA_YTD_Count__c;
					YYYTDTotalAmount =  YYYTDTotalAmount + POSRevenueObjectListIn[i].YY_YTD_Total_Amount__c;
					YYYTDCount = YYYTDCount   +  POSRevenueObjectListIn[i].YY_YTD_Count__c;
					POSRevenueObjectListIn.remove(i);
				}
			}
		}
		return POSRevenueTmpList;
	}
	
	//PRISM Key Route
	public list<PRISMKeyRoutesWrapper> GetAllPRISMKeyRoute()
	{
		if(PRISMKeyRoutesWrapperList == null || PRISMKeyRoutesWrapperList.size()==0)
		{
			PRISMKeyRoutesWrapperList = new list<PRISMKeyRoutesWrapper>();
			List<AggregateResult> groupedStartAndEndDates 
			=[Select p.Type__c, p.Start_Date__c, p.End_Date__c 
			From PRISM_Key_Routes__c p
			where p.Account__c = :CurrentAccountId
			and p.Type__c != null
			group by  p.Type__c, p.Start_Date__c, p.End_Date__c
			order by  p.Type__c asc];
			
			List<PRISM_Key_Routes__c> PRISMKeyRoutesDomestic = 
			[Select p.VA_Share_of_Count_Previous_Year__c,
			p.VA_Share_of_Count_Current_Year__c, 
			p.VA_Share_of_Amount_Previous_Year__c,
			p.VA_Share_of_Amount_Current_Year__c, 
			p.VA_Count_Previous_Year__c, 
			p.VA_Count_Current_Year__c, 
			p.VA_Amount_Previous_Year__c,
			p.VA_Amount_Current_Year__c, 
			p.Type__c, p.SystemModstamp, 
			p.Start_Date__c, 
			p.PRISM_Record_ID__c, 
			p.Origin__c, 
			p.Order_Number__c, 
			p.Name,
			p.Id, 
			p.End_Date__c, p.Destination__c, p.Date_Range__c,
			p.CreatedDate, p.CreatedById, p.Account__c,
			p.Origin_Full__c,
			p.Destination_Full__c 
			From PRISM_Key_Routes__c p
			Where Account__c = :CurrentAccountId
			order by p.Order_Number__c desc];
			
			PRISM_Key_Routes__c PRISMKeyRoutescTmp = new PRISM_Key_Routes__c();
			for (AggregateResult ar : groupedStartAndEndDates)
			{
				PRISMKeyRoutescTmp = new PRISM_Key_Routes__c();
				String TypeC =  String.valueOf(ar.get('Type__c'));
				Date StartDate = Date.valueOf(ar.get('Start_Date__c'));
				Date EndDate = Date.valueOf(ar.get('End_Date__c'));
				
				PRISMKeyRoutescTmp.Type__c = TypeC;
				PRISMKeyRoutescTmp.Start_Date__c = StartDate;
				PRISMKeyRoutescTmp.End_Date__c = EndDate;
				PRISMKeyRoutesWrapper PRISMKeyRoutesWrapperTmp = new PRISMKeyRoutesWrapper();
				PRISMKeyRoutesWrapperTmp.PRISMKeyRoutesHeader = PRISMKeyRoutescTmp;
				
				VAAmountCurrentYear = 0;
				VACountCurrentYear = 0;
				VAAmountPreviousYear = 0;
				VACountPreviousYear = 0;
				
				list<PRISM_Key_Routes__c> PRISMKeyRoutesTmp =  
				SearchPRISMKeyRoute(TypeC,StartDate, EndDate,PRISMKeyRoutesDomestic,10);
				if(PRISMKeyRoutesTmp!=null && PRISMKeyRoutesTmp.size()>0)
				{
					PRISMKeyRoutesWrapperTmp.PRISMKeyRoutesDetailList = PRISMKeyRoutesTmp;
					PRISMKeyRoutesWrapperTmp.VAAmountCurrentYear= VAAmountCurrentYear;
					PRISMKeyRoutesWrapperTmp.VACountCurrentYear=VACountCurrentYear;
					PRISMKeyRoutesWrapperTmp.VAAmountPreviousYear=VAAmountPreviousYear;
					PRISMKeyRoutesWrapperTmp.VACountPreviousYear=VACountPreviousYear;
					PRISMKeyRoutesWrapperList.add(PRISMKeyRoutesWrapperTmp);
				}
				PRISMKeyRoutesWrapperTmp = new PRISMKeyRoutesWrapper(); 
				PRISMKeyRoutescTmp = new PRISM_Key_Routes__c();
			}
		}
		DeleteCollapseItem();
		return PRISMKeyRoutesWrapperList;
	}
	
	private List<PRISM_Key_Routes__c>  SearchPRISMKeyRoute(String Type,
	Date StartDate, Date EndDate,List<PRISM_Key_Routes__c> obj, 
	Integer limitNumber)
	{
		List<PRISM_Key_Routes__c> tmpList = new List<PRISM_Key_Routes__c>();
		Integer lim = 0;
		for(Integer i = obj.size()-1; i >=0; i --)
		{
			if(obj[i].Start_Date__c == StartDate 
				&& obj[i].End_Date__c== EndDate 
				&& obj[i].Type__c == Type)
			{
				tmpList.add(obj[i]);
				VAAmountCurrentYear=VAAmountCurrentYear 
														+	obj[i].VA_Amount_Current_Year__c;  
				VACountCurrentYear= VACountCurrentYear 
														+	obj[i].VA_Count_Current_Year__c;
				VAAmountPreviousYear=	VAAmountPreviousYear
														+	obj[i].VA_Amount_Previous_Year__c;
				VACountPreviousYear=VACountPreviousYear
														+	obj[i].VA_Count_Previous_Year__c;
				obj.remove(i);
				lim++;
			}
			if(lim==limitNumber)
			{
				break;
			}
		}
		return tmpList;
	}
	public PageReference viewAsPDF() 
	{
		PageReference pdfPage = Page.PRISMDataReviewPDF;
		pdfPage.setRedirect(true);
		if(dynamicQuery!=null)
		{
			pdfPage = new PageReference('/apex/PRISMDataReviewPDF?delid='+dynamicQuery);
		}
		return pdfPage;
	}
	
	//delete all collapes revenue
	private void DeleteCollapseItem()
	{
		String ItemName = ApexPages.currentPage().getParameters().get('delid');
		if(ItemName != null)
		{
			String[] ListName = ItemName.split(';');
			Boolean DeleteParent=false;
			String ParentName='';
			for(String s : ListName)
			{
				if(!s.trim().contains('-'))
				{
					if(s=='Annual' || s=='Monthly')
					{
						if(AnnualAndMonthlyAllObject!=null)
						{
							for(Integer i=AnnualAndMonthlyAllObject.size()-1; i >=0;i--)
							{
								if(AnnualAndMonthlyAllObject[i].RevenueTypeName==s)
								{
									AnnualAndMonthlyAllObject.remove(i);
								}
							}
						}
					}
					else if(s=='Key Routes')
					{
						PRISMKeyRoutesWrapperList= null;
						PRISMKeyRoute = false;
					}
				}
				else
				{
					String[] Sectiontitle=s.split('-');
					if(Sectiontitle.size()==2)
					{
						for(Integer i=AnnualAndMonthlyAllObject.size()-1; i >=0;i--)
						{
							if(AnnualAndMonthlyAllObject[i].RevenueTypeName==Sectiontitle[0].trim())
							{
								for(Integer j=AnnualAndMonthlyAllObject[i].TypeObjectList.size()-1;j>=0; j--)
								{
									if(AnnualAndMonthlyAllObject[i].TypeObjectList[j].TypeName==Sectiontitle[1].trim())
									{
										AnnualAndMonthlyAllObject[i].TypeObjectList.remove(j);	
									}
								}
							}
						}
					}
				}
			}
			for(Integer i=AnnualAndMonthlyAllObject.size()-1; i>=0; i--)
			{
				if(AnnualAndMonthlyAllObject[i].TypeObjectList == null 
				|| AnnualAndMonthlyAllObject[i].TypeObjectList.size()<1 )
				{
					AnnualAndMonthlyAllObject.remove(i);
				}
			}
		}
	}
	//get current viewing account
	public String CurrentAccountName
	{
		set;
		get
		{
			if(CurrentAccountId !=null)
			{
				Account account = [Select a.Name 
														From Account a 
														Where a.Id=:CurrentAccountId];
			if(account!=null)
			{
					return account.Name;
			}
			}
			return '';
		}
	}
	
	//End PRISM Key Route
	public class AnnualAndMonthly
	{
		public String RevenueTypeName{get;set;}
		public List<Type> TypeObjectList{set;get;} 
		public  AnnualAndMonthly(){}
	}
	public class Type
	{
		public list<POSRevenueWrapper> POSRevenueWrapperList
		{
			set;
			get;
		}
		public String TypeName
		{
			set;
			get;
		}
		public Type(){}		
	}
	
	
	//this class is created to keep the relationship between date rang and list
	public class POSRevenueWrapper
	{
		public POS_Revenue__c POSRevenueHeader{get; set;}
		public Decimal VAYTDTotalAmount{
			get;			
			set;
		}
		public Decimal VAYTDCount{
			get;			
			set;
		}	
		public Decimal YYYTDTotalAmount{
			get;			
			set;
		}
		public Decimal YYYTDCount{
			get;			
			set;
		}
			
		public list<POS_Revenue__c> POSRevenueList{get; set;}
		public POSRevenueWrapper()
		{	
		}
	}
	public class PRISMKeyRoutesWrapper
	{
		public Decimal VAAmountCurrentYear{set;get;}
		public Decimal VACountCurrentYear {set;get;}
		public Decimal VAAmountPreviousYear{set;get;}
		public Decimal VACountPreviousYear{set;get;}
		public PRISM_Key_Routes__c PRISMKeyRoutesHeader{set;get;}
		public list<PRISM_Key_Routes__c> PRISMKeyRoutesDetailList{set;get;}
		public PRISMKeyRoutesWrapper(){}
	}
}