public with sharing class PrismContractDataController {
    
public String CurrentContractId{set;get;} 
public List<Contract_Performance__c> domregsearchResults {get;set;}
public List<Contract_Performance__c> cplist = new List<Contract_Performance__c>();
public List<Contract_Performance__c> cpgraph = new List<Contract_Performance__c>();

public String paramValue {
	get;
    
	// *** setter is NOT being called ***
	set {
		paramValue = value;
		System.debug('value: '+value);
	}
} 
     
   public PrismContractDataController() 
    {
        if(CurrentContractId == null)
		{
			CurrentContractId= ApexPages.currentPage().getParameters().get('id');
		}
        
       List<aggregateResult> latestpreflist =[SELECT Contract_Term__c, MAX(Profile_Month__c) maxpfmth
                                                                                FROM Contract_Performance__c
                                                                               WHERE Contract__c = :CurrentContractId
                                                                               and Start_Date__c <= TODAY 
                                                                                GROUP BY Contract_Term__c
                                                             order by MAX(Profile_Month__c),Contract_Term__c ]; 
        
   if (latestpreflist.size() > 0)
    {    
     for(AggregateResult ag :latestpreflist)
     {
       
         string term = String.valueOf(ag.get('Contract_Term__c')) ;
         string profilemonth = String.valueOf(ag.get('maxpfmth')) ;
         
         
         
         Contract_Performance__c maxconperf = new Contract_Performance__c();
         
         maxconperf = [ SELECT id,Contract_Term__c,Profile_Month__c,To_Date_Amount_Share__c,
                                To_Date_Market_Share__c,To_Date_Performace_Amount__c,Requirement__c,
                             	Measure__c,Variance__c,Fulfilled__c,Reporting_Period__c
                                    FROM Contract_Performance__c where 
                                    Contract__c = :CurrentContractId
                                    and  Contract_Term__c  = :term
                                    and   Profile_Month__c = :profilemonth
                       order by Profile_Month__c
                         ]   ;
         
        cplist.add(maxconperf);
     }
     
     if (cplist.size() > 0 )
        {
         domregsearchResults =  cplist ;   
        }
   }else
   {
      ApexPages.addMessage( new ApexPages.message( ApexPages.severity.INFO,  'There is no Prism CTA data for this contract' ));  
   }     
    }

 public PageReference GetChartData() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.PrismContractWithDomData ;
        newPage.getParameters().put('Cid', CurrentContractId);  
        newPage.getParameters().put('CPid', this.paramValue); 
        return newPage ;
    } 
    
}