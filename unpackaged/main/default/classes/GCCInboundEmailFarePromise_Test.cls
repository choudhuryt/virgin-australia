/**
 * @Description: Test class for GCCInboundEmailFarePromise
 * @UpdateBy : Cloudwerx
 */
@isTest
private class GCCInboundEmailFarePromise_Test {
	
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'serge@bs2.com.au', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        INSERT testContactObj;
    }
    
    @isTest
    private static void testGCCInboundEmailFarePromise() {
        Id contactId = [SELECT Id FROM Contact]?.Id;
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        // setup the data for the email
        email.subject = 'Test Fare Promise Form';
        email.fromname = 'FirstName LastName';
        env.fromAddress = 'ssr@email.com';
        email.htmlBody = 'Fare Promise Claim Form Primary Passenger Details First Name:SergeLast Name:LauriouEmail Address:serge@bs2.com.auPhone No:414958923 Velocity Frequent Flyer No: 2102788283 Your Virgin Australia Itinerary Virgin Australia Booking Reference:  SERGEL Date booking was made: 12 Jun, 2019 Was your booking made via the Virgin Australia Website: Yes Is your booking solely a domestic Australian Itinerary Yes Eligible Lower Fare Details Website Address:  https://www.qantas.com.au Date the fare was viewed: 12 Jun, 2019 Time the fare was viewed: 02: 15 AM Total booking amount of all passengers (inclusive of mandatory fees,taxes and surcharges excluding credit card fees and commission): 400 The content of this e-mail, including any attachments, is a confidential communication between Virgin Australia Airlines Pty Ltd (Virgin Australia) or its related entities (or the sender if this email is a private communication) and the intended addressee and is for the sole use of that intended addressee. If you are not the intended addressee, any use, interference with, disclosure or copying of this material is unauthorized and prohibited. If you have received this e-mail in error please contact the sender immediately and then delete the message and any attachment(s). There is no warranty that this email is error, virus or defect free. This email is also subject to copyright. No part of it should be reproduced, adapted or communicated without the written consent of the copyright owner. If this is a private communication it does not represent the views of Virgin Australia or its related entities. Please be aware that the contents of any emails sent to or from Virgin Australia or its related entities may be periodically monitored and reviewed. Virgin Australia and its related entities respect your privacy. Our privacy policy can be accessed from our website: www.virginaustralia.com';
        // add an attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfile.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        Test.startTest();
        // call the email service class and test it with the data in the testMethod
        GCCInboundEmailFarePromise emailProcess = new GCCInboundEmailFarePromise();
        Messaging.InboundEmailResult inboundMessageResult = emailProcess.handleInboundEmail(email, env);
        Test.stopTest();
        // Asserts
        system.assertEquals(true, inboundMessageResult.success);
        // Asserts
        Id farePromiseQueueId = [SELECT Id, Name FROM Group WHERE Name = 'Fare Promise' LIMIT 1]?.Id;
        Id FPRecordTypeId = [SELECT Id FROM RecordType WHERE Name ='GCC Fare Promise']?.Id;
        Case insertedCaseObj = [SELECT Id, Description, Subject, HTML_Email__c, Origin, status, ownerId,  
                                RecordTypeId, ContactId, First_Name__c, Last_Name__c, 
                                Contact_Email__c, Contact_Phone__c, Velocity_Number__c FROM Case WHERE contactId =:contactId];
        system.assertEquals('Fare Promise', insertedCaseObj.Description);
        system.assertEquals(email.htmlBody, insertedCaseObj.HTML_Email__c);
        system.assertEquals(email.subject, insertedCaseObj.Subject);
        system.assertEquals('Web', insertedCaseObj.Origin);
        system.assertEquals('New', insertedCaseObj.status);
        system.assertEquals(farePromiseQueueId, insertedCaseObj.ownerId);
        system.assertEquals(FPRecordTypeId, insertedCaseObj.RecordTypeId);
        system.assertEquals(contactId, insertedCaseObj.ContactId);
        system.assertEquals('Serge', insertedCaseObj.First_Name__c);
        system.assertEquals('Lauriou', insertedCaseObj.Last_Name__c);
        system.assertEquals('serge@bs2.com.au', insertedCaseObj.Contact_Email__c);
        system.assertEquals('414958923', insertedCaseObj.Contact_Phone__c);
        system.assertEquals('2102788283', insertedCaseObj.Velocity_Number__c);
        
        //Asserts
        List<Attachment> insertedAttachments = [SELECT Id, Name, ParentId, Body FROM Attachment];
        system.assertEquals(true, insertedAttachments.size() > 0);
        system.assertEquals(insertedCaseObj.Id, insertedAttachments[0].ParentId);
        system.assertEquals(attachment.fileName, insertedAttachments[0].Name);
        system.assertEquals(attachment.body, insertedAttachments[0].Body);
    }
    
    @isTest
    private static void testGCCInboundEmailFarePromiseException() {
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        // setup the data for the email
        email.subject = 'Test Fare Promise Form';
        email.fromname = 'FirstName LastName';
        env.fromAddress = 'ssr@email.com';
        email.htmlBody = 'First Name: test Last Name: test ';
        
        Test.startTest();
        // call the email service class and test it with the data in the testMethod
        GCCInboundEmailFarePromise emailProcess = new GCCInboundEmailFarePromise();
        Messaging.InboundEmailResult inboundMessageResult = emailProcess.handleInboundEmail(email, env);
        Test.stopTest();
        // Asserts
        system.assertEquals(false, inboundMessageResult.success);
    }
}