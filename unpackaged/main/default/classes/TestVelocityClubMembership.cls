/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestVelocityClubMembership {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Account acc = new Account();
        CommonObjectsForTest comForTest= new CommonObjectsForTest();
        acc = comForTest.CreateAccountObject(0);
       
        insert acc;
        
        
               Contact contactDetails = new Contact();
      			 contactDetails.AccountId = acc.id;
      			 contactDetails.Assistant_Email__c = 'test1@test.com';
				 contactDetails.AssistantName ='tester';
				 contactDetails.Title ='mr';
				 contactDetails.Velocity_Number__c = '1111111111';
				 contactDetails.Velocity_Status__c ='';
				 contactDetails.AssistantPhone = '1234';
				 contactDetails.Phone ='1234';
				 contactDetails.Salutation ='Mr';
       			 contactDetails.FirstName = 'Test';
        		 contactDetails.LastName = 'Test';
        		 contactDetails.Email = 'test@test.com';
        		 contactDetails.Salutation ='Mr';
        		 contactDetails.Velocity_Member__c =true;
        		 insert contactDetails;
        		 
        		 Velocity_Status_Match__c vsm = new Velocity_Status_Match__c();
        		 vsm.RecordTypeId ='012900000009IauAAE';
        		 vsm.Account__c = acc.id;
        		 vsm.Approval_Status__c = 'Draft';
        		 vsm.Passenger_Velocity_Number__c ='1111111111';
        		 vsm.Contact__c = contactdetails.id;
                 vsm.Delivery_To__c ='Account Manager';
                 vsm.Status_Match_or_Upgrade__c = 'Club';
                 vsm.Organisation_Board_Seats_Held__c ='1';
                 vsm.Other_Club_Members_from_the_Org__c ='2';
                 vsm.Justification__c = 'test';
                 vsm.Total_Domestic_Expenditure__c ='1';
                 vsm.Total_International_Revenue_Objective__c ='2';
                 vsm.Domestic_Contract_Revenue_Objective__c = '1';
                 vsm.Date_Requested__c = Date.today();
                 insert vsm;
        
    }
}