/**
 * @description       : Test class for Utilities
 * @Created By         : Cloudwerx
 * @Updated By         : Cloudwerx
**/
@isTest
private class Test_Utilities {
	@isTest
    private static void testUtilitiesFunctions() {
        Test.startTest();
        Datetime convertedDT = Utilities.convert2DateTime('2022-01-01T12:15:15Z');
        Datetime blankDT = Utilities.convert2DateTime('');
        String converted2String = Utilities.convert2DateTimeString('30/01/2014');
        String blankConverted2String = Utilities.convert2DateTimeString('');
        String dt = Utilities.convertDateTime(Datetime.newInstance(2022, 01, 01, 12, 15, 15));
        String blankedDt = Utilities.convertDateTime(null);
        Date convertedDate = Utilities.convert2Date('2022-01-09');
        Date blankConvertedDate = Utilities.convert2Date('');
        Date dateConverted = Utilities.convertStringToDate('2012-01-16T18:23:47');
        Datetime mergedDateTime = Utilities.mergeDateTime('2012-01-16', '18:23:47');
        Datetime blankMergedDateTime = Utilities.mergeDateTime('', '');
        String convertedDateToStr = Utilities.convertDate2String(Date.newInstance(2022, 01, 09));
        List<String> flightNumbers = Utilities.formatFlightNumber('VA0052');
        Id recordypeId = Utilities.getRecordTypeId('Account', 'SmartFly');
        Id blankRecordypeId = Utilities.getRecordTypeId('', '');
        Id ownerId = Utilities.getOwnerId(UserInfo.getUserId());
        Id blankOwnerId = Utilities.getOwnerId(null);        
        Id ownerIdByName = Utilities.getOwnerIdByName(UserInfo.getName());
        Id blankOwnerIdByName = Utilities.getOwnerIdByName('');        
        Test.stopTest();
        //Asserts
        System.assertEquals(Datetime.newInstance(2022, 01, 01, 12, 15, 15), convertedDT);
		System.assertEquals(null, blankDT);
        System.assertEquals('2014-01-30T00:00:00', converted2String);
		System.assertEquals(null, blankConverted2String);
        System.assertEquals('2022-01-01T12:15:15', dt);
		System.assertEquals(null, blankedDt);
        System.assertEquals(Date.newInstance(2022, 01, 09), convertedDate);
		System.assertEquals(null, blankConvertedDate);
        System.assertEquals(Date.newInstance(2012, 01, 16), dateConverted);
        System.assertEquals(Datetime.newInstance(2012, 01, 16, 18, 23, 47), mergedDateTime);
        System.assertEquals(null, blankMergedDateTime);
        System.assertEquals('2022-01-09', convertedDateToStr);
        System.assertEquals('0052', flightNumbers[1]);
        System.assertEquals([SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'SmartFly'].Id, recordypeId);
        System.assertEquals(null, blankRecordypeId);
        System.assertEquals(UserInfo.getUserId(), ownerId);
        System.assertEquals(null, blankOwnerId);
    }
}