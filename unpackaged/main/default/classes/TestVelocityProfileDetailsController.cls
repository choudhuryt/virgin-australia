@isTest
public class TestVelocityProfileDetailsController{

    static testMethod void testConstructor() {
        VelocityProfileDetailsController testObject = new VelocityProfileDetailsController();
        System.assert(testObject.velocity != null);
    }
    
    static testMethod void testQueryVelocity() {
        // setup mock service
        Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());
        
        Case tmp = new Case(Velocity_Number__c = 'Son-test-001');
        insert tmp;
        
        Velocity__c velocity = new Velocity__c(Case__c = tmp.Id);
        insert velocity;
        System.assert(velocity.Id != null);
        VelocityProfileDetailsController testObject = new VelocityProfileDetailsController();
        Velocity__c newVelocity = testObject.queryVelocity(velocity.Id);
        System.assert(newVelocity != null);
        System.assertEquals(velocity.Id, newVelocity.Id);
    }
    
    static testMethod void testVelocityDetails(){
        VelocityDetail vd = new VelocityDetail();
    }
}