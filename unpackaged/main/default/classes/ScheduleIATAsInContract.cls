/** 
 *  File Name		: ScheduleIATAsInContract 
 *
 *  Description		: This batch class updates the IATAs in Contract object.  This custom object
 * 					  is used to store all the IATA numbers that apply to a contract.  When a 
 * 					  deal/contract is made with an account, this also applies to all the child
 *					  accounts.  We need to collect all the IATA numbers of the child accounts
 *					  and then link them to each contract.  
 *
 *                    The actual processing happen in: IATAsInContract, which this class calls.
 *					    
 *  Copyright		: © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
 *  @author			: Steve Kerr
 *  Date			: 22nd June 2015
 *
 *  Notes			: 
 *
 **/ 
global class ScheduleIATAsInContract implements Schedulable{

	global void execute(SchedulableContext sc) {

		IATAsInContract iatasInContract = new IATAsInContract();

		// Set to address for summary email
		iatasInContract.emailTo = new String[] {'steve.kerr2@virginaustralia.com','salesforce.admin@virginaustralia.com'};

		// Set the master query for the batch
		iatasInContract.query = 
				'SELECT AccountId, StartDate, EndDate, Status, Account.Account_Type__c, Account.RecordTypeId ' + 
				'FROM Contract ' + 
				'Where RecordType.Name like \'%Industry%\' ' +
				'and Status <> \'Deactivated\' ' +
				'and StartDate >= LAST_YEAR';
				
		// Note: If you change the above query, you may also need to update the matching
		//       contract query in IATAsInContract.cls

		// process x records per batch
		Database.executeBatch(iatasInContract, 5); 

	} 

}