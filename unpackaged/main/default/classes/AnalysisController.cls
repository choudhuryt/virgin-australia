public with sharing class AnalysisController {

    public Analysis__c currentInstance {get; set;}
    public List<AnswerStructure> currentAnswerList {get; set;}
    public Opportunity currentOpportunity {get; set;}
    public String questionID {get; set;}
    public String swotID {get; set;}

    /**
    * Constructor
    */
    public AnalysisController(ApexPages.StandardController myController) {

        // Get user's profile in order to restrict access to Manage Questions
        Id profileId = userinfo.getProfileId();
        Profile currentProfile = [SELECT Name FROM profile where Id = :profileId];
        
        if(System.currentPageReference().getUrl().contains('manageAnalysis') &&
            !(currentProfile.Name.contains('Manager') || currentProfile.Name.contains('User') || currentProfile.Name.contains('manager') || currentProfile.Name.contains('System Administrator')))
            throw new CustomException(currentProfile.Name + 'You need to be part of a Manager Profile to access this page!');

        String analysisID = System.currentPageReference().getParameters().get('id');

                
        try {
            if (analysisID == null) { // New questionaire

                // Check if there is an opportunity ID in Query String
                String opptyID = System.currentPageReference().getParameters().get('opptyid');
                if (opptyID != null)
                    currentOpportunity = [
                        SELECT Id, Name, AccountId 
                        FROM Opportunity 
                        WHERE Id = :opptyID];
                else 
                    throw new CustomException('Need an opportunity ID...');
                
                // Generate a new analysis
                currentInstance = new Analysis__c();
            }
            else {
                if (analysisID != null) {
                    initialiseAnalysis(analysisID);
                }
                else {
                    throw new CustomException('Need an Analysis ID...');
                }
            }
        }
        catch(CustomException ex){
            ApexPages.addMessages(ex);
        }

        
    }

    /*
    * Public methods
    */
    
    public PageReference create() {
            
        try {
            // Set opportunity lookup
            currentInstance.Opportunity__c = currentOpportunity.Id;
            insert currentInstance;
        }
        catch(CustomException ex){
            ApexPages.addMessages(ex);
        }   

        PageReference thePage = new PageReference('/apex/answerAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference modify() {
        
        updateAnalysis();
                    
        PageReference thePage = new PageReference('/apex/answerAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    }    

    public PageReference modifyManage() {
        
        updateAnalysis();
                    
        PageReference thePage = new PageReference('/apex/manageAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    }
    
    public PageReference saveandback() {
        
        updateAnalysis();
        
        PageReference thePage;          
        if (currentOpportunity != null)         
            thePage = new PageReference('/' + currentOpportunity.Id);
        else
            thePage = new PageReference('/apex/answerAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    } 

    public PageReference saveandbackManage() {
        
        updateAnalysis();
        
        PageReference thePage;          
        thePage = new PageReference('/apex/answerAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    } 
      
    public PageReference newQuestion() {
        
        updateAnalysis();
        
        // Get the number of questions for this analysis
        Integer nextOrder = [SELECT Id FROM Analysis_Question__c WHERE Analysis__r.Id = :currentInstance.Id].size();
                
        Analysis_Question__c newAnswer = new Analysis_Question__c();
        newAnswer.Analysis__c=currentInstance.Id;
        newAnswer.Title__c = 'To be completed';
        newAnswer.Order__c = nextOrder+1;
        insert newAnswer;
            
        PageReference thePage = new PageReference('/apex/manageAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    } 

    public PageReference addSWOT() {
        
        updateAnalysis();
        
        // Get the question Id
        String questionID = System.currentPageReference().getParameters().get('questionID');
        if (questionID == null)
            throw new CustomException('Question Id is not specified...');
            
        Analysis_SWOT__c newSWOT = new Analysis_SWOT__c();
        newSWOT.Analysis_Question__c=questionID;
        insert newSWOT;
            
        PageReference thePage = new PageReference('/apex/answerAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    } 

    public PageReference removeSWOT() {
        
        updateAnalysis();
        
        // Get the SWOT Id
        String swotID = System.currentPageReference().getParameters().get('swotID');
        if (swotID == null)
            throw new CustomException('SWOT Id is not specified...');
            
        Analysis_SWOT__c toDeleteSWOT = [SELECt Id FROM Analysis_SWOT__c WHERE Id = :swotID];
        delete toDeleteSWOT;
            
        PageReference thePage = new PageReference('/apex/answerAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    }
    
    public PageReference moveUp() {
            
        // Get the question Id
        String questionID = System.currentPageReference().getParameters().get('questionID');
        if (questionID == null)
            throw new CustomException('Question Id is not specified...');

        System.debug('3############## : ' + [SELECT Order__c FROM Analysis_Question__c WHERE ID = :questionID].Order__c);   

        updateAnalysis();
        System.debug('3############## : ' + [SELECT Order__c FROM Analysis_Question__c WHERE ID = :questionID].Order__c);   

        Analysis_Question__c q = [
            SELECT Id, Order__c
            FROM Analysis_Question__c 
            WHERE Id = :questionID];
        if (q == null)
            throw new CustomException('Question not found...');



        Analysis_Question__c q2 = [
            SELECT Id , Order__c 
            FROM Analysis_Question__c 
            WHERE Analysis__r.Id = :currentInstance.Id
            AND Order__c = :q.Order__c-1];
        if (q2 == null)
            throw new CustomException('Previous question not found...');

        q.Order__c = q.Order__c - 1;
        q2.Order__c = q2.Order__c + 1;
            
        update q;
        update q2;
        
        PageReference thePage = new PageReference('/apex/manageAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    } 

    public PageReference moveDown() {
            
        // Get the question Id
        String questionID = System.currentPageReference().getParameters().get('questionID');
        if (questionID == null)
            throw new CustomException('Question Id is not specified...');

        updateAnalysis();

        Analysis_Question__c q = [
            SELECT Id, Order__c
            FROM Analysis_Question__c 
            WHERE Id = :questionID];
        if (q == null)
            throw new CustomException('Question not found...');

        Analysis_Question__c q2 = [
            SELECT Id , Order__c 
            FROM Analysis_Question__c 
            WHERE Analysis__r.Id = :currentInstance.Id
            AND Order__c = :q.Order__c+1];
        if (q2 == null)
            throw new CustomException('Next question not found...');

        q.Order__c = q.Order__c + 1;
        q2.Order__c = q2.Order__c - 1;

        update q;
        update q2;
        PageReference thePage = new PageReference('/apex/manageAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    }   
    
    public PageReference deleteQuestion() {
        
        updateAnalysis();
        
        // Get the question Id
        String questionID = System.currentPageReference().getParameters().get('questionID');
        if (questionID == null)
            throw new CustomException('Question Id is not specified...');
        
        Analysis_Question__c q = [
            SELECT Id 
            FROM Analysis_Question__c 
            WHERE Id = :questionID];
        if (q == null)
            throw new CustomException('Question not found...');
            
        delete q;
        
        PageReference thePage = new PageReference('/apex/manageAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    }   
              
    public PageReference manage() {
        
        PageReference thePage = new PageReference('/apex/manageAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    } 

    public PageReference answer() {
        
        PageReference thePage = new PageReference('/apex/answerAnalysis?id=' + currentInstance.Id);
        thePage.setRedirect(true);
        return thePage;

    } 
 
    public PageReference cloneAnalysis() {
        
        Analysis__c newAnalysis = currentInstance.clone();
        newAnalysis.Is_Master__c = false;
        insert newAnalysis;
        
        List<Analysis_Question__c> newAnswers = new List<Analysis_Question__c>();
        for(AnswerStructure a:currentAnswerList) {
            Analysis_Question__c newAnswer = a.answer.clone();
            newAnswer.Analysis__c = newAnalysis.Id;
            newAnswers.add(newAnswer);
        }
            
        insert newAnswers;
        
        PageReference thePage = new PageReference('/apex/answerAnalysis?id=' + newAnalysis.Id);
        thePage.setRedirect(true);
        return thePage;

    } 
                  
    /*
    * Private methods
    */
    private void updateAnalysis() {
        
        List<Analysis_Question__c> toSave = new List<Analysis_Question__c>();
        List<Analysis_SWOT__c> SWOTtoSave = new List<Analysis_SWOT__c>();
        
        try {
            update currentInstance;
                
            for(AnswerStructure a:currentAnswerList) {
                Analysis_Question__c updatedAnswer = a.getAnswer();
                if (a.getSelectedRating()!=null) updatedAnswer.Selected_Score__c= Decimal.valueOf(a.getSelectedRating());
                if (a.getSelectedRating2()!=null) updatedAnswer.Selected_Score_2__c= Decimal.valueOf(a.getSelectedRating2());
                for (SWOTStructure s: a.swotList)
                    SWOTtoSave.add(s.getSwot());
                toSave.add(updatedAnswer);
            }
            
            if (toSave.size()>0) {
                update toSave;
                update SWOTtoSave;
            }
                
        }
        catch(CustomException ex){
            ApexPages.addMessages(ex);
        }  
    }
        
    
    private void initialiseAnalysis (String analysisID) {
        try {
            currentInstance = [
                SELECT Id, Name, Comment__c, Account__r.Id, Description__c, 
                    Is_Master__c, Opportunity__r.Id, Type__c, Status__c, 
                    Is_Editable__c, Show_Approval__c,
                    Show_Rating__c, Show_Rating_2__c, Rating_Title__c, Rating_Title_2__c, 
                    Range_1_Min__c, Range_1_Max__c, Range_1_Title__c,
                    Range_2_Min__c, Range_2_Max__c, Range_2_Title__c,
                    Range_3_Min__c, Range_3_Max__c, Range_3_Title__c,
                    Range_4_Min__c, Range_4_Max__c, Range_4_Title__c,
                    Range_5_Min__c, Range_5_Max__c, Range_5_Title__c,
                    CreatedDate, LastModifiedDate, Opportunity__r.Owner.FirstName, Opportunity__r.Owner.LastName
                FROM Analysis__c 
                WHERE Id =:analysisID];
            if (currentInstance == null)
                throw new CustomException('Analysis not found...');

            // Get opportunity
            if(currentInstance.Opportunity__r.Id != null) {
                currentOpportunity = [
                    SELECT Id, Name 
                    FROM Opportunity 
                    WHERE Id = :currentInstance.Opportunity__r.Id];
            }
            
            // Get answers for the instance
            List<Analysis_Question__c> answerList = [
                SELECT Id, Analysis__c, Description__c, Has_SWOT__c,
                    Range_Min__c, Range_Max__c, Selected_Score__c, Range_Description__c,
                    Range_Min_2__c, Range_Max_2__c, Selected_Score_2__c, Range_Description_2__c,
                    Comment_Title_1__c, Comment_Title_2__c, Comment_Title_3__c, 
                    Answer_1__c, Answer_2__c, Answer_3__c, Title__c, Order__c, 
                    (
                        SELECT Id, Type__c, Title__c, Description__c, Market__c
                        FROM Analysis_SWOT__r ORDER BY CreatedDate ASC
                    )
                FROM Analysis_Question__c 
                WHERE Analysis__c=:currentInstance.Id
                ORDER BY Order__c, Name];
            
            // Get the list of all Tasks related to the SWOTs
            Set<Id> SwotIds = new Set<Id>();
            for (Analysis_Question__c q: answerList) {
                for (Analysis_SWOT__c s: q.Analysis_SWOT__r)    
                    SwotIds.add(s.Id);
            }
            
            List<Task> lstTasks = [
                SELECT AccountId, Action_Outcome__c, Action_Strategy__c, Id, 
                    OwnerId, WhoId, CreatedDate, Description, ActivityDate, LastModifiedDate, 
                    WhatId, Priority, Status, Subject, RecordTypeId 
                FROM Task 
                WHERE WhatId IN :SwotIds];
                
            currentAnswerList = new List<AnswerStructure>();
            for(Analysis_Question__c aq: answerList) {
                AnswerStructure a = new AnswerStructure();
                a.setAnswer(aq);
                a.setSelectedRating(aq.Selected_Score__c);
                a.setSelectedRating2(aq.Selected_Score_2__c);
                List<SWOTStructure> swotList = new List<SWOTStructure>();
                for (Analysis_SWOT__c aa: aq.Analysis_SWOT__r){
                    SWOTStructure swot = new SWOTStructure();
                    swot.setSWOT(aa);
                    List<Task> lstSWOTTask = new List<Task>();
                    for (Task t: lstTasks) {
                        if (t.WhatId == aa.Id)
                            lstSWOTTask.add(t);
                    }
                    swot.setTaskList(lstSWOTTask);
                    swotList.add(swot);
                }
                a.setSwotList(swotList);
                currentAnswerList.add(a);
            }               

        }
        catch(CustomException ex){
            ApexPages.addMessages(ex);
        }           
    }
  
    
    /**
    * Wrapper classes
    */
    public with sharing class AnswerStructure{

        public Decimal selectedRating;
        public Decimal selectedRating2;
        public Analysis_Question__c answer;
        public Boolean isSelected;
        public SWOTStructure[] swotList;
        
        public Double getSelectedRating() { return selectedRating; }
        public Double getSelectedRating2() { return selectedRating2; }
        public Analysis_Question__c getAnswer() { return answer; }
        public Boolean getIsSelected() { return isSelected; }
        public SWOTStructure[] getSwotList() { return swotList; }

        public void setSelectedRating( Decimal i ) { this.selectedRating = i; }
        public void setSelectedRating2( Decimal i ) { this.selectedRating2 = i; }
        public void setAnswer( Analysis_Question__c a ) { this.answer = a; }
        public void setIsSelected( Boolean b ) { this.isSelected = b; }
        public void setSwotList( SWOTStructure[] l ) { this.swotList = l; }
 

        /**
        * Constructors
        */
        public AnswerStructure(){       
            this.selectedRating         = 0.0;
            this.selectedRating2        = 0.0;
        }

        /*public AnswerStructure( Analysis_Question__c q ){       
            this.selectedRating         = 0.0;
            this.selectedRating2        = 0.0;
            this.answer                 = new Analysis_Question__c();
            this.isSelected             = false;
            this.swotList               = null;
        }*/
    }    

    public with sharing class SWOTStructure{

        public Analysis_SWOT__c swot;
        public Task[] taskList;
        
        public Analysis_SWOT__c getSWOT() { return swot; }
        public Task[] getTaskList() { return taskList; }

        public void setTaskList( Task[] l ) { this.taskList = l; }
        public void setSWOT( Analysis_SWOT__c s ) { this.swot = s; }

        /**
        * Constructors
        */
        public SWOTStructure(){       
            this.taskList               = null;
        }
    }    
}