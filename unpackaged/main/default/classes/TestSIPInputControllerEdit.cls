/**
* @description       : Test Class for SIPInputControllerEdit
* @Created By         : Cloudwerx
* @Updated By         : Cloudwerx
**/
@isTest
private class TestSIPInputControllerEdit {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('Account Closed', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        
        SIP_Selection__c testSIPSelectionObj = TestDataFactory.createTestSIPSelection('Wholesale', 'Industry', 1, 'SIPSelection_' + Datetime.now());
        INSERT testSIPSelectionObj;
        
        SIP_Option__c testSIPOptionObj = TestDataFactory.createTestSIPOption('Demo Description', '1', testSIPSelectionObj.Id, 1, 'SIPOption_' + Datetime.now());
        INSERT testSIPOptionObj;
    }
    
    @isTest
    private static void testSIPInputControllerEditInit() {
        Contract testContractObj = [SELECT Id FROM Contract LIMIT 1];
        Test.startTest();
        SIPInputControllerEdit sipInputControllerEditCtrl  = new SIPInputControllerEdit();
        PageReference initPageRefernce = sipInputControllerEditCtrl.init();
        PageReference cancelPageReference = sipInputControllerEditCtrl.Cancel();
        PageReference saveAndClosePageReference = sipInputControllerEditCtrl.SaveAndClose();
        Test.stopTest();
        // Asserts
        System.assertEquals('/home/home.jsp', initPageRefernce.getUrl());
        System.assertEquals('/', cancelPageReference.getUrl());
    }
    
    @isTest
    private static void testSIPInputControllerEditApexMessage() {
        Contract testContractObj = [SELECT Id FROM Contract LIMIT 1];
        Test.startTest();
        ApexPages.currentPage().getParameters().put('cid', testContractObj.Id);
        SIPInputControllerEdit sipInputControllerEditCtrl  = new SIPInputControllerEdit();
        PageReference initPageRefernce = sipInputControllerEditCtrl.init();
        Test.stopTest();
        // Asserts
        System.assertEquals(true, ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
        System.assertEquals(null, initPageRefernce);
    }
    
    @isTest
    private static void testSIPInputControllerEdit() {
        Contract testContractObj = [SELECT Id FROM Contract LIMIT 1];
        
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'SIP Header Umbrella', 1000, 2000);
        testSIPHeaderObj.Umbrella_Target_Revenue__c  = 10;
        testSIPHeaderObj.SIP_Percentage__c  = 10;
        INSERT testSIPHeaderObj;
        
        SIP_Rebate__c testSipRebateObj = TestDataFactory.createTestSIPRebate(testContractObj.Id, testSIPHeaderObj.Id, 20, 30, 10); 
        INSERT testSipRebateObj;
        
        Test.startTest();
        PageReference pageRef = Page.SIPInputEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('cid', testContractObj.Id);
        SIPInputControllerEdit sipInputControllerEditCtrl  = new SIPInputControllerEdit();
        PageReference initPageRefernce = sipInputControllerEditCtrl.init();
        sipInputControllerEditCtrl.SIPOptionTitle = 'Title';
        sipInputControllerEditCtrl.DeleteId = new List<String> {'1'};
        List<SIPInputControllerEdit.SIPHeaderWrapper> sipHeaderWrapper = sipInputControllerEditCtrl.GetSIPOptionList();
        Test.stopTest();
        // Asserts
        System.assertEquals('/' + testContractObj.Id, sipInputControllerEditCtrl.GoBackLink);
        System.assertEquals(testSIPHeaderObj.Id, sipHeaderWrapper[0].headerItem.Id);
        System.assertEquals(1.0, sipHeaderWrapper[0].TempSIPCalculation);
        System.assertEquals(1, sipHeaderWrapper[0].sipRebateList.size());
    }
    
    @isTest
    private static void testSIPInputControllerEditSIPOption() {
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1];
        Contract testContractObj = [SELECT Id FROM Contract LIMIT 1];
        SIP_Option__c testSIPOptionObj = [SELECT Id FROM SIP_Option__c LIMIT 1];
        
        Contract testContractObj1 = TestDataFactory.createTestContract('Contract1', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj1.Estimated_Domestic_Revenue__c  = 1000;
        testContractObj1.Domestic_LY__c  = 2000;
        INSERT testContractObj1;
        
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj1.Id, 'SIP Header Umbrella', null, null);
        testSIPHeaderObj.IsTemplate__c = true;
        INSERT testSIPHeaderObj;
        testSIPHeaderObj.Description__c = testSIPHeaderObj.Id;
        UPDATE testSIPHeaderObj;
        
        SIP_Option_Join__c testSIPOptionJoin = TestDataFactory.createTestSIPOptionJoin(testSIPHeaderObj.Id, testSIPOptionObj.Id);
        INSERT testSIPOptionJoin;
        
        SIP_Rebate__c testSipRebateObj = TestDataFactory.createTestSIPRebate(testContractObj1.Id, testSIPHeaderObj.Id, 20, 30, 10); 
        INSERT testSipRebateObj;
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('sipid', testSIPOptionObj.Id);
        ApexPages.currentPage().getParameters().put('cid', testContractObj.Id);
        ApexPages.currentPage().getParameters().put('currentID', testSIPHeaderObj.Id);
        SIPInputControllerEdit sipInputControllerEditCtrl  = new SIPInputControllerEdit();
        List<SIPInputControllerEdit.SIPHeaderWrapper> sipHeaderWrapper = sipInputControllerEditCtrl.GetSIPOptionList();
        PageReference savePageReference = sipInputControllerEditCtrl.save();
        PageReference saveAndClosePageReference = sipInputControllerEditCtrl.SaveAndClose();
        PageReference goBackPageReference = sipInputControllerEditCtrl.GoBack();
        PageReference cancelPageReference = sipInputControllerEditCtrl.Cancel();
        Test.stopTest();
        // Asserts
        System.assertEquals(0, sipHeaderWrapper[0].headerItem.Est_Current_Full_Year_Revenue__c);
        System.assertEquals(0, sipHeaderWrapper[0].headerItem.Full_Last_Year_Actual_Revenue__c);
        System.assertEquals(0.00, sipHeaderWrapper[0].TempSIPCalculation);
        System.assertEquals(1, sipHeaderWrapper[0].sipRebateList.size());
        System.assertEquals('/' + testContractObj.Id, saveAndClosePageReference.getUrl());
        System.assertEquals('/home/home.jsp', goBackPageReference.getUrl());
    }
    
    @isTest
    private static void testSIPInputControllerEditSIPOptionRemoveHeader() {
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1];
        Contract testContractObj = [SELECT Id FROM Contract LIMIT 1];
        SIP_Option__c testSIPOptionObj = [SELECT Id FROM SIP_Option__c LIMIT 1];
        
        Contract testContractObj1 = TestDataFactory.createTestContract('Contract1', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj1.Estimated_Domestic_Revenue__c  = 1000;
        testContractObj1.Domestic_LY__c  = 2000;
        INSERT testContractObj1;
        
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj1.Id, 'SIP Header Umbrella', null, null);
        testSIPHeaderObj.IsTemplate__c = true;
        INSERT testSIPHeaderObj;
        testSIPHeaderObj.Description__c = testSIPHeaderObj.Id;
        UPDATE testSIPHeaderObj;
        
        SIP_Option_Join__c testSIPOptionJoin = TestDataFactory.createTestSIPOptionJoin(testSIPHeaderObj.Id, testSIPOptionObj.Id);
        INSERT testSIPOptionJoin;
        
        SIP_Rebate__c testSipRebateObj = TestDataFactory.createTestSIPRebate(testContractObj1.Id, testSIPHeaderObj.Id, 20, 30, 10); 
        INSERT testSipRebateObj;
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('sipid', testSIPOptionObj.Id);
        ApexPages.currentPage().getParameters().put('cid', testContractObj.Id);
        ApexPages.currentPage().getParameters().put('currentID', testSIPHeaderObj.Id);
        SIPInputControllerEdit sipInputControllerEditCtrl  = new SIPInputControllerEdit();
        List<SIPInputControllerEdit.SIPHeaderWrapper> sipHeaderWrapper = sipInputControllerEditCtrl.GetSIPOptionList();
        PageReference savePageReference = sipInputControllerEditCtrl.save();
        PageReference saveAndClosePageReference = sipInputControllerEditCtrl.SaveAndClose();
        PageReference goBackPageReference = sipInputControllerEditCtrl.GoBack();
        PageReference cancelPageReference = sipInputControllerEditCtrl.Cancel();
        PageReference removeHeaderPageReference = sipInputControllerEditCtrl.RemoveHeader();
        sipInputControllerEditCtrl.SIPHeaderDelete = new List<SIP_Header__c>{testSIPHeaderObj};
        sipInputControllerEditCtrl.DeleteHeader();
        Test.stopTest();
        // Asserts
        System.assertEquals(new List<SIPInputControllerEdit.SIPHeaderWrapper>(), sipHeaderWrapper);
    }
}