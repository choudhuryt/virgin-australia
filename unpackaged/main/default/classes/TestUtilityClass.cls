/*
 * @author: Warjie Malibago (Accenture CloudFirst)
 * @date: May 19, 2016
 * @description: This is the utility class for creating test data in test classes.
 * 
 * Updated by Cloudwerx : Changed profile name "VA System Administrator" to "Integration Profile" at line 15
 * 	Also updated recordTypeId references to utilities class
*/
@isTest
public class TestUtilityClass {
    public static User createTestUser(){
         String orgId = UserInfo.getOrganizationId();
         String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
         Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
         String uniqueName = orgId + dateString + randomInt;
         String profileId = [SELECT Id FROM Profile WHERE Name = 'Integration Profile' LIMIT 1].Id;
        User u = new User(
            ProfileId = profileId,
            FirstName = 'Test First Name',
            LastName = 'Test Last Name',
            Email = 'test@email.com',
            IsActive = TRUE,
            Username =  uniqueName + '@test' + orgId + '.org',
            Alias = 'tuser'   ,           
            TimeZoneSidKey = 'Australia/Sydney',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            Empower_Points_Allocated__c = 1000.00 // Added by CWerx            
        );
        return u;
    }
    
        
    public static Account createTestAccount(){
        Account a = new Account(
            Name = 'Test Account Name',
            ShippingStreet = 'Test Street',
            ShippingState = 'Test State',
            ShippingPostalCode = '12345',
            ShippingCountry = 'USA',
            Description = 'Test Description',
            BillingStreet = 'Test Street',
            BillingState = 'AU',
            BillingPostalCode = '12345',
            BillingCountry = 'USA',
            BillingCity = 'Test City' ,
            Account_Manager_Email_Address__c = 'test@virginaustralia.com',
            website= 'www.test.web.com',
            Industry_Type__c= 'Other',
            Send_To_Inteflow__c = false,
            OwnerId = UserInfo.getUserId(), // Added by CWerx
            Waiver_Points_Allocated__c = 10000.00 // Added by CWerx
        );
        return a;
    }
    
    public static Opportunity createTestOpportunity(Id accId){
        Opportunity o = new Opportunity(
            Name = 'Test Account Name',
            Accountid = accid,
            CloseDate = Date.newInstance(2019, 06, 01),
            StageName = 'Open'
        );
        return o;
    }
      public static Account createTestParentAccount(){
        Account a = new Account(
            Name = 'Test Parent Account Name',
            ShippingStreet = 'Test Street',
            ShippingState = 'Test State',
            ShippingPostalCode = '12345',
            ShippingCountry = 'USA',
            Description = 'Test Description',
            BillingStreet = 'Test Street',
            BillingState = 'AU',
            BillingPostalCode = '12345',
            BillingCountry = 'USA',
            BillingCity = 'Test City'            
        );
        return a;
    }
    public static Contact createTestContact(Id accId){
        Contact c = new Contact(
            AccountId = accId,
            Status__c = 'Active' ,
            //RecordTypeId = '012900000007krg',
            RecordTypeId = Utilities.getRecordTypeId('Contact', 'Contact'),
            Email = 'testcontact@email.com',
            FirstName = 'Contact First Name',
            LastName = 'Contact Last Name',
            Salutation = 'Mr.',
            Key_Contact__c  =true  ,
            title = 'Manager',
            Phone = '098737464664'
            );
        return c;
    }
    
    public static Lead createTestLead(){
        Lead l = new Lead(
            FirstName = 'Test Lead First Name',
            LastName = 'Test Lead Last Name',
            Country = 'Australia',
            Company = 'ABC Sydney House'
        );
        return l;
    }
    
    public static Contract createTestContract(Id accId){
        Contract c = new Contract(
            RecordTypeId = Utilities.getRecordTypeId('Contract', 'Accelerate POS/Rebate'),            
            Name = 'Test Contract Name',
            AccountId = accId,
            Status = 'Draft',
            Other_Details__c = 'Test Contract Summary',
            StartDate = Date.newInstance(2022, 01, 01),
            EndDate = Date.newInstance(2025, 12, 31),
            Currency_Contract_Terms_Stated_In__c = 'AUD',
            Currency_Payment_to_be_Made_In__c = 'AUD',
            Internal_Notes__c = 'Test Internal Notes',
            Type__c = 'Retail'
        );
        return c;
    }
    
    
    
    public static Contract createSmartflyTestContract(Id accId){
        Contract c = new Contract(
            RecordTypeId = Utilities.getRecordTypeId('Contract', 'SmartFly'),            
            Name = 'Test Contract Name',
            AccountId = accId,
            Status = 'Draft',
            Other_Details__c = 'Test Contract Summary',
            StartDate = Date.newInstance(2016, 06, 01),
            EndDate   = Date.newInstance(2020, 12, 31),
            Currency_Contract_Terms_Stated_In__c = 'AUD',
            Currency_Payment_to_be_Made_In__c = 'AUD',
            Internal_Notes__c = 'Test Internal Notes',
            Type__c = 'SmartFly'
        );
        return c;
    }
    
    public static Marketing_Spend_Down__c createTestMarketingSpendDown(Id accId, Id contractId){
        Marketing_Spend_Down__c m = new Marketing_Spend_Down__c(
            RecordTypeId = Utilities.getRecordTypeId('Marketing_Spend_Down__c', 'Marketing Fund Spend Down'),            
            Account__c = accId,
            Contract__c = contractId,
            Deduction_Category__c = 'Entertainment',
            Channel_Type__c = 'Retail',
            Currency__c = 'AUD',
            Amount_to_be_Deducted__c = 25,
            Campaign_Name__c = 'Test Campaign Name'
        );
        return m;
    }
    
    public static VA_Revenue__c createTestVARevenue(Id accId, Id contractId){
        VA_Revenue__c v = new VA_Revenue__c(
            Account__c = accId,
            Contract__c = contractId,
            Start_Date__c = Date.today() - 10,
            End_Date__c = Date.today() - 5,
            Active__c = TRUE
        );
        return v;
    }
    
    public static Blacklisted_Word__c createTestBlacklistedWord(){
        Blacklisted_Word__c b = new Blacklisted_Word__c(
            Word__c = 'Test Bad Word',
            Match_Whole_Words_Only__c = TRUE,
            Prevent_Post_on_Breach__c = FALSE,
            Substitution__c = '*****',
            Custom_Expression__c = 'Test Custom Expression',
            RegexValue__c = 'Test Regex Value'
        );
        return b;
    }
    
    public static Data_Validity__c createTestDataValidity(Id accId, Id agyId){
        Data_Validity__c d = new Data_Validity__c(
            From_Account__c = accId,
            AGY__c = agyId,
            To_Date__c = NULL
        );
        return d;
    }
    
    public static AGYLogin__c createTestAGYLogin(Id accId){
        AGYLogin__c d = new AGYLogin__c(
            Account__c = accId,
            AGYLogin__c = '123qwe'
        );
        return d;
    }
    
    public static TIDS__c createTestTIDS(Id accId){
        TIDS__c d = new TIDS__c(
            Account__c = accId,
            Effectivity_Changed__c = 'Yes',
            TIDS__c = '123qwe',
            TIDs_Effective_Date_From__c = Date.newInstance(2016, 01, 01),
            TIDs_Effective_Date_To__c = Date.newInstance(2016, 01, 01)
        );
        return d;
    }
    
    public static Velocity_Status_Match__c createTestVelocityStatusMatch(Id accId){
        Velocity_Status_Match__c d = new Velocity_Status_Match__c(
            Approval_Status__c = 'Draft',
            Account__c = accId,
            Passenger_Velocity_Number__c = '1234567890',
            Date_Requested__c = Date.today()
        );
        return d;
    }
    
    public static Contract_Fulfilment__c createTestContractFulfilment(Id accId, Id ctId){
        Contract_Fulfilment__c c = new Contract_Fulfilment__c(
        Account__c = accId,
        Status__c = 'Active',
        Contract__c = ctId,
        Nominated_Booking_Channels__c = 'Test Booking '
        );
        return c;
    }
    public static Spend__c createTestSpend(Id accId, Id ctId){
            Spend__c s = new Spend__c(
            Account__c = accId,  
            Contract__c =  ctId,    
            Eligible_Spend__c = 20000,
            Spend__c = 25000,            
            Category__c = 'D'    
        );
        return s;
    }


public static Spend__c createTTTestSpend(Id accId, Id ctId){
            Spend__c s = new Spend__c(
            Account__c = accId,  
            Contract__c =  ctId,    
        Eligible_Spend__c = 15000,
        Spend__c = 18000,            
            Category__c = 'T'    
        );
        return s;
    } 
    
    
    public static Spend__c createINTTestSpend(Id accId, Id ctId){
            Spend__c s = new Spend__c(
            Account__c = accId,  
            Contract__c =  ctId,    
        Eligible_Spend__c = 15000,
        Spend__c = 18000,            
            Category__c = 'L'    
        );
        return s;
    } 
   public static Contract_Performance__c createTestContractPerformanceAmount(Id ctId){
            Contract_Performance__c cp = new Contract_Performance__c(
            AccountName__c = 'Test Account Name',  
            Contract__c =  ctId,    
            Profile_Month__c = '2020-07',
            Contract_Term__c = 'Dom & Reg COS AU POS (Feb-2016 - Jan-2019)', 
           	Current_Month_Amount_Share__c = 50,
            Current_Month_Market_Share__c = 50,
            Current_Month_Performance_Amount__c = 25000,
            Measure__c = 'Amount',
            Requirement_Amount__c = 25000,
            To_Date_Market_Share__c = 50,
            To_Date_Amount_Share__c  = 50,
            To_Date_Performace_Amount__c = 30000    
        );
        
        return cp;       
       
    } 
    
    
     public static Contract_Performance__c createTestContractPerformanceShare(Id ctId){
            Contract_Performance__c cp = new Contract_Performance__c(
            AccountName__c = 'Test Account Name',  
            Contract__c =  ctId,    
            Profile_Month__c = '2020-07',
            Contract_Term__c = 'Dom & Reg COS AU POS (Feb-2016 - Jan-2019)', 
           	Current_Month_Amount_Share__c = 50,
            Current_Month_Market_Share__c = 50,
            Current_Month_Performance_Amount__c = 25000,
            Measure__c = 'Share of Amount',
            Requirement_Percentage__c = 60,
            To_Date_Market_Share__c = 50,
            To_Date_Amount_Share__c  = 50,
            To_Date_Performace_Amount__c = 30000    
        );
        
        return cp;       
       
    } 
    
        public static Contract_Performance__c createTestContractPerformanceNoReq(Id ctId){
            Contract_Performance__c cp = new Contract_Performance__c(
            AccountName__c = 'Test Account Name',  
            Contract__c =  ctId,    
            Profile_Month__c = '2020-07',
            Contract_Term__c = 'Dom & Reg COS AU POS (Feb-2016 - Jan-2019)', 
           	Current_Month_Amount_Share__c = 50,
            Current_Month_Market_Share__c = 50,
            Current_Month_Performance_Amount__c = 25000,
            Measure__c = 'No Requirement',
            Requirement_Percentage__c = 0,
            To_Date_Market_Share__c = 50,
            To_Date_Amount_Share__c  = 50,
            To_Date_Performace_Amount__c = 30000    
        );
        
        return cp;       
       
    } 
    
    
    public static Contract_Performance__c createTestContractPerformanceISH(Id ctId){
            Contract_Performance__c cp = new Contract_Performance__c(
            AccountName__c = 'Test Account Name',  
            Contract__c =  ctId,    
        Profile_Month__c = '2017-07',
        Contract_Term__c = 'ISH Trans-Tasman AU POS JFF (Feb-2016 - Jan-2019)',            
            To_Date_Market_Share__c = 50,
            To_Date_Amount_Share__c  = 50,
            To_Date_Performace_Amount__c = 30000    
        );
        
        return cp;       
       
    } 
    
     public static Contract_Performance__c createTestContractPerformanceNA(Id ctId){
            Contract_Performance__c cp = new Contract_Performance__c(
            AccountName__c = 'Test Account Name',  
            Contract__c =  ctId,    
        Profile_Month__c = '2017-07',
        Contract_Term__c = 'ILH NTH AMERICA POS AU JFF (MAR-2017 - JAN-2019)',            
            To_Date_Market_Share__c = 50,
            To_Date_Amount_Share__c  = 50,
            To_Date_Performace_Amount__c = 30000    
        );
        
        return cp;       
       
    } 
      public static Contract_Performance__c createTestContractPerformanceTT(Id ctId){
            Contract_Performance__c cp = new Contract_Performance__c(
            AccountName__c = 'Test Account Name',  
            Contract__c =  ctId,    
        Profile_Month__c = '2017-07',
        Contract_Term__c = 'ISH Trans-Tasman AU POS JFF (FEB-2016 - JAN-2019)',            
            To_Date_Market_Share__c = 50,
            To_Date_Amount_Share__c  = 50,
            To_Date_Performace_Amount__c = 30000    
        );
        
        return cp;       
       
    } 
    
    public static Contract_Performance__c createTestContractPerformanceAF(Id ctId){
            Contract_Performance__c cp = new Contract_Performance__c(
            AccountName__c = 'Test Account Name',  
            Contract__c =  ctId,    
        Profile_Month__c = '2017-07',
        Contract_Term__c = 'ILH AFRICA SQ POS AU (FEB-2016 - APR-2017)',            
            To_Date_Market_Share__c = 50,
            To_Date_Amount_Share__c  = 50,
            To_Date_Performace_Amount__c = 30000    
        );
        
        return cp;
    } 
    
    public static Contract_Performance__c createTestContractPerformanceAS(Id ctId){
            Contract_Performance__c cp = new Contract_Performance__c(
            AccountName__c = 'Test Account Name',  
            Contract__c =  ctId,    
        Profile_Month__c = '2017-07',
        Contract_Term__c = 'ILH ASIA SQ POS AU (FEB-2016 - JAN-2019)',            
            To_Date_Market_Share__c = 50,
            To_Date_Amount_Share__c  = 50,
            To_Date_Performace_Amount__c = 30000    
        );
        
        return cp;
    } 
    
     public static Contract_Performance__c createTestContractPerformanceEU(Id ctId){
            Contract_Performance__c cp = new Contract_Performance__c(
            AccountName__c = 'Test Account Name',  
            Contract__c =  ctId,    
        Profile_Month__c = '2017-07',
        Contract_Term__c = 'ILH UK/EU SQ POS AU (FEB-2016 - JAN-2019)',            
            To_Date_Market_Share__c = 50,
            To_Date_Amount_Share__c  = 50,
            To_Date_Performace_Amount__c = 30000    
        );
        
        return cp;
    } 
    
    public static Tourcodes__c createTestTourCodes(Id accId, Id ctId){
        Tourcodes__c c = new Tourcodes__c(
            Account__c = accId,
        Status__c = 'Active',
        Contract__c = ctId,
        Tourcode_Purpose__c = 'Tactical',
        Tourcode__c = '12345678'
        );
        return c;
    }
    
    public static IATA_Number__c createTestIATANumber(Id accId, Id ctId){
        IATA_Number__c c = new IATA_Number__c(
            Account__c = accId,
        Contract__c = ctId,
        Effectivity_Changed__c = 'Yes',
        IATA_Number__c = '0000795'
        );
        return c;
    }
    
    public static PCC__c createTestPCC(Id accId, Id ctId, Id iaId, Id iaId2){
        PCC__c c = new PCC__c(
            Account__c = accId,
        Contract__c = ctId,
        IATA_PCC__c = iaId,
        IATA_Number1__c = iaId2,
        PCC_Effective_Date_From__c = Date.newInstance(2016, 01, 01),
        PCC_Effective_Date_To__c = Date.newInstance(2016, 12, 31),
        IATA_PCC_GDS__c = 'Galileo',
        Effectivity_Changed__c = 'Yes',
        Name = 'Test PCC'
        );
        return c;
    }
    
    public static Account_Data_Validity__c createAccountDataValidity(Id accId, Id userId, Id accId2){
        Account_Data_Validity__c adv = new Account_Data_Validity__c(
            From_Account__c = accId,
            Account_Record_Type__c = Utilities.getRecordTypeId('Account', 'Accelerate'),            
            From_Date__c = Date.newInstance(2016, 01, 01),
            To_Date__c = Date.newInstance(2016, 12, 31),
            Account_Record_Type_Picklist__c = 'Accelerate',
            Soft_Delete__c = FALSE,
            Parent_Account__c = accId2,
            Market_Segment__c = 'Accelerate',
            Sales_Matrix_Owner__c = 'Accelerate',
            Account_Owner__c = userId
        );
        return adv;
    }
    
    public static Task createTestTask(Id whatId, String subject, String status, Id userId){
        Task t = new Task(
            Subject = subject,
            Status = status,
            ActivityDate = Date.today(),
            OwnerId = userId,
            WhatId = whatId,
            Priority = 'Normal'
        );
        return t;
    }
    
    public static Ticket_Request__c createTestTicketRequest(Id accId){
        Ticket_Request__c t = new Ticket_Request__c(
            Ticket_Request__c = accId,
            RecordTypeId = Utilities.getRecordTypeId('Ticket_Request__c', 'FOC'),            
            Approval_Status__c = 'Draft',
            Business_Justification__c = 'Test Business Justification',
            Travel_Type__c = 'Domestic',
            Ticket_Purpose__c = 'Prize Winner',
            Cabin__c = 'Economy',
            Taxes_Included__c = 'Yes',
            Prize_Certificate__c = 'Yes',
            Prize_Certificate_Validility_From__c = Date.newInstance(2016, 01, 01),
            Prize_Certificate_Validity_To__c = Date.newInstance(2016, 12, 31),
            Number_of_pax__c = 2,
            Request_Name__c = 'Test Request Name'
        );
        return t;
    }
    
    public static Waiver_Favour__c createTestWaiverFavour(Id accId, Id userId){
        Waiver_Favour__c w = new Waiver_Favour__c(
            Account__c = accId,
            Agency_Location__c = 'NSW',
            Agency_Name__c = 'Test Agency Name',
            Approval_Date__c = Date.today(),
            RecordTypeId = Utilities.getRecordTypeId('Waiver_Favour__c', 'Waiver & Favour'),            
            Requested_by_VA__c = userId,
            Enquiry_Method__c = 'Email',
            Waiver_Category__c = 'Cancellations & Refunds',
            Waiver_Type__c= 'Cancellation Fee',
            Business_Justification__c = 'Test Business Justification',
            Status__c = 'Approval',
            Currency__c = 'AUD'
        );
        return w;
    }
    
    
    
     public static Waiver_Favour__c createCommecialAdhocWaiver(Id accId, Id userId){
        Waiver_Favour__c w = new Waiver_Favour__c(
            Account__c = accId,
            Agency_Location__c = 'NSW',
            Agency_Name__c = 'Test Agency Name',
            Approval_Date__c = Date.today(),
            RecordTypeId = Utilities.getRecordTypeId('Waiver_Favour__c', 'Commercial Adhoc Waiver'),            
            Requested_by_VA__c = userId,
            Enquiry_Method__c = 'Email',
            Waiver_Category__c = 'Miscellaneous',
            Waiver_Type__c= 'Prepaid Baggage',
            Business_Justification__c = 'Test Business Justification',
            Status__c = 'Approved',
            Currency__c = 'AUD'
        );
        return w;
    }
    public static Waiver_Favour__c createTestHeadOfficePointWaiverFavour(Id accId, Id userId){
        Waiver_Favour__c w = new Waiver_Favour__c(
            Account__c = accId,            
            Agency_Location__c = 'NSW',
            Agency_Name__c = 'Test Agency Name',
            Approval_Date__c = Date.today(),
            RecordTypeId = Utilities.getRecordTypeId('Waiver_Favour__c', 'Waiver Point Head-Office Waiver'),            
            Requested_by_VA__c = userId,
            Enquiry_Method__c = 'Email',
            Waiver_Category__c = 'Cancellations & Refunds',
            Waiver_Type__c= 'Cancellation Fee',
            Waiver_Group_del__c = 'Commercial',
            Business_Justification__c = 'Test Business Justification',
            Status__c = 'Approved',
            Currency__c = 'AUD'
        );
        return w;
    }
    
      public static Waiver_Favour__c createTestTravelBankCompensationWaiver(Id accId, Id userId){
        Waiver_Favour__c w = new Waiver_Favour__c(
            Account__c = accId,            
            Agency_Location__c = 'NSW',
            Agency_Name__c = 'Test Agency Name',
            Approval_Date__c = Date.today(),
            Compensation_Amount__c = 10,
            //RecordTypeId = '0126F0000013oaz',
            RecordTypeId = Utilities.getRecordTypeId('Waiver_Favour__c', 'Travel Bank Compensation'),            
            Requested_by_VA__c = userId,
            Enquiry_Method__c = 'Email',
            Waiver_Category__c = 'Travel bank compensation',
            Waiver_Type__c= 'Travel bank compensation',
            Waiver_Group_del__c = 'VA policy',
            Business_Justification__c = 'Test Business Justification',
            Status__c = 'Approved',
            Currency__c = 'AUD'
        );
        return w;
    }
     public static Waiver_Favour__c createTestStoreLevelPointWaiverFavour(Id accId, Id userId){
        Waiver_Favour__c w = new Waiver_Favour__c(
            Account__c = accId,            
            Agency_Location__c = 'NSW',
            Agency_Name__c = 'Test Agency Name',
            Approval_Date__c = Date.today(),
            RecordTypeId = Utilities.getRecordTypeId('Waiver_Favour__c', 'Waiver Point Store Level Waiver'),            
            Requested_by_VA__c = userId,
            Enquiry_Method__c = 'Email',
            Waiver_Category__c = 'Cancellations & Refunds',
            Waiver_Type__c= 'Cancellation Fee',
            Waiver_Group_del__c = 'Commercial',
            Business_Justification__c = 'Test Business Justification',
            Status__c = 'Approved',
            Currency__c = 'AUD'
        );
        return w;
    }
    
    
   public static Waiver_Favour__c createAccountManagerPointWaiverFavour(Id accId, Id userId){
        Waiver_Favour__c w = new Waiver_Favour__c(
            Account__c = accId,            
            Agency_Location__c = 'NSW',
            Agency_Name__c = 'Test Agency Name',
            Approval_Date__c = Date.today(),
            RecordTypeId = Utilities.getRecordTypeId('Waiver_Favour__c', 'Waiver Point Account Manager Waiver'),            
            Requested_by_VA__c = userId,
            ownerid = userId,
            Enquiry_Method__c = 'Email',
            Waiver_Category__c = 'Cancellations & Refunds',
            Waiver_Type__c= 'Cancellation Fee',
            Waiver_Group_del__c = 'Commercial',
            Business_Justification__c = 'Test Business Justification',
            Status__c = 'Approved',
            Currency__c = 'AUD'
        );
        return w;
    }
     
    
     public static Waiver_Point__c createTestStoreLevelWaiverPoint(Id accId, Id acc1Id){
       Waiver_Point__c wp = new Waiver_Point__c(
            Account__c = accId, 
            Waiver_Point_Parent__c =  acc1Id,           
            Waiver_Points_Allocated__c= 50 ,
            Waiver_Points_Used__c = 0 ,
            Waiver_Points_Pending_Approval__c = 0 ,
            Waiver_Type__c = 'Account Store Level',
            Start_Date__c  =  Date.newInstance(2019, 07, 01) ,  
            End_Date__c    =  System.today() + 365
        );
        return wp;
    }
    
     public static Waiver_Point__c createTestHeadOfficeLevelWaiverPoint(Id accId){
       Waiver_Point__c wp = new Waiver_Point__c(
            Account__c = accId,           
            Waiver_Points_Allocated__c= 50 ,
            Waiver_Type__c = 'Account Head Office Level',
            Start_Date__c  =  Date.newInstance(2019, 07, 01) ,  
            End_Date__c    =   System.today() + 365
        );
        return wp;
    }
    
    public static Waiver_Point__c createTestTravelBankWaiverPoint(Id accId, Id userId)
    {        
        Waiver_Point__c wp = new Waiver_Point__c(
            //Account__c = '0016F00001sUAt6',           
            Account__c = accId,
            Travel_Bank_Compensation_Allowed__c = 1000,
            Travel_Bank_Compensation_Used__c = 100,
            Travel_Bank_Compensation_Pending__c = 100,
            Travel_Bank_Compensation_Draft__c = 2000,
            Waiver_Type__c = 'Travel Bank Compensation',
            Start_Date__c  =  Date.newInstance(2022, 07, 01),  
            End_Date__c    =   System.today() + 365            
        );
        return wp;
    }
    
    
    public static Waiver_Point__c createTestAMLevelWaiverPoint(Id userId){
       Waiver_Point__c wp = new Waiver_Point__c(
            Account_Manager__c = userId,           
            Waiver_Points_Allocated__c= 50 ,
            Waiver_Points_Used__c= 0 ,
           	Waiver_Points_Pending_Approval__c = 10,
            Waiver_Type__c = 'Account Manager Level',
            Start_Date__c  =  Date.newInstance(2019, 07, 01) ,  
            End_Date__c    =  System.today() + 365 
        );
        return wp;
    }
    
     public static Waiver_Ticket_Details__c createTestWaiverTicket(Id wfid){
        Waiver_Ticket_Details__c wt = new Waiver_Ticket_Details__c(
            Waiver_Favour__c = wfId,
            Original_Ticket_Number__c = '7951234567890',
            PNR__c= 'ABCDEF',
            Waiver_Amount__c = 100            
        );
        return wt;
    }
    
    
     public static Upgrade__c createTestUpgrade(Id accId, Id conId){
            Upgrade__c u = new Upgrade__c(
            Account__c = accId,
            Contact__c = conId,
            Flight_Details__c = 'One Way Upgrade',
            Approval_Flight_Status__c = 'Space Available',
            Justification__c = 'Test Business Justification',
            Date_Of_Departure__c = Date.today().addDays(30),
            PNR__c = 'QWERTY',
            Number_of_PAX__c = 3,
            Cabin_Requested__c = 'Premium Economy',
            Cabin_Currently_Booked_In__c = 'Economy',
            Levels_Up__c = '1',
            Inbound_Flight_Number__c = 'QWERTY',
            Flight_Number__c = 'QWERTY',
            Level_1__c = 'VIP(Club Member)'
        );
        return u;
    }
    
      public static Contract_Termination__c createTestContractTermination(Id accId, Id ctId){
        Contract_Termination__c ct = new Contract_Termination__c(
            Account__c = accId,
  			Status__c = 'Draft',
  			Contract__c = ctId
        );
        return ct;
    }
    
    
     public static Contract_Extension__c createTestContractExtension(Id accId, Id ctId){
        Contract_Extension__c ce = new Contract_Extension__c(
            Account__c = accId,
  			Status__c = 'Draft',
  			Contract__c = ctId
        );
        return ce;
    }
    
      public static Global_Sales_Activity__c createGlobalSalesActivity(){
        Global_Sales_Activity__c gsa = new Global_Sales_Activity__c(

        );
        return gsa;
    }
    
    
    
    public static Case createTestCase( ){
        Case c = new Case(
  			SuppliedEmail = 'testcontact@email.com',
  			Status = 'New',
            RecordTypeId = Utilities.getRecordTypeId('Case', 'GR Case')            
        );
        return c;
    }
}