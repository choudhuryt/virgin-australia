/** * File Name      : InternationalDiscountController
* Description        : This Apex Class is the Controller for InternationalDiscount VF Page
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Andy Burgess
* * Date             : 7 August 2012
* * Technical Task ID: 
* * Notes            :  The test class for this file is:  TestInternationalDiscountController
* Modification Log =============================================================== 
Ver Date Author Modification --- ---- ------ -------------
* */

public with sharing class USInternationalController {

     //standard controller and search results
     private ApexPages.StandardController controller {get; set;}
     public List<International_Discounts_USA_Canada__c> searchResults {get;set;}
     public List<International_Discounts_AUS_NZ__c> searchResults1 {get;set;}
     
 
 	 //flags
	 public integer usacanFlag {get; set;} 
	 public integer aunzFlag {get; set;}

	 
	 //contract instance
	 private Contract a;

   
    // Constructor
    
    public USInternationalController(ApexPages.StandardController myController) 
    {
    	a=(Contract)myController.getrecord();
    	usacanFlag=0;
    	aunzFlag=0;

    }
 
    //pre-processing prior to page load
     public PageReference initDisc() 
     {
	
	  searchResults= [
        SELECT Applicable_Routes_BNE_SYD_MEL_to__c, CreatedById, CreatedDate, IsDeleted,
         Int_DiscountUSACANLookupToContract__c, International_Discount_USA_CAN_Exists__c, Name, 
         Intl_Discount_USA_CAN_EligBk_Class__c, Intl_Discount_USA_CAN_EligBk_Class2__c, 
         Intl_Discount_USA_CAN_EligBk_Class3__c, Intl_Discount_USA_CAN_EligBk_Class4__c, 
         Intl_Discount_USA_CAN_EligBk_Class5__c, Intl_Discount_USA_CAN_EligBk_Class6__c, 
         Intl_Discount_USA_CAN_EligBk_Class7__c, Intl_Discount_USA_CAN_Eligible_Fare_Type__c, 
         Intl_Discount_USA_CAN_Eligible_FareType2__c, Intl_Discount_USA_CAN_Eligible_FareType3__c, 
         Intl_Discount_USA_CAN_Eligible_FareType4__c,Intl_Discount_USA_CAN_Eligible_FareType5__c,
         Intl_Discount_USA_CAN_Eligible_FareType6__c,Intl_Discount_USA_CAN_Eligible_FareType7__c,
         Intl_Discount_USA_CAN_Eligible_FareTyp8__c,
         Intl_Discount_USA_CAN_off_Published_Fare__c, Intl_Discount_USA_CAN_EligBk_Class8__c,
         Intl_Discount_USA_CAN_off_PublishedFare2__c, Intl_Discount_USA_CAN_off_PublishedFare3__c, 
         Intl_Discount_USA_CAN_off_PublishedFare4__c, Intl_Discount_USA_CAN_off_PublishedFare5__c, 
         Intl_Discount_USA_CAN_off_PublishedFare6__c, Intl_Discount_USA_CAN_off_PublishedFare7__c,
          LastModifiedById, LastModifiedDate, OwnerId, Id, SystemModstamp,Intl_Discount_USA_CAN_off_PublishedFare8__c 
          FROM International_Discounts_USA_Canada__c
          where Int_DiscountUSACANLookupToContract__r.id = :ApexPages.currentPage().getParameters().get('id')
          ORDER BY CreatedDate];
	  if(searchResults.size()>0)
	  {
	  	usacanFlag=1; 
	  }
       searchResults1= [
		SELECT CreatedById, CreatedDate, IsDeleted, 
		Name, IntlDisc_AUNZ_DiscOffPublishFare__c, IntlDisc_AUNZ_DiscOffPublishFare2__c, 
		IntlDisc_AUNZ_DiscOffPublishFare3__c, IntlDisc_AUNZ_DiscOffPublishFare4__c, 
		IntlDisc_AUNZ_DiscOffPublishFare5__c, IntlDisc_AUNZ_DiscOffPublishFare6__c, 
		IntlDisc_AUNZ_DiscOffPublishFare7__c, IntlDisc_AUNZ_EligibleBookClass__c, 
		IntlDisc_AUNZ_EligibleBookClass2__c, IntlDisc_AUNZ_EligibleBookClass3__c, 
		IntlDisc_AUNZ_EligibleBookClass4__c, IntlDisc_AUNZ_EligibleBookClass5__c, 
		IntlDisc_AUNZ_EligibleBookClass6__c, IntlDisc_AUNZ_EligibleBookClass7__c, 
		IntlDisc_AUNZ_EligibleFareType__c, IntlDisc_AUNZ_EligibleFareType2__c, 
		IntlDisc_AUNZ_EligibleFareType3__c, IntlDisc_AUNZ_Exist__c, IntlDisc_AUNZ_EligibleFareType4__c, IntlDisc_AUNZ_EligibleFareType5__c,
		IntlDisc_AUNZ_Lookup_To_Contract__c, IntlDisc_NZAU_ApplicableRtsBNE_SYD_MELto__c, IntlDisc_AUNZ_EligibleFareType6__c,IntlDisc_AUNZ_EligibleFareType7__c,
		IntlDisc_AUNZ_EligibleFareType8__c,
		LastModifiedById, LastModifiedDate, OwnerId, Id, SystemModstamp, IntlDisc_AUNZ_DiscOffPublishFare8__c, 
		 IntlDisc_AUNZ_EligibleBookClass8__c 
		FROM International_Discounts_AUS_NZ__c
	    where IntlDisc_AUNZ_Lookup_To_Contract__r.id = :ApexPages.currentPage().getParameters().get('id')
	    ORDER BY CreatedDate];
	  if(searchResults1.size()>0)
	  {
	  	aunzFlag=1;
	  }
	  
      return null;
      
      
  
   }
 
 

 
  // fired when the save records button is clicked
  public PageReference save() {
   // try {
     
      upsert searchResults1;
      upsert searchResults;

	UpdateContractPercentagesForContractCalculation();
      
   // } Catch (DMLException e) {
   //   ApexPages.addMessages(e);
   //   return null;
   //}
 
    //return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
    PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
		          //
		          thePage.setRedirect(true);
		          //
		          return thePage;
  }
  
  // fired when the save records button is clicked
  public PageReference qsave() {
   // try {
      upsert searchResults;
      upsert searchResults1;
      UpdateContractPercentagesForContractCalculation();
      
   // } Catch (DMLException e) {
   //   ApexPages.addMessages(e);
   //   return null;
   //}
 
    //return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
    PageReference thePage = new PageReference('/apex/USInternationalDiscountPage?id=' + a.Id);
		          //
		          thePage.setRedirect(true);
		          //
		          return thePage;
  }
  
  
 
  // takes user back to main record
  public PageReference cancel() {
   // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
   PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
		          //
		          thePage.setRedirect(true);
		          //
		          return thePage;
   
  }
  
 
  public PageReference newInterUSA() 
  {
 	
   if (searchResults.size()<1)
      {
    	searchResults.add
    	(
				new International_Discounts_USA_Canada__c
			      (
                    Int_DiscountUSACANLookupToContract__c = ApexPages.currentPage().getParameters().get('id'),
                    Intl_Discount_USA_CAN_Eligible_Fare_Type__c    =    'Business',
                    Intl_Discount_USA_CAN_Eligible_FareType2__c    =    'Business',
                    Intl_Discount_USA_CAN_Eligible_FareType3__c    =    'Business Saver',
                    Intl_Discount_USA_CAN_Eligible_FareType4__c    =    'Premium Economy',
                    
                    Intl_Discount_USA_CAN_Eligible_FareType5__c    =    'Premium Economy',
                    Intl_Discount_USA_CAN_Eligible_FareType6__c    =    'Economy',
                    Intl_Discount_USA_CAN_Eligible_FareType7__c    =    'Economy',
                    Intl_Discount_USA_CAN_Eligible_FareTyp8__c    =    'Economy',
                    
                    Intl_Discount_USA_CAN_EligBk_Class__c  =  'J',
                    Intl_Discount_USA_CAN_EligBk_Class2__c =  'C',            
                    Intl_Discount_USA_CAN_EligBk_Class3__c =  'D/I',
                    Intl_Discount_USA_CAN_EligBk_Class4__c =  'W',
                    Intl_Discount_USA_CAN_EligBk_Class5__c =  'R',
                    Intl_Discount_USA_CAN_EligBk_Class6__c =  'Y',
                    Intl_Discount_USA_CAN_EligBk_Class7__c =  'B',
                    Intl_Discount_USA_CAN_EligBk_Class8__c =  'H',
                    
                    Intl_Discount_USA_CAN_off_Published_Fare__c =  0,
                    Intl_Discount_USA_CAN_off_PublishedFare2__c =  0,
                    Intl_Discount_USA_CAN_off_PublishedFare3__c =  0,
                    Intl_Discount_USA_CAN_off_PublishedFare4__c =  0,
                    Intl_Discount_USA_CAN_off_PublishedFare5__c =  0,
                    Intl_Discount_USA_CAN_off_PublishedFare6__c =  0,
                    Intl_Discount_USA_CAN_off_PublishedFare7__c =  0,
                    Intl_Discount_USA_CAN_off_PublishedFare8__c =  0,
                    
                    International_Discount_USA_CAN_Exists__c = False,
                    Applicable_Routes_BNE_SYD_MEL_to__c = '  '
                   )                       
         );
            	    
            	  
            	    return null;
     }
    else{
  
   			 PageReference thePage = new PageReference('/apex/USInternationalDiscountPage?id=' + a.Id);
		     thePage.setRedirect(true);
   			 return thePage;
         }
  }
  
 
  
  public PageReference remUSACAN() 
  {
		International_Discounts_USA_Canada__c toDelete = searchResults[searchResults.size()-1];
			
		delete toDelete;
		
		 PageReference thePage = new PageReference('/apex/USInternationalDiscountPage?id=' + a.Id);
		          
		          thePage.setRedirect(true);
		          
		          return thePage;
  }
  
  
  
  
    public PageReference newInterAUNZ() 
  {
 	
   if (searchResults1.size()<1)
      {
    	searchResults1.add
    	(
				new International_Discounts_AUS_NZ__c
			      (
			        IntlDisc_AUNZ_Lookup_To_Contract__c = ApexPages.currentPage().getParameters().get('id'),
			        IntlDisc_AUNZ_EligibleFareType__c     = 'Business',
			        IntlDisc_AUNZ_EligibleFareType2__c    =	'Business',
			        IntlDisc_AUNZ_EligibleFareType3__c    =	'Business Saver',
			        IntlDisc_AUNZ_EligibleFareType4__c    =	'Premium Economy',
			        
			        IntlDisc_AUNZ_EligibleFareType5__c    =	'Premium Economy',
			        IntlDisc_AUNZ_EligibleFareType6__c    =	'Economy',
			        IntlDisc_AUNZ_EligibleFareType7__c    =	'Economy',
			        IntlDisc_AUNZ_EligibleFareType8__c    =	'Economy',
			        
			        IntlDisc_AUNZ_EligibleBookClass__c	    =   'J',
			        IntlDisc_AUNZ_EligibleBookClass2__c		=  	'C',		      
			        IntlDisc_AUNZ_EligibleBookClass3__c		=   'D/I',
			        IntlDisc_AUNZ_EligibleBookClass4__c	    =   'W',
			        IntlDisc_AUNZ_EligibleBookClass5__c       =   'R',
			        IntlDisc_AUNZ_EligibleBookClass6__c       =   'Y',
			        IntlDisc_AUNZ_EligibleBookClass7__c       =   'B',
			        IntlDisc_AUNZ_EligibleBookClass8__c       =   'H',
			        
			        IntlDisc_AUNZ_DiscOffPublishFare__c	     =0,
			        IntlDisc_AUNZ_DiscOffPublishFare2__c	 =0,
			        IntlDisc_AUNZ_DiscOffPublishFare3__c	 =0,
			        IntlDisc_AUNZ_DiscOffPublishFare4__c	 =0,
			        IntlDisc_AUNZ_DiscOffPublishFare5__c	 =0,
			        IntlDisc_AUNZ_DiscOffPublishFare6__c	 =0,
			        IntlDisc_AUNZ_DiscOffPublishFare7__c	 =0,
			        IntlDisc_AUNZ_DiscOffPublishFare8__c	 =0,
			        
			        IntlDisc_NZAU_ApplicableRtsBNE_SYD_MELto__c='',
			        IntlDisc_AUNZ_Exist__c = True
                   )                  
         );
            	    
            	   return null;
     }
    else{
  
   			 PageReference thePage = new PageReference('/apex/USInternationalDiscountPage?id=' + a.Id);
		     thePage.setRedirect(true);
   			 return thePage;
         }
  }
  
    public PageReference remAUNZ() 
  {
		International_Discounts_AUS_NZ__c toDelete = searchResults1[searchResults1.size()-1];
			
		delete toDelete;
		
		 PageReference thePage = new PageReference('/apex/USInternationalDiscountPage?id=' + a.Id);
		          
		          thePage.setRedirect(true);
		          
		          return thePage;
  }
  
   public Boolean UpdateContractPercentagesForContractCalculation(){
    
    
    List <Decimal> sortOrder = new List<Decimal>();
        if(searchResults != null){
        if(searchResults.size()>0){
      International_Discounts_USA_Canada__c intUSACan = new International_Discounts_USA_Canada__c();        
      intUSACan = searchResults.get(0);
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_Published_Fare__c); 
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare2__c);
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare3__c); 
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare4__c);
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare5__c); 
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare6__c); 
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare7__c);
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare8__c);
        }
    }
        if(searchResults1 != null){
      International_Discounts_AUS_NZ__c intAUnz = new International_Discounts_AUS_NZ__c();
      if (searchResults1.size()>0){
      intAUnz = searchResults1.get(0);
      
      sortOrder.add(intAUnz.IntlDisc_AUNZ_DiscOffPublishFare__c);
      sortOrder.add(intAUnz.IntlDisc_AUNZ_DiscOffPublishFare2__c);
      sortOrder.add(intAUnz.IntlDisc_AUNZ_DiscOffPublishFare3__c);
      sortOrder.add(intAUnz.IntlDisc_AUNZ_DiscOffPublishFare4__c);
      sortOrder.add(intAUnz.IntlDisc_AUNZ_DiscOffPublishFare5__c);
      sortOrder.add(intAUnz.IntlDisc_AUNZ_DiscOffPublishFare6__c);
      sortOrder.add(intAUnz.IntlDisc_AUNZ_DiscOffPublishFare7__c);
      sortOrder.add(intAUnz.IntlDisc_AUNZ_DiscOffPublishFare8__c);
        }
        }
        


      if(sortOrder.size() > 0){
  
      
      sortOrder.sort(); 
    
    Integer i = sortOrder.size();
    Decimal d = sortorder.get(i -1);
    
    Contract contract = new Contract();
    
    contract = [Select UpperPerInt__c From Contract where id = :a.id ];
    contract.UpperPerInt__c = d;
    update contract;
      }
    return true;
    
  }
 
}