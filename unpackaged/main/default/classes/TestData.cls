/**
 * @description       : TestData
**/
@isTest
public with sharing class TestData {

	
	/**
	* Set the necesary attributes used during test
	*/
    public static void createTestHierarchy(){
    	
    	TestUtilities testUtils = new TestUtilities();
    	//Set of Fields should be checked
    	Set<String> fieldsToCheck = new Set<String>{'AnnualRevenue', 'BillingCity','BillingCountry','BillingPostalCode','BillingState', 'BillingStreet', 'Description', 'ShippingCity', 'ShippingStreet', 'Name', 'ShippingState', 'ShippingPostalCode', 'ShippingCountry' };
		
		//Create my Parent Account 
		testUtils.createAccounts( 1 , fieldsToCheck );
 		testUtils.testAccList[0].Name = 'HIERARCHYTEST0';
 		testUtils.updateAccountList( fieldsToCheck );
 		
 		Account parentAccount = testUtils.testAccList[0];
        Id parentID = parentAccount.Id;
        System.Assert( parentID != null , 'Parent Id not found' );
        
        // Create 10 sub accounts
    	testUtils.createAccounts( 10 , fieldsToCheck );
    	Integer i = 0;
        for ( Account accAux : testUtils.testAccList ){ //Now i need change the names
        	accAux.Name = 'HIERARCHYTEST' + String.valueOf( i );
            i++;
        }
        testUtils.updateAccountList( fieldsToCheck );        
        
        List<Account> accountList = [ Select Id, parentID, Main_Corporate_TMC__c, name from account where name like 'HierarchyTest%' ORDER BY Name limit 10 ];
                
        for ( Integer x = 0; x < accountList.size(); x++ ){
            if ( accountList[x].name != 'HIERARCHYTEST0' ){
                accountList[x].parentID = parentID;
                accountList[x].Main_Corporate_TMC__c = parentID;
                parentID = accountList[x].Id; 
            }
        }
        
        testUtils.testAccList.clear();
        testUtils.testAccList.addAll( accountList );
        testUtils.updateAccountList( fieldsToCheck );

		// Create 10 sub accounts
		Account subTreeParent = [ Select id, parentID, name from account where name = 'HierarchyTest4' limit 10 ];
        parentID = subTreeParent.Id;
        testUtils.createAccounts( 10, fieldsToCheck );
    	 
		i = 0;
		for ( Account accAux : testUtils.testAccList ){ //Now i need change the names
        	accAux.Name = 'HIERARCHYTEST' + '4.' + String.valueOf( i );
        }
		testUtils.updateAccountList( fieldsToCheck );

        List<Account> subAccountsList = [ Select Id, parentID, Main_Corporate_TMC__c, Name from Account where Name like 'HierarchyTest4%' limit 10  ];
        for ( Integer z = 1; z < subAccountsList.size(); z++ ){
            subAccountsList[z].parentID = parentID;
            subAccountsList[z].Main_Corporate_TMC__c = parentID;
            parentID = accountList[z].Id; 
        }
        
        testUtils.testAccList.clear();
        testUtils.testAccList.addAll( subAccountsList );
    }

}