public class FCRUtilities 
{
	/****************************************************************************/	
    public static void removeDOMRegional(set<id> ContractIDSet)
    {
        list<Proposal_Table__c> ProposalTableList = new list<Proposal_Table__c>();
        map<id, id> ContractOptyIdMap = new map<id, id>();
        map<id, string> ContractIdTierMap = new map<id, string>();
        ProposalTableList = [select id, name, Opportunity__c, Contract__c, Template_Tier__c, FCR_Domestic_Tier__c, FCR_Remarks__c
                             from Proposal_Table__c where contract__c in :ContractIDSet and name in ('DOM Regional', 'DOM Mainline')];
        if ((ProposalTableList != null) && (ProposalTableList.size() > 0))
        {
            for (integer count = 0; count < ProposalTableList.size(); count++)
            {
                ProposalTableList[count].FCR_Opportunity_Back_Up__c = ProposalTableList[count].Opportunity__c;
                ProposalTableList[count].FCR_Contract_Back_Up__c = ProposalTableList[count].Contract__c;
                ProposalTableList[count].FCR_Processed__c	=	true;
                ProposalTableList[count].FCR_For_Deletion__c	=	true;
                ContractOptyIdMap.put(ProposalTableList[count].Contract__c, ProposalTableList[count].Opportunity__c);
                ContractIdTierMap.put(ProposalTableList[count].Contract__c, ProposalTableList[count].FCR_Domestic_Tier__c);
                ProposalTableList[count].Opportunity__c = null;
                ProposalTableList[count].Contract__c = null;
                if (string.isNotEmpty(ProposalTableList[count].FCR_Remarks__c))
                {
                    ProposalTableList[count].FCR_Remarks__c = ProposalTableList[count].FCR_Remarks__c +'  '+'Old DOM Record marked for Deletion';
                }    
                else
                {
                    ProposalTableList[count].FCR_Remarks__c = 'Old DOM Record marked for Deletion';
                }    
                
                
            }    
        }
        update ProposalTableList;
        createDOMRegional(ContractOptyIdMap, ContractIdTierMap, ContractIDSet);
    }
	
    
    /****************************************************************************/	
	public static void createDOMRegional(map<id, id> ContractOptyIdMap, map<id, string> ContractIdTierMap, set<id> ContractIDSet)
    {
        list<FCR_Proposal_Tier__c> FCRProposalTierList = new list<FCR_Proposal_Tier__c>();
        list<Proposal_Table__c> ProposalTableList = new list<Proposal_Table__c>();
        list<Market__c> MarketList = new list<Market__c>();
        MarketList = [SELECT Contract__c, Requested_Tier__c FROM Market__c where Contract__c in :ContractIDSet
                     and name = 'DOM Mainline'];
        if ((MarketList != null) && (MarketList.size() > 0))
        {
            for(Market__c Market : MarketList)
            {
                ContractIdTierMap.put(Market.Contract__c, Market.Requested_Tier__c);
            }    
            
        }    
        
        FCRProposalTierList = [select id, Discount_Off_Published_Fare__c, Eligible_Booking_Class__c, Eligible_Fare_Type__c, Name, Proposal_Table_Name__c, Sort_Order__c,
                               Template_Tier__c, Tier__c, VA_Eligible_Booking_Class__c from FCR_Proposal_Tier__c where
                               Proposal_Table_Name__c = 'DOM Mainline'];
        for (id contractId : ContractOptyIdMap.keySet())
        {
            if((FCRProposalTierList != null) && (FCRProposalTierList.size() > 0) && (ContractIdTierMap.containsKey(contractId)))
            {
                for (integer i = 0; i < FCRProposalTierList.size(); i++)
                {
                    if(ContractIdTierMap.get(contractId) == FCRProposalTierList[i].tier__c)
                    {
                        Proposal_Table__c ProposalTableRec = new Proposal_Table__c();
                    	ProposalTableRec.VA_ELIGIBLE_BOOKING_CLASS__c = FCRProposalTierList[i].VA_Eligible_Booking_Class__c;
                    	ProposalTableRec.DISCOUNT_OFF_PUBLISHED_FARE__c = FCRProposalTierList[i].Discount_Off_Published_Fare__c;
                    	ProposalTableRec.ELIGIBLE_FARE_TYPE__c = FCRProposalTierList[i].Eligible_Fare_Type__c;
                    	//ProposalTableRec.Tier__c = FCRProposalTierList[i].Tier__c;
                    	ProposalTableRec.Name = FCRProposalTierList[i].Proposal_Table_Name__c;
                        //ProposalTableRec.sort_order__c = FCRProposalTierList[i].sort_order__c;
                    	ProposalTableRec.Contract__c = contractId;
                    	ProposalTableRec.Opportunity__c = ContractOptyIdMap.get(contractId);
                    	ProposalTableRec.FCR_Created__c = true;
                        ProposalTableRec.FCR_Processed__c = true;
                        ProposalTableRec.FCR_Remarks__c = 'DOM Record created as per new Tier Table';
                    	ProposalTableList.add(ProposalTableRec);
                    }    
                    
                }    
            }       
        }    
        if (ProposalTableList.size() > 0)
        {
            insert(ProposalTableList);
        }
        system.debug('ContractOptyIdMap.keyset()>>'+ContractOptyIdMap.keyset());
        fixISHTT(ContractOptyIdMap.keyset());
    }    
    
    
    /****************************************************************************/	
    public static void fixISHTT(set<id> ContractIDSet)
    {
        list<Proposal_Table__c> ProposalTableList = new list<Proposal_Table__c>();
        list<Proposal_Table__c> ProposalTableListforAddition = new list<Proposal_Table__c>();
       	map<string, string> BaseClassClonedClassMap = new map<string, string>();
        BaseClassClonedClassMap.put('B', 'W');
        BaseClassClonedClassMap.put('E', 'R');
        BaseClassClonedClassMap.put('N', 'O');
        BaseClassClonedClassMap.put('Q', 'P');
        ProposalTableList = [select id, name, Opportunity__c, Contract__c, VA_ELIGIBLE_BOOKING_CLASS__c,
                             Discount_Off_Published_Fare__c, Eligible_Fare_Type__c, Tier__c, FCR_Remarks__c
                             from Proposal_Table__c where contract__c in :ContractIDSet and name in ('INT Short Haul', 'Trans Tasman TT', 'Trans Tasman TT ex NZ')];
        if ((ProposalTableList != null) && (ProposalTableList.size() > 0))
        {
            for (integer i = 0; i < ProposalTableList.size(); i++)
            {
                if ((ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'B') || (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'E') 
                    || (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'N') || (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'Q'))
                {
                    Proposal_Table__c ProposalTableRec = new Proposal_Table__c();
                    ProposalTableRec.DISCOUNT_OFF_PUBLISHED_FARE__c = ProposalTableList[i].Discount_Off_Published_Fare__c;
                    ProposalTableRec.ELIGIBLE_FARE_TYPE__c = ProposalTableList[i].Eligible_Fare_Type__c;
                    ProposalTableRec.Tier__c = ProposalTableList[i].Tier__c;
                    ProposalTableRec.Name = ProposalTableList[i].Name;
                    ProposalTableRec.Contract__c = ProposalTableList[i].contract__c;
                    ProposalTableRec.Opportunity__c = ProposalTableList[i].opportunity__c;
                    ProposalTableRec.FCR_Created__c = true;
                    ProposalTableRec.VA_ELIGIBLE_BOOKING_CLASS__c = BaseClassClonedClassMap.get(ProposalTableList[i].VA_Eligible_Booking_Class__c);
                    ProposalTableRec.FCR_Processed__c = true;
                    ProposalTableRec.FCR_Remarks__c = 'WROP Record created as per FCR Reqs';
                    ProposalTableListforAddition.add(ProposalTableRec);
                }
                else if ((ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'I') || (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'I*'))
                {
                    ProposalTableList[i].FCR_Opportunity_Back_Up__c = ProposalTableList[i].Opportunity__c;
                    ProposalTableList[i].FCR_Contract_Back_Up__c = ProposalTableList[i].Contract__c;
                    ProposalTableList[i].FCR_Processed__c	=	true;
                    ProposalTableList[i].FCR_For_Deletion__c	=	true;
                    if (string.isNotEmpty(ProposalTableList[i].FCR_Remarks__c))
                    {
                        ProposalTableList[i].FCR_Remarks__c = ProposalTableList[i].FCR_Remarks__c +'  '+'I / I* Record deleted as per FCR Reqs';
                    }    
                    else
                    {
                        ProposalTableList[i].FCR_Remarks__c = 'I / I* Record deleted as per FCR Reqs';
                    }
                    ProposalTableList[i].Opportunity__c = null;
                    ProposalTableList[i].Contract__c = null;
                }    
            }    
        }
        update ProposalTableList;
        insert ProposalTableListforAddition;
        updateOtherAirlines(ContractIDSet);
    }    
    
    
    /****************************************************************************/	
    public static void updateOtherAirlines(set<id> ContractIDSet)
    {
        list<Proposal_Table__c> ProposalTableList = new list<Proposal_Table__c>();
        ProposalTableList = [select id, name, Opportunity__c, Contract__c, VA_ELIGIBLE_BOOKING_CLASS__c,
                             Discount_Off_Published_Fare__c, Eligible_Fare_Type__c, Tier__c, FCR_Remarks__c
                             from Proposal_Table__c where contract__c in :ContractIDSet and name not in ('INT Short Haul', 'Trans Tasman TT', 'Trans Tasman TT ex NZ')
                             and Partner_Eligible_Booking_Class__c != ''];
        if ((ProposalTableList != null) && (ProposalTableList.size() > 0))
        {
            for (integer i = 0; i < ProposalTableList.size(); i++)
            {
            	ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS_BAK_UP__c = ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c;
                ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c = '';
                ProposalTableList[i].FCR_Processed__c	=	true;
                if (string.isNotEmpty(ProposalTableList[i].FCR_Remarks__c))
                {
                    ProposalTableList[i].FCR_Remarks__c = ProposalTableList[i].FCR_Remarks__c +'  '+'Eligible Booking Class Value Cleared';
                }    
                else
                {
                    ProposalTableList[i].FCR_Remarks__c = 'Eligible Booking Class Value Cleared';
                }
            }
        }
        update ProposalTableList;
        updateFareTypeVAClass(ContractIDSet);
    }

    
    /****************************************************************************/	
	public static void updateFareTypeVAClass(set<id> ContractIdSet)
    {
        list<FCR_Proposal_Tier__c> FCRProposalTierList = new list<FCR_Proposal_Tier__c>();
        list<Proposal_Table__c> ProposalTableList = new list<Proposal_Table__c>();
        FCRProposalTierList = [select id, Eligible_Fare_Type__c, VA_Eligible_Booking_Class__c from FCR_Proposal_Tier__c where
                               Proposal_Table_Name__c = 'DOM Mainline'
                               and Tier__c = 'Tier 1'];
        map<string, string> VABCFTMap = new map<string, string>();
        for (FCR_Proposal_Tier__c FCRProposalTier : FCRProposalTierList)
        {
            VABCFTMap.put(FCRProposalTier.VA_Eligible_Booking_Class__c, FCRProposalTier.Eligible_Fare_Type__c);
        }
        ProposalTableList = [select id, fcr_remarks__c, name, Opportunity__c, Contract__c, VA_ELIGIBLE_BOOKING_CLASS__c,
                             Discount_Off_Published_Fare__c, Eligible_Fare_Type__c, Tier__c
                             from Proposal_Table__c where contract__c in :ContractIDSet and VA_ELIGIBLE_BOOKING_CLASS__c != null];
        if ((ProposalTableList != null) && (ProposalTableList.size() > 0))
        {
            for (integer i = 0; i < ProposalTableList.size(); i++)
            {
                if (VABCFTMap.containskey(ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c))
                {
                    ProposalTableList[i].Eligible_Fare_Type_Bak_Up__c = ProposalTableList[i].Eligible_Fare_Type__c;
                    ProposalTableList[i].Eligible_Fare_Type__c = VABCFTMap.get(ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c);
                    ProposalTableList[i].FCR_Processed__c	=	true;
                	if (string.isNotEmpty(ProposalTableList[i].FCR_Remarks__c))
                    {
                        ProposalTableList[i].FCR_Remarks__c = ProposalTableList[i].FCR_Remarks__c +'  '+'VA Fare Type Updated';
                    }    
                    else
                    {
                        ProposalTableList[i].FCR_Remarks__c = 'VA Fare Type Updated';
                    }    
                }    
                 
            }  
        }
        update ProposalTableList;
        updateFareTypePartnerClass(ContractIdSet);
    }    
    
    
    
    public static void updateFareTypePartnerClass(set<id> ContractIdSet)
    {
        list<Proposal_Table__c> ProposalTableList = new list<Proposal_Table__c>();
        map<string, string> VABCFTMap = new map<string, string>();
        VABCFTMap.put('Elevate', 'Economy');
        VABCFTMap.put('Freedom', 'Economy');
        VABCFTMap.put('Getaway', 'Economy');
        VABCFTMap.put('Gateway', 'Economy');
        VABCFTMap.put('Business Tactical', 'Business');
        VABCFTMap.put('Business Saver', 'Business');
        ProposalTableList = [select id, fcr_remarks__c, name, Opportunity__c, Contract__c, Partner_Eligible_Booking_Class__c,
                             Discount_Off_Published_Fare__c, Eligible_Fare_Type__c, Tier__c
                             from Proposal_Table__c where contract__c in :ContractIDSet and Partner_Eligible_Booking_Class__c != null];
        if ((ProposalTableList != null) && (ProposalTableList.size() > 0))
        {
            for (integer i = 0; i < ProposalTableList.size(); i++)
            {
                system.debug(ProposalTableList[i].Eligible_Fare_Type__c+'    '+VABCFTMap.containskey(ProposalTableList[i].Eligible_Fare_Type__c));
                system.debug('Eligible_Fare_Type__c.contains - Business Saver>>'+ProposalTableList[i].Eligible_Fare_Type__c.contains('Business Saver'));
                string EligibleFareType;
                if (ProposalTableList[i].Eligible_Fare_Type__c.contains('Business Saver'))
                {
                    EligibleFareType = 'Business Saver';
                }    
                else
                {
                    EligibleFareType = ProposalTableList[i].Eligible_Fare_Type__c;
                }    
                system.debug('EligibleFareType>>'+EligibleFareType);
                if (VABCFTMap.containskey(EligibleFareType))
                {
                    ProposalTableList[i].Eligible_Fare_Type_Bak_Up__c = ProposalTableList[i].Eligible_Fare_Type__c;
                    ProposalTableList[i].Eligible_Fare_Type__c = VABCFTMap.get(EligibleFareType);
                    system.debug(ProposalTableList[i].Id+'    '+'      '+EligibleFareType+'    '+VABCFTMap.get(EligibleFareType));
                    ProposalTableList[i].FCR_Processed__c	=	true;
                	if (string.isNotEmpty(ProposalTableList[i].FCR_Remarks__c))
                    {
                        ProposalTableList[i].FCR_Remarks__c = ProposalTableList[i].FCR_Remarks__c +'  '+'Partner Fare Type Updated';
                    }    
                    else
                    {
                        ProposalTableList[i].FCR_Remarks__c = 'Partner Fare Type Updated';
                    }    
                }    
                 
            }  
        }
        update ProposalTableList;
        fixVAnonISHTT(ContractIDSet);
    }
    
     /****************************************************************************/	
    public static void fixVAnonISHTT(set<id> ContractIDSet)
    {
        list<Proposal_Table__c> ProposalTableList = new list<Proposal_Table__c>();
        list<Proposal_Table__c> ProposalTableListforAddition = new list<Proposal_Table__c>();
       	map<string, string> BaseClassClonedClassMap = new map<string, string>();
        BaseClassClonedClassMap.put('B', 'W');
        BaseClassClonedClassMap.put('E', 'R');
        BaseClassClonedClassMap.put('N', 'O');
        BaseClassClonedClassMap.put('Q', 'P');
        ProposalTableList = [select id, name, Opportunity__c, Contract__c, VA_ELIGIBLE_BOOKING_CLASS__c,
                             Discount_Off_Published_Fare__c, Eligible_Fare_Type__c, Tier__c, FCR_Remarks__c
                             from Proposal_Table__c where contract__c in :ContractIDSet and name not in ('INT Short Haul', 'Trans Tasman TT', 'Trans Tasman TT ex NZ', 'DOM Mainline')
            				 and VA_ELIGIBLE_BOOKING_CLASS__c != null];
        if ((ProposalTableList != null) && (ProposalTableList.size() > 0))
        {
            for (integer i = 0; i < ProposalTableList.size(); i++)
            {
                if ((ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'B') || (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'E') 
                    || (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'N') || (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'Q'))
                {
                    Proposal_Table__c ProposalTableRec = new Proposal_Table__c();
                    ProposalTableRec.DISCOUNT_OFF_PUBLISHED_FARE__c = ProposalTableList[i].Discount_Off_Published_Fare__c;
                    ProposalTableRec.ELIGIBLE_FARE_TYPE__c = ProposalTableList[i].Eligible_Fare_Type__c;
                    ProposalTableRec.Tier__c = ProposalTableList[i].Tier__c;
                    ProposalTableRec.Name = ProposalTableList[i].Name;
                    ProposalTableRec.Contract__c = ProposalTableList[i].contract__c;
                    ProposalTableRec.Opportunity__c = ProposalTableList[i].opportunity__c;
                    ProposalTableRec.FCR_Created__c = true;
                    ProposalTableRec.VA_ELIGIBLE_BOOKING_CLASS__c = BaseClassClonedClassMap.get(ProposalTableList[i].VA_Eligible_Booking_Class__c);
                    ProposalTableRec.FCR_Processed__c = true;
                    ProposalTableRec.FCR_Remarks__c = 'WROP Record created as per FCR Reqs - Others';
                    ProposalTableListforAddition.add(ProposalTableRec);
                }
                else if ((ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'I') || (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'I*') ||
                        (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'I (Year Round)') ||
                        (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'I (21AP)') ||
                        (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'I (50AP)') ||
                        (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'I ( 90AP)') ||
                        (ProposalTableList[i].VA_ELIGIBLE_BOOKING_CLASS__c == 'I * Tactical'))
                {
                    ProposalTableList[i].FCR_Opportunity_Back_Up__c = ProposalTableList[i].Opportunity__c;
                    ProposalTableList[i].FCR_Contract_Back_Up__c = ProposalTableList[i].Contract__c;
                    ProposalTableList[i].FCR_Processed__c	=	true;
                    ProposalTableList[i].FCR_For_Deletion__c	=	true;
                    ProposalTableList[i].FCR_Remarks__c = 'I / I* Record deleted as per FCR Reqs - Others';
                    ProposalTableList[i].Opportunity__c = null;
                    ProposalTableList[i].Contract__c = null;
                }    
            }    
        }
        update ProposalTableList;
        insert ProposalTableListforAddition;
        updateContractforProcessed(ContractIDSet);
    } 
    
    /****************************************************************************/	
    public static void updateContractforProcessed(set<id> ContractIDSet) 
    {
        list<contract> contractList = new list<contract>();
        contractList = [select id from contract where id in :ContractIDSet];
        if ((contractList != null) && (contractList.size() > 0))
        {
            for (integer i = 0; i < contractList.size(); i++)
            {
                contractList[i].FCR_Processed__c = true;
            }    
        }
        update contractList;
    }
	
      


}