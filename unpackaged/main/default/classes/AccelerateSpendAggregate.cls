public class AccelerateSpendAggregate implements Database.Batchable<SObject> {
    public Date pre1MonthStartDate, pre3MonthsStartDate, pre12MonthsStartDate, endDate, preCYStartDate, preCYEndDate;

    public AccelerateSpendAggregate() {
        Date today = System.today();
        this.endDate = Date.newInstance(today.year(), today.month(), 1).addDays(-1);
        this.pre1MonthStartDate = this.endDate.addMonths(-1).addDays(1);
        this.pre3MonthsStartDate = this.endDate.addMonths(-3).addDays(1);
        this.pre12MonthsStartDate = this.endDate.addMonths(-12).addDays(1);

        this.preCYStartDate = Date.newInstance(today.year() - 1, 1, 1);
        this.preCYEndDate = this.preCYStartDate.addYears(1).addDays(-1);
    }

    public Iterable<SObject> start(Database.BatchableContext BC) {
        return (Iterable<SObject>) [
                SELECT Id
                FROM Account
                WHERE Id IN (
                        SELECT Account__c
                        FROM Spend__c
                        WHERE Year__c >= :this.preCYStartDate.year()
                        AND Contract__r.RecordType.DeveloperName IN ('Accelerate_POS_Rebate', 'NZ_Accelerate_POS_Rebate')
                )
                AND RecordType.DeveloperName IN ('Accelerate', 'SmartFly', 'SmartFlyErr')
        ];
    }

    public void execute(Database.BatchableContext BC, List<SObject> sos) {
        Map<Id, Account> accountMap = new Map<Id, Account>();
        for (SObject so : sos) {
            accountMap.put((Id) so.get('Id'), new Account(
                    Id = (Id) so.get('Id'),
                    Total_Eligible_Spend__c = null,
                    Total_Spend__c = null,
                    Spend_Available_Till__c = null,
                    Spend_Last_Month__c = null,
                    Spend_Last_3Months__c = null,
                    Spend_Last_12Months__c = null,
                    Spend_2019__c = null));
        }

        List<AggregateResult> aggregateResults = [
                SELECT Account__c, SUM(Eligible_Spend__c) Total_ESpend, SUM(Spend__c) Total_Spend, MAX(Spend_End_Date__c) Max_SpendDate
                FROM Spend__c
                WHERE Account__c IN :accountMap.keySet()
                AND Contract__r.RecordType.DeveloperName IN ('Accelerate_POS_Rebate', 'NZ_Accelerate_POS_Rebate')
                GROUP BY Account__c
        ];
        for (AggregateResult result : aggregateResults) {
            Account account = accountMap.get((Id) result.get('Account__c'));
            account.Total_Eligible_Spend__c = (Decimal) result.get('Total_ESpend');
            account.Total_Spend__c = (Decimal) result.get('Total_Spend');
            account.Spend_Available_Till__c = (Date) result.get('Max_SpendDate');
        }

        aggregateResults = [
                SELECT Account__c, SUM(Spend__c) Total_1MSpend
                FROM Spend__c
                WHERE Account__c IN :accountMap.keySet()
                AND Contract__r.RecordType.DeveloperName IN ('Accelerate_POS_Rebate', 'NZ_Accelerate_POS_Rebate')
                AND Spend_Start_Date__c >= :this.pre1MonthStartDate
                AND Spend_Start_Date__c <= :this.endDate
                GROUP BY Account__c
        ];
        for (AggregateResult result : aggregateResults) {
            Account account = accountMap.get((Id) result.get('Account__c'));
            account.Spend_Last_Month__c = (Decimal) result.get('Total_1MSpend');
        }

        aggregateResults = [
                SELECT Account__c, SUM(Spend__c) Total_3MSpend
                FROM Spend__c
                WHERE Account__c IN :accountMap.keySet()
                AND Contract__r.RecordType.DeveloperName IN ('Accelerate_POS_Rebate', 'NZ_Accelerate_POS_Rebate')
                AND Spend_Start_Date__c >= :this.pre3MonthsStartDate
                AND Spend_Start_Date__c <= :this.endDate
                GROUP BY Account__c
        ];
        for (AggregateResult result : aggregateResults) {
            Account account = accountMap.get((Id) result.get('Account__c'));
            account.Spend_Last_3Months__c = (Decimal) result.get('Total_3MSpend');
        }

        aggregateResults = [
                SELECT Account__c, SUM(Spend__c) Total_12MSpend
                FROM Spend__c
                WHERE Account__c IN :accountMap.keySet()
                AND Contract__r.RecordType.DeveloperName IN ('Accelerate_POS_Rebate', 'NZ_Accelerate_POS_Rebate')
                AND Spend_Start_Date__c >= :this.pre12MonthsStartDate
                AND Spend_Start_Date__c <= :this.endDate
                GROUP BY Account__c
        ];
        for (AggregateResult result : aggregateResults) {
            Account account = accountMap.get((Id) result.get('Account__c'));
            account.Spend_Last_12Months__c = (Decimal) result.get('Total_12MSpend');
        }

        aggregateResults = [
                SELECT Account__c, SUM(Spend__c) Total_PreCYSpend
                FROM Spend__c
                WHERE Account__c IN :accountMap.keySet()
                AND Contract__r.RecordType.DeveloperName IN ('Accelerate_POS_Rebate', 'NZ_Accelerate_POS_Rebate')
                AND Spend_Start_Date__c >= :this.preCYStartDate
                AND Spend_Start_Date__c <= :this.preCYEndDate
                GROUP BY Account__c
        ];
        for (AggregateResult result : aggregateResults) {
            Account account = accountMap.get((Id) result.get('Account__c'));
            account.Spend_2019__c = (Decimal) result.get('Total_PreCYSpend');
        }

        update accountMap.values();
    }

    public void finish(Database.BatchableContext BC) {
    }
}