/**
 * @description       : Test class for ContractMarketDiscountUpdateController
 * @Updated By          : Cloudwerx
**/
@isTest
private class TestContractMarketDiscountUpdate {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;  
        
        Opportunity testOpportunityObj = TestDataFactory.createTestOpportunity('testOpp1', 'Sales Opportunity Analysis', testAccountObj.Id, Date.today(), 1.00);
        INSERT testOpportunityObj; 
        
        List<Contract> testContracts = new List<Contract>();
        testContracts.add(TestDataFactory.createTestContract('TestContract', testAccountObj.Id, 'Draft', '15', testOpportunityObj.Id, true, Date.today()));
        testContracts.add(TestDataFactory.createTestContract('TestContract1', testAccountObj.Id, 'Draft', '15', testOpportunityObj.Id, true, Date.today()));
        INSERT testContracts;
        
        List<Market__c> testMarkets = new List<Market__c>();
        testMarkets.add(TestDataFactory.createTestMarket('DOM(Mainline)', testOpportunityObj.Id, testContracts[0].Id, 'DOM(Mainline)', 'Tier 1', 'Tier 1', 1, 1, 'UA', 1000));
        testMarkets.add(TestDataFactory.createTestMarket('DOM(Regional)', testOpportunityObj.Id, testContracts[0].Id, 'DOM(Regional)', 'Tier 1', 'Tier 1', 1, 1, 'UA', 2000));
        testMarkets.add(TestDataFactory.createTestMarket('INT. Short-Haul (excl. TT)', testOpportunityObj.Id, testContracts[0].Id, 'INT. Short-Haul (excl. TT)', 'Tier 1', 'Tier 1', 1, 1, 'UA', 2000));
        testMarkets.add(TestDataFactory.createTestMarket('China1', testOpportunityObj.Id, testContracts[0].Id, 'China', 'Tier 1', 'Tier 1', 1, 1, 'UA', 2000));
        testMarkets.add(TestDataFactory.createTestMarket('INT. Japan', testOpportunityObj.Id, testContracts[0].Id, 'INT. Japan', 'Tier 1', 'Tier 1', 1, 1, 'UA', 2000));
        testMarkets.add(TestDataFactory.createTestMarket('Hong Kong', testOpportunityObj.Id, testContracts[0].Id, 'Hong Kong', 'Tier 1', 'Tier 1', 1, 1, 'UA', 2000));
        testMarkets.add(TestDataFactory.createTestMarket('Trans Tasman (TT)', testOpportunityObj.Id, testContracts[0].Id, 'Trans Tasman (TT)', 'Tier 1', 'Tier 1', 1, 1, 'UA', 2000));
        testMarkets.add(TestDataFactory.createTestMarket('London1', testOpportunityObj.Id, testContracts[0].Id, 'London', 'Tier 1', 'Tier 1', 1, 1, 'UA', 2000));
        testMarkets.add(TestDataFactory.createTestMarket('UK/Europe EY', testOpportunityObj.Id, testContracts[0].Id, 'UK/Europe', 'Tier 1', 'Tier 1', 1, 1, 'VA', 2000));
        INSERT testMarkets;
        
        List<Proposal_Table__c> testProposalTables = new List<Proposal_Table__c>();
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContracts[0].Id, 'Tier 1', 'DOM Mainline'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContracts[0].Id, 'Tier 1', 'DOM Regional'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContracts[0].Id, 'Tier 1', 'HK Hong Kong'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContracts[0].Id, 'Tier 1', 'Trans Tasman TT'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContracts[0].Id, 'Tier 1', 'INT Short Haul'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContracts[0].Id, 'Tier 1', 'London'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContracts[0].Id, 'Tier 1', 'China'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContracts[0].Id, 'Tier 1', 'Japan'));
        INSERT testProposalTables;
    }
    
    @isTest static void testContractMarketDiscountUpdate() {  
        Contract con = [SELECT Id FROM Contract LIMIT 1];
        List<Market__c> markets = [SELECT Id FROM Market__c];
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate; 
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        lController.AddMarket() ;
        lController.save();
        lController.paramValue= markets[0].Id; 
        lController.Cancel() ;
        lController.DeleteId();    
        Test.StopTest(); 
    }
    
    @isTest static void testNoContract() {
        Contract con = [SELECT Id, Status FROM Contract LIMIT 1];
        con.status = 'Expired';
        UPDATE con;
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        Test.StopTest(); 
    }
    
    @isTest static void testMarketIFDOMMainline() {
        Contract con = [SELECT Id FROM Contract LIMIT 1];
        Opportunity oppObj = [SELECT Id FROM Opportunity LIMIT 1];
        Market__c marketObj = TestDataFactory.createTestMarket('UK/Europe EY', oppObj.Id, con.Id, 'DOM(Mainline)', 'Tier 1', 'Tier 1', 1, 1, 'VK', 100);
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        System.assertEquals(true, marketObj.Id == null);
        lController.addmarket = marketObj;
        lController.save();
        Test.StopTest(); 
        // Asserts
        System.assertEquals(true, marketObj.Id != null);
        System.assertEquals('DOM Mainline', marketObj.Name);
        System.assertEquals('VA', marketObj.Carrier__c);
    }
    
    @isTest static void testMarketIFDOMRegional() {
        Contract con = [SELECT Id FROM Contract LIMIT 1];
        Opportunity oppObj = [SELECT Id FROM Opportunity LIMIT 1];
        Market__c marketObj = TestDataFactory.createTestMarket('', oppObj.Id, con.Id, 'DOM(Regional)', 'Tier 1', 'Tier 1', 1, 1, 'VK', 200);
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        System.assertEquals(true, marketObj.Id == null);
        lController.addmarket = marketObj;
        lController.save();
        Test.StopTest(); 
        // Asserts
        System.assertEquals(true, marketObj.Id != null);
        System.assertEquals('DOM Regional', marketObj.Name);
        System.assertEquals('VA', marketObj.Carrier__c);
    }
    
    @isTest static void testMarketIFHongKong() {
        Contract con = [SELECT Id FROM Contract LIMIT 1];
        Opportunity oppObj = [SELECT Id FROM Opportunity LIMIT 1];
        Market__c marketObj = TestDataFactory.createTestMarket('', oppObj.Id, con.Id, 'Hong Kong', 'Tier 1', 'Tier 1', 1, 1, 'VK', 300);
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        System.assertEquals(true, marketObj.Id == null);
        lController.addmarket = marketObj;
        lController.save();
        Test.StopTest(); 
        // Asserts
        System.assertEquals(true, marketObj.Id != null);
        System.assertEquals('HK Hong Kong', marketObj.Name);
        System.assertEquals('VA', marketObj.Carrier__c);
    }
    
    @isTest static void testMarketIFTransTasman() {
        Contract con = [SELECT Id FROM Contract LIMIT 1];
        Opportunity oppObj = [SELECT Id FROM Opportunity LIMIT 1];
        Market__c marketObj = TestDataFactory.createTestMarket('', oppObj.Id, con.Id, 'Trans Tasman (TT)', 'Tier 1', 'Tier 1', 1, 1, 'VK', 400);
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        System.assertEquals(true, marketObj.Id == null);
        lController.addmarket = marketObj;
        lController.save();
        Test.StopTest(); 
        // Asserts
        System.assertEquals(true, marketObj.Id != null);
        System.assertEquals('Trans Tasman TT', marketObj.Name);
        System.assertEquals('VA', marketObj.Carrier__c);
    }
    
    @isTest static void testMarketIFINTShortHaul() {
        Contract con = [SELECT Id FROM Contract LIMIT 1];
        Opportunity oppObj = [SELECT Id FROM Opportunity LIMIT 1];
        Market__c marketObj = TestDataFactory.createTestMarket('', oppObj.Id, con.Id, 'INT. Short-Haul (excl. TT)', 'Tier 1', 'Tier 1', 1, 1, 'VK', 500);
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        System.assertEquals(true, marketObj.Id == null);
        lController.addmarket = marketObj;
        lController.save();
        Test.StopTest(); 
        // Asserts
        System.assertEquals(true, marketObj.Id != null);
        System.assertEquals('INT Short Haul', marketObj.Name);
        System.assertEquals('VA', marketObj.Carrier__c);
    }
    
    @isTest static void testMarketIFChina() {
        Contract con = [SELECT Id FROM Contract LIMIT 1];
        Opportunity oppObj = [SELECT Id FROM Opportunity LIMIT 1];
        Market__c marketObj = TestDataFactory.createTestMarket('', oppObj.Id, con.Id, 'China', 'Tier 1', 'Tier 1', 1, 1, 'VK', 600);
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        System.assertEquals(true, marketObj.Id == null);
        lController.addmarket = marketObj;
        lController.save();
        Test.StopTest(); 
        // Asserts
        System.assertEquals(true, marketObj.Id != null);
        System.assertEquals('China', marketObj.Name);
        System.assertEquals('VA', marketObj.Carrier__c);
    }
    
    @isTest static void testMarketIFJapan() {
        Contract con = [SELECT Id FROM Contract LIMIT 1];
        Opportunity oppObj = [SELECT Id FROM Opportunity LIMIT 1];
        Market__c marketObj = TestDataFactory.createTestMarket('', oppObj.Id, con.Id, 'Japan', 'Tier 1', 'Tier 1', 1, 1, 'VK', 700);
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        System.assertEquals(true, marketObj.Id == null);
        lController.addmarket = marketObj;
        lController.save();
        Test.StopTest(); 
        // Asserts
        System.assertEquals(true, marketObj.Id != null);
        System.assertEquals('Japan', marketObj.Name);
        System.assertEquals('VA', marketObj.Carrier__c);
    }
    
    @isTest static void testMarketIFLondon() {
        Contract con = [SELECT Id FROM Contract LIMIT 1];
        Opportunity oppObj = [SELECT Id FROM Opportunity LIMIT 1];
        Market__c marketObj = TestDataFactory.createTestMarket('', oppObj.Id, con.Id, 'London', 'Tier 1', 'Tier 1', 1, 1, 'VK', 800);
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        System.assertEquals(true, marketObj.Id == null);
        lController.addmarket = marketObj;
        lController.save();
        Test.StopTest(); 
        // Asserts
        System.assertEquals(true, marketObj.Id != null);
        System.assertEquals('London', marketObj.Name);
        System.assertEquals('VA', marketObj.Carrier__c);
    }
    
    @isTest static void testMarketWithOtherMarketName() {
        Contract con = [SELECT Id FROM Contract LIMIT 1];
        Opportunity oppObj = [SELECT Id FROM Opportunity LIMIT 1];
        Market__c marketObj = TestDataFactory.createTestMarket('', oppObj.Id, con.Id, 'Other Market Name', 'Tier 1', 'Tier 1', 1, 1, 'VK', 900);
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        lController.addmarket = marketObj;
        lController.save();
        Test.StopTest(); 
    }
    
    @isTest static void testMarketWithNoContractId() {
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', null);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        Test.StopTest(); 
    }
    
    @isTest static void testMarketWithCheckMarketList() {
        Contract con = [SELECT Id FROM Contract LIMIT 1];
        Opportunity oppObj = [SELECT Id FROM Opportunity LIMIT 1];
        Market__c marketObj = TestDataFactory.createTestMarket('UK/Europe', oppObj.Id, con.Id, 'UK/Europe', 'Tier 1', 'Tier 1', 1, 1, 'VA', 1100);
        Test.StartTest(); 
        PageReference pageRef = Page.ContractMarketDiscountUpdate;
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountUpdateController lController = new ContractMarketDiscountUpdateController();
        lController.initDisc();
        lController.addmarket = marketObj;
        lController.save();
        Test.StopTest(); 
    }
}