/*
**Modified By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Modified Date: 22.03.2022
**Description: Modified to enable ALMS
*/
global class  CorporateContactVelocityStatusUpdate implements Database.Batchable<SObject>,Database.AllowsCallouts,Database.Stateful
{	
    public static string almsActivated = system.Label.ALMS_Integration;
    List<contact> lstConUpdate = new List<Contact>(); 
    List<Comms_Log__c> logList = new List<Comms_Log__c>();
    global CorporateContactVelocityStatusUpdate(){}
    
    global String gstrQuery = 'Select id , Velocity_Number__c '+
            'FROM Contact where  '+
            ' recordtypeid in ( \'012900000007krgAAA\') ' +
            ' and  Status__c = \'Active\'' +
            ' and  IsCorporateContact__c = true' +
            ' and  Velocity_Number__c != null ' +
            ' and  Velocity_Number__c != \'----------\'' ;
    //global String gstrQuery = 'Select id , Velocity_Number__c '+'FROM Contact where  '+'  Velocity_Number__c != null ' +' and  Velocity_Number__c != \'----------\'' ;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug(gstrQuery); 
        return Database.getQueryLocator(gstrQuery);
    }   
    global void execute(Database.BatchableContext BC,List<sObject> scope)
    {
        
        System.debug('** Starting Batch Process ** '+scope);
        
        List<Contact> contactList = new List <Contact>();
        
        for (Sobject s : scope)
        {
            Contact c = (Contact)s;
            contactList.add(c);
        }   
        System.debug('contactList--'+contactList);
        
        for(Integer x =0; x<ContactList.size(); x++)
        {
            
            Contact contact = contactList.get(x);
            
            if(almsActivated == 'Inactive'){
                if(Test.isRunningTest() )
                {
                }
                else
                { 
                    MakeVelocityCallout.apexcallout(contact.id,contact.Velocity_Number__c); 
                }
            }if(almsActivated == 'Active'){                
                Map<String, String> velDetailsMap = GetVelocityDetailsWS.getVelDetFromAlms(contact.Velocity_Number__c); 
                System.debug('velDetailsMap--'+velDetailsMap);
                if(velDetailsMap.size()>0){
                    if(velDetailsMap.containsKey('Tier') && velDetailsMap.get('Tier') != NULL){
                        String tier = velDetailsMap.get('Tier');
                        Contact con = new Contact();
                        con.Id = contact.id;
                        if(tier.equalsIgnoreCase('R') || tier.equalsIgnoreCase('Red'))
                        {	
                            con.Velocity_Status__c = 'Red';
                        }else if(tier.equalsIgnoreCase('S') || tier.equalsIgnoreCase('Silver'))
                        {
                            con.Velocity_Status__c = 'Silver';
                        }
                        else if(tier.equalsIgnoreCase('G') || tier.equalsIgnoreCase('Gold'))
                        {
                            con.Velocity_Status__c = 'Gold';
                        }else if(tier.equalsIgnoreCase('P') || tier.equalsIgnoreCase('Platinum'))
                        {
                            con.Velocity_Status__c = 'Platinum';
                        }else if(tier.equalsIgnoreCase('V') || tier.equalsIgnoreCase('VIP'))
                        {
                            con.Velocity_Status__c  ='VIP';
                        }  
                        lstConUpdate.add(con);
                    }
                }System.debug('lstConUpdate--'+lstConUpdate); 
                
                Comms_Log__c lg = new Comms_Log__c();
                lg.Type__c = 'Contact';
                lg.Contact__c = contact.id;
                lg.Request_Type__c = 'GET';
                lg.Velocity_Number__c = contact.Velocity_Number__c;
                if(velDetailsMap.containsKey('Attempt') && velDetailsMap.get('Attempt') != NULL){
                    lg.Attempts__c = Integer.valueOf(velDetailsMap.get('Attempt'));
                }
                if(velDetailsMap.containsKey('Status') && velDetailsMap.get('Status') != NULL){
                    lg.Status__c = velDetailsMap.get('Status');
                    if(velDetailsMap.get('Status') == 'Failed'){
                        if(velDetailsMap.containsKey('ErrorDetails') && velDetailsMap.get('ErrorDetails') != NULL){
                            lg.Error__c = velDetailsMap.get('ErrorDetails');
                        } 
                    }
                }
                logList.add(lg);
            }System.debug('logList--'+logList);           
        }        
        System.debug('Consolidated lstConUpdate--'+lstConUpdate);
        if(lstConUpdate.size()>0){
            update lstConUpdate;
        }
        if(logList.size()>0){
            insert logList;
        }
    }
    //Finishing the batch  (Or though this is not executiong code this methos needs to exsist for the batch class to run!!)  
    global void finish(Database.BatchableContext BC)
    {
    }
}