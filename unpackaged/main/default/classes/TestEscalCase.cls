@isTest
private class TestEscalCase 
{
     public static testMethod void testCaseEscal() 
    {
 List<RecordType> recordTypeGRCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
									                    		r.Description, r.BusinessProcessId From RecordType r
									                        	where SobjectType = 'Case'
										                        and IsActive = true
										                        and DeveloperName = 'GR_Case' LIMIT 1];
    
     Case cm = new Case(SuppliedEmail='smitha.nair@inmail.com',
                            SuppliedName='John Doe',
                            Velocity_Member__c = false,
                            Subject='Feedback - Something',
                            Contact_First_Name__c='Tom',
                            Contact_Last_name__c = 'Johns',
                            Compliance_Status__c = 'Compliance form NOT required',
                            Category_Number__c = 'a1w6F000004JD9b',
                            Pre_Filter_Primary_Category__c = 'Alliances',
                            recordTypeId=recordTypeGRCase[0].id);
    
    insert cm ;
    
     PageReference  ref1 = Page.EscalateToBNETeam;
     ref1.getParameters().put('id', cm.Id);
    Test.setCurrentPage(ref1);
    ApexPages.StandardController conL = new ApexPages.StandardController(cm);
    EscalCase lController = new EscalCase(conL);
    Test.startTest();
       
           ref1 = lController.caseEscalation();
   Test.stopTest();    
        
    }
}