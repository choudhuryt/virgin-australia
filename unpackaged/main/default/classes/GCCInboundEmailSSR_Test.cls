/**
 * @Description: Test class for GCCInboundEmailSSR
 */
@isTest
private class GCCInboundEmailSSR_Test {

    static testMethod void myUnitTest() {
       // create a new email and envelope object
      Messaging.InboundEmail email = new Messaging.InboundEmail() ;
      Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
   
      // setup the data for the email
      email.subject = 'Test SSR Form';
      email.fromname = 'FirstName LastName';
      env.fromAddress = 'ssr@email.com';
   	  email.htmlBody = ' Special Service Request Form Passenger Information Virgin Australia Booking Reference TESTAB Title Mrs. First Name Julie Last Name Test Phone Number Mobile Please enter Country Code, Area Code, Number Australia (61) Phone Number 0411222333 Email Address test@test.com.au Other Assistance Additional Information I am coeliac and therefore require any food to be gluten fre';
      email.htmlBody += ' Carer Details,Assistance Person Details,Medical Condition Information,Hidden Disability,Travelling with Powered Medical Equipment,Other Assistance,Hidden Disability';
        email.htmlBody += 'I am vision impaired but do not require assistance';
        
      // add an attachment
      //Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
      //attachment.body = blob.valueOf('my attachment text');
      //attachment.fileName = 'textfile.txt';
      //attachment.mimeTypeSubType = 'text/plain';
   
      //email.binaryAttachments =
       //  new Messaging.inboundEmail.BinaryAttachment[] { attachment };
   
      // call the email service class and test it with the data in the testMethod
      GCCInboundEmailSSR emailProcess = new GCCInboundEmailSSR();
      emailProcess.handleInboundEmail(email, env);
   
      // query for the contact the email service created
      //Contact contact = [select id, firstName, lastName, email from contact
      //  where firstName = 'FirstName' and lastName = 'LastName'];
   
      //System.assertEquals(contact.firstName,'FirstName');
      //System.assertEquals(contact.lastName,'LastName');
      //System.assertEquals(contact.email,'sfdcsrini@email.com');
   
      // find the attachment
      //Attachment a = [select name from attachment where parentId = :contact.id];
   
      //System.assertEquals(a.name,'textfile.txt');

    }
}