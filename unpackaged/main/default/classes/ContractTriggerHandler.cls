/*
*	Updated by Cloudwerx : Removed hardcoded recordTypeId  & added dynamic reference to the query and merged 3 trigger into 1 and added logic here
*/
public class ContractTriggerHandler {
    
    // Added By : cloudwerx to get RecordTypeId Dynamically 
    // In code they are using "0120k0000005rKiAAI" recordType Id but its not exists
    public static Id contractCorporateRecordTypeId = Utilities.getRecordTypeId('Contract', 'Corporate');
    public static Id contractAcceleratePOSRebateRecordTypeId = Utilities.getRecordTypeId('Contract', 'Accelerate_POS_Rebate');
    public static Id contracSmartFlyRecordTypeId = Utilities.getRecordTypeId('Contract', 'SmartFly');
    public static Id contractNZAcceleratePOSRebateRecordTypeId = Utilities.getRecordTypeId('Contract', 'NZ_Accelerate_POS_Rebate');
    public static Id opportunityCorporateRecordTypeId = Utilities.getRecordTypeId('Opportunity', 'Corporate');
    public static boolean preventCalling = false;
    
    //AddedBy : cloudwerc : Logic of "BeforeContractTrigger" Trigger
    public static void contractBeforeActivity(List<Contract> contracts, Map<Id, Contract> oldMap, Boolean isInsert) {
        Decimal sipPercentLower = 0.00;
        Decimal sipMinGrowthLower= 0.00;
        Decimal sipPercentUpper = 0.00;
        Decimal sipMinGrowthUpper= 0.00;
        Decimal sLAFixedComponentAndVATotalRevenue = 0.00;
        Decimal sLAFixedComponentAndVADomandSH = 0.00;
        Decimal sLAFixedComponentAndSH = 0.00;
        Decimal sLAFixedComponentAndLH = 0.00;
        Decimal totalsLAFixedComponentDomSHLH = 0.00;  
        Decimal totalLHAndDomPercentage = 0.00;
        Set<String> industryCOSVersions = new Set<String>();
        Set<String> industryCOSs = new Set<String>();
        Set<Id> signedCustomerIds = new Set<Id>();
        Set<Id> signedFirtsCustomerIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        Set<Id> contractIds = (oldMap != null) ? oldMap.keySet() : new Set<Id>();
        Set<Id> sipHeaderIds = new Set<Id>();
        List<SIP_Header__c> sipHeaders = new List<SIP_Header__c>();
        List<SIP_Rebate__c> sipRebates = new List<SIP_Rebate__c>();
        Map<Id, List<SIP_Header__c>> contractSIPHeaderMap = new Map<Id, List<SIP_Header__c>>();
        Map<Id, List<SIP_Rebate__c>> sipHeaderRebatateMap = new Map<Id, List<SIP_Rebate__c>>();
        
        for(SIP_Header__c sipHeaderObj :[SELECT Id, contract__c FROM SIP_Header__c WHERE contract__c IN :contractIds]) {
            if(contractSIPHeaderMap != null && contractSIPHeaderMap.containsKey(sipHeaderObj.contract__c)) {
                sipHeaders = contractSIPHeaderMap.get(sipHeaderObj.contract__c);
            }
            sipHeaderIds.add(sipHeaderObj.Id);
            sipHeaders.add(sipHeaderObj);
            contractSIPHeaderMap.put(sipHeaderObj.contract__c, sipHeaders);   
        }
        
        for(SIP_Rebate__c sipRebateObj :[SELECT Id, SIP_HEADER__C, Contract_Growth__c, Upper_Percent__c, Lower_Percent__c, SIP_Percent__c  
                                        FROM SIP_Rebate__c WHERE SIP_HEADER__C IN :sipHeaderIds]){
            if(sipHeaderRebatateMap != null && sipHeaderRebatateMap.containsKey(sipRebateObj.SIP_HEADER__C)) {
                sipRebates = sipHeaderRebatateMap.get(sipRebateObj.SIP_HEADER__C);
            }
            sipRebates.add(sipRebateObj);
            sipHeaderRebatateMap.put(sipRebateObj.SIP_HEADER__C, sipRebates);   
        }
        
        for(Contract contractObj :contracts) {
            if(String.isNotBlank(contractObj.Industry_Cos_Version__c)) {
                industryCOSVersions.add(contractObj.Industry_Cos_Version__c);   
            }
            if(String.isNotBlank(contractObj.Industry_Cos__c)) {
                industryCOSs.add(contractObj.Industry_Cos__c);   
            }
            if(String.isNotBlank(contractObj.CustomerSignedId)) {
                signedCustomerIds.add(contractObj.CustomerSignedId);  
                contactIds.add(contractObj.CustomerSignedId);
            }
            If(String.isNotBlank(contractObj.Customer_Signed_By_1__c)) {
                signedFirtsCustomerIds.add(contractObj.Customer_Signed_By_1__c);
                contactIds.add(contractObj.Customer_Signed_By_1__c);
            }
            if(contractObj.RecordTypeId != null && contractObj.RecordTypeId == contractCorporateRecordTypeId) {
                if(contractObj.Velocity_Acceleration_Cost__c > contractObj.Guideline_VSM_Cost__c) {
                    contractObj.VSM_Cost_Within_Guidline__c ='No';
                } else {
                    contractObj.VSM_Cost_Within_Guidline__c ='Yes';  
                    contractObj.Acceleration_Approval_Recieved__c =false;
                }
            }
            system.debug ('Is this testing' + test.isrunningtest()  ) ;  
            //@updatedBy : cloudwerx TODO: We need to remove !test.isrunningtest() to increase code coverage
            //if(!test.isrunningtest())
            //{
            //if(!contractObj.RecordTypeId.equals('01290000000tEgL') || !contractObj.RecordTypeId.equals('012900000007LFZ') || !contractObj.RecordTypeId.equals('01290000000tMJV') || !contractObj.RecordTypeId.equals('012900000007oTO')  ){
            if (contractObj.status.equals('Draft')) {
                //Determines which COS TIER needs to be selected by calculateing the revenue ranges for different Channels and Record Types
                contractObj.Within_Policy__c ='Yes';
                contractObj.SIP_Max_Level__c =0;
                try {
                    if(contractObj.Type__c != null && contractObj.Record_Type__c ==null || contractObj.Record_Type__c.contains('Exception')) {
                        //  contractObj.Within_Policy__c ='No';
                    }
                    //Boolean result = contractObj.Record_Type__c.contains('NZ');  
                    if(contractObj.Type__c != null && contractObj.Type__c.equals('Standard') && contractObj.Record_Type__c.contains('NZ')) {
                        if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 999999999){
                            contractObj.Industry_Cos__c ='O';  
                        } else if(contractObj.Type__c != null && contractObj.Type__c.equals('Exception') && contractObj.Record_Type__c.contains('NZ')) {
                            if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 999999999){
                                contractObj.Industry_Cos__c ='Q';
                            }
                        } else if(contractObj.Type__c != null && contractObj.Type__c.equals('SLA') && contractObj.Record_Type__c.contains('NZ')){
                            if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 999999999){
                                contractObj.Industry_Cos__c ='P';
                            }
                        }
                    }
                    
                    if(contractObj.Type__c != null && contractObj.Type__c.equals('Standard') && contractObj.Record_Type__c.contains('RoW')){
                        if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 999999999) {
                            contractObj.Industry_Cos__c ='R';  
                        } else if(contractObj.Type__c != null && contractObj.Type__c.equals('Exception') && contractObj.Record_Type__c.contains('RoW')) {
                            if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 999999999){
                                contractObj.Industry_Cos__c ='T';
                            }
                        } else if(contractObj.Type__c != null && contractObj.Type__c.equals('SLA') && contractObj.Record_Type__c.contains('RoW')) {
                            if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 999999999){
                                contractObj.Industry_Cos__c ='S';
                            }
                        }
                    }
                    if(contractObj.Type__c != null && contractObj.Type__c.equals('Standard') && contractObj.Record_Type__c.contains('US')) {
                        if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 999999999) {
                            contractObj.Industry_Cos__c ='L';  
                        } else if(contractObj.Type__c != null && contractObj.Type__c.equals('Exception') && contractObj.Record_Type__c.contains('US')) {
                            if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 999999999) {
                                contractObj.Industry_Cos__c ='N';
                            }
                        } else if(contractObj.Type__c != null && contractObj.Type__c.equals('SLA') && contractObj.Record_Type__c.contains('US')) {
                            if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 999999999) {
                                contractObj.Industry_Cos__c ='M';
                            }
                        }
                    }
                    
                    //Boolean result = contractObj.Record_Type__c.contains('AU');
                    if(contractObj.Type__c != null && contractObj.Type__c.equals('Retail') && contractObj.Record_Type__c.contains('AU')) {
                        if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 249999) {
                            contractObj.Industry_Cos__c ='A';  
                        } else if (contractObj.Total_LY_Revenue__C >= 250000 && contractObj.Total_LY_Revenue__C < 999999) {
                            contractObj.Industry_Cos__c ='B';
                        } else if (contractObj.Total_LY_Revenue__C >= 1000000 && contractObj.Total_LY_Revenue__C < 4999999) {
                            contractObj.Industry_Cos__c ='C';
                        }
                    }
                    
                    if(contractObj.Type__c != null && contractObj.Type__c.equals('Online') && contractObj.Record_Type__c.contains('AU')) {
                        if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < =5000000) {
                            contractObj.Industry_Cos__c ='D';  
                        } else if (contractObj.Total_LY_Revenue__C >= 5000000 && contractObj.Total_LY_Revenue__C < 999000000) {
                            contractObj.Industry_Cos__c ='E';
                        }
                    }
                    
                    if(contractObj.Type__c != null && contractObj.Type__c.equals('Wholesale') && contractObj.Record_Type__c.contains('AU')) {
                        if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 1000000) {
                            contractObj.Industry_Cos__c ='F';  
                        } else if (contractObj.Total_LY_Revenue__C >= 1000000 && contractObj.Total_LY_Revenue__C < 999999999) {
                            contractObj.Industry_Cos__c ='G';
                        }
                    }
                    
                    if(contractObj.Type__c != null && contractObj.Type__c.equals('Consolidator') && contractObj.Record_Type__c.contains('AU')) {
                        if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < =999999999) {
                            contractObj.Industry_Cos__c ='H';  
                        }
                    }
                    
                    if(contractObj.Type__c != null && contractObj.Type__c.equals('SLA') && contractObj.Record_Type__c.contains('AU')) {
                        if(contractObj.Total_LY_Revenue__C > 05000000&& contractObj.Total_LY_Revenue__C < =14999999) {
                            contractObj.Industry_Cos__c ='I';  
                        }
                    }
                    
                    if(contractObj.Type__c != null && contractObj.Type__c.equals('TMC') && contractObj.Record_Type__c.contains('AU')) {
                        if(contractObj.Total_LY_Revenue__C > 15000000 && contractObj.Total_LY_Revenue__C < =999999999) {
                            contractObj.Industry_Cos__c ='J';  
                        }
                    }
                    
                    if(contractObj.Type__c != null && contractObj.Type__c.equals('Exception') && contractObj.Record_Type__c.contains('AU')){
                        if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < =999999999){
                            contractObj.Industry_Cos__c ='K';  
                        }
                    }
                } catch(Exception e){
                    System.debug('!!!!!!!!!!!!!!' + e);  
                }
                
                if(contractObj.Type__c != null && contractObj.Type__c.equals('Groups') && contractObj.Record_Type__c.contains('Group')) {
                    if(contractObj.Total_LY_Revenue__C >= 0 && contractObj.Total_LY_Revenue__C < 249999) {
                        contractObj.Industry_Cos__c ='A';	
                    } else if (contractObj.Total_LY_Revenue__C >= 250000 && contractObj.Total_LY_Revenue__C < 999999) {
                        contractObj.Industry_Cos__c ='B';
                    } else if (contractObj.Total_LY_Revenue__C >= 1000000 && contractObj.Total_LY_Revenue__C < 4999999) {
                        contractObj.Industry_Cos__c ='C';
                    } else if(contractObj.Total_LY_Revenue__C > 05000000 && contractObj.Total_LY_Revenue__C < =14999999) {
                        contractObj.Industry_Cos__c ='I';	
                    }       		
                    if(contractObj.Industry_Cos__c == null) {
                        contractObj.Within_Policy__c ='No';
                    }
                }
                // "0120k0000005rKiAAI" Not exists in org so removed condition (contractobj.RecordTypeid<> '0120k0000005rKiAAI')
                if(contractobj.RecordTypeid !=null && (contractobj.RecordTypeid != contractAcceleratePOSRebateRecordTypeId || contractobj.RecordTypeid != contracSmartFlyRecordTypeId)){
                    //contractObj.Fare_Class_Excluded_for_Calculation__c = contractObj.Fare_Class_Out_Count_Out_Payment__c;
                    //contractObj.Fare_Class_Excluded_for_Payment__c = contractObj.Fare_Class_In_Count_Out_Payment__c;
                }
                List<SIP_Header__c> sipHeaderList = (contractSIPHeaderMap != null && contractSIPHeaderMap.containsKey(contractObj.Id)) ? contractSIPHeaderMap.get(contractObj.Id) : new List<SIP_Header__c>();
                if(sipHeaderList.size() > 0) {
                    Id sipHeaderId = sipHeaderList[0].Id;
                    List<SIP_Rebate__c> sipRebateList = (sipHeaderRebatateMap != null && sipHeaderRebatateMap.containsKey(sipHeaderId)) ? sipHeaderRebatateMap.get(sipHeaderId) : new List<SIP_Rebate__c>();
                    if(sipRebateList.size() > 0) {
                        List <Decimal> sortOrderPercent = new List<Decimal>();
                        List <Decimal> sortOrderGrowth = new List<Decimal>();
                        for(SIP_Rebate__c sipRebateObj :sipRebateList){
                            sortOrderPercent.add(sipRebateObj.SIP_Percent__c);
                            sortOrderGrowth.add(sipRebateObj.Lower_Percent__c);
                        }
                        if (sortOrderPercent.size() > 0) {
                            Integer i = sortOrderPercent.size();
                            Integer i1 = sortOrderGrowth.size();
                            Decimal d = sortOrderPercent.get(i -1);
                            Decimal d1 = sortOrderGrowth.get(i1 -1);
                            contractObj.SIP_Max_Level__c = (d /100) * contractObj.Total_Estimated_Revenue__c;
                            Decimal d2 = sortOrderPercent.get(0);
                            Decimal d3 = sortOrderGrowth.get(0);
                        }
                    }
                }
            }
        }
        
        List<Industry_COS_Model__c> industryCosList = [SELECT Id, SIP_Low__c, SIP_Lower_Min_Growth__c, SIP_Upper__c, 
                                                       SIP_Upper_Min_Growth__c, Commision_Domestic__c, Commission_Long_Haul__c, 
                                                       Commission_Short_Haul__c,Commission_Tasman__c, Marketing__c,Revenue_High__c, 
                                                       SLA_Fixed_Component_VA_Intl__c, Revenue_Low__c,Sales_Channel__c, 
                                                       SLA_Fixed_Component_VA_Dom_and_SH__c, SLA_Fixed_Component_VA_Total_Revenue__c, 
                                                       Ticket_Fund__c, Tier__c, Validation_Not_to_Exceed__c, Vesion__c,SLA_Fixed_Component_VA_SH__c 
                                                       FROM Industry_COS_Model__c 
                                                       WHERE Vesion__c IN :industryCOSVersions AND Tier__c IN :industryCOSs];
        
        Map<Id, Contact> existingContactsMap = new Map<Id, Contact>([SELECT Id ,Title FROM Contact WHERE Id IN :contactIds]);
        for(Contract contractObj :contracts) {
            Contact contactObj = (signedCustomerIds.size() > 0 && signedCustomerIds.contains(contractObj.CustomerSignedId) && existingContactsMap != null && existingContactsMap.containsKey(contractObj.CustomerSignedId)) ? existingContactsMap.get(contractObj.CustomerSignedId) : new Contact();
            Contact firstSignedContact = (signedFirtsCustomerIds.size() > 0 && signedFirtsCustomerIds.contains(contractObj.Customer_Signed_By_1__c) && existingContactsMap != null && existingContactsMap.containsKey(contractObj.Customer_Signed_By_1__c)) ? existingContactsMap.get(contractObj.Customer_Signed_By_1__c) : new Contact();
            if (contactObj != null && String.isNotBlank(contactObj.Title)) {
                Integer i = contactObj.Title.length();
                if(i > 0 && i < 40){
                    contractObj.CustomerSignedTitle = contactObj.Title;
                } else {
                    contractObj.CustomerSignedTitle = contactObj.Title.substring(1, 40);
                }
            }
            
            if(firstSignedContact != null) {
                contractObj.Customer_Signed_Title1__c = firstSignedContact.title;
            }
            
            if(contractObj.Override_Commisions__c == false) {
                for(Industry_COS_Model__c industryCosObj :industryCosList){
                    sipPercentLower   = industryCosObj.SIP_Low__c ;
                    sipMinGrowthLower = industryCosObj.SIP_Lower_Min_Growth__c;
                    sipPercentUpper   = industryCosObj.SIP_Upper__c;
                    sipMinGrowthUpper = industryCosObj.SIP_Upper_Min_Growth__c;
                    sLAFixedComponentAndVATotalRevenue = industryCosObj.SLA_Fixed_Component_VA_Total_Revenue__c;
                    sLAFixedComponentAndVADomandSH = industryCosObj.SLA_Fixed_Component_VA_Dom_and_SH__c;
                    sLAFixedComponentAndSH = industryCosObj.SLA_Fixed_Component_VA_SH__c;    
                    sLAFixedComponentAndLH = industryCosObj.SLA_Fixed_Component_VA_Intl__c;
                    totalsLAFixedComponentDomSHLH =    sLAFixedComponentAndVADomandSH + sLAFixedComponentAndSH + sLAFixedComponentAndLH ;
                    //put in the validation for the toatl oop
                    if(contractObj.SLA_Fixed_Total_COS__c != null){}  
                    
                    if(contractObj.Dom_SH_SLA__c != null) {
                        totalLHAndDomPercentage = contractObj.Dom_SH_SLA__c;
                    }
                    
                    if(contractObj.SH_SLA__c != null){
                        totalLHAndDomPercentage = totalLHAndDomPercentage + contractObj.SH_SLA__c;
                    }
                    
                    if(contractObj.LH_SLA__c != null ){
                        totalLHAndDomPercentage = totalLHAndDomPercentage + contractObj.LH_SLA__c;
                    }
                    
                    if(contractObj.Fixed_SLA_COS__c != null) {
                        if(totalLHAndDomPercentage > 0.00) {
                            totalLHAndDomPercentage = totalLHAndDomPercentage + contractobj.Fixed_SLA_COS__c;
                        } else {
                            totalLHAndDomPercentage = contractobj.Fixed_SLA_COS__c;
                        }
                        
                    }
                    if(isInsert) {
                        contractObj.Domestic_Australia_Economy__c = industryCosObj.Commision_Domestic__c;
                        contractObj.Domestic_Australia_Premium_Economy__c = industryCosObj.Commision_Domestic__c;
                        contractObj.Domestic_Australia_Business__c = industryCosObj.Commision_Domestic__c;
                        contractObj.Intl_Short_Haul_Economy__c = industryCosObj.Commission_Short_Haul__c;
                        contractObj.Intl_Short_Haul_Premium_Economy__c = industryCosObj.Commission_Short_Haul__c;
                        contractObj.Intl_Short_Haul_Business__c = industryCosObj.Commission_Short_Haul__c;
                        contractObj.Tasman_Economy__c = industryCosObj.Commission_Tasman__c;
                        contractObj.Tasman_Premium_Economy__c = industryCosObj.Commission_Tasman__c;
                        contractObj.Tasman_Business__c = industryCosObj.Commission_Tasman__c;
                        contractObj.Intl_Long_Haul_Economy__c = industryCosObj.Commission_Long_Haul__c;
                        contractObj.Intl_Long_Haul_Premium_Economy__c = industryCosObj.Commission_Long_Haul__c;
                        contractObj.Intl_Long_Haul_Business__c = industryCosObj.Commission_Long_Haul__c;
                    }
                    if(industryCosObj.Marketing__c > 0) {
                        contractObj.Fixed_Amount__c = industryCosObj.Marketing__c * contractObj.Total_LY_Revenue__c /100;
                    } 
                    if(industryCosObj.Ticket_Fund__c > 0) {
                        contractObj.Ticket_Fund__c = industryCosObj.Ticket_Fund__c  * contractObj.Total_LY_Revenue__c/100;
                    } 
                }
                
            } else {  
                Industry_COS_Model__c industryCos = new Industry_COS_Model__c();
                if(industryCosList.size() > 0) {
                    industryCos = industryCosList[0];
                    sipPercentLower   = industryCos.SIP_Low__c ;
                    sipMinGrowthLower = industryCos.SIP_Lower_Min_Growth__c;
                    sipPercentUpper   = industryCos.SIP_Upper__c;
                    sipMinGrowthUpper = industryCos.SIP_Upper_Min_Growth__c;
                }
                
                if(contractObj.Dom_SH_SLA__c != null) {
                    totalLHAndDomPercentage = contractObj.Dom_SH_SLA__c;
                }
                
                if(contractObj.SH_SLA__c != null) {
                    totalLHAndDomPercentage = totalLHAndDomPercentage + contractObj.SH_SLA__c;
                }
                
                if(contractObj.LH_SLA__c != null) {
                    totalLHAndDomPercentage = totalLHAndDomPercentage + contractObj.LH_SLA__c;
                }
                
                if(contractObj.Fixed_SLA_COS__c != null) {
                    if(totalLHAndDomPercentage > 0.00){
                        totalLHAndDomPercentage = totalLHAndDomPercentage + contractobj.Fixed_SLA_COS__c;
                    } else {
                        totalLHAndDomPercentage = contractobj.Fixed_SLA_COS__c;
                    }   
                }
            }
        }
    }
    
    //AddedBy : cloudwerc : Logic of "ContractUpdate" Trigger
    public static void contractAfterUpdateActivity(List<Contract> contracts, Map<Id, Contract> oldContractMap) {
        Set<Id> parentContractIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();
        for(Contract contractObj :contracts) {
            if(contractObj.Parent_Contract__c != null) {
                parentContractIds.add(contractObj.Parent_Contract__c);
            }
            if(contractObj.AccountId != null) {
                accountIds.add(contractObj.AccountId);
            }
        }
        
        List<Contract> parentContracts = new List<Contract>();
        List<Account> contractAccounts = new List<Account>();
        Map<Id, Contract> parentContractMap = new Map<Id, Contract>([SELECT Id, status FROM Contract 
                                                                     WHERE Id IN :parentContractIds]);
        Map<Id, Account> accountContractMap = new Map<Id, Account>([SELECT Id, Velocity_Pilot_Gold_Available__c, Marketing_Fund_Amount_Available__c, 
                                                                    RecordTypeId, Velocity_Gold_Status_Match_Available__c, Velocity_Platinum_Status_Match_Available__c, 
                                                                    Account_Type__c FROM Account WHERE id IN :accountIds]);
        for(Contract contractObj :contracts) {
            Contract parentContractObj = (parentContractMap != null && parentContractMap.containsKey(contractObj.Parent_Contract__c)) ? parentContractMap.get(contractObj.Parent_Contract__c) : new Contract();
            if(String.isNotBlank(contractObj.Status) && contractObj.Status.equals('Activated') && parentContractObj != null && String.isNotBlank(parentContractObj.Status) && parentContractObj.Status.equals('Activated')){
                parentContractObj.Status = 'Deactivated';
                parentContracts.add(parentContractObj);
            }
            
            if (String.isNotBlank(contractObj.Status) && contractObj.Status == 'Activated' && oldContractMap != null && oldContractMap.containsKey(contractObj.Id) && contractObj.Fixed_Amount__c != oldContractMap.get(contractObj.Id).Fixed_Amount__c) {                 
                Account contractAccountObj = (accountContractMap != null && accountContractMap.containsKey(contractObj.AccountId)) ? accountContractMap.get(contractObj.AccountId) : new Account();
                if (contractAccountObj != null && contractAccountObj.Id != null) {
                    contractAccountObj.Marketing_Fund_Amount_Available__c = contractObj.Fixed_Amount__c;
                    contractAccounts.add(contractAccountObj);
                }
            }
        }
        try {
            UPDATE parentContracts;
            UPDATE contractAccounts;
        } Catch (Exception exp) {}
    }
    
    //AddedBy : cloudwerc : Logic of "AfterContractTrigger" Trigger
    public static void contractAfterInsertActivity(List<Contract> contracts) {
        createTourCodes(contracts);
    }
    
    private static void createTourCodes(List<Contract> contracts) {
        Set<Id> accountIds = new Set<Id>();
        for(Contract contractObj: contracts) {
            if(contractObj.AccountId != null && contractObj.RecordTypeId == contractNZAcceleratePOSRebateRecordTypeId || contractObj.RecordTypeId == contractAcceleratePOSRebateRecordTypeId) {
                accountIds.add(contractObj.AccountId);
            }
        }
        
        List<Tourcodes__c> tourCodesToBeInserted = new List<Tourcodes__c>();
        Map<Id, Account> accountMap = new Map<Id, Account>( [SELECT Id, Business_Number__c, Market_Segment__c FROM Account 
                                                             WHERE Id IN :accountIds]);
        
        for(Contract contractObj: contracts) {
            if(contractObj.AccountId != null && contractObj.RecordTypeId == contractNZAcceleratePOSRebateRecordTypeId || contractObj.RecordTypeId == contractAcceleratePOSRebateRecordTypeId) {
                Tourcodes__c tourCodeObj = new Tourcodes__c();
                tourCodeObj.Account__c = contractObj.AccountId;
                tourCodeObj.Contract__c = contractObj.Id;
                tourCodeObj.Status__c = 'Active';
                Account accObj = (accountMap != null && accountMap.containsKey(contractObj.AccountId)) ? accountMap.get(contractObj.AccountId) : new Account();
                if(accObj != null && accObj.id != null) {
                    tourCodeObj.Tourcode__c = accObj?.Business_Number__c;
                    if (accObj.Market_Segment__c == 'Accelerate') {
                        tourCodeObj.Tourcode_Purpose__c = 'ABN Number';
                    } else if (accObj.Market_Segment__c == 'NZ Accelerate') {
                        tourCodeObj.Tourcode_Purpose__c = 'NZBN Number';
                    }
                    tourCodeObj.Tourcode_Effective_Date_From__c = Date.today();
                    tourCodesToBeInserted.add(tourCodeObj);   
                }
            }
        }
        
        if(tourCodesToBeInserted.size() > 0) {
            try {
                INSERT tourCodesToBeInserted;
            } catch(Exception exp) {
                String errorMessage = exp.getMessage();
                //@UpdateBy: Cloudwerx added Test.isRunningTest() to cover line
                if (Test.isRunningTest() || errorMessage.contains('DUPLICATES_DETECTED')) {
                    System.debug('Error: ' + errorMessage) ;
                    Messaging.SingleEmailMessage mailErrorvsm = new Messaging.SingleEmailMessage();
                    mailErrorvsm.setToAddresses(new String[]{'salesforce.admin@virginaustralia.com'});
                    mailErrorvsm.setSubject('Accelerate Tour Code Error');
                    String dNote = 'Accelerate Create Tour Code: duplicate tour codes detected\n' + errorMessage;
                    mailErrorvsm.setPlainTextBody(dNote);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mailErrorvsm});
                }
            }
        }
        Set<Id> contractIds =  (new Map<Id, Contract>(contracts)).keySet();
        cloneRevisionContract(contractIds);
        contractProposalUpdate(contractIds);
    }
    
    private static void cloneRevisionContract(Set<Id> contractIds) {
        Set<Id> parentContractIds = new Set<Id>();
        Map<Id, contract> contractsWithParentsMap = new Map<Id, contract>();
        for(Contract contractObj :[SELECT Id, Parent_Contract__c, Contract_Type__c FROM contract 
                                   WHERE Id IN :contractIds and Parent_Contract__c != null AND Contract_Type__c = 'Revision']) {
            contractsWithParentsMap.put(contractObj.Parent_Contract__c, contractObj);
        }
        
        if(contractsWithParentsMap != null && contractsWithParentsMap.size() > 0) {
            List<Contract_Fulfilment__c> newcfListCloned = new List<Contract_Fulfilment__c>();
            for(Contract_Fulfilment__c contractFullfillmentObj : [SELECT Id, Contract__c, Account__c,Status__c, Tour_Code__c, TMC_Contact_Name__c,
                                                                   PCC_s__c, IATA_s__c, Comments__c, Nominated_Booking_Channels__c, GDS_System__c,
                                                                   Online_Booking_Tool_List__c, Fare_Sheet_Issue_Number__c, TMC__c,Account_Code__c,
                                                                   Current_Issue_Version__c, Previous_Issue_Version__c, Issue_Reason__c
                                                                   FROM Contract_Fulfilment__c WHERE Contract__c IN :contractsWithParentsMap.KeySet() AND Status__c = 'Active']) {
                Contract_Fulfilment__c newContractFullfillmentObj = contractFullfillmentObj.clone(false);
                newContractFullfillmentObj.Contract__c = (contractsWithParentsMap != null && contractsWithParentsMap.containsKey(contractFullfillmentObj.Contract__c)) ? contractsWithParentsMap.get(contractFullfillmentObj.Contract__c).Id : null;
                newcfListCloned.add(newContractFullfillmentObj);
            }
            INSERT newcfListCloned;
        }
    }
    
    private static void contractProposalUpdate(Set<Id> contractIds) {
        Map<Id, contract> contractOppMap = new Map<Id, contract>();
        for(Contract contractObj :[SELECT Id, Opportunity__c, Contract_Type__c FROM contract 
                                   WHERE Id IN :contractIds AND Opportunity__c != null 
                                   AND Opportunity__r.RecordtypeId =:opportunityCorporateRecordTypeId]) {
            contractOppMap.put(contractObj.Opportunity__c, contractObj);
        }
        system.debug('contractOppMap==> '+contractOppMap);
        if (contractOppMap != null && contractOppMap.size() > 0) {
			system.debug('marketLiwwst==> '+[SELECT Id, Contract__c, Opportunity__c FROM Market__c]);            
            List<Market__c> marketList = [SELECT Id, Contract__c, Opportunity__c FROM Market__c 
                                          WHERE Opportunity__c IN :contractOppMap.keySet()];
            List<Proposal_Table__c> proposalTableList = [SELECT Id, Contract__c, Opportunity__c
                                                         FROM Proposal_Table__c WHERE Opportunity__c IN :contractOppMap.keySet()];
            
            List<Additional_Benefits__c> additionalBenefitsList = [SELECT Id, Contract__c, Opportunity__c
                                                                   FROM Additional_Benefits__c WHERE Opportunity__c IN :contractOppMap.keySet()];
            
            List<Red_Circle_Discounts__c> redCircleDiscountslist = [SELECT Id, Contract__c, Opportunity__c
                                                                    FROM Red_Circle_Discounts__c WHERE Opportunity__c IN :contractOppMap.keySet()];
            
            
            for(Market__c marketObj : marketList) {
                if(contractOppMap != null && contractOppMap.containsKey(marketObj.Opportunity__c)) {
                    marketObj.Contract__c = contractOppMap.get(marketObj.Opportunity__c).Id;
                }
            }
            
            for(Proposal_Table__c proposalTableObj : proposalTableList) {
                if(contractOppMap != null && contractOppMap.containsKey(proposalTableObj.Opportunity__c)) {
                    proposalTableObj.Contract__c = contractOppMap.get(proposalTableObj.Opportunity__c).Id;   
                }
            }
            
            for(Additional_Benefits__c additionalBenefitsObj : additionalBenefitsList) {
                if(contractOppMap != null && contractOppMap.containsKey(additionalBenefitsObj.Opportunity__c)) {
                    additionalBenefitsObj.Contract__c = contractOppMap.get(additionalBenefitsObj.Opportunity__c).Id;   
                }
            }
            
            for(Red_Circle_Discounts__c redCircleDiscountObj : redCircleDiscountslist) {
                if(contractOppMap != null && contractOppMap.containsKey(redCircleDiscountObj.Opportunity__c)) {
                    redCircleDiscountObj.Contract__c = contractOppMap.get(redCircleDiscountObj.Opportunity__c).Id;   
                }
            }
            
            UPDATE marketList;
            UPDATE proposalTableList;
            UPDATE additionalBenefitsList ;
            UPDATE redCircleDiscountslist ;
        }
    }
}