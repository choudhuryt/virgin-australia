/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * 
 * 	Updated by Cloudwerx : Removed hardcoded Ids And added indentation
 */
 
@isTest
private class TestCaseCreateorUpdateContactTrigger 
{
	

    public static testMethod void testWebBulkContactsGetCreated() 
    {
    	// This code runs as the "GR8 System Admin [Business]" user
        User u1;

        try
        {
	         u1 = [select Id from User WHERE IsActive=True AND Profile.Name = 'GR8 System Admin [Business]'  LIMIT 1];
        } 
        catch (QueryException qe)
        {
			System.debug(qe);
        }
		
		//first get the record type id
		List<RecordType> recordTypeGRCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
												r.Description, r.BusinessProcessId From RecordType r
											where SobjectType = 'Case'
											and IsActive = true
											and DeveloperName = 'GR_Case' LIMIT 1];								    	
	    Test.startTest();	

	    List<Case> newCases = new List<Case>();
	    for (Integer i = 0; i<2; i++) 
	    {
	            Case c = new Case(SuppliedEmail='jdoe_test_test@doe.com' + i,
	            					Velocity_Member__c = false,
	                                SuppliedName='John Doe' + i,
	                                Subject='Feedback - Something' + i,
	                                Contact_First_Name__c='Tom'+i,
	                                Contact_Last_name__c = 'Johns'+i);
	            newCases.add(c);
	   }
	   
	   System.runas(u1)
       {
       		try
       		{
	        	insert newCases;
       		}
       		catch(Exception e)
       		{
       			System.debug(e);
       		}
	   }
	   
	   Test.stopTest();
	   
	   List<Id> newCaseIds = new List<Id>();
	   for (Case caseObj:newCases) 
	   {
	        newCaseIds.add(caseObj.Id);    
	   }          

        
        List<Case> updatedCases = [Select Id, ContactId, recordTypeId From Case Where Id in :newCaseIds];
	        
	    for (Case caseObj:updatedCases) 
	    {
	         System.debug(caseObj.Id + ' ' + caseObj.ContactId);
	        // System.assert(caseObj.ContactId!=null,'There should be no null contacts');
	         System.assert(caseObj.recordTypeId == recordTypeGRCase[0].id, 'Case must be of GR record type');
	     }
    }

    public static testMethod void testWebContactGetsCreated() 
    {
    	
    	// This code runs as the "GR8 System Admin [Business]" user
        User u1;

        try
        {
	         u1 = [select Id from User WHERE IsActive=True AND Profile.Name = 'GR8 System Admin [Business]'  LIMIT 1];
        } 
        catch (QueryException qe)
        {
			System.debug(qe);
        }
          	
		//first get the record type id
		List<RecordType> recordTypeGRCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Case'
										and IsActive = true
										and DeveloperName = 'GR_Case' LIMIT 1];
										
		List<RecordType> recordTypeGRContact = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Contact'
										and IsActive = true
										and DeveloperName = 'GR_Contact' LIMIT 1];		
																		    	
       	Test.startTest();	
        
        Case c = new Case(SuppliedEmail='jdoe_test_test@doe.com',
                            SuppliedName='John Doe',
                            Velocity_Member__c = false,
                            Subject='Feedback - Something',
                           Contact_First_Name__c='Tom',
                            Contact_Last_name__c = 'Johns');

	   System.runas(u1)
       {
       		try
       		{
	        	insert c;
       		}
       		catch(Exception e)
       		{
       			System.debug(e);
       		}
	   }
	   	
	   	Test.stopTest();
	   
        //List<Contact> johnDoes = [select Id, recordTypeId from Contact where Email='jdoe_test_test@doe.com'];

	    //System.assert(johnDoes.size()==1, 'There should be one Contact Created!');
	    //System.assert(johnDoes.get(0).recordTypeId==recordTypeGRContact[0].id, 'Contact created must be of GR record type');
        
        //Case caseObj = [Select Id, ContactId, recordTypeId From Case where Id=:c.Id];
       // System.assert(caseObj.ContactId!=null,'There should be no null contact on the case');
	    //System.assert(caseObj.recordTypeId == recordTypeGRCase[0].id, 'Case must be of GR record type');
    }

    public static testMethod void testWebNoDupesAreCreated() 
    {
    	// This code runs as the "GR8 System Admin [Business]" user
        User u1;

        try
        {
	         u1 = [select Id from User WHERE IsActive=True AND Profile.Name = 'GR8 System Admin [Business]'  LIMIT 1];
        } 
        catch (QueryException qe)
        {
			System.debug(qe);
        }    	
        									
		List<RecordType> recordTypeGRContact = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Contact'
										and IsActive = true
										and DeveloperName = 'GR_Contact' LIMIT 1];
										
       	Test.startTest();	
       	
	    Contact cnt1 = new Contact(FirstName = 'John',
	                                LastName = 'Doe',
	                                Email='jdoe_test_test@doe.com',
	                                recordTypeId=recordTypeGRContact[0].id);
		
	    Case case1 = new Case(SuppliedEmail='jdoe_test_test@doe.com',
	                            SuppliedName='John Doe',
	                            Subject='Feedback - Something',
	                           Contact_First_Name__c='Tom',
	                            Contact_Last_name__c = 'Johns'); 
	                            	
	    System.runas(u1)
        {
        	try
        	{
		    	insert cnt1;
	        	//insert case1;
        	}
        	catch(Exception e)
        	{
        		System.debug(e);
        	}
        }
        
        Test.stopTest();	

        List<Contact> johnDoes = [select Id from Contact where Email='jdoe_test_test@doe.com'];

        //there should be only 1 -- the trigger should not have created another
         System.assert(johnDoes.size()==1, 'There should be only one John Doe!');
    }

    public static testMethod void testWebExistingContactIsUpdated() 
    {
    	// This code runs as the "GR8 System Admin [Business]" user
        User u1;

        try
        {
	         u1 = [select Id from User WHERE IsActive=True AND Profile.Name = 'GR8 System Admin [Business]'  LIMIT 1];
        } 
        catch (QueryException qe)
        {
			System.debug(qe);
        }    	
        									
		List<RecordType> recordTypeGRContact = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Contact'
										and IsActive = true
										and DeveloperName = 'GR_Contact' LIMIT 1];
										
       	Test.startTest();	
       	
	    Contact cnt1 = new Contact(FirstName = 'John',
	                                LastName = 'Doe',
	                                Email='jdoe_test_test@doe.com',
	                                recordTypeId=recordTypeGRContact[0].id);
		
	    Case case1 = new Case(SuppliedEmail='jdoe_test_test@doe.com',
	                            SuppliedName='John Doe',
	                            Subject='Feedback - Something',
	                           Contact_First_Name__c='Tom',
	                            Contact_Last_name__c = 'Johns',
	                            Contact_Phone__c = '3201234',
	                            Contact_Mobile__c = '0211433234',
	                            Velocity_Number__c = '1231237891',
	                            Travel_Agent_ID__c = '987654'); 
	                            	
	    System.runas(u1)
        {
        	try
        	{
		    	insert cnt1;
	        	//insert case1;
        	}
        	catch(Exception e)
        	{
        		System.debug(e);
        	}
        }
        
        Test.stopTest();	

        List<Contact> johnDoes = [select Id, phone, mobilePhone, Velocity_Member__c, Velocity_Number__c,
        			    	travel_agent_id__c, I_am_a_travel_agent__c, RecordTypeId from Contact 
        			    	where Email='jdoe_test_test@doe.com'];
        System.assert(johnDoes.size()==1, 'There should be no new contacts created!');
        
        //check fields updated in contact
       // System.assertEquals(johnDoes[0].phone, case1.Contact_Phone__c);
       // System.assertEquals(johnDoes[0].mobilePhone, case1.Contact_Mobile__c);
      //  System.assertEquals(johnDoes[0].Velocity_Number__c, case1.Velocity_Number__c);
       // System.assertEquals(johnDoes[0].Velocity_Member__c, true);
      //  System.assertEquals(johnDoes[0].travel_agent_id__c, case1.Travel_Agent_ID__c);    
      //  System.assertEquals(johnDoes[0].I_am_a_travel_agent__c, true);                
    }
    
    public static testMethod void testManualBulkContactsGetCreated() 
    {
    	// This code runs as the "GR8 System Admin [Business]" user
        User u1;

        try
        {
	         u1 = [select Id from User WHERE IsActive=True AND Profile.Name = 'GR8 System Admin [Business]'  LIMIT 1];
        } 
        catch (QueryException qe)
        {
			System.debug(qe);
        }
		
		//first get the record type id
		List<RecordType> recordTypeGRCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
												r.Description, r.BusinessProcessId From RecordType r
											where SobjectType = 'Case'
											and IsActive = true
											and DeveloperName = 'GR_Case' LIMIT 1];								    	
	    Test.startTest();	

	    List<Case> newCases = new List<Case>();
	    for (Integer i = 0; i<2; i++) 
	    {
	            Case c = new Case(Contact_email__c='jdoe_test_test@doe.com' + i,
	            					Velocity_Member__c = false,
	                                Subject='Feedback - Something' + i,
	                                Contact_First_Name__c='Tom'+i,
	                                Contact_Last_name__c = 'Johns'+i);
	            newCases.add(c);
	   }
	   
	   System.runas(u1)
       {
       		try
       		{
	        	insert newCases;
       		}
       		catch(Exception e)
       		{
       			System.debug(e);
       		}
	   }
	   
	   Test.stopTest();
	   
	   List<Id> newCaseIds = new List<Id>();
	   for (Case caseObj:newCases) 
	   {
	        newCaseIds.add(caseObj.Id);    
	   }          

        
        List<Case> updatedCases = [Select Id, ContactId, recordTypeId From Case Where Id in :newCaseIds];
	        
	    for (Case caseObj:updatedCases) 
	    {
	         System.debug(caseObj.Id + ' ' + caseObj.ContactId);
	        // System.assert(caseObj.ContactId!=null,'There should be no null contacts');
	         System.assert(caseObj.recordTypeId == recordTypeGRCase[0].id, 'Case must be of GR record type');
	     }
    }

    public static testMethod void testManualContactGetsCreated() 
    {
    	
    	// This code runs as the "GR8 System Admin [Business]" user
        User u1;

        try
        {
	         u1 = [select Id from User WHERE IsActive=True AND Profile.Name = 'GR8 System Admin [Business]'  LIMIT 1];
        } 
        catch (QueryException qe)
        {
			System.debug(qe);
        }
          	
		//first get the record type id
		List<RecordType> recordTypeGRCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Case'
										and IsActive = true
										and DeveloperName = 'GR_Case' LIMIT 1];
										
		List<RecordType> recordTypeGRContact = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Contact'
										and IsActive = true
										and DeveloperName = 'GR_Contact' LIMIT 1];		
																		    	
       	Test.startTest();	
        
        Case c = new Case(Contact_email__c='jdoe_test_test@doe.com',
                            Velocity_Member__c = false,
                            Subject='Feedback - Something',
                           Contact_First_Name__c='Tom',
                            Contact_Last_name__c = 'Johns',
                          Additional_Details__c='I am a travel Agent');

	   System.runas(u1)
       {
       		try
       		{
	        	insert c;
       		}
       		catch(Exception e)
       		{
       			System.debug(e);
       		}
	   }
	   	
	   	Test.stopTest();
	   
    }

    public static testMethod void testManualNoDupesAreCreated() 
    {
    	// This code runs as the "GR8 System Admin [Business]" user
        User u1;

        try
        {
	         u1 = [select Id from User WHERE IsActive=True AND Profile.Name = 'GR8 System Admin [Business]'  LIMIT 1];
        } 
        catch (QueryException qe)
        {
			System.debug(qe);
        }    	
        									
		List<RecordType> recordTypeGRContact = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Contact'
										and IsActive = true
										and DeveloperName = 'GR_Contact' LIMIT 1];
										
       	Test.startTest();	
       	
	    Contact cnt1 = new Contact(FirstName = 'John',
	                                LastName = 'Doe',
	                                Email='jdoe_test_test@doe.com',
	                                recordTypeId=recordTypeGRContact[0].id);
		
	    Case case1 = new Case(Contact_email__c='jdoe_test_test@doe.com',
	                            Subject='Feedback - Something',
	                           Contact_First_Name__c='Tom',
	                            Contact_Last_name__c = 'Johns',
	                            Additional_Details__c='I am a travel Agent'); 
	                            	
	    System.runas(u1)
        {
        	try
        	{
		    	insert cnt1;
	        	//insert case1;
        	}
        	catch(Exception e)
        	{
        		System.debug(e);
        	}
        }
        
        Test.stopTest();	

    }    

    public static testMethod void testManualExistingContactIsUpdated() 
    {
    	// This code runs as the "GR8 System Admin [Business]" user
        User u1;

        try
        {
	         u1 = [select Id from User WHERE IsActive=True AND Profile.Name = 'GR8 System Admin [Business]'  LIMIT 1];
        } 
        catch (QueryException qe)
        {
			System.debug(qe);
        }    	
        									
		List<RecordType> recordTypeGRContact = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Contact'
										and IsActive = true
										and DeveloperName = 'GR_Contact' LIMIT 1];
										
       	Test.startTest();	
       	
	    Contact cnt1 = new Contact(FirstName = 'John',
	                                LastName = 'Doe',
	                                Email='jdoe_test_test@doe.com',
	                                recordTypeId=recordTypeGRContact[0].id);
		
	    Case case1 = new Case(Contact_email__c='jdoe_test_test@doe.com',
	                            Subject='Feedback - Something',
	                           Contact_First_Name__c='Tom',
	                            Contact_Last_name__c = 'Johns',
	                            Contact_Phone__c = '3201234',
	                            Contact_Mobile__c = '0211433234',
	                            Velocity_Number__c = '1231237891',
	                            Travel_Agent_ID__c = '987654',
	                            Additional_Details__c='I am a travel Agent',
	                            Velocity_Type__c='Red'); 
	                            	
	    System.runas(u1)
        {
        	try
        	{
		    	insert cnt1;
	        	//insert case1;
        	}
        	catch(Exception e)
        	{
        		System.debug(e);
        	}
        }
        
        Test.stopTest();	

                          
    }

    public static testMethod void testManualCaseWithNonGRUser() 
    {
    	
    	// This code runs as the Sys Admin user (non gr)
        User u1;

        try
        {
	         u1 = [select Id from User WHERE IsActive=True AND Profile.Name = 'System Administrator'  LIMIT 1];
        } 
        catch (QueryException qe)
        {
			System.debug(qe);
        }
          	
		//first get the record type id
		List<RecordType> recordTypeGRCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Case'
										and IsActive = true
										and DeveloperName = 'GR_Case' LIMIT 1];
										
		List<RecordType> recordTypeGRContact = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Contact'
										and IsActive = true
										and DeveloperName = 'GR_Contact' LIMIT 1];		
																		    	
       	Test.startTest();	
       	
       	List<Contact> johnDoesBeforeTest = [select Id, recordTypeId from Contact limit 50];
        
        Case c = new Case(Velocity_Member__c = false,
                            Subject='Feedback - Something');

	   System.runas(u1)
       {
       		try
       		{
	        	//insert c;
       		}
       		catch(Exception e)
       		{
       			System.debug(e);
       		}
	   }
	   	
	   	Test.stopTest();
	   
       
    }
    
    public static testMethod void testPCCIATAFromWeb() 
    {
    	// This code runs as the "GR8 System Admin [Business]" user
        User u1;

        try
        {
	         u1 = [select Id from User WHERE IsActive=True AND Profile.Name = 'GR8 System Admin [Business]'  LIMIT 1];
        } 
        catch (QueryException qe)
        {
			System.debug(qe);
        }
		
								    	
	    Test.startTest();	

	    List<Case> newCases = new List<Case>();
        
        Id  caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('GR Case').getRecordTypeId();

	    for (Integer i = 0; i<1; i++) 
	    {
	            Case c = new Case(SuppliedEmail='jdoe_test_test@doe.com' + i,
	            					Velocity_Member__c = false,
	                                SuppliedName='John Doe' + i,
	                                Subject='Feedback - Something' + i,
	                                Contact_First_Name__c='Tom'+i,
	                                Contact_Last_name__c = 'Johns'+i,
                                    //RecordTypeID = '012900000008LAT',
                                    RecordTypeId = caseRecTypeId,
                                    PCC_From_Web__c = 'MELA8' );
	            newCases.add(c);
	   }

       System.runas(u1)
       {
       		try
       		{
	        	insert newCases;
       		}
       		catch(Exception e)
       		{
       			System.debug(e);
       		}
	   }                           
        
	    for (Integer i = 0; i<1; i++) 
	    {
	            Case c = new Case(SuppliedEmail='jdoe_test_test@doe.com' + i,
	            					Velocity_Member__c = false,
	                                SuppliedName='John Doe' + i,
	                                Subject='Feedback - Something' + i,
	                                Contact_First_Name__c='Tom'+i,
	                                Contact_Last_name__c = 'Johns'+i,
                                    //RecordTypeID = '012900000008LAT',
                                    RecordTypeId = caseRecTypeId,
                                    IATA_From_Web__c = '0234685' );
	            newCases.add(c);
	   }

	   
	   System.runas(u1)
       {
       		try
       		{
	        	insert newCases;
       		}
       		catch(Exception e)
       		{
       			System.debug(e);
       		}
	   }
	   
	   Test.stopTest();
	   
	
  
    }
    
}