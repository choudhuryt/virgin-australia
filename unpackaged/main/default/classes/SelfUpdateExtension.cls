public with sharing class SelfUpdateExtension {

    public List<Account> acct {get;set;}  
    private Account a;  
    public Account accoutdetail{get;set;} 
	public ApexPages.StandardController mycontroller;
	public string recId{get; set;}
	public Account_Self_Update__c newAcctSelfUpdate {get;set;}
	private Account_Self_Update__c oldAcctSelfUpdate;
	private String noteChanges;
    public boolean dispAcctSect{get;set;}
    public boolean dispContSect{get;set;}
    
	public SelfUpdateExtension( ApexPages.StandardController stdController ) 
	{    
		 this.mycontroller = stdController;
         a=(Account)myController.getrecord();
         recId = ApexPages.currentPage().getParameters().get('Id');     
         dispAcctSect = false ;       
         dispContSect = false ; 
        
         acct=[select id,Name ,BillingStreet, Business_Number__c,BillingCity , BillingState,BillingPostalCode,Update_Account__c  from Account where id =:ApexPages.currentPage().getParameters().get('id')];
        
         accoutdetail = acct.get(0);
        
       
         newAcctSelfUpdate = new Account_Self_Update__c();
        
	}

   
    


	public PageReference clickSave() {
        
		 newAcctSelfUpdate.Account__c = accoutdetail.Id;        
         newAcctSelfUpdate.Notes__c = 'Credit Option';   
         newAcctSelfUpdate.Updated__c = true;
         insert newAcctSelfUpdate;
         PageReference pageRef = Page.SelfUpdateSuccessful;
    	 pageRef.setRedirect(true);    		
    	 return pageRef;
	}

    
    
	public PageReference redirectIfInvalid()
    {
        
		Id acctId = ApexPages.currentPage().getParameters().get('id');        
	
        
       
		
	/*	if( acctId != null )
		{
			List<Mailer_Expiry__c> expRecords = [Select Id, ID__c, Expiry_Date__c from Mailer_Expiry__c
												Where Link_Type__c = 'FFC Option' ];
            
            system.debug('eXPIRTY SETTING' + expRecords[0].Expiry_Date__c  );

			if( expRecords.isEmpty() ||
				(!expRecords.isEmpty() && expRecords[0].Expiry_Date__c == null) ||
				(!expRecords.isEmpty() && expRecords[0].Expiry_Date__c != null && Date.today() > expRecords[0].Expiry_Date__c) )
			{
				return Page.RequestCannotBeCompleted;
			}
			
		}*/

		if( acctId != null )
		{
			List<Account_Self_Update__c> aSelfUpdateRecords= [Select Id, Account__c, Account_Name__c, Billing_Street__c, Annual_DOM_Spend_ALL_Carriers__c,
																	 Annual_INT_Spend_ALL_Carriers__c, Billing_City__c, Business_Number__c,
																	 Billing_State_Province__c, Billing_Post_Code__c, Notes__c, Please_Contact_Me__c,
																	 Last_Self_Update_Date_Time__c, Updated__c, Self_Update_Changes__c 
															        FROM Account_Self_Update__c
															        WHERE Account__c =: acctId
                                                                    and   Updated__c = true
                                                                    and Notes__c = 'Credit Option'];
			if( aSelfUpdateRecords.size()>0)
            {     
				return Page.RequestAlreadyProcessed;
			
		     }
           
         }
        
		 return null;
		
	}
    

}