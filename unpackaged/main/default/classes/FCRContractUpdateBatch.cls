global class FCRContractUpdateBatch implements Database.Batchable<sObject> 
{
    List<contract_addendum__c> CAListGlobal = new List<contract_addendum__c>();
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        // collect the batches of records or objects to be passed to execute
        string BatchLimit = Label.FCR_Batch_Limit; 
        String query;
        if(!Test.isRunningTest())
        {
             //query = 'SELECT Id from contract where fcr_processed__c = false and status not in (\'deactivated\',\'rejected\') limit '+BatchLimit;
             query = 'select AccountId, id from contract where Account.recordtype.name in (\'Corporate\', \'Government\') and status = \'Activated\' and Account.Owner.Name not in (\'%accelerate%\', \'Justine Flegler\') and accountid != \'\' and fcr_processed__c != true limit '+BatchLimit;
        }    
        else
        {
			query = 'SELECT Id from contract where id = \'8006F000001Zbp2QAC\'';            
        }    
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<contract> ContractList) 
    {
        try
        {
            
            for(contract Contract : ContractList) 
            {        
                set<id> contractIdSet = new set<Id>();
                contractIdSet.add(Contract.id);
                FCRUtilities.removeDOMRegional(contractIdSet);
            }
        }
    	catch(Exception e) 
        {
            System.debug(e);
        }
         
    }   
     
    global void finish(Database.BatchableContext BC) 
    {
        system.debug('Inside Finish');
        // execute any post-processing operations like sending email
        if(!Test.isRunningTest())  
        {
  			//System.scheduleBatch(new FCRContractUpdateBatch(), 'FCR Contract Update Job', 1, 1);
        }   
    }
}