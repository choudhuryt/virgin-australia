/**
* @description       : Test class for DomesticAddendumDiscountController
* @Updated By         : Cloudwerx
**/
@isTest
private class TestDomesticAddendumDiscountController {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        
        Contract_Addendum__c testContractAddendumObj = TestDataFactory.createTestContractAddendum(testContractObj.Id, '15', 'Tier 1', 'Tier 1', true, 'SYDMEL', 'PERKTH', 'Draft');
        INSERT testContractAddendumObj;
        
        Opportunity testOpportunityObj = TestDataFactory.createTestOpportunity('testOpp1', 'Sales Opportunity Analysis', testAccountObj.Id, Date.today(), 1.00);
        INSERT testOpportunityObj; 
        
        List<Proposal_Table__c> testProposalTables = new List<Proposal_Table__c>();
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'DOM Mainline'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'DOM Regional'));
        INSERT testProposalTables;
    }
    
    @isTest
    private static void testDomesticAddendumDiscountController() {
        Contract_Addendum__c testContractAddendumObj = [SELECT Id FROM Contract_Addendum__c];
        
        List<Proposed_Discount_Tables__c> testProposedDiscountTables = new List<Proposed_Discount_Tables__c>();
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('DOM Mainline', testContractAddendumObj.Id, 10, 'Business', 'J'));
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('DOM Regional', testContractAddendumObj.Id, 10, 'Business', 'D'));
        INSERT testProposedDiscountTables;
        
        Test.startTest();
        PageReference pageRef = Page.DomesticAddendumDiscount;
        pageRef.getParameters().put('id', testContractAddendumObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractAddendumObj);
        DomesticAddendumDiscountController lController = new DomesticAddendumDiscountController(conL);        
        PageReference initDiscPageReference = lController.initDisc();
        PageReference savePageReference = lController.save();
        PageReference qSavePageReference = lController.qsave();
        PageReference newDomPageReference = lController.newDom();
        PageReference newRegPageReference = lController.newReg();
        PageReference remDomPageReference = lController.remDom();
        PageReference remRegPageReference = lController.remReg();
        PageReference cancelPageReference = lController.cancel();
        Test.stopTest();
        //Asserts
        system.assertEquals(null, initDiscPageReference);
        system.assertEquals('/' + testContractAddendumObj.Id, savePageReference.getUrl());
        system.assertEquals('/apex/DomesticAddendumDiscount?id=' + testContractAddendumObj.Id, qSavePageReference.getUrl());
        system.assertEquals('/apex/DomesticAddendumDiscount?id=' + testContractAddendumObj.Id, newDomPageReference.getUrl());
        system.assertEquals('/apex/DomesticAddendumDiscount?id=' + testContractAddendumObj.Id, newRegPageReference.getUrl());
        system.assertEquals('/apex/DomesticAddendumDiscount?id=' + testContractAddendumObj.Id, remDomPageReference.getUrl());
        system.assertEquals('/apex/DomesticAddendumDiscount?id=' + testContractAddendumObj.Id, remRegPageReference.getUrl());
        system.assertEquals('/' + testContractAddendumObj.Id, cancelPageReference.getUrl());
    }
    
    @isTest
    private static void testDomesticAddendumDiscountControllerWithNoProposedDiscountTables() {
        Contract_Addendum__c testContractAddendumObj = [SELECT Id, Red_Circle__c  FROM Contract_Addendum__c];
        testContractAddendumObj.Red_Circle__c  = false;
        UPDATE testContractAddendumObj;
        
        Test.startTest();
        PageReference pageRef = Page.DomesticAddendumDiscount;
        pageRef.getParameters().put('id', testContractAddendumObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractAddendumObj);
        DomesticAddendumDiscountController lController = new DomesticAddendumDiscountController(conL);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newDomPageReference = lController.newDom();
        PageReference newRegPageReference = lController.newReg();
        Test.stopTest();
        //Asserts
        system.assertEquals(null, initDiscPageReference);
        system.assertEquals(null, newDomPageReference);
        system.assertEquals(null, newRegPageReference);
        List<Proposed_Discount_Tables__c> discountTablesDOMMainline = lController.searchResults;
        List<Proposed_Discount_Tables__c> discountTablesDOMRegional = lController.searchResults1;
        //Asserts
        system.assertEquals(true, discountTablesDOMMainline.size() > 0);
        system.assertEquals(true, discountTablesDOMRegional.size() > 0);
        //Asserts
        List<Proposal_Table__c> proposalTables = [SELECT Id, DISCOUNT_OFF_PUBLISHED_FARE__c, ELIGIBLE_FARE_TYPE__c, VA_ELIGIBLE_BOOKING_CLASS__c FROM Proposal_Table__c];
        system.assertEquals(proposalTables[0].DISCOUNT_OFF_PUBLISHED_FARE__c, discountTablesDOMMainline[0].DISCOUNT_OFF_PUBLISHED_FARE__c);
        system.assertEquals(proposalTables[0].ELIGIBLE_FARE_TYPE__c, discountTablesDOMMainline[0].ELIGIBLE_FARE_TYPE__c);
        system.assertEquals(proposalTables[0].VA_ELIGIBLE_BOOKING_CLASS__c, discountTablesDOMMainline[0].VA_ELIGIBLE_BOOKING_CLASS__c);
        system.assertEquals(proposalTables[1].DISCOUNT_OFF_PUBLISHED_FARE__c, discountTablesDOMRegional[0].DISCOUNT_OFF_PUBLISHED_FARE__c);
        system.assertEquals(proposalTables[1].ELIGIBLE_FARE_TYPE__c, discountTablesDOMRegional[0].ELIGIBLE_FARE_TYPE__c);
        system.assertEquals(proposalTables[1].VA_ELIGIBLE_BOOKING_CLASS__c, discountTablesDOMRegional[0].VA_ELIGIBLE_BOOKING_CLASS__c);
    }
}