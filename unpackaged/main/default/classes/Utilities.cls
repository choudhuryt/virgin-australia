public class Utilities {
    /*
	*	Convert database format string to Datetime
	*	Input: yyyy-MM-ddTHH:mm:ssZ
	*/
    public static DateTime convert2DateTime(String dbString){
        if (dbString!=null && !String.isEmpty(dbString)){
            if (dbString.containsAny('TZ')) {
                dbString = dbString.replace('T', ' ').replace('Z','');
                return DateTime.valueOf(dbString);
            }
        }
        return null;
    }
    
    /*
	*	Convert database format string 'DD/mm/YYYY' to Datetime
	*	Input: yyyy-MM-ddTHH:mm:ss
	*/
    public static String convert2DateTimeString(String dbString){
        if (dbString!=null && !String.isEmpty(dbString)){
            if (!dbString.containsAny('TZ')) {
            	//String col = '30/01/2014';
				String[] tempStr = dbString.split('/');
				Integer d = Integer.valueOf(tempStr[0]);
				Integer m = Integer.valueOf(tempStr[1]);
				Integer y = Integer.valueOf(tempStr[2]);
				Date dToday = Date.newInstance(y,m,d);
				Datetime dt = Datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
				String out = Utilities.convertDateTime(dt);
				System.debug('hanhluu::convert2DateTime::' + out);
				return out;
            }
        }
        return null;
    }
    
    
    /*
	*	Convert Datatime to string in database format
	*	Output: yyyy-MM-ddTHH:mm:ss
	*/
    public static String convertDateTime(DateTime dt){
        if (dt!= null) {
        	return dt.format('yyyy-MM-dd\'T\'HH:mm:ss');
        }
        return null;
    }
    
    /**
	*	Convert database format string to Date
	*	Input: yyyy-MM-dd or yyyy-M-dd or yyyy-M-d
	*/
    public static Date convert2Date(String dbString){
        try {
            if (dbString!=null && !String.isBlank(dbString)){
                String datePatern = '([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})';
                Pattern myPattern = Pattern.compile(datePatern);
                Matcher myMatcher = myPattern.matcher(dbString);
                if (myMatcher.matches()) {
                    String strYear = myMatcher.group(1);
                    String strMonth = myMatcher.group(2);
                    String strDay = myMatcher.group(3);
                    
                    return Date.newInstance(Integer.valueOf(strYear), Integer.valueOf(strMonth), Integer.valueOf(strDay));
                }
            }
        } catch (Exception ex){
            System.debug('Utilities-convert2Date: ' + ex.getMessage());
        }
        return null;
    }
    
    /*
	*	Convert database datetime string format to Date
	*	Input: yyyy-MM-ddTHH:mm:ss (eg: 2012-01-16T18:23:47)
	*/
    public static Date convertStringToDate(String datetimevalue){
        Date d = null;
        try{
            if(datetimevalue!=null){
                datetimevalue = datetimevalue.replace('T',' ');
                d = (DateTime.valueOf(datetimevalue)).date();
                System.debug('date:' + d.format());
            }
        }catch(Exception ex){
			System.debug('Utilities-convertStringToDate: ' + ex.getMessage());
        }
        return d;
    }
    
    /*
	*	Combine date string and time string to DateTime
	*	Intput: yyyy-MM-dd  and  HH:mm:ss
	*	Output: DateTime
	*/
    public static DateTime mergeDateTime(String strDate, String strTime){
        try {
            if (strDate!=null & strTime!=null) {
                Integer posT = strTime.indexOf('T');
                strTime = strTime.substring(posT+1);
                return DateTime.valueOf(strDate + ' ' + strTime);
            }
        } catch (Exception ex){
            System.debug('Utilities-mergeDateTime: ' + ex.getMessage());
        }
        return null;
    }
    
    /*
     * Convert Date value to String format
     * Output: yyyy-MM-dd
	*/
    public static String convertDate2String(Date dateValue){
        return String.valueOf(dateValue).substring(0,10);
    }
    
    /*
     * Break Flight Number to Airline code and 4-digit format number
     * Input: VA0052, 52, 000052, SG00034
     * Output: VA + 0052, SG + 0034
     */
    public static List<String> formatFlightNumber(String flightNumber){
        String numPattern = '([a-zA-Z]*)([0-9]+)';
        Pattern myPattern = Pattern.compile(numPattern);
        Matcher myMatcher = myPattern.matcher(flightNumber);
        if (myMatcher.matches()) {
            List<String> result = new List<String>();
            String code = myMatcher.group(1);
            if (String.isNotBlank(code)){
                result.add(code);
            } else {
                result.add('VA');
            }
            
            String num = myMatcher.group(2);
            num = '000'+num;
            num = num.substring(num.length()-4);
            result.add(num);
            
            return result;
        }
        return new List<String>{'', flightNumber};
    }

    /**
    * @description This method is used to get RecordType Id according to name and SObjectType 
    * @param sObjectType 
    * @param recordTypeName
    * @return Id 
    **/
    public static Id getRecordTypeId(String sObjectType, String recordTypeName) {
        if (String.isBlank(sObjectType) || String.isBlank(recordTypeName)) {
            return null;
        }
        Id recordTypeId = Schema.getGlobalDescribe().get(sObjectType).getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeName)?.getRecordTypeId();
        return (recordTypeId != null ) ? recordTypeId : Schema.getGlobalDescribe().get(sObjectType).getDescribe().getRecordTypeInfosByName().get(recordTypeName)?.getRecordTypeId();
    }

    /**
    * @description This method is used to get user Id
    * @param userId 
    * @return Id 
    **/
    public static Id getOwnerId(Id userId) {
        if (String.isBlank(userId)) {
            return null;
        }
        Id ownerId = [SELECT Id FROM User WHERE Id =:userId AND isActive = TRUE LIMIT 1]?.Id;
        return String.isNotBlank(ownerId) ? ownerId : UserInfo.getUserId();
    }
    
    /**
    * @description This method is used to get user Id by Name
    * @param userName
    * @return Id 
    **/
    public static Id getOwnerIdByName(String userName) {
        if (String.isBlank(userName)) {
            return null;
        }
        Id ownerId = [SELECT Id FROM User WHERE Name =:userName AND isActive = TRUE LIMIT 1]?.Id;
        return String.isNotBlank(ownerId) ? ownerId : UserInfo.getUserId();
    }
}