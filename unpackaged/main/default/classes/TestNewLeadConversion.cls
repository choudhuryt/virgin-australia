@isTest


public with sharing class TestNewLeadConversion {
    
  private static Id leadId;
   static {
     
//Lead testLead = new Lead();
//testLead.FirstName = 'Test First';
//testLead.LastName = 'Test Last';
//testLead.Company = 'Test Co';

        /////////////////////
        // Create a Lead l1
        /////////////////////
         Lead testLead = new Lead();
         CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
         testLead = commonObjectsForTest.CreateNewlead(0);
         
         //l1.OwnerId = u1.Id;     
         /*testLead.LastName = 'test500'; 
         testLead.Company = 'TEST'; 
         testLead.Lead_Type__c = 'Corporate';
         testLead.Target_Markets__c = 'Thailand';
         testLead.Estimated_Spend__c = '1m+';
         testLead.Travel_Policy__c = 'Flexi Only';
         testLead.Booked_Via__c = 'TMC';
         testLead.Status = 'New';
         testLead.Country = 'Australia';
         */
         //insert testLead;

        insert testLead;
    leadId = testLead.Id;

    Account acc = new Account();
    acc = commonObjectsForTest.CreateAccountObject(0);   
         acc.Target_Markets__c = 'Thailand';
         acc.Estimated_Spend__c = '1m+';
         acc.Travel_Policy__c = 'Flexi Only';
         acc.Booked_Via__c = 'TMC';
         acc.Name = 'TEST177';
         acc.BillingCountry = 'Australia';
         acc.BillingPostalCode ='4000';
         acc.BillingState ='QLD';
         acc.BillingStreet = 'Test';
//MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
        acc.BillingCity = 'MAL';
        acc.Billing_Country_Code__c = 'AU';
        acc.Industry_Type__c = 'Engineering';
        acc.Account_Lifecycle__c = 'Offer';
//MOD-END CILAG INS CASE# 00129600 09.23.16
    insert acc;
 
    Key_Routes__c keyRoutes = new Key_Routes__c();
    keyRoutes.Lead__c = testLead.id;
    keyRoutes.Account__c = acc.id; 
    insert keyRoutes;
    

  }

static testMethod void Test_LeadConvert(){

test.StartTest();
Database.LeadConvert lc = new database.LeadConvert(); 
lc.setLeadId(leadId);
lc.setDoNotCreateOpportunity(True); 

LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
lc.setConvertedStatus(convertStatus.MasterLabel);

Database.LeadConvertResult lcr = Database.convertLead(lc);
System.assert(lcr.isSuccess());

test.StopTest();


   

}
    

}