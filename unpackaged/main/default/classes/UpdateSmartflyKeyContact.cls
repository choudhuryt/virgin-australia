/*
 *  Updated by Cloudwerx: Refactored the below code.
 * 
 * 
 */

public class UpdateSmartflyKeyContact {
    @InvocableMethod    
    public static void UpdateSmartflyKeyContact(List<Id> ContactIds) {
        
        List<Contact> lstnewconUpdate = new List<Contact>(); 
        List<Contact> lstoldconUpdate = new List<Contact>(); 
        
        Set<Id> accountIds = new Set<Id>();
        List<Contact> contacts =  [SELECT Id, AccountId, SmarftFly_Tourcode__c, Key_Contact__c  FROM Contact WHERE id =:ContactIds];
        for (Contact contactObj :contacts) {
            if (contactObj.AccountId != null) {
                accountIds.add(contactObj.AccountId);   
            }
        }
        
        Map<Id, String> smartTourCodesMap = new Map<Id, String>();
        for (Contact oldContactObj :[SELECT Id, AccountId, SmarftFly_Tourcode__c, Key_Contact__c FROM Contact 
                                     WHERE AccountId IN :accountIds AND SmarftFly_Tourcode__c != null]) {
            smartTourCodesMap.put(oldContactObj.AccountId, oldContactObj.SmarftFly_Tourcode__c);
            oldContactObj.SmarftFly_Tourcode__c  = ''; 
            oldContactObj.Key_Contact__c = false ;
            lstoldconUpdate.add(oldContactObj);
        }
        
        UPDATE lstoldconUpdate;
        
        for(Contact contactObj :contacts) {    
            contactObj.SmarftFly_Tourcode__c  = (smartTourCodesMap != null && smartTourCodesMap.containsKey(contactObj.AccountId)) ? smartTourCodesMap.get(contactObj.AccountId) : '';
            contactObj.Key_Contact__c = true ;
            lstnewconUpdate.add(contactObj);
        } 
        
        UPDATE lstnewconUpdate;
    }
}