/** * File Name      : TestDomesticDiscountController
* Description        : This Apex Test Class is the Test Class for DomesticDiscountController  
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Ed 'the man' Stachyra
* * Date             : Updated 28 February 2013
* * Technical Task ID: 
* * Notes            :  The test class for this file is:  TestDomesticDiscountController
* Modification Log =============================================================== 
Ver Date Author Modification --- ---- ------ -------------
* */
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestUSInternationalDiscountController {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        
        //setup account
        Account account                 = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
    
            
        insert account; 
 
        Contract contractOld = new Contract();
        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        
        
        insert contractOld;
         
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(contractOld);
        USInternationalController lController = new USInternationalController(conL);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conX = new ApexPages.StandardController(contractOld);
        USInternationalController xController = new USInternationalController(conX);
        Test.startTest();
        PageReference  ref1 = Page.USInternationalDiscountPage;
        ref1 = lController.initDisc();
        ref1 = lController.newInterUSA();
        ref1 = lController.newInterAUNZ();
        ref1 = lController.save();
        ref1 = lController.qsave();
        ref1 = lController.newInterUSA();
        ref1 = lController.newInterAUNZ();
        ref1 = lController.remAUNZ();
        ref1 = lController.remUSACAN();
        ref1 = lController.cancel();
        test.stopTest();
        
    }
 
    
}