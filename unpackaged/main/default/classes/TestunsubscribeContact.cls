public with sharing class TestunsubscribeContact {



public static testMethod void unSubscribe(){

		
	// Create a new contact		
	Contact ListFailContact = new Contact();
	Contact newContact = new Contact();
	
	newContact.FirstName ='Andy';
	newContact.LastName ='C';
	
	
	
	insert newContact;
	
	Contact exsisitingContract = new Contact();
	exsisitingContract.LastName ='D';
	insert exsisitingContract;
	
	Subscriptions__c newSubscription = new Subscriptions__c();
	newSubscription.Contact_ID__c =exsisitingContract.Id;
	
	insert newSubscription; 
	
	String testISDN;
	//Test converage for the myPage visualforce page

	 Test.startTest();
	 
	PageReference pageRef1 = Page.UnsubscribeContact;
	
	Test.setCurrentPageReference(pageRef1);

	ApexPages.currentPage().getParameters().put('cid', exsisitingContract.Id);
	ApexPages.currentPage().getParameters().put('t', '1' );
	
	// create an instance of the controller
	
	ApexPages.StandardController conL1 = new ApexPages.StandardController(exsisitingContract);
	
	UnsubscribeContact unsubscribe1 = new UnsubscribeContact(conL1);
	unsubscribe1.unsubscribe();
	unsubscribe1.name();
	
	//checking unsubscribe with type 2
	
	PageReference pageRef2 = Page.UnsubscribeContact;

	Test.setCurrentPageReference(pageRef2);

	ApexPages.currentPage().getParameters().put('cid', exsisitingContract.Id);
	ApexPages.currentPage().getParameters().put('t', '2' );

	// create an instance of the controller
	
	ApexPages.StandardController conL2 = new ApexPages.StandardController(exsisitingContract);
	UnsubscribeContact unsubscribe2 = new UnsubscribeContact(conL2);
	unsubscribe2.unsubscribe();
	unsubscribe2.name();
	//checking unsubscribe with type 3
	
		PageReference pageRef3 = Page.UnsubscribeContact;
		Test.setCurrentPageReference(pageRef3);

	ApexPages.currentPage().getParameters().put('cid', exsisitingContract.Id);
	ApexPages.currentPage().getParameters().put('t', '3' );

	// create an instance of the controller
	
	ApexPages.StandardController conL3 = new ApexPages.StandardController(exsisitingContract);
	
	UnsubscribeContact unsubscribe3 = new UnsubscribeContact(conL3);
	unsubscribe3.unsubscribe();
	unsubscribe3.name();
	
		//checking unsubscribe with type 4
	
	PageReference pageRef4 = Page.UnsubscribeContact;

	Test.setCurrentPageReference(pageRef4);

	ApexPages.currentPage().getParameters().put('cid', exsisitingContract.Id);
	ApexPages.currentPage().getParameters().put('t', '4' );
	
	// create an instance of the controller
	
	ApexPages.StandardController conL4 = new ApexPages.StandardController(exsisitingContract);

	UnsubscribeContact unsubscribe4 = new UnsubscribeContact(conL4);
	unsubscribe4.unsubscribe();
	unsubscribe4.name();
	
	//testing currentContact.Invitations
	
	PageReference pageRef5 = Page.UnsubscribeContact;

	Test.setCurrentPageReference(pageRef5);

	ApexPages.currentPage().getParameters().put('cid', exsisitingContract.Id);
	ApexPages.currentPage().getParameters().put('t', '5' );

	// create an instance of the controller
	
	ApexPages.StandardController conL5 = new ApexPages.StandardController(exsisitingContract);
	
	UnsubscribeContact unsubscribe5 = new UnsubscribeContact(conL5);
	unsubscribe5.unsubscribe();
	unsubscribe5.name();
	
	//Testing if parm1 "cid" have null values for Unscubscribe Class
	
	PageReference pageRefFailTest = Page.UnsubscribeContact;

	Test.setCurrentPageReference(pageRefFailTest);

	ApexPages.currentPage().getParameters().put('cid', null );
	ApexPages.currentPage().getParameters().put('t', null );
	
	// create an instance of the controller
	
	ApexPages.StandardController conLFail = new ApexPages.StandardController(exsisitingContract);
	
	UnsubscribeContact unsubscribeFail = new UnsubscribeContact(conLFail);
	unsubscribeFail.contactID =exsisitingContract.Id;
	unsubscribeFail.unsubscribe();
	
	//Testing if parm1 "t" have null values for Unscubscribe Class
	
	PageReference pageRefFailTest1 = Page.UnsubscribeContact;

	Test.setCurrentPageReference(pageRefFailTest1);

	ApexPages.currentPage().getParameters().put('cid', '1');
	ApexPages.currentPage().getParameters().put('t', null );
	
	// create an instance of the controller
	
	ApexPages.StandardController conLFail1 = new ApexPages.StandardController(exsisitingContract);
	
	UnsubscribeContact unsubscribeFail1 = new UnsubscribeContact(conLFail1);
	unsubscribeFail1.unsubscribe();
	unsubscribeFail1.contactID =exsisitingContract.Id;
	
	
	//testing exception ion list object
	PageReference pageRefListFail = Page.UnsubscribeContact;

	Test.setCurrentPageReference(pageRefListFail);

	ApexPages.currentPage().getParameters().put('cid', '123');
	ApexPages.currentPage().getParameters().put('t', '1' );
	
	// create an instance of the controller
	
	ApexPages.StandardController conLListFail = new ApexPages.StandardController(ListFailContact);
	
	UnsubscribeContact unsubscribeListFail = new UnsubscribeContact(conLListFail);
	unsubscribeListFail.unsubscribe();
	
	test.stopTest();
		
	}

}