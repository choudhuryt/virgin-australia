@isTest 
public class appendEmailWithInvalidBatchTest {
    static testMethod void appendEmailWithInvalidMethod(){
        {
            List<Contact> lstContact= new List<Contact>();
            for(Integer i=0 ;i <200;i++)
            {
                Contact Con = new Contact();
                Con.LastName ='Name'+i;
                Con.Email='test'+i+'@test.com';
                lstContact.add(Con);
            }
            
            insert lstContact;
            
            Test.startTest();
            
            appendEmailWithInvalidBatch obj = new appendEmailWithInvalidBatch();
            DataBase.executeBatch(obj); 
            
            Test.stopTest();
        }
    }
}