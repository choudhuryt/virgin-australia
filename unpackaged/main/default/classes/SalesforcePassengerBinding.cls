/**
* This class is used to get passenger manifest from webservice
* @author Son Pham
*/
public class SalesforcePassengerBinding {

    /**
    * Default constructor
    */
    public SalesforcePassengerBinding() {}

    /**
    * Gets passenger manifest from webservice
    * @param airlineCode airline code
    * @param flightNumber flight number
    * @param flightDate flight date
    * @return SalesForceHistoricalDetailModel.GetPassengerManifestRSType object
    */
    public SalesForceHistoricalDetailModel.GetPassengerManifestRSType getPassengerDetails(String airlineCode, String flightNumber, String flightDate) {
        SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService
            service = new SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService();
        // Retrieve Apex Callout options from Custom Settings
        Apex_Callouts__c apexCallouts = Apex_Callouts__c.getValues('PassengerDetails');
        if(apexCallouts != null){
            service.endpoint_x = apexCallouts.Endpoint_URL__c;
            service.timeout_x = Integer.valueOf(apexCallouts.Timeout_ms__c);
            service.clientCertName_x = apexCallouts.Certificate__c;
        }
        return service.GetPassengerManifest(airlineCode, flightNumber, flightDate);
    }
}