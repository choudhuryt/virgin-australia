/**
* @description       : Test class for VelocitySnapshotController
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestVelocitySnapshotController {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('FlyPlus', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', null);
        INSERT testContactObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        
        Apex_Callouts__c testApexCalloutObj = TestDataFactory.createTestApexCallouts('VelocityDetails', 'https://services-mssl.virginaustralia.com/service/partner/salesforce/1.0/SalesforceLoyalty', 90000, 'client_mssl_virginaustralia2018_com');
        INSERT testApexCalloutObj;
        
        ALMS_Integration__c testALMSIntegrationObj = TestDataFactory.createTestALMSIntegration('Activate ALMS', false);
        INSERT testALMSIntegrationObj;
        
        Velocity_Status_Match__c testVelocityStatusMatchObj = TestDataFactory.createTestVelocityStatusMatch(testAccountObj.Id, testContractObj.Id, 'No', 'Activated', Date.today(), 'some', 'Velocity Status Upgrade');
        testVelocityStatusMatchObj.RecordTypeId = Schema.SObjectType.Velocity_Status_Match__c.getRecordTypeInfosByDeveloperName().get('Velocity_Upgrade').getRecordTypeId();
        testVelocityStatusMatchObj.Passenger_Email__c = testContactObj.Email;
        testVelocityStatusMatchObj.Passenger_Velocity_Number__c = '9878786756';
        INSERT testVelocityStatusMatchObj;
        
        Sales_Incentive__c testSalesIncentiveObj = TestDataFactory.createTestSalesIncentive(testAccountObj.Id, 'Government', 200, 'Draft');
        INSERT testSalesIncentiveObj;
        testSalesIncentiveObj.Status__c = 'Approved';
        UPDATE testSalesIncentiveObj;
        
        Incentive_Members__c testIncentiveMembersObj = TestDataFactory.createTestIncentiveMembers(testAccountObj.Id, 'testIncenive@gmail.com', 'Test Incentive Members', testSalesIncentiveObj.Id, null, 0);
        INSERT testIncentiveMembersObj;
    }
    
    @isTest
    private static void testVelocitySnapshotController() {
        Velocity_Status_Match__c testVelocityStatusMatchObj = [SELECT Id, RecordTypeId, Passenger_Velocity_Number__c FROM Velocity_Status_Match__c LIMIT 1];
        testVelocityStatusMatchObj.RecordTypeId = Schema.SObjectType.Velocity_Status_Match__c.getRecordTypeInfosByDeveloperName().get('Club_Membership').getRecordTypeId();
        testVelocityStatusMatchObj.Passenger_Velocity_Number__c = null;
        UPDATE testVelocityStatusMatchObj;
        
        Test.startTest();
        PageReference pageRef = Page.VelocitySnapshot;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testVelocityStatusMatchObj);
        VelocitySnapshotController velocitySnapshotControllerObj  = new VelocitySnapshotController(conL);
        Test.stopTest();
    }
    
    @isTest
    private static void testVelocitySnapshotControllerForContact() {
        Contact testContactObj = [SELECT Id FROM Contact LIMIT 1];
        Test.startTest();
        PageReference pageRef = Page.VelocitySnapshotContact;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContactObj);
        VelocitySnapshotController velocitySnapshotControllerObj  = new VelocitySnapshotController(conL);
        Test.stopTest();
    }
    
    @isTest
    private static void testVelocitySnapshotSalesIncentive() {
        Incentive_Members__c testIncentiveMembersObj = [SELECT Id FROM Incentive_Members__c LIMIT 1];
        Test.startTest();
        PageReference pageRef = Page.VelocitySnapshotSalesIncentive;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testIncentiveMembersObj);
        VelocitySnapshotController velocitySnapshotControllerObj  = new VelocitySnapshotController(conL);
        Test.stopTest();
    }
}