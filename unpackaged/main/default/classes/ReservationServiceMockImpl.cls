@isTest
global class ReservationServiceMockImpl implements WebServiceMock{
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       GuestCaseReservationInfo.SalesforceReservationService services = new GuestCaseReservationInfo.SalesforceReservationService();
       GuestCaseReservationInfoModel.ReservationType response_x = services.GetReservationDetailsDemo();
       GuestCaseReservationInfoModel.GetReservationDetailsRSType ReservationDetailsRS = new GuestCaseReservationInfoModel.GetReservationDetailsRSType();
       ReservationDetailsRS.Reservation = response_x;
       response.put('response_x', ReservationDetailsRS); 
   }
}