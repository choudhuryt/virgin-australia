/**
* @description       : Test class for FCRCAUtilities
* @CreatedBy         : Cloudwerx
* @Updated By         : Cloudwerx
**/
@isTest
private class TestTopEcomGuestImpactingDisplayCntrl {
	@isTest
    private static void testTopEcomGuestImpactingDisplayCntrl() {
        TopEcomGuestImpacting__c testTopEcomGuestImpactingObj = TestDataFactory.createTestTopEcomGuestImpacting('TopEcom', '23', '33', true);
        INSERT testTopEcomGuestImpactingObj;
        Test.startTest();
        PageReference pg = new TopEcomGuestImpactingDisplayController().initTopEcom();
        Test.stopTest();
        // Asserts
        System.assertEquals(null, pg);
    }
}