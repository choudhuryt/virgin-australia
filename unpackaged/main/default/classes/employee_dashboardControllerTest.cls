@isTest(seeAllData = true)
private class employee_dashboardControllerTest {
    public static testmethod void testGetDashboards(){
        String dashboardName = 'Baggage, Disability Services, Ground Experience, Check-in, Boarding';
        String userId = UserInfo.getUserId();
        User userRecord = [Select Id, Dashboard_Assigned__c from User where Id=: userId];
        userRecord.Dashboard_Assigned__c = dashboardName;
        update userRecord;
        System.assert(employee_dashboardController.getDashboards() != null);
    }

}