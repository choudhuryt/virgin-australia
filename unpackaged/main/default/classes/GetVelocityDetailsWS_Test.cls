/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com) and Naveen V(naveenkumar.vankadhara@virginaustralia.com)
**Created Date: 19.04.2022
**Description: Test Class for GetVelocityDetailsWS
*/
@isTest
public class GetVelocityDetailsWS_Test {
    //REST Mock
    Class SingleRequestMock implements HttpCalloutMock{
        protected Integer code;
        protected String status;
        protected String bodyAsString;
        protected Blob bodyAsBlob;
        protected Map<String, String> responseHeaders;
        
        public SingleRequestMock(Integer code, String status, String body, Map<String, String> responseHeaders){
            this.code = code;
            this.status = status;
            this.bodyAsBlob = null;
            this.bodyAsString = body;
            this.responseHeaders = responseHeaders;
        }
        public SingleRequestMock(Integer code, String status, Blob body, Map<String, String> responseHeaders){
            this.code = code;
            this.status = status;
            this.bodyAsBlob = body;
            this.bodyAsString = null;
            this.responseHeaders = responseHeaders;
        }
        
        public HTTPResponse respond(HTTPRequest req){
            CalloutException e;
            HTTPResponse resp = new HttpResponse();
            if(code != NULL){
                resp.setStatusCode(code);
            }else{
                e = (CalloutException)CalloutException.class.newInstance();
                e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings.');
                //throw e;
            }
            
            resp.setStatus(status);
            if(bodyAsBlob != null){
                resp.setBodyAsBlob(bodyAsBlob);
            }else if(e != NULL){
                resp.setBody(String.valueOf(e));
            }else{
                resp.setBody(bodyAsString);
            }
            if(responseHeaders != null){
                for(String key: responseHeaders.keySet()){
                    resp.setHeader(key, responseHeaders.get(key));
                }
            }
            
            return resp;
        }        
    }
    
    class UnauthorizedEndpointResponse implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            CalloutException e = (CalloutException)CalloutException.class.newInstance();
            e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings.');
            throw e; 
        }
    }
    //SOAP Mock
    public class WebServiceMockImpl implements WebServiceMock {
        public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
                GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType loyDet = new GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType();
                GuestCaseVelocityInfoModel.MemberInformationType memInfo = new GuestCaseVelocityInfoModel.MemberInformationType();
                List<GuestCaseVelocityInfoModel.ContactTelephoneType> ContPhList = new List<GuestCaseVelocityInfoModel.ContactTelephoneType>(); //= new GuestCaseVelocityInfoModel.ContactTelephoneType[];
				
                loyDet.MemberInformation = memInfo;
               
				memInfo.ContactEmail = new String[]{'abc@test.com'};                
                GuestCaseVelocityInfoModel.ContactTelephoneType telType = new GuestCaseVelocityInfoModel.ContactTelephoneType();
                telType.UnparsedTelephoneNumber = '9595959595';
                ContPhList.add(telType);
                memInfo.ContactTelephone = ContPhList;
                GuestCaseVelocityInfoModel.PersonType perType = new GuestCaseVelocityInfoModel.PersonType();
                perType.Surname = 'XYZ';
                perType.GivenName = 'ABC';
                memInfo.Person = perType;
                GuestCaseVelocityInfoModel.ProfileInformationType profInfoType = new GuestCaseVelocityInfoModel.ProfileInformationType();
                profInfoType.TierLevel = 'Red';
                loyDet.ProfileInformation = profInfoType;
                System.debug('loyDet--'+loyDet);
                response.put('response_x', loyDet); 
            }
    }
    
    @isTest
    static void GetVelocityDetails_SuccessScenario1(){
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "RED", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "MOBILE", "countryCallingCode": "61", "number": "0478704103", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } }, { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Ok',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        Test.startTest();
        Map<String, String> flagMap = GetVelocityDetailsWS.getVelDetFromAlms('0000360491');
        System.debug('flagMap--'+flagMap);
        Test.stopTest();
        
        System.assertEquals(flagMap.get('Tier'), 'RED');
    }
    @isTest
    static void GetVelocityDetails_SuccessScenario2(){
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "RED", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Ok',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        Test.startTest();
        Map<String, String> flagMap = GetVelocityDetailsWS.getVelDetFromAlms('0000360491');
        System.debug('flagMap--'+flagMap);
        Test.stopTest();
        
        System.assertEquals(flagMap.get('Tier'), 'RED');
    }
    @isTest
    static void GetVelocityDetails_FailureScenario1(){
        String body= '{ "code": 37105, "title": "Not Found", "detail": "No matching member found.", "status": 404 }';
        SingleRequestMock fakeResponse = new SingleRequestMock(404,'Not Found',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        Test.startTest();
        Map<String, String> flagMap = GetVelocityDetailsWS.getVelDetFromAlms('0000360491');
        System.debug('flagMap--'+flagMap.get('Status'));
        Test.stopTest();
        
        System.assertEquals(flagMap.get('Status'), 'Failed');
    }
    @isTest
    static void GetVelocityDetails_FailureScenario2(){
        String body= '{ "code": 37105, "title": "Not Found", "description": "No matching member found.", "status": 404 }';
        SingleRequestMock fakeResponse = new SingleRequestMock(404,'Not Found',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        Test.startTest();
        Map<String, String> flagMap = GetVelocityDetailsWS.getVelDetFromAlms('0000360491');
        System.debug('flagMap--'+flagMap.get('Status'));
        Test.stopTest();
        
        System.assertEquals(flagMap.get('Status'), 'Failed');
    }   
}