/*
**Modified By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com) and Naveen V(naveenkumar.vankadhara@virginaustralia.com)
**Modified Date: 22.03.2022
**Description: Modified to enable ALMS
*/
global class TriggerUtility{
    
    public static String almsActivated = System.Label.ALMS_Integration;
    
    @future (callout=true)
    public static void UpdateVelocity(List<String> newCase, List<String> velocityNumber)
    {       
        //Call function from webservice to get velocity details
        List<Velocity__c> velocityList = new List<Velocity__c>(); 
        Velocity__c VelocityItem;
        List<Case> updateCaseList = new List<Case>();
        List<Comms_Log__c> logList = new List<Comms_Log__c>();
        Case GRCase;
        Map<String, String> velocityTypeMap = new Map<String, String> {'R' => 'Red', 'S' => 'Silver','G' => 'Gold', 'P' => 'Platinum', 'V' => 'VIP'};
        //Map<String, String> almsTierMap = new Map<String, String>{'Red'=>'R','Silver'=>'S','Gold'=>'G','Platinum'=>'P','VIP'=>'V'};
        String[] ContactEmail;
        Integer position = 0;
        String caseId;
        Boolean testAlmsActivated = false;
        if(Test.isRunningTest()){
            List<ALMS_Integration__c> flagList = [Select Activate_GET__c from ALMS_Integration__c where Name =:'Activate ALMS'];
            if(flagList.size()>0){ 
                testAlmsActivated = flagList[0].Activate_GET__c; 
            }            
        }
        System.debug(Test.isRunningTest()+'--almsActivated--'+almsActivated);
        if((Test.isRunningTest() && testAlmsActivated == true) || (!Test.isRunningTest() && almsActivated == 'Inactive')){
            GuestCaseVelocityInfo.SalesforceLoyaltyService GuestCaseVelocityInfoClass = new GuestCaseVelocityInfo.SalesforceLoyaltyService();
                Apex_Callouts__c apexCallouts = Apex_Callouts__c.getValues('VelocityDetails');
            if(apexCallouts != null){
                GuestCaseVelocityInfoClass.endpoint_x = apexCallouts.Endpoint_URL__c;
                GuestCaseVelocityInfoClass.timeout_x = Integer.valueOf(apexCallouts.Timeout_ms__c);
                GuestCaseVelocityInfoClass.clientCertName_x = apexCallouts.Certificate__c;
            }
            GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType velocityDetailObject;
            
            GuestCaseVelocityInfoModel.ContactTelephoneType[] ContactPhone;
            GuestCaseVelocityInfoModel.MemberInformationType MemberInformation;
            GuestCaseVelocityInfoModel.ProfileInformationType ProfileInformationType;
            GuestCaseVelocityInfoModel.PersonType PersonType;
            
            for(String item :velocityNumber)            
            {
                if(String.isNotBlank(item)) { 
                    caseId = newCase.get(position);
                    
                    VelocityItem = null;
                    try {
                        velocityDetailObject = GuestCaseVelocityInfoClass.GetLoyaltyDetails(item);
                        MemberInformation = velocityDetailObject.MemberInformation;
                        ContactEmail = MemberInformation.ContactEmail;
                        Contactphone  = MemberInformation.ContactTelephone;                
                        PersonType = MemberInformation.Person;
                        
                        ProfileInformationType = velocityDetailObject.ProfileInformation;
                        
                        VelocityItem = new Velocity__c(); 
                        VelocityItem.Search_Date_Time__c = System.now();
                        VelocityItem.Surname__c = PersonType.Surname;
                        VelocityItem.First_Name__c = PersonType.GivenName;
                        VelocityItem.Velocity_Number__c = item;
                        VelocityItem.Tier__c = ProfileInformationType.TierLevel;
                        
                        if( ContactEmail != null && ContactEmail.size()>0)
                        {
                            VelocityItem.Email__c = ContactEmail[0];
                        }
                        if(ContactPhone !=null && ContactPhone.size()>0)
                        {
                            if(ContactPhone[0].UnparsedTelephoneNumber != null)
                            {
                                VelocityItem.Phone_Number__c = ContactPhone[0].UnparsedTelephoneNumber;
                            }
                        }
                        VelocityItem.Case__c = caseId;
                        
                        TravelBank.SalesforceTravelBankService tv = new TravelBank.SalesforceTravelBankService();
                        Apex_Callouts__c apexCallouts1 = Apex_Callouts__c.getValues('TravelBankDetails');
                        if(apexCallouts1 != null){
                            tv.endpoint_x = apexCallouts1.Endpoint_URL__c;
                            tv.timeout_x = Integer.valueOf(apexCallouts1.Timeout_ms__c);
                            tv.clientCertName_x = apexCallouts.Certificate__c;
                        }
                        if(!Test.isRunningTest()){
                            TravelBankModel.GetTravelBankAccountRSType response_x = tv.GetTravelBankAccount(item);                        
                        	VelocityItem.Travel_Bank_Number__c = response_x.AccountNumber;                        
                        	velocityList.add(VelocityItem);
                        }                        
                    } catch (CalloutException e) {
                        if (VelocityItem!=null && VelocityItem.Case__c!=null){
                            velocityList.add(VelocityItem);
                        }
                        System.debug('CalloutException occurred when getting travelbankdetail service');
                    } finally {
                        // update info back to parent Case
                        if (VelocityItem!=null){
                            List<Case> caseList = [SELECT Id, Velocity_Type__c, Travel_Bank_Account__c
                                                   FROM Case WHERE Id = :caseId];
                            if (caseList.size()>0){
                                GRCase = caseList.get(0);
                                GRCase.Travel_Bank_Account__c = VelocityItem.Travel_Bank_Number__c;
                                GRCase.Velocity_Type__c = velocityTypeMap.get(VelocityItem.Tier__c);
                                updateCaseList.add(GRCase);
                            }
                        }
                    }
                    
                }
                position++;
            }
        }else if(almsActivated == 'Active'){
            for(String item :velocityNumber)            
            {
                if(String.isNotBlank(item)) { 
                    caseId = newCase.get(position);
                    String tier;
                    String firstName;
                    String lastName;
                    String email;
                    String phoneNumber;
                    String status;
                    String errorDetails;
                    Integer attempt;
                    Map<String, String> velDetailsMap = GetVelocityDetailsWS.getVelDetFromAlms(item); System.debug('velDetailsMap--'+velDetailsMap);
                    if(velDetailsMap.size()>0){
                        VelocityItem = new Velocity__c(); 
                        VelocityItem.Search_Date_Time__c = System.now();
                        VelocityItem.Case__c = caseId;
                        VelocityItem.Velocity_Number__c = item;
                        
                        if(velDetailsMap.containsKey('Tier') && velDetailsMap.get('Tier') != NULL){
                            tier = velDetailsMap.get('Tier');
                            VelocityItem.Tier__c = tier.substring(0, 1);                            
                        }if(velDetailsMap.containsKey('FirstName') && velDetailsMap.get('FirstName') != NULL){
                            VelocityItem.First_Name__c = velDetailsMap.get('FirstName');
                        }if(velDetailsMap.containsKey('LastName') && velDetailsMap.get('LastName') != NULL){
                            VelocityItem.Surname__c = velDetailsMap.get('LastName');
                        }if(velDetailsMap.containsKey('Email') && velDetailsMap.get('Email') != NULL){
                            VelocityItem.Email__c = velDetailsMap.get('Email');
                        }if(velDetailsMap.containsKey('PhoneNumber') && velDetailsMap.get('PhoneNumber') != NULL){
                            VelocityItem.Phone_Number__c = velDetailsMap.get('PhoneNumber');
                        }if(velDetailsMap.containsKey('PointBalance') && velDetailsMap.get('PointBalance') != NULL){
                            VelocityItem.Point_Balance__c = Integer.valueOf(velDetailsMap.get('PointBalance'));
                        }if(velDetailsMap.containsKey('Attempt') && velDetailsMap.get('Attempt') != NULL){
                            attempt = Integer.valueOf(velDetailsMap.get('Attempt'));
                        }if(velDetailsMap.containsKey('Status') && velDetailsMap.get('Status') != NULL){
                            status = velDetailsMap.get('Status');
                        }if(velDetailsMap.containsKey('ErrorDetails') && velDetailsMap.get('ErrorDetails') != NULL){
                            errorDetails = velDetailsMap.get('ErrorDetails');
                        }                      
                        velocityList.add(VelocityItem);
                        
                        
                        Case updateCase = new Case();
                        updateCase.Id = caseId;
                        if(tier != NULL){
                            updateCase.Velocity_Type__c = tier;
                        }else{
                            updateCase.Velocity_Type__c = NULL;
                        }
                        //Travel_Bank_Account__c
                        updateCaseList.add(updateCase);
                           
                                                
                        Comms_Log__c lg = new Comms_Log__c();
                        lg.Type__c = 'Case';
                        lg.Case__c = caseId;
                        lg.Attempts__c = attempt;
                        lg.Status__c = status;
                        lg.Request_Type__c = 'GET';
                        lg.Velocity_Number__c = item;
                        if(errorDetails != NULL){
                            lg.Error__c = errorDetails;
                        }
                        logList.add(lg);
                    }                                                           
                }
                position++;
            }
        }
        
		System.debug('velocityList--'+velocityList+'--updateCaseList--'+updateCaseList+'--logList--'+logList);        
        try {
            if (velocityList.size() > 0)
            {
                upsert velocityList;
            }
        } catch (Exception e){}
        try {
            if (updateCaseList.size() > 0) {
                update updateCaseList;
            }
        } catch (Exception e){}
        
        if(logList.size()>0){
            insert logList;
        }
    }
}