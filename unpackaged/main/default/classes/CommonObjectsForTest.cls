/** * File Name      : CommonObjectsForTest
* Description        : This is a utilities class that is taking commonly used Sales force objects
					   or tables if at database level and for testing purposes is allocating them 
					   to memory for use in test classes. 
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Andy Burgess
* * Date             : 30th May 2012
* * Technical Task ID: Internal requirment
* * Notes            : Test classes do not insert, update or delete records in the databases.
					   Because of this you need to create the object(s) as a SOQL query would return.
					   you then need to allocate them to memory for the test methods to reference,
					   The howManyNeedToBeCreated is for testing bulkified triggers 0 needs to be passed as a defaulr
					   if only a single object is required	
					   Any new method added needs a test method adding into TestCommonObjectsForTest 
					   If this is not done thenm it will loose code coverage
* Modification Log ==================================================​============= 
Ver Date Author Modification --- ---- ------ -------------
*/

public class CommonObjectsForTest {


/**
 * Returns an Account object
 * <p>
 * This method always returns immediately 
 *
 * @param  --No params
 * @return      Account 
 * 
 */
public  Account CreateAccountObject(Integer howManyNeedToBeCreated){
	
	    ///////////////////// 
        // Create an Account a1
        /////////////////////
        Account a1 = new Account(); 
        if (howManyNeedToBeCreated == 0 ){
             
         a1.Target_Markets__c = 'Thailand';
         a1.Estimated_Spend__c = '1m+';
         a1.Travel_Policy__c = 'Flexi Only';
         a1.Booked_Via__c = 'TMC';
         a1.Name = 'TEST177';
         a1.BillingCountry = 'Australia';
         a1.BillingStreet		= '1 Main St.';
  		 a1.BillingState		= 'VA';
  		 a1.BillingPostalCode	= '12345';
  		 a1.BillingCountry 		= 'USA';
  		 a1.BillingCity			= 'Anytown';
  		 a1.AnnualRevenue		= 10000;
  		 a1.GDS_User__c			=true;
  		 a1.Velocity_Gold_Status_Match_Available__c =10;
  		 a1.Velocity_Pilot_Gold_Available__c = 10;
  		 a1.Velocity_Platinum_Status_Match_Available__c =10;
  		 a1.Estimated_Domestic_Revenue__c =1;
        }
        
        else{
        a1.Target_Markets__c = 'Thailand';
         a1.Estimated_Spend__c = '1m+';
         a1.Travel_Policy__c = 'Flexi Only';
         a1.Booked_Via__c = 'TMC';
         a1.Name = 'TEST177' + '' + howManyNeedToBeCreated;
         a1.BillingCountry = '';
         a1.BillingStreet		= '1 Main St.';
  		 a1.BillingState		= 'VA';
  		 a1.BillingPostalCode	= '12345';
  		 a1.BillingCountry 		= 'USA';
  		 a1.BillingCity			= 'Anytown';
  		 a1.AnnualRevenue		= 10000;
  		 a1.GDS_User__c			=true;
  		 a1.Velocity_Gold_Status_Match_Available__c =10;
  		 a1.Velocity_Pilot_Gold_Available__c = 10;
  		 a1.Velocity_Platinum_Status_Match_Available__c =10;
  		 a1.Estimated_Domestic_Revenue__c =1;
        }
         
	return a1;
}
 
 		 //////////////////
         //Create a New Contact newContact
         //////////////////
         
/**
 * Returns a new Contact Object
 * <p>
 * This method always returns immediately 
 *
 * @param  --No params
 * @return      Contact
 * Notes: how many is there in case you want to chech a bulkified trigger.
 */

	public  Contact CreateNewContact(Integer howManyNeedToBeCreated){
	
	Contact newContact = new Contact();
	
	if (howManyNeedToBeCreated <> 0 ){
	newContact.FirstName ='Joe' + ''+ howManyNeedToBeCreated;
	newContact.LastName ='Bloogs' + ''+ howManyNeedToBeCreated;
		
	}else{
		newContact.FirstName ='Joe';
		newContact.LastName ='Bloogs';
	}
			
	
		return newContact;
	} 

/**
 * Returns a new User Object
 * <p>
 * This method always returns immediately 
 *
 * @param  --No params
 * @return      User
 * 
 */

	public  User CreateNewUser(Integer howManyNeedToBeCreated){
		
		 //////////////////
         //Create a User ui
         //////////////////
        User u1 = new User(); 
        if (howManyNeedToBeCreated <> 0){
        	
		u1.Alias = 'John' +'' +howManyNeedToBeCreated;
        u1.LastName = 'Marty' +'' +howManyNeedToBeCreated;
        u1.Email = 'John@virginaustralia.com'+'' +howManyNeedToBeCreated;
        u1.Username = 'John' + '' +howManyNeedToBeCreated + '@virginaustralia.com';
        u1.CommunityNickname = 'John'+'' +howManyNeedToBeCreated;
        u1.TimeZoneSidKey ='Australia/Brisbane';
        u1.LocaleSidKey ='en_AU';
        u1.ProfileId ='00e90000000N66CAAS';
        u1.LanguageLocaleKey ='en_US';
        u1.EmailEncodingKey ='ISO-8859-1';       
        	
       
        	
        }else{ 
        
   		u1.Alias = 'Joe';
        u1.LastName = 'Smith';
        u1.Email = 'joe@test.com';
        u1.Username = 'joe@virginaustralia.com';
        u1.CommunityNickname = 'Joe';
        u1.TimeZoneSidKey ='Australia/Brisbane';
        u1.LocaleSidKey ='en_AU';
        u1.ProfileId ='00e90000000mpRqAAI';
        u1.LanguageLocaleKey ='en_US';
        u1.EmailEncodingKey ='ISO-8859-1';
   		
        }           
        
        
        return u1;
	}
	
/**
 * Returns a new Lead Object
 * <p>
 * This method always returns immediately 
 *
 * @param  --No params
 * @return   Lead   
 * 
 */

	public  Lead CreateNewlead(Integer howManyNeedToBeCreated){
	
		/////////////////////
        // Create a Lead l1
        /////////////////////
        Lead l1 = new Lead();
        if (howManyNeedToBeCreated <> 0){
        	
        // l1.OwnerId = 'Jjoe@virginaustralia.com' + '' +howManyNeedToBeCreated ;     
         l1.LastName = 'test500' + '' + howManyNeedToBeCreated; 
         l1.Company = 'TEST' + '' + howManyNeedToBeCreated; 
         l1.Lead_Type__c = 'Corporate';
         l1.Target_Markets__c = 'Thailand';
         l1.Estimated_Spend__c = '1m+';
         l1.Travel_Policy__c = 'Flexi Only';
         l1.Booked_Via__c = 'TMC';
         l1.Status = 'New';
         l1.Status = 'Qualified';
//MOD-BEGIN CILAG INS CASE# 129600 09.23.16
         l1.Street  = 'BLI';
         l1.City    = 'MAL';
         l1.State   = 'BUL';
         l1.PostalCode = '3000';
         l1.Billing_Country_Code__c = 'AU';
//MOD-END CILAG INS CASE# 129600 09.23.16
         l1.Country = 'Australia';
   
        	
        }else{
         
         //l1.OwnerId = 'Jjoe@virginaustralia.com';     
         l1.LastName = 'test500'; 
         l1.Company = 'TEST'; 
         l1.Lead_Type__c = 'Corporate';
         l1.Target_Markets__c = 'Thailand';
         l1.Estimated_Spend__c = '1m+';
         l1.Travel_Policy__c = 'Flexi Only';
         l1.Booked_Via__c = 'TMC';
         l1.Status = 'New';
         l1.Status = 'Qualified';
//MOD-BEGIN CILAG INS CASE# 129600 09.23.16
         l1.Street  = 'BLI';
         l1.City    = 'MAL';
         l1.State   = 'BUL';
         l1.PostalCode = '3000';
         l1.Billing_Country_Code__c = 'AU';
//MOD-END CILAG INS CASE# 129600 09.23.16
         l1.Country = 'Australia';
        }
         
			return l1;
	}
	
/**
 * Returns a new Opportunity Object
 * <p>
 * This method always returns immediately 
 *
 * @param  --No params
 * @return      Opportunity
 * 
 */

	public  Opportunity CreateNewOpportunity(Integer howManyNeedToBeCreated){
	 	
	 	/////////////////////
	 	// Create opportunity
        /////////////////////
        
        Opportunity opp1 = new Opportunity();
        
        if(howManyNeedToBeCreated <> 0 ){
        
        opp1.Name = 'testOpp1';
        opp1.StageName = 'Sales Opportunity Analysis';
        opp1.recordtypeid =  '01290000000so61';
        opp1.Type = 'Acquisition'   ; 
            //opp1.StageName = 'Open';
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        
        	
        }else{
        	
        opp1.Name = 'testOpp1' + ''+ howManyNeedToBeCreated;
        opp1.StageName = 'Sales Opportunity Analysis' + ''+ howManyNeedToBeCreated;
        opp1.recordtypeid =  '01290000000so61';
        opp1.Type = 'Acquisition'   ;     
        //opp1.StageName = 'Open';
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        
        	
        }
        
        
        
        return opp1;
	}

	public Contract CreateOldStandardContract (Integer howManyNeedToBeCreated){
		
		Contract contract = new Contract();
		if(howManyNeedToBeCreated == 0){
		
		contract.Status = 'Draft';
        // contract.AccountId = account.id;
        contract.ContractTerm = 12;
        contract.StartDate = Date.newInstance(2011,01,01);
        contract.Contract_Title__c = 'TEST';
        contract.Contract_Reference__c = 'TEST';
		}else{
			contract.Status = 'Draft';
        // contract.AccountId = account.id;
        contract.ContractTerm = 12;
        contract.StartDate = Date.newInstance(2011,01,01);
        contract.Contract_Title__c = 'TEST' + '' + howManyNeedToBeCreated;
        contract.Contract_Reference__c = 'TEST' + '' + howManyNeedToBeCreated;
		}	
			
		return contract;
	}
	
	public SIP_Header__c CreateNewSIPHeader (Integer howmanyNeedToBeCreated){
		SIP_Header__c header = new SIP_Header__c();
		if (howmanyNeedToBeCreated == 0){
		
		header.Description__c ='Test';
		header.Est_Current_Full_Year_Revenue__c = 1000;
		header.Full_Last_Year_Actual_Revenue__c = 1200;
		
		
		}else{
		header.Description__c ='Test' + '' + howmanyNeedToBeCreated;
		header.Est_Current_Full_Year_Revenue__c = 1000;
		header.Full_Last_Year_Actual_Revenue__c = 1200;
			
			
		}
		return header; 
	}
	
	//contract id needs to be created outsdide this method for context.
	public SIP_Rebate__c CreatenewSIPRebate(Integer howmanyNeedToBeCreated){
		SIP_Rebate__c rebate1 = new SIP_Rebate__c();
		if (howmanyNeedToBeCreated == 0){
        rebate1.Lower_Percent__c = 10;
        rebate1.Upper_Percent__c = 20;
        rebate1.SIP_Percent__c = 1;
        //rebate1.Contract_Growth__c = contractOld.id; 	
			
		}else{
		rebate1.Lower_Percent__c = 10;
        rebate1.Upper_Percent__c = 20;
        rebate1.SIP_Percent__c = 1;
        //rebate1.Contract_Growth__c = contractOld.id;
		}
		return rebate1;
	}
	
	public IATA_Number__c CreateNewIATANumber (Integer howManyNeedToBeCreated){
		
		//Contract number needs to be created outside of this method
		IATA_Number__c iata_num1 = new IATA_Number__c();
		if (howManyNeedToBeCreated == 0){
        //iata_num1.Name = 'iatanum';
        iata_num1.IATA_Number__c = '2461' + howManyNeedToBeCreated ;
       // iata_num1.Contract__c = contractOld.id;
         }else{
       // iata_num1.Name = 'iatanum' + '' + howManyNeedToBeCreated;
        iata_num1.IATA_Number__c = '2461' + '' + howManyNeedToBeCreated;
         }
		return iata_num1;
	} 
	
	public Velocity_Status_Match__c CreateVelocityStatusmatch (Integer howManyNeedToBeCreated){
		//Need to assign account number and contract number outside of this method before the insert of the object,
		//This method does not have access to the account or contract method.
		
		
		Velocity_Status_Match__c VSM = new Velocity_Status_Match__c();
		Date objDate = Date.today();
		if (howManyNeedToBeCreated <> 0){
			
			VSM.Approval_Status__c = 'Draft';
			VSM.Date_Requested__c = objDate;
			VSM.Passenger_Name__c = 'TEST' + '' + howmanyNeedToBeCreated;
			VSM.Position_in_Company__c = 'TESTER' + '' + howmanyNeedToBeCreated;
			VSM.Passenger_Velocity_Number__c ='123456789' + '' + howmanyNeedToBeCreated;
			VSM.Passenger_Email__c ='test@test.com' + '' + howmanyNeedToBeCreated;
			VSM.Delivery_To__c ='Member';
			VSM.Status_Match_or_Upgrade__c = 'Platinum';
			VSM.Within_Contract__c ='Yes';
			
		}else{
			
			VSM.Approval_Status__c = 'Draft';
			VSM.Date_Requested__c = objDate;
			VSM.Passenger_Name__c = 'TEST';
			VSM.Position_in_Company__c = 'TESTER';
			VSM.Passenger_Velocity_Number__c ='1234567890';
			VSM.Passenger_Email__c ='test@test.com';
			VSM.Delivery_To__c ='Member';
			VSM.Status_Match_or_Upgrade__c = 'Platinum';
			VSM.Within_Contract__c ='Yes';
			
		}
		
		
		return VSM;
		
	}
	
	public Marketing_Spend_Down__c CreatemarketingSpendDown (Integer howManyNeedToBeCreated ){
		//Need to assign account number and contract number outside of this method before the insert of the object,
		//This method does not have access to the account or contract method.
		Marketing_Spend_Down__c marketingSpendDown = new Marketing_Spend_Down__c();
		
		if (howManyNeedToBeCreated <> 0){
			
			marketingSpendDown.Approval_Status__c = 'Draft';
			marketingSpendDown.Deduction_Category__c = 'Entertainment';
			marketingSpendDown.Channel_Type__c = 'Retail';
			marketingSpendDown.Amount_to_be_Deducted__c =1000;
			marketingSpendDown.Campaign_Name__c ='TEST' + ' ' + howManyNeedToBeCreated;
			
		}else{
			
			marketingSpendDown.Approval_Status__c = 'Draft';
			marketingSpendDown.Deduction_Category__c = 'Entertainment';
			marketingSpendDown.Channel_Type__c = 'Retail';
			marketingSpendDown.Amount_to_be_Deducted__c =1000;
			marketingSpendDown.Campaign_Name__c ='TEST';
		}
		return marketingSpendDown;
	}
	
	Public PCC__c CreatePCC (Integer howManyNeedToBeCreated){
		
		PCC__c PCC1 = new PCC__c();
		if (howManyNeedToBeCreated == 0){
        PCC1.Office_Store_Name__c = 'AA';
        PCC1.Name = 'AA';
        //PCC1.Contract__c = contractOld.id; needs to be created outside this method 
		}else {
		PCC1.Office_Store_Name__c = 'AA' + '' + howManyNeedToBeCreated;
        PCC1.Name = 'AA' + '' + howManyNeedToBeCreated;
		}
		return PCC1;
	}
	
	public International_City_Pairs__c CreateNewInternationalPairs(Integer howManyNeedToBeCreated){
		International_City_Pairs__c ICC1 = new International_City_Pairs__c();
		
		if (howManyNeedToBeCreated == 0){
        ICC1.Region__c = 'Asia';
        ICC1.City__c = 'SIN';
        //ICC1.Contract__c = contractOld.id; Needs to be created out of this method for context
		}else { 
			ICC1.Region__c = 'Asia' + '' + howManyNeedToBeCreated;
            ICC1.City__c = 'SIN' + '' + howManyNeedToBeCreated;
			
		}
		return ICC1;
	}
	
	public PRISM_Key_Routes__c CreatePrismKeyRoutes(Integer howManyNeedToBeCreated){
		
		PRISM_Key_Routes__c pkr1 = new PRISM_Key_Routes__c();
		if (howManyNeedToBeCreated == 0){
         
         pkr1.Destination__c = 'SYD';
         pkr1.Origin__c = 'BNE';
       //  pkr1.Account__c = newAccount.id; needs to be created outside of this method
		}else{
		 pkr1.Destination__c = 'SYD';
         pkr1.Origin__c = 'BNE';
		}
		return pkr1;
	}
	
	
}