/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class UpdateInactiveAccountTestClass {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
         Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);
    account.Velocity_Gold_Status_Match_Available__c =10;
    account.Velocity_Pilot_Gold_Available__c = 10;
    account.Velocity_Platinum_Status_Match_Available__c =10;
    account.Accelerate_Batch_Update__c = false;
    account.RecordTypeId ='01290000000tSR0';
    account.Business_Number__c='090808232434';
    account.Sales_Matrix_Owner__c = 'Accelerate';
	account.OwnerId = '00590000000LbNz';
	account.Account_Owner__c = '00590000000LbNz';
	account.Velocity_Pilot_Gold_Available__c = 2;
	account.Sales_Support_Group__c = '00590000000LbNz'; 
    account.Billing_Country_Code__c ='AU';
    account.Market_Segment__c = 'Accelerate';	
	account.Lounge__c =true;
    account.Pilot_Gold_Email__c ='test@test.com';
    account.Pilot_Gold_Email_Second_Nominee__c = 'test@test.com';
    account.Lounge_Email__c = 'test123456789@test.com';
    account.Account_Lifecycle__c ='Offer'; 
   
    insert account;   
        
	Contact newContact = new Contact(); 
	newContact.FirstName ='Andy';
	newContact.LastName ='C';
	//newContact.Name ='TEST456';
	newContact.AccountId = account.id;
	
	
	//contact.Email ='Andy.B@TEST456.com.au' 
	Date dateobj = Date.today();
	
	insert newContact;
    
    Contract contract = New Contract();
    contract.name ='PUTCONTRACTNAMEHERE';
    contract.AccountId =account.id;
    contract.Status ='Draft';
    contract.RecordTypeId ='012900000007oTOAAY';
    
    insert contract;
     
    Contract contract1 = New Contract();
   
    contract1 =(Contract) [select id from Contract where Accountid=:account.id];
    contract1.status ='Activated';
    update contract1;
     
    Account accountToUpdate = new Account();
    accountToUpdate = [select id,Account_Lifecycle__c from Account where id =:account.id];
    //accountToUpdate.Account_Lifecycle__c = 'Inactive';
    update accountToUpdate;   
    }
}