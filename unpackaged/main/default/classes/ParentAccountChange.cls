public class ParentAccountChange
{
@InvocableMethod
    public static void ParentAccountUpdate(List<Id> Caseids )
    {
     Date dadv ;
     id Relatedaccountid ;
     List<Case> Caselist = [SELECT Id,CaseNumber,AccountId,NEW_Parent_Account1__c,Change_From_Date1__c  FROM Case where id =:CaseIds and NEW_Parent_Account1__c <> null ];  
    
      List<Account_Data_Validity__c> advList = new List<Account_Data_Validity__c>();
        
        
       if(Caselist.size() > 0 )
     {
          List<Account> accountlist =  [Select Id      from Account where id =:Caselist[0].AccountId    ]; 
         
         Relatedaccountid = accountlist[0].id   ;
         
          advList = [SELECT Account_Owner__c,Account_Record_Type__c,From_Account__c,From_Date__c,Id,
                        Market_Segment__c,Name,Record_Type__c,Sales_Matrix_Owner__c,To_Date__c,
                        Parent_Account__c,Account_Record_Type_Picklist__c
                        FROM
                        Account_Data_Validity__c   where   From_Account__c =:Relatedaccountid 
                        and  To_Date__c = null ];
         if(advList.size()>0)
         {  
              if(  advList[0].From_Date__c >= Caselist[0].Change_From_Date1__c.toStartofMonth().addDays(-1))
              {
               dadv = Caselist[0].Change_From_Date1__c;
               dadv = dadv.addMonths(1).toStartofMonth().addDays(-1);   
              }else   
              {dadv = Caselist[0].Change_From_Date1__c;
               dadv = dadv.toStartofMonth().addDays(-1);
              }
               advList[0].To_Date__c = dadv ;
              try 
              {
                 update advList;
               }catch(DmlException e) 
              {
                System.debug('The following exception has occurred: ' + e.getMessage());
             }
             Account_Data_Validity__c advNew = new Account_Data_Validity__c();            
             advNew.Account_Record_Type__c = advList[0].Account_Record_Type__c ;
             advNew.From_Account__c =advList[0].From_Account__c;
             advNew.From_Date__c = dadv.AddDays(1);
             advNew.Account_Owner__c = advList[0].Account_Owner__c;
             advNew.Market_Segment__c = advList[0].Market_Segment__c ;
             advNew.Sales_Matrix_Owner__c = advList[0].Sales_Matrix_Owner__c ;  
             if( Caselist[0].NEW_Parent_Account1__c <> null )
             {
                 advNew.Parent_Account__c = Caselist[0].NEW_Parent_Account1__c ;
             }else
             {
                 advNew.Parent_Account__c =  advList[0].Parent_Account__c ;
             }
              advNew.Account_Record_Type__c = advList[0].Account_Record_Type__c;
              advNew.Account_Record_Type_Picklist__c = advList[0].Account_Record_Type_Picklist__c ;
             
             try 
            {    
            insert advNew;
             }catch(DmlException e) 
           {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }
             
           List<ContentNote> nte = new List<ContentNote>();
           List<ContentDocumentLink> lnk = new List<ContentDocumentLink>();
           ContentNote cnt = new ContentNote();
           cnt.Content = Blob.valueof(Caselist[0].CaseNumber);
           cnt.Title = 'Case Number';
           nte.add(cnt);
             
           if(nte.size()>0)
           {
                           insert nte;
           }
              
          ContentDocumentLink clnk = new ContentDocumentLink();
          clnk.LinkedEntityId = advNew.Id;
          clnk.ContentDocumentId = nte[0].Id;
          clnk.ShareType = 'I';
          lnk.add(clnk);   

           if(nte.size()>0){
               if(!Test.isRunningTest()) { insert lnk;}
           }
                
         }

         
         
     }
    }
}