/**
* @description       : Test class for SendCasetoAlmsTrigger
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestSendCasetoAlmsTrigger {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
    }
    
    @isTest
    private static void testIsInsertSendCasetoAlmsTrigger() {
        Account testAccountObj = [SELECT Id, Name FROM Account LIMIT 1];
        Test.startTest();
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '0000360491', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        INSERT testCaseObj;
		Test.stopTest();  
        //Asserts
        system.assertEquals('0000360491', testCaseObj.Velocity_Number__c);
    }
    
    @isTest
    private static void testIsUpdateSendCasetoAlmsTrigger() {
        Account testAccountObj = [SELECT Id, Name FROM Account LIMIT 1];
        
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '0000360491', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        INSERT testCaseObj;
        
        EmailMessage testEmailMessageObj = TestDataFactory.createTestEmailMessage('Test Body', 'Test Subject', testCaseObj.Id);
        INSERT testEmailMessageObj;

        Test.startTest();
        testCaseObj.Velocity_Number__c = '0000360500';
        UPDATE testCaseObj;
		Test.stopTest();  
        //Asserts
        system.assertEquals('0000360500', testCaseObj.Velocity_Number__c);
    }
}