/** * File Name      : Accelerate Contracts Batch
 * Description        : This Apex Class is a batch class that runs every day to Check an active contract 
					   that has an accelerate record ad that has a VA revenue record that has expired, 
					   so an end date less or = to today, 
					   the Code then creates a new VA record taking the previous spend values and inserts a 
					   new one updates the expired record to invalid status.
 * * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
 * * @author          : Andy Burgess
 * * Date             : 16th May 2012
 * * Technical Task ID: 126
 * * Notes            : Batch Classes require the start, execute and finish methods and must implement Database.Batchable
                     : To run as a schedule task they also need a Schedule Class that implements Schedulable, this class has
                       a scheduledClass called ScheduleAcceleratorBatch.cls. 
 * Modification Log ==================================================​============= 
Ver Date Author Modification --- ---- ------ -------------
 * */ 

global class AccelerateContractBatch implements Database.Batchable<SObject>, Database.Stateful{

	global String gstrQuery = 'SELECT  ContractNumber,  Status, Type__c,RecordTypeId,Record_Type__c FROM Contract where Status = \'Activated\' and Record_Type__c in (\'Accelerate POS/Rebate\', \'NZ Accelerate POS/Rebate\')';
	global Integer wereThereAnyInserErrors;
	global Integer howManyrecordsUpdated;

	global AccelerateContractBatch(){
		wereThereAnyInserErrors = 0;
		howManyrecordsUpdated = 0;
	}

	//Loading and running the query string
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(gstrQuery);
	}   

	//Running The execute Method
	global void execute(Database.BatchableContext BC, 
			List<sObject> scope){

		List<Marketing_Spend_Down__c> msdList = [select id From Marketing_Spend_Down__c];
		if(msdList.size() > 0){
			try {
				update msdList;
			}catch(Exception ex){
				Messaging.SingleEmailMessage mailErrorvsm = new Messaging.SingleEmailMessage();
				mailErrorvsm.setToAddresses(new String[] {'andrew.burgess@virginaustralia.com','salesforce.admin@virginaustralia.com'});
				mailErrorvsm.setReplyTo('batch@VirginAustralia.com');
				mailErrorvsm.setSenderDisplayName('Batch Processing');
				mailErrorvsm.setSubject('Batch Process Failed to update Marketing Spend Down');
				mailErrorvsm.setPlainTextBody('Batch Process Failed to Process Marketing Spend Down, Exception is ' + ex);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailErrorvsm });
			}
		}





		// **********  Add this to a separate class  *************************


		/*

		// Method determines if VSM's are in contract or not, New ones have a work flow but old ones need to be
		//touched to activate the flow  
		List<Velocity_Status_Match__c> vsmList = [select id,Within_Contract__c From Velocity_Status_Match__c where Within_Contract__c ='Yes' and IsAccountContract__c =True ];
		//Velocity_Status_Match__c VSM = new Velocity_Status_Match__c();
		if(vsmList.size() > 0){
			try{
				update vsmList;
			}catch(Exception ex){
				Messaging.SingleEmailMessage mailErrorvsm = new Messaging.SingleEmailMessage();
				mailErrorvsm.setToAddresses(new String[] {'andrew.burgess@virginaustralia.com','salesforce.admin@virginaustralia.com'});
				mailErrorvsm.setReplyTo('batch@VirginAustralia.com');
				mailErrorvsm.setSenderDisplayName('Batch Processing');
				mailErrorvsm.setSubject('Batch Process Failed to update VSMS');
				mailErrorvsm.setPlainTextBody('Batch Process Failed to Process VSMs, Exception is ' + ex);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailErrorvsm });
			}
		}

		 */

		List<Contract> contractList = new List <Contract>();	
		for (Sobject s : scope){
			Contract c = (Contract)s;
			contractList.add(c);

		}  



		//Checking that the revenue record is not expired by looking at todays date and if the record is active and putting all records found into a list object!!                               
		//List <VA_Revenue__c> revenue = new List<VA_Revenue__c>();
		List <VA_Revenue__c> revenue = new List<VA_Revenue__c>(); 
		List <VA_Revenue__c> newRevenueList = new List<VA_Revenue__c>();
		List <VA_Revenue__c> expiredRevenueList = new List<VA_Revenue__c>(); 
		List <String> emailContractNumbers = new list <String>() ;

		revenue = [select Account__c, Actual_Booked_Domestic_Spend__c, Actual_Booked_International_Spend__c, Actual_Booked_Total_Spend__c, 
		           Actual_Flown_Domestic_Spend__c, Actual_Flown_International_Spend__c, Actual_Flown_Total_Spend__c, Contract__c, 
		           Converted_Domestic_Spend__c, Converted_International_Spend__c, Converted_Spend__c, CreatedById, CreatedDate, 
		           Date_Range__c, IsDeleted, End_Date__c, Estimated_Domestic_Spend__c, Estimated_International_Spend__c, 
		           Estimated_Total_Spend__c, LastModifiedById, LastModifiedDate, Rebate_Achieved__c, Rebate_Paid__c, Id, Start_Date__c, 
		           SystemModstamp, Name, Actual_Booked_Domestic_Spend_PY__c ,Actual_Booked_International_Spend_PY__c,
		           Actual_Flown_Domestic_Spend_PY__c,Actual_Flown_International_Spend_PY__c 
		           from VA_Revenue__c where Active__c = true and End_Date__c <= today and Contract__c IN: contractList];

		for (Integer i = 0; i < revenue.size(); i++) 
		{

			VA_Revenue__c expiredVARecord = new VA_Revenue__c();
			expiredVARecord = (VA_Revenue__c) revenue.get(i);
			VA_Revenue__c newVARecord = new VA_Revenue__c();
			Date obj = Date.today();
			//assign values from expired opbject to newVARecord object and insert and thats this class done
			// newVARecord = expiredVARecord;
			newVARecord.Account__c = expiredVARecord.Account__c;
			newVARecord.Contract__c = expiredVARecord.Contract__c;

			newVARecord.Estimated_Domestic_Spend__c = expiredVARecord.Estimated_Domestic_Spend__c;
			newVARecord.Estimated_International_Spend__c = expiredVARecord.Estimated_International_Spend__c;
			newVARecord.Actual_Booked_Domestic_Spend__c = expiredVARecord.Actual_Booked_Domestic_Spend__c;
			newVARecord.Actual_Booked_International_Spend__c = expiredVARecord.Actual_Booked_International_Spend__c;
			newVARecord.Actual_Flown_Domestic_Spend__c = expiredVARecord.Actual_Flown_Domestic_Spend__c;
			newVARecord.Actual_Flown_International_Spend__c = expiredVARecord.Actual_Flown_International_Spend__c;
			newVARecord.Actual_Booked_Domestic_Spend_PY__c = expiredVARecord.Actual_Booked_Domestic_Spend_PY__c;
			newVARecord.Actual_Booked_International_Spend_PY__c = expiredVARecord.Actual_Booked_International_Spend_PY__c;
			newVARecord.Actual_Flown_Domestic_Spend_PY__c = expiredVARecord.Actual_Flown_Domestic_Spend_PY__c;
			newVARecord.Actual_Flown_International_Spend_PY__c = expiredVARecord.Actual_Flown_International_Spend_PY__c;
			newVARecord.Start_Date__c = obj.addDays(1);
			newVARecord.End_Date__c = obj.addDays(365);
			newVARecord.Active__c = true;
			newRevenueList.add(newVARecord);
			expiredVARecord.Active__c = false;
			emailContractNumbers.add(newVARecord.Contract__c = expiredVARecord.Contract__c);

			for (Integer x = 0; x< contractList.size(); x++ )
			{
				Contract c = contractList.get(x);
				if (c.id == expiredVARecord.Contract__c){
					emailContractNumbers.add(c.ContractNumber);
				}
			}
			expiredRevenueList.add(expiredVARecord);
		}

		try {

			// Inserting new record	
			insert newRevenueList;
			howManyrecordsUpdated = howManyrecordsUpdated + newRevenueList.size();
			// Updating new Record	
			update expiredRevenueList;	

		}catch(Exception e){ 

			//Sending an Email if there are any errors when trying to update and insert the new records and expired records in the VA_Revenue__c table.		
			Messaging.SingleEmailMessage mailError = new Messaging.SingleEmailMessage();
			mailError.setToAddresses(new String[] {'andrew.burgess@virginaustralia.com','salesforce.admin@virginaustralia.com'});
			mailError.setReplyTo('batch@VirginAustralia.com');
			mailError.setSenderDisplayName('Batch Processing');
			mailError.setSubject('Batch Process Failed to insert VA Record');
			mailError.setPlainTextBody('Batch Process Failed to insert VA Record, Exception is ' + e + 'Contract number to check are  :' + emailContractNumbers );
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailError });
			wereThereAnyInserErrors++;

		}	

	}

	//Finishing the batch  (Or though this is not executiong code this methos needs to exsist for the batch class to run!!)  
	global void finish(Database.BatchableContext BC){
		//Sending an email on Completion of the batch class with how many records were updated and if there were any errors.
		String bodyOfEmailSuccess ='The Accelerate Batch class has completed and it has updated  ' + String.valueOf(howManyrecordsUpdated) + ' Record(s). There were ';
		String bodyOfEmailFailuire= String.valueOf(wereThereAnyInserErrors + ' Record(s) with Errors!');

		Messaging.SingleEmailMessage mailfinish = new Messaging.SingleEmailMessage();
		mailfinish.setToAddresses(new String[] {'steve.kerr2@virginaustralia.com','salesforce.admin@virginaustralia.com'});
		//mailfinish.setToAddresses(new String[] {'steve.kerr2@virginaustralia.com'});
		mailfinish.setReplyTo('batch@VirginAustralia.com');
		mailfinish.setSenderDisplayName('Batch Processing');
		mailfinish.setSubject('Accelerate Contract batch has finished');
		mailfinish.setPlainTextBody(bodyOfEmailSuccess + bodyOfEmailFailuire);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailfinish });
	}
}