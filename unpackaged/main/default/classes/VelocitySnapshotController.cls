/** 
 *  File Name       : VelocitySnapshotController 
 *
 *  Description     : Represents a virtual Velocity card.  The data is fetched from Crane 
 *                      
 *                      
 *  Copyright       : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
 *  @author         : Steve Kerr
 *  Date            : 10th November 2015
 *
 *  Notes           : 
 *
 **/ 
 public class VelocitySnapshotController {

	public VelocitySnapshot Snapshot {get; set;}
	private Velocity_Status_Match__c vsm;
	private Contact cont;
    private Incentive_Members__c im;
	/**
	 * Default constructor
	 */
	public VelocitySnapshotController(ApexPages.StandardController myController){
		
		String velocityNo = '';
		String name = '';
		
		// *** For the VSM page ***
		If (myController.getrecord() instanceof Velocity_Status_Match__c)
        {
			vsm=(Velocity_Status_Match__c)myController.getrecord();

			// Look up the account
			List<Velocity_Status_Match__c> VSMs = [SELECT Passenger_Velocity_Number__c, Passenger_Name__c 
			                                       FROM Velocity_Status_Match__c WHERE Id = :vsm.id];

			for(Velocity_Status_Match__c vsm1 : VSMs) 
            {
				velocityNo = vsm1.Passenger_Velocity_Number__c;
				name = vsm1.Passenger_Name__c;  
				if(velocityNo == null)
                {
					velocityNo = '';
				}          
			}    		

		// *** For the Contact page ***
		} else if (myController.getrecord() instanceof Contact)
        {
			cont=(Contact)myController.getrecord();

			// Look up the contact
			List<Contact> contacts = [SELECT FirstName,LastName,Velocity_Number__c 
			                          FROM Contact WHERE Id = :cont.id];

			for(Contact contact1 : contacts) 
            {
				velocityNo = contact1.Velocity_Number__c;
				name = contact1.FirstName + ' ' + contact1.LastName;  
				if(velocityNo == null)
                {
					velocityNo = '';
				}          
			}  			
		} else if (myController.getrecord() instanceof Incentive_Members__c) 
        {
			im=(Incentive_Members__c)myController.getrecord();

			// Look up the contact
			List<Incentive_Members__c> imember = [SELECT Name__c,Velocity_Number__c 
			                          FROM Incentive_Members__c WHERE Id = :im.id];

			for(Incentive_Members__c im1 : imember) 
            {
				velocityNo = im1.Velocity_Number__c;
				name = im1.Name__c;  
				if(velocityNo == null)
                {
					velocityNo = '';
				}          
			}  			
		}
    
	// Populate the date!!
		Snapshot = new VelocitySnapshot(velocityNo, name);
}
 }