public class EmailMessageNavigation
{
    public Id prevId {get;set;}
    public Id nextId {get;set;}
    public Id caseId {get;set;}
    public Boolean isLex {get;set;}
    public String prevURL {get;set;}
    public String nextURL {get;set;}
    public String emailMsgListURL {get;set;}
    public EmailMessage eMsg;
    public String statusMsg {get;set;}
    
    public EmailMessageNavigation(ApexPages.StandardController stdController) {        
        this.eMsg = (EmailMessage)stdController.getRecord();
        statusMsg = null;
        getPreviousNextEMsgIds();
        
    }
    
    public PageReference previousMsg(){
            if(prevId!=null){
            PageReference page = new PageReference(prevURL);
            page.setRedirect(true);
            return page;
        }
        else
            statusMsg = 'No More Previous Emails.';
        return null;
    }
    public PageReference nextMsg(){
             if(nextId!=null){
            PageReference page = new PageReference(nextURL);
            page.setRedirect(true);
            return page;
        }
        else
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No More Next Emails'));
        return null;
    }
    public PageReference emailMsgList(){
        System.debug('In emailMsgList');
        PageReference page = new PageReference(emailMsgListURL);
        page.setRedirect(true);
        return page;
    }
    public void getPreviousNextEMsgIds() {
        System.debug('eMsg--'+eMsg.Id);
        for(EmailMessage obj: [SELECT ParentId FROM EmailMessage WHERE Id =:eMsg.Id])
            caseId = obj.ParentId;
        System.debug('caseId--'+caseId);
        List<Id> lstId = new List<Id>();
        for(EmailMessage obj: [SELECT Id FROM EmailMessage WHERE ParentId =:caseId ORDER BY MessageDate ASC])
            lstId.add(obj.Id);
        System.debug('lstId--'+lstId);
        Integer result = lstId.indexOf(String.valueOf(eMsg.Id));
        System.debug('result--'+result);
        if(result > 0){
            prevId = lstId.get(result-1);
            if(result < lstId.size()-1)
                nextId = lstId.get(result+1);
        }
        else if(result == 0 && lstId.size() > 1)
            nextId = lstId.get(result+1);
        
        String uiThemeDisplayed = UserInfo.getUiThemeDisplayed();
        if(uiThemeDisplayed.equalsIgnoreCase('Theme4d'))
            isLex = true;
        else 
            isLex = false;
        System.debug('isLex--'+isLex);
        isLex = true;
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        if(isLex){
            if(prevId!=null)
                prevURL = sfdcBaseURL+'/lightning/r/EmailMessage/'+prevId+'/view';
            if(nextId!=null)
                nextURL = sfdcBaseURL+'/lightning/r/EmailMessage/'+nextId+'/view';
            emailMsgListURL = sfdcBaseURL+'/lightning/r/'+caseId+'/related/EmailMessages/view';
        }
        else{
            if(prevId!=null)
                prevURL = sfdcBaseURL+'/'+prevId;
            if(nextId!=null)
                nextURL = sfdcBaseURL+'/'+nextId;
            emailMsgListURL = sfdcBaseURL+'/ui/email/EmailMessageListPage?id='+caseId;
        }
        System.debug('prevURL--'+prevURL + emailMsgListURL );
    }
    
    
}