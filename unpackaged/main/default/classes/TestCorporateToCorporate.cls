@isTest
private class TestCorporateToCorporate
{
static testMethod void myUnitTest()
    {
    
    Test.StartTest();
        
        Id accRecTypeId = Utilities.getRecordTypeId('Account', 'Corporate');            
        Id caseRecTypeId = Utilities.getRecordTypeId('Case', 'Account Ownership/SMO/Market Segment/Change Case');            
        
        User systemsUser = [select Id,Name from User Where Name = 'Systems Administrator'];
        User salesUser = [select Id,Name from User Where Name = 'Sales Dataloader'];
        
        
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.Sales_Matrix_Owner__c = 'VIC';
        account.Market_Segment__c = 'Large Market';
	    //account.OwnerId = '00590000000K0ju'; 
	    account.OwnerId = systemsUser.Id;
        account.Account_Lifecycle__c = 'Contract';
  		//account.RecordTypeId = '01290000000tSR2';	
  		account.RecordTypeId = accRecTypeId;
 		insert account;
        
        Case c1 = new Case(//RecordTypeID = '0126F0000012HR7',
							RecordTypeId = caseRecTypeId,            
                            Accountid = account.id ,
                            Movement_Type__c = 'Corporate-Corporate',
                            //New_Account_Owner1__c= '00590000002s7Fc' ,
                            New_Account_Owner1__c = salesUser.Id,
                            NEW_Market_Segment1__c='Mid Market',
                            NEW_Sales_Matrix_Owner_SMO1__c = 'NSW',
                            Change_From_Date1__c = date.Today());    
      
         insert c1;
        
        
        List<Id> cid = new List<Id>();        
        cid.add(c1.Id);
        CorporateToCorporate.MoveCorporatetoCorporate(cid) ;
        
       Test.StopTest(); 
    }
    
    static testMethod void myUnitTest1()
    {
    
    Test.StartTest();
        
        Id accRecTypeId = Utilities.getRecordTypeId('Account', 'Corporate');            
        Id caseRecTypeId = Utilities.getRecordTypeId('Case', 'Account Ownership/SMO/Market Segment/Change Case'); 
        
        User systemsUser = [select Id,Name from User Where Name = 'Systems Administrator'];
        User salesUser = [select Id,Name from User Where Name = 'Sales Dataloader'];
        
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.Sales_Matrix_Owner__c = 'VIC';
        account.Market_Segment__c = 'Large Market';
	    //account.OwnerId = '00590000000K0ju'; 
	    account.OwnerId = systemsUser.Id;
        account.Account_Lifecycle__c = 'Contract';
  		//account.RecordTypeId = '01290000000tSR2';	
  		account.RecordTypeId = accRecTypeId;
 		insert account;
        
        Case c1 = new Case(RecordTypeID = caseRecTypeId,
                                   Accountid = account.id ,
                                   Movement_Type__c = 'Corporate-Corporate',
                                   New_Account_Owner1__c= salesUser.Id,
                                   Change_From_Date1__c = date.Today()) ;    
      
         insert c1;
        
        
        List<Id> cid = new List<Id>();        
        cid.add(c1.Id);
        CorporateToCorporate.MoveCorporatetoCorporate(cid) ;
        
       Test.StopTest(); 
    }
   
    
     static testMethod void myUnitTest2()
    {
    
    Test.StartTest();
        
        Id accRecTypeId = Utilities.getRecordTypeId('Account', 'Corporate');            
        Id caseRecTypeId = Utilities.getRecordTypeId('Case', 'Account Ownership/SMO/Market Segment/Change Case'); 
        
        User systemsUser = [select Id,Name from User Where Name = 'Systems Administrator'];        
        
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.Sales_Matrix_Owner__c = 'VIC';
        account.Market_Segment__c = 'Large Market';
	    //account.OwnerId = '00590000000K0ju'; 
	    account.OwnerId = systemsUser.Id;
        account.Account_Lifecycle__c = 'Contract';
  		//account.RecordTypeId = '01290000000tSR2';	
  		account.RecordTypeId = accRecTypeId;
 		insert account;
        
        Case c1 = new Case(RecordTypeID = caseRecTypeId,
                                   Accountid = account.id ,
                                   Movement_Type__c = 'Corporate-Corporate',
                                   NEW_Market_Segment1__c='Mid Market',
                                   Change_From_Date1__c = date.Today()) ;    
      
         insert c1;
        
        
        List<Id> cid = new List<Id>();        
        cid.add(c1.Id);
        CorporateToCorporate.MoveCorporatetoCorporate(cid) ;
        
       Test.StopTest(); 
    }
}