/**
* @description       : Test class for ExternalSharingCheck
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestExternalSharingCheck {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
    }
    
    @isTest
    private static void testExternalSharingCheck()  {
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1];
        Test.startTest();
        PageReference pageRef = Page.ExternalSharingBanner;
        pageRef.getParameters().put('a.id', testAccountObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testAccountObj);
        ExternalSharingCheck externalSharingController  = new ExternalSharingCheck(conL);
        PageReference initDiscPageReference = externalSharingController.initDisc();
        PageReference stopSharingPageReference = externalSharingController.stopsharing();
        Test.stopTest();
        //Asserts
        system.assertEquals('/' + testAccountObj.Id, stopSharingPageReference.getUrl());
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.INFO));
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('This account is  not externally shared.', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.INFO, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testExternalStopSharingCheck()  {
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1];
        PartnerNetworkRecordConnection testPartnerNetworkRecordConnectionObj = TestDataFactory.createTestPartnerNetworkRecordConnection(null, testAccountObj.Id);
        Test.startTest();
        PageReference pageRef = Page.ExternalSharingBanner;
        Test.setCurrentPage(pageRef);
        ExternalSharingCheck.isCallFromTestClass = true;
        ApexPages.StandardController conL = new ApexPages.StandardController(testAccountObj);
        ExternalSharingCheck externalSharingController  = new ExternalSharingCheck(conL);
        externalSharingController.searchResults = new List<PartnerNetworkRecordConnection>{testPartnerNetworkRecordConnectionObj};
        PageReference stopSharingPageReference = externalSharingController.stopsharing();
        Test.stopTest();
        //Asserts
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.INFO));
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals(true, msg.getSummary().containsIgnoreCase('This account is externally shared and was received from Flight Centre Travel Group since '));
            System.assertEquals(ApexPages.SEVERITY.INFO, msg.getSeverity());
        }
    }
}