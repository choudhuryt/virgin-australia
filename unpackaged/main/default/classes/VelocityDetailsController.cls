/**
* This class is controller for VelocityDetails VF page
* @author Son Pham
*/
public class VelocityDetailsController{
    public GuestCaseVelocityInfoModel model {get; set;}
    public GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType velocityDetails {get; set;}
    public GuestCaseVelocityInfoModel.MemberInformationType memberInfo {get; set;}
    public GuestCaseVelocityInfoModel.PersonType person {get; set;}
    public String contactEmail1 {get; set;}
    public GuestCaseVelocityInfoModel.ContactTelephoneType contactTelephone1 {get; set;}
    public GuestCaseVelocityInfoModel.AddressType address1 {get; set;}
    public GuestCaseVelocityInfoModel.CountryType country1 {get; set;}
    public String addressLine {get; set;}
    // For Profile Details subsection
    public GuestCaseVelocityInfoModel.ProfileInformationType profileInfo {get; set;}
    // For employment details
    public GuestCaseVelocityInfoModel.EmploymentInformationType employmentInfo {get; set;}
    // For Preference
    public GuestCaseVelocityInfoModel.PreferencesType preference {get;set;}
    public GuestCaseVelocityInfoModel.SeatingPreferenceType seatingPref {get; set;}
    public GuestCaseVelocityInfoModel.CommunicationPreferencesType commPref{get; set;}
    public String velocityNumber {get; set;}
    public String caseId {get; set;}
    public PrefModel[] addressModel {get; set;}
    public PrefModel[] preferenceModel{get; set;}
    public string needClose{get;set;}
    public boolean isError{get;set;}

    /**
    * Default constructor
    */
    public VelocityDetailsController(){
    	isError = false;
        velocityNumber = ApexPages.currentPage().getParameters().get('velocityNumber');
        caseId = ApexPages.currentPage().getParameters().get('caseId');
        GuestCaseVelocityInfo.SalesforceLoyaltyService service = new GuestCaseVelocityInfo.SalesforceLoyaltyService();
        // Retrieve Apex Callout options from Custom Settings
        Apex_Callouts__c apexCallouts = Apex_Callouts__c.getValues('VelocityDetails');
        if(apexCallouts != null){
            service.endpoint_x = apexCallouts.Endpoint_URL__c;
            service.timeout_x = Integer.valueOf(apexCallouts.Timeout_ms__c);
            service.clientCertName_x = apexCallouts.Certificate__c;
        }
        try {
            //Pass velocity number to get data from webservice
            velocityDetails = service.GetLoyaltyDetails(velocityNumber);
    
            prepareMemberShipDetails();
    
            this.contactEmail1 = prepareContactEmail(memberInfo.ContactEmail);
            prepareProfileDetails();
            prepareEmploymentDetails();
    
            preference = profileInfo.Preferences;
            GuestCaseVelocityInfoModel.SeatingPreferenceType[] SeatingPreference = preference.SeatingPreference;
            this.seatingPref = prepareSeatingPreferences(SeatingPreference);
    
            // build address model
            String[] leftFieldLabels = new String[] {'Surname', 'Title', 'Date of Birth', 'Gender'};
            String[] leftFieldValues = new String[] {person.Surname, person.Title, person.DateOfBirth, person.Gender};
            String[] rightFieldValues = new String[] {AddressLine, address1.County, address1.PostalCode, country1.CountryCode};
            this.addressModel = buildListModel(leftFieldLabels, leftFieldValues, 'Address', rightFieldValues);
    
            // build destinations model
            PrefModel[] prefList = new List<PrefModel>();
            commPref = new GuestCaseVelocityInfoModel.CommunicationPreferencesType();
            if (profileInfo.CommunicationPreferences != null) {
                commPref = profileInfo.CommunicationPreferences;
            }
            String[] destinations = preference.DestinationPreference;
            String[] refFieldLabels = new String[] {'Receive Market Research', 'Receive Member Updates', 'Other Parner Offers', 'Travel Partner Offers'};
            String[] refFieldValues = new String[] {String.valueOf(commPref.MarketResearch), String.valueOf(commPref.MemberUpdates), String.valueOf(commPref.OtherPartnerOffers), String.valueOf(commPref.TravelPartnerOffer)};
            this.preferenceModel = buildListModel(refFieldLabels, refFieldValues, 'Destinations', destinations);
    
            needClose = '';
        } catch (System.CalloutException e) {
        	isError = true;
            String error = e.getMessage();
            if (String.isNotEmpty(error) && String.isNotBlank(error.substringAfter('SOAP Fault:').substringBefore('faultcode'))) {
                error = error.substringAfter('SOAP Fault:').substringBefore('faultcode');
            } else {
                error = 'Communication Error';
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
        } catch(Exception e){
        	isError = true;
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    /**
    * Loads membership details from loyalty details returned form webservice
    */
    private void prepareMemberShipDetails() {
        memberInfo = velocityDetails.MemberInformation;
        person = memberInfo.Person;
        GuestCaseVelocityInfoModel.AddressType[] addressList = memberInfo.Address;
        address1 = addressList.get(0);
        country1 = address1.Country;
        addressLine = '';
        for (String addline : address1.AddressLine) {
            addressLine += ' ' + addline;
        }
        //contact telephone
        GuestCaseVelocityInfoModel.ContactTelephoneType[] contactTelephones = memberInfo.ContactTelephone;
        contactTelephone1 = new GuestCaseVelocityInfoModel.ContactTelephoneType();
        if (contactTelephones != null || contactTelephones.size() > 0) {
            contactTelephone1 = contactTelephones.get(0);
        }
    }

    /**
    * Gets the first email in email list
    * @param emails array of email address
    * @return the first item if the list is not empty, empty string otherwise
    */
    public String prepareContactEmail(String[] emails) {
        if (emails != null && emails.size() > 0) {
            return emails.get(0);
        }
        return '';
    }

    /**
    * Gets profile information from loyalty details returned from webservice
    */
    private void prepareProfileDetails() {
        profileInfo = velocityDetails.ProfileInformation;
    }

    /**
    * Gets employment information from loyalty details returned from webservice
    */
    private void prepareEmploymentDetails() {
        employmentInfo = memberInfo.EmploymentInformation;
    }

    /**
    * Gets seating reference from loyalty details returned from webservice
    */
    public GuestCaseVelocityInfoModel.SeatingPreferenceType prepareSeatingPreferences(GuestCaseVelocityInfoModel.SeatingPreferenceType[] SeatingPreference) {
        if (SeatingPreference != null && SeatingPreference.size() > 0) {
            return SeatingPreference.get(0);
        }
        return new GuestCaseVelocityInfoModel.SeatingPreferenceType();
    }

    /**
    * Builds model to display fields in case right field's values are not equal to number of left-column fields
    * @param leftFieldLabels field labels of left column on the velocity detail form
    * @param leftFieldValues field values of left column on the velocity detail form
    * @param rightFieldLabel field label of right column on the velocity detail form
    * @param rightFieldValues field values of right column on the velocity detail form
    * @return list of reference model used to display fields on the form
    */
    private PrefModel[] buildListModel(String[] leftFieldLabels, String[] leftFieldValues, String rightFieldLabel, String[] rightFieldValues) {
        PrefModel pModel;
        PrefModel[] prefList = new List<PrefModel>();
        for (Integer i = 0; i < leftFieldLabels.size(); i++) {
            // for left i-th item
            pModel = new PrefModel(leftFieldLabels.get(i), leftFieldValues.get(i));
            prefList.add(pModel);
            // for right i-th item
            String label = '';
            // display label at the first time
            if (i == 0) {
                label = rightFieldLabel;
            }
            if(rightFieldValues != null && rightFieldValues.size() > i) {
                pModel = new PrefModel(label, rightFieldValues.get(i));
                prefList.add(pModel);
            } else {
                pModel = new PrefModel(label, '');
                prefList.add(pModel);
            }
        }
        if (rightFieldValues != null && rightFieldValues.size() > leftFieldLabels.size()) {
            for (Integer i = leftFieldLabels.size(); i < rightFieldValues.size(); i++) {
                // left empty item
                pModel = new PrefModel();
                prefList.add(pModel);
                // right non-label item
                pModel = new PrefModel('', rightFieldValues.get(i));
                prefList.add(pModel);
            }
        }
        return prefList;
    }

    /**
    * Model class to display fields
    */
    public class PrefModel {
        public String label {get; set;}
        public String value {get; set;}
        public PrefModel(){
            this.label = '';
            this.value = '';
        }
        public PrefModel(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
    
    /**
    * Saves velocity to SF
    */
    public void SaveVelocity()
    {
        Velocity__c item = new Velocity__c();
        item.Email__c = contactEmail1;
        item.First_Name__c = person.GivenName;
        item.Surname__c = person.Surname;
        item.Tier__c = profileInfo.TierLevel;
        item.Velocity_Number__c = velocityNumber;
        item.Case__c = this.caseId;
        item.Phone_Number__c = contactTelephone1.UnparsedTelephoneNumber;
        item.Point_Balance__c =  profileInfo.PointsBalance;
        item.Search_Date_Time__c = system.now();
        // begine DE4948 fix - set Case.Travel_Bank_Account__c to Velocity__c.Travel_Bank_Number__c instead of a WS callout
        List<Case> caseList = [Select Id, Travel_Bank_Account__c From Case Where Id =: this.caseId];
        if (caseList != null && !caseList.isEmpty()) {
            Case theCase = caseList.get(0);
            item.Travel_Bank_Number__c = theCase.Travel_Bank_Account__c;
        }
        // end DE7948 fix
        try
        {
            insert item;
            needClose = '1';
        }
        catch (Exception ex)
        {
        	isError = true;
            ApexPages.addMessages(ex);
        }
    }
}