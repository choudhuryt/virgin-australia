/*
*   Test class for ReservationController
*/
@isTest
private class TestReservationController{
    
    // Senario 1: Reservation already exist, data will be loaded from Salesforce
    static testMethod void testSearchInDatabase() {
        Test.setMock(WebServiceMock.class, new GetTravelBankServiceMockImpl());
        Case tmpCase = new Case();
        insert tmpCase;
        
        Reservation__c tmpReser = new Reservation__c(Reservation_Number__c = 'ZZZ');
        tmpReser.Case__c = tmpCase.Id;
        insert tmpReser;

        Passenger__c tmpPassenger = new Passenger__c();
        tmpPassenger.Reservation__c = tmpReser.Id;
        insert tmpPassenger;
        
        FlightSegment__c tmpFlight = new FlightSegment__c();
        tmpFlight.Reservation__c = tmpReser.Id;
        insert tmpFlight;
        
        SpecialServiceRequest__c tmpSSR = new SpecialServiceRequest__c();
        tmpSSR.Flight_Segment__c = tmpFlight.Id;
        tmpSSR.Passenger__c = tmpPassenger.Id;
        insert tmpSSR;
        
        Other_Service_Information__c tmpOSI = new Other_Service_Information__c();
        tmpOSI.Passengers__c = tmpPassenger.Id;
        insert tmpOSI;
        
        PageReference reserSectionPage = new PageReference('/apex/ReservationSection?id='+tmpCase.Id+'&pnr='+tmpReser.Reservation_Number__c+'&date='+System.today());
        Test.setCurrentPage(reserSectionPage);
        
        ReservationController customReserController = new ReservationController();       
        System.assert(customReserController.flightSegmentList.size() == 1);
        System.assert(customReserController.passengerList.size() == 1);
        System.assert(customReserController.ssrList.size() == 1);  
        System.assert(customReserController.osiList.size() == 1);
    }
    
    // Senario 2: Reservation object is not exist on Salesforce yet, 
    // data will be load from web service.
    static testMethod void testSearchInServiceAndSave() {
        Test.setMock(WebServiceMock.class, new ReservationServiceMockImpl());
        
        Case tmpCase = new Case();
        insert tmpCase;
		
        Test.startTest();
        PageReference reserSectionPage = new PageReference('/apex/ReservationSection?id='+tmpCase.Id+'&pnr=AAA&date='+System.now().format('dd/MM/yyyy'));
        Test.setCurrentPage(reserSectionPage);
        
        ReservationController customReserController = new ReservationController(); 
        customReserController.saveReservation();
        Test.stopTest();
        
        Reservation__c reserObj = [select Id, Reservation_Number__c 
                                   from Reservation__c where Case__c = :tmpCase.Id and Reservation_Number__c = 'AAA'];
        List<FlightSegment__c> flightSegmentList = [select Id from FlightSegment__c where Reservation__c = :reserObj.Id];
        List<Passenger__c> passengerList = [select Id from Passenger__c where Reservation__c = :reserObj.Id];
        List<SpecialServiceRequest__c> ssrList = [select Id from SpecialServiceRequest__c 
                                                  where Flight_Segment__r.Reservation__c = :reserObj.Id];
        List<Other_Service_Information__c> osiList = [select Id from Other_Service_Information__c 
                                                      where Passengers__r.Reservation__c = :reserObj.Id];
        
        System.assert(passengerList.size() > 0);
        System.assert(flightSegmentList.size() > 0);
        System.assert(ssrList.size() > 0);
        System.assert(osiList.size() > 0);
    }

}