global class ResponsysBatch implements Database.Batchable<SObject>{

	/**
	 * File Name      : Accelerate Contracts Batch
	 * Description        : This Apex Class is a batch class that runs every day to look for Responsys Campaign members who have a status of 'hard bounced' or 'Opted Out' or 'Reported as SPAM'.
                       If one of these is found the Account Manager is notified and the email is removed from the contact.
	 * * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
	 * * @author          : Edward Stachyra
	 * * Date             : 29 August 2012
	 * * Technical Task ID: 
	 * * Notes            : Batch Classes require the start, execute and finish methods and must implement Database.Batchable
                     : To run as a schedule task they also need a Schedule Class that implements Schedulable, this class has
                       a scheduledClass called  
                       1.)	Hard Bounce –              The Batch will:
                                                       Null their email in the Contact area in Salesforce
                                                       Mark a boolean checkbox as Handled
                                                       Notify the Account Manager via an email
                                                       Notify the Salesforce Administrator via an email


						2.) Reported as SPAM– 			 The Batch will unsubscribe the Contact from email in their affiliated Subscription
                                                         will null their email in the Contact area in Salesforce
                                                         Mark a boolean checkbox as Handled
                                                         Notify the Account Manager via an email
                                                        Notify the Salesforce Administrator via an email


						3.) Opt Out   -                  The Batch will unsubscribe the Contact from email in their affiliated Subscription
                                                         Mark a boolean checkbox as Handled
                                                         Notify the Account Manager via an email
                                                         Notify the Salesforce Administrator via an email

	 * Modification Log ==================================================​============= 
Ver Date Author Modification --- ---- ------ -------------
	 * */ 


	global ResponsysBatch(){}

	global  String gstrQuery = 'SELECT CampaignId, Id, ContactId, CreatedById, CreatedDate, IsDeleted, FirstRespondedDate, Handled__c, LastModifiedById, LastModifiedDate, LeadId, HasResponded, Status, SystemModstamp FROM CampaignMember WHERE Status !=\'Sent\' AND handled__c = false  LIMIT 8 ';
	//global  String gstrQuery = 'SELECT CampaignId, Id, ContactId, CreatedById, CreatedDate, IsDeleted, FirstRespondedDate, Handled__c, LastModifiedById, LastModifiedDate, LeadId, HasResponded, Status, SystemModstamp FROM CampaignMember WHERE HasResponded =false AND Status !=\'Sent\' AND handled__c = false LIMIT 8 ';
	global  integer flagsend = 0;
	global  integer flagsendSPAM = 0;
	global  integer flagsendOut = 0;
	global String match;
	global String matchSPAM;
	global String matchOut;
	global integer counter=0;

	//Loading and running the query string
	global Database.QueryLocator start(Database.BatchableContext BC){

		return Database.getQueryLocator(gstrQuery);
	}   

	//Running The execute Method
	global void execute(Database.BatchableContext BC, 
			List<sObject> scope){

		List<CampaignMember> campaignmemList = new List <CampaignMember>();	  //this will be the scope
		for (Sobject s : scope)
		{
			CampaignMember c = (CampaignMember)s;
			campaignmemList.add(c);
		}  


		//Campaign Members List for Hard Bounce
		List <CampaignMember> campMembers = 
				[ 
				 SELECT CampaignId, Id, ContactId,  IsDeleted, 
				 Status, Handled__c
				 FROM CampaignMember 
				 where Status = 'Hard Bounced'
				 AND ContactId in (SELECT Id FROM Contact)
				 AND Handled__c = false //LIMIT 8
				 ];   

		//Campaign Members List for Complained of SPAM
		List <CampaignMember> campMembersSPAM = 
				[ 
				 SELECT CampaignId, Id, ContactId,  IsDeleted, 
				 Status, Handled__c
				 FROM CampaignMember 
				 where Status = 'Complained of spam'//'Complained of spam' 
				 AND ContactId in (SELECT Id FROM Contact)
				 AND Handled__c = false //LIMIT 8
				 ];   

		//Campaign Members List for Opted Out
		List <CampaignMember> campMembersOut = 
				[ 
				 SELECT CampaignId, Id, ContactId,  IsDeleted, 
				 Status, Handled__c
				 FROM CampaignMember 
				 where Status = 'Opted Out' 
				 AND ContactId in (SELECT Id FROM Contact)
				 AND Handled__c = false //LIMIT 8
				 ];  


		//A Contact List for Hard Bounce
		List<Contact> notifylist = new List<Contact>();
		List<Contact> notifylist2 = new List<Contact>();

		List<Contact> cont =
				[ 
				 SELECT AccountId,  
				 Id, IsDeleted, 
				 Email,  
				 FirstName, LastName, OwnerId, Subscriptions__c 
				 FROM Contact where IsDeleted = false AND Id in (SELECT ContactId FROM CampaignMember where Status = 'Hard Bounced' AND Handled__c = false )  ORDER BY OwnerId LIMIT 8 ]; //'Hard Bounced' //'Responded'



		//A Contact List for Complained of SPAM
		List<Contact> notifylist2SPAM = new List<Contact>();
		List<Contact> contSPAM =
				[ 
				 SELECT AccountId,  
				 Id, IsDeleted, 
				 Email,  
				 FirstName, LastName, OwnerId, Subscriptions__c 
				 FROM Contact where IsDeleted = false AND Id in (SELECT ContactId FROM CampaignMember where Status = 'Complained of spam' AND Handled__c = false) ORDER BY OwnerId LIMIT 8 ]; //'Complained of spam' 


		//A Contact List for Opted Out
		List<Contact> notifylist2Out = new List<Contact>();
		List<Contact> contOut =
				[ 
				 SELECT AccountId,  
				 Id, IsDeleted, 
				 Email,  
				 FirstName, LastName, OwnerId, Subscriptions__c 
				 FROM Contact where IsDeleted = false AND Id in (SELECT ContactId FROM CampaignMember where Status = 'Opted Out' AND Handled__c = false ) ORDER BY OwnerId LIMIT 8];//'Opted Out' 


		//A Subscription List for Complained of Hard Bounce
		List<Subscriptions__c> sub =
				[ 
				 SELECT Accelerate_EDM__c, Accelerate_EDM_Opt_Out__c, Business_News__c,
				 Business_News_Opt_Out__c, Contact_ID__c, CreatedById, CreatedDate, 
				 IsDeleted, Fare_Sheets__c, Fare_Sheets_Opt_Out__c, Invitations__c, 
				 Invitations_Opt_Out__c,Xmas_Card__c,Xmas_Card_Opt_Out__c, LastModifiedById, LastModifiedDate, OwnerId, 
				 Id, Name, SystemModstamp, Trade_Release__c, Trade_Release_Opt_Out__c 
				 FROM Subscriptions__c
				 where IsDeleted = false AND Id in (SELECT Subscriptions__c FROM Contact where Id in:cont )];

		//A Subscription List for Complained of SPAM
		List<Subscriptions__c> subSPAM =
				[ 
				 SELECT Accelerate_EDM__c, Accelerate_EDM_Opt_Out__c, Business_News__c,
				 Business_News_Opt_Out__c, Contact_ID__c, CreatedById, CreatedDate, 
				 IsDeleted, Fare_Sheets__c, Fare_Sheets_Opt_Out__c, Invitations__c, 
				 Invitations_Opt_Out__c,Xmas_Card__c,Xmas_Card_Opt_Out__c, LastModifiedById, LastModifiedDate, OwnerId, 
				 Id, Name, SystemModstamp, Trade_Release__c, Trade_Release_Opt_Out__c 
				 FROM Subscriptions__c
				 where IsDeleted = false AND Id in (SELECT Subscriptions__c FROM Contact where Id in:contSPAM )];


		//A Subscription List for Opted Out
		List<Subscriptions__c> subOut =
				[ 
				 SELECT Accelerate_EDM__c, Accelerate_EDM_Opt_Out__c, Business_News__c,
				 Business_News_Opt_Out__c, Contact_ID__c, CreatedById, CreatedDate, 
				 IsDeleted, Fare_Sheets__c, Fare_Sheets_Opt_Out__c, Invitations__c, 
				 Invitations_Opt_Out__c,Xmas_Card__c,Xmas_Card_Opt_Out__c,LastModifiedById, LastModifiedDate, OwnerId, 
				 Id, Name, SystemModstamp, Trade_Release__c, Trade_Release_Opt_Out__c 
				 FROM Subscriptions__c
				 where IsDeleted = false AND Id in (SELECT Subscriptions__c FROM Contact where Id in:contOut )];


		//An Account Manager List for Hard Bounce
		List<user> noti = new List<user>();
		List<user> userx = 
				[
				 SELECT AccountId,  MobilePhone, 
				 City, CommunityNickname, CompanyName, ContactId,
				 Department, Division, Email, EmailEncodingKey, FirstName, Name, ReceivesInfoEmails, 
				 LanguageLocaleKey, LastLoginDate, LastModifiedById, 
				 LastModifiedDate, LastName, LastPasswordChangeDate, 
				 LocaleSidKey, Manage_Analysis_Questions__c, ManagerId, 
				 UserPermissionsMarketingUser, OfflineTrialExpirationDate, UserPermissionsOfflineUser, Phone, ProfileId,  
				 Title, Id, UserType, Username
				 FROM User
				 where  Id in (SELECT OwnerId FROM Contact where Id in:cont )];

		//An Account Manager List for Complained of SPAM
		List<user> userxSPAM = 
				[
				 SELECT AccountId,  MobilePhone, 
				 City, CommunityNickname, CompanyName, ContactId,
				 Department, Division, Email, EmailEncodingKey, FirstName, Name, ReceivesInfoEmails, 
				 LanguageLocaleKey, LastLoginDate, LastModifiedById, 
				 LastModifiedDate, LastName, LastPasswordChangeDate, 
				 LocaleSidKey, Manage_Analysis_Questions__c, ManagerId, 
				 UserPermissionsMarketingUser, OfflineTrialExpirationDate, UserPermissionsOfflineUser, Phone, ProfileId,  
				 Title, Id, UserType, Username
				 FROM User
				 where  Id in (SELECT OwnerId FROM Contact where Id in:contSPAM )];

		//An Account Manager List for Opted Out
		List<user> userxOut = 
				[
				 SELECT AccountId,  MobilePhone, 
				 City, CommunityNickname, CompanyName, ContactId,
				 Department, Division, Email, EmailEncodingKey, FirstName, Name, ReceivesInfoEmails, 
				 LanguageLocaleKey, LastLoginDate, LastModifiedById, 
				 LastModifiedDate, LastName, LastPasswordChangeDate, 
				 LocaleSidKey, Manage_Analysis_Questions__c, ManagerId, 
				 UserPermissionsMarketingUser, OfflineTrialExpirationDate, UserPermissionsOfflineUser, Phone, ProfileId,  
				 Title, Id, UserType, Username
				 FROM User
				 where  Id in (SELECT OwnerId FROM Contact where Id in:contOut )];



		//For loop for Contacts who have Hard Bounced   
		for(Contact mmm :cont )
		{  
			if (counter ==9)
			{
				break;
			}
			notifylist.add(mmm);

			Account accountname = new Account();
			accountname = [SELECT Name, Account_Owner__r.Email from Account WHERE Id =: mmm.AccountId];
			
			match = accountname.Account_Owner__r.Email;
			
			// waste of time
			//for (User zzz:userx)
			//{
			//	if (zzz.Id == mmm.OwnerId)
			//		match = zzz.email;  //found the Account Manager's email
			//}


			string tempOwneridorig = mmm.OwnerId;
			if(mmm.OwnerId == tempOwneridorig)
			{
				flagsend = 1; 	
				notifylist2.add(mmm);
			}

			if (flagsend==1)
			{
				integer x=0;


				Messaging.SingleEmailMessage mailnotifyaccountman = new Messaging.SingleEmailMessage();
				mailnotifyaccountman.setToAddresses(new String[] {'commercialalliancesystems@virginaustralia.com','salesforce.admin@virginaustralia.com', + match});
				mailnotifyaccountman.setReplyTo('commercialalliancesystems@virginaustralia.com');
				mailnotifyaccountman.setSenderDisplayName('ACCOUNT MANAGER ALERT NOTIFICATION for your CONTACT');
				mailnotifyaccountman.setSubject('ACCOUNT MANAGER ALERT – Invalid email address for your Contact in Salesforce ');
				//mailfinish.setPlainTextBody(' -- TEST --  Responsys Batch test in Dev 2 Sandbox --TEST -- '  );
				mailnotifyaccountman.setPlainTextBody(  
						'Dear Account Manager, \n' + 

						'\n You are receiving this email because one of your Contacts in Salesforce has an email address that is no longer valid. \n' + 

						'\n The email address:  '+mmm.Email + ' for ' + mmm.FirstName + ' ' + mmm.LastName + ','+ ' Salesforce Account: ' + accountname.Name + ' has been removed from Salesforce and will no longer receive any communications from Virgin Australia. \n' +

						'\n Please update Salesforce with a new valid email address for your Contact. \n' +

						'\n The Contact is still subscribed to receive communications, as they have not withdrawn consent.  \n' +
						'\n Thank you.' +
						'\n\n The Virgin Australia Salesforce Team'

						);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailnotifyaccountman });
				counter=counter+1;
				flagsend =0;
				notifylist2.clear();

			}	

			if (mmm.Email != null)
				//if (mmm.Email != null)&&(mmm.Id==acc.)
			{

				mmm.Email = 'donotsend@virginaustralia.com';

			}

			//Marking the Campaign Member as handled //

			for(CampaignMember acc :campMembers )
			{


				//if (acc.status =='Hard Bounced')
				if (acc.status =='Hard Bounced' && acc.ContactId == mmm.Id) 
				{
					//acc.status = 'Soft Bounced';
					acc.handled__c = true; 


				}	
			}

			//

			update cont;

		}


		//For loop for Contacts who have Reported SPAM
		for(Contact mmmSPAM :contSPAM )
		{  
			if (counter ==9)
			{
				break;
			}
			notifylist.add(mmmSPAM);

			Account accountname2 = new Account();
			accountname2 = [SELECT Name, Account_Owner__r.Email from Account WHERE Id =: mmmSPAM.AccountId];

			// We will notify the owner of the account
			matchSPAM = accountname2.Account_Owner__r.Email;
			
			//for (User zzzSPAM:userxSPAM)
			//{
			//	if (zzzSPAM.Id == mmmSPAM.OwnerId)
			//		matchSPAM = zzzSPAM.email;  //found the Account Manager's email
			//}


			string tempOwneridorigSPAM = mmmSPAM.OwnerId;
			if(mmmSPAM.OwnerId == tempOwneridorigSPAM)
			{
				flagsendSPAM = 1; 	
				notifylist2SPAM.add(mmmSPAM);
			}

			if (flagsendSPAM==1)
			{
				integer xSPAM=0;

				Messaging.SingleEmailMessage mailnotifyaccountman2 = new Messaging.SingleEmailMessage();
				mailnotifyaccountman2.setToAddresses(new String[] {'commercialalliancesystems@virginaustralia.com','salesforce.admin@virginaustralia.com' }); //, + matchSPAM
				mailnotifyaccountman2.setReplyTo('commercialalliancesystems@virginaustralia.com');
				mailnotifyaccountman2.setSenderDisplayName('SALESFORCE ADMINISTRATOR ALERT NOTIFICATION for a CONTACT');
				mailnotifyaccountman2.setSubject('SALESFORCE ADMINISTRATOR ALERT -REPORT AS SPAM NOTIFICATION for your CONTACT enclosed -  Responsys  ');
				//mailfinish.setPlainTextBody(' -- TEST --  Responsys Batch test in Dev 2 Sandbox --TEST -- '  );
				mailnotifyaccountman2.setPlainTextBody(  
						'SALESFORCE ADMINISTRATOR, \n'+

						'\n You are receiving this email because one of your Contacts in Salesforce has an email address that was reported as SPAM.    \n'+ 

						'\n The email address:  '+mmmSPAM.Email + ' for ' + mmmSPAM.FirstName +' ' +  mmmSPAM.LastName + ', Salesforce Account: ' + accountname2.Name + ' has been removed from Salesforce and will no longer receive any communications from Virgin Australia. \n' +
						'\n Thank you.' +
						'\n\n The Virgin Australia Salesforce Team'
						);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailnotifyaccountman2 });
				counter=counter+1;
				flagsendSPAM =0;
				notifylist2SPAM.clear();

			}	

			if (mmmSPAM.Email != null)
				//if (mmm.Email != null)&&(mmm.Id==acc.)
			{

				mmmSPAM.Email = 'donotsend@virginaustralia.com';

			}


			//Marking the Campaign Member as handled and then Opting out of their Subscriptions ///////
			for(CampaignMember accSPAM :campMembersSPAM )
			{


				if (accSPAM.status == 'Complained of spam' && accSPAM.ContactId == mmmSPAM.Id )
				{

					accSPAM.Handled__c = true;

					for(Subscriptions__c xxxSPAM :subSPAM  )
					{
						if (xxxSPAM.Contact_ID__c == mmmSPAM.Id && accSPAM.status == 'Complained of spam' )
						{
							xxxSPAM.Accelerate_EDM_Opt_Out__c = true;
							xxxSPAM.Accelerate_EDM__c = false;
							xxxSPAM.Business_News_Opt_Out__c = true;
							xxxSPAM.Business_News__c = false;
							xxxSPAM.Fare_Sheets_Opt_Out__c = true;
							xxxSPAM.Fare_Sheets__c = false;
							xxxSPAM.Invitations__c = false;                           	
							xxxSPAM.Invitations_Opt_Out__c = true;
                            xxxSPAM.Xmas_Card__c = false;
                            xxxSPAM.Xmas_Card_Opt_Out__c = true;
							xxxSPAM.Trade_Release__c = false;
							xxxSPAM.Trade_Release_Opt_Out__c = true;
							xxxSPAM.Fare_Advice__c = false;
							xxxSPAM.Fare_Advice_Opt_Out__c = true;
							
						}	

					}
					update subSPAM;
				}	
			}  


			//


			update contSPAM;


		} 

		//For loop for Contacts who have Reported Opt Out
		for(Contact mmmOut :contOut )
		{  
			if (counter ==9)
			{
				break;
			}

			notifylist.add(mmmOut);

			Account accountname3 = new Account();
			accountname3 = [SELECT Name from Account WHERE Id =: mmmOut.AccountId];

			for (User zzzOut:userxSPAM)
			{
				if (zzzOut.Id == mmmOut.OwnerId)
					match = zzzOut.email;  //found the Account Manager's email
			}


			string tempOwneridorigOut = mmmOut.OwnerId;
			if(mmmOut.OwnerId == tempOwneridorigOut)
			{
				flagsendOut = 1; 	
				notifylist2Out.add(mmmOut);
			}

			if (flagsendOut==1)
			{
				integer xOut=0;

				Messaging.SingleEmailMessage mailnotifyaccountman3 = new Messaging.SingleEmailMessage();
				mailnotifyaccountman3.setToAddresses(new String[] {'commercialalliancesystems@virginaustralia.com','salesforce.admin@virginaustralia.com'});//, + matchSPAM});
				mailnotifyaccountman3.setReplyTo('commercialalliancesystems@virginaustralia.com');
				mailnotifyaccountman3.setSenderDisplayName('SALESFORCE ADMINISTRATOR ALERT NOTIFICATION for a CONTACT');
				mailnotifyaccountman3.setSubject('SALESFORCE ADMINISTRATOR ALERT - OPT OUT NOTIFICATION for a CONTACT enclosed -  Responsys');
				//mailfinish.setPlainTextBody(' -- TEST --  Responsys Batch test in Dev 2 Sandbox --TEST -- '  );
				mailnotifyaccountman3.setPlainTextBody(  
						//'*****  SALESFORCE ADMINISTRATOR, You are receiving this email because one of your CONTACTS who is a CAMPAIGN MEMBER was reported by Responsys as an OPT OUT.  Please take appropriate action. The contact affected is listed here --->'  + notifylist2Out

						'SALESFORCE ADMINISTRATOR, \n'+

						'\n You are receiving this email because one of your Contacts in Salesforce has an email address that was reported by Responsys as an OPT OUT.    \n'+ 

						'\n The email address:  '+mmmOut.Email + ' for ' + mmmOut.FirstName +' '+  mmmOut.LastName + ', Salesforce Account: ' + accountname3.Name + ' has been unsubscribed from appropriate communication as per their request. \n' +
						'\n Thank you.' +
						'\n\n The Virgin Australia Salesforce Team'


						);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailnotifyaccountman3 });
				counter=counter+1;
				flagsendOut =0;
				notifylist2Out.clear();

			}	

			if (mmmOut.Email != null)
			{

				mmmOut.Email = 'donotsend@virginaustralia.com';

			}

			//Marking the Campaign Member as handled and then Opting out of their Subscriptions///////

			for(CampaignMember accOut :campMembersOut )
			{

				if (accOut.status =='Opted Out' && accOut.ContactId == mmmOut.Id )
				{

					accOut.Handled__c = true;

					for(Subscriptions__c xxxOut :subOut )
					{

						if (xxxOut.Contact_ID__c == mmmOut.Id && accOut.status == 'Opted Out'  )
						{
							//Opting out of all of their Subscriptions		
							xxxOut.Accelerate_EDM_Opt_Out__c = true;
							xxxOut.Accelerate_EDM__c = false;
							xxxOut.Business_News_Opt_Out__c = true;
							xxxOut.Business_News__c = false;
							xxxOut.Fare_Sheets_Opt_Out__c = true;
							xxxOut.Fare_Sheets__c = false;
							xxxOut.Invitations__c = false;
							xxxOut.Invitations_Opt_Out__c = true;
                            xxxOut.Xmas_Card__c = false;
                            xxxOut.Xmas_Card_Opt_Out__c = true;
							xxxOut.Trade_Release__c = false;
							xxxOut.Trade_Release_Opt_Out__c = true;
							xxxOut.Fare_Advice__c = false;
							xxxOut.Fare_Advice_Opt_Out__c = true;
						}

					}
					update subOut;
				}	
			}


			//

			update contOut;


		} 

		try {

			update campMembers;
			update campMembersSPAM;
			update campMembersOut;


		}catch(Exception e){

			//Sending an Email if there are any errors when trying to update and insert the new records and expired records in the VA_Revenue__c table.		
			Messaging.SingleEmailMessage mailError = new Messaging.SingleEmailMessage();
			mailError.setToAddresses(new String[] {'ed.stachyra@virginaustralia.com','salesforce.admin@virginaustralia.com'});
			mailError.setReplyTo('commercialalliancesystems@VirginAustralia.com');
			mailError.setSenderDisplayName('salesforce.admin@virginaustralia.com');
			mailError.setSubject('Batch Process Failed Responsys Status Batch  ');
			mailError.setPlainTextBody('Batch Process Failed '); 
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailError });

		}	



		Messaging.SingleEmailMessage mailfinish = new Messaging.SingleEmailMessage();
		mailfinish.setToAddresses(new String[] {'commercialalliancesystems@virginaustralia.com','salesforce.admin@virginaustralia.com' });
		mailfinish.setReplyTo('commercialalliancesystems@VirginAustralia.com');
		mailfinish.setSenderDisplayName(' Responsys Status, Batch Processing ');
		mailfinish.setSubject('RESPONSYS STATUS CHECK BATCH HAS FINISHED ');
		//mailfinish.setPlainTextBody(' -- TEST --  Responsys Batch test in Dev 2 Sandbox --TEST -- '  );
		mailfinish.setPlainTextBody(  
				'SALESFORCE ADMINISTRATOR, Please note that the Responsys Status Batch has ran.  If any CONTACTS who are CAMPAIGN MEMBERS were reported by Responsys as a HARD BOUNCE or SPAM or as an OPT OUT they will be listed here  ---->     '  + notifylist


				);

		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailfinish });

	}

	//Finishing the batch  (Or though this is not executiong code this methos needs to exsist for the batch class to run!!)  
	global void finish(Database.BatchableContext BC){

	}
}