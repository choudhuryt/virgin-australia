public with sharing class TopEcomGuestImpactingController {

    
  
   //Set&Get Lists
  // private ApexPages.StandardController controller {get; set;}
   public List<TopEcomGuestImpacting__c> searchResults {get;set;}
  

    

    //Set&Get Lists
     public integer TopEcomFlag {get; set;} 
     
     
     private  TopEcomGuestImpacting__c a;

    // Constructor
    
    public TopEcomGuestImpactingController() 
    {
      //  a=(TopEcomGuestImpacting__c)myController.getrecord();
        TopEcomFlag =0;
       
    
    }
 
    //pre-processing prior to Page load
     public PageReference initTopEcom() 
     {
    
      searchResults= [
        SELECT 	Impact_Rating_01__c, Impact_Rating_02__c,Impact_Rating_03__c,Impact_Rating_04__c,CreatedById, CreatedDate, IsDeleted, EcomGuestItem_1__c, 
        		Impact_Rating_05__c,Impact_Rating_006__c,Impact_Rating_07__c,Impact_Rating_08__c,EcomGuestItem_10__c, EcomGuestItem_2__c, EcomGuestItem_3__c, EcomGuestItem_4__c, EcomGuestItem_5__c,
         		Impact_Rating_09__c,Impact_Rating_10__c,EcomGuestItem_6__c, EcomGuestItem_7__c, EcomGuestItem_8__c, EcomGuestItem_9__c, LastModifiedById, 
        		LastModifiedDate, OwnerId, Id, SystemModstamp, Name, exist__c, ImpactRating1__c,ImpactRating2__c,ImpactRating3__c,
        		ImpactRating4__c,ImpactRating5__c,ImpactRating6__c,ImpactRating7__c,ImpactRating8__c,ImpactRating9__c,ImpactRating10__c
        FROM    TopEcomGuestImpacting__c
                // where contract__r.id = :ApexPages.currentPage().getParameters().get('id')
        WHERE   exist__c = True
               ORDER BY CreatedDate];
      
      
     		if(searchResults.size()>0)
    			 {
     			  TopEcomFlag=1;
      			 }
    
      return null;
 
   }
   

  public PageReference save() {

    upsert searchResults;
     
    PageReference thePage = new PageReference('/apex/TopEcomGuestImpacting');
                  
                  thePage.setRedirect(true);
                  
                  return thePage;
 
  }
  
   
  public PageReference qsave() {
   
      upsert searchResults;
      
      PageReference thePage = new PageReference('/apex/TopEcomGuestImpacting');
                  
                  thePage.setRedirect(true);
                  
                  return thePage;
        
    }
  
  
  public PageReference cancel() {
   // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
   PageReference thePage = new PageReference('/apex/TopEcomGuestImpacting');
                  
                  thePage.setRedirect(true);
                  
                  return thePage;
  }
      


  public PageReference newEcom() 
  {
    
   if (searchResults.size()<1)
      {
        searchResults.add
        (
                new TopEcomGuestImpacting__c
                  (
                   // Contract__c = ApexPages.currentPage().getParameters().get('id'),
                    EcomGuestItem_1__c  = '', 
                    EcomGuestItem_2__c  = '', 
                    EcomGuestItem_3__c  = '', 
                    EcomGuestItem_4__c  = '', 
                    EcomGuestItem_5__c  = '', 
                    EcomGuestItem_6__c  = '', 
                    EcomGuestItem_7__c  = '', 
                    EcomGuestItem_8__c  = '', 
                    EcomGuestItem_9__c  = '', 
                    EcomGuestItem_10__c = '',
                    ImpactRating1__c = 1,
                    ImpactRating2__c = 1,
                    ImpactRating3__c = 1,
                    ImpactRating4__c = 1,
                    ImpactRating5__c = 1,
                    ImpactRating6__c = 1,
                    ImpactRating7__c = 1,
                    ImpactRating8__c = 1,
                    ImpactRating9__c = 1,
                    ImpactRating10__c = 1,
                   
                    exist__c = True
                   )                  
         );
                    
                  
                    return null;
     }
    else{
  
        //     PageReference thePage = new PageReference('/apex/DomesticDiscountPage?id=' + a.Id);
        //     thePage.setRedirect(true);
        //     return thePage;
        return null; ///yes
         }
  }


  public PageReference remEcom() 
  {
        TopEcomGuestImpacting__c toDelete = searchResults[searchResults.size()-1];
            
        delete toDelete;
        
         PageReference thePage = new PageReference('/apex/TopEcomGuestImpacting');
                  
                  thePage.setRedirect(true);
                  
                  return thePage;
 
//return true;
  }
}