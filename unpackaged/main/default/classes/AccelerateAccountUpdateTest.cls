/**
* @description       : Test Class for AccelerateAccountUpdate
* @Created By         : Cloudwerx
* @Updated By         : Cloudwerx
**/
@isTest
private class AccelerateAccountUpdateTest {
    public static Id contractSmartFlyRecordTypeId = Utilities.getRecordTypeId('Contract', 'SmartFly');
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        testAccountObj.AccountSource = 'Smartfly';
        testAccountObj.Business_Number__c = 'ABC';
        testAccountObj.FC_Store_Name__c = 'fcStore';
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj.RecordTypeId = contractSmartFlyRecordTypeId;
        INSERT testContractObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        INSERT testContactObj;
        
        Velocity_Status_Match__c testVelocityStatusMatchObj = TestDataFactory.createTestVelocityStatusMatch(testAccountObj.Id, testContractObj.Id, 'Yes', 'Activated', Date.today(), 'some', 'Velocity Status Upgrade');
        testVelocityStatusMatchObj.RecordTypeId = Utilities.getRecordTypeId('Velocity_Status_Match__c', 'Velocity_Upgrade');
        testVelocityStatusMatchObj.Passenger_Velocity_Number__c = '9878786756';
        testVelocityStatusMatchObj.Passenger_Email__c = testContactObj.Email;
        INSERT testVelocityStatusMatchObj;
    }
    
    @isTest
    private static void testAccelerateAccountUpdate() {
        Account accObj = [SELECT Id, Lounge__c, Lounge_First_Name__c, Lounge_Last_Name__c, Lounge_Email__c, 
                          BillingStreet, BillingCity, BillingPostalCode, BillingState, Pilot_Gold_Position__c, 
                          Pilot_Gold_First_Name__c, Pilot_Gold_Last_Name__c, Pilot_Gold_Velocity_Number__c, 
                          Pilot_Gold_Email__c, Pilot_Gold_Position_Second_Nominee__c, 
                          Pilot_Gold_Email_Second_Nominee__c, Pilot_Gold_First_Name_Second_Nominee__c, 
                          Pilot_Gold_last_Name_Second_Nominee__c, Pilot_Gold_Velocity_Gold_Second_Nominee__c 
                          FROM Account LIMIT 1]; 
        accObj.Lounge__c = true;
        accObj.Lounge_First_Name__c = accObj.Id + '_Lounge_First_Name';
        accObj.Lounge_Last_Name__c = accObj.Id + '_Lounge_Last_Name';
        accObj.Lounge_Email__c = 'test_' + accObj.Id + '_LoungeEmail@gmail.com';
        accObj.Pilot_Gold_Position__c =  accObj.Id + '_PilotGoldPosition';
        accObj.Pilot_Gold_First_Name__c = accObj.Id + '_PilotGoldFirstName';
        accObj.Pilot_Gold_Last_Name__c = accObj.Id + '_PilotGoldLastName';
        accObj.Pilot_Gold_Velocity_Number__c = '0123456789';
        accObj.Pilot_Gold_Email__c = 'test_' + accObj.Id + '_PilotGoldEmail@gmail.com';
        accObj.Pilot_Gold_Email_Second_Nominee__c = accObj.Id + '_PilotGoldEmailSecondNominee@gmail.com';
        accObj.Pilot_Gold_Position_Second_Nominee__c = accObj.Id + '_PilotGPositionSecNom';
        accObj.Pilot_Gold_First_Name_Second_Nominee__c = accObj.Id + '_PilotGFrstNameSecNom';
        accObj.Pilot_Gold_last_Name_Second_Nominee__c = accObj.Id + '_PilotGlstNameSecNomi';
        accObj.Pilot_Gold_Velocity_Gold_Second_Nominee__c = '9876543210';
        UPDATE accObj;
        
        Test.startTest();
        AccelerateAccountUpdate.ProcessAccelerateAccount(new List<Account>{accObj});
        Test.stopTest();

        // Asserts
        List<Contact> createdContacts = [SELECT Id, FirstName, LastName, Email, OwnerId FROM Contact];
        // Asserts
        System.assertEquals(2, createdContacts.size());
        System.assertEquals(accObj.Lounge_First_Name__c.toLowercase(), createdContacts[1].FirstName);
        System.assertEquals(accObj.Lounge_Last_Name__c.toLowercase(), createdContacts[1].LastName);
        System.assertEquals(accObj.Lounge_Email__c.toLowercase(), createdContacts[1].Email);
        System.assertEquals(AccelerateAccountUpdate.vaUserId, createdContacts[1].OwnerId);
        List<Contract> contracts = [SELECT Id, AccountId FROM Contract WHERE AccountId =:accObj.Id];
        List<SIP_Header__c> sipHeaders = [SELECT Id, Contract__c, Description__c FROM SIP_Header__c];
        System.assertEquals(true, sipHeaders.size() > 0);
        System.assertEquals(contracts[1].Id, sipHeaders[0].Contract__c);
        System.assertEquals('Accelerate - All Revenue', sipHeaders[0].Description__c);
        List<SIP_Rebate__c> sipRebates = [SELECT Id, SIP_Header__c, Lower_Percent__c, Upper_Percent__c, SIP_Percent__c FROM SIP_Rebate__c];
        System.assertEquals(6, sipRebates.size());
        System.assertEquals(sipHeaders[0].Id, sipRebates[0].SIP_Header__c);
        System.assertEquals(2, sipRebates[0].Lower_Percent__c);
        System.assertEquals(2, sipRebates[0].Upper_Percent__c);
        System.assertEquals(2, sipRebates[0].SIP_Percent__c);
    }
    
    @isTest
    private static void testAccelerateAccountUpdateException() {
        String exceptionMessage = '';
        Test.startTest();
        try {
            AccelerateAccountUpdate.ProcessAccelerateAccount(new List<Account>());
        } catch(Exception exp) {
            exceptionMessage = exp.getMessage();
        }
        Test.stopTest();
        // Asserts
        System.assertEquals(true,  String.isNotBlank(exceptionMessage));
    }
}