@isTest
private class TestContractAddendumMasterTrigger

{
    static testMethod void myUnitTest() 
    {
        Account account         = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';
        
        insert account;
        
        Contract contractOld = new Contract();        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.Cos_Version__c ='16.0';
        contractOld.Red_Circle__c = true;
        insert contractOld;
        
        Contract_Addendum__c conadd = new Contract_Addendum__c();
        conadd.Contract__c = contractOld.Id ;
        conadd.Cos_Version__c = '16.0';
        conadd.Domestic_Requested_Tier__c = 'Tier 1';
        conadd.Regional_Requested_Tier__c = 'Tier 1';
        conadd.Trans_Tasman_Requested_Tier__c = 'Tier 1';
        conadd.INT_short_Haul_Requested_Tier__c = 'Tier 1';
        conadd.North_America_Requested_Tier__c = 'Tier 1';
        conadd.Africa_requested_Tier__c = 'Tier 1';
        conadd.Middle_East_Requested_Tier__c = 'Tier 1';
        conadd.UK_Europe_Requested_Tier_QR__c = 'Tier 1';
        conadd.UK_Europe_Requested_Tier_SQ__c = 'Tier 1';
        conadd.Red_Circle__c =false ;    
        conadd.Status__c = 'Draft' ;
        conadd.Update_Discounts_On_Contract__c = 'No' ;
        conadd.Lounge_Scheme_Selected__c = true;
        conadd.Lounge_Membership_Annual_Fee__c = 399;
        insert conadd;
                        
        Additional_Benefits__c addBen = new Additional_Benefits__c();
        addBen.Contract__c = contractOld.Id;
        addBen.Membership_Fee__c = 100;
        Insert addBen;
        
        Proposal_Table__c domdiscounts1 = new Proposal_Table__c(); 
        domdiscounts1.Contract__c =  contractOld.Id ;
        domdiscounts1.Discount_Off_Published_Fare__c = 10 ;
        domdiscounts1.Eligible_Fare_Type__c= 'Business';
        domdiscounts1.VA_Eligible_Booking_Class__c = 'J';
        domdiscounts1.Name = 'DOM Mainline';  
        insert  domdiscounts1;  
                        
        Proposed_Discount_Tables__c domdiscounts = new Proposed_Discount_Tables__c(); 
        domdiscounts.Contract_Addendum__c =  conadd.Id;
        domdiscounts.Contract__c   =  contractOld.Id ;
        domdiscounts.Discount_Off_Published_Fare__c = 10 ;
        domdiscounts.Eligible_Fare_Type__c= 'Business';
        domdiscounts.VA_Eligible_Booking_Class__c = 'J';
        domdiscounts.Name = 'DOM Mainline';  
        insert  domdiscounts;  
                                        
        Contract_Extension__c cf = TestUtilityClass.createTestContractExtension(account.Id, contractOld .Id);
        insert cf;
        
        conadd.Contract_Extension__c = cf.id ; 
        conadd.Update_Discounts_On_Contract__c = 'Yes' ;
        conadd.Status__c = 'Approved By VA' ;
        update conadd ;                 
    }
    
    static testMethod void myUnitTest1(){
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';        
        insert account;
        
        Contract contractOld = new Contract();        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.Cos_Version__c ='16.0';
        contractOld.Red_Circle__c = true;
        insert contractOld;
        
        Contract_Addendum__c conadd = new Contract_Addendum__c();
        conadd.Contract__c = contractOld.Id ;
        conadd.Cos_Version__c = '16.0';
        conadd.Domestic_Requested_Tier__c = 'Tier 1';
        conadd.Regional_Requested_Tier__c = 'Tier 1';
        conadd.Trans_Tasman_Requested_Tier__c = 'Tier 1';
        conadd.INT_short_Haul_Requested_Tier__c = 'Tier 1';
        conadd.North_America_Requested_Tier__c = 'Tier 1';
        conadd.Africa_requested_Tier__c = 'Tier 1';
        conadd.Middle_East_Requested_Tier__c = 'Tier 1';
        conadd.UK_Europe_Requested_Tier_QR__c = 'Tier 1';
        conadd.UK_Europe_Requested_Tier_SQ__c = 'Tier 1';
        conadd.Red_Circle__c =false ;    
        conadd.Status__c = 'Draft' ;
        conadd.Update_Discounts_On_Contract__c = 'No' ;
        conadd.Lounge_Scheme_Selected__c = true;
        conadd.Lounge_Membership_Annual_Fee__c = 399;
        insert conadd;
        
        Red_Circle_Discounts__c rcdiscounts1 = new Red_Circle_Discounts__c(); 
        rcdiscounts1.Contract__c =  contractOld.Id ;
        rcdiscounts1.Discount_Off_Published_Fare__c = 10 ;
        rcdiscounts1.Eligible_Fare_Type__c= 'Business';
        rcdiscounts1.VA_Eligible_Booking_Class__c = 'J';
        rcdiscounts1.Name = 'DOM Mainline';  
        insert  rcdiscounts1;
        
        Proposed_Red_Tables__c rcdiscounts = new Proposed_Red_Tables__c(); 
        rcdiscounts.Contract_Addendum__c =  conadd.Id;
        rcdiscounts.Contract__c   =  contractOld.Id ;
        rcdiscounts.Discount_Off_Published_Fare__c = 10 ;
        rcdiscounts.Eligible_Fare_Type__c= 'Business';
        rcdiscounts.VA_Eligible_Booking_Class__c = 'J';
        rcdiscounts.Name = 'DOM Mainline';  
        insert rcdiscounts;
        
        List<Market__c> marketList = new List<Market__c>();
        for(integer i=0; i<9; i++){
            Market__c mar = new Market__c();
            if(i==0){
                mar.Name = 'DOM Mainline';
                mar.Tier__c = 'Tier 2';
            }else if(i == 1){
                mar.Name = 'DOM Regional';
                mar.Tier__c = 'Tier 3';
            }else if(i == 2){
                mar.Name = 'INT Short Haul';
                mar.Tier__c = 'Tier 4';
            }else if(i == 3){
                mar.Name = 'Trans Tasman';
                mar.Tier__c = 'Tier 5';
            }else if(i == 4){
                mar.Name = 'North America';
                mar.Tier__c = 'Tier 6';
            }else if(i == 5){
                mar.Name = 'Africa';
                mar.Tier__c = 'Tier 7';
            }else if(i == 6){
                mar.Name = 'Middle East QR';
                mar.Tier__c = 'Tier 8';
            }else if(i == 7){
                mar.Name = 'UK/Europe QR';
                mar.Tier__c = 'Tier 9';
            }else if(i == 8){
                mar.Name = 'UK/Europe SQ';
                mar.Tier__c = 'Tier 10';
            }
            mar.Contract__c = contractOld.Id;
            marketList.add(mar);
        }
        Insert marketList;
        
        conadd.Update_Discounts_On_Contract__c = 'Yes' ;
        update conadd;
    }    
}