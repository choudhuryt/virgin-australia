public class ContractExtensionTriggerHandler
{

    public static void setLevel3Manager( Boolean isInsert, List<Contract_Extension__c> CEInTrigger, Map<Id,Contract_Extension__c> oldCEMap )
  {
       Set<Id> ownerIDs  = new Set<Id>();
       Set<Id> level3IDs = new Set<Id>();
    
    List<Contract_Extension__c> affectedCE = new List<Contract_Extension__c>();
      
    for( Contract_Extension__c ce: CEInTrigger) 
      {
        affectedce.add(ce);
        ownerIDs.add(ce.OwnerID__c);
      }
        
      
   
    system.debug('The owner id is ' +ownerIDs  + affectedce );
    
    List <User> u = [SELECT Id, contract_approver__r.Id, contract_approver__r.Level__c, contract_approver__r.ManagerId
                  FROM User WHERE Id In :ownerIDs];
    
    List<Contract_Extension__c> CEToUpdate = new List<Contract_Extension__c>();
    
    
     for(Contract_Extension__c ce : affectedce)
    {
        if (u.size() > 0)
        {    
    	
            
        if  ( u[0].contract_approver__r.Level__c  > 2 )
        {
         ce.Level3_Manager__c    = u[0].contract_approver__r.Id; 
        }
            else
        {
         ce.Level3_Manager__c    = u[0].contract_approver__r.ManagerId;    
        }
        
        }    
    	CEToUpdate.add(ce); 
        
    }
  }
    
   public static void setMaxExtensionVersion( Boolean isInsert, List<Contract_Extension__c> CEInTrigger, Map<Id,Contract_Extension__c> oldCEMap )
  {
        Set<Id> ContractIDs  = new Set<Id>();
        integer maxvalue ;
        List<Contract> ContractToUpdate = new List<Contract>();
        List<Contract> affectedContract = new List<Contract>();
        List<Contract_Extension__c> affectedCE = new List<Contract_Extension__c>();
        for( Contract_Extension__c ce: CEInTrigger) 
        {
           if(ce.Status__c == 'Draft')
           {
           affectedce.add(ce); 
           ContractIDs.add(ce.Contract__c);
           }
        }

       affectedContract = [SELECT Id, Max_Extension_Version__c  FROM Contract  WHERE Id =: ContractIDs];
    
      
        List<aggregateResult> maxverlist =[SELECT Max(Extension_Version__c)maxver  
                                                           FROM Contract_Extension__c 
                                                           where Contract__c = : ContractIDs
                                                           and Status__c not in ('Rejected', 'Deactivated') ]; 
      
      
        if (maxverlist.size() > 0)
     {    
     for(AggregateResult ag :maxverlist)
     {
       maxvalue     =   Integer.valueOf(ag.get('maxver')==null?0:ag.get('maxver'));
     }
     }
    
         for(Contract c: affectedContract)
       { 
           c.Max_Extension_Version__c = maxvalue ;
           ContractToUpdate.add(c);  
       }
      
      update ContractToUpdate ;
  }
}