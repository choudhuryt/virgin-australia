global class SchedulePaymentBatch implements Schedulable{

	global void execute(SchedulableContext sc) {
	
	PaymentBatch paymentBatch = new PaymentBatch();
    Database.executeBatch(paymentBatch, 1); 
	    
	} 

}