@isTest
public class TestInternationalSQDiscountController_v3
{
    static testMethod void myUnitTest1()
    {
        // TO DO: implement unit test
        
        
        //setup account
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
  	
  			
        insert account;
 
        Contract contractOld = new Contract();
        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.RecordTypeid = '012900000007LFZAA2';
        contractOld.Cos_Version__c = '14.4';
        contractOld.VA_SQ_Requested_Tier__c = '6';
        contractOld.Red_Circle__c =false;
        contractOld.EUONDSelected__c = 'Adelaide-Amsterdam';
        contractOld.SQNAONDSelected__c = 'Adelaide-Guangzhou';
        contractOld.SQWAAONDSelected__c = 'Adelaide-Ahmedabad';        
        contractOld.SQSEAONDSelected__c = 'Adelaide-Bandung';
        contractOld.SQSEAOND__c = 'Adelaide-Bandung';
        contractOld.SQEUOND__c = 'Adelaide-Amsterdam';
        contractOld.SQWAAOND__c = 'Adelaide-Ahmedabad'; 
        contractOld.SQNAOND__c =  'Adelaide-Guangzhou';
        contractOld.status = 'Draft';
        contractOld.Virgin_Australia_Amount__c = 1;
        contractOld.Intra_WA_Amount__c =1;
        insert contractOld;
        
                 International_Discounts_SQ__c  SQdiscountEU = new  International_Discounts_SQ__c();
                 SQdiscountEU.Tier__c ='6';
                 SQdiscountEU.Is_Template__c =false;
                 SQdiscountEU.Cos_Version__c  ='14.4';        			
                    
                SQdiscountEU.SQ_DiscOffPublishFare_01__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountEU.SQ_DiscOffPublishFare_15__c = 0;
    SQdiscountEU.SQ_DiscOffPublishFare_16__c = 0;    
                SQdiscountEU.Region__c = 'EUROPE' ;
                insert SQdiscountEU;
        		
//
    
               International_Discounts_SQ__c  SQdiscountAF = new  International_Discounts_SQ__c();
                 SQdiscountAF.Tier__c ='3';
                 SQdiscountAF.Is_Template__c =false;
                 SQdiscountAF.Cos_Version__c  ='14.4';        			
                    
                SQdiscountAF.SQ_DiscOffPublishFare_01__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountAF.SQ_DiscOffPublishFare_15__c = 0;   
    SQdiscountAF.SQ_DiscOffPublishFare_16__c = 0;     
    SQdiscountAF.Region__c = 'AFRICA' ;
    insert SQdiscountAF;
 //
        
                International_Discounts_SQ__c  SQdiscountAS = new  International_Discounts_SQ__c();
                SQdiscountAS.Tier__c ='3';
                SQdiscountAS.Is_Template__c =false;
                SQdiscountAS.Cos_Version__c  ='14.4';        			
                    
                SQdiscountAS.SQ_DiscOffPublishFare_01__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountAS.SQ_DiscOffPublishFare_15__c = 0;  
    SQdiscountAS.SQ_DiscOffPublishFare_16__c = 0;     
    SQdiscountAS.Region__c = 'ASIA' ;
    insert SQdiscountAS;

    International_Discounts_SQ__c  SQdiscountNA = new  International_Discounts_SQ__c();
    SQdiscountNA.Tier__c ='3';
    SQdiscountNA.Is_Template__c =false;
    SQdiscountNA.Cos_Version__c  ='14.4';        			
                    
    SQdiscountNA.SQ_DiscOffPublishFare_01__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountNA.SQ_DiscOffPublishFare_15__c = 0;         
    SQdiscountNA.SQ_DiscOffPublishFare_16__c = 0;
    SQdiscountNA.Region__c = 'NORTH ASIA' ;
    insert SQdiscountNA;
        

    International_Discounts_SQ__c  SQdiscountSEA = new  International_Discounts_SQ__c();
    SQdiscountSEA.Tier__c ='3';
    SQdiscountSEA.Is_Template__c =false;
    SQdiscountSEA.Cos_Version__c  ='14.4';        			
                    
    SQdiscountSEA.SQ_DiscOffPublishFare_01__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountSEA.SQ_DiscOffPublishFare_15__c = 0;         
    SQdiscountSEA.SQ_DiscOffPublishFare_16__c = 0; 
    SQdiscountSEA.Region__c = 'SOUTH EAST ASIA' ;
    insert SQdiscountSEA;
        
        
             
    International_Discounts_SQ__c  SQdiscountWAA= new  International_Discounts_SQ__c();
    SQdiscountWAA.Tier__c ='3';
    SQdiscountWAA.Is_Template__c =false;
    SQdiscountWAA.Cos_Version__c  ='14.4';        			
                    
    SQdiscountWAA.SQ_DiscOffPublishFare_01__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountWAA.SQ_DiscOffPublishFare_15__c = 0;         
    SQdiscountWAA.SQ_DiscOffPublishFare_16__c = 0;  
    SQdiscountWAA.Region__c = 'WEST ASIA AFRICA' ;
    insert SQdiscountWAA;

   
  

        
       PageReference pref = Page.InternationalDiscountPage_v5;
       pref.getParameters().put('id', contractOld.id);
       Test.setCurrentPage(pref);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(contractOld);
        InternationalDiscountController_v3 lController = new InternationalDiscountController_v3(conL);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conX = new ApexPages.StandardController(contractOld);
        InternationalDiscountEYController_v4 xController = new InternationalDiscountEYController_v4(conX);
          
        Test.startTest();
        PageReference  ref1 = Page.InternationalDiscountPage_v5;
        ref1 = lController.initDisc();
        
        ref1 = lController.newSQEU();         
        ref1 = lController.newSQAF(); 
        ref1 = lController.newSQAS();
              
        
        ref1 = lController.save();  
        ref1 = lController.qsave(); 
        ref1 = lController.cancel();
        
       
        ref1 = lController.remSQEU();
        ref1 = lController.remSQAF();
        ref1 = lController.remSQAS();


       PageReference  ref2 = Page.InternationalDiscountPage_v6;
        ref2 = lController.initDisc();
        ref2 = lController.save();  
        ref2 = lController.qsave(); 
        ref2 = lController.cancel();
        
       /* ref2 = lController.newSQEU();         
        ref2 = lController.newSQAF(); 
        ref2 = lController.newSQAS();
        ref2 = lController.newSQWAA();         
        ref2 = lController.newSQSEA(); 
        ref2 = lController.newSQNA(); 
        ref2 = lController.remSQEU();
        ref2 = lController.remSQAS();
        ref2 = lController.remSQNA();
        ref2 = lController.remSQWAA();
        ref2 = lController.remSQSEA();*/ 
       

        test.stopTest();
        
        
    }

    static testMethod void myUnitTest2()
    {
        // TO DO: implement unit test
        
        
        //setup account
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
  	
  			
        insert account;
 
        Contract contractOld = new Contract();
        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.RecordTypeid = '012900000007LFZAA2';
        contractOld.Cos_Version__c = '14.4';
        contractOld.VA_SQ_Requested_Tier__c = '3';
        contractOld.Red_Circle__c =false;
        contractOld.EUONDSelected__c = 'Adelaide-Amsterdam';
        contractOld.SQNAONDSelected__c = 'Adelaide-Guangzhou';
        contractOld.SQWAAONDSelected__c = 'Adelaide-Ahmedabad';        
        contractOld.SQSEAONDSelected__c = 'Adelaide-Bandung';
        contractOld.SQSEAOND__c = 'Adelaide-Bandung';
        contractOld.SQEUOND__c = 'Adelaide-Amsterdam';
        contractOld.SQWAAOND__c = 'Adelaide-Ahmedabad'; 
        contractOld.SQNAOND__c =  'Adelaide-Guangzhou';
        contractOld.status = 'Draft';
        contractOld.Virgin_Australia_Amount__c = 1;
        contractOld.Intra_WA_Amount__c =1;
        insert contractOld;
        
                 International_Discounts_SQ__c  SQdiscountEU = new  International_Discounts_SQ__c();
                 SQdiscountEU.Tier__c ='3';
                 SQdiscountEU.Is_Template__c =TRUE;
                 SQdiscountEU.Cos_Version__c  ='14.4';        			
                    
                SQdiscountEU.SQ_DiscOffPublishFare_01__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountEU.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountEU.SQ_DiscOffPublishFare_16__c = 0;      
    SQdiscountEU.SQ_DiscOffPublishFare_15__c = 0;         
    SQdiscountEU.Region__c = 'EUROPE' ;
    insert SQdiscountEU;
        		
//
    
    International_Discounts_SQ__c  SQdiscountAF = new  International_Discounts_SQ__c();
    SQdiscountAF.Tier__c ='3';
    SQdiscountAF.Is_Template__c =TRUE;
    SQdiscountAF.Cos_Version__c  ='14.4';        			
                    
    SQdiscountAF.SQ_DiscOffPublishFare_01__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountAF.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountAF.SQ_DiscOffPublishFare_15__c = 0;         
    SQdiscountAF.SQ_DiscOffPublishFare_16__c = 0;
    SQdiscountAF.Region__c = 'AFRICA' ;
    insert SQdiscountAF;
 //
        
  International_Discounts_SQ__c  SQdiscountAS = new  International_Discounts_SQ__c();
  SQdiscountAS.Tier__c ='3';
  SQdiscountAS.Is_Template__c =TRUE;
  SQdiscountAS.Cos_Version__c  ='14.4';        			
                    
    SQdiscountAS.SQ_DiscOffPublishFare_01__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountAS.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountAS.SQ_DiscOffPublishFare_15__c = 0;         
    SQdiscountAS.Region__c = 'ASIA' ;
    insert SQdiscountAS;

    International_Discounts_SQ__c  SQdiscountNA = new  International_Discounts_SQ__c();
    SQdiscountNA.Tier__c ='3';
    SQdiscountNA.Is_Template__c =true;
    SQdiscountNA.Cos_Version__c  ='14.4';        			
                    
    SQdiscountNA.SQ_DiscOffPublishFare_01__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountNA.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountNA.SQ_DiscOffPublishFare_15__c = 0;         
    SQdiscountNA.SQ_DiscOffPublishFare_16__c = 0; 
    SQdiscountNA.Region__c = 'NORTH ASIA' ;
    insert SQdiscountNA;
        

    International_Discounts_SQ__c  SQdiscountSEA = new  International_Discounts_SQ__c();
    SQdiscountSEA.Tier__c ='3';
    SQdiscountSEA.Is_Template__c =true;
    SQdiscountSEA.Cos_Version__c  ='14.4';        			
                    
    SQdiscountSEA.SQ_DiscOffPublishFare_01__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountSEA.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountSEA.SQ_DiscOffPublishFare_15__c = 0;         
    SQdiscountSEA.SQ_DiscOffPublishFare_16__c = 0; 
    SQdiscountSEA.Region__c = 'SOUTH EAST ASIA' ;
    insert SQdiscountSEA;
        
        
             
  International_Discounts_SQ__c  SQdiscountWAA= new  International_Discounts_SQ__c();
  SQdiscountWAA.Tier__c ='3';
  SQdiscountWAA.Is_Template__c =TRUE;
  SQdiscountWAA.Cos_Version__c  ='14.4';        			
                    
  SQdiscountWAA.SQ_DiscOffPublishFare_01__c = 0;
  SQdiscountWAA.SQ_DiscOffPublishFare_02__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_03__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_04__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_05__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_06__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_07__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_08__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_09__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_10__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_11__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_12__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_13__c = 0;
	SQdiscountWAA.SQ_DiscOffPublishFare_14__c = 0;  
    SQdiscountWAA.SQ_DiscOffPublishFare_15__c = 0;         
    SQdiscountWAA.SQ_DiscOffPublishFare_16__c = 0;
    SQdiscountWAA.Region__c = 'WEST ASIA AFRICA' ;
    insert SQdiscountWAA;

   
   
        
       PageReference pref = Page.InternationalDiscountPage_v5;
       pref.getParameters().put('id', contractOld.id);
       Test.setCurrentPage(pref);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(contractOld);
        InternationalDiscountController_v3 lController = new InternationalDiscountController_v3(conL);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conX = new ApexPages.StandardController(contractOld);
        InternationalDiscountEYController_v4 xController = new InternationalDiscountEYController_v4(conX);
          
        Test.startTest();
        PageReference  ref1 = Page.InternationalDiscountPage_v5;
        ref1 = lController.initDisc();
        
        ref1 = lController.newSQEU();         
        ref1 = lController.newSQAF(); 
        ref1 = lController.newSQAS();
        

       PageReference  ref2 = Page.InternationalDiscountPage_v6;
        ref2 = lController.initDisc();
        ref2 = lController.save();  
        ref2 = lController.qsave(); 
        ref2 = lController.cancel();
        
        
       /* ref2 = lController.newSQEU();         
        ref2 = lController.newSQAF(); 
        ref2 = lController.newSQAS();
        ref2 = lController.newSQNA();      
        ref2 = lController.newSQSEA();
        ref2 = lController.newSQWAA();         
        ref2 = lController.remSQEU();     
        ref2 = lController.remSQAS();
         ref2 = lController.remSQNA();
        ref2 = lController.remSQWAA();
        ref2 = lController.remSQSEA();*/


        test.stopTest();
        
        
    }

    
    
}