/** 
*  File Name       : VSM_Utills 
*
*  Description     : A central place for VSM calculations and tasks.  This class was created 
*                    initially to tally up available/remaining VSMs for a contract.  
*                      
*  Copyright       : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
*  @author         : Steve Kerr
*  Date            : 14th October 2015
*
*  Notes           : 
* UpdatedBy: cloudwerx (Did indentation)
**/ 
public with sharing class VSM_Utills {
    
    public class VSM_Totals {
        Map<String, Integer> availableMap = new Map<String, Integer>();
        Map<String, Integer> allocatedMap = new Map<String, Integer>();
        // Used for visualForce Pages
        public String ContractNumber = '';
        public String getContractNumber(){return ContractNumber;}
        public void setContractNumber(String contractId){ContractNumber = contractId;}
        public String ContractId = '';
        public String getContractId(){return ContractId;}
        public void setContractId(String conId){ContractId = conId;}
        public Integer getSilverAvailable(){return getAvailable('Silver');}
        public Integer getSilverUsed(){return getAllocated('Silver');}
        public Integer getSilverRemaining(){return getRemaining('Silver');}
        public Integer getPilotGoldAvailable(){return getAvailable('Pilot Gold');}
        public Integer getPilotGoldUsed(){return getAllocated('Pilot Gold');}
        public Integer getPilotGoldRemaining(){return getRemaining('Pilot Gold');}
        public Integer getGoldAvailable(){return getAvailable('Gold');}
        public Integer getGoldUsed(){return getAllocated('Gold');}
        public Integer getGoldRemaining(){return getRemaining('Gold');}
        public Integer getPlatinumAvailable(){return getAvailable('Platinum');}
        public Integer getPlatinumUsed(){return getAllocated('Platinum');}
        public Integer getPlatinumRemaining(){return getRemaining('Platinum');}
        public Integer getBeyondAvailable(){return getAvailable(label.Virgin_Australia_Beyond);}
        public Integer getBeyondUsed(){return getAllocated(label.Virgin_Australia_Beyond);}
        public Integer getBeyondRemaining(){return getRemaining(label.Virgin_Australia_Beyond);}
        
        /*
        * Sets the number available for a particular category
        */
        public void setAvailable(String cat, Integer noAvailable){
            if(noAvailable == null){
                noAvailable  = 0;
            }
            availableMap.put(cat, noAvailable);
        }
        
        /*
        * Adds 1 to a particular category, e.g. Gold
        */
        public void incrementAllocated(String cat){
            // Current Value
            Integer val = getAllocated(cat);
            // increment, and store back in the map
            val++;
            allocatedMap.put(cat, val);
        }
        
        
        /*
        * Gets the current value, or returns 0
        */
        public Integer getAvailable(String cat){
            Integer val = 0;
            if(availableMap.containsKey(cat)){
                val = availableMap.get(cat); 
            }
            return val;
        }
        
        /*
        * Gets the current value, or return 0
		*/
        public Integer getAllocated(String cat){
            Integer val = 0;
            if(allocatedMap.containsKey(cat)){
                val = allocatedMap.get(cat); 
            }  
            return val;
        }
        
        /*
        * Returns how many VSMs are remaining in this category
        * (Available - allocated)
        *
        */
        public Integer getRemaining(String cat){
            Integer val = getAvailable(cat) - getAllocated(cat);
            system.debug('getAvailable(cat)++->'+cat+'  '+getAvailable(cat)); 
            system.debug('getAllocated(cat)++->'+cat+'  '+getAllocated(cat)); 
            system.debug('val++->'+val); 
            return val;
        }
    }
    
    public static VSM_Utills.VSM_Totals getVSM_Totals(ID contractId){
        date  vsmstartdate ;
        VSM_Totals totals = new VSM_Totals();
        
        // Set the Available number from the contract
        List<Contract> contracts = [SELECT Id, CreatedDate,Silver__c, Pilot_Gold__c, Gold__c, Platinum__c, 
                                    ContractNumber, Virgin_Australia_Beyond__c,
                                    Velocity_Acceleration_Frequency__c
                                    FROM Contract
                                    WHERE Id = : contractId AND (EndDate >= today OR EndDate = null) AND Status in ('Activated')];
        if(contracts.size() > 0) {         
            Contract con = contracts[0];          
            if(con.Velocity_Acceleration_Frequency__c == 'Annual') {
                List<Velocity_Acceleration__c> VelAcc = [SELECT Id, Start_Date__c,Silver__c, Pilot_Gold__c, Gold__c, Platinum__c, Virgin_Australia_Beyond__c
                                                         FROM Velocity_Acceleration__c
                                                         WHERE Contract__c = : contractId
                                                         AND End_Date__c >= today AND Status__c ='Active' LIMIT 1];
                
                if(VelAcc.size() > 0 )   {
                    Velocity_Acceleration__c v = VelAcc[0]; 
                    vsmstartdate = VelAcc[0].Start_Date__c ;
                    totals.setAvailable('Silver', Integer.valueOf(v.Silver__c));
                    totals.setAvailable('Pilot Gold', Integer.valueOf(v.Pilot_Gold__c));
                    totals.setAvailable('Gold', Integer.valueOf(v.Gold__c));
                    totals.setAvailable('Platinum', Integer.valueOf(v.Platinum__c));  
                    totals.setAvailable(label.Virgin_Australia_Beyond, Integer.valueOf(v.Virgin_Australia_Beyond__c));
                    //totals.setAvailable('Virgin Australia Beyond', Integer.valueOf(v.Virgin_Australia_Beyond__c));
                }
            } else {
                totals.setAvailable('Silver', Integer.valueOf(con.Silver__c));
                totals.setAvailable('Pilot Gold', Integer.valueOf(con.Pilot_Gold__c));
                totals.setAvailable('Gold', Integer.valueOf(con.Gold__c));
                totals.setAvailable('Platinum', Integer.valueOf(con.Platinum__c));
                totals.setAvailable(label.Virgin_Australia_Beyond, Integer.valueOf(con.Virgin_Australia_Beyond__c)); 
                //totals.setAvailable('Virgin Australia Beyond', Integer.valueOf(con.Virgin_Australia_Beyond__c));
            }
            // Set Contract Id
            totals.setContractNumber(con.ContractNumber);
            totals.setContractId(con.Id + '');
            system.debug('totals>>'+totals);
        }                        
        List<Velocity_Status_Match__c> VSMs ;
        if (vsmstartdate <> null) { 
            VSMs = [SELECT Id, Status_Match_or_Upgrade__c 
                    FROM Velocity_Status_Match__c 
                    WHERE Contract__c = :contractId
                    AND Within_Contract__c = 'Yes'AND Approval_Status__c = 'Activated' AND Date_Requested__c  >  :vsmstartdate];
        } else {
            VSMs = [SELECT Id, Status_Match_or_Upgrade__c 
                    FROM Velocity_Status_Match__c 
                    WHERE Contract__c = :contractId
                    AND Within_Contract__c = 'Yes' AND Approval_Status__c = 'Activated'];   
        }
        // Count VSMs Allocated
        for(Velocity_Status_Match__c VSM : VSMs) {
            totals.incrementAllocated(VSM.Status_Match_or_Upgrade__c);
        }
        return totals;
    }
}