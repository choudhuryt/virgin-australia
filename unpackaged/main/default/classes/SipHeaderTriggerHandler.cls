public class SipHeaderTriggerHandler
{
public static void updatesippercentage (List<SIP_Header__c> sipInTrigger)
    {
        List<SIP_Header__c> lstSHUpdate = new List<SIP_Header__c>();
        List<SIP_Rebate__c> srlist  = new List<SIP_Rebate__c>();
        Set<id> sids  = new Set<id>()  ;   
         
      for( SIP_Header__c sipdata:sipInTrigger ) 
     { 
     sids.add(sipdata.id) ;         
     } 
        
      List<SIP_Header__c> sipList =  [select id,	Full_Last_Year_Actual_Revenue__c,Est_Current_Full_Year_Revenue__c,SIP_Percentage__c
                                      FROM SIP_Header__c  where 
                                      id = :sids  ];    
      if (siplist.size() > 0 )
      {
        srlist = [select id ,SIP_Header__c,SIP_Percent__c  from SIP_Rebate__c where  SIP_Header__c =:siplist[0].id and JH_Tier__c = 'Current Tier'];
      } 
       
            
       for(SIP_Header__c sh: sipList)
      {         
               if(srList.size() > 0 && (srList[0].SIP_Percent__c <> siplist[0].SIP_Percentage__c ) )  
            {  
              sh.SIP_Percentage__c =   srList[0].SIP_Percent__c ;
              lstSHUpdate.add(sh)  ;
            }
       }
        
       update lstSHUpdate;   
    }
}