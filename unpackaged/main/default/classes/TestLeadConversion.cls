/**
* @description       : Test class for LeadConversion
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestLeadConversion {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'super@gmail.com', 'Fry', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        INSERT testContactObj;
        
        Lead testLeadObj = TestDataFactory.createTestLead('Fry', 'Qantas Fry And Sons', 'super@test.com', 'GSO', 'Unqualified', 'Corporate', 'Hot', 'Travel', 'Agency', true, 'test@test.com', 'andy@test.com', 'andy@test.com', '1111111111', '3333333333', 'BLI', 'MAL', 'BUL', '3000', 'AU', 'Australia');
        INSERT testLeadObj;
    }
    
    @isTest
    private static void testLeadConversion() {
        Lead testLeadObj = [SELECT Id FROM Lead];
        Account testAccountObj = [SELECT Id FROM Account];
        Contact testContactObj = [SELECT Id FROM Contact];
        Test.startTest();
        Database.LeadConvert leadconvt = new Database.LeadConvert();
        leadconvt.setLeadId(testLeadObj.Id);
        leadconvt.setAccountId(testAccountObj.Id);
        leadconvt.setContactId(testContactObj.Id);
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        leadconvt.setConvertedStatus(convertStatus.MasterLabel);
        Database.LeadConvertResult lcr = Database.convertLead(leadconvt);
        Test.stopTest();
        //Asserts
		List<Contact> contacts = [SELECT Id FROM Contact];
        system.assert(lcr.isSuccess());
        system.assertEquals(0, contacts.size());
    }
}