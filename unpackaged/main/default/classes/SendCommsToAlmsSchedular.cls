/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 22.03.2022
**Description: Schedular Class to send notifications for the failed POST transactions
*/
global class SendCommsToAlmsSchedular implements Schedulable{
	global void execute(SchedulableContext sc){
        SendCommsToAlmsWS.sendNotification();
    }
}