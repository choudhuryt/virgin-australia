/**
* @description       : WaivorAndFavourWorkflow Trigger Handler
* @CreatedBy         : Cloudwerx
* @updatedBy         : Cloudwerx
**/
public class WaivorAndFavourWorkflowHandler {
    //@AddedBy : cloudwerx here we are updating waiverfavour ownerid
    public static void waiverFavourActivity(List<Waiver_Favour__c> waiverFavours) {
        // @updatedBy: cloudwerx Get the record ID for VISTA Waivers 
        Id waiverRecordTypeId = Utilities.getRecordTypeId('Waiver_Favour__c', 'Waiver_Favour_VISTA');
        //@updatedBy: cloudwerx removed the SOQL query from loop and added logic to get recordTypeId
        Set<Id> waiverAccountIds = new Set<Id>();
        Set<Id> waiverIds = new Set<Id>();
        for(Waiver_Favour__c waiverObj :waiverFavours) {
            // Skip if the flag isn't set or if it's a VISTA Waiver
            if(waiverObj.Notify_Account_Owner__c == true && waiverObj.RecordTypeid != waiverRecordTypeId) {
                waiverAccountIds.add(waiverObj.Account__c);
            }
        }
        
        Map<Id, Account> waiverAccountMap = new Map<Id, Account>([SELECT Id, OwnerId FROM Account WHERE Id IN :waiverAccountIds]);
        
        for(Waiver_Favour__c waiverObj: waiverFavours) {
            Id accountOwnerId = (waiverAccountMap != null && waiverAccountMap.containsKey(waiverObj.Account__c)) ? waiverAccountMap.get(waiverObj.Account__c)?.Ownerid : null;
            if(accountOwnerId != null) {
                waiverObj.ownerid = accountOwnerId;
                if(waiverObj.Status__c != null && waiverObj.Status__c.equals('Approved')) {
                    waiverIds.add(waiverObj.Id);
                }
            }
        }
        
        List<Waiver_Ticket_Details__c> waiverTicketDetails = [SELECT Id FROM Waiver_Ticket_Details__c WHERE Waiver_Favour__c IN :waiverIds];
        if(waiverTicketDetails.size() > 1) {
            UPDATE waiverTicketDetails;
        }
    }
}