/**
* @description       : Test class for CreateMeetingNotes
* @Updated By         : Cloudwerx
**/
@isTest
private class TestCreateMeetingNotes {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        INSERT testContactObj;
        
        Meeting_Notes__c testMeetingNotesObj = TestDataFactory.createTestMeetingNotes('This is test', 'Travel Policy', 'Test', 'This is test travel policy');
        INSERT testMeetingNotesObj;
        
        Event testEventObj = TestDataFactory.createTestEvent(testAccountObj.Id, testContactObj.Id, testContactObj.Id, 'Test', 10, testMeetingNotesObj.Id, System.now());
        INSERT testEventObj;
        
    }
    
    @isTest
    private static void testCreateMeetingNotesApexMessage() {
        Event testEventObj = [SELECT Id FROM Event];
        Test.startTest();
        PageReference pageRef = Page.CreateMeetingNotes;
        pageRef.getParameters().put('id', testEventObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController evenL = new ApexPages.StandardController(testEventObj);
        CreateMeetingNotes lController = new CreateMeetingNotes(evenL);
        
        //instantiate the DomesticDiscountController 
        ApexPages.StandardController evenX = new ApexPages.StandardController(testEventObj);
        CreateMeetingNotes xController = new CreateMeetingNotes(evenX);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference savePageReference = lController.save();
        PageReference cancelPageReference = lController.cancel();
        Test.stopTest();
        //Asserts
        system.assertEquals('/' + testEventObj.Id, savePageReference.getUrl());
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('There are meeting notes available. Please edit the same.', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.ERROR, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testCreateMeetingNotes() {
        Event testEventObj = [SELECT Id, Meeting_Notes__c, Account__c FROM Event];
        testEventObj.Meeting_Notes__c = null;
        UPDATE testEventObj;
        Test.startTest();
        PageReference pageRef = Page.CreateMeetingNotes;
        pageRef.getParameters().put('id', testEventObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController evenL = new ApexPages.StandardController(testEventObj);
        CreateMeetingNotes lController = new CreateMeetingNotes(evenL);
        
        //instantiate the DomesticDiscountController 
        ApexPages.StandardController evenX = new ApexPages.StandardController(testEventObj);
        CreateMeetingNotes xController = new CreateMeetingNotes(evenX);
        lController.newmeetnote = new Meeting_Notes__c(Market_Intelligence_Sub_Type__c = 'Test SubType',
                                                       Market_Intelligence_Type__c = 'Travel Policy', 
                                                       Market_Intelligence_Details__c = 'Market Intelligence Details');
        PageReference initDiscPageReference = lController.initDisc();
        PageReference savePageReference = lController.save();
        Test.stopTest();
        //Asserts
        system.assertEquals('/' + testEventObj.Id, savePageReference.getUrl());
        Data_Smart_Research__c insertedDataSmartResearchObj = [SELECT Id, Account__c, Subject__c, Sub_Category__c, Detail__c FROM Data_Smart_Research__c];
        system.assertEquals(true, insertedDataSmartResearchObj.Id != null);
        system.assertEquals(testEventObj.Account__c, insertedDataSmartResearchObj.Account__c);
        system.assertEquals(lController.newmeetnote.Market_Intelligence_Type__c, insertedDataSmartResearchObj.Subject__c);
        system.assertEquals(lController.newmeetnote.Market_Intelligence_Sub_Type__c, insertedDataSmartResearchObj.Sub_Category__c);
        system.assertEquals(lController.newmeetnote.Market_Intelligence_Details__c, insertedDataSmartResearchObj.Detail__c);
    }
    
    @isTest
    private static void testCreateMeetingNotesError() {
        Event testEventObj = [SELECT Id FROM Event];
        Test.startTest();
        PageReference pageRef = Page.CreateMeetingNotes;
        pageRef.getParameters().put('id', testEventObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController evenL = new ApexPages.StandardController(testEventObj);
        CreateMeetingNotes lController = new CreateMeetingNotes(evenL);
        
        //instantiate the DomesticDiscountController 
        ApexPages.StandardController evenX = new ApexPages.StandardController(testEventObj);
        CreateMeetingNotes xController = new CreateMeetingNotes(evenX);
        lController.newmeetnote = [SELECT Id FROM Meeting_Notes__c];
        PageReference savePageReference = lController.save();
        Test.stopTest();
        //Asserts
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals(true, String.isNotBlank(msg.getSummary()));
        }
    }
}