/*
 * Author: Warjie Malibago (Accenture CloudFirst)
 * Date: June 9, 2016
 * Description: Test class for TourCodeTrigger
*/
@isTest
private class TourCodeTrigger_Test {

	private static testMethod void test() {
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            insert acc;
            
            Contact con = TestUtilityClass.createTestContact(acc.Id);
            con.SmartFly_Contact_Status__c = 'key Contact';
            con.SmarftFly_Tourcode__c = '12345679';
            insert con;
            
            Contract cont = TestUtilityClass.createTestContract(acc.Id);
            cont.Type__c = 'Rebate';
            insert cont;
            
            Tourcodes__c tc = TestUtilityClass.createTestTourCodes(acc.Id, cont.Id);
            tc.Tourcode_Purpose__c = 'Smartfly Tourcode';
            insert tc;
            system.assertEquals('12345678', [SELECT SmarftFly_Tourcode__c FROM Contact WHERE Id =: con.Id].SmarftFly_Tourcode__c);
            
            AGYLogin__c agy = TestUtilityClass.createTestAGYLogin(acc.Id);
            insert agy;
            
            IATA_Number__c iata = TestUtilityClass.createTestIATANumber(acc.Id, cont.Id);
            insert iata;
            
            Data_Validity__c dv = TestUtilityClass.createTestDataValidity(acc.Id, agy.Id);
            dv.Tourcode__c = tc.Id;
            dv.IATA__c = iata.Id;
            dv.RecordTypeId = Schema.SObjectType.Data_Validity__c.getRecordTypeInfosByName().get('IATA Validity').getRecordTypeId();
            insert dv;
        }
	}
}