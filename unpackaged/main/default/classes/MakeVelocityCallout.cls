public with sharing class MakeVelocityCallout 
{

   
      

    public static void apexcallout(string conid,string velNum) 
   {
     
      String err ;
      String tier ;
      String velocityNumber = velNum;
      err = '';
      

 
      List<contact> lstConUpdate = new List<Contact>(); 
      
      GuestCaseVelocityInfoModel.ProfileInformationType profileInfo;
      GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType velocityDetails;
      GuestCaseVelocityInfoModel.MemberInformationType memberInfo;
      GuestCaseVelocityInfoModel.PersonType person;

    // Callout setup
      GuestCaseVelocityInfo.SalesforceLoyaltyService service = new GuestCaseVelocityInfo.SalesforceLoyaltyService();
      Apex_Callouts__c apexCallouts = Apex_Callouts__c.getValues('VelocityDetails');
      if(apexCallouts != null)
       {
        service.endpoint_x = apexCallouts.Endpoint_URL__c;
        service.timeout_x = Integer.valueOf(apexCallouts.Timeout_ms__c);
        service.clientCertName_x = apexCallouts.Certificate__c;
       }
    // Fetch details
    try {
      
      velocityDetails = service.GetLoyaltyDetails(velocityNumber);

      // Extract details
      profileInfo = velocityDetails.ProfileInformation;
      tier = profileInfo.TierLevel;
      memberInfo = velocityDetails.MemberInformation;
      person = memberInfo.Person;
      }catch(Exception e)
      {
            
      // Check if not found      
        if(e.getMessage().contains('does not correspond to a current Member'))
        {
        err = 'Please check the number.  Record not found';
        } 
       
      }   
        system.debug('The error :' + err + 'The Tier :' + tier)  ;
          
        if (err == '' && tier != null )
        {
          
        
         List<Contact> conlist =   [SELECT Id , Velocity_Status__c ,Velocity_Number__c FROM Contact WHERE id= :conid and Velocity_Number__c = :velNum ];
         system.debug('The conlist'+  conlist + conlist.size()  + tier ); 
            
            if (conlist.size() > 0)
            {
                 for(Contact con: conlist)
                 {
                     
                 if(tier.equals('R'))
                 {	
					con.Velocity_Status__c   ='Red';
				 }else if(tier.equals('S'))
                 {
					con.Velocity_Status__c  ='Silver';
				 }
				 else if(tier.equals('G'))
                 {
					con.Velocity_Status__c  ='Gold';
				 }else if(tier.equals('P'))
                 {
				    con.Velocity_Status__c  ='Platinum';
				 }else if(tier.equals('V'))
                 {
					con.Velocity_Status__c  ='VIP';
				 }    
                
                  lstConUpdate.add(con);
                 }
                 update lstConUpdate;
            }
           
        }
   }
}