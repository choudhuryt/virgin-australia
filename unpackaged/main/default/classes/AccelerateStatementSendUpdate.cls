global class AccelerateStatementSendUpdate implements Database.Batchable<SObject>
{
    
    //**************************************************************
    //
    
    

    global AccelerateStatementSendUpdate(){}

     
    global String gstrQuery = 'Select name,id,Business_Number__c, Statement_Generated__c   From Account where Statement_Generated__c = true  AND Account_Lifecycle__c  = \'Contract\'  and   RecordType.DeveloperName =\'Accelerate\'' ;
    global Integer howManyrecordsUpdated = 0;
   
    
    //Loading and running the query string
    global Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator(gstrQuery);
    }   

    //Running The execute Method
    global void execute(Database.BatchableContext BC,List<sObject> scope)
    {
        List<Account> accountList = new List <Account>(); 
        for (Sobject s : scope)
        {
            Account a = (Account)s;
            accountList.add(a);

        }  

        List<Account> accountListToUpdate = new List<Account>();
        for(Integer x =0; x<accountList.size(); x++){
            Account account = new Account();
            account = accountList.get(x);
            account.Statement_Generated__c =  false ;
            try{
                update account;
                
            }catch(Exception e){
                //VirginsutilitiesClass.sendEmailError(e);
            }

           }   
    }
          

     
       

  

    //Finishing the batch  (Or though this is not executiong code this methos needs to exsist for the batch class to run!!)  
    global void finish(Database.BatchableContext BC){
        
        

    }

}