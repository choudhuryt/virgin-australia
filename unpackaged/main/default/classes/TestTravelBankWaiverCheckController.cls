/*
 * 	Updated by Cloudwerx : Added parameters to TestUtilityClass.createTestTravelBankWaiverPoint at line 23
 * 
 */
@isTest
public class TestTravelBankWaiverCheckController
{

 static testMethod void TestTravelBankCompensationWaiver() 
     {
       User u = TestUtilityClass.createTestUser();
       insert u;      
       
      system.runAs(u)
      {
        Test.startTest();
        Account acc = TestUtilityClass.createTestAccount();      
        insert acc; 
          
        Waiver_Favour__c w1  = TestUtilityClass.createTestTravelBankCompensationWaiver(acc.Id, u.Id);          
        insert w1; 
          
        Waiver_Point__c wp1 =   TestUtilityClass.createTestTravelBankWaiverPoint(acc.Id, u.Id); 
        insert wp1;
          
       ApexPages.StandardController conL = new ApexPages.StandardController(w1);   
       TravelBankWaiverCheckController lController = new TravelBankWaiverCheckController(conL);
       PageReference tbwPage = new PageReference('/apex/TravelBankWaiverCheck');
       Test.setCurrentPage(tbwPage);      
       Test.stopTest();
      }   
     }
}