@isTest
private class TestUpdateContactTrigger
{
   
    
  static testMethod void testProcessContact() {
        // TO DO: implement unit test
        
        Test.startTest();
        List<Account> acctList = new List<Account>();
        Account accountToTest = new Account();
        accountToTest.Account_Type__c = 'Industry';
        accountToTest.Name = 'Test Name';
        accountToTest.Billing_Country_Code__c = 'AU';
        accountToTest.BillingStreet = 'Test Street';
        accountToTest.BillingState = 'AU';
        accountToTest.BillingPostalCode = '12345';
        accountToTest.BillingCountry = 'USA';
        accountToTest.BillingCity = 'Test City';
        accountToTest.Industry_Type__c = 'Hospitality & Tourism';
        accountToTest.Market_Segment__c = 'Accelerate';
        accountToTest.Lounge__c = TRUE;
        accountToTest.Pilot_Gold_Email__c = 'testgoldmail@yahoo.com';
        accountToTest.Pilot_Gold_Email_Second_Nominee__c = 'testgoldmailsec@yahoo.com';
        acctList.add(accountToTest);
        insert acctList;
        
        
        
        Contact contactTest = new Contact();
        contactTest.LastName = 'TestLast Contact';
        contactTest.FirstName  = 'TestFirst Contact';
        contactTest.Key_Contact__c  = TRUE;
        contactTest.Email = 'testContact@yahoo.com';        
        contactTest.Title = 'Test Title';
        contactTest.Phone = '123456';
        contactTest.Velocity_Number__c = '2105327372';
        contactTest.AccountId = accountToTest.Id;
        insert contactTest;  
        
        Test.setMock(WebServiceMock.class, new LoyaltyServiceMockImpl());
      
      
	 
		MakeVelocityCallout.apexcallout(contactTest.id,'2105327372');      
        Test.StopTest();
   

      
}
    

}