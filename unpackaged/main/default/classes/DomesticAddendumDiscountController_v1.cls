public  with sharing class DomesticAddendumDiscountController_v1 
{ 
    private ApexPages.StandardController controller {get; set;}
    
    public List<Proposed_Discount_Tables__c> searchResults {get;set;}
    public List<Proposed_Discount_Tables__c>  searchResults1 {get;set;} 
    
    public List<Proposed_Discount_Tables__c>  searchResultsTrans {get;set;}
    public List<Proposed_Discount_Tables__c>  searchResultsInt {get;set;}
    public List<Proposed_Discount_Tables__c>  searchResultsNA {get;set;}
    public List<Proposed_Discount_Tables__c>  searchResultsME {get;set;}
    public List<Proposed_Discount_Tables__c>  searchResultsAf {get;set;}
    public List<Proposed_Discount_Tables__c>  searchResultsUK_QR {get;set;}
    public List<Proposed_Discount_Tables__c>  searchResultsUK_SQ {get;set;}
    
    public List <Contract_Addendum__c> contractadd {get;set;}
    public Contract_Addendum__c conlist{get;set;}
    public Boolean iseditable {get;set;}
    public integer domFlag {get; set;} 
    public integer regFlag {get; set;}
    
    public integer iFlag {get; set;}
    public integer mFlag {get; set;}
    public integer nFlag {get; set;}
    public integer tFlag {get; set;}
    public integer aFlag {get; set;}
    public integer uQRFlag {get; set;}
    public integer uSQFlag {get; set;}
    
    public Boolean isUA {get;set;}
    public Boolean isVA {get;set;}
    public Boolean isQR {get;set;}
    public Boolean isSQ {get;set;}
    public Boolean isAirlineSelected {get;set;}
    
    private Contract_Addendum__c  a;
    
    public static string domMainline = System.Label.DOM_Mainline;
    public static string domRegional = System.Label.DOM_Regional;
    public static string intShortHaul = System.Label.INT_Short_Haul;
    public static string transTasman = System.Label.Trans_Tasman;
    public static string northAmerica = System.Label.North_America;
    public static string africa = System.Label.Africa;
    public static string middleEast = System.Label.Middle_East;
    public static string ukEuropeQR = System.Label.UK_Europe_QR;
    public static string ukEuropeSQ = System.Label.UK_Europe_SQ;
    
    public DomesticAddendumDiscountController_v1(ApexPages.StandardController myController) 
    {
        a=(Contract_Addendum__c)myController.getrecord();
        domFlag=0;
        regFlag=0;
        
        iFlag = 0;
        mFlag = 0;
        nFlag = 0;
        tFlag = 0;
        aFlag = 0;
        uQRFlag = 0;
        uSQFlag = 0;
        
        isUA = false;
        isVA = false;
        isQR = false;
        isSQ = false;
        isAirlineSelected = false;
        
        //  transFlag=0;
        //Id conid = 
    }
    
    public PageReference initDisc() 
    {        
        contractadd=[SELECT Contract__c,Cos_Version__c,Domestic_Guideline_Tier__c,Domestic_Requested_Tier__c,Africa_requested_Tier__c, INT_short_Haul_Requested_Tier__c, Middle_East_Requested_Tier__c, North_America_Requested_Tier__c, Trans_Tasman_Requested_Tier__c, UK_Europe_Requested_Tier_QR__c, UK_Europe_Guideline_Tier_SQ__c, UK_Europe_Requested_Tier_SQ__c,
                     Red_Circle__c,Regional_Guideline_Tier__c ,Regional_Requested_Tier__c,Status__c,IsEditable__c, Airlines_Selected__c
                     FROM Contract_Addendum__c 
                     where id =:ApexPages.currentPage().getParameters().get('id')];        
        conlist = contractadd.get(0);
        
        if(conlist.IsEditable__c ==true  )
        {
            iseditable = true;            
        }else{
            iseditable = false;
        }
        
        if(conlist.Airlines_Selected__c != NULL){
            isAirlineSelected = true;
            if(conlist.Airlines_Selected__c.contains('UA')){
                isUA = true;
            }
            if(conlist.Airlines_Selected__c.contains('VA')){
                isVA = true;
            }
            if(conlist.Airlines_Selected__c.contains('QR')){
                isQR = true;
            }
            if(conlist.Airlines_Selected__c.contains('SQ')){
                isSQ = true;
            }
        }        
        
        searchResults= [SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
                        where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id') and  Name =:domMainline
                        ORDER BY Sort_Order__c];
        System.debug(ApexPages.currentPage().getParameters().get('id')+'searchResults--'+searchResults);
        if(searchResults.size()>0)
        {
            domFlag=1;
        }
        
        searchResults1= [
            SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
            where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')and  Name =:domRegional
            ORDER BY Sort_Order__c];
        if(searchResults1.size()>0)
        {
            regFlag=1;
        }
        
        searchResultsTrans= [SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
                             where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')and  Name =:transTasman
                             ORDER BY Sort_Order__c];
        
        if(searchResultsTrans.size()>0)
        {
            tFlag=1;
        }
        
        searchResultsInt= [SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
                           where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')and  Name =:intShortHaul
                           ORDER BY Sort_Order__c];
        
        if(searchResultsInt.size()>0)
        {
            iFlag=1;
        }
        
        searchResultsNA= [SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
                          where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')and  Name =:northAmerica
                          ORDER BY Sort_Order__c];
        
        if(searchResultsNA.size()>0)
        {
            nFlag=1;
        }
        
        searchResultsME= [SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
                          where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')and  Name =:middleEast
                          ORDER BY Sort_Order__c];
        
        if(searchResultsME.size()>0)
        {
            mFlag=1;
        }
        
        searchResultsAf= [SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
                          where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')and  Name =:africa
                          ORDER BY Sort_Order__c];
        
        if(searchResultsAf.size()>0)
        {
            aFlag=1;
        }
        
        searchResultsUK_QR= [SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
                             where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')and  Name =:ukEuropeQR
                             ORDER BY Sort_Order__c];
        
        if(searchResultsUK_QR.size()>0)
        {
            uQRFlag=1;
        }
        
        searchResultsUK_SQ= [SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,VA_Eligible_Booking_Class__c FROM Proposed_Discount_Tables__c
                             where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')and  Name =:ukEuropeSQ
                             ORDER BY Sort_Order__c];
        
        if(searchResultsUK_SQ.size()>0)
        {
            uSQFlag=1;
        }
        
        return null;        
    }
    
    public PageReference qsave() {
        upsert searchResults;
        upsert searchResults1; 
        
        upsert searchResultsTrans;
        upsert searchResultsInt;
        upsert searchResultsNA;
        upsert searchResultsME;
        upsert searchResultsAf;
        upsert searchResultsUK_QR;
        upsert searchResultsUK_SQ;
        
        PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference save() {        
        upsert searchResults;
        upsert searchResults1; 
        
        upsert searchResultsTrans;
        upsert searchResultsInt;
        upsert searchResultsNA;
        upsert searchResultsME;
        upsert searchResultsAf;
        upsert searchResultsUK_QR;
        upsert searchResultsUK_SQ;
        
        PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id')); 
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference newDom() 
    {
        
        if (searchResults.size()<1)
        {
            List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>(); 
            
            templateDomesticDiscountList =
                [
                    SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                    ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                    Partner_Eligible_Booking_Class__c 
                    FROM Proposal_Table__c WHERE
                    Name =:domMainline
                    AND IsTemplate__c = true
                    AND Tier__c = : conlist.Domestic_Requested_Tier__c
                    AND Cos_Version__c = :conlist.Cos_Version__c
                    ORDER BY Sort_Order__c
                ];
            
            
            if(templateDomesticDiscountList.size() > 0 )
            {
                
                for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) 
                {   
                    Proposed_Discount_Tables__c newprop = new Proposed_Discount_Tables__c();     
                    newprop.Contract__c = conlist.Contract__c ;
                    newprop.Contract_Addendum__c = conlist.id;
                    newprop.Name = tempdisc.Name;
                    newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
                    newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
                    newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                    searchResults.add(newprop);    
                }               
            }
            
            return null;
        }
        else{
            
            PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
            thePage.setRedirect(true);
            return thePage;
        }
    }
    
    public PageReference newReg() 
    {
        
        if (searchResults1.size()<1)
        {
            List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>(); 
            
            templateDomesticDiscountList =
                [
                    SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                    ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                    Partner_Eligible_Booking_Class__c 
                    FROM Proposal_Table__c WHERE
                    Name =:domRegional
                    AND IsTemplate__c = true
                    AND Tier__c = : conlist.Domestic_Requested_Tier__c
                    AND Cos_Version__c = :conlist.Cos_Version__c
                    ORDER BY Sort_Order__c
                ];
            
            
            if(templateDomesticDiscountList.size() > 0 )
            {
                
                for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) 
                {   
                    Proposed_Discount_Tables__c newprop = new Proposed_Discount_Tables__c();     
                    newprop.Contract__c = conlist.Contract__c ;
                    newprop.Contract_Addendum__c = conlist.id;
                    newprop.Name = tempdisc.Name;
                    newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
                    newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
                    newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                    searchResults1.add(newprop);    
                }               
            }
            
            return null;
        }
        else{
            
            PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
            thePage.setRedirect(true);
            return thePage;
        }
    }
    
    public PageReference newTrans() 
    {
        
        if (searchResultsTrans.size()<1)
        {
            List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>(); 
            
            templateDomesticDiscountList =
                [
                    SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                    ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                    Partner_Eligible_Booking_Class__c 
                    FROM Proposal_Table__c WHERE
                    Name =:transTasman
                    AND IsTemplate__c = true
                    AND Tier__c = : conlist.Trans_Tasman_Requested_Tier__c
                    AND Cos_Version__c = :conlist.Cos_Version__c
                    ORDER BY Sort_Order__c
                ];
            
            
            if(templateDomesticDiscountList.size() > 0 )
            {
                
                for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) 
                {   
                    Proposed_Discount_Tables__c newprop = new Proposed_Discount_Tables__c();     
                    newprop.Contract__c = conlist.Contract__c ;
                    newprop.Contract_Addendum__c = conlist.id;
                    newprop.Name = tempdisc.Name;
                    newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
                    newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
                    newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                    searchResultsTrans.add(newprop);    
                }               
            }
            
            return null;
        }
        else{
            
            PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
            thePage.setRedirect(true);
            return thePage;
        }
    }
    
    public PageReference newInt() 
    {
        
        if (searchResultsInt.size()<1)
        {
            List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>(); 
            
            templateDomesticDiscountList =
                [
                    SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                    ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                    Partner_Eligible_Booking_Class__c 
                    FROM Proposal_Table__c WHERE
                    Name =:intShortHaul
                    AND IsTemplate__c = true
                    AND Tier__c = : conlist.INT_short_Haul_Requested_Tier__c
                    AND Cos_Version__c = :conlist.Cos_Version__c
                    ORDER BY Sort_Order__c
                ];
            
            if(templateDomesticDiscountList.size() > 0 )
            {
                
                for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) 
                {   
                    Proposed_Discount_Tables__c newprop = new Proposed_Discount_Tables__c();     
                    newprop.Contract__c = conlist.Contract__c ;
                    newprop.Contract_Addendum__c = conlist.id;
                    newprop.Name = tempdisc.Name;
                    newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
                    newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
                    newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                    searchResultsInt.add(newprop);    
                }               
            }
            
            return null;
        }
        else{
            
            PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
            thePage.setRedirect(true);
            return thePage;
        }
    }
    
    public PageReference newNA() 
    {
        
        if (searchResultsNA.size()<1)
        {
            List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>(); 
            
            templateDomesticDiscountList =
                [
                    SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                    ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                    Partner_Eligible_Booking_Class__c 
                    FROM Proposal_Table__c WHERE
                    Name =:northAmerica
                    AND IsTemplate__c = true
                    AND Tier__c = : conlist.North_America_Requested_Tier__c
                    AND Cos_Version__c = :conlist.Cos_Version__c
                    ORDER BY Sort_Order__c
                ];
            
            
            if(templateDomesticDiscountList.size() > 0 )
            {
                
                for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) 
                {   
                    Proposed_Discount_Tables__c newprop = new Proposed_Discount_Tables__c();     
                    newprop.Contract__c = conlist.Contract__c ;
                    newprop.Contract_Addendum__c = conlist.id;
                    newprop.Name = tempdisc.Name;
                    newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
                    newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
                    newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                    searchResultsNA.add(newprop);    
                }               
            }
            
            return null;
        }
        else{
            
            PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
            thePage.setRedirect(true);
            return thePage;
        }
    }
    
    public PageReference newME() 
    {
        
        if (searchResultsME.size()<1)
        {
            List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>(); 
            
            templateDomesticDiscountList =
                [
                    SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                    ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                    Partner_Eligible_Booking_Class__c 
                    FROM Proposal_Table__c WHERE
                    Name =:middleEast
                    AND IsTemplate__c = true
                    AND Tier__c = : conlist.Middle_East_Requested_Tier__c
                    AND Cos_Version__c = :conlist.Cos_Version__c
                    ORDER BY Sort_Order__c
                ];
            
            
            if(templateDomesticDiscountList.size() > 0 )
            {
                
                for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) 
                {   
                    Proposed_Discount_Tables__c newprop = new Proposed_Discount_Tables__c();     
                    newprop.Contract__c = conlist.Contract__c ;
                    newprop.Contract_Addendum__c = conlist.id;
                    newprop.Name = tempdisc.Name;
                    newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
                    newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
                    newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                    searchResultsME.add(newprop);    
                }               
            }
            
            return null;
        }
        else{
            
            PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
            thePage.setRedirect(true);
            return thePage;
        }
    }
    
    public PageReference newAf() 
    {
        
        if (searchResultsAf.size()<1)
        {
            List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>(); 
            
            templateDomesticDiscountList =
                [
                    SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                    ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                    Partner_Eligible_Booking_Class__c 
                    FROM Proposal_Table__c WHERE
                    Name =:africa
                    AND IsTemplate__c = true
                    AND Tier__c = : conlist.Africa_requested_Tier__c
                    AND Cos_Version__c = :conlist.Cos_Version__c
                    ORDER BY Sort_Order__c
                ];
            
            
            if(templateDomesticDiscountList.size() > 0 )
            {
                
                for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) 
                {   
                    Proposed_Discount_Tables__c newprop = new Proposed_Discount_Tables__c();     
                    newprop.Contract__c = conlist.Contract__c ;
                    newprop.Contract_Addendum__c = conlist.id;
                    newprop.Name = tempdisc.Name;
                    newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
                    newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
                    newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                    searchResultsAf.add(newprop);    
                }               
            }
            
            return null;
        }
        else{
            
            PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
            thePage.setRedirect(true);
            return thePage;
        }
    }
    
    public PageReference newUK_QR() 
    {
        
        if (searchResultsUK_QR.size()<1)
        {
            List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>(); 
            
            templateDomesticDiscountList =
                [
                    SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                    ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                    Partner_Eligible_Booking_Class__c 
                    FROM Proposal_Table__c WHERE
                    Name =:ukEuropeQR
                    AND IsTemplate__c = true
                    AND Tier__c = : conlist.UK_Europe_Requested_Tier_QR__c
                    AND Cos_Version__c = :conlist.Cos_Version__c
                    ORDER BY Sort_Order__c
                ];
            
            
            if(templateDomesticDiscountList.size() > 0 )
            {
                
                for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) 
                {   
                    Proposed_Discount_Tables__c newprop = new Proposed_Discount_Tables__c();     
                    newprop.Contract__c = conlist.Contract__c ;
                    newprop.Contract_Addendum__c = conlist.id;
                    newprop.Name = tempdisc.Name;
                    newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
                    newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
                    newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                    searchResultsUK_QR.add(newprop);    
                }               
            }
            
            return null;
        }
        else{
            
            PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
            thePage.setRedirect(true);
            return thePage;
        }
    }  
    
    public PageReference newUK_SQ() 
    {
        
        if (searchResultsUK_SQ.size()<1)
        {
            List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>(); 
            
            templateDomesticDiscountList =
                [
                    SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                    ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                    Partner_Eligible_Booking_Class__c 
                    FROM Proposal_Table__c WHERE
                    Name =:ukEuropeSQ
                    AND IsTemplate__c = true
                    AND Tier__c = : conlist.UK_Europe_Requested_Tier_SQ__c
                    AND Cos_Version__c = :conlist.Cos_Version__c
                    ORDER BY Sort_Order__c
                ];
            
            
            if(templateDomesticDiscountList.size() > 0 )
            {
                
                for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) 
                {   
                    Proposed_Discount_Tables__c newprop = new Proposed_Discount_Tables__c();     
                    newprop.Contract__c = conlist.Contract__c ;
                    newprop.Contract_Addendum__c = conlist.id;
                    newprop.Name = tempdisc.Name;
                    newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
                    newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
                    newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                    searchResultsUK_SQ.add(newprop);    
                }               
            }
            
            return null;
        }
        else{
            
            PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
            thePage.setRedirect(true);
            return thePage;
        }
    }
    
    public PageReference remdom()
    {
        Proposed_Discount_Tables__c toDelete = searchResults[searchResults.size()-1];            
        delete toDelete;        
        PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference remreg()
    {
        Proposed_Discount_Tables__c toDelete = searchResults1[searchResults1.size()-1];            
        delete toDelete;        
        PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference remTrans()
    {
        Proposed_Discount_Tables__c toDelete = searchResultsTrans[searchResultsTrans.size()-1];            
        delete toDelete;        
        PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference remInt()
    {
        Proposed_Discount_Tables__c toDelete = searchResultsInt[searchResultsInt.size()-1];            
        delete toDelete;        
        PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference remNA()
    {
        Proposed_Discount_Tables__c toDelete = searchResultsNA[searchResultsNA.size()-1];            
        delete toDelete;        
        PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference remME()
    {
        Proposed_Discount_Tables__c toDelete = searchResultsME[searchResultsME.size()-1];            
        delete toDelete;        
        PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference remAf()
    {
        Proposed_Discount_Tables__c toDelete = searchResultsAf[searchResultsAf.size()-1];            
        delete toDelete;        
        PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference remUK_QR()
    {
        Proposed_Discount_Tables__c toDelete = searchResultsUK_QR[searchResultsUK_QR.size()-1];            
        delete toDelete;        
        PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference remUK_SQ()  
    {
        Proposed_Discount_Tables__c toDelete = searchResultsUK_SQ[searchResultsUK_SQ.size()-1];            
        delete toDelete;        
        PageReference thePage = new PageReference('/apex/DomesticAddendumDiscount_v1?id=' + a.Id);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference cancel() {
        //return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
        PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
        //
        thePage.setRedirect(true);
        //
        return thePage;
    }  
}