@isTest
global class ReservationServiceMockImpl2 implements WebServiceMock{
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       GuestCaseReservationInfo.SalesforceReservationService services = new GuestCaseReservationInfo.SalesforceReservationService();
       GuestCaseReservationInfoModel.FindReservationDetailsRSType response_x = services.FindReservationDemo();
       response.put('response_x', response_x); 
   }
}