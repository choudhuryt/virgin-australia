/**
* @description       : Test class for NewVersionCorporateContractController
* @Updated By        : Cloudwerx
**/
@isTest
private class TestNewVersionCorporateContract {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('Account Closed', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Opportunity testOpportunityObj = TestDataFactory.createTestOpportunity('testOpp1', 'Sales Opportunity Analysis', testAccountObj.Id, Date.today(), 1.00);
        INSERT testOpportunityObj; 
        
        Contract testContractObj = TestDataFactory.createTestContract('TestContract', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        
        Velocity_Status_Match__c testVelocityStatusMatchObj = TestDataFactory.createTestVelocityStatusMatch(testAccountObj.Id, testContractObj.Id, 'Yes', 'Activated', Date.today(), 'some', 'Velocity Status Upgrade');
        testVelocityStatusMatchObj.RecordTypeId = Utilities.getRecordTypeId('Velocity_Status_Match__c', 'Velocity_Upgrade');
        testVelocityStatusMatchObj.Passenger_Velocity_Number__c = '9878786756';
        INSERT testVelocityStatusMatchObj;
        
        Contract_Fulfilment__c testContractFulfilmentObj = TestDataFactory.createTestContractFulfilment(testAccountObj.Id, testContractObj.Id, 'Active', 'Test Booking');
        insert testContractFulfilmentObj;
        
        List<Market__c> testMarkets = new List<Market__c>();
        testMarkets.add(TestDataFactory.createTestMarket('DOM(Mainline)', testOpportunityObj.Id, testContractObj.Id, 'DOM(Mainline)', 'Tier 1', 'Tier 1', 1, 1, 'UA', 1000));
        testMarkets.add(TestDataFactory.createTestMarket('DOM(Regional)', testOpportunityObj.Id, testContractObj.Id, 'DOM(Regional)', 'Tier 1', 'Tier 1', 1, 1, 'UA', 2000));
        INSERT testMarkets;
        
        List<Proposal_Table__c> testProposalTables = new List<Proposal_Table__c>();
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'DOM Mainline'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'DOM Regional'));
        INSERT testProposalTables;
        
        Additional_Benefits__c testAdditionalBenefitsObj = TestDataFactory.createTestAdditionalBenefits(testOpportunityObj.Name, testContractObj.Id, testOpportunityObj.Id);
        INSERT testAdditionalBenefitsObj;
    }
    
    @isTest
    private static void testNewVersionCorporateContractController() {
        Contract testContractObj = [SELECT Id, status FROM Contract LIMIT 1];
        testContractObj.status = 'Activated';
        UPDATE testContractObj;
        Test.startTest();
        PageReference pageRef = Page.NewCorporateContractVersion; 
        pageRef.getParameters().put('contractId', testContractObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        NewVersionCorporateContractController lController  = new NewVersionCorporateContractController(conL);
        Pagereference actionButtonYesPagereference = lController.actionButtonYes();
        Pagereference actionButtonNoPagereference = lController.actionButtonNo();
        Contract oldContract = lController.getOldContract();
        //Asserts
        Contract newContractObj = [SELECT Id, Status, Name FROM Contract WHERE id !=:testContractObj.Id];
        system.assertEquals('TestContract_Version_2.0', newContractObj.Name);
        system.assertEquals('Draft', newContractObj.Status);
        Velocity_Status_Match__c updatedVelocityStatusMatch = [SELECT Id, Contract__c FROM Velocity_Status_Match__c];
        system.assertEquals(newContractObj.Id, updatedVelocityStatusMatch.Contract__c);
        system.assertEquals(oldContract.Id, testContractObj.Id);
        Test.stopTest();
    }
    
    @isTest
    private static void testNewVersionCorporateContractControllerWithNoContractVersion() {
        
        Test.startTest();
        Contract testContractObj = [SELECT Id, status, Contract_Version__c FROM Contract LIMIT 1];
        testContractObj.status = 'Activated';
        testContractObj.Contract_Version__c = null;
        UPDATE testContractObj;
        
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Contract testContractObj1 = TestDataFactory.createTestContract('TestContract2', accountId, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj1.Parent_Contract__c = testContractObj.Id;
        testContractObj1.Contract_Version__c = 2;
        INSERT testContractObj1;
        
        PageReference pageRef = Page.NewCorporateContractVersion; 
        pageRef.getParameters().put('contractId', testContractObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        NewVersionCorporateContractController lController  = new NewVersionCorporateContractController(conL);
        Pagereference actionButtonYesPagereference = lController.actionButtonYes();
        Test.stopTest();
        //Asserts
        system.assertEquals(null, actionButtonYesPagereference);
    }
    
    @isTest
    private static void testNewVersionCorporateContractControllerWithApexMessage() {
        Contract testContractObj = [SELECT ID FROM Contract LIMIT 1];
        Test.startTest();
        PageReference pageRef = Page.NewCorporateContractVersion; 
        pageRef.getParameters().put('contractId', testContractObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        NewVersionCorporateContractController lController  = new NewVersionCorporateContractController(conL);
        Pagereference actionButtonYesPagereference = lController.actionButtonYes();
        Pagereference actionButtonNoPagereference = lController.actionButtonNo();
        Test.stopTest();
        //Asserts
        system.assertEquals('/' + testContractObj.Id, actionButtonNoPagereference.getUrl());
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('Only activated contracts can be versioned!', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.ERROR, msg.getSeverity());
        }
    }
}