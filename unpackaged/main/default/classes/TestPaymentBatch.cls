@isTest
private class TestPaymentBatch
{
    
static testMethod void myAccUnitTest()
{
    Test.StartTest();	
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='01290000000tSR0';
    account.Sales_Matrix_Owner__c = 'Accelerate';
	account.OwnerId = '00590000000LbNz'; 
    account.Corporate_ID__c = '123456R';
    account.Corporate_Travel_Bank_Account_Number__c = '912321321311';
    account.Business_Number__c = '12322322322';
    account.Account_Lifecycle__c = 'Contract';
    insert account;  
    
    
    Date d= Date.today();
	Contract contract = new Contract();
	contract.AccountId = account.id;
	contract.RecordTypeId = '012900000007oTOAAY';
	contract.Status = 'Draft';
	contract.Type__c = 'Accelerate';
	contract.Name =  ' Accelerate';
	contract.Contracting_Entity__c = 'Virgin Australia';
	contract.Sales_Basis__c = 'Flown';
	contract.OwnerId = '00590000000LbNz';
	contract.StartDate = d.addYears(-2);
	//contract.Anniversary_Month__c = contractAccelerate.Anniversary_Month__c;
	//d.toStartOfMonth();
	insert contract;
    
    contract.Status ='Activated';
    update contract;
    
    
    
    Spend__c sp1 = TestUtilityClass.createTestSpend(account.Id, contract.Id);         
    insert sp1; 
    sp1.Year__c= d.year() ;  
    sp1.Month__c =  d.addMonths(-1).month() ;  
    update sp1; 
    
    Spend__c sp2 = TestUtilityClass.createTestSpend(account.Id, contract.Id);         
    insert sp2; 
    sp2.Year__c= d.year() ;  
    sp2.Month__c = d.addMonths(-2).month() ;  
    update sp2; 
     
       
     Spend__c sp3 = TestUtilityClass.createTestSpend(account.Id, contract.Id);         
     insert sp3; 
     sp3.Year__c= d.year() ;   
     sp3.Month__c = d.addMonths(-3).month() ;   
     update sp3;
    
    
    Spend__c sp4 = TestUtilityClass.createTTTestSpend(account.Id, contract.Id);         
    insert sp4; 
    sp4.Year__c= d.year() ;  
    sp4.Month__c =  d.addMonths(-1).month() ;  
    update sp4; 
    
    Spend__c sp5 = TestUtilityClass.createTTTestSpend(account.Id, contract.Id);         
    insert sp5; 
    sp5.Year__c= d.year() ;  
    sp5.Month__c = d.addMonths(-2).month() ;  
    update sp5; 
     
       
     Spend__c sp6 = TestUtilityClass.createTTTestSpend(account.Id, contract.Id);         
     insert sp6; 
     sp6.Year__c= d.year() ;   
     sp6.Month__c = d.addMonths(-3).month() ;   
     update sp6;
   
    
    Spend__c sp7 = TestUtilityClass.createINTTestSpend(account.Id, contract.Id);         
    insert sp7; 
    sp7.Year__c= d.year() ;  
    sp7.Month__c =  d.addMonths(-1).month() ;  
    update sp7; 
    
     PaymentBatch testBatch = new PaymentBatch();
     Database.executeBatch(testBatch);
      
       
       
       Test.StopTest();
       
          
    
    
}
 
    
    static testMethod void mySmfUnitTest()
{
    Test.StartTest();	
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrPAAU';
    account.Sales_Matrix_Owner__c = 'Accelerate';
	account.OwnerId = '00590000000LbNz'; 
    account.Business_Number__c = '12322322322';
    account.Corporate_ID__c = '123456w';
    account.Corporate_Travel_Bank_Account_Number__c = '912355521311';
    account.Account_Lifecycle__c = 'Contract';
    insert account;  
    account.RecordTypeId = '012900000009HrPAAU';
    update account;
    
    Date d= Date.today();
	Contract contract = new Contract();
	contract.AccountId = account.id;
	contract.RecordTypeId = '012900000007oTOAAY';
	contract.Status = 'Draft';
	contract.Type__c = 'Accelerate';
	contract.Name =  ' Accelerate';
	contract.Contracting_Entity__c = 'Virgin Australia';
	contract.Sales_Basis__c = 'Flown';
	contract.OwnerId = '00590000000LbNz';
	contract.StartDate = d.addYears(-2);
	//contract.Anniversary_Month__c = contractAccelerate.Anniversary_Month__c;
	//d.toStartOfMonth();
	insert contract;
    
    contract.Status ='Activated';
    update contract;
    
    
    
    Spend__c sp1 = TestUtilityClass.createTestSpend(account.Id, contract.Id);         
    insert sp1; 
    sp1.Year__c= d.year() ;  
    sp1.Month__c =  d.addMonths(-1).month() ;  
    update sp1; 
    
    Spend__c sp2 = TestUtilityClass.createTestSpend(account.Id, contract.Id);         
    insert sp2; 
    sp2.Year__c= d.year() ;  
    sp2.Month__c = d.addMonths(-2).month() ;  
    update sp2; 
     
       
     Spend__c sp3 = TestUtilityClass.createTestSpend(account.Id, contract.Id);         
     insert sp3; 
     sp3.Year__c= d.year() ;   
     sp3.Month__c = d.addMonths(-3).month() ;   
     update sp3;
    
    
    Spend__c sp4 = TestUtilityClass.createTTTestSpend(account.Id, contract.Id);         
    insert sp4; 
    sp4.Year__c= d.year() ;  
    sp4.Month__c =  d.addMonths(-1).month() ;  
    update sp4; 
    
    Spend__c sp5 = TestUtilityClass.createTTTestSpend(account.Id, contract.Id);         
    insert sp5; 
    sp5.Year__c= d.year() ;  
    sp5.Month__c = d.addMonths(-2).month() ;  
    update sp5; 
     
       
     Spend__c sp6 = TestUtilityClass.createTTTestSpend(account.Id, contract.Id);         
     insert sp6; 
     sp6.Year__c= d.year() ;   
     sp6.Month__c = d.addMonths(-3).month() ;   
     update sp6;
   
     PaymentBatch testBatch = new PaymentBatch();
     Database.executeBatch(testBatch);
      
       
       
       Test.StopTest();
       
          
    
    
}
}