@isTest
private class TestBeyondContractValueController 
{
 static testMethod void myUnitTest() 
 {
        // TO DO: implement unit test
        
        
        //setup account
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
  	
  			
 		insert account;
 
        Fly_Forward_Data__c f1 = new Fly_Forward_Data__c();
        Fly_Forward_Data__c f2 = new Fly_Forward_Data__c();
     
        f1.Account__c = account.id;
        f1.PNR_Created_Dtate__c = System.today();
        f1.Indicative_Value__c =100;
        f1.Upsert_ID__c = account.id + 'aaa' ;
        insert f1;
         
        f2.Account__c = account.id;
        f2.PNR_Created_Dtate__c = System.today()-365;
        f2.Indicative_Value__c =200;
        f2.Upsert_ID__c = account.id + 'bbb' ;
        insert f2;
     
        Economy_X__c e1 = new Economy_X__c();
        e1.Account__c = account.id;
        e1.PNR_Created_Date__c = System.today();
        e1.Indicative_Value__c =100;
        e1.Upsert_ID__c = account.id + 'xxx';
        insert e1;
        
        Economy_X__c e2 = new Economy_X__c();
        e2.Account__c = account.id;
        e2.PNR_Created_Date__c = System.today()-365;
        e2.Indicative_Value__c =100;
        e2.Upsert_ID__c = account.id + 'yyy' ;
        insert e2;
     
        
        Velocity_Status_Match__c vsm = new Velocity_Status_Match__c();
        vsm.Account__c = account.id;
        vsm.Approval_Status__c = 'Activated' ;
        vsm.Within_Contract__c = 'no';
        vsm.Status_Match_or_Upgrade__c = 'Gold';
        vsm.Passenger_Name__c = 'Test';
        vsm.Passenger_Email__c ='andy@test.com';
        vsm.Position_in_Company__c = 'tester';
        vsm.Passenger_Velocity_Number__c = '9530011641';
        vsm.Delivery_To__c = 'Member';
        vsm.Justification__c = 'test';
        vsm.Date_Requested__c = Date.today();
     //Added for failing test classes start 28/11/2019
        vsm.PA_Email__c = 'test@test.com.au';
     	vsm.PA_name__c = 'Sarita Gaur';
     	vsm.PA_Number__c = '1013143410';
     
       
        //Added for failing test classes stop 28/11/2019
		insert vsm;
     
     
     	PageReference pref = Page.BeyondContractValue;
        pref.getParameters().put('id', account.id);
        Test.setCurrentPage(pref);
     
     
        ApexPages.StandardController conL = new ApexPages.StandardController(account);
        BeyondContractValueController lController = new BeyondContractValueController(conL);
     
     
         
         Test.startTest();
         PageReference  ref1 = Page.BeyondContractValue;
         ref1 = lController.initDisc();
         test.stopTest();
        
         
 }
}