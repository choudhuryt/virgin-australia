/*
* HISTORY
*
* 19.May.2016  Warjie Malibago (Accenture CloudFirst); added new test method 'testSetAccountOwner'; see 190516WBM
*/
@isTest
private class AccountTriggerHandlerTest {
    
    @isTest static void accountwaiverpoints()
    {
        Account acc = TestUtilityClass.createTestAccount();
        insert acc;
        Account parentacc = TestUtilityClass.createTestAccount();       
        parentacc.Waiver_Points_Allocated__c = 0;
        parentacc.Waiver_Points_Used__c = 0;
        parentacc.Waiver_Points_Pending_Approval__c = 0;
        insert parentacc ;
        
        acc.Waiver_Points_Allocated__c = 10;
        acc.Waiver_Points_Used__c = 2;
        acc.Waiver_Points_Pending_Approval__c = 2;
        acc.Waiver_Parent_Account__c = parentacc.Id ;
        acc.Empower_Password__c = 'xxxxxx';
        update acc ;
        
    }
    @isTest static void updateoldABNTest()
    {
        User u = TestUtilityClass.createTestUser();
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Sales Super User' LIMIT 1].Id;
        insert u;
        String RECORDTYPE_ACCELERATE = 'Accelerate';
        String RECORDTYPE_SMARTFLY = 'SmartFly';
        Id accelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_ACCELERATE ).getRecordTypeId();
        Id smartFlyRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_SMARTFLY ).getRecordTypeId();
        
        system.runAs(u){
            Test.startTest();
            
            List<Account> accList = new List<Account>();
            Map<Id, Account> accOldMap = new Map<Id, Account>();
            Account acc = TestUtilityClass.createTestAccount();
            acc.Account_Lifecycle__c = 'Offer';
            acc.Accelerate_Batch_Update__c = TRUE;
            acc.Lounge__c = true ;
            acc.Business_Number__c = '12345654321';
            insert acc;
            accList.add(acc);
            
            AGYLogin__c ag = TestUtilityClass.createTestAGYLogin(acc.Id);
            insert ag;
            
            Data_Validity__c dv = TestUtilityClass.createTestDataValidity(acc.Id, ag.Id);
            insert dv;
            
            for(Account a: accList){
                a.Account_Lifecycle__c = 'Inactive';
                a.Business_Number__c = '12375659999';
                //a.RecordTypeId = accelerateRecTypeId;
                accOldMap.put(a.Id, a);
            }
            update accList;
            
            AccountTriggerHandler.updateoldABN(false, accList, accOldMap);
            
            Test.stopTest();
            //  system.assertEquals(Date.today(), [SELECT To_Date__c FROM Data_Validity__c WHERE Id =: dv.Id].To_Date__c);
        }
        
    }
    @isTest static void markDuplicatesV2Test() {
        
        Test.startTest();
        String RECORDTYPE_ACCELERATE = 'Accelerate';
        
        Ignore_Email_Domains__c ignoreDomain = new Ignore_Email_Domains__c();
        ignoreDomain.Name = 'yahoo.com';
        
        insert ignoreDomain;
        // These accounts are for testing of duplicates
        Id accelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_ACCELERATE ).getRecordTypeId();
        
        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<3;i++) {
            String randomInt = String.valueOf(math.rint(math.random()*1000000));
            
            testAccts.add(new Account(Name = 'TestDuplicateAcct',
                                      Business_Number__c = randomInt,
                                      Administrator_Contact_Email__c = 'TestAdmin@testsalesforce.com',
                                      RecordTypeId = accelerateRecTypeId,
                                      //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                      BillingStreet = 'Bliss',
                                      BillingCity = 'Malolos',
                                      BillingState = 'BUL',
                                      BillingPostalCode = '3000',
                                      BillingCountry = 'PH',
                                      Billing_Country_Code__c = 'AU',
                                      Industry_Type__c = 'Engineering',
                                      Account_Lifecycle__c = 'Offer',
                                      //MOD-END CILAG INS CASE# 00129600 09.23.16
                                      Administrator_Contact_Last_Name__c = 'Test ' + i));
        }
        insert testAccts; 
        
        Account acct = new Account(Name = 'TestDuplicateAcct1',
                                   Business_Number__c = '11122233344',
                                   Administrator_Contact_Email__c = 'TestAdmin@testsalesforce1.com',
                                   RecordTypeId = accelerateRecTypeId,
                                   //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                   BillingStreet = 'Bliss',
                                   BillingCity = 'Malolos',
                                   BillingState = 'BUL',
                                   BillingPostalCode = '3000',
                                   BillingCountry = 'PH',
                                   Billing_Country_Code__c = 'AU',
                                   Industry_Type__c = 'Engineering',
                                   Account_Lifecycle__c = 'Offer',
                                   //MOD-END CILAG INS CASE# 00129600 09.23.16
                                   Administrator_Contact_Last_Name__c = 'Test last');
        
        insert acct;
        // Implement test code
        List<Account> accountList = [SELECT Id,Name, Business_Number__c,Administrator_Contact_Email__c, RecordTypeId, 
                                     Administrator_Contact_Last_Name__c, Potential_Duplicate__c FROM Account];
        
        Integer duplicateCount = 0;
        for(Account acctTest : accountList){
            if(acctTest.Potential_Duplicate__c){
                duplicateCount++;
            }
        }
        
        System.assertEquals(2, duplicateCount);
        System.assertEquals(4, accountList.size());
        Test.stopTest();
    }
    
    @isTest static void markDuplicatesAccountWebsiteTest() {
        
        Test.startTest();
        String RECORDTYPE_ACCELERATE = 'Accelerate';
        
        Ignore_Email_Domains__c ignoreDomain = new Ignore_Email_Domains__c();
        ignoreDomain.Name = 'yahoo.com';
        
        insert ignoreDomain;
        // These accounts are for testing of duplicates
        Id accelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_ACCELERATE ).getRecordTypeId();
        
        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<3;i++) {
            testAccts.add(new Account(Name = 'TestDuplicateAcct'+i,
                                      Business_Number__c = '1112223334'+i,
                                      Administrator_Contact_Email__c = 'TestAdmin@testsalesforce.com' + i,
                                      RecordTypeId = accelerateRecTypeId,
                                      //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                      BillingStreet = 'Bliss',
                                      BillingCity = 'Malolos',
                                      BillingState = 'BUL',
                                      BillingPostalCode = '3000',
                                      BillingCountry = 'PH',
                                      Billing_Country_Code__c = 'AU',
                                      Industry_Type__c = 'Engineering',
                                      Account_Lifecycle__c = 'Offer',
                                      //MOD-END CILAG INS CASE# 00129600 09.23.16
                                      Website = 'http://test.com',
                                      Administrator_Contact_Last_Name__c = 'Test ' + i));
        }
        insert testAccts; 
        
        Account acct = new Account(Name = 'TestDuplicateAcct11',
                                   Business_Number__c = '11122232344',
                                   Administrator_Contact_Email__c = 'TestAdmin@testsalesforce1.com',
                                   RecordTypeId = accelerateRecTypeId,
                                   //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                   BillingStreet = 'Bliss',
                                   BillingCity = 'Malolos',
                                   BillingState = 'BUL',
                                   BillingPostalCode = '3000',
                                   BillingCountry = 'PH',
                                   Billing_Country_Code__c = 'AU',
                                   Industry_Type__c = 'Engineering',
                                   Account_Lifecycle__c = 'Offer',
                                   //MOD-END CILAG INS CASE# 00129600 09.23.16
                                   Website = 'http://www.test1.com',
                                   Administrator_Contact_Last_Name__c = 'Test last');
        
        insert acct;
        
        Account acct2 = new Account(Name = 'TestDuplicateAcct21',
                                    Business_Number__c = '11122232341',
                                    Administrator_Contact_Email__c = 'TestAdmin@testsalesfor2ce1.com',
                                    RecordTypeId = accelerateRecTypeId,
                                    //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                    BillingStreet = 'Bliss',
                                    BillingCity = 'Malolos',
                                    BillingState = 'BUL',
                                    BillingPostalCode = '3000',
                                    BillingCountry = 'PH',
                                    Billing_Country_Code__c = 'AU',
                                    Industry_Type__c = 'Engineering',
                                    Account_Lifecycle__c = 'Offer',
                                    //MOD-END CILAG INS CASE# 00129600 09.23.16
                                    Website = 'http://www.yahoo.com',
                                    Administrator_Contact_Last_Name__c = 'T2est last');
        
        insert acct2;
        // Implement test code
        List<Account> accountList = [SELECT Id,Name, Business_Number__c,Administrator_Contact_Email__c, RecordTypeId, 
                                     Administrator_Contact_Last_Name__c, Potential_Duplicate__c FROM Account];
        
        Integer duplicateCount = 0;
        for(Account acctTest : accountList){
            if(acctTest.Potential_Duplicate__c){
                duplicateCount++;
            }
        }
        
        Test.stopTest();
    }
    
    //190516 WBM Start
    @isTest static void testSetAccountOwnerInactive(){
        User u = TestUtilityClass.createTestUser();
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Sales Super User' LIMIT 1].Id;
        insert u;
        
        system.runAs(u){
            Test.startTest();
            
            List<Account> accList = new List<Account>();
            Map<Id, Account> accOldMap = new Map<Id, Account>();
            Account acc = TestUtilityClass.createTestAccount();
            acc.Account_Lifecycle__c = 'Offer';
            acc.Accelerate_Batch_Update__c = TRUE;
            acc.Lounge__c = true ;
            insert acc;
            accList.add(acc);
            
            AGYLogin__c ag = TestUtilityClass.createTestAGYLogin(acc.Id);
            insert ag;
            
            Data_Validity__c dv = TestUtilityClass.createTestDataValidity(acc.Id, ag.Id);
            insert dv;
            
            for(Account a: accList){
                a.Account_Lifecycle__c = 'Inactive';
                accOldMap.put(a.Id, a);
            }
            update accList;
            
            AccountTriggerHandler.setAccountOwner(false, accList, accOldMap);
            
            Test.stopTest();
            //  system.assertEquals(Date.today(), [SELECT To_Date__c FROM Data_Validity__c WHERE Id =: dv.Id].To_Date__c);
        }
    }
    
    
    //190516 WBM End
    
    //190516 WBM Start
    @isTest static void testSetAccountOwnerContract(){
        User u = TestUtilityClass.createTestUser();
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Sales Super User' LIMIT 1].Id;
        insert u;
        
        system.runAs(u){
            List<Account> accList = new List<Account>();
            Map<Id, Account> accOldMap = new Map<Id, Account>();
            Account acc = TestUtilityClass.createTestAccount();
            acc.Account_Lifecycle__c = 'Offer';
            acc.Agreed_To_Terms_And_Conditions__c = true;
            acc.Accelerate_Batch_Update__c = false;
            insert acc;
            accList.add(acc);
            
            Contract ct = TestUtilityClass.createTestContract(acc.Id);
            insert ct;
            
            Velocity_Status_Match__c vsm = TestUtilityClass.createTestVelocityStatusMatch(acc.Id);
            vsm.Contract__c = ct.Id;
            insert vsm;
            
            AGYLogin__c ag = TestUtilityClass.createTestAGYLogin(acc.Id);
            insert ag;
            
            Data_Validity__c dv = TestUtilityClass.createTestDataValidity(acc.Id, ag.Id);
            insert dv;
            
            for(Account a: accList){
                a.Account_Lifecycle__c = 'Contract';
                a.Accelerate_Key_Contact_Email__c = 'test.best@gmail.com';
                a.Agreed_To_Terms_And_Conditions__c = TRUE;
                a.Accelerate_Batch_Update__c = TRUE;
                accOldMap.put(a.Id, a);
            }
            update accList;
            
            Test.startTest();
            AccountTriggerHandler.setAccountOwner(false, accList, accOldMap);
            Test.stopTest();
        }
    }
    //190516 WBM End
    @isTest static void testsetrelatedtmcparent(){
        User u = TestUtilityClass.createTestUser();
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Sales Super User' LIMIT 1].Id;
        insert u;
        
        system.runAs(u){
            List<Account> accList = new List<Account>();
            Map<Id, Account> accOldMap = new Map<Id, Account>();
            Account acc = TestUtilityClass.createTestAccount();
            insert acc;
            Account upacc = TestUtilityClass.createTestParentAccount();
            insert upacc;
            Account tmcacc = TestUtilityClass.createTestAccount();
            tmcacc.ParentId =   upacc.id;                    
            insert tmcacc;                       
            accList.add(acc);
            
            
            for(Account a: accList){
                a.Main_Corporate_TMC__c = tmcacc.id;
                accOldMap.put(a.Id, a);
            }
            update accList;
            
            Test.startTest();
            AccountTriggerHandler.setrelatedtmcparent(accList, accOldMap);
            Test.stopTest();
        }
    }
    @isTest static void markDuplicatesTest()
    {
        User u = TestUtilityClass.createTestUser();
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Sales Super User' LIMIT 1].Id;
        insert u;

        String RECORDTYPE_ACCELERATE = 'Accelerate';
        String RECORDTYPE_SMARTFLY = 'SmartFly';

        Id accelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_ACCELERATE ).getRecordTypeId();
        Id smartFlyRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_SMARTFLY ).getRecordTypeId();
        
        
        system.runAs(u){
            
            
            List<Account> accList = new List<Account>();
            Account acc = TestUtilityClass.createTestAccount();
            acc.Account_Lifecycle__c = 'Offer';
            acc.Accelerate_Batch_Update__c = TRUE;
            acc.Lounge__c = true ;
            acc.RecordTypeId = accelerateRecTypeId;
            acc.Business_Number__c = '12345654321';
            
            List<Account> accList2 = new List<Account>();
            Account acc2 = TestUtilityClass.createTestAccount();
            acc2.Account_Lifecycle__c = 'Offer';
            acc2.Accelerate_Batch_Update__c = TRUE;
            acc2.Lounge__c = true ;
            acc2.RecordTypeId = smartFlyRecTypeId;
            acc2.Business_Number__c = '12345666321';
            
            Test.startTest();
            insert acc;
            insert acc2;

            accList.add(acc);
            accList2.add(acc2);

            AccountTriggerHandler.markDuplicates(accList);
            AccountTriggerHandler.markDuplicates(accList2);
            
            Test.stopTest();
            //  system.assertEquals(Date.today(), [SELECT To_Date__c FROM Data_Validity__c WHERE Id =: dv.Id].To_Date__c);
        }
    }
}