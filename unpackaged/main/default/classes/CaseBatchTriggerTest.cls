/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 30.11.2021
**Description: Test Class to cover Classes-'CaseAttachmentDeletionBatch', 'CaseAttachmentDeletionScheduler' & Triggers - 'CaseResolutionTimeTrigger','CaseCreateOrUpdateContact'
*/
@isTest
public class CaseBatchTriggerTest {
    
    static testMethod void batchSuccessScenario(){
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        List<ContentDocumentLink> contentLinkList = new List<ContentDocumentLink>();
        List<EmailMessage> msgList = new List<EmailMessage>();
        List<Attachment> attList = new List<Attachment>();
                        
        for(integer i=0; i<10;i++){
            Case cs = new Case();
        	cs.RecordTypeId = caseRTId;
        	cs.Status = 'Closed';
            caseList.add(cs);
        }
        insert caseList; System.debug('caseList--'+caseList);
                
        Blob bodyBlob = Blob.valueOf('Content of the document for the Test Class for Batch Class');
        ContentVersion cv = new ContentVersion(
            Title = 'Content Document',
            PathonClient = 'Content Document.txt',
            VersionData = bodyBlob,
            origin = 'H'
        );
        insert cv;
        
        ContentVersion cd = [Select Id, ContentDocumentId from ContentVersion where Id =:cv.Id LIMIT 1];
        
        for(integer j=0; j<10; j++){
            ContentDocumentLink cl = new ContentDocumentLink();
            cl.LinkedEntityId = caseList[j].Id;
            cl.contentdocumentid = cd.ContentDocumentId;
            cl.ShareType = 'V';
            contentLinkList.add(cl);
        }
        insert contentLinkList;
        
        for(integer k=0; k<10; k++){
            EmailMessage em = new EmailMessage();
            em.Subject = 'Test Email'+k;
            em.RelatedToId = caseList[k].Id;
            msgList.add(em);
        }
        insert msgList;
        
        for(integer l=0;l<10;l++){
            Attachment att = new Attachment();
            att.Name = 'Email Attachment'+l;
            att.ParentId = msgList[l].Id;
            att.Body = bodyBlob;
            attList.add(att);
        }
        insert attList;
        
        Test.startTest();
        	CaseAttachmentDeletionBatch obj = new CaseAttachmentDeletionBatch();
        	DataBase.executeBatch(obj);
        
        Test.stopTest();
    }
    
    static testMethod void batchSchedulerScenario(){
        Test.startTest();
        CaseAttachmentDeletionScheduler sched = new CaseAttachmentDeletionScheduler();
        String sch = '0 0 23 * * ?';
        System.schedule('Test Status Check', sch, sched);
        Test.stopTest();
    }
    
    static testMethod void triggerScenario(){
        
        /*Contact con = new Contact();
        con.LastName = 'Demo Contact';
        con.Phone = '7878787878';
        con.mobilePhone = '7878787878';
        con.Email = 'abc@test.com';
        con.Velocity_Number__c = '4444444444';
        insert con;*/
        
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();
        System.debug('caseRTId--'+caseRTId);
        Case trigCase = new Case();
        trigCase.RecordTypeId = caseRTId;
        trigCase.Status = 'New';
        trigCase.SuppliedEmail = 'abc@test.com';
        trigCase.Contact_Email__c = 'abc@test.com';
        insert trigCase;
        
        trigCase.First_Response_Time__c = System.now();
        trigCase.First_Response__c = true;
        update trigCase;
        
        trigCase.Status = 'Closed';
        update trigCase;
        
        trigCase.Status = 'Re-Opened';
        trigCase.Case_Re_Opened_Date__c = System.now();
        trigCase.First_Response_Time__c = NULL;
        trigCase.First_Response__c = false;
        update trigCase;
        
        trigCase.First_Response_Time__c = System.now();
        trigCase.First_Response__c = true;
        update trigCase;
        
        trigCase.Status = 'Closed';
        update trigCase;
        
        
    }
    
    static testMethod void triggerScenario1(){
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();
        Case trigCase1 = new Case();
        trigCase1.RecordTypeId = caseRTId;
        trigCase1.Status = 'New';
        trigCase1.SuppliedEmail = 'abc@test.com';
        trigCase1.Contact_Email__c = NULL;
        insert trigCase1;
        
        Case trigCase2 = new Case();
        trigCase2.RecordTypeId = caseRTId;
        trigCase2.Status = 'New';
        trigCase2.SuppliedEmail = NULL;
        trigCase2.Contact_Email__c = 'abc@test.com';
        insert trigCase2;
    }
}