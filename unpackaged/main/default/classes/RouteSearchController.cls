public with sharing class RouteSearchController {

   public string qry {get;set;}
   public string search_type {get;set;}
   public string lookupDisplayData {get;set;}
   public List<Airport_Codes__c> listData {get;set;}
   public String filter;
   //Constructor
   public RouteSearchController(){
       listData = new List<Airport_Codes__c>();
       lookupDisplayData = '';        
       qry  = ApexPages.currentPage().getParameters().get('q');
       search_type= ApexPages.currentPage().getParameters().get('search_type');
       String query = '';
           
       listData  = getQuery(); 
       lookupDisplayData = getDisplayData(listData);
           
   }
   
   /**
   //GetRouteQuery
   //Gives query to be fired for 'Airport_Codes__c' popup Page
   */
   private List<Airport_Codes__c> getQuery(){
        filter = '%' + qry + '%';
        return [SELECT id, Name from Airport_Codes__c WHERE Airport_Code__c like :filter order by Airport_Code__c limit 20];
        //return [SELECT id, Name from Airport_Codes__c WHERE (Airport_Code__c  like :filter)OR (City__c  like :filter) order by Airport_Code__c limit 20];
   }
   //WHERE Airport_Code__c like :filter order by Name
   
   
   /**
   //getRouteDisplayData
   //Gives output as string from route data list to show on screen
   */
   private string getDisplayData(List<Airport_Codes__c> dataList){
        string output = '';
        for(Airport_Codes__c item: dataList){
            output += item.Name + '|' + item.Name + '|' + item.Name +'\n';           
        }
        return output;
   }
     

}