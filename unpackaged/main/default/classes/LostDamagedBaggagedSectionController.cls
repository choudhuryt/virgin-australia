public class LostDamagedBaggagedSectionController{
    
    public Case caseRecord{set;get;}
    public List<Lost_Baggage__c> LostBaggageList {set;get;}
    
    //Constructor
    public LostDamagedBaggagedSectionController(ApexPages.StandardController controller) {        
        this.caseRecord  = (Case)controller.getRecord();
        this.LoadLostBaggage();
    }
    
    /*
	Get Lost Baggage based on current case id
	*/
    public void LoadLostBaggage()
    {
         this.LostBaggageList = [SELECT id,Search_Date_Time__c,CreatedById,PIR_Number__c 
                                FROM Lost_Baggage__c
                                WHERE Case__c =: this.caseRecord.id
                                ORDER BY Search_Date_Time__c desc];
    }
}