public with sharing class AccountManagerPointUpdateController
{
    
    public List <Waiver_Point__c> wplist ;
    public List <Waiver_Point__c> waiverpointlist {get;set;}    
    public List <User> userlist ;
    public string userId;
    public List<User> userrec {get;set;} 
    public User userdetail{get;set;} 
    
    public AccountManagerPointUpdateController(ApexPages.StandardController controller)
    {        
        userId = ApexPages.currentPage().getParameters().get('Id');               
    }
    
    public PageReference initDisc() 
    {
        
        userlist = [Select id, Name from User where id = :userId ];
        
        if (userlist.size() > 0)
        {
            userdetail =  userlist[0];
        }
        
        waiverpointlist = [select id,Account_Manager__c,Waiver_Points_Allocated__c,Start_Date__c,End_Date__c
                           from  Waiver_Point__c where Account_Manager__c=:userId
                           and Status__c = 'Active' 
                          ];                
        
        if (waiverpointlist.size() == 0 )
        {
            Waiver_Point__c objwp = new Waiver_Point__c();   
            objwp.Account_Manager__c = ApexPages.currentPage().getParameters().get('Id');  
            objwp.Waiver_Type__c  = 'Account Manager Level' ; 
            objwp.Start_Date__c =   Date.newInstance(2017, 07, 1);
            objwp.End_Date__c   =   Date.newInstance(2018, 06, 30); 
            objwp.Waiver_Points_Allocated__c   =   0 ; 
            waiverpointlist.add(objwp);              
        }         
        return null;
    }
    
    public PageReference save()
    {
        try { 
            upsert waiverpointlist ;
            
        } Catch (DMLException e)
        {
            ApexPages.addMessages(e);
            return null;  
        }      
        PageReference thePage = new PageReference('/apex/AccountManagerPointCheckForUpdate' );
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record Created/Updated Successfully.Thank you!'));   
        
        thePage.setRedirect(true);
        return thePage; 
    }
    
    public PageReference cancel()
    {
        PageReference pageRef = Page.AccountManagerPointCheckForUpdate; 
        pageRef.setRedirect(true);    		
        return pageRef;
    }
    
    
}