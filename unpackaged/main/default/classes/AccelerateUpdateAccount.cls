/**
 * @description       : AccelerateUpdateAccount
 * updatedBy : cloudwerx (removed hard coded id)
**/
public with sharing class AccelerateUpdateAccount {
    
    /*
        @Updated By: Cloudwerx
        description : Here we added code to get user id and record type ids dynamically
    */
    public static Id accounAccelerateRecordTypeId = Utilities.getRecordTypeId('Account', 'Accelerate');
    @future 
    public static void UpdateTheAccount(Id l) {
        
        /*  Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Lead.fields.getMap();
            List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
            
            String theQuery = 'SELECT ';
            for(Schema.SObjectField s : fldObjMapValues)
            {
            String theName = s.getDescribe().getName();
            theQuery += theName + ','; 
            }
            
            // Trim last comma
            theQuery = theQuery.subString(0, theQuery.length() - 1);
            
            // Finalize query string
            theQuery += ' FROM Lead WHERE Id = \'' + l + '\'';
            
            // Make your dynamic call
            */	
        Lead convertedLead= new Lead();
        convertedLead = [select ConvertedAccountId from Lead where id =:l];
        Account account = new Account();
        if(convertedLead.ConvertedAccountId != null){
            account = (Account) [SELECT Id, name, Business_Number__c, Account_Lifecycle__c, Sales_Support_Group__c, 
                                 Sales_Matrix_Owner__c, OwnerId, Account_Owner__c, Velocity_Pilot_Gold_Available__c,
                                 Lounge_Last_Name__c, BillingState, BillingStreet, BillingCity, Lounge_First_Name__c,
                                 Lounge_Email__c, BillingPostalCode, Pilot_Gold_Position__c, Pilot_Gold_First_Name__c, 
                                 Pilot_Gold_Velocity_Number__c, Pilot_Gold_Email__c, Pilot_Gold_Last_Name__c, Pilot_Gold_Position_Second_Nominee__c, 
                                 Pilot_Gold_First_Name_Second_Nominee__c, Pilot_Gold_last_Name_Second_Nominee__c, Lounge_Position__c, 
                                 Pilot_Gold_Velocity_Gold_Second_Nominee__c, Pilot_Gold_Email_Second_Nominee__c, market_segment__c	
                                 FROM Account WHERE id=:convertedLead.ConvertedAccountId];
            
            account.Sales_Matrix_Owner__c = 'Accelerate';
            account.Account_Lifecycle__c = 'Acceptance';
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
            account.OwnerId = AccelerateAccountUpdate.vaUserId;
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
            account.Account_Owner__c = AccelerateAccountUpdate.vaUserId;
            account.Velocity_Pilot_Gold_Available__c = 2;
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
            account.Sales_Support_Group__c = AccelerateAccountUpdate.vaUserId;
            account.Sales_Matrix_Owner__c ='Accelerate';
            account.market_segment__c = 'Accelerate';
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
            account.recordtypeid = accounAccelerateRecordTypeId;
            
            Account_Data_Validity__c Adv = new Account_Data_Validity__c();
            adv = [select id, Account_Owner__c, Account_Record_Type__c, From_Account__c, Market_Segment__c, Sales_Matrix_Owner__c 
                   FROM Account_Data_Validity__c WHERE From_Account__c = :account.id LIMIT 1];
            adv.Market_Segment__c = 'Accelerate';
            adv.Sales_Matrix_Owner__c = 'Accelerate';
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
            adv.Account_Record_Type__c = accounAccelerateRecordTypeId;
            UPDATE adv;
            
            try {
                UPDATE account;
            } catch(Exception e) {
                VirginsutilitiesClass.sendEmailError(e);
            }
            
            
            Contact keyContact = new Contact();
            keyContact = (Contact) [SELECT Id, OwnerId, Key_Contact__c, Email, Subscriptions__c FROM Contact 
                                    WHERE AccountId =:account.id LIMIT 1];
            keyContact.Key_Contact__c = true;
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
            keyContact.OwnerId = AccelerateAccountUpdate.vaUserId;
            try {
                UPDATE keyContact;
            } catch(Exception e) {
                VirginsutilitiesClass.sendEmailError(e);
            }
            //update Subscription
            Subscriptions__c tempSub= new Subscriptions__c(); 
            tempSub.Contact_ID__c = keyContact.Id;
            tempSub.Contact_ID__c = tempSub.Contact_ID__c.substring(0, Math.min(tempSub.Contact_ID__c.length(), 15));
            
            Subscriptions__c subscription = new Subscriptions__c();
            subscription = (Subscriptions__c) [SELECT id, Contact_ID__c, Business_News__c, Accelerate_EDM__c FROM Subscriptions__c 
                                               WHERE Contact_ID__c =:tempSub.Contact_ID__c];
            
            subscription.Business_News__c = true;
            subscription.Accelerate_EDM__c = true;
            try {
                UPDATE subscription;
            } catch(Exception e) {
                VirginsutilitiesClass.sendEmailError(e);	
            }
            //Adding a task for Sales support adding a Lounge Scheme Cordinator	
            
            if(account.Lounge__c = true){
                
                if(keyContact.Email <> account.Lounge_Email__c){	
                    Contact loungeContact = new Contact();
                    loungeContact.FirstName = account.Lounge_First_Name__c ;
                    loungeContact.LastName = account.Lounge_Last_Name__c;
                    loungeContact.Email = account.Lounge_Email__c;
                    loungeContact.AccountId = account.id;
                    loungeContact.MailingStreet = account.BillingStreet;
                    loungeContact.MailingCity = account.BillingCity;
                    loungeContact.MailingPostalCode = account.BillingPostalCode;
                    loungeContact.MailingState = account.BillingState;
                    // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
                    loungeContact.OwnerId = AccelerateAccountUpdate.vaUserId;
                    try {
                        INSERT loungeContact;
                    } catch(Exception e) {
                        VirginsutilitiesClass.sendEmailError(e);
                    }
                }
                
                
                //task
                Task task = new Task();
                task.WhatId = account.Id;
                task.Subject ='Please Process the following Accelerate Lounge Menmber';
                // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
                task.OwnerId =AccelerateAccountUpdate.vaUserId;
                task.Status = 'Not Started';
                task.Priority = 'Normal';
                task.Notify_Creator__c = true;
                task.Description = 'Contract Name: ' + account.Lounge_First_Name__c + ' ' + account.Lounge_Last_Name__c + ' ' + account.Lounge_Position__c;
                try {
                    INSERT task;
                } catch(Exception ex) {
                    VirginsutilitiesClass.sendEmailError(ex);
                }
            }
            
            Date d= Date.today();
            d.toStartOfMonth();
            //Do more stuff here.....
            Contract contract = new Contract();
            contract.AccountId = account.id;
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic
            contract.RecordTypeId = AccelerateAccountUpdate.contractAcceleratePOSRebateRecordTypeId;
            contract.Status = 'Draft';
            contract.Type__c = 'Rebate';
            contract.Contracting_Entity__c = 'Virgin Australia';
            contract.Sales_Basis__c = 'Flown';
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
            contract.OwnerId = AccelerateAccountUpdate.vaUserId;
            contract.StartDate = d;
            try {
                INSERT contract;
            } catch(Exception ex){
                VirginsutilitiesClass.sendEmailError(ex);
            }	
            //Add pilot gold if applicable
            
            //if(account.Pilot_Gold_Email__c != null && !account.Pilot_Gold_Email__c.equals('')){
            if(account.Pilot_Gold_Email__c != null){
                Velocity_Status_Match__c vsm = new Velocity_Status_Match__c();
                vsm.Account__c = account.id;
                vsm.Approval_Status__c = 'Draft';
                vsm.Date_Requested__c = Date.today();
                vsm.Delivery_To__c = 'Member';
                vsm.Position_in_Company__c = account.Pilot_Gold_Position__c;
                vsm.Passenger_Name__c = account.Pilot_Gold_First_Name__c + ' ' + account.Pilot_Gold_Last_Name__c;
                vsm.Passenger_Velocity_Number__c = account.Pilot_Gold_Velocity_Number__c;
                vsm.Passenger_Email__c = account.Pilot_Gold_Email__c;
                vsm.Within_Contract__c = 'Yes';
                vsm.Contract__c = contract.id;
                vsm.Status_Match_or_Upgrade__c ='Pilot Gold';
                vsm.Justification__c = 'Within Contract';
                try {
                    INSERT vsm;
                } catch(Exception e) {
                    VirginsutilitiesClass.sendEmailError(e);
                }
            }
            //}
            //if(account.Pilot_Gold_Email_Second_Nominee__c != null && !account.Pilot_Gold_Email_Second_Nominee__c.equals('') ){
            if(account.Pilot_Gold_Email_Second_Nominee__c != null){
                Velocity_Status_Match__c vsm1 = new Velocity_Status_Match__c();
                vsm1.Account__c = account.id;
                vsm1.Approval_Status__c = 'Draft';
                vsm1.Date_Requested__c = Date.today();
                vsm1.Delivery_To__c = 'Member';
                vsm1.Position_in_Company__c = account.Pilot_Gold_Position_Second_Nominee__c;
                vsm1.Passenger_Name__c = account.Pilot_Gold_First_Name_Second_Nominee__c  + ' ' + account.Pilot_Gold_last_Name_Second_Nominee__c;
                vsm1.Passenger_Velocity_Number__c = account.Pilot_Gold_Velocity_Gold_Second_Nominee__c;
                vsm1.Passenger_Email__c = account.Pilot_Gold_Email_Second_Nominee__c;
                vsm1.Within_Contract__c = 'Yes';
                vsm1.Contract__c = contract.id;
                vsm1.Justification__c = 'Within Contract';
                vsm1.Status_Match_or_Upgrade__c ='Pilot Gold';
                try {
                    INSERT vsm1;
                } catch(Exception e) {
                    VirginsutilitiesClass.sendEmailError(e);
                }
            }
            //}
            //	}
            //System.debug('Method called with: ' + a + ' and ' + i);
            // Perform long-running code
        }
    }
}