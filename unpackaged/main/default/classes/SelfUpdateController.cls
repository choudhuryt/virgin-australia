public class SelfUpdateController {
    
   public List<Account> acct {get;set;}   
   public List<Contact> keyContactList {get;set;}    
   private Account a;  
   public Contact keycontact{get;set;}
   public Account accountdetail{get;set;} 
   public  string recId{get; set;}
   public  string eId{get; set;} 
   public SelfUpdateController(ApexPages.StandardController myController) 
    {
       
        a=(Account)myController.getrecord();
        recId = ApexPages.currentPage().getParameters().get('Id');  
        eId = ApexPages.currentPage().getParameters().get('eId');
        acct=[select id,Name ,BillingStreet, Business_Number__c,BillingCity , BillingState,BillingPostalCode,
                     Estimated_International_Revenue__c, Estimated_Domestic_Revenue__c
                    from Account where id =:ApexPages.currentPage().getParameters().get('id')];
        
        accountdetail = acct.get(0);
        
        keyContactList = [SELECT Id,Salutation, LastName, FirstName, Title, Phone, MobilePhone, Email, Status__c 
										FROM Contact 
										WHERE AccountId = :ApexPages.currentPage().getParameters().get('id')
										AND Key_Contact__c = true
										AND Status__c = 'Active' ];
        keycontact = keyContactList.get(0);
             
    }   
    
    public PageReference clickYes(){
		if(this.acct != null){
            
         Account_Self_Update__c newAcctSelfUpdate = new Account_Self_Update__c();
         newAcctSelfUpdate.Account__c = accountdetail.Id;
         newAcctSelfUpdate.Contact__c = keycontact.Id ;
         newAcctSelfUpdate.Account_Name__c = accountdetail.Name;
         newAcctSelfUpdate.Billing_Street__c = accountdetail.BillingStreet;
         newAcctSelfUpdate.Billing_City__c = accountdetail.BillingCity;
         newAcctSelfUpdate.Business_Number__c = accountdetail.Business_Number__c;
         newAcctSelfUpdate.Billing_State_Province__c = accountdetail.BillingState;
         newAcctSelfUpdate.Billing_Post_Code__c = accountdetail.BillingPostalCode;
         newAcctSelfUpdate.Notes__c = 'No changes';
         newAcctSelfUpdate.Please_Contact_Me__c = false;
         newAcctSelfUpdate.Updated__c = false;
            
         insert newAcctSelfUpdate; 
		 return Page.SelfUpdateConfirmation;
			//return new PageReference('/' + this.acct.Id);
		}else{
			return null;
		}
	}
    
   public PageReference clickNo()
	{
            
		PageReference pageRef = Page.SelfUpdateEdit;
        pageRef.getParameters().put('id', recid);  
        pageRef.getParameters().put('eid', eid); 
		pageRef.setRedirect(true);		
		return pageRef;
	} 
    
}