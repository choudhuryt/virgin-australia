/** * File Name      : ScheduleAResponsysBatch implements Schedulable
* Description        : This Apex Class is a batch class that runs every day to look for Responsys Campaign members who have a status of 'hard bounce'.
                       If a hard bounce is found the Account Manager is notified and the email is removed from the contact.
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Edward Stachyra
* * Date             : 23 August 2012
* * Technical Task ID: 
* * Notes            : Batch Classes require the start, execute and finish methods and must implement Database.Batchable
                     : To run as a schedule task they also need a Schedule Class that implements Schedulable, this class has
                       a scheduledClass called ScheduleResponsysBatch.cls. 
* Modification Log ==================================================​============= 
Ver Date Author Modification --- ---- ------ -------------
* */ 

global class ScheduleResponsysBatch implements Schedulable{

	global void execute(SchedulableContext sc) {
	
	ResponsysBatch c = new ResponsysBatch();
    Database.executeBatch(c);
	   
	}


}