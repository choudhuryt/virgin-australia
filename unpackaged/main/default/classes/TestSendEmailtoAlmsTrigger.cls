/**
* @description       : Test class for SendEmailtoAlmsTrigger
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestSendEmailtoAlmsTrigger {
    
    @testSetup 
    private static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
    }
    
    @isTest
    private static void testIsInsertSendEmailtoAlmsTrigger() {
        Account testAccountObj = [SELECT Id,Name FROM Account LIMIT 1];
        
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '0000360491', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        INSERT testCaseObj;
        
        Test.startTest(); 
        EmailMessage testEmailMessageObj = TestDataFactory.createTestEmailMessage('Test Text Body', 'Test email',testAccountObj.Id);
        testEmailMessageObj.FromAddress = 'test@abc.org';
        testEmailMessageObj.Incoming = True;
        testEmailMessageObj.ToAddress= 'test@xyz.org';
        testEmailMessageObj.HtmlBody = 'Test email body';
        testEmailMessageObj.ParentId = testCaseObj.Id; 
        testEmailMessageObj.Status ='2';
        INSERT testEmailMessageObj;
        
        Test.stopTest();  
        //Asserts
        system.assertEquals('0000360491', testCaseObj.Velocity_Number__c);
        system.assertEquals('Test email', testEmailMessageObj.Subject);
    }
    
    @isTest
    private static void testUpdateSendEmailtoAlmsTrigger() {
        Account testAccountObj = [SELECT Id,Name FROM Account LIMIT 1];
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '0000360491', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        INSERT testCaseObj;
        
        Test.startTest(); 
        EmailMessage testEmailMessageObj = TestDataFactory.createTestEmailMessage('Test Text Body', 'Test email', testAccountObj.Id);
        testEmailMessageObj.FromAddress = 'test@abc.org';
        testEmailMessageObj.Incoming = False;
        testEmailMessageObj.ToAddress= 'test@xyz.org';
        testEmailMessageObj.HtmlBody = 'Test email body';
        testEmailMessageObj.ParentId = testCaseObj.Id; 
        testEmailMessageObj.Status ='2';
        INSERT testEmailMessageObj;
        
        testEmailMessageObj.Status = '3';
        UPDATE testEmailMessageObj;
        Test.stopTest();  
        //Asserts
        system.assertEquals('0000360491', testCaseObj.Velocity_Number__c);
        system.assertEquals('Test email', testEmailMessageObj.Subject);
    }
}