/**
 * @description       : Mock class to mock REST API request
 * @CreatedBy         : CloudWerx
 * @UpdatedBy         : CloudWerx
**/
@isTest
public class HTTPCalloutRequestMock implements HttpCalloutMock {
    //REST Mock
    protected Integer code;
    protected String status;
    protected String bodyAsString;
    protected Blob bodyAsBlob;
    protected Map<String, String> responseHeaders;
    
    public HTTPCalloutRequestMock(Integer code, String status, String body, Map<String, String> responseHeaders){
        this.code = code;
        this.status = status;
        this.bodyAsBlob = null;
        this.bodyAsString = body;
        this.responseHeaders = responseHeaders;
    }
    
    public HTTPCalloutRequestMock(Integer code, String status, Blob body, Map<String, String> responseHeaders){
        this.code = code;
        this.status = status;
        this.bodyAsBlob = body;
        this.bodyAsString = null;
        this.responseHeaders = responseHeaders;
    }
    
    public HTTPResponse respond(HTTPRequest req){
        CalloutException e;
        HTTPResponse resp = new HttpResponse();
        if(code != null) {
            resp.setStatusCode(code);
        } else {
            e = (CalloutException)CalloutException.class.newInstance();
            e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings.');
            //throw e;
        }
        
        resp.setStatus(status);
        if(bodyAsBlob != null) {
            resp.setBodyAsBlob(bodyAsBlob);
        } else if(e != null) {
            resp.setBody(String.valueOf(e));
        } else {
            resp.setBody(bodyAsString);
        }
        if(responseHeaders != null){
            for(String key: responseHeaders.keySet()){
                resp.setHeader(key, responseHeaders.get(key));
            }
        }
        return resp;
    }        
}