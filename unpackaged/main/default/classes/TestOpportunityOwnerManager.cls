/////////////////////////////////////////////////////////////
//An apex class to test /provide coverage on two triggers: //
//(1.) SetLeadownerManager                                 //
//(2.)SetOpportunityOwnersManager                          //
//by Ed Stachyra                                           //
/////////////////////////////////////////////////////////////

@isTest

private class TestOpportunityOwnerManager
{
	
	
     //User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
     // System.runAs( thisUser ) {
     //...your setup-object DML...   	
        	
    static testMethod void testSetLeadManagerTrigger()
    {
    	Test.startTest();
    	
    	//a user that is both in prod and sandbox to clone from
    	User initUser = [SELECT Id, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey FROM User WHERE Alias='SADMIN'];
    	
        //Create 4 Users
    	User u1 = initUser.clone();
    	u1.Alias = 'Joe';
    	u1.LastName = 'Smith';
    	u1.Email = 'joe@virginaustralia.com';
    	u1.Username = 'joe@virginaustralia.com';
    	u1.CommunityNickname = 'Joe';    		
    	insert u1;
    	 	
    	///User u2 = new User();
    	User u2 = initUser.clone();
    	u2.Alias = 'Susan';
    	u2.LastName = 'Smith';
    	u2.Email = 'Susan@virginaustralia.com';
    	u2.Username = 'Susan@virginaustralia.com';
    	u2.CommunityNickname = 'Susan';	
    	insert u2;
    		
    	//User u3 = new User();
    	User u3 = initUser.clone();
    	u3.Alias = 'Peter';
    	u3.LastName = 'Smith';
    	u3.Email = 'Peter@virginaustralia.com';
    	u3.Username = 'Peter@virginaustralia.com';
    	u3.CommunityNickname = 'Peter';	
    	insert u3;
    	
        //User u4 = new User();
        User u4 = initUser.clone();
    	u4.Alias = 'Jan';
    	u4.LastName = 'Smith';
    	u4.Email = 'Jan@virginaustralia.com';
    	u4.Username = 'Jan@virginaustralia.com';
    	u4.CommunityNickname = 'Jan';
    	insert u4;
    	
    	//User u5 = new User();
        User u5 = initUser.clone();
    	u5.Alias = 'Jed';
    	u5.LastName = 'Smith';
    	u5.Email = 'Jed@virginaustralia.com';
    	u5.Username = 'Jed@virginaustralia.com';
    	u5.CommunityNickname = 'Jed';
    	insert u5;
    	
    	//Now that users exist - modify their contract approvers to each other
   		u1.contract_approver__c = u2.Id; 
   		update u1;
   		u2.contract_approver__c = u3.Id;
   		update u2;
   		u3.contract_approver__c = u4.Id;
   		update u3;
   		u4.contract_approver__c = u5.Id;
   		update u4;	
     	
  //********* Lead  Owner Manager Change Portion of Test Class *****************//    
      	
      	System.runAs( initUser ) {
         // Lead test setup code
         Lead b = new Lead();
         b.OwnerId = u1.Id;     
         b.LastName = 'test500'; 
         b.Company = 'TEST'; 
         b.Lead_Type__c = 'Corporate';
         b.Target_Markets__c = 'Thailand';
         b.Estimated_Spend__c = '1m+';
         b.Travel_Policy__c = 'Flexi Only';
         b.Booked_Via__c = 'TMC';
         insert b;
         
       //send a debug entry to the log for this new Lead
        System.debug('Lead Owner before Lead Ownership change is: '+ b.Owner);
        System.debug('Lead Owner Manager before Lead Ownership change is: '+ b.Lead_Owner_Manager__c);
   	
    	//Testing the trigger by Assigning a new owner to the Lead
    	b.OwnerId = u3.Id;
	    update b;        //insert the lead into the database
         
        //send a debug entry to the log for this Lead after the trigger is ran
        System.debug('Lead Owner after Lead Ownership change is: '+ b.Owner);
        System.debug('Lead Owner Manager after Lead Ownership change is: '+ b.Lead_Owner_Manager__c);
         
         ///}    //To cure the Setup -object DML issue
    	
        //Refresh memory from the DB and test do the Assert test
        b = [SELECT Id, OwnerId FROM Lead WHERE Id=:b.Id];
        system.assertEquals(u3.Id, b.OwnerId); //Passed
        
//********* Opportunity Owner Manager Change Portion of Test Class *****************//        
         CommonObjectsForTest commonObjForTest = new CommonObjectsForTest();
        
        Account account = new Account();
        account = commonObjForTest.CreateAccountObject(0);
  		insert account; 
           
        //setting up the opportunity
         Opportunity a = new Opportunity();
         a.OwnerId = u1.Id; 
         a.AccountId = account.id ;
         a.Name = 'testOpp';
        //a.AccountId = '001';
         a.StageName = 'Open';
         a.CloseDate = Date.today();
         a.Amount = 1000.00;
         insert a;
         
        //send a debug entry to the log for the new Opportunity prior to Change
         System.debug('Opportunity Owner before Opportunity Ownership Change is: '+ a.Owner);
         System.debug('Opportunity Owner Manager before Opportunity Ownership Change is: '+ a.Opportunity_Owner_Manager__c);
          
        //Assigning a new owner to this Opportunity
    	a.OwnerId = u3.Id;
		update a;        //insert the modified opportunity into the database

        //Refresh memory from the DB
        a = [SELECT Id, OwnerId FROM Opportunity WHERE Id=:a.Id];
        system.assertEquals(u3.Id, a.OwnerId); //Passed
                 
    	//send a debug entry to the log for the Opportunity post Change
       // System.debug('Opportunity Owner after Opportunity Ownership Change is: '+ a.Owner);
       // System.debug('Opportunity Owner Manager after Opportunity Ownership Change is: '+ a.Opportunity_Owner_Manager__c); 
      	}
        Test.stopTest();
    }
  
    
}