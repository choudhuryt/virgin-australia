public with sharing class SIPInputControllerEdit {

	public List<SIPHeaderWrapper> allObjectItem{get; set;}
	public String contractId { get; set; }
	public String sipOptionId { get; set; }	
	public String ShowOptionTitle{get; set;}
	
	public String GoBackLink {get;set;}
	public List<String> DeleteId{set; get;}
	public list<SIP_Header__c> SIPHeaderDelete{get;set;}
	public String SIPOptionName{get;set;}
	
	public String SIPOptionTitle{get;set;}
	
    public SIPInputControllerEdit()
	{		
		ShowOptionTitle = 'block';
		if(contractId == null)
		{
			try
			{
				contractId = ApexPages.currentPage().getParameters().get('cid');
			}
			catch (Exception ex)
			{
			}
		}
		if(sipOptionId == null)
		{
			try
			{
				sipOptionId = ApexPages.currentPage().getParameters().get('sipid');
			}
			catch (Exception ex)
			{
			}
		}
		GoBackLink = '/home/home.jsp';
	}	
	public PageReference init ()
	{
		PageReference url = null;
		if(contractId == null)
		{
			url = new PageReference(GoBackLink);
			url.setRedirect(true);	
		}
		else
		{
			
			List<SIP_Header__c> SIPHeaderExisted = 
                
		  [Select s.Type__c,s.Key_Market__c, 
			s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, 
			s.Name, s.LastModifiedDate, s.LastModifiedById, 
			s.IsTemplate__c, s.IsDeleted, s.Id, 
			s.Full_Last_Year_Actual_Revenue__c, 
			s.Est_Current_Full_Year_Revenue__c, s.Description__c, 
			s.CreatedDate, s.CreatedById, s.Contract__c, 
			(Select rb.Upper_Percent__c, rb.Tier__c, rb.SystemModstamp, 
			rb.SIP_Rebate_Parent__c, rb.SIP_Percent__c, rb.SIP_Header__c,
			 rb.Name, rb.Lower_Percent__c, rb.LastModifiedDate,
			  rb.LastModifiedById, rb.JH_Tier__c, rb.IsDeleted, 
			  rb.Id, rb.Current_Tier__c, rb.CreatedDate,
			   rb.CreatedById, rb.Contract_Growth__c, 
			   rb.Calculated_Upper_Amount__c, 
			   rb.Calculated_Lower_Amount__c From SIP_Rebate__r rb order by rb.Lower_Percent__c asc )
			    From SIP_Header__c s where s.Contract__c =:contractId order by s.Description__c];  
			    
			    
			    
			if( SIPHeaderExisted == null || SIPHeaderExisted.size() < 1)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'There is not existing SIP tables for this contract.If you want to add new tables, please click on the Add SIP Table button'));          
			    return null;
				
			}
			else
			{
				
				GoBackLink = '/' + contractID;
			}
		}
        system.debug('Gobacklink is'  + GoBackLink );
		return url;
	}
	
	public List<SIPHeaderWrapper> GetSIPOptionList()
	{
		
		if(allObjectItem == null)
		{			
			//check existed header 
			List<SIP_Header__c> SIPHeaderExisted =                
             [Select s.Type__c, s.SystemModstamp, s.Key_Market__c,
			s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, 
			s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, 
			s.Full_Last_Year_Actual_Revenue__c, 
			s.Est_Current_Full_Year_Revenue__c, s.Description__c, 
			s.CreatedDate, s.CreatedById, s.Contract__c, 
			(Select rb.Upper_Percent__c, rb.Tier__c, rb.SystemModstamp, 
			rb.SIP_Rebate_Parent__c,
			 rb.SIP_Percent__c, rb.SIP_Header__c, rb.Name, 
			 rb.Lower_Percent__c, rb.LastModifiedDate, 
			 rb.LastModifiedById, rb.JH_Tier__c, rb.IsDeleted, rb.Id, 
			 rb.Current_Tier__c, rb.CreatedDate, rb.CreatedById, 
			 rb.Contract_Growth__c, rb.Calculated_Upper_Amount__c, 
			 rb.Calculated_Lower_Amount__c From SIP_Rebate__r rb order by Lower_Percent__c asc ) 
			 From SIP_Header__c s where s.Contract__c =:contractId ]; 
            
			if( SIPHeaderExisted != null && SIPHeaderExisted.size()>0)
			{
				ShowOptionTitle = 'none';
				GetExistedSIPHeaderList(SIPHeaderExisted);
               
			}
			else
			{
				SIP_Option__c sipOptionItem = new SIP_Option__c(); 
				if(sipOptionId!=null)
				{
					//get sip option based on the selected sip option
					
					sipOptionItem  = [Select s.Unique_ID__c,  s.SIP_Selection__c, s.Option__c, s.Name, s.Id, s.Description__c From SIP_Option__c s where s.Id=:sipOptionId];
					SIPOptionName =sipOptionItem.Name +  ' - '+sipOptionItem.Description__c ;
					
					List<SIP_Header__c> SIPHeaders = [Select s.Type__c, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Key_Market__c,
																s.IsTemplate__c, s.Id, s.Full_Last_Year_Actual_Revenue__c,
																s.Est_Current_Full_Year_Revenue__c, s.Description__c,
																(Select Id, Upper_Percent__c, Tier__c, SIP_Rebate_Parent__c,
																	Name, SIP_Percent__c, Lower_Percent__c, Current_Tier__c 
																	From SIP_Rebate__r order by Lower_Percent__c asc) 
																From SIP_Header__c s 
																Where s.IsTemplate__c = true 
																and s.Id in (
																	select SIP_Header__c 
																	from SIP_Option_Join__c 
																	where SIP_Option__c = :sipOptionItem.Id) 
																	order by Description__c];
				
					List<SIPHeaderWrapper> SIPHeaderWrapperList = new List<SIPHeaderWrapper>(); 
					
					for(SIP_Header__c SIPHeaderTmp : SIPHeaders )
					{	
					
						SIPHeaderWrapper SIPHeaderWrapperItem = new SIPHeaderWrapper();
						
						SIPHeaderWrapperItem.headerItem= SIPHeaderTmp.clone(false);
						//here for revenue
						Contract contract = new Contract();
						contract = (Contract) [select Total_Estimated_Revenue__c,Total_LY_Revenue__c From Contract where id =:contractId];
						
						//get sip rebate lsit
						List<SIP_Rebate__c> SIPRebateList = SIPHeaderTmp.SIP_Rebate__r;
						
						List<SIP_Rebate__c> SIPRebateListTmp = new List<SIP_Rebate__c>();
						for(SIP_Rebate__c item: SIPRebateList)
						{
							SIP_Rebate__c itemTmp = item.clone(false);
							SIPRebateListTmp.add(itemTmp);
						}
						
						if(SIPHeaderWrapperItem.headerItem.Est_Current_Full_Year_Revenue__c == null ){
						SIPHeaderWrapperItem.headerItem.Est_Current_Full_Year_Revenue__c = contract.Total_Estimated_Revenue__c;
						SIPHeaderWrapperItem.headerItem.Full_Last_Year_Actual_Revenue__c = contract.Total_LY_Revenue__c;
						}
						SIPHeaderWrapperItem.sipRebateList =SIPRebateListTmp;
						SIPHeaderWrapperList.add(SIPHeaderWrapperItem);
					}
					allObjectItem = SIPHeaderWrapperList;
				}
			}
		}
		return allObjectItem;
	}
	
	//Get existed header
	public List<SIPHeaderWrapper> GetExistedSIPHeaderList(List<SIP_Header__c> SIPHeaderList)
	{
		if(allObjectItem == null)
		{
			List<SIPHeaderWrapper> SIPHeaderWrapperList = new List<SIPHeaderWrapper>();
			for(SIP_Header__c SIPHeaderItem :  SIPHeaderList)
			{
				SIPHeaderWrapper SIPHeaderWrapperItem = new SIPHeaderWrapper();
				SIPHeaderWrapperItem.headerItem = SIPHeaderItem;
				SIPHeaderWrapperItem.TempSIPCalculation = SIPHeaderItem.SIP_Calculation__c;
				SIPHeaderWrapperItem.SIPRebateList = SIPHeaderItem.SIP_Rebate__r;
				SIPHeaderWrapperList.add(SIPHeaderWrapperItem); 
			}
			allObjectItem = SIPHeaderWrapperList;
		}
		return allObjectItem;
	}
    @TestVisible private Boolean CheckInputLevel()
	{
		Boolean value= true;
		if(allObjectItem != null && allObjectItem.size()> 0)		
		{
				Integer section=1;
				Boolean isFirst = true;
				String FirstHeader ='';
				for(SIPHeaderWrapper SIPHeaderWrapperItem : allObjectItem)
				{
					Integer level=1;
					if(isFirst)
					{
						FirstHeader = SIPHeaderWrapperItem.headerItem.Type__c;
						isFirst= false;
					}
					else if(SIPHeaderWrapperItem.headerItem.Type__c !=FirstHeader  && SIPHeaderWrapperItem.headerItem.Type__c != 'Key Market')
					{
						ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Header type at section ' + section + ' must be the same as the first header type'));
						value = false;
					}
					String Header =  SIPHeaderWrapperItem.headerItem.Description__c + ' at SIP Header section ' + section;
					SIP_Rebate__c preSIPRebateItem = null;
					for(SIP_Rebate__c SIPRebateItem : SIPHeaderWrapperItem.sipRebateList )
					{						
						if(SIPHeaderWrapperItem.headerItem.Full_Last_Year_Actual_Revenue__c == null)
						{
							ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Header +  ' please enter "Full Last Year Actual Revenue"'));
							value = false;
						}					
						if(SIPHeaderWrapperItem.headerItem.Est_Current_Full_Year_Revenue__c == null)
						{
							ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Header + ' please enter "Est Current Full Year Revenue"'));
							value = false;
						}
						if(preSIPRebateItem == null)
						{
							if(SIPRebateItem.Upper_Percent__c < SIPRebateItem.Lower_Percent__c)
							{
								ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Header + ' at Level ' + level +' Upper% must be greater than Lower%'));
								value = false;
							}
						}
						else
						{						
							if(SIPRebateItem.Lower_Percent__c < preSIPRebateItem.Upper_Percent__c)
							{
								ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Header  + ' at Level ' + level +' the Lower% must be greater than Upper% at level ' + (level-1)));							 
								value = false;
							}
							if(SIPRebateItem.Upper_Percent__c < SIPRebateItem.Lower_Percent__c)
							{
								ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,+ Header + ' at Level ' + level +' Upper% must be greater than Lower%'));
								preSIPRebateItem = SIPRebateItem;
								value = false;
							}
						}
						preSIPRebateItem = SIPRebateItem;
						level++;
					}
					section++;
				}
		}
		DeleteHeader();
		return value;
	}
	
	public PageReference Save() {
		
		if(CheckInputLevel())
		{
			//Call insert method
			SaveOrUpdate();
		}
		return null;
	  	
	}
	public PageReference SaveAndClose() {
		
		if(CheckInputLevel())
		{
			//Call insert method
			SaveOrUpdate();
			PageReference nextPage = new PageReference('/'+contractID);
			nextPage.setRedirect(true);
    	return nextPage;
		}
		return null;
	  	
	}
	public PageReference RemoveHeader() {
		
		String headerId = ApexPages.currentPage().getParameters().get('currentID');
		Remove(headerId);
		return null;
	}
	
	public void Remove(String name)
	{
		if(!allObjectItem.isEmpty())
		{
			for(Integer i = allObjectItem.size()-1; i>=0; i--)
			{
				if(allObjectItem[i].headerItem.Description__c == name)
				{
					if(allObjectItem[i].headerItem.Id !=null)
					{
						if(SIPHeaderDelete == null)
						{
							SIPHeaderDelete = new list<SIP_Header__c>();
						}
						SIPHeaderDelete.add(allObjectItem[i].headerItem);
					}
					allObjectItem.remove(i);
					break;
				}
			}
		}
	}
	private void SaveOrUpdate()
	{
		if(allObjectItem != null && allObjectItem.size()> 0)
		{
			for(SIPHeaderWrapper SIPHeaderWrapperItem : allObjectItem)
			{
				try
				{			
					SIPHeaderWrapperItem.headerItem.Contract__c = contractId;
				}
				catch (Exception ex)
				{
				}
				SIPHeaderWrapperItem.headerItem.IsTemplate__c = false;
				
				for(SIP_Rebate__c SIPRebateItem : SIPHeaderWrapperItem.sipRebateList )
				{						 
					if( SIPHeaderWrapperItem.headerItem.Est_Current_Full_Year_Revenue__c 
						>= ((SIPHeaderWrapperItem.headerItem.Full_Last_Year_Actual_Revenue__c * (SIPRebateItem.Lower_Percent__c)/100)
						+ SIPHeaderWrapperItem.headerItem.Full_Last_Year_Actual_Revenue__c)
						&& SIPHeaderWrapperItem.headerItem.Est_Current_Full_Year_Revenue__c
						<= ((SIPHeaderWrapperItem.headerItem.Full_Last_Year_Actual_Revenue__c * (SIPRebateItem.Upper_Percent__c)/100) 
						+ SIPHeaderWrapperItem.headerItem.Full_Last_Year_Actual_Revenue__c))
					{
						SIPHeaderWrapperItem.headerItem.SIP_Percentage__c = SIPRebateItem.SIP_Percent__c;
						SIPHeaderWrapperItem.TempSIPCalculation = SIPHeaderWrapperItem.headerItem.Est_Current_Full_Year_Revenue__c * (SIPRebateItem.SIP_Percent__c/100);
						break;
					}
				}
			}
			UpsertValue();
			
			//Delete the deleted record 
			DeleteHeader();
		}
	}	
	
	private void UpsertValue()
	{
		list<SIP_Header__c> SIPHeaderObjectList = new list<SIP_Header__c>();
		list<SIP_Rebate__c> SIPSIPRebateList = new list<SIP_Rebate__c>();
		for(SIPHeaderWrapper SIPHeaderWrapperItem : allObjectItem)
		{
			  SIPHeaderObjectList.add(SIPHeaderWrapperItem.headerItem);
		}
		upsert SIPHeaderObjectList;
		
		for(SIPHeaderWrapper SIPHeaderWrapperItem : allObjectItem)
		{
			for(SIP_Rebate__c SIPRebateItem : SIPHeaderWrapperItem.sipRebateList)
			{
				try
				{
					SIPRebateItem.SIP_Header__c = SIPHeaderWrapperItem.headerItem.Id;
				}
				catch (Exception ex)
				{
				}
				SIPSIPRebateList.add(SIPRebateItem);
			}
		}
		
		upsert SIPSIPRebateList;
		
		
	}
	
	public void DeleteHeader()
	{
		if(SIPHeaderDelete != null && SIPHeaderDelete.size()>0)
		{
			try
			{
				delete SIPHeaderDelete;
				SIPHeaderDelete = null;
			}
			catch(Exception ex)
			{
			}
		} 
	}
	
	public PageReference GoBack()
	{
		PageReference url = null;
		url = new PageReference(GoBackLink);
		url.setRedirect(true);
		return url;
	}
	
	public PageReference Cancel()
	{
		PageReference url = null;
		if(contractId != null)
		{
			url = new PageReference('/'+ contractId);
		}
		else
		{
				url = new PageReference('/');
		}
		url.setRedirect(true);
		return url;
	}
	
	public class SIPHeaderWrapper{
		public SIP_Header__c headerItem {get; set;}
		public List<SIP_Rebate__c> sipRebateList {get; set;}
		//Est_Current_Full_Year_Revenue__c * SIP_Percentage__c
		public Decimal TempSIPCalculation{get;set;}
		
		public SIPHeaderWrapper()
		{
		}
	}
}