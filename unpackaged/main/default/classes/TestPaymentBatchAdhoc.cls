/**
* @description       : Test class for NewMarketingSpendDownController
* @createdBy         : Cloudwerx
* @Updated By        : Cloudwerx
**/
@isTest
private class TestPaymentBatchAdhoc {
    
    @testSetup static void setup() {
        Account testAccObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        testAccObj.recordTypeId = Utilities.getRecordTypeId('Account', 'Accelerate');
        testAccObj.Sales_Matrix_Owner__c = 'Accelerate';
        testAccObj.Corporate_ID__c = '124';
        testAccObj.Corporate_Travel_Bank_Account_Number__c = '233';
        INSERT testAccObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('Test Contract', testAccObj.Id, 'Draft', '15', null, true, Date.newInstance(2021,04,01));
        testContractObj.RecordTypeId = new List<Id>(ContractSpendCalculation.contractRecordTypeIds)[0];
        INSERT testContractObj;
        testContractObj.status = 'Activated';
        UPDATE testContractObj;
        
        Contract testContractObj1 = TestDataFactory.createTestContract('Test Contract 1', testAccObj.Id, 'Draft', '15', null, true, Date.newInstance(2021,04,01));
        testContractObj1.RecordTypeId = Utilities.getRecordTypeId('Contract', 'SmartFly');
        INSERT testContractObj1;
        testContractObj1.status = 'Activated';
        UPDATE testContractObj1;
        
        List<Spend__c> testSpends = new List<Spend__c>();
        testSpends.add(TestDataFactory.createTestSpend(testAccObj.Id, testContractObj.Id, 200000, 250000, 'D', 8, 2021));
        testSpends.add(TestDataFactory.createTestSpend(testAccObj.Id, testContractObj.Id, 200000, 250000, 'L', 8, 2021));
        testSpends.add(TestDataFactory.createTestSpend(testAccObj.Id, testContractObj.Id, 200000, 250000, 'T', 8, 2021));
        testSpends.add(TestDataFactory.createTestSpend(testAccObj.Id, testContractObj.Id, 200000, 250000, 'S', 8, 2021));
        INSERT testSpends;
    }
    
    @isTest
    private static void testPaymentBatchAdhoc() {
        Account testAccObj = [SELECT Id FROM Account LIMIT 1];
        Contract testContractObj = [SELECT Id FROM Contract LIMIT 1];
        Test.startTest();
        Id JobId = Database.executeBatch(new PaymentBatchAdhoc());
        Test.stopTest();
        // Asserts
        List<Payments__c> insertedPayments = [SELECT Id, Account__c, Contract__c, Reward_Travel_Bank_User_Name__c, 
                                              Reward_Travel_Bank_Number__c, Total_Expenditure__c, Total_Eligible_Expenditure__c,
                                              Rebate_Percentage__c, Intl_Eligible_Expenditure__c, TT_Eligible_Expenditure__c, 
                                              Domestic_Eligible_Expenditure__c, Payment_Type__c, SmartFly_Dom_Eligible__c, SmartFly_tt_Eligible__c,  
                                              GST__c, Payment_Earned_On_Domestic__c, Payment_Earned_On_Intl__c, Payment_Amount__c, 
                                              FCTG_Dom_Rebate_Recovery__c, FCTG_TT_Rebate_Recovery__c
                                              FROM Payments__c];
        // Asserts
        System.assertEquals(true, insertedPayments.size() > 0);
        System.assertEquals(testAccObj.Id, insertedPayments[0].Account__c);
        System.assertEquals(testContractObj.Id, insertedPayments[0].Contract__c);
        System.assertEquals('124', insertedPayments[0].Reward_Travel_Bank_User_Name__c);
        System.assertEquals('233', insertedPayments[0].Reward_Travel_Bank_Number__c);
        System.assertEquals(1000000.00, insertedPayments[0].Total_Expenditure__c);
        System.assertEquals(800000.00, insertedPayments[0].Total_Eligible_Expenditure__c);
        System.assertEquals(0.0500, insertedPayments[0].Rebate_Percentage__c);
        System.assertEquals(400000.00, insertedPayments[0].Intl_Eligible_Expenditure__c);
        System.assertEquals(200000.00, insertedPayments[0].TT_Eligible_Expenditure__c);
        System.assertEquals(200000.00, insertedPayments[0].Domestic_Eligible_Expenditure__c);
        System.assertEquals('Sales Payment', insertedPayments[0].Payment_Type__c);
        System.assertEquals(0.00, insertedPayments[0].SmartFly_Dom_Eligible__c);
        System.assertEquals(0.00, insertedPayments[0].SmartFly_TT_Eligible__c);
        System.assertEquals(909.090909090909090909090909090909, insertedPayments[0].GST__c);
        System.assertEquals(9090.909090909090909090909090909091, insertedPayments[0].Payment_Earned_On_Domestic__c);
        System.assertEquals(30000.00, insertedPayments[0].Payment_Earned_On_Intl__c);
        System.assertEquals(14090.909090909090909090909090909091, insertedPayments[0].Payment_Amount__c);
        System.assertEquals(0.00, insertedPayments[0].FCTG_Dom_Rebate_Recovery__c);
        System.assertEquals(0.00, insertedPayments[0].FCTG_TT_Rebate_Recovery__c);
    }
    
    @isTest
    private static void testPaymentBatchAdhocSmartFly() {
        Contract testContractObj = [SELECT Id FROM Contract LIMIT 1];
        Account testAccObj = [SELECT Id, recordTypeId FROM Account LIMIT 1];
        testAccObj.recordTypeId = new List<Id>(ContractSpendCalculation.accountRecordTypeIds)[0];
        UPDATE testAccObj;
        Test.startTest();
        Id JobId = Database.executeBatch(new PaymentBatchAdhoc());
        Test.stopTest();
        // Asserts
        List<Payments__c> insertedPayments = [SELECT Id, Account__c, Contract__c, Total_Expenditure__c, Total_Eligible_Expenditure__c,
                                              Rebate_Percentage__c, Intl_Eligible_Expenditure__c, TT_Eligible_Expenditure__c, 
                                              Domestic_Eligible_Expenditure__c, Payment_Type__c, SmartFly_Dom_Eligible__c, SmartFly_tt_Eligible__c,  
                                              GST__c, Payment_Earned_On_Domestic__c, Payment_Earned_On_Intl__c, Payment_Amount__c, 
                                              SmartFLY_TT_Rebate__c, FCTG_Dom_Rebate_Recovery__c, FCTG_TT_Rebate_Recovery__c
                                              FROM Payments__c];
        // Asserts
        System.assertEquals(true, insertedPayments.size() > 0);
        System.assertEquals(testAccObj.Id, insertedPayments[0].Account__c);
        System.assertEquals(testContractObj.Id, insertedPayments[0].Contract__c);
        System.assertEquals(1000000.00, insertedPayments[0].Total_Expenditure__c);
        System.assertEquals(800000.00, insertedPayments[0].Total_Eligible_Expenditure__c);
        System.assertEquals(0.0500, insertedPayments[0].Rebate_Percentage__c);
        System.assertEquals(400000.00, insertedPayments[0].Intl_Eligible_Expenditure__c);
        System.assertEquals(200000.00, insertedPayments[0].TT_Eligible_Expenditure__c);
        System.assertEquals(200000.00, insertedPayments[0].Domestic_Eligible_Expenditure__c);
        System.assertEquals('SmartFLY Rebate', insertedPayments[0].Payment_Type__c);
        System.assertEquals(200000.00, insertedPayments[0].SmartFly_Dom_Eligible__c);
        System.assertEquals(200000.00, insertedPayments[0].SmartFly_TT_Eligible__c);
        System.assertEquals(1272.72727272727272727272727272727, insertedPayments[0].GST__c);
        System.assertEquals(12727.27272727272727272727272727273, insertedPayments[0].Payment_Earned_On_Domestic__c);
        System.assertEquals(34000.00, insertedPayments[0].Payment_Earned_On_Intl__c);
        System.assertEquals(46727.27272727272727272727272727273, insertedPayments[0].Payment_Amount__c);
        System.assertEquals(14000.00, insertedPayments[0].SmartFLY_TT_Rebate__c);
        System.assertEquals(2000.00, insertedPayments[0].FCTG_Dom_Rebate_Recovery__c);
        System.assertEquals(2000.00, insertedPayments[0].FCTG_TT_Rebate_Recovery__c);
    }
}