@isTest
private class TestOppContractDiscountDetails {
    
    static testMethod void myUnitTestContract() 
    {
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';
       
        insert account;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.StageName = 'Sales Opportunity Analysis'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1; 
        
        Contract con = new Contract();
        con = commx.CreateOldStandardContract(0);
        con.AccountId = account.id;
        con.Opportunity__c = opp1.id ;
        con.SQ_Discount_Type__c = 'Specific OD Pairs';
        insert con;
        
        
        
        Market__c market1 = new Market__c();
        market1.Opportunity__c = opp1.id ;
        market1.Contract__c = con.id ;
        market1.Name = 'DOM Mainline';
        insert market1;
        
        Additional_Benefits__c addben1 = new  Additional_Benefits__c();
        addben1.Opportunity__c = opp1.id ;
        addben1.Contract__c = con.id ; 
        addben1.Name = opp1.Name ;
        insert  addben1;
        
        Proposal_Table__c  prop1 = new  Proposal_Table__c();
        prop1.Opportunity__c = opp1.id ;
        prop1.Contract__c = con.id ;
        prop1.Name = 'DOM Mainline'; 
        insert prop1 ;
        
        Proposal_Table__c  prop2 = new  Proposal_Table__c();
        prop2.Opportunity__c = opp1.id ;
        prop2.Contract__c = con.id ;
        prop2.Name = 'INT Short Haul';   
        insert prop2;
        
        Proposal_Table__c  prop6 = new  Proposal_Table__c();
        prop6.Opportunity__c = opp1.id ;
        prop6.Contract__c = con.id ;
        prop6.Name = 'China';   
        insert prop6; 
        
        Proposal_Table__c  prop3 = new  Proposal_Table__c();
        prop3.Opportunity__c = opp1.id ;
        prop3.Contract__c = con.id ;
        prop3.Name = 'UK/Europe SQ'; 
        insert prop3 ;
        
        Proposal_Table__c  prop4 = new  Proposal_Table__c();
        prop4.Opportunity__c = opp1.id ;
        prop4.Contract__c = con.id ;
        prop4.Name = 'North America';  
        insert prop4 ;
        
        Proposal_Table__c  prop5 = new  Proposal_Table__c();
        prop5.Opportunity__c = opp1.id ;
        prop5.Contract__c = con.id ;
        prop5.Name = 'London';  
        insert prop5 ; 
        
        Test.StartTest(); 
        
        PageReference pageRef1 = Page.ContractWithDiscountData; 
        pageRef1.getParameters().put('cid',con.Id);
        pageRef1.getParameters().put('Region','DOMREG');  
        Test.setCurrentPage(pageRef1);
        ContractDiscountViewController dController = new ContractDiscountViewController();
        dController.initDisc();
        dController.cancel();
        dController.cancel1();
        
        PageReference pageRef2 = Page.ContractWithDiscountData; 
        pageRef2.getParameters().put('cid',con.Id);
        pageRef2.getParameters().put('Region','INTSH');  
        Test.setCurrentPage(pageRef2);
        ContractDiscountViewController iController = new ContractDiscountViewController();
        iController.initDisc();
        
        PageReference pageRef3 = Page.ContractWithDiscountData; 
        pageRef3.getParameters().put('cid',con.Id);
        pageRef3.getParameters().put('Region','LONDON');  
        Test.setCurrentPage(pageRef3);
        ContractDiscountViewController lController = new ContractDiscountViewController();
        lController.initDisc();  
        
        
        /* PageReference pageRef4 = Page.ContractWithDiscountData; 
pageRef4.getParameters().put('cid',con.Id);
pageRef4.getParameters().put('Region','SQ');  
Test.setCurrentPage(pageRef4);
ContractDiscountViewController sqController = new ContractDiscountViewController();
sqController.initDisc();  

PageReference pageRef6 = Page.ContractWithDiscountData; 
pageRef6.getParameters().put('cid',con.Id);
pageRef6.getParameters().put('Region','NA');  
Test.setCurrentPage(pageRef6);
ContractDiscountViewController naController = new ContractDiscountViewController();
naController.initDisc();


PageReference pageRef7 = Page.ContractWithDiscountData; 
pageRef7.getParameters().put('cid',con.Id);
pageRef7.getParameters().put('Region','RED');  
Test.setCurrentPage(pageRef7);
ContractDiscountViewController redController = new ContractDiscountViewController();
redController.initDisc();*/
        
        Test.StopTest();
    }
    static testMethod void myUnitTestOpp() 
    {
        
        Account account	= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';        
        insert account;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.StageName = 'Sales Opportunity Analysis'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1; 
        
        Contract con = new Contract();
        con = commx.CreateOldStandardContract(0);
        con.AccountId = account.id;
        con.Opportunity__c = opp1.id ;
        con.SQ_Discount_Type__c = 'Specific OD Pairs';
        insert con;
        
        Market__c market1 = new Market__c();
        market1.Opportunity__c = opp1.id ;
        market1.Contract__c = con.id ;
        market1.Name = 'DOM Mainline';
        insert market1;
        
        Additional_Benefits__c addben1 = new  Additional_Benefits__c();
        addben1.Opportunity__c = opp1.id ;
        addben1.Contract__c = con.id ; 
        addben1.Name = opp1.Name ;
        insert  addben1;
        
        Proposal_Table__c  prop1 = new  Proposal_Table__c();
        prop1.Opportunity__c = opp1.id ;
        prop1.Contract__c = con.id ;
        prop1.Name = 'DOM Mainline'; 
        insert prop1 ;
        
        Proposal_Table__c  prop2 = new  Proposal_Table__c();
        prop2.Opportunity__c = opp1.id ;
        prop2.Contract__c = con.id ;
        prop2.Name = 'INT Short Haul';   
        insert prop2;
        
        Proposal_Table__c  prop6 = new  Proposal_Table__c();
        prop6.Opportunity__c = opp1.id ;
        prop6.Contract__c = con.id ;
        prop6.Name = 'China';   
        insert prop6; 
        
        Proposal_Table__c  prop3 = new  Proposal_Table__c();
        prop3.Opportunity__c = opp1.id ;
        prop3.Contract__c = con.id ;
        prop3.Name = 'UK/Europe SQ'; 
        insert prop3 ;
        
        Proposal_Table__c  prop4 = new  Proposal_Table__c();
        prop4.Opportunity__c = opp1.id ;
        prop4.Contract__c = con.id ;
        prop4.Name = 'North America';  
        insert prop4 ;
        
        Proposal_Table__c  prop5 = new  Proposal_Table__c();
        prop5.Opportunity__c = opp1.id ;
        prop5.Contract__c = con.id ;
        prop5.Name = 'London';  
        insert prop5 ; 
        
        Proposal_Table__c  propUA = new  Proposal_Table__c();
        propUA.Opportunity__c = opp1.id ;
        propUA.Contract__c = con.id ;
        propUA.Name = 'UK/Europe(QA)';  
        insert propUA ; 
        
        Proposal_Table__c  propQR = new  Proposal_Table__c();
        propQR.Opportunity__c = opp1.id ;
        propQR.Contract__c = con.id ;
        propQR.Name = 'Africa';  
        insert propQR ; 
        
        Test.StartTest(); 
        
        PageReference pageRef1 = Page.OpportunityWithDiscountData; 
        pageRef1.getParameters().put('cid',opp1.Id);
        pageRef1.getParameters().put('Region','DOMREG');  
        Test.setCurrentPage(pageRef1);
        OpportunityWithDiscountDataController dController = new OpportunityWithDiscountDataController();
        dController.initDisc();
        dController.cancel();
        dController.cancel1();
        
        PageReference pageRef2 = Page.OpportunityWithDiscountData; 
        pageRef2.getParameters().put('cid',opp1.Id);
        pageRef2.getParameters().put('Region','INTSH');  
        Test.setCurrentPage(pageRef2);
        OpportunityWithDiscountDataController iController = new OpportunityWithDiscountDataController();
        iController.initDisc();
        
        PageReference pageRef3 = Page.OpportunityWithDiscountData; 
        pageRef3.getParameters().put('cid',opp1.Id);
        pageRef3.getParameters().put('Region','LONDON');  
        Test.setCurrentPage(pageRef3);
        OpportunityWithDiscountDataController lController = new OpportunityWithDiscountDataController();
        lController.initDisc(); 
        
        
        
        /*PageReference pageRef6 = Page.OpportunityWithDiscountData; 
        pageRef6.getParameters().put('cid',opp1.Id);
        pageRef6.getParameters().put('Region','EY');  
        Test.setCurrentPage(pageRef6);
        OpportunityWithDiscountDataController aController = new OpportunityWithDiscountDataController();
        aController.initDisc();
        
        PageReference pageRef7 = Page.OpportunityWithDiscountData; 
        pageRef7.getParameters().put('cid',opp1.Id);
        pageRef7.getParameters().put('Region','SQ');  
        Test.setCurrentPage(pageRef7);
        OpportunityWithDiscountDataController bController = new OpportunityWithDiscountDataController();
        bController.initDisc();        */
        
        /*     PageReference pageRef4 = Page.OpportunityWithDiscountData; 
pageRef4.getParameters().put('cid',con.Id);
pageRef4.getParameters().put('Region','SQ');  
Test.setCurrentPage(pageRef4);
OpportunityWithDiscountDataController sqController = new OpportunityWithDiscountDataController();
sqController.initDisc();  

PageReference pageRef6 = Page.ContractWithDiscountData; 
pageRef6.getParameters().put('cid',con.Id);
pageRef6.getParameters().put('Region','NA');  
Test.setCurrentPage(pageRef6);
ContractDiscountViewController naController = new ContractDiscountViewController();
naController.initDisc();


PageReference pageRef7 = Page.ContractWithDiscountData; 
pageRef7.getParameters().put('cid',con.Id);
pageRef7.getParameters().put('Region','RED');  
Test.setCurrentPage(pageRef7);
ContractDiscountViewController redController = new ContractDiscountViewController();
redController.initDisc();*/
        
        Test.StopTest();
    }
    static testMethod void myUnitTestOpp1(){
        Account account	= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';        
        insert account;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.StageName = 'Sales Opportunity Analysis'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1; 
        
        Contract con = new Contract();
        con = commx.CreateOldStandardContract(0);
        con.AccountId = account.id;
        con.Opportunity__c = opp1.id ;
        con.SQ_Discount_Type__c = 'Specific OD Pairs';
        insert con;
        
        Market__c market1 = new Market__c();
        market1.Opportunity__c = opp1.id ;
        market1.Contract__c = con.id ;
        market1.Name = 'DOM Mainline';
        insert market1;
        
        Additional_Benefits__c addben1 = new  Additional_Benefits__c();
        addben1.Opportunity__c = opp1.id ;
        addben1.Contract__c = con.id ; 
        addben1.Name = opp1.Name ;
        insert  addben1;
        
        List<Proposal_Table__c> propTableList = new List<Proposal_Table__c>();
        
        for(integer i=0; i<5; i++){
            Proposal_Table__c  prop = new  Proposal_Table__c();
        	prop.Opportunity__c = opp1.id ;
        	prop.Contract__c = con.id ;
            if(i==0){
                prop.Name = 'North America';
            }else if(i==1){
                prop.Name = 'UK/Europe(QA)';
            }else if(i==2){
                prop.Name = 'UK/Europe(SQ)';
            }else if(i==3){
                prop.Name = 'Middle East(EY)';
            }
        	  
			propTableList.add(prop);
        }
        Insert propTableList;
        
        Test.startTest();
        
        PageReference pageRef2 = Page.OpportunityWithDiscountData; 
        pageRef2.getParameters().put('cid',opp1.Id);
        pageRef2.getParameters().put('Region','QR');  
        Test.setCurrentPage(pageRef2);
        OpportunityWithDiscountDataController Controller2 = new OpportunityWithDiscountDataController();
        Controller2.initDisc();
        
        PageReference pageRef3 = Page.OpportunityWithDiscountData; 
        pageRef3.getParameters().put('cid',opp1.Id);
        pageRef3.getParameters().put('Region','NA');  
        Test.setCurrentPage(pageRef3);
        OpportunityWithDiscountDataController Controller3 = new OpportunityWithDiscountDataController();
        Controller3.initDisc();
        
        PageReference pageRef4 = Page.OpportunityWithDiscountData; 
        pageRef4.getParameters().put('cid',opp1.Id);
        pageRef4.getParameters().put('Region','EY');  
        Test.setCurrentPage(pageRef4);
        OpportunityWithDiscountDataController Controller4 = new OpportunityWithDiscountDataController();
        Controller4.initDisc();
        
        /*PageReference pageRef5 = Page.OpportunityWithDiscountData; 
        pageRef5.getParameters().put('cid',opp1.Id);
        pageRef5.getParameters().put('Region','SQ');  
        Test.setCurrentPage(pageRef5);
        OpportunityWithDiscountDataController Controller5 = new OpportunityWithDiscountDataController();
        Controller5.initDisc();*/
        
        Test.stopTest();
    } 
}