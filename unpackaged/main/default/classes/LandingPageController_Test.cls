/*
 * Author: Warjie Malibago (Accenture CloudFirst)
 * Date: June 7, 2016
 * Description: Test class of LandingPageController
*/
@isTest
private class LandingPageController_Test {

	private static testMethod void testWithData() {
        LandingPageController l = new LandingPageController();
        l.getCurrentDate();
        ChatterSettings__c ch = new ChatterSettings__c(
            Name = 'Test',
            Staff_Notification__c = 'Test Notification',
            Company_Policies_Link__c = 'https://www.google.com.ph/',
            Latest_Video_Link__c = 'https://www.google.com.ph/',
            Staff_Offers_Link__c = 'https://www.google.com.ph/',
            Chatter_Group_Id__c = 'test id',
            Chatter_Group_Link__c = 'https://www.google.com.ph/',
            Internal_IT_Support_Link__c = 'https://www.google.com.ph/',
            News_Link__c = 'https://www.google.com.ph/'
        );
        insert ch;
        l.getChatterSetting();
        system.assertEquals(1, [SELECT COUNT() FROM ChatterSettings__c]);
	}
	
	private static testMethod void testWithoutData() {
        LandingPageController l = new LandingPageController();
        l.getCurrentDate();
        l.getChatterSetting();
        system.assertEquals(0, [SELECT COUNT() FROM ChatterSettings__c]);
	}
}