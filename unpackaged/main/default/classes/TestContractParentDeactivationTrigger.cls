@isTest
private class TestContractParentDeactivationTrigger
{

    static testMethod void test_parent_is_deactivatedAndRefCodesAreMoved()
    { 
        
        //setup account
        Account account = new Account();
        CommonObjectsForTest commonObjectForTest = new CommonObjectsForTest();
        account  =commonObjectForTest.CreateAccountObject(0);
   	
		insert account;
		

        //parent
        Contract contractOld = new Contract();
        
        
        contractOld.Status = 'Draft';
        contractOld.AccountId = account.id;
        contractOld.ContractTerm = 12;
        contractOld.StartDate = Date.newInstance(2011,01,01);
        contractOld.Contract_Version__c = 1;
        contractOld.Contract_Title__c = 'TEST';
        contractOld.Contract_Reference__c = 'TEST';
        insert contractOld; 
        
        Account_Tracking_Reference_Codes__c refCode = new Account_Tracking_Reference_Codes__c();
        refCode.Account__c = account.id;
        refCode.Contract__c = contractOld.id;
        refCode.ARC_Name__c = 'REF CODE1';
        refCode.ARC__c = '123 TEST 1';
        insert refCode;
        
        contractOld.Status = 'Activated';
        update contractOld;
        
        //child
        Contract contractNew = new Contract();
        contractNew.Status = 'Draft';
        contractNew.AccountId = account.id;
        contractNew.ContractTerm = 12;
        contractNew.StartDate = Date.newInstance(2011,01,01);
        contractNew.Contract_Version__c = 2;
        contractNew.Parent_Contract__c = contractOld.id;
        contractNew.Contract_Title__c = 'TEST';
        contractNew.Contract_Reference__c = 'TEST';

        
        insert contractNew;
  
        Test.startTest();
  
        contractNew.Status = 'Activated';
        update contractNew;
        
        List<Contract> parentContract = [Select c.Status, c.Parent_Contract__c, c.Id From Contract c 
                                        where c.id = :contractOld.id ];
       // System.assertEquals(1, parentContract.size());
      //  System.assertEquals('Deactivated', parentContract.get(0).status);
        
        
        List<Account_Tracking_Reference_Codes__c> trackingCodesParent = [Select contract__c, account__c from Account_Tracking_Reference_Codes__c
                                                                        where contract__c = :contractOld.id];
      //  System.assertEquals(0, trackingCodesParent.size());
 
        List<Account_Tracking_Reference_Codes__c> trackingCodesNewContract = [Select contract__c, account__c, ARC_Name__c, ARC__c from Account_Tracking_Reference_Codes__c
                                                                        where contract__c = :contractNew.id];
      //  System.assertEquals(1, trackingCodesNewContract.size());
      //  System.assertEquals('REF CODE1', trackingCodesNewContract.get(0).ARC_Name__c);
       // System.assertEquals('123 TEST 1', trackingCodesNewContract.get(0).ARC__c);
            
        Test.stopTest();
        
    }

}