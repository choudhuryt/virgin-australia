@isTest
private class TestCaseMasterTrigger {
    @IsTest
    static void TestAutoCloseSpam() {
        Case c = new Case(RecordtypeId = CaseTriggerHandler.RT_ACCELERATE,
                          Contact_email__c = 'dev_test@test.com',
                          Subject = 'TEST - Case',
                          Contact_First_Name__c = 'Deven',
                          Contact_Last_name__c = 'P',
                          ACC_Primary_Category__c = 'Smartfly');
        insert c;
        
        Test.startTest();
        c.ACC_Primary_Category__c = 'SPAM';
        update c;
        Test.stopTest();
        System.assert([SELECT Id, IsClosed FROM CASE WHERE Id = :c.Id].IsClosed, true);
    }
    
    @isTest static void ReopenCaseCapture() {
        
        Test.StartTest();
        
        List<RecordType> recordTypeGRCase = [
            Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName,
            r.Description, r.BusinessProcessId
            From RecordType r
            where SobjectType = 'Case'
            and IsActive = true
            and DeveloperName = 'VAAgentHelpdesk'
            LIMIT 1
        ];
        
        Case c = new Case(RecordtypeId = recordTypeGRCase[0].id,
                          Contact_email__c = 'jdoe_test_test@doe.com',
                          Velocity_Member__c = false,
                          Subject = 'Feedback - Something',
                          Contact_First_Name__c = 'Tom',
                          Contact_Last_name__c = 'Johns',
                          Additional_Details__c = 'I am a travel Agent');
        insert c;
        
        c.Status = 'Closed';
        update c ;
        
        c.Status = 'Pending VA response' ;
        update c ;
    }
    
    
    @isTest static void CaseAssignmentCapture() {
        
        Test.StartTest();
        
        List<RecordType> recordTypeGRCase = [
            Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName,
            r.Description, r.BusinessProcessId
            From RecordType r
            where SobjectType = 'Case'
            and IsActive = true
            and DeveloperName = 'VAAgentHelpdesk'
            LIMIT 1
        ];
        
        Case c = new Case(RecordtypeId = recordTypeGRCase[0].id,
                          Contact_email__c = 'jdoe_test_test@doe.com',
                          Velocity_Member__c = false,
                          Subject = 'Feedback - Something',
                          Contact_First_Name__c = 'Tom',
                          Contact_Last_name__c = 'Johns',
                          Additional_Details__c = 'I am a travel Agent',
                          OwnerId = '00G6F000003aHH6');
        insert c;
        
        
        c.ownerid = '00590000000K0juAAC' ;
        update c ;
    }
    /* @IsTest
static void TestUpdateFlightNumber() {
Case c = new Case(RecordtypeId = CaseTriggerHandler.RT_GCCGRCase,
Subject = 'TEST - Flight number',
Flight_Number__c = 'vaTest',
Description = 'TEST - Flight number');
Test.startTest();
insert c;
c.Flight_Number__c = 'Test';
update c;
Test.stopTest();
Case cs = [SELECT Id,Flight_Number__c FROM Case];
System.assertEquals(cs.Flight_Number__c,'VATEST');

List<Case> casesBulk = new list<Case>();
for (Integer i = 1; i <= 10; i++){
Case cas = new Case(RecordtypeId = CaseTriggerHandler.RT_GCCGRCase,
Subject = 'TEST - Flight number'+i,
Flight_Number__c = 'vaTest',
Description = 'TEST - Flight number'+i);
casesBulk.add(cas);
}
insert casesBulk;
}*/
    @IsTest static void updateRecordType() {
        Case c = new Case(RecordtypeId = CaseTriggerHandler.RT_GCCEconomyX,
                          Contact_email__c = 'neha_test@test.com',
                          Subject = 'TEST - Case Recordtype',
                          Contact_First_Name__c = 'Neha',
                          Contact_Last_name__c = 'A',
                          Description = 'TEST - Case RecordType');
        
        insert c; 
        
        Case cs = [SELECT Id,RecordtypeId FROM Case];
        System.assertEquals(cs.RecordtypeId,CaseTriggerHandler.RT_GCCGeneralEnquiries);
        
    }
    @IsTest static void attachEmailGCCSSRCase() {
        Case cs = new Case(RecordtypeId = CaseTriggerHandler.RT_GCCSSR,
                           Contact_email__c = 'nabz@xxx.com',
                           Subject = 'TEST',
                           Contact_First_Name__c = 'Nabz',
                           Contact_Last_name__c = 'A',
                           Description = 'TEST',
                           Origin = 'Web',
                           Hearing_Impaired_No_Assistance__c = true,
                           Can_Use_Stairs_and_Walk_to_Seat__c = true,
                           Immobile_and_Travelling_with_Carer__c = true,
                           Immobile_and_Providing_Assistance_Person__c = true,
                           Medical_Condition_May_require_Clearance__c = true,
                           Other_Powered_Medical_Equipment__c = true,
                           Hidden_Disability_Details__c ='Test',
                           Other_Assistance_Additional_Information__c ='Test',
                           Vision_Impaired_No_Assistance__c = true);
        insert cs; 
        
    }
}