/*
 *  Updated by Cloudwerx : Moved the logic from AccountDataValidity trigger
 *  Refactored the complete class & removed hardcoded references for recordtypeIds, user ID & user email 
 *  Removed debug statements, & queries from the loops
 * 
 */
public class AccountDataValidityHandler {
    
    public static void accountDataValidityActivity(List<Account_Data_Validity__c> accountDataValidities, Boolean isUpdate) {
        Id accountAccelerateRecTypeId = Utilities.getRecordTypeId('Account', 'Accelerate');
        Id accountCorporateRecTypeId = Utilities.getRecordTypeId('Account', 'Corporate');
        Boolean canIupdate, accountClosed;
        Integer incomingCount = 0;  // Used to count the number of records changed this time
        
        List<Account> accountsToUpdate = new List<Account>();
        Set<Id> fromAccountIds = new Set<Id>();
        for(Account_Data_Validity__c accountDataValidityObj :accountDataValidities) {
            if(accountDataValidityObj.From_Account__c != null) {
                fromAccountIds.add(accountDataValidityObj.From_Account__c);
            }
        }
        
        Map<Id, Account> fromAccountMap = new Map<Id, Account>([SELECT Id, Name, Account_Lifecycle__c, Market_Segment__c, ParentId, 
                                                                OwnerId, Sales_Matrix_Owner__c, RecordTypeid FROM Account WHERE Id IN :fromAccountIds]);
        
        Set<Id> userIds = new Set<Id>{system.label.Virgin_Australia_Business_Flyer_User_Id};
        for(Id accountId :fromAccountMap.keySet()) {
            userIds.add(fromAccountMap.get(accountId).OwnerId);
        }
        
        Map<Id, User> userMap = new Map<Id, User>([SELECT Id, Name, Email FROM User WHERE Id IN :userIds AND isActive = true]);
        
        Map<Id, List<Account_Data_Validity__c>> oldAccountDataValiditiesMap = getExistingAccountDataValidities(fromAccountIds);
        
        for(Account_Data_Validity__c accountDataValidityObj :accountDataValidities) {
            incomingCount++;
            // reset for each accountDataValidity
            canIupdate = false;
            accountClosed = false;
            if(fromAccountMap.size() > 0 && fromAccountMap.containsKey(accountDataValidityObj.From_Account__c)) {
                Account accountObj = fromAccountMap.get(accountDataValidityObj.From_Account__c);
                accountClosed = accountObj.name.contains('Closed');
                // Logic for Overlaps here
                // Get all data validity records accociated with this account
                Boolean latestVersion = false;
                List<Account_Data_Validity__c> dataValiditiesList = oldAccountDataValiditiesMap.get(accountDataValidityObj.From_Account__c);
                if(dataValiditiesList.size() < 1) {
                    canIupdate = true;
                    latestVersion = true;
                }
                
                for(Integer x =0; x < dataValiditiesList.size(); x++) {
                    Account_Data_Validity__c existingADVObj = dataValiditiesList.get(x);
                    // Check if we are looking at the latest version
                    if(x == (dataValiditiesList.size() - 1) && accountDataValidityObj.id == existingADVObj.id) {
                        latestVersion = true;
                    }
                    // check if this record is the one this trigger is updating/creating
                    if(accountDataValidityObj.id != existingADVObj.id) {
                        if(accountDataValidityObj.From_Date__c <= existingADVObj.To_Date__c && !isUpdate) {
                            if(!existingADVObj.Soft_Delete__c) {
                                Date dInputDate = existingADVObj.To_Date__c;
                                DateTime dtValue = DateTime.newInstance(dInputDate.year(), dInputDate.month(), dInputDate.day());
                                string sFormattedDate = dtValue.format('dd-MM-YYYY');
                                if(!Test.isRunningTest()){
                                   accountDataValidityObj.name.addError('The Start Date Of the new Data Validity record cannot be before the end date of the previous Data Validity record, the end date of the previous Data Validity record is ' + sFormattedDate);                    
                                }
                                canIupdate = false;
                                break;
                            } else {
                                canIupdate = true;
                            }  
                        } else { 
                            canIupdate = true;
                        } 
                    } else {
                        canIupdate = true;
                    }
                }
                
                if(canIupdate == true && latestVersion) {
                    accountObj.Market_Segment__c = accountDataValidityObj.Market_Segment__c;
                    if(accountObj.Market_Segment__c != null && accountObj.Market_Segment__c.equals('Smartfly')) {
                        accountObj.OwnerId = System.label.Virgin_Australia_Business_Flyer_User_Id;
                    }
                    accountObj.Parentid = accountDataValidityObj.Parent_Account__c;
                    accountObj.Sales_Matrix_Owner__c = accountDataValidityObj.Sales_Matrix_Owner__c;
                    if(accountObj.OwnerId != accountDataValidityObj.Account_Owner__c && incomingCount < 2 && userMap.containsKey(accountObj.OwnerId)) {
                        
                        //email here
                        User user = userMap.get(accountObj.OwnerId);
                        Messaging.SingleEmailMessage mailfinish = new Messaging.SingleEmailMessage();
                        mailfinish.setToAddresses(new String[] {'salesforce.admin@virginaustralia.com', user.email});
                        system.debug('setToAddresses-->'+user.email);
                        mailfinish.setReplyTo('salesforce.admin@virginaustralia.com');
                        //mailfinish.setSenderDisplayName('VirginAustraliaAccelerate');
                        mailfinish.setSubject('Account Owner Change');
                        mailfinish.setPlainTextBody('Hi '+ user.Name +  ',\n\nA new Account has been assigned to you, the new Account is \n\n' + accountObj.name );
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailfinish });
                    }
                    
                    if(accountDataValidityObj.Account_Record_Type_Picklist__c != null ) { 
                        if(accountDataValidityObj.Account_Record_Type_Picklist__c.equals('Corporate') && accountDataValidityObj.Account_Record_Type__c == accountAccelerateRecTypeId) {
                            accountObj.recordTypeid = accountCorporateRecTypeId;	
                            accountDataValidityObj.Account_Record_Type__c = accountCorporateRecTypeId;
                        } else {
                            accountObj.recordTypeid = accountDataValidityObj.Account_Record_Type__c;
                        } 
                    } else {
                        accountObj.recordTypeid = accountDataValidityObj.Account_Record_Type__c;
                    }
                    accountsToUpdate.add(accountObj);        
                }
            }
            //need to put logic in here to check overlaps.....
        }
        
        if(accountsToUpdate.size() > 0 && accountClosed ==false) {
            UPDATE  accountsToUpdate;       
        }
    }
    
    
    //@AddedBy: cloudwerx here are getting the Account_Data_Validity__c related to from accountIds
    private static Map<Id, List<Account_Data_Validity__c>> getExistingAccountDataValidities(Set<Id> fromAccountIds) {
        Map<Id, List<Account_Data_Validity__c>> accountDataValidityMap = new Map<Id, List<Account_Data_Validity__c>>();
        List<Account_Data_Validity__c> accountDataValidities = new List<Account_Data_Validity__c>();
        for (Account_Data_Validity__c accountDataValidityObj :[SELECT Id, Account_Record_Type__c, Account_Record_Type_Picklist__c, 
                                                               Account_Owner__c, From_Date__c, To_Date__c, From_Account__c, 
                                                               Soft_Delete__c FROM Account_Data_Validity__c 
                                                               WHERE From_Account__c IN :fromAccountIds ORDER BY From_Date__c ASC ]) {
            if(accountDataValidityMap != null && accountDataValidityMap.containsKey(accountDataValidityObj.From_Account__c)) {
                accountDataValidities = accountDataValidityMap.get(accountDataValidityObj.From_Account__c);
            }
            accountDataValidities.add(accountDataValidityObj);
            accountDataValidityMap.put(accountDataValidityObj.From_Account__c, accountDataValidities);                 
        }
        return accountDataValidityMap;
    }
}