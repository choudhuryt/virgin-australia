/** * File Name      : TestDomesticDiscountController
* Description        : This Apex Test Class is the Test Class for DomesticDiscountController  
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Ed 'the man' Stachyra
* * Date             : Updated 28 February 2013
* * Technical Task ID: 
* * Notes            :  The test class for this file is:  TestDomesticDiscountController
* Modification Log =============================================================== 
Ver Date Author Modification --- ---- ------ -------------
* */
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestDomesticDiscountController {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        
        //setup account
        
        Domestic_Discounts__c domdiscounts = new Domestic_Discounts__c();
        domdiscounts.Domestic_off_Published_Fare_2__c =1;
        domdiscounts.Domestic_off_Published_Fare_3__c =3;
        
        domdiscounts.Is_Template__c = true;
        domdiscounts.tier__c ='1';
        domdiscounts.Cos_Version__c ='11.2';
        insert domdiscounts;
        
        Intra_WA_Discounts__c intraDiscounts = new Intra_WA_Discounts__c();
        intraDiscounts.Intra_WA_Discount_off_Published_Fare2__c =5;
        intraDiscounts.Intra_WA_Discount_off_Published_Fare5__c=10;
        intraDiscounts.Is_Template__c = true;
        intraDiscounts.tier__c ='1';
        intraDiscounts.cos_version__C = '11.2';
        insert intraDiscounts;
        
       
        
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
  		account.BillingCountry ='AU';
  			
 		insert account;
 
        Contract contractOld = new Contract();
        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.Cos_Version__c ='11.2';
        contractOld.Saver_Discounts__c ='No';
        contractOld.Domestic_Short_Haul_Tier__c = '1';
        contractOld.Red_Circle__c = true;
        insert contractOld;
        
       
        
        //instantiate the DomesticDiscountController 
        PageReference  ref1 = Page.DomesticDiscountPage;
        ref1.getParameters().put('id', contractOld.Id);
        Test.setCurrentPage(ref1);
        ApexPages.StandardController conL = new ApexPages.StandardController(contractOld);
        DomesticDiscountController lController = new DomesticDiscountController(conL);
        
        //instantiate the DomesticDiscountController 
        ApexPages.StandardController conX = new ApexPages.StandardController(contractOld);
        DomesticDiscountController xController = new DomesticDiscountController(conX);
        
        Test.startTest();
        //PageReference  ref1 = Page.DomesticDiscountPage;
        ref1 = lController.initDisc();
        
       
        ref1 = lController.newDom();
        ref1 = lController.newIntra();
       
        ref1 = lController.save();
        ref1 = lController.initDisc();
        ref1 = lController.qsave();
        
    //    ref1 = lController.newDom();
    //    ref1 = lController.newIntra();
        ref1 = lController.initDisc();
        ref1 = lController.remDom();
       
        ref1 = lController.remIntra();
       
       contractOld.Cos_Version__c ='14.0';
       contractOld.Domestic_Short_Haul_Tier__c = '';
       contractOld.Red_Circle__c = false;
       contractOld.Saver_Discounts__c ='No';
       update contractOld;
       
        ref1 = lController.initDisc();
        ref1 = lController.newDom();
        ref1 = lController.newIntra();
       
       ref1 = lController.cancel();
       
       contractOld.Cos_Version__c ='12.0';
       contractOld.Domestic_Short_Haul_Tier__c = '1';
       contractOld.Red_Circle__c = false;
       contractOld.Saver_Discounts__c ='Yes';
       update contractOld;
       
       
        ref1 = lController.initDisc();
        ref1 = lController.newDom();
        ref1 = lController.newIntra();
       
        Test.stopTest();
        
    }
    
    
    
    
    
}