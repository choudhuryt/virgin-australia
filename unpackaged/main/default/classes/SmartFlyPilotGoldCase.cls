public class SmartFlyPilotGoldCase {
    
@InvocableMethod
public static void SmartflyPilotGoldLink( List<Id> CaseIds )
  {
      string BusinessNumber ; 
      List<Case> caseUpdate = new List<Case>(); 
      List<Account> accList = new List<Account>();  
      List<Case> caselist = new List<Case>();   
      caselist =  [select id ,Subject,accountid,ACC_Primary_Category__c, ACC_Secondary_Category__c,ACC_Tertiary_Category__c from case where id=:caseIds and subject like 'Pilot Gold Request | ABN: %'  ];
      
     if (caselist.size() > 0)
     {
       BusinessNumber =    caselist[0].Subject.substringAfter('ABN:').substring(1);  
         
       if(BusinessNumber.length() == 11 ||  BusinessNumber.length() == 9 )  
       {    
         accList  =  [select id , Business_Number__c from Account where Business_Number__c =:BusinessNumber and recordtypeid = '012900000009HrP'  limit 1];    
       
       }
    
      for(Case c: caselist)
       { 
       if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
       }
         c.ACC_Primary_Category__c = 'Client' ;
         c.ACC_Secondary_Category__c=  'Pilot Gold' ;
         c.ACC_Tertiary_Category__c = 'Action Sales Support';
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     }
  }
}