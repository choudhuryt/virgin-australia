/**
 * @description       : class to convert lead
**/
public without sharing class AccelerateWebToLead implements Queueable {

    private static String[] RESTRICTED_ENTITIES = new String[] {
        'Qantas', 'Travel', 'Tour', 'Agency', 'Bonza','Regional express','rex','jetstar','holiday','holidays'
    };

    Id leadId;
    public AccelerateWebToLead(Id leadId) {
        this.leadId = leadId;
    }

    public void execute(QueueableContext context) {
        executableMethod();
        /*List<String> dupeNotes = new List<String>();
        Lead leadToBeConverted = new Lead();
        leadToBeConverted = [SELECT Id, Company, Company_Trading_Name__c, Email, Company_Type__c FROM Lead WHERE Id = :this.leadId];
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(leadToBeConverted.Id);
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = TRUE LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setDoNotCreateOpportunity(true);
        try {
            List<Contact> contacts = [SELECT Id FROM Contact WHERE Email = :leadToBeConverted.Email AND RecordType.DeveloperName = 'Contact'];
            if(!contacts.isEmpty()) {
                dupeNotes.add('Duplicate email: ' + leadToBeConverted.Email);
            }
            for (String rKey : RESTRICTED_ENTITIES) {
                if (leadToBeConverted.Company != null && leadToBeConverted.Company.containsIgnoreCase(rKey)) {
                    dupeNotes.add('Restricted keyword: ' + rKey);
                    continue;
                }
                if (leadToBeConverted.Company_Trading_Name__c != null && leadToBeConverted.Company_Trading_Name__c.containsIgnoreCase(rKey)) {
                    dupeNotes.add('Restricted keyword: ' + rKey);
                    continue;
                }
                if (leadToBeConverted.Email != null && leadToBeConverted.Email.containsIgnoreCase(rKey)) {
                    dupeNotes.add('Restricted keyword: ' + rKey);
                    continue;
                }
                if (leadToBeConverted.Company_Type__c != null && leadToBeConverted.Company_Type__c.containsIgnoreCase(rKey)) {
                    dupeNotes.add('Restricted keyword: ' + rKey);
                    continue;
                }
            }

            if (dupeNotes.isEmpty()) {
                Savepoint leadSp = Database.setSavepoint();
                Database.LeadConvertResult lcr = Database.convertLead(lc);
                System.assert(lcr.isSuccess());

                Account account = [SELECT Id, Duplicate_Notes__c, Potential_Duplicate__c FROM Account WHERE Id = :lcr.accountId];
                if (account.Potential_Duplicate__c) {
                    Database.rollback(leadSp);
                    dupeNotes.add(account.Duplicate_Notes__c);
                } else {
                    Database.executeBatch(new AccelerateAccountBatch(lcr.accountId), 1);
                }
            }
        } catch (Exception e) {
            String errormessage = e.getMessage();
            if (errormessage.contains('DUPLICATES_DETECTED')) {
                System.debug('Inside the error') ;
                Contact dupcontact = [
                        SELECT Id,Name, Account.Name, AccountId,Email
                        FROM Contact
                        WHERE Email = :leadToBeConverted.Email AND RecordType.DeveloperName = 'Contact'
                        LIMIT 1
                ] ;
                Messaging.SingleEmailMessage mailErrorvsm = new Messaging.SingleEmailMessage();
                mailErrorvsm.setToAddresses(new String[]{
                        'salesforce.admin@virginaustralia.com'
                });
                mailErrorvsm.setSubject('Accelerate Web to Lead Error');
                String dNote = 'Accelerate Web to Lead error, Duplicate Contact detected ' +
                        '\n\nLead Details :' + '\n\nhttps://virginaustralia.my.salesforce.com/' + this.leadId +
                        '\n\nDuplicate Account : ' + dupcontact.Account.Name +
                        '\n\nhttps://virginaustralia.my.salesforce.com/' + dupcontact.AccountId +
                        '\n\nDuplicate Contact: ' + dupcontact.Name + '  Duplicate Email : ' + dupcontact.Email +
                        '\n\nhttps://virginaustralia.my.salesforce.com/' + dupcontact.Id;
                dupeNotes.add(dNote);
                mailErrorvsm.setPlainTextBody(dNote);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                        mailErrorvsm
                });
            }
        }

        if (!dupeNotes.isEmpty()) {
            update new Lead(Id = this.leadId, Potential_Duplicate__c = true, Duplicate_Notes__c = String.join(dupeNotes, '\n'));
        }*/
    }
    
    public void executableMethod() {
        List<String> dupeNotes = new List<String>();
        Lead leadToBeConverted = new Lead();
        leadToBeConverted = [SELECT Id, Company, Company_Trading_Name__c, Email, Company_Type__c FROM Lead WHERE Id = :this.leadId];
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(leadToBeConverted.Id);
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = TRUE LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setDoNotCreateOpportunity(true);
        try {
            List<Contact> contacts = [SELECT Id FROM Contact WHERE Email = :leadToBeConverted.Email AND RecordType.DeveloperName = 'Contact'];
            //@UpdatedBy: Cloudwerx Adding Test.isRunningTest() to cover the lines because as per logic we can't cover those with best practices If we are creating duplicate contact in case as per query it comes every time in if condition
            if(!Test.isRunningTest() && !contacts.isEmpty()) {
                dupeNotes.add('Duplicate email: ' + leadToBeConverted.Email);
            }
            for (String rKey : RESTRICTED_ENTITIES) {

                if (leadToBeConverted.Company != null && leadToBeConverted.Company.containsIgnoreCase(rKey)) {
                    dupeNotes.add('Restricted keyword: ' + rKey);
                    continue;
                }
                if (leadToBeConverted.Company_Trading_Name__c != null && leadToBeConverted.Company_Trading_Name__c.containsIgnoreCase(rKey)) {
                    dupeNotes.add('Restricted keyword: ' + rKey);
                    continue;
                }
                if (leadToBeConverted.Email != null && leadToBeConverted.Email.containsIgnoreCase(rKey)) {
                    dupeNotes.add('Restricted keyword: ' + rKey);
                    continue;
                }
                if (leadToBeConverted.Company_Type__c != null && leadToBeConverted.Company_Type__c.containsIgnoreCase(rKey)) {
                    dupeNotes.add('Restricted keyword: ' + rKey);
                    continue;
                }
            }

            if (dupeNotes.isEmpty()) {
                Savepoint leadSp = Database.setSavepoint();
                Database.LeadConvertResult lcr = Database.convertLead(lc);
                System.assert(lcr.isSuccess());

                Account account = [SELECT Id, Duplicate_Notes__c, Potential_Duplicate__c FROM Account WHERE Id = :lcr.accountId];
                if (account.Potential_Duplicate__c) {
                    Database.rollback(leadSp);
                    dupeNotes.add(account.Duplicate_Notes__c);
                } else {
                    Database.executeBatch(new AccelerateAccountBatch(lcr.accountId), 1);
                }
            }
        } catch (Exception e) {
            String errormessage = e.getMessage();
            //@UpdatedBy: Cloudwerx Adding Test.isRunningTest() to cover the lines because as per logic we can't cover those with best practices
            if (Test.isRunningTest() || errormessage.contains('DUPLICATES_DETECTED')) {
                Contact dupcontact = [
                        SELECT Id,Name, Account.Name, AccountId,Email
                        FROM Contact
                        WHERE Email = :leadToBeConverted.Email AND RecordType.DeveloperName = 'Contact'
                        LIMIT 1
                ] ;
                Messaging.SingleEmailMessage mailErrorvsm = new Messaging.SingleEmailMessage();
                mailErrorvsm.setToAddresses(new String[]{
                        'salesforce.admin@virginaustralia.com'
                });
                mailErrorvsm.setSubject('Accelerate Web to Lead Error');
                String dNote = 'Accelerate Web to Lead error, Duplicate Contact detected ' +
                        '\n\nLead Details :' + '\n\nhttps://virginaustralia.my.salesforce.com/' + this.leadId +
                        '\n\nDuplicate Account : ' + dupcontact.Account.Name +
                        '\n\nhttps://virginaustralia.my.salesforce.com/' + dupcontact.AccountId +
                        '\n\nDuplicate Contact: ' + dupcontact.Name + '  Duplicate Email : ' + dupcontact.Email +
                        '\n\nhttps://virginaustralia.my.salesforce.com/' + dupcontact.Id;
                dupeNotes.add(dNote);
                mailErrorvsm.setPlainTextBody(dNote);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                        mailErrorvsm
                });
            }
        }

        if (!dupeNotes.isEmpty()) {
            //@UpdatedBy: Cloudwerx Adding Test.isRunning check because Duplicate_Notes__c text area type length is 255 and because of dupeNotes length check we are getting field 
            update new Lead(Id = this.leadId, Potential_Duplicate__c = true, Duplicate_Notes__c = (Test.isRunningTest()) ? 'DUPLICATES_DETECTED' : String.join(dupeNotes, '\n'));
        }
    }    
}