/**
* @description       : Test class for UpdateMasterCase
* @Updated By        : Cloudwerx
**/
@isTest
private class TestUpdateMasterCase {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Entitlement testEntitlementObj = TestDataFactory.createTestEntitlement(testAccountObj.Id, 'Entitlement', 'Phone Support');
        INSERT testEntitlementObj;
        
        GR8_Case_Categories__c testGR8CaseCategoryObj = TestDataFactory.createTestGRCaseCategories(true, true, 'Test', true, Date.today(), false, 'Boarding');
        testGR8CaseCategoryObj.RecordTypeId = Utilities.getRecordTypeId('GR8_Case_Categories__c', 'GR_Live');
        INSERT testGR8CaseCategoryObj;
        
        Id caseRecordTypeId = Utilities.getRecordTypeId('Case', 'GR_Case');
        Case testParentCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        testParentCaseObj.Category_Number__c = testGR8CaseCategoryObj.Id;
        testParentCaseObj.Airline_Organisation_Short__c = 'VAA';
        testParentCaseObj.Type = 'Complaint';
        testParentCaseObj.Pre_Filter_Primary_Category__c = 'Boarding';
        testParentCaseObj.RecordTypeId = caseRecordTypeId;
        testParentCaseObj.EntitlementId = testEntitlementObj.Id;
        INSERT testParentCaseObj;
        
        Case testChildCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Child Case', '210086333', 'movementType', UserInfo.getUserId(), 'childCaseSegment', 'ACT', Date.today(), 'Both');
        testChildCaseObj.parentId = testParentCaseObj.Id;
        testChildCaseObj.RecordTypeId = caseRecordTypeId;
        testChildCaseObj.EntitlementId = testEntitlementObj.Id;
        testChildCaseObj.Child_Case_Reason__c = 'Testing Reason';
        testChildCaseObj.Category_Number__c = testGR8CaseCategoryObj.Id;
        testChildCaseObj.Airline_Organisation_Short__c = 'VAA';
        testChildCaseObj.Type = 'Complaint';
        testChildCaseObj.Pre_Filter_Primary_Category__c = 'Boarding';
        INSERT testChildCaseObj;
        Database.merge(testParentCaseObj, testChildCaseObj.Id);
    }
    
    @isTest
    private static void testUpdateMasterCase() {
        List<Case> testCases = [SELECT Id, CaseNumber FROM Case];
        Test.startTest();
        UpdateMasterCase.GRCaseStatusUpdate(new List<Id>{testCases[1].Id});
        Test.stopTest();
        Case updatedCaseObj = [SELECT CaseNumber, Child_Case_Comments__c, Child_Case_Reason__c, Compliance_Status__c,
                               Pre_Filter_Primary_Category__c, Status FROM Case WHERE ParentId != null LIMIT 1];
        System.assertEquals('Duplicate', updatedCaseObj.Child_Case_Comments__c);
        System.assertEquals('Duplicate', updatedCaseObj.Child_Case_Reason__c);
        System.assertEquals('Compliance form NOT required', updatedCaseObj.Compliance_Status__c);
        System.assertEquals('Service Recovery', updatedCaseObj.Pre_Filter_Primary_Category__c);
        System.assertEquals('Closed', updatedCaseObj.Status);
        
        CaseComment caseCommentObj = [SELECT ParentId, CommentBody, IsPublished 
                                      FROM CaseComment WHERE ParentId =:testCases[1].Id LIMIT 1];
        System.assertEquals(testCases[1].Id, caseCommentObj.ParentId);
        System.assertEquals('Case '+ testCases[1].CaseNumber + ' has been merged with Case ' + testCases[0].CaseNumber + '.', caseCommentObj.CommentBody);
        System.assertEquals(true, caseCommentObj.IsPublished);
    }
    
    @isTest
    private static void testUpdateMasterCaseDeleteCase() {
        List<Case> testCases = [SELECT Id, Origin FROM Case];
        testCases[0].Origin = 'Email';
        testCases[1].Origin = 'E2C';
        UPDATE testCases;
        Test.startTest();
        UpdateMasterCase.GRCaseStatusUpdate(new List<Id>{testCases[1].Id});
        Test.stopTest();
        List<Case> testCasesAfterDelete = [SELECT Id FROM Case];
        System.assertEquals(1, testCasesAfterDelete.size());
    }
}