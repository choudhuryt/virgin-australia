/**
 * @description       : Handler class for SendEmailtoAlmsTrigger
 * @CreatedBy         : CloudWerx
 * @UpdatedBy         : CloudWerx
**/
public class SendEmailtoAlmsTriggerHandler {
    public static void emailMessageActivities(List<EmailMessage> emailMessages, Boolean isInsert, Boolean isUpdate) {
        List<Id> emailIds = new List<Id>();
        //@updateBy : cloudwerx here added check to allow TestSendCasetoAlmsTrigger to execute insert and update conditions
        if(Test.isRunningTest() || System.Label.POST_ALMS_Integration == 'Active') {          
            for(EmailMessage emailMessageObj :emailMessages) {  
                String relatedIdObjName = (emailMessageObj.RelatedToId != NULL) ? emailMessageObj.RelatedToId.getSObjectType().getDescribe().getName() : '';
                if(isInsert && (emailMessageObj.Incoming == true || (emailMessageObj.Incoming == false && emailMessageObj.Status == '3')) && ((emailMessageObj.RelatedToId != null && (relatedIdObjName == 'Case'|| relatedIdObjName == 'Contact')) || (emailMessageObj.RelatedToId == null && emailMessageObj.ActivityId != null))) {
                    emailIds.add(emailMessageObj.Id);
                } else if(isUpdate && emailMessageObj.Incoming == false && emailMessageObj.Status == '3') {
                    emailIds.add(emailMessageObj.Id);
                }
            }
        }
        
        if(emailIds.size() > 0) {
            //SendToAlmsWS.sendEmail(emailIds);
            SendCommsToAlmsWS.sendtoAlms(emailIds, null);
        }
    }
}