/**
 * @description       : ContractSpendCalculation
**/
global class ContractSpendCalculation implements Database.Batchable<SObject> {
    
    global ContractSpendCalculation(){}
    /*
        @Updated By: Cloudwerx
        description : Here we added code to get user id and record type ids dynamically
    */
    global static Set<Id> contractRecordTypeIds = new Set<Id> {Utilities.getRecordTypeId('Contract', 'Accelerate_POS_Rebate')};
    global static Set<Id> accountRecordTypeIds = new Set<Id> {Utilities.getRecordTypeId('Account', 'SmartFly')};
    global String gstrQuery = 'SELECT id ,Total_Eligible_Spend_for_PG_Calc__c ,Total_Eligible_Spend_used_for_PG_Calc__c '+
        'FROM Contract WHERE  '+
        'recordtypeId IN :contractRecordTypeIds ' +
        'AND  Status = \'Activated\''  + 
        'AND  Account.RecordTypeID IN :accountRecordTypeIds '  ;
    
    //Loading and running the query string
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(gstrQuery);
    }   
    
    /** Running The execute Method **/
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        System.debug('** Starting Batch Process ** ');
        // Cast the incoming list of Objects into Account Contract Map.  
        //@updatedBy: cloudwerx to covert list of SObjects to Map<Id, Contract>
        Map<Id, Contract> contractsMap = new Map<Id, Contract>((List<Contract>) scope);
        Map<Id,List<Spend__c>> contractSpendsMap = getContractSpends(contractsMap.keySet());
        // Capture success for summary email.
        List <String> successContractNames = new list <String>();
        // Process each Account
        for(Id contractId :contractsMap.keySet()) {
            Contract contract = contractsMap.get(contractId);
            List<Spend__c> spendList = (contractSpendsMap != null && contractSpendsMap.containsKey(contract.Id)) ? contractSpendsMap.get(contract.Id) : new List<Spend__c>();
            if(spendList.size() > 1) {
                processSpendData(contract, successContractNames); 
                if(contract.Total_Eligible_Spend_for_PG_Calc__c >= 50000) {
                    processPGCalc(contract, successContractNames); 
                }    
            }
        }
    }  
    
    public void processSpendData(Contract contract, list <String> successContractNames) {
        
        List<aggregateResult> annresults =[SELECT SUM(Eligible_Spend__c) annespend , SUM(Spend__c) annspend , Contract__c 
                                           FROM Spend__c
                                           WHERE Contract__c = :contract.id  AND   Contract_Type__c= 'Accelerate POS/Rebate' 
                                           AND   Eligible_For_PG_Calc__c = true GROUP BY Contract__c];
        
        if (annresults.size() > 0) {    
            for(AggregateResult ar1 : annresults) {
                contract.Total_Eligible_Spend_for_PG_Calc__c =   (decimal)ar1.get('annespend')==null?0:(decimal)ar1.get('annespend');
            }
        }
        try{
            UPDATE contract ;
            successcontractNames.add(contract.ContractNumber);            
        } catch(Exception e){
            System.debug(Logginglevel.INFO,'** Failed ** ');
            //VirginsutilitiesClass.sendEmailError(e);
        }
    }
    
    
    public void processPGCalc(Contract contract, list <String> successContractNames) {
        List<Smartfly_Benefit__c> smfbenlist =  [SELECT Id, Total_Spend__c,Spend_Used_For_PG__c,Pilot_Gold_Added__c 
                                                 FROM Smartfly_Benefit__c  WHERE Contract__c =:contract.id];   
        
        if (smfbenlist.size() == 0 ) {
            Smartfly_Benefit__c smfben = new Smartfly_Benefit__c();
            smfben.Contract__c = contract.id ;
            smfben.Total_Spend__c = contract.Total_Eligible_Spend_for_PG_Calc__c ; 
            smfben.Pilot_Gold_Added__c = (contract.Total_Eligible_Spend_for_PG_Calc__c/50000).round(System.RoundingMode.DOWN) ; 
            smfben.Spend_Used_For_PG__c = smfben.Pilot_Gold_Added__c * 50000;
            INSERT smfben ;
        } else  if (smfbenlist.size() > 0 ) {
            List<Smartfly_Benefit__c> latestbenlist =  [SELECT Id, Total_Spend__c,Spend_Used_For_PG__c,Pilot_Gold_Added__c 
                                                        FROM Smartfly_Benefit__c  where Contract__c =:contract.id 
                                                        ORDER BY createddate  desc limit 1];
            system.debug('The size ' + contract.Total_Eligible_Spend_used_for_PG_Calc__c + contract.Total_Eligible_Spend_for_PG_Calc__c );    
            
            if(contract.Total_Eligible_Spend_for_PG_Calc__c - contract.Total_Eligible_Spend_used_for_PG_Calc__c >= 50000) {
                Smartfly_Benefit__c smfben = new Smartfly_Benefit__c();
                smfben.Contract__c = contract.id ;
                smfben.Total_Spend__c = contract.Total_Eligible_Spend_for_PG_Calc__c ; 
                smfben.Pilot_Gold_Added__c = ((contract.Total_Eligible_Spend_for_PG_Calc__c - contract.Total_Eligible_Spend_used_for_PG_Calc__c)/50000).round(System.RoundingMode.DOWN) ; 
                smfben.Spend_Used_For_PG__c = smfben.Pilot_Gold_Added__c * 50000;  
                insert smfben ; 
            }    
        }
    }
    
    //@AddedBy: cloudwerx Here we are getting Spends records related to contracts
    private static Map<Id, List<Spend__c>> getContractSpends(Set<Id> contractIds) {
        Map<Id, List<Spend__c>> spendsMap = new Map<Id, List<Spend__c>>();
        List<Spend__c> spends = new List<Spend__c>();
        for (Spend__c spendObj :[SELECT Id, Account__c, Contract__c FROM Spend__c WHERE Contract__c IN :contractIds]) {
            if(spendsMap != null && spendsMap.containsKey(spendObj.Contract__c)) {
                spends = spendsMap.get(spendObj.Contract__c);
            }
            spends.add(spendObj);
            spendsMap.put(spendObj.Contract__c,spends);                 
        }
        return spendsMap;
    }
    
    //Finishing the batch  (Or though this is not executiong code this methos needs to exsist for the batch class to run!!)  
    global void finish(Database.BatchableContext BC){
        
    }
}