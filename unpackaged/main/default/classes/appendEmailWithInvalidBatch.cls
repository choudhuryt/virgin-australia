public class appendEmailWithInvalidBatch implements Database.Batchable<sObject> {    
    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        String query= 'SELECT Id,Email FROM Contact WHERE (not( email like  \'%' + 'invalid' + '%\' )) AND email !=null';
        return Database.getQueryLocator(query); 
    }
    
    public void execute(Database.BatchableContext bc, List<Contact> scope)
    {
        system.debug('scope++'+scope);
        List<Contact> contactsToUpdate = new List<Contact>();
        for(Contact con: scope)
        {
            if(!con.Email.Contains('.invalid'))
            con.Email = con.Email+ '.invalid';
            contactsToUpdate.add(con);
        }
        Database.update (contactsToUpdate,false);
    }
    
    public void finish(Database.BatchableContext bc)
    {
        
    }
}