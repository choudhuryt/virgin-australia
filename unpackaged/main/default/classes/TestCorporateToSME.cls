/**
 * @description       : Test class for CorporateToSME 
 * @updatedBy         : Cloudwerx
**/
@isTest
private class TestCorporateToSME {

    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('Account Closed', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        testAccountObj.RecordTypeId = Utilities.getRecordTypeId('Account', 'Government');
        INSERT testAccountObj;
        
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Corporate Case', '2100865670', 'Corporate-Accelerate/Smartfly', UserInfo.getUserId(), 'Accelerate', 'ACT', date.newinstance(Date.today().year(), Date.today().month(), 20), 'Both');
        INSERT testCaseObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('Test Contract', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj.RecordTypeId = CorporateToSME.contractAcceleratePOSRebateRecordTypeId;
        INSERT testContractObj;
        testContractObj.Status = 'Activated';
        UPDATE testContractObj;
    }
    
    @isTest
    private static void testCorporateToSMEAccelerate() {
        Test.startTest();
        Case testCaseObj = [SELECT Id FROM Case LIMIT 1];
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1];
        
        User u = TestUtilityClass.createTestUser();
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Integration Profile' LIMIT 1].Id;
        INSERT u;
        
        
        system.runAs(u){
            Account testAccountObj1 = TestDataFactory.createTestAccount('testAccountDataValidity Closed', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
            testAccountObj1.AccountSource = 'Accelerate';
            testAccountObj1.RecordTypeId = Utilities.getRecordTypeId('Account', 'Accelerate');
            testAccountObj1.Business_Number__c = 'ABC';
            testAccountObj1.Account_Lifecycle__c = 'Active';
            INSERT testAccountObj1;
            
            Account_Data_Validity__c testAccountDataValidityObj = TestUtilityClass.createAccountDataValidity(testAccountObj.Id, CorporateToSME.vaUserId, testAccountObj1.Id);
            testAccountDataValidityObj.To_Date__c = null;
            testAccountDataValidityObj.Account_Record_Type__c = Utilities.getRecordTypeId('Account', 'Corporate');
            INSERT testAccountDataValidityObj;
            
            CorporateToSME.MovetoSME(new List<Id>{testCaseObj.Id});
        }
        Test.stopTest();
        //Asserts
        List<Contract> contracts = [SELECT Id, AccountId FROM Contract WHERE AccountId =:testAccountObj.Id];
        List<SIP_Header__c> sipHeaders = [SELECT Id, Description__c FROM SIP_Header__c];
        System.assertEquals(true, sipHeaders.size() > 0);
        System.assertEquals('Accelerate - All Revenue', sipHeaders[0].Description__c);
        List<SIP_Rebate__c> sipRebates = [SELECT Id, SIP_Header__c, Lower_Percent__c, Upper_Percent__c, SIP_Percent__c FROM SIP_Rebate__c];
        System.assertEquals(6, sipRebates.size());
        System.assertEquals(sipHeaders[0].Id, sipRebates[0].SIP_Header__c);
        System.assertEquals(2, sipRebates[0].Lower_Percent__c);
        System.assertEquals(2, sipRebates[0].Upper_Percent__c);
        System.assertEquals(2, sipRebates[0].SIP_Percent__c);
    }
    
    @isTest
    private static void testCorporateToSMEAccelerateDays() {
        Test.startTest();
        Case testCaseObj = [SELECT Id ,Change_From_Date1__c FROM Case LIMIT 1];
        testCaseObj.Change_From_Date1__c = date.newinstance(Date.today().year(), Date.today().month(), 2);
        UPDATE testCaseObj;
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1];
        User u = TestUtilityClass.createTestUser();
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Integration Profile' LIMIT 1].Id;
        INSERT u;
        
        system.runAs(u){
            Account testAccountObj1 = TestDataFactory.createTestAccount('testAccountDataValidity Closed', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
            testAccountObj1.AccountSource = 'Accelerate';
            testAccountObj1.RecordTypeId = Utilities.getRecordTypeId('Account', 'Accelerate');
            testAccountObj1.Business_Number__c = 'ABC';
            testAccountObj1.Account_Lifecycle__c = 'Active';
            INSERT testAccountObj1;
            
            Account_Data_Validity__c testAccountDataValidityObj = TestUtilityClass.createAccountDataValidity(testAccountObj.Id, CorporateToSME.vaUserId, testAccountObj1.Id);
            testAccountDataValidityObj.To_Date__c = null;
            testAccountDataValidityObj.Account_Record_Type__c = Utilities.getRecordTypeId('Account', 'Corporate');
            INSERT testAccountDataValidityObj;
            
            CorporateToSME.MovetoSME(new List<Id>{testCaseObj.Id});
        }
        Test.stopTest();
        //Asserts
        List<Contract> contracts = [SELECT Id, AccountId FROM Contract WHERE AccountId =:testAccountObj.Id];
        List<SIP_Header__c> sipHeaders = [SELECT Id, Description__c FROM SIP_Header__c];
        System.assertEquals(true, sipHeaders.size() > 0);
        System.assertEquals('Accelerate - All Revenue', sipHeaders[0].Description__c);
        List<SIP_Rebate__c> sipRebates = [SELECT Id, SIP_Header__c, Lower_Percent__c, Upper_Percent__c, SIP_Percent__c FROM SIP_Rebate__c];
        System.assertEquals(6, sipRebates.size());
        System.assertEquals(sipHeaders[0].Id, sipRebates[0].SIP_Header__c);
        System.assertEquals(2, sipRebates[0].Lower_Percent__c);
        System.assertEquals(2, sipRebates[0].Upper_Percent__c);
        System.assertEquals(2, sipRebates[0].SIP_Percent__c);
    }
    
    @isTest
    private static void testCorporateToSMEFlyPlus() {
        Test.startTest();
        Case testCaseObj = [SELECT Id, NEW_Market_Segment1__c FROM Case LIMIT 1];
        testCaseObj.NEW_Market_Segment1__c = 'FlyPlus';
        UPDATE testCaseObj;
        
        CorporateToSME.MovetoSME(new List<Id>{testCaseObj.Id});
        Test.stopTest();
        //Asserts
        Account testAccountObj = [SELECT Id, Move_to_FlyPlus__c, Booking_Type__c FROM Account LIMIT 1];
        System.assertEquals(true, testAccountObj != null);
        System.assertEquals(true, testAccountObj.Move_to_FlyPlus__c);
        System.assertEquals(true, String.isBlank(testAccountObj.Booking_Type__c));
    }
    
    @isTest
    private static void testCorporateToSMESmartFly() {
        Test.startTest();
        Case testCaseObj = [SELECT Id, NEW_Market_Segment1__c FROM Case LIMIT 1];
        testCaseObj.NEW_Market_Segment1__c = 'SmartFly';
        UPDATE testCaseObj;
        
        CorporateToSME.MovetoSME(new List<Id>{testCaseObj.Id});
        Test.stopTest();
        //Asserts
        Account testAccountObj = [SELECT Id, Move_to_SmartFly__c, Booking_Type__c FROM Account LIMIT 1];
        System.assertEquals(true, testAccountObj != null);
        System.assertEquals(true, testAccountObj.Move_to_SmartFly__c);
        System.assertEquals(true, String.isBlank(testAccountObj.Booking_Type__c));
    }
}