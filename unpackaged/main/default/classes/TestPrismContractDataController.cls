@isTest
public class TestPrismContractDataController 
{

    static testMethod void myUnitTest()
    {
        // TO DO: implement unit test
        
        
        //setup account
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
    
        
        insert account;
 
        Contract contractrec= new Contract();
        
        contractrec = commx.CreateOldStandardContract(0);
        contractrec.AccountId = account.id;
        contractrec.RecordTypeid = '012900000007LFZAA2';
        contractrec.Cos_Version__c = '14.4';
        insert contractrec ;
            
        Contract_Performance__c cp = TestUtilityClass.createTestContractPerformanceAmount(contractrec.id);          
        insert cp;      
            
       
        
        Test.startTest();
        
         
        
        PageReference  ref1 = Page.PrismContractDataView ;        
        ref1.getParameters().put('id', contractrec.id);        
        Test.setCurrentPage(ref1);         
        PrismContractDataController lController = new PrismContractDataController() ;          
        lController.GetChartData(); 
        Test.StopTest();

    }
    
}