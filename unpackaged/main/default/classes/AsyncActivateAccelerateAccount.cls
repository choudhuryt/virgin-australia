public class AsyncActivateAccelerateAccount implements Queueable {
    Set<String> accountIds; Set<String> contractIds;
    public AsyncActivateAccelerateAccount(Set<String> accountIds, Set<String> contractIds) {
        this.accountIds = accountIds;
        this.contractIds = contractIds;
    }

    public void execute(QueueableContext context) {
        List<SObject> sos = new List<SObject>();
        for(String accountId: accountIds) {
            sos.add(new Account(Id=accountId, Account_Lifecycle__c='Contract'));
        }

        for(String contractId: contractIds) {
            sos.add(new Contract(Id=contractId, Status='Activated'));
        }

        update sos;
    }
}