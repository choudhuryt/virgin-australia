public with sharing class NewVersionCorporateContractController 
{
    
        //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
    public String userName {get; set;}
    public String contractId;
    public Id newContractId {get; set;}
    public Contract oldContractVersion = new Contract();
    public ID conIdAB ;
    
    public NewVersionCorporateContractController(ApexPages.StandardController controller) 
    {
 
        
        this.controller = controller;

        userName = UserInfo.getName();
 		
                
        contractId = ApexPages.currentPage().getParameters().get('contractId');
        
    
     
       oldContractVersion = [ SELECT AccountId,AFZ1ONDSelected__c,AFZ2ONDSelected__c,B2B_Request__c,BillingCity,StartDate,Name,Contract_Purpose__c,
                              PRISM_Participant__c, BillingCountry,BillingPostalCode,Status, BillingState,BillingStreet,Company_Signed_By__c,CustomerSignedId,Customer_Signed_By_1__c,
                              ContractNumber,ContractTerm,Contract_Type__c,Contract_Version__c,Cos_Version__c,Date_JFF_Amendment_Letter_Recieved__c,
                              Date_JFF_Amendment_Letter_Sent__c,Description,EndDate,End_Date__c,EUZ1ONDSelected__c,EUZ2ONDSelected__c,
                              EUZ3ONDSelected__c,MEZ1ONDSelected__c,MEZ2ONDSelected__c,MEZ3ONDSelected__c,MEZ4ONDSelected__c,Internal_Notes__c,
                              JFF_Amendment_Letter_Status__c,JFF_Amendment_Rejection_Reason__c,JFF_Comments__c,JFF_DL_Contract_Summary_Form_Received__c,
                              JFF_DL_Contract_Summary_Form_Sent__c,JFF_DL_Contract_Summary_Form_Status__c,JFF_EY_Contract_Summary_Form_Received__c,
                              JFF_EY_Contract_Summary_Form_Sent__c,JFF_EY_Contract_Summary_Form_Status__c,JFF_NZ_Contract_Summary_Form_Received__c,
                              JFF_NZ_Contract_Summary_Form_Sent__c,JFF_NZ_Contract_Summary_Form_Status__c,JFF_SQ_Contract_Summary_Form_Received__c,
                              JFF_SQ_Contract_Summary_Form_Sent__c,JFF_SQ_Contract_Summary_Form_Status__c,Opportunity__c,OwnerExpirationNotice,
                              OwnerId,Parent_Contract__c,SQNAOND__c,SQSEAOND__c,SQEUOND__c,SQWAAOND__c ,TTDiscountAvailable__c,	CNDiscountAvailable__c ,
                              EYEUDiscountAvailable__c,EYMEDiscountAvailable__c,EYAFDiscountAvailable__c,DLDiscountAvailable__c,HKDiscountAvailable__c,
                              LONDiscountAvailable__c,SHDiscountAvailable__c,SQAfDiscountAvailable__c,SQASDiscountAvailable__c,SQEUDiscountAvailable__c,
                              Nominated_LMS_Scheme_Coordinator__c,Other_Details__c,Red_Circle__c,	Red_Circle_Notes__c,Status_Match_Email_Address__c,
                              exNZ__c,exHKG__c
                             FROM Contract  where Id = :contractId
                          ];
    

		 
	//Assigning a ID varable for the query lookup as it looks at contract id from contract number
	    
	    conIdAB = contractId;
    
       
    
    }
    
    public Pagereference actionButtonYes()
    {
        
        if(oldContractVersion.Status != 'Activated')
        {
            ApexPages.addMessage( new ApexPages.message( ApexPages.severity.ERROR,
                                   'Only activated contracts can be versioned!' ));
            newContractId = null;
            return null;
        }
        Double newVersionNumber;
        if(oldContractVersion.Contract_Version__c != null)
        {
            newVersionNumber = oldContractVersion.Contract_Version__c + 1;
        }
        else
        {
            newVersionNumber = 2;
         }
        

        
        //check we dont create same version more than once
        List<Contract> versionedContractsList = [Select c.Status, c.Parent_Contract__c, c.Id, c.Contract_Version__c, c.ContractNumber From Contract c
                                                where c.Parent_Contract__c = :contractId
                                                and c.Contract_Version__c = :newVersionNumber];
        if(!versionedContractsList.isEmpty())
        {
            ApexPages.addMessage( new ApexPages.message( ApexPages.severity.ERROR,
                                    'A new version has already been created for this contract!' ));
            newContractId = null;
            return null;
        }
        
        
         // setup the save point for rollback
         Savepoint sp = Database.setSavepoint();
         Contract newContractVersion;
            
         try {
                 

                 
                 
            //copy the Contract
            newContractVersion = oldContractVersion.clone(false, true, true, true);
            newContractVersion.Contract_Version__c = newVersionNumber;
            newContractVersion.Parent_Contract__c = oldContractVersion.Id;
            newContractVersion.Name = oldContractVersion.Name + '_' + 'Version' + '_' + newVersionNumber ;  
            newContractVersion.Status = 'Draft';
            insert newContractVersion;
            
            //for testing purposes
            newContractId = newContractVersion.id;
                                   
            
           
           	List<Contract_Fulfilment__c> ContractFulFilmentNew = new List<Contract_Fulfilment__c>(); 
            ContractFulFilmentNew = getContractFulfilment(newContractVersion.id,conIdAB);
            if (ContractFulFilmentNew.size() > 0){
            	insert ContractFulFilmentNew;
            }
           
             
           List<Market__c> Marketdetails = new List<Market__c>(); 
           Marketdetails = getMarketdetails(newContractVersion.id,conIdAB);
            if (Marketdetails.size() > 0){
            	insert Marketdetails;
            }
             
            List<Proposal_Table__c> proposaldetails = new List<Proposal_Table__c>(); 
            proposaldetails = getproposaldetails(newContractVersion.id,conIdAB);
            if (proposaldetails.size() > 0){
            	insert proposaldetails;
            }  
           
            List<Additional_Benefits__c> additionalbenefits = new List<Additional_Benefits__c>(); 
            additionalbenefits = getAddBenefits(newContractVersion.id,conIdAB);
            if (additionalbenefits.size() > 0){
            	insert additionalbenefits;
            }
            
            VelocityStatusMatch(newContractVersion.id,conIdAB);
             
            
         } 
         catch (Exception e)
         {
            // roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            newContractId = null;
            return null;
         }
         

        return new PageReference('/'+ newContractVersion.Id);
    }
    
    public Pagereference actionButtonNo()
    {
        return new PageReference('/'+ contractId);
    }
    
  

    private Boolean VelocityStatusMatch(Id newContractId, Id conIdAB){
      
      List <Velocity_Status_Match__c> VSMList = new List<Velocity_Status_Match__c>();
      List <Velocity_Status_Match__c> updateVSMList = new List<Velocity_Status_Match__c>();
      List <Velocity_Status_Match__c> insertVSMList = new List<Velocity_Status_Match__c>();
      VSMList = [SELECT Account__c, Approval_Date__c, Approval_Status__c, Contract__c, Date_Requested__c, IsDeleted, Delivery_To__c, Delivery_To_Other__c, IsAccountContract__c, Justification__c, LastActivityDate, LastModifiedById, LastModifiedDate, Passenger_Email__c, Passenger_Name__c, Passenger_Velocity_Number__c, Position_in_Company__c, Id, Related_Account_del__c, Status_Match_or_Upgrade__c, SystemModstamp, Name, Within_Contract__c FROM Velocity_Status_Match__c where Contract__c = :conIdAB ];
      
      for (Integer i = 0; i < VSMList.size(); i++){
        
        Velocity_Status_Match__c vsm = new Velocity_Status_Match__c();
        
        vsm = VSMList.get(i);
        vsm.Contract__c =newContractId;
        updateVSMList.add(vsm);
        
        
      }
      
      if(updateVSMList <> null){
        update updateVSMList;
        
        return true;
        
      }else return false;
     
    } 
    
    
    private List<Contract_Fulfilment__c> getContractFulfilment(Id newContractId, Id oldContractId){
    	
    	List<Contract_Fulfilment__c> contractFulList = new List<Contract_Fulfilment__c>();
    	
    	
    	Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Contract_Fulfilment__c.fields.getMap();
		List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
		
		String theQuery = 'SELECT ';
		for(Schema.SObjectField s : fldObjMapValues)
		{
		   String theName = s.getDescribe().getName();
		   theQuery += theName + ',';
		}
		
		// Trim last comma
		theQuery = theQuery.subString(0, theQuery.length() - 1);
		
		// Finalize query string
		theQuery += ' FROM Contract_Fulfilment__c WHERE Contract__c = \'' + oldContractId + '\' and Status__c =\'Active\'';
		
		// Make your dynamic call
		//oldContractFulfilment = (Contract_Fulfilment__c)Database.query(theQuery);
    	
    	List<Sobject> s = new List<Sobject>();
		s = VirginsutilitiesClass.getData(theQuery);
		 //s = Database.query(theQuery);
		for (Integer i = 0; i < s.size(); i++)
			{
		Contract_Fulfilment__c oldContractFulfilment = new Contract_Fulfilment__c();
    	Contract_Fulfilment__c newContractFulfilment = new Contract_Fulfilment__c();
		oldContractFulfilment = (Contract_Fulfilment__c)s.get(i);  	
		newContractFulfilment = oldContractFulfilment.clone(false, true, true, true);
    	newContractFulfilment.Contract__c = newContractId;
    	contractFulList.add(newContractFulfilment);
			}
    	
    	return contractFulList; 
    }
         
    private List<Market__c> getMarketDetails(Id newContractId, Id oldContractId){
    	
    	List <Market__c> mar = new List<Market__c>();
		   	
		
		Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Market__c.fields.getMap();
		List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
		
		String theQuery = 'SELECT ';
		for(Schema.SObjectField s : fldObjMapValues)
		{
		   String theName = s.getDescribe().getName();
		   theQuery += theName + ',';
		}
		
		// Trim last comma
		theQuery = theQuery.subString(0, theQuery.length() - 1);
		
		// Finalize query string
		theQuery += ' FROM Market__c WHERE Contract__c = \'' + oldContractId + '\'';
		
		List<Sobject> s = new List<Sobject>();
		s = VirginsutilitiesClass.getData(theQuery);
		 //s = Database.query(theQuery);
		for (Integer i = 0; i < s.size(); i++)
			{
		Market__c oldmarket = new Market__c();
		Market__c newmarket = new Market__c();    
		oldmarket = (Market__c)s.get(i);  	
		newmarket = oldmarket.clone(false, true, true, true);
    	newmarket.Contract__c = newContractId;
    	mar.add(newmarket);
			}
    	
    	
    	
    	return mar; 
    	
    }
    
     private List<Proposal_Table__c> getProposalDetails(Id newContractId, Id oldContractId){
    	
    	List <Proposal_Table__c> proposal = new List<Proposal_Table__c>();
		   	
		
		Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Proposal_Table__c.fields.getMap();
		List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
		
		String theQuery = 'SELECT ';
		for(Schema.SObjectField s : fldObjMapValues)
		{
		   String theName = s.getDescribe().getName();
		   theQuery += theName + ',';
		}
		
		// Trim last comma
		theQuery = theQuery.subString(0, theQuery.length() - 1);
		
		// Finalize query string
		theQuery += ' FROM Proposal_Table__c WHERE Contract__c = \'' + oldContractId + '\'';
		
		
		
		List<Sobject> s = new List<Sobject>();
		s = VirginsutilitiesClass.getData(theQuery);
		 //s = Database.query(theQuery);
		for (Integer i = 0; i < s.size(); i++)
			{
		Proposal_Table__c oldproposal = new Proposal_Table__c();
		Proposal_Table__c newproposal = new Proposal_Table__c();    
		oldproposal = (Proposal_Table__c)s.get(i);  	
		newproposal = oldproposal.clone(false, true, true, true);
    	newproposal.Contract__c = newContractId;
    	proposal.add(newproposal);
			}
    	
    	
    	
    	return proposal; 
    	
    }
    
        private List<Additional_Benefits__c> getAddBenefits(Id newContractId, Id oldContractId){
    	
    	List <Additional_Benefits__c> addben = new List<Additional_Benefits__c>();
	  	
		
		Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Additional_Benefits__c.fields.getMap();
		List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
		
		String theQuery = 'SELECT ';
		for(Schema.SObjectField s : fldObjMapValues)
		{
		   String theName = s.getDescribe().getName();
		   theQuery += theName + ',';
		}
		
		// Trim last comma
		theQuery = theQuery.subString(0, theQuery.length() - 1);
		
		// Finalize query string
		theQuery += ' FROM Additional_Benefits__c WHERE Contract__c = \'' + oldContractId + '\'';
		
		
    	
    	List<Sobject> s = new List<Sobject>();
		s = VirginsutilitiesClass.getData(theQuery);
		 //s = Database.query(theQuery);
		for (Integer i = 0; i < s.size(); i++)
			{
		Additional_Benefits__c oldaddben = new Additional_Benefits__c();
		Additional_Benefits__c newaddben= new Additional_Benefits__c();   
		oldaddben = (Additional_Benefits__c)s.get(i);  	
		newaddben = oldaddben.clone(false, true, true, true);
    	newaddben.Contract__c = newContractId;
    	//newIntraController.Intra_WA_Discount_off_Published_Fare2__c=1;
    	addben.add(newaddben);
			}
    	
    	return addben; 
    	
    }
   
      
    public Contract getOldContract()
    {
        return oldContractVersion;
    }
    
}