/**
 * @description       : Handler class for SendChattoAlmsTrigger
 * @CreatedBy         : CloudWerx
 * @UpdatedBy         : CloudWerx
**/
public class SendChattoAlmsTriggerHandler {
    public static void liveChatTranscriptUpdateActivities(List<LiveChatTranscript> liveChatTranscripts, Map<Id, LiveChatTranscript> oldLiveChatTranscriptMap, Boolean isBeforeUpdate, Boolean isAfterUpdate) {
        List<Id> chatIds = new List<Id>();
        List<LiveChatTranscript> maskLiveChatTranscripts = new List<LiveChatTranscript>();
        String type = '';
        //@updateBy : cloudwerx here added check to allow TestSendCasetoAlmsTrigger to execute update conditions
        if(Test.isRunningTest() || System.Label.POST_ALMS_Integration == 'Active') {
            for(LiveChatTranscript liveChatTranscriptObj :liveChatTranscripts) {
                if(isBeforeUpdate) {  
                    if (oldLiveChatTranscriptMap.get(liveChatTranscriptObj.Id).Body != liveChatTranscriptObj.Body || oldLiveChatTranscriptMap.get(liveChatTranscriptObj.Id).Subject__c != liveChatTranscriptObj.Subject__c) { 
                        if (pcify.Manager.getManager('LiveChatTranscript').pcify__isActive__c) {
                            maskLiveChatTranscripts.add(liveChatTranscriptObj);
                        }
                    }
                } else if(isAfterUpdate) {
                    System.debug(oldLiveChatTranscriptMap.get(liveChatTranscriptObj.Id).ContactId+'--Contact--'+liveChatTranscriptObj.ContactId);
                    System.debug(oldLiveChatTranscriptMap.get(liveChatTranscriptObj.Id).CaseId+'--Case--'+liveChatTranscriptObj.CaseId+'--'+liveChatTranscriptObj.Velocity_Number__c+'--'+liveChatTranscriptObj.Contact_Velocity_Number__c);
                    if((oldLiveChatTranscriptMap.get(liveChatTranscriptObj.Id).Status != liveChatTranscriptObj.Status && liveChatTranscriptObj.Status == 'Completed' && (liveChatTranscriptObj.Velocity_Number__c != NULL || liveChatTranscriptObj.ContactId != NULL))|| (oldLiveChatTranscriptMap.get(liveChatTranscriptObj.Id).Velocity_Number__c != liveChatTranscriptObj.Velocity_Number__c && liveChatTranscriptObj.Velocity_Number__c != NULL /*&& liveChatTranscriptObj.Velocity_Number__c != liveChatTranscriptObj.Contact_Velocity_Number__c && liveChatTranscriptObj.Velocity_Number__c != liveChatTranscriptObj.Case_Velocity_Number__c*/)) {
                        if(liveChatTranscriptObj.Velocity_Number__c != NULL) {
                            chatIds.add(liveChatTranscriptObj.Id);
                            type = 'Chat';
                        } else if(liveChatTranscriptObj.Velocity_Number__c == NULL && liveChatTranscriptObj.ContactId != NULL) {
                            chatIds.add(liveChatTranscriptObj.Id);
                            type = 'Contact Chat';
                        }                        
                    } else if(oldLiveChatTranscriptMap.get(liveChatTranscriptObj.Id).ContactId != liveChatTranscriptObj.ContactId && liveChatTranscriptObj.ContactId != NULL /*&& liveChatTranscriptObj.Velocity_Number__c == NULL && liveChatTranscriptObj.Case_Velocity_Number__c == NULL*/) {
                        chatIds.add(liveChatTranscriptObj.Id);
                        type = 'Contact Chat';
                    } else if(oldLiveChatTranscriptMap.get(liveChatTranscriptObj.Id).CaseId != liveChatTranscriptObj.CaseId && liveChatTranscriptObj.CaseId != NULL /*&& liveChatTranscriptObj.Velocity_Number__c == NULL && liveChatTranscriptObj.Contact_Velocity_Number__c == NULL*/) {
                        chatIds.add(liveChatTranscriptObj.Id);
                        type = 'Case Chat';
                    }
                }            
                
            }       
        }   
        
        if (maskLiveChatTranscripts.size() > 0) {
            pcify.Processor.maskCreditCards(maskLiveChatTranscripts, pcify.Manager.getMaskFields('LiveChatTranscript'), 'LiveChatTranscript');
        }
        
        if(chatIds.size() > 0) {
            If(type == 'chat') { }
            SendCommsToAlmsWS.sendtoAlms(chatIds, type);
        }
    }
}