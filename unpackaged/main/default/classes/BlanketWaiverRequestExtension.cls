/**
 * @author: Joy S. Marasigan
 * @createdDate: April 4, 2016
 * @description: An extension class for the Blanket Waiver Request object to handle the creation of new blanket request versions.
 * Please see Case #00101013 for more information regarding the requirements
 */
 
public class BlanketWaiverRequestExtension {

    public static final Integer idLength = 6;
    public Blanket_Waiver_Request__c bwrParentObject {get; set;}
    public Blanket_Waiver_Request__c bwrNewVersion {get; set;}
    public Boolean isNew = FALSE;
    
    public BlanketWaiverRequestExtension(ApexPages.StandardController controller){
        
        //a custom setting is used to keep track of the latest number of original records.
        String bwrOrigCount = String.valueOf(Integer.valueOf(Blanket_Waiver_Request_Settings__c.getValues('BWR Original Count').BWR_Original_Count__c) + 1); 
        
        //for new records
        if(controller.getId() == NULL){
            
            this.bwrNewVersion = (Blanket_Waiver_Request__c) controller.getRecord();
            this.bwrNewVersion.Unique_Waiver_Code__c = 'BW' + bwrOrigCount.leftPad(idLength).replace(' ', '0');
            this.bwrNewVersion.Submission_Counter__c =  1;
            this.isNew = TRUE;

        }
        //for versions
        else{
            
            this.bwrParentObject = [SELECT Id, Name, Unique_Waiver_Code__c, Waiver_Type__c, Submission_Counter__c,
                                        Waiver_Group__c, Business_Justification__c, Blanket_Waiver_Start_Date__c,
                                        Blanket_Waiver_End_Date__c, Replicated_From__c
                                        FROM Blanket_Waiver_Request__c
                                        WHERE Id =: controller.getId()
                                    ];
                                
            this.bwrNewVersion = new Blanket_Waiver_Request__c();
                                                        
            this.bwrNewVersion.Unique_Waiver_Code__c = this.bwrParentObject.Unique_Waiver_Code__c;
            this.bwrNewVersion.Waiver_Type__c = this.bwrParentObject.Waiver_Type__c;
            this.bwrNewVersion.Waiver_Group__c = this.bwrParentObject.Waiver_Group__c;
            this.bwrNewVersion.Business_Justification__c = this.bwrParentObject.Business_Justification__c;
            this.bwrNewVersion.Blanket_Waiver_Start_Date__c = this.bwrParentObject.Blanket_Waiver_Start_Date__c;
            this.bwrNewVersion.Blanket_Waiver_End_Date__c = this.bwrParentObject.Blanket_Waiver_End_Date__c;
            
            if(String.isBlank(this.bwrParentObject.Replicated_From__c)){
                
                this.bwrNewVersion.Replicated_From__c = this.bwrParentObject.Id;
            }
            else{
               
               this.bwrNewVersion.Replicated_From__c = this.bwrParentObject.Replicated_From__c;
               
            }
            
            if(!String.isBlank(this.bwrNewVersion.Unique_Waiver_Code__c)){
                
                List<AggregateResult> versions = [SELECT COUNT(Id)
                                                    FROM Blanket_Waiver_Request__c
                                                    WHERE (Unique_Waiver_Code__c =: this.bwrNewVersion.Unique_Waiver_Code__c)
                                                ];
            
                Decimal lastVersion = (Decimal) versions[0].get('expr0');
                this.bwrNewVersion.Submission_Counter__c =  lastVersion + 1;
            
                Blanket_Waiver_Request__c lastVersionObject = [SELECT Blanket_Waiver_End_Date__c
                                                                FROM Blanket_Waiver_Request__c
                                                                WHERE (Submission_Counter__c =: lastVersion
                                                                AND Unique_Waiver_Code__c =: this.bwrNewVersion.Unique_Waiver_Code__c)
                                                            ];
               	                                            
                this.bwrNewVersion.Last_End_Date__c = lastVersionObject.Blanket_Waiver_End_Date__c;
                
            }
            
        }
    }
    
    //Function to override the original saving of Blanket Waiver Request records
    public PageReference saveVersion(){
        
        try{
			
    		insert bwrNewVersion;
    		
    		if(isNew){
    		    
    		    Blanket_Waiver_Request_Settings__c bwrSettings = Blanket_Waiver_Request_Settings__c.getValues('BWR Original Count');
    		    bwrSettings.BWR_Original_Count__c = Integer.valueOf(bwrSettings.BWR_Original_Count__c) + 1;
    		    update bwrSettings;
    		    
    		}

		} catch(DmlException e){
            
            //silent debug - revise if you can.
            System.debug(e.getMessage());

			return null;

		}
		
		PageReference pageRef = new PageReference('/' + bwrNewVersion.Id);

    	return pageRef;
    }
}