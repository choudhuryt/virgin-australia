/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 23.05.2022
**Description: Test Class for VelocitySnapshot
* @UpdatedBy : Cloudwerx
*/
@isTest
public class VelocitySnapshot_Test {
    public static String velocityNumber = '0000360491';
    
    @testSetup static void setup() {
        Apex_Callouts__c testApexCalloutObj = TestDataFactory.createTestApexCallouts('VelocityDetails', 'https://services-mssl.virginaustralia.com/service/partner/salesforce/1.0/SalesforceLoyalty', 90000, 'client_mssl_virginaustralia2018_com');
        INSERT testApexCalloutObj;
        ALMS_Integration__c testALMSIntegrationObj = TestDataFactory.createTestALMSIntegration('Activate ALMS', false);
        INSERT testALMSIntegrationObj;
    }
    
    //REST Mock
    Class SingleRequestMock implements HttpCalloutMock {
        protected Integer code;
        protected String status;
        protected String bodyAsString;
        protected Blob bodyAsBlob;
        protected Map<String, String> responseHeaders;
        
        public SingleRequestMock(Integer code, String status, String body, Map<String, String> responseHeaders){
            this.code = code;
            this.status = status;
            this.bodyAsBlob = null;
            this.bodyAsString = body;
            this.responseHeaders = responseHeaders;
        }
        
        public SingleRequestMock(Integer code, String status, Blob body, Map<String, String> responseHeaders){
            this.code = code;
            this.status = status;
            this.bodyAsBlob = body;
            this.bodyAsString = null;
            this.responseHeaders = responseHeaders;
        }
        
        public HTTPResponse respond(HTTPRequest req){
            CalloutException e;
            HTTPResponse resp = new HttpResponse();
            if(code != null) {
                resp.setStatusCode(code);
            } else {
                e = (CalloutException)CalloutException.class.newInstance();
                e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings.');
                //throw e;
            }
            
            resp.setStatus(status);
            if(bodyAsBlob != null) {
                resp.setBodyAsBlob(bodyAsBlob);
            } else if(e != null) {
                resp.setBody(String.valueOf(e));
            } else {
                resp.setBody(bodyAsString);
            }
            if(responseHeaders != null){
                for(String key: responseHeaders.keySet()){
                    resp.setHeader(key, responseHeaders.get(key));
                }
            }
            return resp;
        }        
    }
    
    public class WebServiceMockImplR implements WebServiceMock {
        public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
                GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType loyDet = new GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType();
                GuestCaseVelocityInfoModel.MemberInformationType memInfo = new GuestCaseVelocityInfoModel.MemberInformationType();
                loyDet.MemberInformation = memInfo;
                GuestCaseVelocityInfoModel.PersonType perType = new GuestCaseVelocityInfoModel.PersonType();
                perType.Surname = 'XYZ';
                perType.GivenName = 'ABC';
                memInfo.Person = perType;
                GuestCaseVelocityInfoModel.ProfileInformationType profInfoType = new GuestCaseVelocityInfoModel.ProfileInformationType();
                profInfoType.TierLevel = 'R';
                loyDet.ProfileInformation = profInfoType;
                response.put('response_x', loyDet); 
            }
    }
    
    public class WebServiceMockImplS implements WebServiceMock {
        public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
                GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType loyDet = new GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType();
                GuestCaseVelocityInfoModel.MemberInformationType memInfo = new GuestCaseVelocityInfoModel.MemberInformationType();
                loyDet.MemberInformation = memInfo;
                GuestCaseVelocityInfoModel.PersonType perType = new GuestCaseVelocityInfoModel.PersonType();
                perType.Surname = 'XYZ';
                perType.GivenName = 'ABC';
                memInfo.Person = perType;
                GuestCaseVelocityInfoModel.ProfileInformationType profInfoType = new GuestCaseVelocityInfoModel.ProfileInformationType();
                profInfoType.TierLevel = 'S';
                loyDet.ProfileInformation = profInfoType;
                response.put('response_x', loyDet); 
            }
    }
    
    public class WebServiceMockImplG implements WebServiceMock {
        public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
                GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType loyDet = new GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType();
                GuestCaseVelocityInfoModel.MemberInformationType memInfo = new GuestCaseVelocityInfoModel.MemberInformationType();
                loyDet.MemberInformation = memInfo;
                GuestCaseVelocityInfoModel.PersonType perType = new GuestCaseVelocityInfoModel.PersonType();
                perType.Surname = 'XYZ';
                perType.GivenName = 'ABC';
                memInfo.Person = perType;
                GuestCaseVelocityInfoModel.ProfileInformationType profInfoType = new GuestCaseVelocityInfoModel.ProfileInformationType();
                profInfoType.TierLevel = 'G';
                loyDet.ProfileInformation = profInfoType;
                response.put('response_x', loyDet); 
            }
    }
    
    public class WebServiceMockImplP implements WebServiceMock {
        public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
                GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType loyDet = new GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType();
                GuestCaseVelocityInfoModel.MemberInformationType memInfo = new GuestCaseVelocityInfoModel.MemberInformationType();
                loyDet.MemberInformation = memInfo;
                GuestCaseVelocityInfoModel.PersonType perType = new GuestCaseVelocityInfoModel.PersonType();
                perType.Surname = 'XYZ';
                perType.GivenName = 'ABC';
                memInfo.Person = perType;
                GuestCaseVelocityInfoModel.ProfileInformationType profInfoType = new GuestCaseVelocityInfoModel.ProfileInformationType();
                profInfoType.TierLevel = 'P';
                loyDet.ProfileInformation = profInfoType;
                response.put('response_x', loyDet); 
            }
    }
    
    public class WebServiceMockImplV implements WebServiceMock {
        public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
                GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType loyDet = new GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType();
                GuestCaseVelocityInfoModel.MemberInformationType memInfo = new GuestCaseVelocityInfoModel.MemberInformationType();
                loyDet.MemberInformation = memInfo;
                GuestCaseVelocityInfoModel.PersonType perType = new GuestCaseVelocityInfoModel.PersonType();
                perType.Surname = 'XYZ';
                perType.GivenName = 'ABC';
                memInfo.Person = perType;
                GuestCaseVelocityInfoModel.ProfileInformationType profInfoType = new GuestCaseVelocityInfoModel.ProfileInformationType();
                profInfoType.TierLevel = 'V';
                loyDet.ProfileInformation = profInfoType;
                response.put('response_x', loyDet); 
            }
    }
    
    @isTest
    static void testVelocitySnapShotRestRedSuccess(){
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "RED", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Ok' ,body, null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.startTest();            
        VelocitySnapshot.almsActivated = 'Active';
        VelocitySnapshot snapshot = new VelocitySnapshot(velocityNumber, 'Rest RedSuccess');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.tierName, 'Red');       
    }
    
    @isTest
    static void testVelocitySnapShotRestSilverSuccess(){
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "SILVER", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Ok', body, null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        Test.startTest();
        VelocitySnapshot.almsActivated = 'Active';
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber ,'Rest SilverSucccess');
        Test.stopTest(); 
        // Asserts       
        System.assertEquals(snapshot.tierName, 'Silver');        
    }
    
    @isTest
    static void testVelocitySnapShotRestGoldSuccess(){
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "Gold", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Ok', body, null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);    
        Test.startTest();     
        VelocitySnapshot.almsActivated = 'Active';
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber, 'Rest GoldSuccess');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.tierName, 'Gold');    
    }
    
    @isTest
    static void testVelocitySnapShotRestPlatinumSuccess(){
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "PLATINUM", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Ok', body, null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        Test.startTest();       
        VelocitySnapshot.almsActivated = 'Active';
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber, 'Rest PlatinumSuccess');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.tierName, 'Platinum');       
    }
    
    @isTest
    static void testVelocitySnapShotRestVipSuccess(){
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "VIP", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Ok', body, null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        Test.startTest();            
        VelocitySnapshot.almsActivated = 'Active';
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber, 'Rest VIPSuccess');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.tierName, 'VIP');        
    }
    
    @isTest
    static void testVelocitySnapShotRestNoTitle(){
        String body = '{"data":{"channel":"AGENT_UI","lastModifiedAt":"2022-04-12T17:21:36.12Z","subType":"INDIVIDUAL","membershipId":"0000360491","pointBalance":610,"pointBalanceExpiryDate":"2024-02-29","statusCredit":200,"eligibleSector":0,"status":{"main":"OPEN","effectiveAt":"2021-07-19","sub":{"accountIdentifier":{"activity":"ACTIVE","merge":"NEUTRAL","fraudSuspicion":"NEUTRAL","billing":"INACTIVE","completeness":{"percentage":100},"accountLoginStatus":"UNLOCKED"},"characteristicIdentifier":{"lifeCycle":{"isDeceased":false,"ageGroup":"ADULT"}}}},"enrolmentSource":"MCC","enrolmentDate":"2021-07-19","mainTier":{"level":"RED","code":"SR","label":"STANDARD RED","validityStartDate":"2021-07-18T14:00:00Z"},"individual":{"identity":{"suffix":"Jr","preferredFirstName":"Tessssaaasasasating","birthDate":"1933-10-09","gender":"MALE","employments":[{"company":"Virgin"}]},"consents":[{"frequency":"Daily","status":"NOT_GRANTED","to":"VELOCITY","via":"POST","for":"FUEL PARTNER OFFERS"}],"preferences":[{"category":"REFERENTIAL_DATA","subCategory":"CURRENCY","value":"AUD"}],"fulfillmentDetail":{},"contact":{"emails":[{"category":"PERSONAL","address":"jorge.test03-nosec@virginaustralia.com","contactValidity":{"isMain":true,"isValid":true,"isConfirmed":false}}],"phones":[{"category":"PERSONAL","deviceType":"LANDLINE","countryCallingCode":"61","areaCode":"02","number":"987654356","contactValidity":{"isMain":false,"isValid":true,"isConfirmed":false}}],"addresses":[{"category":"BUSINESS","lines":["Test Line 1","Test Line 2",null],"postalCode":"1234","countryCode":"AU","cityName":"Windsor","stateCode":"QLD","contactValidity":{"isMain":true,"isValid":true,"isConfirmed":false}}]}},"enrolmentDetail":{"referringMemberId":"1026372134","promoCode":"AFLLIONS"}}}';
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Ok', body, null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        Test.startTest();            
        VelocitySnapshot.almsActivated = 'Active';
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber, 'Rest VIPSuccess');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.tierName, 'Red');        
    }
    
    @isTest 
    static void testVelocitySnapShotSOAPRedSuccess() {
        updateALMSIntegrationRecord();
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplR());
        Test.startTest();
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber, 'SOAP RedSuccess');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.tierName, 'Red');
    }
    
    @isTest 
    static void testVelocitySnapShotSOAPSilverSuccess() {
        updateALMSIntegrationRecord();
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplS());
        Test.startTest();
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber, 'SOAP SilverSuccess');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.tierName, 'Silver');
    }
    
    @isTest 
    static void testVelocitySnapShotSOAPGoldSuccess() {
        updateALMSIntegrationRecord();
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplG());
        Test.startTest();
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber, 'SOAP GoldSuccess');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.tierName, 'Gold');
    }
    
    @isTest 
    static void testVelocitySnapShotSOAPPlatinumSuccess() {
        updateALMSIntegrationRecord();
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplP());
        Test.startTest();
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber, 'SOAP PlatinumSuccess');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.tierName, 'Platinum');
    }
    
    @isTest 
    static void testVelocitySnapShotSOAPVipSuccess() {
        updateALMSIntegrationRecord();
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplV());
        Test.startTest();
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber, 'SOAP VIPSuccess');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.tierName, 'VIP');
    }
    
    @isTest
    static void testVelocitySnapShotRestFailure(){
        String body= '{ "code": 37105, "title": "Not Found", "description": "No matching member found.", "status": 404 }';
        SingleRequestMock fakeResponse = new SingleRequestMock(404, 'Not Found', body, null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        Test.startTest();            
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber, 'Rest Failure');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.velocityNumber, velocityNumber);
    }
    
    @isTest
    static void testVelocitySnapShotRestFailureNoNumber(){
        String body= '{}';
        SingleRequestMock fakeResponse = new SingleRequestMock(404, 'Not Found', body, null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        Test.startTest();            
        VelocitySnapshot Snapshot = new VelocitySnapshot('', 'REST BlankBody');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.velocityNumber, '');
    }
    
    @isTest
    static void testVelocitySnapShotRestFailureWithBlankBody(){
        String body= '{}';
        SingleRequestMock fakeResponse = new SingleRequestMock(404, 'Not Found', body, null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        Test.startTest();            
        VelocitySnapshot Snapshot = new VelocitySnapshot(velocityNumber, 'REST BlankBody');
        Test.stopTest();
        // Asserts
        System.assertEquals(snapshot.velocityNumber, velocityNumber);
    }
    
    private static void updateALMSIntegrationRecord() {
        ALMS_Integration__c testALMSIntegrationObj = [SELECT Id, Activate_GET__c FROM ALMS_Integration__c LIMIT 1];
        testALMSIntegrationObj.Activate_GET__c = true;
        UPDATE testALMSIntegrationObj;
    }
}