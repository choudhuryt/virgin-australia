/**
 * @description       : WaiverPointPageController
 * UpdatedBy: cloudwerx (Did indentation)
**/
public with sharing class WaiverPointPageController {
    
    public List<Waiver_Point__c> searchResults {get;set;}
    public list<AggregateResult> rollupresult {get;set;}
    public List<Account> AccoutDetails {get;set;}
    public String CurrentAccountId { get; set;}
    public Boolean IsWaiverParent { get; set;}
    public String dynamicQuery { get; set;}
    public integer sumallocated { get; set;}
    
    public WaiverPointPageController() {
        if(CurrentAccountId == null) {
            CurrentAccountId= ApexPages.currentPage().getParameters().get('cid');
        }
        dynamicQuery='';
        IsWaiverParent =false ;
    }
    
    public PageReference initDisc()  {  
        if(CurrentAccountId == null) {
            CurrentAccountId= ApexPages.currentPage().getParameters().get('cid');
        }
        
        AccoutDetails = [SELECT Id,Name, IsWavierParentAccount__c FROM Account WHERE Id=:CurrentAccountId];
        
        if (AccoutDetails[0].IsWavierParentAccount__c ) {
            IsWaiverParent = true ;
            searchResults= [SELECT id , Account__r.name, Waiver_Points_Allocated__c, Waiver_Points_Used__c,
                            Waiver_Points_Pending_Approval__c,Waiver_Point_Status__c,Waiver_Points_Remaining__c
                            FROM Waiver_Point__c WHERE Waiver_Point_Parent__c =:CurrentAccountId and status__c = 'Active'];
            
            rollupresult = [SELECT Waiver_Point_Parent__r.Name AccName, sum(Waiver_Points_Allocated__c)totalallocated, sum(Waiver_Points_Used__c)totalused,
                            sum(Waiver_Points_Pending_Approval__c)totalpending, sum(Waiver_Points_Remaining__c)totalremaining
                            FROM Waiver_Point__c WHERE Waiver_Point_Parent__c =:CurrentAccountId AND status__c = 'Active' 
                            GROUP BY Waiver_Point_Parent__r.Name];
        } else {
            searchResults= [SELECT id, Account__r.name, Waiver_Points_Allocated__c, Waiver_Points_Used__c,
                            Waiver_Points_Pending_Approval__c, Waiver_Point_Status__c, Waiver_Points_Remaining__c
                            FROM Waiver_Point__c WHERE Account__c =:CurrentAccountId AND status__c = 'Active' ];
        }   
        if(searchResults.size() == 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No waiver points allocated for account');
            ApexPages.addMessage(myMsg); 
        }   
        return null;  
    } 
    
    public list<AggregateResult> rolluplist {
        get { return rollupresult;}
    }
    
    public String CurrentAccountDetails {
        set;
        get
        {
            if(CurrentAccountId !=null) {
                Account acc = [SELECT Id, Name FROM Account WHERE Id=:CurrentAccountId];
                if(acc.Name !=null) {
                    return acc.Name;
                }
            }
            return '';
        }
    }
}