@isTest
private class TestContractMarketCalculation 
{
    
    static testMethod void myUnitTestContract1() 
    {
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';
        
        insert account;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.StageName = 'Sales Opportunity Analysis'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1; 
        
        Contract con = new Contract();
        con = commx.CreateOldStandardContract(0);
        con.AccountId = account.id;
        con.Opportunity__c = opp1.id ;
        con.SQ_Discount_Type__c = 'Specific OD Pairs';
        insert con;
        
        Market__c market1 = new Market__c();
        market1.Opportunity__c = opp1.id ;
        market1.Contract__c = con.id ;
        market1.Name = 'DOM Mainline';
        market1.VA_Revenue_Commitment__c = 100;
        market1.Total_Spend__c = 150;
        insert market1;
        
        Market__c market2 = new Market__c();
        market2.Opportunity__c = opp1.id ;
        market2.Contract__c = con.id ;
        market2.Name = 'DOM Regional';
        market2.VA_Revenue_Commitment__c = 100; 
        market2.Total_Spend__c = 150;
        insert market2; 
        
        
        
        List<Id> conid = new List<Id>();        
        conid.add(con.Id); 
        Test.startTest(); 
        ContractMarketCalculation.UpdateRelatedContract(conid)  ;  
        
        Test.stopTest(); 
        
    }  
    
    
    static testMethod void myUnitTestContract2() 
    {
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';
        
        insert account;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.StageName = 'Sales Opportunity Analysis'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1; 
        
        Contract con = new Contract();
        con = commx.CreateOldStandardContract(0);
        con.AccountId = account.id;
        con.Opportunity__c = opp1.id ;
        con.SQ_Discount_Type__c = 'Specific OD Pairs';
        insert con;
        
        
        
        Market__c market2 = new Market__c();
        market2.Opportunity__c = opp1.id ;
        market2.Contract__c = con.id ;
        market2.Name = 'China';
        market2.VA_Revenue_Commitment__c = 100; 
        insert market2; 
        
        
        Market__c market4 = new Market__c();
        market4.Opportunity__c = opp1.id ;
        market4.Contract__c = con.id ;
        market4.Name = 'North America';
        market4.VA_Revenue_Commitment__c = 100; 
        insert market4; 
        
        
        List<Id> conid = new List<Id>();        
        conid.add(con.Id); 
        Test.startTest(); 
        ContractMarketCalculation.UpdateRelatedContract(conid)  ;  
        
        Test.stopTest(); 
        
    }  
    
    static testMethod void myUnitTestContract3() 
    {
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';
        
        insert account;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.StageName = 'Sales Opportunity Analysis'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1; 
        
        Contract con = new Contract();
        con = commx.CreateOldStandardContract(0);
        con.AccountId = account.id;
        con.Opportunity__c = opp1.id ;
        con.SQ_Discount_Type__c = 'Specific OD Pairs';
        insert con;
        
        Market__c market1 = new Market__c();
        market1.Opportunity__c = opp1.id ;
        market1.Contract__c = con.id ;
        market1.Name = 'UK/Europe SQ';
        market1.VA_Revenue_Commitment__c = 100; 
        insert market1;
        
        Market__c market2 = new Market__c();
        market2.Opportunity__c = opp1.id ;
        market2.Contract__c = con.id ;
        market2.Name = 'UK/Europe EY';
        market2.VA_Revenue_Commitment__c = 100; 
        insert market2; 
        
        
        
        
        List<Id> conid = new List<Id>();        
        conid.add(con.Id); 
        Test.startTest(); 
        ContractMarketCalculation.UpdateRelatedContract(conid)  ;  
        
        Test.stopTest(); 
        
    } 
    
    static testMethod void myUnitTestContract4() 
    {
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';
        
        insert account;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.StageName = 'Sales Opportunity Analysis'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1; 
        
        Contract con = new Contract();
        con = commx.CreateOldStandardContract(0);
        con.AccountId = account.id;
        con.Opportunity__c = opp1.id ;
        con.SQ_Discount_Type__c = 'Specific OD Pairs';
        insert con;
        
        Market__c market1 = new Market__c();
        market1.Opportunity__c = opp1.id ;
        market1.Contract__c = con.id ;
        market1.Name = 'UK/Europe EY';
        market1.VA_Revenue_Commitment__c = 100; 
        insert market1;
        
        Market__c market2 = new Market__c();
        market2.Opportunity__c = opp1.id ;
        market2.Contract__c = con.id ;
        market2.Name = 'Trans Tasman TT';
        market2.VA_Revenue_Commitment__c = 100; 
        insert market2; 
        
        Test.startTest(); 
        
        Market__c market3 = new Market__c();
        market3.Opportunity__c = opp1.id ;
        market3.Contract__c = con.id ;
        market3.Name = 'INT Short Haul';
        market3.VA_Revenue_Commitment__c = 100; 
        insert market3; 
        
        List<Id> conid = new List<Id>();        
        conid.add(con.Id); 
        
        ContractMarketCalculation.UpdateRelatedContract(conid)  ;  
        
        Test.stopTest(); 
        
    }  
}