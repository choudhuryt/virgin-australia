/**
 * Test Class for Opportunity Analysis module 
 */
@isTest
private class TestOpportunityAnalysis {

    static testMethod void myUnitTest() {
        
        CommonObjectsForTest commonObjForTest = new CommonObjectsForTest();
        
        Account account = new Account();
        account = commonObjForTest.CreateAccountObject(0);
  		insert account; 
        
        // Create opportunity
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.AccountId = account.id ;
        opp1.StageName = 'Sales Opportunity Analysis';
        //opp1.StageName = 'Open';
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1;
        
        // Create master analysis
        Analysis__c master1 = new Analysis__c();
        master1.Type__c = 'Buying Criteria';
        master1.Opportunity__c = opp1.id;
        insert master1;
        
        Analysis_Question__c question1 = new Analysis_Question__c();
        question1.Title__c = 'Test1';
        question1.Order__c = 1;
        question1.Analysis__c = master1.Id;
        insert question1;

        Analysis_Question__c question2 = new Analysis_Question__c();
        question2.Title__c = 'Test2';
        question2.Order__c = 2;
        question2.Analysis__c = master1.Id;
        insert question2;
        
        // Start test
        test.startTest();
        
        PageReference AnswerAnalysis = Page.answerAnalysis;
        Test.setCurrentPage( AnswerAnalysis );
        ApexPages.currentPage().getParameters().put('id', master1.id );
    
        // Instanciate Controller
        ApexPages.StandardController con = new ApexPages.StandardController(master1);
        AnalysisController controller = new AnalysisController(con);
        
        // Clone the master
        PageReference ref1 = controller.cloneAnalysis();

        //switch to new page/controller
        Test.setCurrentPage(ref1);
        String strSlave1Id = ApexPages.currentPage().getParameters().get('id');
        con = new ApexPages.StandardController([SELECT Id FROM Analysis__c WHERE Id = :strSlave1Id]);
        controller = new AnalysisController(con);

        // saveandback test
        ref1 = controller.saveandback();
                
        // Get generated analysis
        Analysis__c slave1 = controller.currentInstance;
        //system.debug('########################test: ' + slave1.Id + ' / ' + master1.Id);
        
        // Get question 1
        Analysis_Question__c q1 = controller.currentAnswerList[0].answer;
                
        // Add SWOT
        ApexPages.currentPage().getParameters().put('questionId', q1.Id );
        ref1 = controller.addSWOT();

        // Remove SWOT
        ApexPages.currentPage().getParameters().put('questionId', q1.Id );
        ref1 = controller.addSWOT();        
        
        // Get newly created SWOT
        Analysis_SWOT__c swot1 = [SELECT Id FROM Analysis_SWOT__c WHERE Analysis_Question__c = :q1.Id LIMIT 1];

        // Remove SWOT
        ApexPages.currentPage().getParameters().put('swotID', swot1.Id );       
        ref1 = controller.removeSWOT();     
        
        //switch to new page/controller
        //Test.setCurrentPage(ref1);
        //con = new ApexPages.StandardController([SELECT Id FROM Analysis__c WHERE Id = :strSlave1Id]);
        //controller = new AnalysisController(con);
        
        // Add question
        ref1 = controller.newQuestion();
        
        // Move question down
        ApexPages.currentPage().getParameters().put('questionId', q1.Id );
        ref1 = controller.moveDown();

        // Refresh controller
        Test.setCurrentPage(ref1);
        con = new ApexPages.StandardController([SELECT Id FROM Analysis__c WHERE Id = :strSlave1Id]);
        controller = new AnalysisController(con);
                    
        // Move question up
        ApexPages.currentPage().getParameters().put('questionId', q1.Id );
        //System.debug('############## : ' + [SELECT Id, Order__c   FROM Analysis_Question__c WHERE Id = :q1.ID].Order__c);
        ref1= controller.moveUp();

        // Refresh controller
        //Test.setCurrentPage(ref1);
        //con = new ApexPages.StandardController([SELECT Id FROM Analysis__c WHERE Id = :strSlave1Id]);
        //controller = new AnalysisController(con);

        // modifyManage test
        ref1 = controller.modifyManage();

        // Refresh controller
        //Test.setCurrentPage(ref1);
        //con = new ApexPages.StandardController([SELECT Id FROM Analysis__c WHERE Id = :strSlave1Id]);
        //controller = new AnalysisController(con);
        
        // saveandback test
        ref1 = controller.saveandback();

        // Refresh controller
        Test.setCurrentPage(ref1);
        ApexPages.currentPage().getParameters().put('opptyid', opp1.id );
        ApexPages.currentPage().getParameters().put('id', slave1.id );
        con = new ApexPages.StandardController([SELECT Id FROM Analysis__c WHERE Id = :strSlave1Id]);
        controller = new AnalysisController(con);

        // saveandbackManage test
        ref1 = controller.saveandbackManage();

        // Refresh controller
        Test.setCurrentPage(ref1);
        con = new ApexPages.StandardController([SELECT Id FROM Analysis__c WHERE Id = :strSlave1Id]);
        controller = new AnalysisController(con);

        // manage test
        ref1 = controller.manage();

        // Refresh controller
        //Test.setCurrentPage(ref1);
        //con = new ApexPages.StandardController([SELECT Id FROM Analysis__c WHERE Id = :strSlave1Id]);
        //controller = new AnalysisController(con);
        
        // answer test
        ref1 = controller.answer();

        // Refresh controller
        //Test.setCurrentPage(ref1);
        //con = new ApexPages.StandardController([SELECT Id FROM Analysis__c WHERE Id = :strSlave1Id]);
        //controller = new AnalysisController(con);
        
        // Delete question
        ApexPages.currentPage().getParameters().put('questionId', q1.Id );
        ref1 = controller.deleteQuestion();
              
        // Create new analysis
        AnswerAnalysis = Page.answerAnalysis;
        Test.setCurrentPage( AnswerAnalysis );
        ApexPages.currentPage().getParameters().put('opptyid', opp1.id );
        con = new ApexPages.StandardController(master1);
        controller = new AnalysisController(con);
        ref1 = controller.create();

        // Test select Master Analysis
        PageReference SelectMasterAnalysis = Page.selectMasterAnalysis;
        Test.setCurrentPage( SelectMasterAnalysis );
        ApexPages.currentPage().getParameters().put('opptyid', opp1.id );
        ApexPages.currentPage().getParameters().put('type', 'Buying Criteria' );
    
        // Instanciate Controller
        ApexPages.StandardController con2 = new ApexPages.StandardController(master1);
        AnalysisMasterSelectionController controller2 = new AnalysisMasterSelectionController(con2);
        controller2.selectedAnalysisID = master1.Id;
        PageReference ref2 = controller2.selectAnalysis();
        
        ref2 =  controller2.cancel();
        
        // Stop test
        test.stopTest();
        
    }
}