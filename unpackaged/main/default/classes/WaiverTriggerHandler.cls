public class WaiverTriggerHandler
{
    
    public static void  CalculateTravelBankCompensation (Boolean isDelete, List<Waiver_Favour__c> waiverInTrigger, List<Waiver_Favour__c> waiverOldInTrigger)
    {
      Set<id> Wids  = new Set<id>()  ;     
      List<Waiver_Point__c> lstWPUpdate = new List<Waiver_Point__c>();   
       
        
        
      if(isDelete)
     {
       for( Waiver_Favour__c waiverOld: waiverOldInTrigger)
      {            
            Wids.add(waiverOld.id) ;         
      }  
     }else  
     {     
    for( Waiver_Favour__c waiverdata: waiverInTrigger ) 
     { 
     wids.add(waiverdata.id) ;         
     } 
    } 
        
     List<Waiver_Favour__c> wfList =  [select id ,Compensation_Amount__c, Status__c
                                      FROM Waiver_Favour__c  where 
                                      id = :wids and RecordTypeId in (Select id  from RecordType
                                      where SobjectType = 'Waiver_Favour__c'
									  and IsActive = true 
                                      and DeveloperName in('Travel_Bank_Compensation')) ]; 
        
     List<Waiver_Point__c> wpList =  [select id ,Travel_Bank_Compensation_Allowed__c, 
                                      Travel_Bank_Compensation_Used__c,
                                      Travel_Bank_Compensation_Pending__c,
                                      Travel_Bank_Compensation_Draft__c
                                      from Waiver_Point__c where Account__c = '0016F00001sUAt6'
                                      and status__c = 'Active' 
                                      and Waiver_Type__c = 'Travel Bank Compensation'
                                      order by CreatedDate desc limit 1  ]; 
                                      
        
   
   system.debug ('The sizes' + wplist.size() + '***' + wfList.size() )  ;   
   if (wplist.size() > 0  && wfList.size() > 0)
  {  
      
      
        AggregateResult[] totalcompvalueapproved =
                                              [SELECT SUM(Compensation_Amount__c) sumappv FROM Waiver_Favour__c
                                               where  Status__c in ('Approved' )
                                               and RecordTypeId in (Select id  from RecordType
                                               where SobjectType = 'Waiver_Favour__c'
                                               and IsActive = true 
                                               and DeveloperName in ('Travel_Bank_Compensation'))
                                              ];
       AggregateResult[] totalcompvaluepending = 
                                            [ SELECT SUM(Compensation_Amount__c) sumpend FROM Waiver_Favour__c
                                               where Status__c in ('Pending Approval')
                                               and RecordTypeId in (Select id  from RecordType
                                               where SobjectType = 'Waiver_Favour__c'
											   and IsActive = true 
                                               and DeveloperName in('Travel_Bank_Compensation'))
                                             ]; 
  
        AggregateResult[] totalcompvaluedraft = [ SELECT SUM(Compensation_Amount__c) sumdraft FROM Waiver_Favour__c
                                                  where Status__c in ('Draft')
                                                  and RecordTypeId in (Select id  from RecordType
                                                  where SobjectType = 'Waiver_Favour__c'
											      and IsActive = true 
                                                  and DeveloperName in('Travel_Bank_Compensation'))
                                                 ]; 
      
        
         for(Waiver_Point__c wp: wplist)
         {  
          wp.Travel_Bank_Compensation_Used__c = (decimal)totalcompvalueapproved[0].get('sumappv')==null?0:(decimal)totalcompvalueapproved[0].get('sumappv');  
          wp.Travel_Bank_Compensation_Pending__c = (decimal)totalcompvaluepending[0].get('sumpend')==null?0:(decimal)totalcompvaluepending[0].get('sumpend');
          wp.Travel_Bank_Compensation_Draft__c = (decimal)totalcompvaluedraft[0].get('sumdraft')==null?0:(decimal)totalcompvaluedraft[0].get('sumdraft'); 
          lstWPUpdate.add(wp)  ;  
         } 
       
       if (wplist[0].Travel_Bank_Compensation_Allowed__c > 
           (wplist[0].Travel_Bank_Compensation_Used__c+ wpList[0].Travel_Bank_Compensation_Pending__c + wpList[0].Travel_Bank_Compensation_Draft__c  ))
        {
         update  lstWPUpdate ;    
       }else 
      {
      if  (!Test.isRunningTest() &&  wfList[0].Status__c == 'Draft' )
      {    
       for( Waiver_Favour__c waiverdata: waiverInTrigger )     
        {
         waiverdata.adderror('The max travel bank compensation amount of 70K has been reached.');   
        } 
      }    
      }   
             
  }      
    }
        
   

   
    public static void L3ManagerAllocation( List<Waiver_Favour__c> waiverInTrigger )
    {
       Set<Id> ownerIDs  = new Set<Id>();
       Set<Id> level3IDs = new Set<Id>();
    
       List<Waiver_Favour__c> affectedwaiver = new List<Waiver_Favour__c>();
    
       for( Waiver_Favour__c waiverdata: waiverInTrigger )
       {
        affectedWAIVER.add(waiverdata);
            ownerIDs.add(waiverdata.ownerID);
       }  
        system.debug('The account owner is' + ownerIDs );
        
        List <User> u = [SELECT Id, contract_approver__r.Id, contract_approver__r.Level__c, contract_approver__r.ManagerId
                  FROM User WHERE Id In :ownerIDs];
        
        
       List<Waiver_Favour__c> WFToUpdate = new List<Waiver_Favour__c>();
    
    
      for(Waiver_Favour__c wf : affectedwaiver)
      {
        if (u.size() > 0)
        {    
    	
            
        if  ( u[0].contract_approver__r.Level__c  > 2 )
        {
         wf.L3_Manager__c    = u[0].contract_approver__r.Id; 
        }
            else
        {
         wf.L3_Manager__c    = u[0].contract_approver__r.ManagerId;    
        }
        
        }    
    	WFToUpdate.add(wf); 
        
    } 
        
        
    }

    public static void CalculateAccountLevelPoints( Boolean isDelete, List<Waiver_Favour__c> waiverInTrigger, List<Waiver_Favour__c> waiverOldInTrigger )
  {
   boolean AccountWaiver = false ;   
   Set<Id> accIds  = new Set<Id>();   
   List<Waiver_Point__c> lstWPUpdate = new List<Waiver_Point__c>();   
   List<Account> lstAccUpdate = new List<Account>();  
   AccountWaiver =false;     
   List<RecordType> recordType1 = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
												r.Description, r.BusinessProcessId From RecordType r
											where SobjectType = 'Waiver_Favour__c'
											and IsActive = true
											and DeveloperName = 'Waiver_Point_Head_Office_Waiver' LIMIT 1];
      
   List<RecordType> recordType2 = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
												r.Description, r.BusinessProcessId From RecordType r
											where SobjectType = 'Waiver_Favour__c'
											and IsActive = true
											and DeveloperName = 'Waiver_Point_Store_Level_Waiver' LIMIT 1];   
  if(isDelete)
  {
   for( Waiver_Favour__c waiverOld: waiverOldInTrigger)
   { 
     if (waiverOld.Account__c !=  NULL  && ( waiverOld.recordtypeid == recordType1[0].id || waiverOld.recordtypeid == recordType2[0].id))
     {    
     accIds.add(waiverOld.Account__c);
     AccountWaiver = true ;    
     }    
   }  
  }else  
  {     
   for( Waiver_Favour__c waiverdata: waiverInTrigger ) 
   { 
     if (waiverdata.Account__c !=  NULL   && ( waiverdata.recordtypeid == recordType1[0].id || waiverdata.recordtypeid == recordType2[0].id))
     {    
      accIds.add(waiverdata.Account__c);
       AccountWaiver = true ;   
     }    
   } 
  }   
  
  if( AccountWaiver) 
  {
      
    List<Waiver_Point__c> wpList =  [select id ,End_Date__c,Start_Date__c, Waiver_Points_Allocated__c, Waiver_Points_Used__c,Waiver_Points_Pending_Approval__c
                                from Waiver_Point__c where Account__c =:accIds and status__c = 'Active' order by CreatedDate desc limit 1  ]; 
   
  
   
  if (wplist.size() > 0)
  {   
       AggregateResult[] waiverpointapproved = [SELECT SUM(No_of_Points_allocated__c) appsum FROM Waiver_Favour__c
                                               where Account__c = :accIds  and Status__c in ('Approved' )
                                               and CreatedDate >= :wpList[0].Start_Date__c 
                                               and CreatedDate <= :wpList[0].End_Date__c
                                               and RecordTypeId in (Select id  from RecordType
                                                                    where SobjectType = 'Waiver_Favour__c'
											                        and IsActive = true 
                                                                    and DeveloperName in ('Waiver_Point_Head_Office_Waiver','Waiver_Point_Store_Level_Waiver'))];
        
        
    AggregateResult[] waiverpointpending = [ SELECT SUM(No_of_Points_allocated__c) pendsum FROM Waiver_Favour__c
                                               where Account__c = :accIds and Status__c in ('Pending Approval')
                                               and CreatedDate >= :wpList[0].Start_Date__c 
                                               and CreatedDate <= :wpList[0].End_Date__c
                                               and RecordTypeId in (Select id  from RecordType
                                                                    where SobjectType = 'Waiver_Favour__c'
											                        and IsActive = true 
                                                                    and DeveloperName in ('Waiver_Point_Head_Office_Waiver','Waiver_Point_Store_Level_Waiver'))]; 
  
      
      
       for(Waiver_Point__c wp: wplist)
       { 
        wp.Waiver_Points_Used__c = (decimal)waiverpointapproved[0].get('appsum')==null?0:(decimal)waiverpointapproved[0].get('appsum');  
        wp.Waiver_Points_Pending_Approval__c = (decimal)waiverpointpending[0].get('pendsum')==null?0:(decimal)waiverpointpending[0].get('pendsum');   
        wp.Waiver_Points_Allocated__c = wplist[0].Waiver_Points_Allocated__c;   
        if( (wp.Waiver_Points_Allocated__c < (wp.Waiver_Points_Used__c + wp.Waiver_Points_Pending_Approval__c ) ) || wp.Waiver_Points_Allocated__c == 0  ) 
        {
        for( Waiver_Favour__c waiverdata: waiverInTrigger )     
        {
         waiverdata.adderror('The account does not have sufficient waiver points to action this commercial waiver');   
        } 
        }
        else    
        {         
        SYSTEM.DEBUG( 'Waiver Points Calc' +  wp.Waiver_Points_Used__c + '****' + wp.Waiver_Points_Pending_Approval__c+ wp.Account__c)  ;   
        lstWPUpdate.add(wp)  ;   
        }    
       } 
       update  lstWPUpdate ;
  } else if (!Test.isRunningTest())
  {    
      for( Waiver_Favour__c waiverdata: waiverInTrigger )     
        {
         waiverdata.adderror('The account does not have sufficient waiver points to action this commercial waiver');   
        } 
  }   
  }
  }
  
  public static void CalculateAccountManagerPoints( Boolean isDelete, List<Waiver_Favour__c> waiverInTrigger, List<Waiver_Favour__c> waiverOldInTrigger )
  {
      boolean UserWaiver = false ;
      Set<Id> userIds  = new Set<Id>(); 
      List<Waiver_Point__c> lstwpUpdate = new List<Waiver_Point__c>();   

      List<RecordType> recordType = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
			r.Description, r.BusinessProcessId From RecordType r
			where SobjectType = 'Waiver_Favour__c'
			and IsActive = true
			and DeveloperName = 'Waiver_Point_Account_Manager_Waiver' LIMIT 1];
      

  if(isDelete)
  {
   for( Waiver_Favour__c waiverOld: waiverOldInTrigger)
   { 
     if (waiverOld.OwnerId  !=  NULL &&  waiverOld.recordtypeid == recordType[0].id)
     {    
     userIds.add(waiverOld.OwnerId);
     UserWaiver = true ;    
     }    
   }  
  }else  
  {     
   for( Waiver_Favour__c waiverdata: waiverInTrigger ) 
   { 
     system.debug('The values' + waiverdata.OwnerId + waiverdata.recordtypeid + recordType[0].id  )  ;
     if (waiverdata.OwnerId !=  NULL &&  waiverdata.recordtypeid == recordType[0].id )
     {    
      userIds.add(waiverdata.OwnerId);
      UserWaiver = true;   
     }    
   } 
  }   
 system.debug('Is this account manager waiver' + userIds );
      
if (UserWaiver ) 
{
   List<Waiver_Point__c> wpList =  [select id , End_Date__c,Start_Date__c,Waiver_Points_Allocated__c, Waiver_Points_Used__c,Waiver_Points_Pending_Approval__c
                                from Waiver_Point__c where Account_Manager__c =:userIds and status__c = 'Active' ]; 

    
   
  if (wplist.size() > 0)
  {   
       
   AggregateResult[] waiverpointapproved = [SELECT SUM(No_of_Points_allocated__c) appsum FROM Waiver_Favour__c
                                               where OwnerId = :userIds  and Status__c in ('Approved' )
                                               and CreatedDate >= :wpList[0].Start_Date__c 
                                               and CreatedDate <= :wpList[0].End_Date__c
                                               and RecordTypeId in (Select id  from RecordType
                                                                                   where SobjectType = 'Waiver_Favour__c'
                                                                                   and IsActive = true 
                                                                                   and DeveloperName = 'Waiver_Point_Account_Manager_Waiver')];
        
        
   AggregateResult[] waiverpointpending = [ SELECT SUM(No_of_Points_allocated__c) pendsum FROM Waiver_Favour__c
                                               where OwnerId = :userIds and Status__c in ('Pending Approval')
                                                and CreatedDate >= :wpList[0].Start_Date__c 
                                               and CreatedDate <= :wpList[0].End_Date__c
                                               and RecordTypeId in (Select id  from RecordType
                                                                                    where SobjectType = 'Waiver_Favour__c'
                                                                                    and IsActive = true 
                                                                                    and DeveloperName = 'Waiver_Point_Account_Manager_Waiver')];
 
      
      
       for(Waiver_Point__c wp: wplist)
       { 
        wp.Waiver_Points_Used__c = (decimal)waiverpointapproved[0].get('appsum')==null?0:(decimal)waiverpointapproved[0].get('appsum');  
        wp.Waiver_Points_Pending_Approval__c = (decimal)waiverpointpending[0].get('pendsum')==null?0:(decimal)waiverpointpending[0].get('pendsum');   
        wp.Waiver_Points_Allocated__c = wplist[0].waiver_Points_Allocated__c;        
        if  (wp.Waiver_Points_Allocated__c < (wp.waiver_Points_Used__c + wp.waiver_Points_Pending_Approval__c )) 
        {
        for( Waiver_Favour__c waiverdata: waiverInTrigger )     
        {
         waiverdata.adderror('The account owner does not have sufficient waiver points to action this commercial waiver');   
        } 
        }
        else    
        {     
        lstwpUpdate.add(wp)  ;   
        }    
       } 
      update  lstwpUpdate ;
  }
      else if (!Test.isRunningTest()){
          for( Waiver_Favour__c waiverdata: waiverInTrigger )     
        {
         waiverdata.adderror('The account owner does not have sufficient waiver points to action this commercial waiver');   
        } 
          
      }  
  }   
  }

}