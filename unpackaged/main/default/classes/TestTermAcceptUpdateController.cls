@isTest
public class TestTermAcceptUpdateController 
{
    
  static testMethod void myUnitTest()
  {
        // TO DO: implement unit test
        
        
        //setup account
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0); 
 		insert account;
         
        Account_Self_Update__c newAcctSelfUpdate = new Account_Self_Update__c(); 
        newAcctSelfUpdate.Account__c = account.id;
        insert newAcctSelfUpdate ;
        newAcctSelfUpdate.Agreed_To_Terms_And_Conditions__c = TRUE;
        newAcctSelfUpdate.Agree_To_Use_The_Booking_Code__c = TRUE;
        newAcctSelfUpdate.Authorised_Contact__c = TRUE;
        newAcctSelfUpdate.Authorisation_Rep_Name__c = 'xxxx';
        newAcctSelfUpdate.Authorisation_Rep_Position__c = 'yyyy';     
        update newAcctSelfUpdate;
      
      	PageReference pref = Page.TermsAcceptUpdate;
       	pref.getParameters().put('id',account.id);
        Test.setCurrentPage(pref);
      
        ApexPages.StandardController accL = new ApexPages.StandardController(account);
        TermsAcceptUpdateController lController = new TermsAcceptUpdateController(accL);
      
         Test.startTest();
         PageReference  ref1 = Page.TermsAcceptUpdate;
         
         ref1 = lController.Checkifvalid();      
         ref1 = lController.clickYes();
      
      
      
         test.stopTest();
  }

}