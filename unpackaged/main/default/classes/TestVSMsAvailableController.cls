/**
* @description       : Test class for VSMsAvailableController
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestVSMsAvailableController {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;  
    }
    
    @isTest
    private static void testVelocitySnapshotController() {
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1];
        Contract testContractObj = TestDataFactory.createTestContract('TestContract', testAccountObj.Id, 'Draft', '15', null, true, Date.today());
        INSERT testContractObj;
        testContractObj.Status = 'Activated';
        testContractObj.EndDate = Date.today().addDays(10);
        UPDATE testContractObj;
        
        Test.startTest();
        PageReference pageRef = Page.VSMsAvailable;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testAccountObj);
        VSMsAvailableController vsmsAvailableControllerObj  = new VSMsAvailableController(conL);
        PageReference initDiscPageReference = vsmsAvailableControllerObj.initDisc();
        Test.stopTest();
        //Asserts
        system.assertEquals(null, initDiscPageReference);
    }
    
    @isTest
    private static void testVelocitySnapshotControllerWithMessage() {
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1];
        Test.startTest();
        PageReference pageRef = Page.VSMsAvailable;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testAccountObj);
        VSMsAvailableController vsmsAvailableControllerObj  = new VSMsAvailableController(conL);
        vsmsAvailableControllerObj.controller = conL;
        Test.stopTest();
        //Asserts
        system.assertEquals('***   No active contracts are available.   ***', vsmsAvailableControllerObj.message);
    }
}