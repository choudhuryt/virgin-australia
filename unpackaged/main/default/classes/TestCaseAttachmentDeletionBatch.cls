/**
* @description       : Test class for CaseAttachmentDeletionBatch
* @createdBy         : Cloudwerx
* @Updated By        : Cloudwerx
**/
@isTest
private class TestCaseAttachmentDeletionBatch {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        testCaseObj.RecordTypeId = Utilities.getRecordTypeId('Case', 'Velocity Member Support');
        testCaseObj.Status = 'Closed';
        INSERT testCaseObj;
        EmailMessage testEmailMessageObj = TestDataFactory.createTestEmailMessage('Test Body', 'Test Subject', testCaseObj.Id);
        INSERT testEmailMessageObj;
        Attachment testAttachmentObj = TestDataFactory.createTestAttachment(testEmailMessageObj.Id, 'Test Attachment for EmailMessage', 'Test Attachment for EmailMessage');
        INSERT testAttachmentObj;
        ContentVersion testContentVersionObj = TestDataFactory.createTestContentVersion('Content Document', 'Content Document.txt', 'Content of the document for the Test Class for Batch Class', 'H');
        INSERT testContentVersionObj;
        ContentVersion contentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =:testContentVersionObj.Id LIMIT 1];
        ContentDocumentLink testContentDocumentLinkObj = TestDataFactory.createTestContentDocumentLink(testCaseObj.Id, contentVersion.ContentDocumentId, 'V');
        INSERT testContentDocumentLinkObj;
    }
    
    @isTest
    private static void testCaseAttachmentDeletionBatchWithValidRecords() {
        Test.startTest();
        CaseAttachmentDeletionBatch caseAttachmentDeletionBatchObj = new CaseAttachmentDeletionBatch();
        caseAttachmentDeletionBatchObj.labelDate = 0;
        DataBase.executeBatch(caseAttachmentDeletionBatchObj);
        Test.stopTest();
    }
    
    @isTest
    private static void testCaseAttachmentDeletionBatchWithNoRecordsDeletion() {
        Test.startTest();
        CaseAttachmentDeletionBatch caseAttachmentDeletionBatchObj = new CaseAttachmentDeletionBatch();
        DataBase.executeBatch(caseAttachmentDeletionBatchObj);
        Test.stopTest();       
    }
}