@isTest
global class PassengerManifestServiceMockImpl implements WebServiceMock {
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
		
        SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService 
            	service = new SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService();
    	SalesForceHistoricalDetailModel.GetPassengerManifestRSType response_x = service.GetPassengerManifestDemo();	
		response.put('response_x', response_x);
               
    }
}