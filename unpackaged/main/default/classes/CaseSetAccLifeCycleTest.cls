@isTest 
public class CaseSetAccLifeCycleTest {
    
    static testMethod void testMethod1(){ 
    Account acc = new account(name = 'test name', Account_Lifecycle__c = 'Opportunity');
    insert acc;
        
        case cs = new case(Subject = 'FOLLOWUP#[' + acc.ID + ']',
                           Description = 'Please be aware of my further',
                           ACC_Primary_Category__c = 'Accounts/NAR',
                           ACC_Secondary_Category__c = 'Chasing Further Info', 
                           Contact_Phone__c = '434645419',
                           RecordTypeId = Schema.sObjectType.Case.getRecordTypeInfosByName().get('Accelerate').getRecordTypeId(),
                           Priority = 'Normal', 
                           Status = 'New',   
                           Origin = 'E2C' 
                           );
        insert cs;
        
    }
    
    static testMethod void testMethod2() {
        
        Account acc = new account(name = 'test name' , Account_Lifecycle__c = 'Follow-up');
    	insert acc;
        
        contact con = new contact(lastname = 'test name' , accountid = acc.id, Key_Contact__c = true);

        case cs = new case(Subject = 'Re: Further Information Required ',
                           Description = 'Please be aware of my further Ref No : '+ acc.id,
                           ACC_Primary_Category__c = 'Accounts/NAR',
                           ACC_Secondary_Category__c = 'Chasing Further Info', 
                           Contact_Phone__c = '434645419',
                           RecordTypeId = Schema.sObjectType.Case.getRecordTypeInfosByName().get('Accelerate').getRecordTypeId(),
                           Priority = 'Normal', 
                           Status = 'New',   
                           Origin = 'E2C' 
                           );
        insert cs;
      
    }
}