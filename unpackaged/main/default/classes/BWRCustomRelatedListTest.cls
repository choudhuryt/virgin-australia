/**
 * @author: Joy S. Marasigan
 * @createdDate: April 18, 2016
 * @description: Test class for BWRCustomRelatedList.cls
 * Please see Case #00101013 for more information regarding the requirements
 **/

@isTest
public class BWRCustomRelatedListTest {

    //test records with a list of versions
    static testMethod void viewRecordWithVersions(){
        
        Blanket_Waiver_Request__c withVersions = new Blanket_Waiver_Request__c(
                                                        Waiver_Type__c = 'Airport related disrupts',
                                                        Waiver_Group__c = 'Commercial',
                                                        Business_Justification__c = 'New Record Test',
                                                        Blanket_Waiver_Start_Date__c = Date.parse('14/04/2016'),
                                                        Blanket_Waiver_End_Date__c = Date.parse('24/04/2016'),
                                                        Submission_Counter__c = 1,
                                                        Unique_Waiver_Code__c = 'BW0000001'
                                                    );
        Test.startTest();
        
        insert withVersions;
        
        Blanket_Waiver_Request__c versionParent = [SELECT Id, Unique_Waiver_Code__c FROM Blanket_Waiver_Request__c WHERE Unique_Waiver_Code__c = 'BW0000001'];
        
        List<Blanket_Waiver_Request__c> versionsList = new List<Blanket_Waiver_Request__c>{
                                                                                            new Blanket_Waiver_Request__c(
                                                                                                Waiver_Type__c = 'Airport related disrupts',
                                                                                                Waiver_Group__c = 'Commercial',
                                                                                                Business_Justification__c = 'New Record Test 1',
                                                                                                Blanket_Waiver_Start_Date__c = Date.parse('14/04/2016'),
                                                                                                Blanket_Waiver_End_Date__c = Date.parse('24/04/2016'),
                                                                                                Submission_Counter__c = 2,
                                                                                                Unique_Waiver_Code__c = versionParent.Unique_Waiver_Code__c,
                                                                                                Replicated_From__c = versionParent.Id
                                                                                            ),
                                                                                            new Blanket_Waiver_Request__c(
                                                                                                Waiver_Type__c = 'Airport related disrupts',
                                                                                                Waiver_Group__c = 'Commercial',
                                                                                                Business_Justification__c = 'New Record Test 2',
                                                                                                Blanket_Waiver_Start_Date__c = Date.parse('14/04/2016'),
                                                                                                Blanket_Waiver_End_Date__c = Date.parse('25/04/2016'),
                                                                                                Submission_Counter__c = 3,
                                                                                                Unique_Waiver_Code__c = versionParent.Unique_Waiver_Code__c,
                                                                                                Replicated_From__c = versionParent.Id
                                                                                            ),
                                                                                            new Blanket_Waiver_Request__c(
                                                                                                Waiver_Type__c = 'Airport related disrupts',
                                                                                                Waiver_Group__c = 'Commercial',
                                                                                                Business_Justification__c = 'New Record Test 3',
                                                                                                Blanket_Waiver_Start_Date__c = Date.parse('14/04/2016'),
                                                                                                Blanket_Waiver_End_Date__c = Date.parse('26/04/2016'),
                                                                                                Submission_Counter__c = 4,
                                                                                                Unique_Waiver_Code__c = versionParent.Unique_Waiver_Code__c,
                                                                                                Replicated_From__c = versionParent.Id
                                                                                            )
                                                                                        };
                                                                                        
        insert versionsList;
        
        Test.setCurrentPageReference(Page.BWRHistorySection);
        
        ApexPages.currentPage().getParameters().put('id',versionParent.Id);
        
        ApexPages.StandardController sCon = new ApexPages.StandardController(versionParent);
        BWRCustomRelatedList bwrHistoryExt = new BWRCustomRelatedList(sCon);
        
        System.assertEquals(bwrHistoryExt.bwrRelatedList.size(), 3);
        
        Test.stoptest();
        
    }
    
    //test records without a list of versions
    static testMethod void viewRecordWithoutVersions(){
        
        Blanket_Waiver_Request__c withoutVersions = new Blanket_Waiver_Request__c(
                                                        Waiver_Type__c = 'Airport related disrupts',
                                                        Waiver_Group__c = 'Commercial',
                                                        Business_Justification__c = 'New Record Test',
                                                        Blanket_Waiver_Start_Date__c = Date.parse('14/04/2016'),
                                                        Blanket_Waiver_End_Date__c = Date.parse('24/04/2016'),
                                                        Submission_Counter__c = 1,
                                                        Unique_Waiver_Code__c = 'BW0000002'
                                                    );
        
        Test.startTest();
        
        insert withoutVersions;
        
        Blanket_Waiver_Request__c noVersions = [SELECT Id FROM Blanket_Waiver_Request__c WHERE Unique_Waiver_Code__c = 'BW0000002'];
        
        Test.setCurrentPageReference(Page.BWRHistorySection);
        
        ApexPages.currentPage().getParameters().put('id',noVersions.Id);
        
        ApexPages.StandardController sCon = new ApexPages.StandardController(noVersions);
        BWRCustomRelatedList bwrHistoryExt = new BWRCustomRelatedList(sCon);
        
        System.assertEquals(bwrHistoryExt.bwrRelatedList.size(), 0);
        
        Test.stoptest();
        
        
    }

}