/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*
* Updated by cloudwerx : Removed hardcoded references from recordtypeId, OwnerId & Other required fields
*
*
*
*/
@isTest
private class TestAccelerateAccountsBatch {
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Profile profileRec = [Select Id, Name From Profile Where Name = 'Contract Implementation Support']; 
        User userRec = [Select Id, Name From User Where profileId =: profileRec.Id And isActive = True Limit 1];
        
        Id accRecTypeId = Utilities.getRecordTypeId('Account', 'Accelerate');            
        Id contractRecTypeId = Utilities.getRecordTypeId('Contract', 'Accelerate POS/Rebate');            
        
        Account account = new Account();
        CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
        account = commonObjectsForTest.CreateAccountObject(0);
        account.Velocity_Gold_Status_Match_Available__c =10;
        account.Velocity_Pilot_Gold_Available__c = 10;
        account.Velocity_Platinum_Status_Match_Available__c =10;
        account.Accelerate_Batch_Update__c = false;
        //account.RecordTypeId ='01290000000tSR0';
        account.RecordTypeId = accRecTypeId;
        account.Sales_Matrix_Owner__c = 'Accelerate';
        //account.OwnerId = '00590000000LbNz';
        account.OwnerId = userRec.Id;
        //account.Account_Owner__c = '00590000000LbNz';
        account.Account_Owner__c = userRec.Id;
        account.Velocity_Pilot_Gold_Available__c = 2;
        //account.Sales_Support_Group__c = '00590000000LbNz'; 		
        account.Sales_Support_Group__c = userRec.Id;
        //account.RecordTypeId ='01290000000tSR0';	
        account.Billing_Country_Code__c ='AU';
        account.Market_Segment__c = 'Accelerate';	
        account.Lounge__c =true;
        account.Business_Number__c =  '49113973087' ; 
        account.Business_Activity__c = 'TEST';
        //account.ownerId = '00590000000LbNz';
        account.ownerId = System.Label.Virgin_Australia_Business_Flyer_User_Id;
        
        // deprecated *************************************
        account.Pilot_Gold_Email__c ='test@test.com';
        account.Pilot_Gold_Email_Second_Nominee__c = 'test@test.com';
        // deprecated *************************************
        
        account.Lounge_Email__c = 'test123456789@test.com';
        insert account;   
        
        Subscriptions__c newSubs = new Subscriptions__c(Name = 'test Sub');
        insert newSubs;
        
        Contact newContact = new Contact(); 
        newContact.FirstName ='Andy';
        newContact.LastName ='C';
        newContact.Phone = '0448946733' ;
        newContact.Title = 'Manager' ;  
        newContact.AccountId = account.id;
        newContact.Key_Contact__c = True;
        newContact.Subscriptions__c = newSubs.Id;        
        newContact.Email ='Andy.B@TEST456.com.au' ;
        Date dateobj = Date.today();
        //Added for failing test classes start 28/11/2019
        newContact.Velocity_Number__c = '1013143410';
        //Added for failing test classes stop 28/11/2019
        
        insert newContact;
        
        Contract contract = New Contract();
        contract.name ='PUTCONTRACTNAMEHERE';
        contract.AccountId =account.id;
        contract.Status ='Draft';
        //contract.RecordTypeId ='012900000007oTOAAY';
        contract.RecordTypeId = contractRecTypeId;
        
        // ** VSMs are now only stored on contracts **
        contract.Gold__c = 10;
        
        // Set dates
        contract.startDate = Date.today();
        contract.endDate = Date.today().addDays(365);
        
        insert contract;
        
        // Activate Contract
        contract.Status ='Activated';
        update contract;
        
        
        Velocity_Status_Match__c vsm = new Velocity_Status_Match__c();
        vsm.Account__c =account.id;
        vsm.RecordTypeId = Utilities.getRecordTypeId('Velocity_Status_Match__c', 'Velocity_Upgrade');
        vsm.Within_Contract__c = 'Yes';
        vsm.Contract__c = contract.Id;
        vsm.Approval_Status__c = 'Draft';
        vsm.Approval_Status__c ='Draft';
        vsm.Passenger_Email__c ='Test@test.com';
        vsm.Passenger_Name__c = 'Test';
        vsm.Passenger_Velocity_Number__c ='1234567890';
        vsm.Position_in_Company__c = 'Test';
        vsm.Delivery_To__c ='Member';
        vsm.Date_Requested__c = dateobj.addDays(1);
        vsm.Status_Match_or_Upgrade__c ='Gold';
        //Added for failing test classes start 28/11/2019
        vsm.Contact__c = newContact.id;
        
        //Added for failing test classes stop 28/11/2019
        
        insert vsm; 
        
        Test.startTest();
        AccelerateAccountBatch testBatch = new AccelerateAccountBatch();
        Database.executeBatch(testBatch);
        
        Test.StopTest();
    }
}