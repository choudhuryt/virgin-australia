/**
* This class is controller for Velocity Profile Details VF page
* @author Son Pham
*/
public class VelocityProfileDetailsController {
    public Velocity__c velocity {get;set;}
    
    /**
	* Default constructor
	*/
    public VelocityProfileDetailsController() {
        String velocityId = ApexPages.currentPage().getParameters().get('Id');
        this.velocity = queryVelocity(velocityId);
    }
    
    /**
	* Gets velocity by velocity id
	* @param velocityId id of velocity
	* @return a velocity record if any, new velocity record otherwise
	*/
    public Velocity__c queryVelocity(String velocityId) {
        List<Velocity__c> velocityList = new List<Velocity__c>();
        velocityList = [SELECT Id, Velocity_Number__c, First_Name__c, Surname__c, Tier__c, Phone_Number__c, Email__c
                                          FROM Velocity__c
                                          WHERE Id=:velocityId];
        if (!velocityList.isEmpty()) {
            return velocityList.get(0);
        }
        return new Velocity__c();
    }
}