@isTest
private class SelfupdateTest {
    
    @isTest static void clickYesTest()
    {
        String RECORDTYPE_ACCELERATE = 'Accelerate';
        
        Id accelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_ACCELERATE ).getRecordTypeId(); 
       
        Account acct = new Account(Name = 'Virgin Test',
           						   Business_Number__c = '11122233344',
           						   Administrator_Contact_Email__c = 'TestAdmin@testsalesforce1.com',
           						   RecordTypeId = accelerateRecTypeId,
        //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                   BillingStreet = 'Bliss',
                                   BillingCity = 'Malolos',
                                   BillingState = 'QLD',
                                   BillingPostalCode = '4006',
                                   BillingCountry = 'AU',
            					   Industry_Type__c = 'Engineering',
            					   Account_Lifecycle__c = 'Offer',
        //MOD-END CILAG INS CASE# 00129600 09.23.16
           						   Administrator_Contact_Last_Name__c = 'Test last');

        insert acct;

        //Select Id, Name, AccountId, Status__c, Key_Contact__c from Contact

        Contact con = new Contact(LastName ='Test Contact',
        						  AccountId = acct.Id,
        						  Status__c = 'Active',
                                  Title = 'Mr',
                                  Phone = '0345454546',
                                  MobilePhone='0454545454', 
                                  Email='test1@gmail.com', 
        						  Key_Contact__c = true);

        insert con;


        Mailer_Expiry__c mExpiry = new Mailer_Expiry__c(Expiry_Date__c = Date.today().addDays(1));
        insert mExpiry;


		PageReference pageRef = Page.SelfUpdate;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('id',acct.Id);		
		ApexPages.currentPage().getParameters().put('eId',mExpiry.Id);
        ApexPages.standardController controller = new ApexPages.standardController(acct);
		SelfUpdateController selfUpdate = new SelfUpdateController(controller);	       
		PageReference pageRefActual = selfUpdate.clickYes();
        List<Account_Self_Update__c> acctSelfUpdateList = [Select Id, Account__c FROM Account_Self_Update__c WHERE Account__c =: acct.Id];
        PageReference pageRefExpected = Page.SelfUpdateConfirmation ;
		System.assertEquals(pageRefExpected.getURL(), pageRefActual.getURL());
		System.assert(!acctSelfUpdateList.isEmpty());
		//Test.stopTest();
    }
    
    @isTest static void clickNoTest()
    {
        String RECORDTYPE_ACCELERATE = 'Accelerate';
        
        Id accelerateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get( RECORDTYPE_ACCELERATE ).getRecordTypeId(); 
       
        Account acct = new Account(Name = 'Virgin No Test',
           						   Business_Number__c = '11122233345',
           						   Administrator_Contact_Email__c = 'TestAdmin@testsalesforce1.com',
           						   RecordTypeId = accelerateRecTypeId,
        //MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
                                   BillingStreet = 'Bliss',
                                   BillingCity = 'Malolos',
                                   BillingState = 'QLD',
                                   BillingPostalCode = '4006',
                                   BillingCountry = 'AU',
            					   Industry_Type__c = 'Engineering',
            					   Account_Lifecycle__c = 'Offer',
        //MOD-END CILAG INS CASE# 00129600 09.23.16
           						   Administrator_Contact_Last_Name__c = 'Test last');

        insert acct;

        //Select Id, Name, AccountId, Status__c, Key_Contact__c from Contact

        Contact con = new Contact(LastName ='Test Contact',
        						  AccountId = acct.Id,
        						  Status__c = 'Active',
                                  Title = 'Mr',
                                  Phone = '0345454546',
                                  MobilePhone='0454545454', 
                                  Email='test1no@gmail.com', 
        						  Key_Contact__c = true);

        insert con;


        Mailer_Expiry__c mExpiry = new Mailer_Expiry__c(Expiry_Date__c = Date.today().addDays(1));
        insert mExpiry;


		PageReference pageRef = Page.SelfUpdate;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('id',acct.Id);		
		ApexPages.currentPage().getParameters().put('eId',mExpiry.Id);
        ApexPages.standardController controller = new ApexPages.standardController(acct);
		SelfUpdateController selfUpdate = new SelfUpdateController(controller);
        
        PageReference pageRefActual = selfUpdate.clickNo();

		PageReference pageRefExpected = Page.SelfUpdateEdit;
		pageRefExpected.getParameters().put('id',acct.Id);		
		pageRefExpected.getParameters().put('eId',mExpiry.Id);
        System.assertEquals(pageRefExpected.getURL().substring(0,20), pageRefActual.getURL().substring(0,20));
		
    } 

}