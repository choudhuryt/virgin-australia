/**
 * @description       : Test class for ContractSpendCalculation
 * @Updated By          : Cloudwerx
**/
@isTest
public class ContractSpendCalculationTest {
    
    @testSetup static void setup() {
        Account testAccObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        testAccObj.recordTypeId = new List<Id>(ContractSpendCalculation.accountRecordTypeIds)[0];
        testAccObj.Sales_Matrix_Owner__c = 'Accelerate';
        INSERT testAccObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj.RecordTypeId = new List<Id>(ContractSpendCalculation.contractRecordTypeIds)[0];
        INSERT testContractObj;
        testContractObj.status = 'Activated';
        UPDATE testContractObj;
        
        List<Spend__c> testSpends = new List<Spend__c>();
        testSpends.add(TestDataFactory.createTestSpend(testAccObj.Id, testContractObj.Id, 200000, 250000, 'D', 8, 2019));     
        testSpends.add(TestDataFactory.createTestSpend(testAccObj.Id, testContractObj.Id, 20000, 25000, 'D', 10, 2019)); 
        INSERT testSpends;
    }
    
    @isTest
    private static void testContractSpendCalculationWithSmartflyBenefits() {
        Contract contract = [SELECT id ,Total_Eligible_Spend_for_PG_Calc__c ,Total_Eligible_Spend_used_for_PG_Calc__c
                             FROM Contract LIMIT 1];
        Smartfly_Benefit__c smartBenefirObject = TestDataFactory.createTestSmartflyBenefit(contract.Id, 10, 20, 30);
        INSERT smartBenefirObject;
        Test.StartTest();	

        ContractSpendCalculation newContractSpend = new ContractSpendCalculation();
        Database.BatchableContext bc;
        newContractSpend.start(bc);
        List<Contract> lstcon =new List<Contract>();
        lstcon.add(contract);
        newContractSpend.execute(bc,lstcon);
        newContractSpend.finish(bc);
        Test.StopTest(); 
        // Asserts
        System.AssertEquals(1, [SELECT COUNT() FROM Contract where recordtypeid IN :ContractSpendCalculation.contractRecordTypeIds]);
    }
    
    @isTest
    private static void testContractSpendCalculationWithNoSmartflyBenefits() {
        Contract contract = [SELECT id ,Total_Eligible_Spend_for_PG_Calc__c ,Total_Eligible_Spend_used_for_PG_Calc__c
                             FROM Contract LIMIT 1];
        Test.StartTest();	

        ContractSpendCalculation newContractSpend = new ContractSpendCalculation();
        Database.BatchableContext bc;
        newContractSpend.start(bc);
        List<Contract> lstcon =new List<Contract>();
        lstcon.add(contract);
        newContractSpend.execute(bc,lstcon);
        newContractSpend.finish(bc);
        newContractSpend.processSpendData(new Contract(), new List<String>());
        Test.StopTest(); 
        // Asserts
        System.AssertEquals(1, [SELECT COUNT() FROM Contract where recordtypeid IN :ContractSpendCalculation.contractRecordTypeIds]);
    }
}