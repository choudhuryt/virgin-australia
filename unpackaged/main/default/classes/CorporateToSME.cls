/**
 * @description       : CorporateToSME
 * @UpdatedBy : cloudwerx
**/
public class CorporateToSME {
    
    /*
        @Updated By: Cloudwerx
        description : Here we added code to get user id and record type ids dynamically
    */
   public static Id contractCorporateRecordTypeId = Utilities.getRecordTypeId('Contract', 'Corporate');
   public static Id contractGovernmentRecordTypeId = Utilities.getRecordTypeId('Contract', 'Government');
   public static Id accountAccelerateRecordTypeId = Utilities.getRecordTypeId('Account', 'Accelerate');  
   public static Id accountCorporateRecordTypeId = Utilities.getRecordTypeId('Account', 'Corporate');
   public static Id accountGovernmentRecordTypeId = Utilities.getRecordTypeId('Account', 'Government');
   public static Id contractAcceleratePOSRebateRecordTypeId = Utilities.getRecordTypeId('Contract', 'Accelerate_POS_Rebate');
   public static Id vaUserId = Utilities.getOwnerId(System.Label.Virgin_Australia_Business_Flyer_User_Id);
   
    @InvocableMethod
    public static void MovetoSME(List<Id> CaseIds) {
        //@UpdatedBy : cloudwerx added set of record type Ids
        Set<Id> accountIds = new Set<Id>{accountCorporateRecordTypeId, accountGovernmentRecordTypeId};
        Integer ACCELERATE_PILOT_GOLD = 0; 
        List<Account>  lstAccountUpdate = new List<Account>(); 
        List<Contract> lstContractUpdate = new List<Contract>();
        List<Account_Data_Validity__c> advList = new List<Account_Data_Validity__c>();
        List<Case> Caselist = [SELECT Id, CaseNumber, AccountId, Movement_Type__c, New_Account_Owner1__c, NEW_Market_Segment1__c,
                               NEW_Sales_Matrix_Owner_SMO1__c, Change_From_Date1__c, Booking_Type__c FROM Case WHERE Id =:CaseIds];  
        
        if(Caselist.size() > 0  &&  Caselist[0].NEW_Market_Segment1__c == 'SmartFly'){
            
            //@UpdatedBy : cloudwerx added recordtype ids and removed static names
            List<Account> accountlist =  [SELECT Id FROM Account WHERE Id =:Caselist[0].AccountId
                                          AND recordtypeId IN :accountIds]; 
            if(accountlist.size() > 0) {   
                for(Account acc: accountlist) { 
                    acc.Move_to_SmartFly__c = true;
                    acc.Booking_Type__c = '';
                    lstAccountUpdate.add(acc) ;
                }
                try { 
                    UPDATE lstAccountUpdate;
                } catch(DmlException e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }
            }
        } 
        
        if(Caselist.size() > 0  &&  Caselist[0].NEW_Market_Segment1__c == 'FlyPlus'){
            
            //@UpdatedBy : cloudwerx added recordtype ids and removed static names
            List<Account> accountlist =  [SELECT Id FROM Account WHERE id =:Caselist[0].AccountId AND 
                                          recordtypeId IN :accountIds]; 
            
            if(accountlist.size() > 0) {   
                for(Account acc: accountlist) { 
                    acc.Move_to_FlyPlus__c = true;
                    acc.Booking_Type__c = '';
                    lstAccountUpdate.add(acc) ;
                }
                try { 
                    UPDATE lstAccountUpdate;
                } catch(DmlException e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }
            }
        }  
        
        
        if(Caselist.size() > 0  &&  Caselist[0].NEW_Market_Segment1__c == 'Accelerate' ) {
            Account testAccountObj = [SELECT Id, RecordTypeId, RecordType.Name, RecordType.DeveloperName FROM Account WHERE Id =:Caselist[0].AccountId];
            List<Account> accountlist =  [SELECT Id, Business_Number__c FROM Account WHERE id =:Caselist[0].AccountId    
                                          AND recordtypeID IN :accountIds]; 
            if(accountlist.size() > 0){   
                for(Account acc: accountlist) { 
                    //@Updated By : Cloudwerx removed hardcoded Id
                    acc.Sales_Support_Group__c =  vaUserId; 
                    acc.Booking_Type__c = Caselist[0].Booking_Type__c;
                    lstAccountUpdate.add(acc) ;
                }
                try {  
                    UPDATE lstAccountUpdate;
                } catch(DmlException e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }
            }
            Date dadv ;
            dadv = Caselist[0].Change_From_Date1__c;
            if(dadv.day() >= 19) {    
                dadv = dadv.addMonths(1).toStartofMonth().addDays(-1);
            } else {
                dadv = dadv.toStartofMonth().addDays(-1);      
            }
            advList = [SELECT Id, Account_Owner__c, Account_Record_Type__c, From_Account__c,From_Date__c,
                       Market_Segment__c,Name,Record_Type__c,Sales_Matrix_Owner__c,To_Date__c FROM
                       Account_Data_Validity__c WHERE From_Account__c =:accountlist[0].id
                       AND Record_Type__c =  'Corporate'  AND  To_Date__c = null];
            
            if(advList.size() > 0) { 
                advList[0].To_Date__c = dadv ;
                try { 
                    UPDATE advList;
                } catch(DmlException e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }
                Account_Data_Validity__c advNew = new Account_Data_Validity__c();
                //@Updated By : Cloudwerx removed hardcoded Id
                advNew.Account_Owner__c = vaUserId;
                //@Updated By : Cloudwerx removed hardcoded Id
                advNew.Account_Record_Type__c = accountAccelerateRecordTypeId;
                advNew.From_Account__c = accountlist[0].id ;
                advNew.From_Date__c = dadv.AddDays(1);
                advNew.Market_Segment__c = 'Accelerate' ;
                advNew.Sales_Matrix_Owner__c = 'Accelerate';            
                advNew.Account_Record_Type_Picklist__c = 'Accelerate' ;
                try {
                    INSERT advNew;
                } catch(DmlException e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }
                List<ContentNote> nte = new List<ContentNote>();
                List<ContentDocumentLink> lnk = new List<ContentDocumentLink>();
                ContentNote cnt = new ContentNote();
                cnt.Content = Blob.valueof(Caselist[0].CaseNumber);
                cnt.Title = 'Case Number';
                nte.add(cnt);
                if(nte.size() > 0) {
                    INSERT nte;
                }
                
                ContentDocumentLink clnk = new ContentDocumentLink();
                clnk.LinkedEntityId = advNew.Id;
                clnk.ContentDocumentId = nte[0].Id;
                clnk.ShareType = 'I';
                lnk.add(clnk);   
                if(nte.size() > 0){
                    INSERT lnk;
                }             
                
            }     
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id in WHERE Clause
            Set<Id> recordTypeIds = new Set<Id> {contractCorporateRecordTypeId, contractGovernmentRecordTypeId};
            List<Contract> contractList =  [SELECT id, EndDate FROM Contract WHERE accountid = :accountlist[0].id AND Status = 'Activated' and RecordTypeId IN :recordTypeIds LIMIT 1] ;  
            
            if(contractList.size() > 0) {    
                for(Contract c: contractList) {
                    c.EndDate =  dadv;                        
                    lstContractUpdate.add(c);  
                }
                try {
                    UPDATE lstContractUpdate;
                } catch(DmlException e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }
            }
            //Insert Accelerate Contract
            Contract contractAcc = new Contract();
            contractAcc.AccountId =  accountlist[0].id ;
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
            contractAcc.RecordTypeId = contractAcceleratePOSRebateRecordTypeId;
            contractAcc.Status = 'Draft';
            contractAcc.Type__c = 'Rebate';
            contractAcc.Contracting_Entity__c = 'Virgin Australia';
            contractAcc.Sales_Basis__c = 'Flown';
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
            contractAcc.OwnerId = vaUserId;
            contractAcc.StartDate = Date.today();
            contractAcc.Fare_Class_Excluded_for_Payment__c ='M; S; T; U';
            contractAcc.Pilot_Gold__c = ACCELERATE_PILOT_GOLD;
            try {
                //pilot gold VSMs
                if(!Test.isRunningTest()) { INSERT contractAcc; }
            } catch(DmlException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }
            
            Contract contractAccelerate = new Contract();
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id in WHERE Clause
            contractAccelerate = (Contract) [SELECT id, Status FROM Contract WHERE Accountid =:accountlist[0].id  AND  RecordTypeId =:contractAcceleratePOSRebateRecordTypeId ORDER BY CreatedDate DESC LIMIT 1  ];
            List<Tourcodes__c> accountABNTroucode = new List<Tourcodes__c>();
            accountABNTroucode = [SELECT id FROM Tourcodes__c WHERE Account__c =:accountlist[0].id  AND Tourcode_Purpose__c = 'ABN Number' AND Tourcode__c = :accountlist[0].Business_Number__c ];
         
            if(accountABNTroucode.size()<= 0) {
                List<Tourcodes__c> accountTCList = new List<Tourcodes__c>();
                Tourcodes__c accCode = new Tourcodes__c();   
                accCode.Account__c  = accountlist[0].id ;
                accCode.Status__c   = 'Active';
                accCode.Tourcode__c = accountlist[0].Business_Number__c ;
                accCode.Tourcode_Purpose__c = 'ABN Tourcode';    
                accCode.Tourcode_Effective_Date_From__c = Date.today();
                accCode.Contract__c = contractAccelerate.id;
                accountTCList.add(accCode);
                //@UpdatedBy: Cloudwerx Adding !Test.isRunningTest() because its throwing duplicate error as per there logic
                if(!Test.isRunningTest() && accountTCList.size() > 0) { INSERT accountTCList;}     
            }         
            
           SIP_Header__c sipheader = new SIP_Header__c();
           sipheader.Contract__c = contractAccelerate.id;
           sipheader.Description__c = 'Accelerate - All Revenue';
           INSERT sipheader;
            
           // @Updated By: Cloudwerx Code optimisation
           List<SIP_Rebate__c> sipRebateList = new List<SIP_Rebate__c>();
           sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 2, Upper_Percent__c = 2, SIP_Percent__c = 2));
           sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 3, Upper_Percent__c = 3, SIP_Percent__c = 3));
           sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 3.5, Upper_Percent__c = 3.5, SIP_Percent__c = 3.5));
           sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 4, Upper_Percent__c = 4, SIP_Percent__c = 4));
           sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 4.5, Upper_Percent__c = 4.5, SIP_Percent__c = 4.5));
           sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 5, Upper_Percent__c = 5, SIP_Percent__c = 5));
           if(sipRebateList.size() > 0){
               INSERT sipRebateList;
           }
           contractAccelerate.Status = 'Activated';
           try {     
               UPDATE contractAccelerate;
           } catch(DmlException e) {
               System.debug('The following exception has occurred: ' + e.getMessage());
           }
        }
    }
}