/**
 * @description       : Test class for NewRevisonOppController
 * @UpdatedBy         : Cloudwerx
**/
@isTest
private class TestNewRevisionOppController {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;  
        
        Opportunity testOpportunityObj = TestDataFactory.createTestOpportunity('testOpp1', 'Sales Opportunity Analysis', testAccountObj.Id, Date.today(), 1.00);
        INSERT testOpportunityObj; 
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', testOpportunityObj.Id, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
    }
    
    @isTest
    private static void testNewRevisonOppControllerNotActivated()  {
        Contract testContractObj = [SELECT Id FROM Contract LIMIT 1];
        Test.startTest();
        PageReference pageRef = Page.NewRevisionOpportunity; 
        pageRef.getParameters().put('contractId', testContractObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        NewRevisonOppController lController  = new NewRevisonOppController(conL);
        lController.actionButtonYes();
        lController.actionButtonNo();
        Test.stopTest();
        //Asserts
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
    }
    
    @isTest
    private static void testNewRevisonOppControllerActivated()  {
        Contract testContractObj = [SELECT Id, status, Revision_Details__c, AccountId FROM Contract LIMIT 1];
        testContractObj.status = 'Activated';
        testContractObj.Revision_Details__c = 'Dummy Revision test 12349123';
        UPDATE testContractObj;
        Test.startTest();
        PageReference pageRef = Page.NewRevisionOpportunity; 
        pageRef.getParameters().put('contractId', testContractObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        NewRevisonOppController lController  = new NewRevisonOppController(conL);
        lController.actionButtonYes();
        Test.stopTest();
        //Asserts
        Opportunity opp = [SELECT Id, AccountId, StageName, Type, RecordTypeId
                           FROM Opportunity WHERE Existing_SF_Contract__c =:testContractObj.Id];
        System.assertEquals(true, opp != null);
        System.assertEquals(Utilities.getRecordTypeId('Opportunity', 'VA_master'), opp.RecordTypeId);
        System.assertEquals('Awaiting BDM Allocation', opp.StageName);
        System.assertEquals('Revision', opp.Type);
        System.assertEquals(testContractObj.AccountId, opp.AccountId);
    }
    
    @isTest
    private static void testNewRevisonOppControllerCatchError()  {
        Contract testContractObj = [SELECT Id, status, Revision_Details__c FROM Contract LIMIT 1];
        testContractObj.status = 'Activated';
        UPDATE testContractObj;
        Test.startTest();
        PageReference pageRef = Page.NewRevisionOpportunity; 
        pageRef.getParameters().put('contractId', testContractObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        NewRevisonOppController lController  = new NewRevisonOppController(conL);
        lController.revisondetails = 'revisondetails';
        lController.controller = new ApexPages.StandardController(testContractObj);
        lController.actionButtonYes();
        Test.stopTest();
        //Asserts
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
    }
}