public class triggerupdatehelper {
    
    // Static variables are local to the context of a Web request  
    // (or testMethod during a runTests call)  
    // Therefore, this variable will be initialized as false  
    // at the beginning of each Web request which accesses it.  

    private static boolean alreadyupdated = false;


    public static boolean hasAlreadyupdatedtrigger() {
        return alreadyupdated;
    }

    // By setting the variable to true, it maintains this  
    // new value throughout the duration of the request  
    // (or testMethod)  
    
    public static void settriggerupdated() {
        alreadyupdated = true;
    }

  

}