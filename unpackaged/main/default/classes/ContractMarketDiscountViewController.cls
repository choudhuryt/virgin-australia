public with sharing class ContractMarketDiscountViewController {
    
public String CurrentContractId{set;get;}  
public String message {get;set;}
public List<Market__c> marketdata {get;set;} 
public Additional_Benefits__c addben {get;set;} 
public String conname {get;set;}     
public List <Contract> con {get;set;}
public List <Contract> contracts {get;set;}
public ContractMarketDiscountViewController() 
    {
        if(CurrentContractId == null)
        {
            CurrentContractId= ApexPages.currentPage().getParameters().get('id');
        }
    }
    
       public PageReference initDisc() 
     {  
         
       if(CurrentContractId == null)
        {
            CurrentContractId= ApexPages.currentPage().getParameters().get('id');
        }      
        
         
         con =         [Select Name ,Contractnumber from Contract where
                          id = :ApexPages.currentPage().getParameters().get('id')
                         ];
         contracts =  [Select JAPANS__c,LondonJapan__c,SQ__c from Contract where
                          id = :ApexPages.currentPage().getParameters().get('id')
                         ];
         if(con.size() > 0)
         {
           conname = con[0].name + ' : '  + con[0].contractnumber;  
         }         
         
           marketdata = [
                           SELECT Carrier__c,Comments__c,Contract__c,Guideline_Tier__c,
                           Id,Name,Requested_Tier__c,Revenue_M_S__c,VA_Revenue_Commitment__c,
                           VA_Serviceable_Routes_p_a__c,Market__c
                           FROM Market__c
                           where Contract__c =:ApexPages.currentPage().getParameters().get('id')
                           and Name not in ('China' , 'HK Hong Kong') 
                      ];
         
         
          List<Additional_Benefits__c> addbendetails = [
                           SELECT Brand_Connections_Payment_Frequency__c,Brand_Connections_Payment_Type__c,
                           Brand_Connections__c,Contract__c,Gold__c,Id,Joining_Fee__c,
                           Lounge_joining_fee_waived_for_60_days__c,Lounge_Membership_Tier__c,Lounge_Membership__c,
                           Membership_Fee__c,Name,Opportunity__c,Pilot_GD_Campaign_Offered__c,
                           Pilot_GD_Campaign_Term__c,Pilot_Gold__c,Virgin_Australia_Beyond__c, Platinum__c,Silver__c,Status_Match_Offered__c,
                           Status_Match_Term__c,Ticket_Fund_Payment_Frequency__c,
                           Ticket_Fund_Payment_Type__c,Ticket_Fund__c,Velocity_Accelerations_Offered__c,
                           Velocity_Acceleration_Frequency__c,Ticket_Fund_Perf_Based__c,
                           Ticket_Fund_Perf_Based_Frequecy__c,Ticket_Fund_Sign_On_Bonus__c,
                           Ticket_Fund_Sign_On_Bonus_Frequency__c,Additional_Notes__c,Red_Circle_Notes__c  FROM Additional_Benefits__c
                           where Contract__c =:ApexPages.currentPage().getParameters().get('id')
                      ];
      
          
         
          if(addbendetails.size() == 0)
        {
            message = '***   No details available.   ***';
        }else{
            addben = addbendetails.get(0);
        }

       
       return null;
     }

  public PageReference DOM() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.ContractWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentContractId);  
        newPage.getParameters().put('Region', 'DOMREG'); 
        return newPage ;
    } 
    
   public PageReference INSH() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.ContractWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentContractId);  
        newPage.getParameters().put('Region', 'INTSH'); 
        return newPage ;
    } 
    
     public PageReference NA() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.ContractWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentContractId);  
        newPage.getParameters().put('Region', 'NA'); 
        return newPage ;
    } 
         public PageReference LON() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.ContractWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentContractId);  
        newPage.getParameters().put('Region', 'LONDON'); 
        return newPage ;
    } 
      public PageReference EY() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.ContractWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentContractId);  
        newPage.getParameters().put('Region', 'EY'); 
        return newPage ;
    } 
      public PageReference SQ() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.ContractWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentContractId);  
        newPage.getParameters().put('Region', 'SQ'); 
        return newPage ;
    } 

    public PageReference RED() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.ContractWithDiscountData ;
        newPage.getParameters().put('Cid', CurrentContractId);  
        newPage.getParameters().put('Region', 'RED'); 
        return newPage ;
    } 
    
    public PageReference cancel() {
   // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
      PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
                  //
                  thePage.setRedirect(true);
                  //
                  return thePage;
    }
    
}