public class UpdateOwnerManager
{
    @InvocableMethod 
    public static void UpdateL3OwnerManager(List<Id> UserIds  )   
    {
        List<Opportunity> lstOppUpdate = new List<Opportunity>(); 
        List<Contract_Extension__c> lstCEUpdate = new List<Contract_Extension__c>(); 
        List<Contract_Termination__c> lstCTUpdate = new List<Contract_Termination__c>();    
        List<Global_Sales_Activity__c> lstGSAUpdate = new List<Global_Sales_Activity__c>();
        
        
        List<User> userlist =  [SELECT contract_approver__c,Id , contract_approver__r.Level__c ,
                                contract_approver__r.contract_approver__c ,
                                contract_approver__r.contract_approver__r.Level__c 
                                FROM User where id =:UserIds 
                               ]; 
        
        List<Opportunity> lstOpp = [ SELECT Id,Level3_Manager__c,Opportunity_Owner_Manager__c FROM Opportunity WHERE OwnerId =:UserIds];
        List<Contract_Extension__c>  lstCE = [SELECT Id,Level3_Manager__c FROM Contract_Extension__c WHERE OwnerID__c = :String.valueOf(UserIds).substring(1, 16)]   ;
        List<Contract_Termination__c> lstCT  = [SELECT Id,Level3_Manager__c FROM Contract_Termination__c WHERE OwnerID__c =:String.valueOf(UserIds).substring(1, 16)]   ;
        List<Global_Sales_Activity__c> lstGSA = [SELECT Associated_Level3__c,Id FROM Global_Sales_Activity__c WHERE OwnerId =:UserIds] ;                
        
        if( userlist.size() > 0  && userlist[0].contract_approver__r.Level__c == 3)   
        {            
            if (lstOpp.size() > 0 )
            {
                for(Opportunity o: lstOpp)
                {
                    o.Opportunity_Owner_Manager__c = userlist[0].contract_approver__c;
                    o.Level3_Manager__c =   userlist[0].contract_approver__c;
                    lstOppUpdate.add(o);      
                }  
                
            }
            
            if (lstCE.size() > 0 )
            {
                for(Contract_Extension__c ce: lstCE)
                {
                    ce.Level3_Manager__c = userlist[0].contract_approver__c;         
                    lstCEUpdate.add(ce);      
                }  
                
            }
            
            if (lstCT.size() > 0 )
            {
                for(Contract_Termination__c ct: lstCT)
                {
                    ct.Level3_Manager__c = userlist[0].contract_approver__c;         
                    lstCTUpdate.add(ct);      
                }  
                
            } 
            
            if (lstGSA.size() > 0 )
            {
                for(Global_Sales_Activity__c gsa: lstGSA)
                {
                    gsa.Associated_Level3__c = userlist[0].contract_approver__c;         
                    lstGSAUpdate.add(gsa);      
                }  
                
            }   
            
            update lstOppUpdate; 
            update lstCTUpdate; 
            update lstCEUpdate; 
            update lstGSAUpdate; 
        }
        
        if( userlist.size() > 0  && userlist[0].contract_approver__r.level__c == 2 && userlist[0].contract_approver__r.contract_approver__r.Level__c == 3)   
        {                        
            
            if (lstOpp.size() > 0 )
            {
                for(Opportunity o: lstOpp)
                {
                    o.Opportunity_Owner_Manager__c = userlist[0].contract_approver__c;
                    o.Level3_Manager__c =   userlist[0].contract_approver__r.contract_approver__c ;
                    lstOppUpdate.add(o);      
                }  
                
            }
            
            if (lstCE.size() > 0 )
            {
                for(Contract_Extension__c ce: lstCE)
                {
                    ce.Level3_Manager__c = userlist[0].contract_approver__r.contract_approver__c ;         
                    lstCEUpdate.add(ce);      
                }  
                
            }
            
            if (lstCT.size() > 0 )
            {
                for(Contract_Termination__c ct: lstCT)
                {
                    ct.Level3_Manager__c = userlist[0].contract_approver__r.contract_approver__c ;         
                    lstCTUpdate.add(ct);      
                }  
                
            } 
            
            if (lstGSA.size() > 0 )
            {
                for(Global_Sales_Activity__c gsa: lstGSA)
                {
                    gsa.Associated_Level3__c = userlist[0].contract_approver__r.contract_approver__c ;         
                    lstGSAUpdate.add(gsa);      
                }  
                
            }   
            
            update lstOppUpdate; 
            update lstCTUpdate; 
            update lstCEUpdate; 
            update lstGSAUpdate; 
        }        
    }
}