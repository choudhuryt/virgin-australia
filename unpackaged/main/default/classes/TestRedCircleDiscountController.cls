/**
* @description       : Test class for RedCircleDiscountController
* @Updated By         : Cloudwerx
**/
@isTest
private class TestRedCircleDiscountController {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        
        Contract_Addendum__c testContractAddendumObj = TestDataFactory.createTestContractAddendum(testContractObj.Id, '15', 'Tier 1', 'Tier 1', true, 'SYDMEL', 'PERKTH', 'Draft');
        INSERT testContractAddendumObj;
        
        Opportunity testOpportunityObj = TestDataFactory.createTestOpportunity('testOpp1', 'Sales Opportunity Analysis', testAccountObj.Id, Date.today(), 1.00);
        INSERT testOpportunityObj; 
        
        List<Proposal_Table__c> testProposalTables = new List<Proposal_Table__c>();
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'DOM Mainline'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'DOM Regional'));
        INSERT testProposalTables;
        
        List<Proposed_Red_Tables__c> testProposedRedTables = new List<Proposed_Red_Tables__c>();
        testProposedRedTables.add(TestDataFactory.createTestProposedRedTables('DOM Mainline', testContractAddendumObj.Id, 10, 'Business', 'J'));
        testProposedRedTables.add(TestDataFactory.createTestProposedRedTables('DOM Regional', testContractAddendumObj.Id, 10, 'Business', 'D'));
        INSERT testProposedRedTables;
        
        List<Red_Circle_Discounts__c> testRedCircleDiscounts = new List<Red_Circle_Discounts__c>();
        testRedCircleDiscounts.add(TestDataFactory.createTestRedCircleDiscounts('DOM Mainline', testContractObj.Id, 10, 'Business', 'J', 'Tier 1'));
        testRedCircleDiscounts.add(TestDataFactory.createTestRedCircleDiscounts('DOM Regional', testContractObj.Id, 10, 'Business', 'J', 'Tier 1'));
        INSERT testRedCircleDiscounts;
    }
    
    @isTest
    private static void testRedCircleDiscountControllerNewDomSeverity() {
        Contract_Addendum__c testContractAddendumObj = [SELECT Domestic_OD_Pairs__c, Regional_OD_Pairs__c, 
                                                        Domestic_Red_Circle_Requested_Tier__c, Regional_Red_Circle_Requested_Tier__c
                                                        FROM Contract_Addendum__c LIMIT 1];
        testContractAddendumObj.Domestic_OD_Pairs__c = 'Domestic OD Pairs';
        testContractAddendumObj.Regional_OD_Pairs__c = 'Regional OD Pairs';
        testContractAddendumObj.Domestic_Red_Circle_Requested_Tier__c = 'Domestic Red Circle Requested Tier';
        testContractAddendumObj.Regional_Red_Circle_Requested_Tier__c = 'Regional Red Circle Requested Tier';
        UPDATE testContractAddendumObj;
        
        Test.startTest();
        PageReference pageRef = Page.RedCircleDiscounts;
        pageRef.getParameters().put('id', testContractAddendumObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractAddendumObj);
        RedCircleDiscountController lController = new RedCircleDiscountController(conL); 
        PageReference initDiscPageReference = lController.initDisc();
        PageReference savePageReference = lController.save();
        PageReference qSavePageReference = lController.qsave();
        PageReference newDomPageReference = lController.newDom();
        PageReference remDomPageReference = lController.remdom();
        PageReference remRegPageReference = lController.remreg();
        PageReference cancelPageReference = lController.cancel();
        lController.newDOMOND();
        lController.newREGOND();
        lController.isTemplate = true;
        lController.isTemplateDiscount = true;
        lController.iseditable  = true;
        lController.isdraft  = true;
        lController.domFlag = 1;
        lController.regFlag = 2;
        Test.stopTest();
        //Asserts
        system.assertEquals(null, initDiscPageReference);
        system.assertEquals('/' + testContractAddendumObj.Id, savePageReference.getUrl());
        system.assertEquals('/apex/RedCircleDiscounts?id=' + testContractAddendumObj.Id, qSavePageReference.getUrl());
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('Guideline Tier and Domestic OD pairs must be selected before inserting discounts', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.WARNING, msg.getSeverity());
        }
        system.assertEquals('/apex/RedCircleDiscounts?id=' + testContractAddendumObj.Id, remDomPageReference.getUrl());
        system.assertEquals('/apex/RedCircleDiscounts?id=' + testContractAddendumObj.Id, remRegPageReference.getUrl());
        system.assertEquals('/' + testContractAddendumObj.Id, cancelPageReference.getUrl());
        //Asserts
        Contract_Addendum__c updatedContractAddendumObj = [SELECT Domestic_OD_Pairs__c, Regional_OD_Pairs__c, 
                                                           Domestic_Red_Circle_Requested_Tier__c, Regional_Red_Circle_Requested_Tier__c
                                                           FROM Contract_Addendum__c WHERE id =:testContractAddendumObj.Id];
        system.assertEquals(null, updatedContractAddendumObj.Domestic_OD_Pairs__c);
        system.assertEquals(null, updatedContractAddendumObj.Regional_OD_Pairs__c);
        system.assertEquals(null, updatedContractAddendumObj.Domestic_Red_Circle_Requested_Tier__c);
        system.assertEquals(null, updatedContractAddendumObj.Regional_Red_Circle_Requested_Tier__c);
    }
    
    @isTest
    private static void testRedCircleDiscountControllerNewReg() {
        Contract_Addendum__c testContractAddendumObj = [SELECT Id FROM Contract_Addendum__c];
        
        Test.startTest();
        PageReference pageRef = Page.RedCircleDiscounts;
        pageRef.getParameters().put('id', testContractAddendumObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractAddendumObj);
        RedCircleDiscountController lController = new RedCircleDiscountController(conL); 
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newRegPageReference = lController.newReg();
        Test.stopTest();
        //Asserts
        system.assertEquals(null, initDiscPageReference);
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('Guideline Tier and Regional OD pairs must be selected before inserting discounts', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.WARNING, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testRedCircleDiscountControllerWithProposedRedTables() {
        Contract_Addendum__c testContractAddendumObj = [SELECT Id, Domestic_OD_Pairs__c, Regional_OD_Pairs__c, Red_Circle__c, Domestic_Red_Circle_Requested_Tier__c, Regional_Red_Circle_Requested_Tier__c  FROM Contract_Addendum__c];
        testContractAddendumObj.Regional_Red_Circle_Requested_Tier__c = 'Tier 1';
        testContractAddendumObj.Domestic_Red_Circle_Requested_Tier__c = 'Tier 1';
        testContractAddendumObj.Domestic_OD_Pairs__c = 'Tier 1';
        testContractAddendumObj.Regional_OD_Pairs__c = 'Tier 1';
        UPDATE testContractAddendumObj;
        
        Test.startTest();
        PageReference pageRef = Page.RedCircleDiscounts;
        pageRef.getParameters().put('id', testContractAddendumObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractAddendumObj);
        RedCircleDiscountController lController = new RedCircleDiscountController(conL);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newDomPageReference = lController.newDom();
        PageReference newRegPageReference = lController.newReg();
        Test.stopTest();
        //Asserts
        system.assertEquals(null, initDiscPageReference);
        system.assertEquals(null, newDomPageReference);
        system.assertEquals(null, newRegPageReference);
        List<Proposed_Red_Tables__c> redTablesDOMMainline = lController.searchResults;
        List<Proposed_Red_Tables__c> redTablesDOMRegional = lController.searchResults1;
        //Asserts
        system.assertEquals(true, redTablesDOMMainline.size() > 0);
        system.assertEquals(true, redTablesDOMRegional.size() > 0);
        //Asserts
        List<Proposal_Table__c> proposalTables = [SELECT Id, DISCOUNT_OFF_PUBLISHED_FARE__c, ELIGIBLE_FARE_TYPE__c, VA_ELIGIBLE_BOOKING_CLASS__c FROM Proposal_Table__c];
        system.assertEquals(10.0, redTablesDOMMainline[0].DISCOUNT_OFF_PUBLISHED_FARE__c);
        system.assertEquals('Business', redTablesDOMMainline[0].ELIGIBLE_FARE_TYPE__c);
        system.assertEquals('J', redTablesDOMMainline[0].VA_ELIGIBLE_BOOKING_CLASS__c);
        system.assertEquals(10.0, redTablesDOMRegional[0].DISCOUNT_OFF_PUBLISHED_FARE__c);
        system.assertEquals('Business', redTablesDOMRegional[0].ELIGIBLE_FARE_TYPE__c);
        system.assertEquals('D', redTablesDOMRegional[0].VA_ELIGIBLE_BOOKING_CLASS__c);
    }
    
    @isTest
    private static void testRedCircleDiscountControllerWithApexError() {
        Contract_Addendum__c testContractAddendumObj = [SELECT Id, Red_Circle__c  FROM Contract_Addendum__c];
        testContractAddendumObj.Red_Circle__c = false;
        UPDATE testContractAddendumObj;
        
        Test.startTest();
        PageReference pageRef = Page.RedCircleDiscounts;
        pageRef.getParameters().put('id', testContractAddendumObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractAddendumObj);
        RedCircleDiscountController lController = new RedCircleDiscountController(conL);
        PageReference initDiscPageReference = lController.initDisc();
        Test.stopTest();
        //Asserts
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('This addendum does not have red circle selected or is not in Draft status', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.WARNING, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testRedCircleDiscountControllerWithNewDomApexError() {
        Contract_Addendum__c testContractAddendumObj = [SELECT Id  FROM Contract_Addendum__c];
        UPDATE testContractAddendumObj;
        
        Test.startTest();
        PageReference pageRef = Page.RedCircleDiscounts;
        pageRef.getParameters().put('id', testContractAddendumObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractAddendumObj);
        RedCircleDiscountController lController = new RedCircleDiscountController(conL);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newDomPageReference = lController.newDom();
        Test.stopTest();
        //Asserts
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('Guideline Tier and Domestic OD pairs must be selected before inserting discounts', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.WARNING, msg.getSeverity());
        }
    }
}