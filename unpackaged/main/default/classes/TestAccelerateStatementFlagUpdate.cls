@isTest
private class TestAccelerateStatementFlagUpdate {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        
    Account account = new Account();
    account.Name = 'TESTFLAGUPDATE' ;   
    account.RecordTypeId ='01290000000tSR0';
    account.Sales_Matrix_Owner__c = 'Accelerate';
	account.OwnerId = '00590000000LbNz';
	account.Account_Owner__c = '00590000000LbNz';
    account.Billing_Country_Code__c ='AU';
    account.Market_Segment__c = 'Accelerate';	
	account.Statement_Generated__c =true;
    account.Business_Number__c =  '49113973087' ; 
    account.Account_Lifecycle__c = 'Contract'    ;
	insert account;  
        
     Test.startTest();   
       AccelerateStatementSendUpdate testaccflagupdate = new AccelerateStatementSendUpdate();
   
        Database.executeBatch(testaccflagupdate);
     Test.StopTest();
    }
}