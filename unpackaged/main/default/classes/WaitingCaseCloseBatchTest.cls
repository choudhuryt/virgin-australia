@isTest
public class WaitingCaseCloseBatchTest {
    static testMethod void batchSuccessScenario(){
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        
        for(integer i=0; i<5;i++){
            Case cs = new Case();            
        	cs.RecordTypeId = caseRTId;
        	cs.Status = 'Waiting for the Member';
            cs.Waiting_Member_Status_Time__c = System.now();
            caseList.add(cs);
        }
        insert caseList;
        
        Test.startTest();
        
        WaitingCaseCloseBatch obj = new WaitingCaseCloseBatch();
        DataBase.executeBatch(obj);
        
        Test.stopTest();
    }
    
    static testMethod void batchSchedulerScenario(){
        Test.startTest();
        WaitingCaseCloseBatchSchedular sched = new WaitingCaseCloseBatchSchedular();
        String sch = '0 0 23 * * ?';
        System.schedule('Test Status Check', sch, sched);
        Test.stopTest();
    } 
}