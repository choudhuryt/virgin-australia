public class TravelBankWaiverCheckController 
{
       ApexPages.StandardController scontroller {get; set;}    
         public List<Waiver_Point__c> searchResults {get;set;}        
         
    
         private User a;
    
     public TravelBankWaiverCheckController(ApexPages.StandardController controller) 
	{
		 sController = controller;
         searchResults= [select id ,Travel_Bank_Compensation_Allowed__c, 
                                      Travel_Bank_Compensation_Used__c,
                                      Travel_Bank_Compensation_Pending__c,
                                      Travel_Bank_Compensation_Draft__c,
                                      Travel_Bank_Compensation_Remaning__c
                                      from Waiver_Point__c where Account__c = '0016F00001sUAt6'
                                      and status__c = 'Active' 
                                      and Waiver_Type__c = 'Travel Bank Compensation'
                                      order by CreatedDate desc limit 1  ];
        
        if(searchResults.size()==0)
                  {
                   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No waiver points allocated for this user');
                   ApexPages.addMessage(myMsg); 
                  }  
         
   
    }
    
    
    
}