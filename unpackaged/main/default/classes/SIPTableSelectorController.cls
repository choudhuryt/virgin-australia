public with sharing class SIPTableSelectorController {
	
	public String contractID {get;set;}
	public String sipOptionId { get; set; }
    public Boolean addsip { get; set; }
	
	public SIPTableSelectorController()
	{
		if(sipOptionId == null)
		{
			sipOptionId = ApexPages.currentPage().getParameters().get('sipid');
		}	
		if(contractID == null)
        {        	
			contractID = ApexPages.currentPage().getParameters().get('cid');
		}
	}
	
	public PageReference CheckExisted()
	{
		PageReference url = null;
        addsip = true ;        
		if(contractID !=null)
		{
			//check existed header
			
			list<SIP_Header__c> listallrevExisted =  [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%All Revenue%' ];
            list<SIP_Header__c> listdomExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%Domestic Revenue%'];
            list<SIP_Header__c> listintExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%International Revenue%'];
            list<SIP_Header__c> listumbExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%Umbrella Revenue%'];
            
            list<SIP_Header__c> listaddExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%Key Market Revenue%'];
	/*		if(listallrevExisted != null && listallrevExisted.size() >0)
            {     
				//url.setRedirect(true);
				addsip = false ;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'There are existing ALL revenue SIP tables for this contract.If you want to add new tables, please delete the existing ones  and if you want to edit the existing tables, please click on the Edit SIP Table button'));          
			    return null;
			}*/
            
           if( (listdomExisted != null && listintExisted.size() >0) && (listdomExisted != null && listdomExisted.size() >0) && (listumbExisted != null && listumbExisted.size() >0))
               
            {     
				//url.setRedirect(true);
				addsip = false ;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'There are existing Domestic,International ,Umbrella and Key Market revenue SIP tables for this contract.If you want to add new tables, please delete the existing ones  and if you want to edit the existing tables, please click on the Edit SIP Table button'));          
			    return null;
			}
            
                
		}
 	  return url ;
    }
	
    public PageReference Cancel()
	{
		PageReference url = null;
		if(contractID !=null)
		{
			url = new PageReference('/' +contractID);
		}
		else
		{
			url = new PageReference('/');
		}
		url.setRedirect(true);
		return url;
    }
	
	public List<SIPSelectionWrapper> SIPSelectionWrapperObj = new List<SIPSelectionWrapper>();
    public List<SIPSelectionWrapperDom> SIPSelectionWrapperObjDom = new List<SIPSelectionWrapperDom>();
    public List<SIPSelectionWrapperInt> SIPSelectionWrapperObjInt = new List<SIPSelectionWrapperInt>();
	public List<SIPSelectionWrapperUmb> SIPSelectionWrapperObjUmb = new List<SIPSelectionWrapperUmb>();
    public List<SIPSelectionWrapperAdd> SIPSelectionWrapperObjAdd = new List<SIPSelectionWrapperAdd>();
    
    private String returnContractType='';
    
	public List<SIPSelectionWrapper> getSIPOption()
    {
        if(SIPSelectionWrapperObj == null || SIPSelectionWrapperObj.size() == 0)
        {
        		List<Contract> contractList = [SELECT Type__c,Record_Type__c  FROM Contract WHERE Contract.Id =:contractID];
        		if(contractList != null && contractList.size()>0){
        			Contract contract =  contractList.get(0);          
            String contractType =contract.Type__c;
            String contractRecordType =contract.Record_Type__c;
            returnContractType = contractType;
            List<SIP_Selection__c> sipselectionObj=	 [Select s.Unique_ID__c, s.SystemModstamp, s.Record_Type__c, s.OwnerId,
            										  s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsDeleted, 
            										  s.Id, s.CreatedDate, s.CreatedById, s.Contract_Type__c, 
            										  (Select Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, Option__c, Unique_ID__c, SIP_Selection__c, Description__c 
            										  From SIP_Options__r where Description__c LIKE '%All Revenue%' order by sort_order__c asc ) 
            										  From SIP_Selection__c s where s.Contract_Type__c =:contractType and s.Record_Type__c=:contractRecordType];
					system.debug('Options available in soql' + sipselectionObj + contractType + contractRecordType ) ;	
                    
                    for(SIP_Selection__c SIPSelectionItem : sipselectionObj) 
						{
							List<SIP_Option__c> SIPOptionIList = SIPSelectionItem.SIP_Options__r; 
                            system.debug('Options list' + SIPOptionIList ) ;	
							for(SIP_Option__c SIPOptionItem : SIPOptionIList)
							{
								SIPSelectionWrapperObj.add(new SIPSelectionWrapper(SIPOptionItem,sipOptionId));
							}
						}
           
                    
			        }
        		}
            system.debug('Options available' + SIPSelectionWrapperObj  ) ;
            return SIPSelectionWrapperObj;
    } 
    
    
    public List<SIPSelectionWrapperDom> getSIPOptionDom()
    {
        if(SIPSelectionWrapperObjDom == null || SIPSelectionWrapperObjDom.size() == 0)
        {
        		List<Contract> contractList = [SELECT Type__c,Record_Type__c  FROM Contract WHERE Contract.Id =:contractID];
        		if(contractList != null && contractList.size()>0){
        			Contract contract =  contractList.get(0);          
            String contractType =contract.Type__c;
            String contractRecordType =contract.Record_Type__c;
            returnContractType = contractType;
            List<SIP_Selection__c> sipselectionObjDom=	 [Select s.Unique_ID__c, s.SystemModstamp, s.Record_Type__c, s.OwnerId,
            						  s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsDeleted, 
            						  s.Id, s.CreatedDate, s.CreatedById, s.Contract_Type__c, 
            						  (Select Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById,
                                       SystemModstamp, Option__c, Unique_ID__c, SIP_Selection__c, Description__c 
                                       
            						   From SIP_Options__r where Description__c LIKE '%Domestic Revenue%'  order by sort_order__c asc ) 
            						   From SIP_Selection__c s where s.Contract_Type__c =:contractType and
                                                          s.Record_Type__c=:contractRecordType];
						for(SIP_Selection__c SIPSelectionItemDom : sipselectionObjDom) 
						{
							List<SIP_Option__c> SIPOptionIListDom = SIPSelectionItemDom.SIP_Options__r; 
							for(SIP_Option__c SIPOptionItemDom : SIPOptionIListDom)
							{
								SIPSelectionWrapperObjDom.add(new SIPSelectionWrapperDom(SIPOptionItemDom,sipOptionId));
							}
						}
           
                    
			        }
        		}
            return SIPSelectionWrapperObjDom;
    } 
    
    public List<SIPSelectionWrapperInt> getSIPOptionInt()
    {
        if(SIPSelectionWrapperObjInt == null || SIPSelectionWrapperObjInt.size() == 0)
        {
        		List<Contract> contractList = [SELECT Type__c,Record_Type__c  FROM Contract WHERE Contract.Id =:contractID];
        		if(contractList != null && contractList.size()>0){
        			Contract contract =  contractList.get(0);          
            String contractType =contract.Type__c;
            String contractRecordType =contract.Record_Type__c;
            returnContractType = contractType;
            List<SIP_Selection__c> sipselectionObjInt=	 [Select s.Unique_ID__c, s.SystemModstamp, s.Record_Type__c, s.OwnerId,
            						  s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsDeleted, 
            						  s.Id, s.CreatedDate, s.CreatedById, s.Contract_Type__c, 
            						  (Select Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById,                                                                                                                                                                       SystemModstamp, Option__c, Unique_ID__c, SIP_Selection__c, Description__c 
            						   From SIP_Options__r where Description__c LIKE '%International Revenue%'  order by sort_order__c asc ) 
            						   From SIP_Selection__c s where s.Contract_Type__c =:contractType and                                                                                                                                                                                                      s.Record_Type__c=:contractRecordType];
						for(SIP_Selection__c SIPSelectionItemInt : sipselectionObjInt) 
						{
							List<SIP_Option__c> SIPOptionIListInt = SIPSelectionItemInt.SIP_Options__r; 
							for(SIP_Option__c SIPOptionItemInt : SIPOptionIListInt)
							{
								SIPSelectionWrapperObjInt.add(new SIPSelectionWrapperInt(SIPOptionItemInt,sipOptionId));
							}
						}
           
                    
			        }
        		}
            return SIPSelectionWrapperObjInt;
    } 
    
    
    public List<SIPSelectionWrapperUmb> getSIPOptionUmb()
    {
        if(SIPSelectionWrapperObjUmb == null || SIPSelectionWrapperObjUmb.size() == 0)
        {
        		List<Contract> contractList = [SELECT Type__c,Record_Type__c  FROM Contract WHERE Contract.Id =:contractID];
        		if(contractList != null && contractList.size()>0){
        			Contract contract =  contractList.get(0);          
            String contractType =contract.Type__c;
            String contractRecordType =contract.Record_Type__c;
            returnContractType = contractType;
            List<SIP_Selection__c> sipselectionObjUmb=	 [Select s.Unique_ID__c, s.SystemModstamp, s.Record_Type__c, s.OwnerId,
            						  s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsDeleted, 
            						  s.Id, s.CreatedDate, s.CreatedById, s.Contract_Type__c, 
            						  (Select Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById,                                                                                                                                                                       SystemModstamp, Option__c, Unique_ID__c, SIP_Selection__c, Description__c 
            						   From SIP_Options__r where Description__c =: 'Umbrella Revenue'  order by sort_order__c asc ) 
            						   From SIP_Selection__c s where s.Contract_Type__c =:contractType and                                                                                                                                                                                                      s.Record_Type__c=:contractRecordType];
						for(SIP_Selection__c SIPSelectionItemUmb : sipselectionObjUmb) 
						{
							List<SIP_Option__c> SIPOptionIListUmb = SIPSelectionItemUmb.SIP_Options__r; 
							for(SIP_Option__c SIPOptionItemUmb : SIPOptionIListUmb)
							{
								SIPSelectionWrapperObjUmb.add(new SIPSelectionWrapperUmb(SIPOptionItemUmb,sipOptionId));
							}
						}
           
                    
			        }
        		}
            return SIPSelectionWrapperObjUmb;
    } 
    
    public List<SIPSelectionWrapperAdd> getSIPOptionAdd()
    {
        if(SIPSelectionWrapperObjAdd == null || SIPSelectionWrapperObjAdd.size() == 0)
        {
        		List<Contract> contractList = [SELECT Type__c,Record_Type__c  FROM Contract WHERE Contract.Id =:contractID];
        		if(contractList != null && contractList.size()>0){
        			Contract contract =  contractList.get(0);          
            String contractType =contract.Type__c;
            String contractRecordType =contract.Record_Type__c;
            returnContractType = contractType;
            List<SIP_Selection__c> sipselectionObjAdd=	 [Select s.Unique_ID__c, s.SystemModstamp, s.Record_Type__c, s.OwnerId,
            						  s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsDeleted, 
            						  s.Id, s.CreatedDate, s.CreatedById, s.Contract_Type__c, 
            						  (Select Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById,                                                                                                                                                                       SystemModstamp, Option__c, Unique_ID__c, SIP_Selection__c, Description__c 
            						   From SIP_Options__r where Description__c =: 'Key Market Revenue'  order by sort_order__c asc ) 
            						   From SIP_Selection__c s where s.Contract_Type__c =:contractType and                                                                                                                                                                                                      s.Record_Type__c=:contractRecordType];
						for(SIP_Selection__c SIPSelectionItemAdd : sipselectionObjAdd) 
						{
							List<SIP_Option__c> SIPOptionIListAdd = SIPSelectionItemAdd.SIP_Options__r; 
							for(SIP_Option__c SIPOptionItemAdd : SIPOptionIListAdd)
							{
								SIPSelectionWrapperObjAdd.add(new SIPSelectionWrapperAdd(SIPOptionItemAdd,sipOptionId));
							}
						}
           
                    
			        }
        		}
            return SIPSelectionWrapperObjAdd;
    } 
    
    public PageReference Next()
    {
    	Integer selectedNumberallrev = 0;
        Integer selectedNumberdom = 0;
        Integer selectedNumberint = 0;
        Integer selectedNumberumb = 0;
        Integer selectedNumberadd = 0;
        Integer existingNumberallrev = 0;
        Integer existingNumberdom = 0;
        Integer existingNumberint = 0;
        Integer existingNumberumb = 0;
        Integer existingNumberadd = 0;
        Integer existingNumberkeydom = 0;
        Integer existingNumberkeyhkc = 0;
        Integer existingNumberkeyusa = 0;
        Integer existingNumberkeytt = 0;
        Integer existingNumberother = 0; 
        
        list<SIP_Header__c> listallrevExisted =  [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%All Revenue%' ];
        list<SIP_Header__c> listdomExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%Domestic Revenue%'];
        list<SIP_Header__c> listintExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%International Revenue%'];
        list<SIP_Header__c> listumbExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%Umbrella Revenue%'];
        list<SIP_Header__c> listaddExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%Key Market Revenue%'];
        list<SIP_Header__c> listkeydomExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%Key Market Revenue%' and s.Key_Market__c = 'DOM'];
        
        list<SIP_Header__c> listkeyusaExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%Key Market Revenue%' and s.Key_Market__c = 'USA'];
        
        list<SIP_Header__c> listkeyhkcExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%Key Market Revenue%' and s.Key_Market__c = 'HKG/China'];
        
        list<SIP_Header__c> listkeyttExisted =   [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s 
                                                      where s.Contract__c =:contractID  and s.Description__c like '%Key Market Revenue%' and s.Key_Market__c = 'Trans-Tasman'];
        
        existingNumberallrev = listallrevExisted.size();
        existingNumberdom = listdomExisted.size();
        existingNumberint = listintExisted.size();
        existingNumberumb = listumbExisted.size(); 
        existingNumberadd = listaddExisted.size(); 
        existingNumberkeydom = listkeydomExisted.size();
        existingNumberkeyusa = listkeyusaExisted.size();
        existingNumberkeytt = listkeyttExisted.size();
        existingNumberkeyhkc = listkeyhkcExisted.size();
        
        existingNumberother =  existingNumberdom+ existingNumberint+ existingNumberumb  ;    
        
        system.debug ('The existing options are' + existingNumberallrev +  existingNumberdom + existingNumberint + existingNumberumb + existingNumberother) ;        
    	
        String sipid='';
      
        
    	for( SIPSelectionWrapper obj : SIPSelectionWrapperObj)
    	{
    		if(obj.selected)
    		{
    			if(sipid=='')
    			{
    				sipid=obj.sip_option.Id;
    			}
    			selectedNumberallrev++;
    		}
    	}
        
        for( SIPSelectionWrapperDom obj : SIPSelectionWrapperObjDom)
    	{
    		if(obj.selecteddom)
    		{
    			if(sipid=='')
    			{
    				sipid=obj.sip_optiondom.Id;
    			}
    			selectedNumberdom++;
    		}
    	}
        
        for( SIPSelectionWrapperInt obj : SIPSelectionWrapperObjInt)
    	{
    		if(obj.selectedint)
    		{
    			if(sipid=='')
    			{
    				sipid=obj.sip_optionint.Id;
    			}
    			selectedNumberint++;
    		}
    	}
        
        for( SIPSelectionWrapperUmb obj : SIPSelectionWrapperObjUmb)
    	{
    		if(obj.selectedumb)
    		{
    			if(sipid=='')
    			{
    				sipid=obj.sip_optionumb.Id;
    			}
    			selectedNumberumb++;
    		}
    	}
        
           for( SIPSelectionWrapperadd obj : SIPSelectionWrapperObjadd)
    	{
    		if(obj.selectedadd)
    		{
    			if(sipid=='')
    			{
    				sipid=obj.sip_optionadd.Id;
    			}
    			selectedNumberadd++;
    		}
    	}
       Integer selectedNumberother = 0;
       selectedNumberother = selectedNumberint + selectedNumberdom + selectedNumberumb   ;
      
        
       system.debug ('The selected options are' +selectedNumberallrev  +  selectedNumberdom  +  selectedNumberint + sipid ) ;
        
    	if(selectedNumberallrev==0 &&  selectedNumberdom==0  && selectedNumberint==0   && selectedNumberumb==0  && selectedNumberadd == 0)
    	{
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select one SIP Table!'));
    		return null;
    	}
        
        if( (selectedNumberallrev > 0 &&  existingNumberother > 0 ) || (selectedNumberother > 0 &&  existingNumberallrev > 0 ) )
    	{
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select either All Revenue Option or a combination of Domestic , International and Umbrella  Revenue Option'));
    		return null;
    	}
        
    	if(selectedNumberallrev >1 && ( selectedNumberint > 1 || selectedNumberdom > 1 || selectedNumberumb > 1) )
    	{
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Only 1 SIP Option can be selected'));
    		return null;
    	}
        
        if( (existingNumberallrev >0 && selectedNumberallrev >0 ) ||
            (existingNumberdom >0 && selectedNumberdom > 0) || 
            (existingNumberint >0 && selectedNumberint > 0) || 
            (existingNumberumb >0 && selectedNumberumb > 0) )
         //   (existingNumberadd >0 && selectedNumberadd > 0) ) 
    	{
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Only 1 SIP Option can be selected'));
    		return null;
    	}
        
   		PageReference nextPage = new PageReference('/apex/SIPInput?cid='+contractID+'&sipid=' + EncodingUtil.urlEncode(sipid, 'UTF-8'));
        nextPage.setRedirect(true);    	
    	return nextPage;
    }
    public String GetContractType()
    {
    	return returnContractType;
    }
    public String GetContractID()
    {
        return contractID;       
    }
 	public class SIPSelectionWrapper
    {        
      public SIP_Option__c sip_option{get; set;}
      public Boolean selected {get; set;}
      public SIPSelectionWrapper(SIP_Option__c sip, String sipOptionId)
      {
      	selected = false;
          
		if(sipOptionId == sip.Id)
		{
			selected = true;
		}
      	sip_option = sip;
      }
    }
    
    public class SIPSelectionWrapperDOM
    {        
      public SIP_Option__c sip_optiondom{get; set;}
      public Boolean selecteddom {get; set;}
      public SIPSelectionWrapperDOM(SIP_Option__c sip, String sipOptionId)
      {
      	selecteddom = false;
		if(sipOptionId == sip.Id)
		{
			selecteddom = true;
		}
      	sip_optiondom = sip;
      }
    }
    
     public class SIPSelectionWrapperInt
    {        
      public SIP_Option__c sip_optionint{get; set;}
      public Boolean selectedint {get; set;}
      public SIPSelectionWrapperInt(SIP_Option__c sip, String sipOptionId)
      {
      	selectedInt = false;
		if(sipOptionId == sip.Id)
		{
			selectedInt = true;
		}
      	sip_optionINT = sip;
      }
    }
    
     public class SIPSelectionWrapperUmb
    {        
      public SIP_Option__c sip_optionumb{get; set;}
      public Boolean selectedumb {get; set;}
      public SIPSelectionWrapperUmb(SIP_Option__c sip, String sipOptionId)
      {
      	selectedUmb = false;
		if(sipOptionId == sip.Id)
		{
			selectedUmb = true;
		}
      	sip_optionUmb = sip;
      }
    }
    
    public class SIPSelectionWrapperAdd
    {        
      public SIP_Option__c sip_optionadd{get; set;}
      public Boolean selectedadd {get; set;}
      public SIPSelectionWrapperadd(SIP_Option__c sip, String sipOptionId)
      {
      	selectedadd = false;
		if(sipOptionId == sip.Id)
		{
			selectedadd = true;
		}
      	sip_optionadd = sip;
      }
    }
}