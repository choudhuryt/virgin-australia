/**
* This class is controller for Passenger List VF page
* @author Son Pham
*/
public class PassengerListController {
    public List<PassengerBinding> passengerList {get; set;}
    public String caseId {get; set;}
    public Case caseRecord {get; set;}
    private Reservation__c reservObj {get;set;}

    /**
    * Default constructor
    */
    public PassengerListController() {
        String caseId = ApexPages.currentPage().getParameters().get('id');
        this.initController(caseId);
    }

    /**
    * Initiates controller
    * @param caseId id of the current case
    */
    public void initController(String caseId) {
        caseRecord = new Case();
        List<Case> caseList = [SELECT Id, CaseNumber, Flight_Number__c, Flight_Date__c, Reservation_Number__c, Airline_Organisation_Short__c
                           FROM Case
                           WHERE Id = :caseId];
        if (!caseList.isEmpty()) {
            caseRecord = caseList.get(0);
        }
        this.caseId = caseRecord.Id;
        List<Reservation__c> resList = [SELECT Id, Case__c, Flight_Number__c, Flight_Date__c
                           FROM Reservation__c
                           WHERE Case__c = :caseId
                           ORDER BY LastModifiedDate
                           DESC LIMIT 1];
        if (!resList.isEmpty()) {
            reservObj = resList.get(0);
        } else {
            reservObj = new Reservation__c();
        }
        // Dodgy HACK to be able to use the caseObject on the page as the Reservation object was created with Flight Date as a timestamp
        if (reservObj.Flight_Number__c != null && reservObj.Flight_Date__c != null) {
            caseRecord.Flight_Number__c = reservObj.Flight_Number__c;
            caseRecord.Flight_Date__c = reservObj.Flight_Date__c.date();
        }
    }

    /**
    * Searchs passengers when case officer clicks on the Search on the popup
    */
    public void searchPassenger() {
        SalesforcePassengerBinding passengerBindingObject = new SalesforcePassengerBinding();
        passengerList = null;   // reset search result to null
        // Call real web-service
        // SAMPLE DATA WORKING FINE (please replace by UI input)
        try { // Added try/catch to distinguish success response and fault response from weservice callouts - Son Pham
            //SalesForceHistoricalDetailModel.GetPassengerManifestRSType result =
            //    passengerBindingObject.getPassengerDetails('VA','0007','2013-02-27');
            
            // break flight number into airline code and 4-digit number (US5764)
            List<String> flightNumber = Utilities.formatFlightNumber(caseRecord.Flight_Number__c);
            caseRecord.Flight_Number__c = flightNumber.get(0) + flightNumber.get(1);
         
            SalesForceHistoricalDetailModel.GetPassengerManifestRSType result =
            passengerBindingObject.getPassengerDetails(flightNumber.get(0), flightNumber.get(1), 
                                                      Utilities.convertDate2String(this.caseRecord.Flight_Date__c));
            passengerList = preparePassengerList(result);

            if (reservObj.Flight_Number__c != caseRecord.Flight_Number__c 
                || reservObj.Flight_Date__c.date() != caseRecord.Flight_Date__c)
            {
                reservObj.Flight_Date__c = caseRecord.Flight_Date__c;
                reservObj.Flight_Number__c = caseRecord.Flight_Number__c;

                // link new reservation obj to parent case                
                if (String.isBlank(reservObj.Id)){
                    reservObj.Case__c = caseRecord.Id;
                }
                // begin DE4911 bug fixing - Charleston Telles
                // it must be upsert, if the Reservation record exist it MUST be updated
                // and not inserted again (duplicate Reservation per Case)
                upsert reservObj;
                // end DE4911 bug fixing
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 
                                                           'Search criteria are saved'));
            }
        }
        catch (System.CalloutException e) {
//            System.debug('Callout exception: ' + e.getMessage());
            String error = e.getMessage();
            if (String.isNotEmpty(error) && String.isNotBlank(error.substringAfter('SOAP Fault:').substringBefore('faultcode'))) {
                error = error.substringAfter('SOAP Fault:').substringBefore('faultcode');
            } else {
                error = 'Communication Error';
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
        }
    }

    /**
    * Gets data from passenger manifest returned from webservice to display on the popup
    * @param result passenger manifest
    * @return list of passenger binding objects
    */
    public List<PassengerBinding> preparePassengerList(SalesForceHistoricalDetailModel.GetPassengerManifestRSType result) {
        List<PassengerBinding> passList = new List<PassengerBinding>();
        if (result == null) {
            return passList;
        }
        SalesForceHistoricalDetailModel.FlightManifestType flightType = result.FlightManifest;
        SalesForceHistoricalDetailModel.PassengerType[] passengers = flightType.Passenger;
        PassengerBinding pass;
        for (SalesForceHistoricalDetailModel.PassengerType passType : passengers) {
            pass = new PassengerBinding();
            pass.AirlineCode = result.AirlineCode;
            pass.FlightDate = result.FlightDate;
            pass.FlightNumber = result.FlightNumber;
            pass.PNRLocator = passType.PNRLocator;
            pass.PNRCreatedDate = passType.PNRCreatedDate;
            pass.PassengerName = passType.FirstName + ' ' + passType.Surname;
            pass.Gender = passType.Gender;
            pass.PNROrigin = passType.PNROrigin;
            pass.PNRDestination = passType.PNRDestination;
            // bug fixing DE5004 - concatenating seat number and seat row
            // author: Charleston Telles
            pass.SeatRowNumber = passType.SeatRowNumber + '' + passType.SeatLetter;
            // end bug fixing DE5004
            pass.BoardingPassIssuedFlag = passType.BoardingPassIssuedFlag;
            pass.OnboardFlag = passType.OnboardFlag;
            pass.NoShowFlag = passType.NoShowFlag;
            pass.ServiceStartDate = passType.ServiceStartDate;
            pass.PriorityListFlag = passType.PriorityListFlag;
            pass.InboundConnectionFlag = passType.InboundConnectionFlag;
            pass.OutboundConnectionFlag = passType.OutboundConnectionFlag;

            SalesForceHistoricalDetailModel.CheckInType checkInType = passType.CheckIn;
            if (checkInType != null) {
                pass.CheckInGroupCode = checkInType.CheckInGroupCode;
                pass.CheckInGroupCount = checkInType.CheckInGroupCount;
                pass.RemoteCheckInFlag = checkInType.RemoteCheckInFlag;
                pass.MobileCheckInFlag = checkInType.MobileCheckInFlag;
                pass.KioskCheckInFlag = checkInType.KioskCheckInFlag;
            }

            SalesForceHistoricalDetailModel.FrquentTravellerType travelType = passType.FrequentTraveller;
            if (travelType != null) {
                pass.FrequentTravellerVendorCode = travelType.FrequentTravellerVendorCode;
                pass.FrequentTravellerNumber = travelType.FrequentTravellerNumber;
                pass.FrequentTravellerTier = travelType.FrequentTravellerTier;
            }

            SalesForceHistoricalDetailModel.BaggageType baggage = passType.Baggage;
            if (baggage != null) {
                pass.BagCount = baggage.BagCount;
                pass.TotalBagWeight = baggage.TotalBagWeight;
            }

            passList.add(pass);
        }

        return passList;
    }

    /**
    * Model to bind passenger details to the popup
    */
    public class PassengerBinding {
        public String AirlineCode {get; set;}
        public String FlightDate {get; set;}
        public String FlightNumber {get; set;}

        public String PNRLocator {get;set;}
        public String PNRCreatedDate {get;set;}
//        public String FirstName;
//        public String Surname;
        public String PassengerName {get; set;}
        public String Gender {get;set;}
        public String PNROrigin {get;set;}
        public String PNRDestination {get;set;}
        public String SeatRowNumber {get;set;}
        public String BoardingPassIssuedFlag {get;set;}
        public String OnboardFlag {get;set;}
        public String NoShowFlag {get;set;}
        public String ServiceStartDate {get;set;}
        public String PriorityListFlag {get;set;}
        public String InboundConnectionFlag {get;set;}
        public String OutboundConnectionFlag {get;set;}

        public String CheckInGroupCode {get;set;}
        public Integer CheckInGroupCount {get;set;}
        public String RemoteCheckInFlag {get;set;}
        public String MobileCheckInFlag {get;set;}
        public String KioskCheckInFlag {get;set;}

        public String FrequentTravellerVendorCode {get;set;}
        public String FrequentTravellerNumber {get;set;}
        public String FrequentTravellerTier {get;set;}

        public Integer BagCount {get;set;}
        public Integer TotalBagWeight {get;set;}

        public PassengerBinding(){}
    }

}