public class UpdateRelatedTravelAgent {
    
@InvocableMethod
public static void UpdateRelatedAccount(List<Id> CaseIds  )   
    {
      List<Case> lstCaseUpdate = new List<Case>(); 
        
      List<Data_Validity__c> iataList = new List<Data_Validity__c>(); 
      List<Data_Validity__c> pccList = new List<Data_Validity__c>();  
        
      List<Case> caselist =  [Select Id, PCC_From_Web__c, IATA_From_Web__c , Related_Travel_Agent_Account__c from Case where id =:CaseIds]; 
   
      system.debug ( 'The case details' + caselist.size() +   caselist[0].IATA_From_Web__c + caselist[0].PCC_From_Web__c  );
      if( caselist.size() > 0  &&  caselist[0].IATA_From_Web__c != NULL)
      {
        iataList =  [select Id,IATA_Number__c,From_Account__c from Data_Validity__c 
                                           where IATA_Number__c =:caselist[0].IATA_From_Web__c 
                                           and  To_Date__c = null
                                           and  Soft_Delete__c= false ] ;     
      }
      
      if( caselist.size() > 0  &&  caselist[0].PCC_From_Web__c != NULL)
      {
        pccList =  [select Id,PCC_name__c,From_Account__c from Data_Validity__c 
                                           where PCC_name__c =:caselist[0].PCC_From_Web__c
                                           and  To_Date__c = null
                                           and  Soft_Delete__c= false ] ;     
      }  
        
      for(Case c: caselist)
      {
         system.debug('Checking IATA PCC' + iataList.size() +  pccList.size() ) ;
       
          if(iataList.size() > 0 && iataList[0].From_Account__c != NULL )  
         {
           c.Related_Travel_Agent_Account__c =  iataList[0].From_Account__c; 
            lstCaseUpdate.add(c);   
                        
         }else if( pccList.size() > 0 && pccList[0].From_Account__c != NULL )  
         {
          c.Related_Travel_Agent_Account__c =  pccList[0].From_Account__c;  
           lstCaseUpdate.add(c);    
         }  
           
       }
        
       update lstCaseUpdate; 
      } 
}