global class WaitingCaseCloseBatchSchedular implements Schedulable{
	global void execute(SchedulableContext sc){
        WaitingCaseCloseBatch caseUp = new WaitingCaseCloseBatch();
        database.executeBatch(caseUp);
    }
}