/** * File Name      : TestInternationalDiscountController
* Description        : This Apex Test Class is the Test Class for DomesticDiscountController  
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Andrew Burgess
* * Date             : Updated 28 February 2013
* * Technical Task ID: 
* * Notes            :  The test class for this file is:  TestDomesticDiscountController
* Modification Log =============================================================== 
Ver Date Author Modification --- ---- ------ -------------
* */
/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
* @updatedBy : cloudwerx
*/
@isTest
private class TestInternationalDiscountController {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('Test Contract', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj.Cos_Version__c = '13.0';
        testContractObj.va_sq_requested_tier__c='1';
        testContractObj.Red_Circle__c =true;
        testContractObj.IsTemplate__c =true;
        testContractObj.UK_Europe_Revenue_Target__c = 1000;
        testContractObj.Middle_East_Revenue_Target__c = 1000;
        testContractObj.Asia_Revenue_Target__c = 1000;
        testContractObj.Africa_revenue_Target__c = 1000;
        INSERT testContractObj;
    }
    
    @isTest
    private static void testInternationalDiscountController() {
        Contract testContractObj = [SELECT Id, Cos_Version__c, VA_SQ_Requested_Tier__c FROM Contract];
        testContractObj.Cos_Version__c = '11.2';
        testContractObj.VA_SQ_Requested_Tier__c = '1';
        UPDATE testContractObj;
        
        List<Contract_Calculation_Discount_Levels__c> testContractCalculationDiscountLevels = new List<Contract_Calculation_Discount_Levels__c>();
        testContractCalculationDiscountLevels.add(TestDataFactory.createTestContractCalculationDiscountLevels(2.2, 2.2, 2.1, 'SQ', 'International Middle East', 1.0));
        testContractCalculationDiscountLevels.add(TestDataFactory.createTestContractCalculationDiscountLevels(2.2, 2.2, 2.1, 'SQ', 'International Asia', 1.0));
        testContractCalculationDiscountLevels.add(TestDataFactory.createTestContractCalculationDiscountLevels(2.2, 2.2, 2.1, 'SQ', 'International Africa', 1.0));
        testContractCalculationDiscountLevels.add(TestDataFactory.createTestContractCalculationDiscountLevels(2.2, 2.2, 2.1, 'SQ', 'International UK/Europe', 1.0));
        INSERT testContractCalculationDiscountLevels;
        
        Cost_of_Sale__c testCostOfSaleObj = TestDataFactory.createTestCostOfSale(testContractObj.Id);
        INSERT testCostOfSaleObj;
        
        Test.startTest();
        InternationalDiscountController lController = getInternationalDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        
        International_Discounts_Asia__c testInternationalDiscountsAsiaObj = TestDataFactory.createTestInternationalDiscountsAsia(testContractObj.Id, '1', false, '13.2');
        testInternationalDiscountsAsiaObj.Cos_Version__c = '11.2';
        testInternationalDiscountsAsiaObj.Is_Template__c = true;
        testInternationalDiscountsAsiaObj.Tier__c = '1';
        testInternationalDiscountsAsiaObj.IntlDisc_Asia_DiscOffPublishFare__c = 2;
        testInternationalDiscountsAsiaObj.IntlDisc_Asia_DiscOffPublishFare2__c = 2;
        testInternationalDiscountsAsiaObj.IntlDisc_Asia_DiscOffPublishFare3__c = 2;
        testInternationalDiscountsAsiaObj.IntlDisc_Asia_DiscOffPublishFare6__c = 2;
        INSERT testInternationalDiscountsAsiaObj;
        PageReference newAsiaPageReference = lController.newAsia();
        
        International_Discounts_UK_EUROPE__c testInternationalDiscountsUKEUROPEObj = TestDataFactory.createTestInternationalDiscountsUKEUROPE(testContractObj.Id, '1', false, '13.2');
        testInternationalDiscountsUKEUROPEObj.Cos_Version__c = '11.2';
        testInternationalDiscountsUKEUROPEObj.Is_Template__c = true;
        testInternationalDiscountsUKEUROPEObj.Tier__c = '1';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_DiscOffPublishedFare__c = 2;
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_DiscOffPublishedFare2__c = 3;
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_DiscOffPublishedFare3__c = 4;
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_DiscOffPublishedFare6__c = 5;
        INSERT testInternationalDiscountsUKEUROPEObj;        
        PageReference newInterUKEURPageReference = lController.newInterUKEUR();
        
        International_Discounts_Middle_East__c testInternationalDiscountsMiddleEastObj = TestDataFactory.createTestInternationalDiscountsMiddleEast(testContractObj.Id, '1', false, '13.2');
        testInternationalDiscountsMiddleEastObj.Cos_Version__c = '11.2';
        testInternationalDiscountsMiddleEastObj.Is_Template__c = true;
        testInternationalDiscountsMiddleEastObj.Tier__c = '1';
        testInternationalDiscountsMiddleEastObj.Intl_Disc_MiddleEast_DiscOffPubFar__c = 10;
        testInternationalDiscountsMiddleEastObj.Intl_Disc_MiddleEast_DiscOffPubFar2__c = 10;
        testInternationalDiscountsMiddleEastObj.Intl_Disc_MiddleEast_DiscOffPubFar3__c = 10;
        testInternationalDiscountsMiddleEastObj.Intl_Disc_MiddleEast_DiscOffPubFar6__c = 10;
        INSERT testInternationalDiscountsMiddleEastObj;
        PageReference newMiddleEPageReference = lController.newMiddleE();
        
        International_Discounts_Africa__c testInternationalDiscountsAfricaObj = TestDataFactory.createTestInternationalDiscountsAfrica(testContractObj.Id, '1', false, '13.2');
        testInternationalDiscountsAfricaObj.Cos_Version__c = '11.2';
        testInternationalDiscountsAfricaObj.Is_Template__c = true;
        testInternationalDiscountsAfricaObj.Tier__c = '1';
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare__c = 2;
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare2__c = 2;
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare3__c = 2;
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare6__c = 2;
        INSERT testInternationalDiscountsAfricaObj;
        PageReference newAfricaPageReference = lController.newAfrica();
        
        PageReference savePageReference = lController.save();
        PageReference qsavePageReference = lController.qsave();
        PageReference cancelPageReference = lController.cancel();
        test.stopTest();
        //Asserts
        Contract updateContractObj = [SELECT Id, SQEUDiscountAvailable__c, SQAsDiscountAvailable__c, SQAfDiscountAvailable__c FROM Contract];
        system.assertEquals(true, updateContractObj.SQEUDiscountAvailable__c);
        system.assertEquals(true, updateContractObj.SQAsDiscountAvailable__c);
        system.assertEquals(true, updateContractObj.SQAfDiscountAvailable__c);
    }
    
    @isTest
    private static void testInternationalDiscountControllerWithFalseRedCircle() {
        Contract testContractObj = [SELECT Id, Red_Circle__c FROM Contract];
        testContractObj.Red_Circle__c = false;
        UPDATE testContractObj;
        
        Test.startTest();
        InternationalDiscountController lController = getInternationalDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newAsiaPageReference = lController.newAsia();     
        PageReference newInterUKEURPageReference = lController.newInterUKEUR();
        PageReference newMiddleEPageReference = lController.newMiddleE();
        PageReference newAfricaPageReference = lController.newAfrica();
        
        PageReference savePageReference = lController.save();
        PageReference qsavePageReference = lController.qsave();
        PageReference cancelPageReference = lController.cancel();
        test.stopTest();
        //Asserts
        Contract updateContractObj = [SELECT Id, SQEUDiscountAvailable__c, SQAsDiscountAvailable__c, SQAfDiscountAvailable__c FROM Contract];
        system.assertEquals(true, updateContractObj.SQEUDiscountAvailable__c);
        system.assertEquals(true, updateContractObj.SQAsDiscountAvailable__c);
        system.assertEquals(true, updateContractObj.SQAfDiscountAvailable__c);
    }
    
    @isTest
    private static void testInternationalDiscountControllerWithApexMessages() {
        Contract testContractObj = [SELECT Id, istemplate__c FROM Contract];
        testContractObj.istemplate__c = false;
        UPDATE testContractObj;
        
        EY_Discount_Africa__c testEYDiscountAfricaObj = TestDataFactory.createTestEYDiscountAfrica(testContractObj.Id, '1', false, '13.2');
        INSERT testEYDiscountAfricaObj;
        
        EY_Discount_Middle_East__c testEYDiscountMiddleEastObj = TestDataFactory.createTestEYDiscountMiddleEast(testContractObj.Id, '1', false, '13.2');
        INSERT testEYDiscountMiddleEastObj;
        
        EY_Discount__c testEYDiscountObj = TestDataFactory.createTestEYDiscount(testContractObj.Id, '1', false, '13.2');
        INSERT testEYDiscountObj;
        
        EY_Discount_ASIA__c testEYDiscountAsiaObj = TestDataFactory.createTestEYDiscountAsia(testContractObj.Id, '1', false, '13.2');
        INSERT testEYDiscountAsiaObj;
        
        Test.startTest();
        InternationalDiscountController lController = getInternationalDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newAsiaPageReference = lController.newAsia();
        PageReference newInterUKEURPageReference = lController.newInterUKEUR();
        PageReference newMiddleEPageReference = lController.newMiddleE();
        PageReference newAfricaPageReference = lController.newAfrica();
        PageReference savePageReference = lController.save();
        PageReference qsavePageReference = lController.qsave();
        PageReference cancelPageReference = lController.cancel();
        test.stopTest();
        //Asserts
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('There is already a Discount table been applied for EY, You cannot select an EY discount and an SQ discount on the same contract, Please delete the EY discount table if you require SQ Discounts!', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.ERROR, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testInternationalDiscountControllerApexMessage() {
        Contract testContractObj = [SELECT Id, istemplate__c FROM Contract];
        testContractObj.istemplate__c = false;
        testContractObj.VA_SQ_Requested_Tier__c = null;
        UPDATE testContractObj;
        
        Test.startTest();
        InternationalDiscountController lController = getInternationalDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newAsiaPageReference = lController.newAsia();
        PageReference newInterUKEURPageReference = lController.newInterUKEUR();
        PageReference newMiddleEPageReference = lController.newMiddleE();
        PageReference newAfricaPageReference = lController.newAfrica();
        PageReference savePageReference = lController.save();
        PageReference qsavePageReference = lController.qsave();
        PageReference cancelPageReference = lController.cancel();
        test.stopTest();
        //Asserts
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('A VA/SQ Tier has not been Selected in the contract so you cannot add a discount', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.ERROR, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testInternationalDiscountControllerWithRelatedRecords() {
        Contract testContractObj = [SELECT Id FROM Contract];
        International_Discounts_UK_EUROPE__c testInternationalDiscountsUKEUROPEObj = TestDataFactory.createTestInternationalDiscountsUKEUROPE(testContractObj.Id, '1', false, '13.2');
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_Eligible_Fare_Type__c = 'Business';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_Eligible_Fare_Type2__c = 'Business';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_Eligible_Fare_Type3__c = 'Business Saver';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_Eligible_Fare_Type4__c = 'Premium Economy';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_Eligible_Fare_Type5__c = 'Premium Economy';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_Eligible_Fare_Type6__c = 'Economy';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_EligibleBookingClass__c = 'J';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_EligibleBookingClass2__c = 'C';              
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_EligibleBookingClass3__c = 'D';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_EligibleBookingClass4__c = 'W';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_EligibleBookingClass5__c = 'R';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_EligibleBookingClass6__c = 'Y/B/H/K/L/E';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEUAplicableRoutesBNE_SYD_MELto__c = 'UK/Europe';
        testInternationalDiscountsUKEUROPEObj.IntlDiscount_UK_EU_Exists__c = True;
        testInternationalDiscountsUKEUROPEObj.Cos_Version__c = '11.2';
        testInternationalDiscountsUKEUROPEObj.Is_Template__c = true;
        testInternationalDiscountsUKEUROPEObj.Tier__c = '1';
        INSERT testInternationalDiscountsUKEUROPEObj;
        
        Contract_Calculation_Discount_Levels__c testContractCalculationDiscountLevelsObj = TestDataFactory.createTestContractCalculationDiscountLevels(2.2, 2.2, 2.1, 'SQ', 'International Africa', 1.0);
        testContractCalculationDiscountLevelsObj.Flexi_Percentage_B_Class__c = 2.2;
        testContractCalculationDiscountLevelsObj.Flexi_Percentage_H_Class__c = 1.0;
        testContractCalculationDiscountLevelsObj.Flexi_Percentage_K_Class__c = 5.0;
        testContractCalculationDiscountLevelsObj.Flexi_Percentage_L_Class__c = 5.0;
        testContractCalculationDiscountLevelsObj.Premium_Economy_W_Class__c = 5.0;
        testContractCalculationDiscountLevelsObj.Premium_Saver_R_Class__c = 3.0;
        testContractCalculationDiscountLevelsObj.Saver_Percentage_E_Class__c = 2.0;
        testContractCalculationDiscountLevelsObj.Saver_Percentage_N_Class__c = 3.0;
        testContractCalculationDiscountLevelsObj.Saver_Percentage_Q_Class__c = 3.0;
        testContractCalculationDiscountLevelsObj.Saver_Percentage_T_Class__c = 2.0;
        testContractCalculationDiscountLevelsObj.Saver_Percentage_V_Class__c = 1.0;
        INSERT testContractCalculationDiscountLevelsObj;
        
        Cost_of_Sale__c testCostOfSaleObj = TestDataFactory.createTestCostOfSale(testContractObj.Id);
        INSERT testCostOfSaleObj;
        
        International_Discounts_Middle_East__c testInternationalDiscountsMiddleEastObj = TestDataFactory.createTestInternationalDiscountsMiddleEast(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsMiddleEastObj;
        
        International_Discounts_Africa__c testInternationalDiscountsAfricaObj = TestDataFactory.createTestInternationalDiscountsAfrica(testContractObj.Id, '1', false, '13.2');
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare__c = 2;
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare2__c = 1;
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare3__c = 3;
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare6__c = 4;
        INSERT testInternationalDiscountsAfricaObj;
        
        International_Discounts_Asia__c testInternationalDiscountsAsiaObj = TestDataFactory.createTestInternationalDiscountsAsia(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsAsiaObj;
        Test.startTest();
        InternationalDiscountController lController = getInternationalDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newAsiaPageReference = lController.newAsia();
        PageReference newInterUKEURPageReference = lController.newInterUKEUR();
        PageReference newMiddleEPageReference = lController.newMiddleE();
        PageReference newAfricaPageReference = lController.newAfrica();
        PageReference savePageReference = lController.save();
        PageReference qsavePageReference = lController.qsave();
        PageReference cancelPageReference = lController.cancel();
        PageReference remUKEURPageReference = lController.remUKEUR();
        PageReference remMiddleEPageReference = lController.remMiddleE();
        PageReference remAsiaEPageReference = lController.remAsia();
        PageReference remAfricaEPageReference = lController.remAfrica();
        test.stopTest();
        //Asserts
        system.assertEquals(1, lController.asiaFlag);
        system.assertEquals(1, lController.ukeurFlag);
        system.assertEquals(1, lController.mideFlag);
        system.assertEquals(1, lController.africaFlag);
        system.assertEquals('/apex/InternationalDiscountPage?id=' + testContractObj.Id, newAsiaPageReference.getUrl());
        system.assertEquals('/' + testContractObj.Id, savePageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountPage?id=' + testContractObj.Id, qsavePageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountPage?id=' + testContractObj.Id, newInterUKEURPageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountPage?id=' + testContractObj.Id, newMiddleEPageReference.getUrl());
        system.assertEquals('/' + testContractObj.Id, cancelPageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountPage?id=' + testContractObj.Id, remUKEURPageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountPage?id=' + testContractObj.Id, remMiddleEPageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountPage?id=' + testContractObj.Id, remAsiaEPageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountPage?id=' + testContractObj.Id, newAfricaPageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountPage?id=' + testContractObj.Id, remAfricaEPageReference.getUrl());
    }
    
    @isTest
    private static void testInternationalDiscountControllerAfricaMS() {
        Contract testContractObj = [SELECT Id, Cos_Version__c, VA_SQ_Requested_Tier__c, Africa_MS__c, Middle_East_MS__c, 
                                    Asia_MS__c, UK_Europe_Revenue_Target__c, Middle_East_Revenue_Target__c, UK_Europe_MS__c, 
                                    AFRICA_Spend__c, Asia_Revenue_Target__c, Africa_revenue_Target__c,
                                    Middle_East_Amount__c, UK_Europe_Amount__c, Asia_Expenditure__c FROM Contract];
        testContractObj.Cos_Version__c = '11.2';
        testContractObj.VA_SQ_Requested_Tier__c = '1';
        testContractObj.UK_Europe_Revenue_Target__c = 0;
        testContractObj.Middle_East_Revenue_Target__c = 0;
        testContractObj.Asia_Revenue_Target__c = 0;
        testContractObj.Africa_revenue_Target__c = 0;
        testContractObj.Africa_MS__c = 100;
        testContractObj.Asia_MS__c = 200;
        testContractObj.Middle_East_MS__c = 300;
        testContractObj.UK_Europe_MS__c = 400;
        testContractObj.AFRICA_Spend__c = 3000;
        testContractObj.Middle_East_Amount__c = 4000;
        testContractObj.UK_Europe_Amount__c = 5000;
        testContractObj.Asia_Expenditure__c = 10;
        UPDATE testContractObj;
        
        List<Contract_Calculation_Discount_Levels__c> testContractCalculationDiscountLevels = new List<Contract_Calculation_Discount_Levels__c>();
        testContractCalculationDiscountLevels.add(TestDataFactory.createTestContractCalculationDiscountLevels(2.2, 2.2, 2.1, 'SQ', 'International Middle East', 1.0));
        testContractCalculationDiscountLevels.add(TestDataFactory.createTestContractCalculationDiscountLevels(2.2, 2.2, 2.1, 'SQ', 'International Asia', 1.0));
        testContractCalculationDiscountLevels.add(TestDataFactory.createTestContractCalculationDiscountLevels(2.2, 2.2, 2.1, 'SQ', 'International Africa', 1.0));
        testContractCalculationDiscountLevels.add(TestDataFactory.createTestContractCalculationDiscountLevels(2.2, 2.2, 2.1, 'SQ', 'International UK/Europe', 1.0));
        INSERT testContractCalculationDiscountLevels;
        
        Cost_of_Sale__c testCostOfSaleObj = TestDataFactory.createTestCostOfSale(testContractObj.Id);
        INSERT testCostOfSaleObj;
        
        Test.startTest();
        InternationalDiscountController lController = getInternationalDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        
        International_Discounts_Asia__c testInternationalDiscountsAsiaObj = TestDataFactory.createTestInternationalDiscountsAsia(testContractObj.Id, '1', false, '13.2');
        testInternationalDiscountsAsiaObj.Cos_Version__c = '11.2';
        testInternationalDiscountsAsiaObj.Is_Template__c = true;
        testInternationalDiscountsAsiaObj.Tier__c = '1';
        testInternationalDiscountsAsiaObj.IntlDisc_Asia_DiscOffPublishFare__c = 2;
        testInternationalDiscountsAsiaObj.IntlDisc_Asia_DiscOffPublishFare2__c = 2;
        testInternationalDiscountsAsiaObj.IntlDisc_Asia_DiscOffPublishFare3__c = 2;
        testInternationalDiscountsAsiaObj.IntlDisc_Asia_DiscOffPublishFare6__c = 2;
        INSERT testInternationalDiscountsAsiaObj;
        PageReference newAsiaPageReference = lController.newAsia();
        
        International_Discounts_UK_EUROPE__c testInternationalDiscountsUKEUROPEObj = TestDataFactory.createTestInternationalDiscountsUKEUROPE(testContractObj.Id, '1', false, '13.2');
        testInternationalDiscountsUKEUROPEObj.Cos_Version__c = '11.2';
        testInternationalDiscountsUKEUROPEObj.Is_Template__c = true;
        testInternationalDiscountsUKEUROPEObj.Tier__c = '1';
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_DiscOffPublishedFare__c = 2;
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_DiscOffPublishedFare2__c = 3;
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_DiscOffPublishedFare3__c = 4;
        testInternationalDiscountsUKEUROPEObj.IntlDiscUKEU_DiscOffPublishedFare6__c = 5;
        INSERT testInternationalDiscountsUKEUROPEObj;        
        PageReference newInterUKEURPageReference = lController.newInterUKEUR();
        
        International_Discounts_Middle_East__c testInternationalDiscountsMiddleEastObj = TestDataFactory.createTestInternationalDiscountsMiddleEast(testContractObj.Id, '1', false, '13.2');
        testInternationalDiscountsMiddleEastObj.Cos_Version__c = '11.2';
        testInternationalDiscountsMiddleEastObj.Is_Template__c = true;
        testInternationalDiscountsMiddleEastObj.Tier__c = '1';
        testInternationalDiscountsMiddleEastObj.Intl_Disc_MiddleEast_DiscOffPubFar__c = 10;
        testInternationalDiscountsMiddleEastObj.Intl_Disc_MiddleEast_DiscOffPubFar2__c = 10;
        testInternationalDiscountsMiddleEastObj.Intl_Disc_MiddleEast_DiscOffPubFar3__c = 10;
        testInternationalDiscountsMiddleEastObj.Intl_Disc_MiddleEast_DiscOffPubFar6__c = 10;
        INSERT testInternationalDiscountsMiddleEastObj;
        PageReference newMiddleEPageReference = lController.newMiddleE();
        
        International_Discounts_Africa__c testInternationalDiscountsAfricaObj = TestDataFactory.createTestInternationalDiscountsAfrica(testContractObj.Id, '1', false, '13.2');
        testInternationalDiscountsAfricaObj.Cos_Version__c = '11.2';
        testInternationalDiscountsAfricaObj.Is_Template__c = true;
        testInternationalDiscountsAfricaObj.Tier__c = '1';
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare__c = 2;
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare2__c = 2;
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare3__c = 2;
        testInternationalDiscountsAfricaObj.IntlDisc_Africa_DiscOffPublishFare6__c = 2;
        INSERT testInternationalDiscountsAfricaObj;
        PageReference newAfricaPageReference = lController.newAfrica();
        
        PageReference savePageReference = lController.save();
        PageReference qsavePageReference = lController.qsave();
        PageReference cancelPageReference = lController.cancel();
        test.stopTest();
        //Asserts
        Contract updateContractObj = [SELECT Id, SQEUDiscountAvailable__c, SQAsDiscountAvailable__c, SQAfDiscountAvailable__c FROM Contract];
        system.assertEquals(true, updateContractObj.SQEUDiscountAvailable__c);
        system.assertEquals(true, updateContractObj.SQAsDiscountAvailable__c);
        system.assertEquals(true, updateContractObj.SQAfDiscountAvailable__c);
    }
    
    private static InternationalDiscountController getInternationalDiscountControllerObj(Contract testContractObj) {
        PageReference pageRef = Page.InternationalDiscountPage;
        pageRef.getParameters().put('id', testContractObj.id);
        Test.setCurrentPage(pageRef);
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        return new InternationalDiscountController(conL);
    }
}