public with sharing class InternationalLONDiscountController 
{

    private ApexPages.StandardController controller {get; set;}

	public List<International_Discounts_LON__c> searchResults1 {get;set;}
    public List <Contract> contracts {get;set;}
	public Boolean isTemplate {get;set;}
	public Boolean isTemplateDiscount {get;set;}
	public Boolean redCircle {get;set;}
    public Contract originalContract{get;set;}
    public string recId{get; set;}
    public integer lonFlag {get; set;}
    
    private Contract a; 
    
    public InternationalLONDiscountController (ApexPages.StandardController myController) 
	{
		a=(Contract)myController.getrecord();
        lonFlag=0;
        recId = ApexPages.currentPage().getParameters().get('Id');
    }
    
    public PageReference initDisc() 
	{  
        contracts=[select id,Cos_Version__c,Red_Circle__c,
                   VA_VS_Requested_Tier__c,IsTemplate__c                   
                   from Contract where id =:ApexPages.currentPage().getParameters().get('id')];
        
		originalContract = contracts.get(0);
		isTemplateDiscount = originalContract.IsTemplate__c;
		redCircle = originalContract.Red_Circle__c;
        
         searchResults1= [
		                 SELECT Cos_Version__c,Id,Is_Template__c,Region__c,DiscOffPublishFare_01__c,
                         DiscOffPublishFare_02__c,DiscOffPublishFare_03__c,DiscOffPublishFare_04__c,
                         DiscOffPublishFare_05__c,DiscOffPublishFare_06__c,DiscOffPublishFare_07__c,
                         DiscOffPublishFare_08__c,DiscOffPublishFare_09__c,DiscOffPublishFare_10__c,
                         DiscOffPublishFare_11__c,DiscOffPublishFare_12__c,DiscOffPublishFare_13__c,
                         DiscOffPublishFare_14__c,DiscOffPublishFare_15__c,DiscOffPublishFare_16__c,
                         Tier__c FROM International_Discounts_LON__c
                         WHERE LookUpToContract__r.id = :ApexPages.currentPage().getParameters().get('id')                         
                         
                       ];
        
        if(searchResults1.size()>0)
		{
			lonFlag=1;
		}
        
        return null;
    }

    public PageReference save()
    {
        upsert searchResults1;
        PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));		
		thePage.setRedirect(true);		
		return thePage;
    }

    public PageReference qsave()
   {
	   upsert searchResults1;
        return ApexPages.currentPage();
   }
    
    public PageReference cancel() 
    {
		// return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
		PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
		//
		thePage.setRedirect(true);
		//
		return thePage;

	 }  
    
    public PageReference newLONDiscounts() 
	{
        if (searchResults1.size()<1)
		{

			Boolean returnError = checkDiscounts();

			if ( returnError == true && originalContract.IsTemplate__c ==false){
				return null;
			}

			isTemplate =false;


			List<International_Discounts_LON__c> templateLONList = new List<International_Discounts_LON__c>();

			International_Discounts_LON__c templateLON = new International_Discounts_LON__c();

			International_Discounts_LON__c LONDiscount = new International_Discounts_LON__c();
           
			templateLONList = [ SELECT Cos_Version__c,Id,Is_Template__c,Region__c,DiscOffPublishFare_01__c,
                                DiscOffPublishFare_02__c,DiscOffPublishFare_03__c,DiscOffPublishFare_04__c,
                                DiscOffPublishFare_05__c,DiscOffPublishFare_06__c,DiscOffPublishFare_07__c,
                                DiscOffPublishFare_08__c,DiscOffPublishFare_09__c,DiscOffPublishFare_10__c,
                                DiscOffPublishFare_11__c,DiscOffPublishFare_12__c,DiscOffPublishFare_13__c,
                                DiscOffPublishFare_14__c,DiscOffPublishFare_15__c,DiscOffPublishFare_16__c,
                                Tier__c FROM International_Discounts_LON__c 
                                WHERE  Is_Template__c = true
                                and Tier__c = :originalContract.VA_VS_Requested_Tier__c
                                and  Cos_Version__c = :originalContract.Cos_Version__c
			                  ];

			if(templateLONList.size() > 0 )
                                                {
				templateLON=templateLONList.get(0);
				istemplate = false;

			}else
                                               {
				isTemplate = true;

			}


			if(isTemplate == true){

				LONDiscount.LookUpToContract__c = ApexPages.currentPage().getParameters().get('id');
				LONDiscount.DiscOffPublishFare_01__c = 10;
				LONDiscount.DiscOffPublishFare_02__c = 10;
				LONDiscount.DiscOffPublishFare_03__c = 10;
				LONDiscount.DiscOffPublishFare_04__c = 10;
				LONDiscount.DiscOffPublishFare_05__c = 10;
				LONDiscount.DiscOffPublishFare_06__c = 10;
				LONDiscount.DiscOffPublishFare_07__c = 10;
				LONDiscount.DiscOffPublishFare_08__c = 10;
				LONDiscount.DiscOffPublishFare_09__c = 10;
				LONDiscount.DiscOffPublishFare_10__c = 10;
				LONDiscount.DiscOffPublishFare_11__c = 10;
				LONDiscount.DiscOffPublishFare_12__c = 10;
				LONDiscount.DiscOffPublishFare_13__c= 10;				
				LONDiscount.DiscOffPublishFare_14__c = 10;
                LONDiscount.DiscOffPublishFare_15__c = 10;
                LONDiscount.DiscOffPublishFare_16__c = 10;
               
                
             
			}else{

				LONDiscount.LookUpToContract__c  =    ApexPages.currentPage().getParameters().get('id');

				LONDiscount.DiscOffPublishFare_01__c = templateLON.DiscOffPublishFare_01__c;
				LONDiscount.DiscOffPublishFare_02__c = templateLON.DiscOffPublishFare_02__c;
				LONDiscount.DiscOffPublishFare_03__c = templateLON.DiscOffPublishFare_03__c;
				LONDiscount.DiscOffPublishFare_04__c = templateLON.DiscOffPublishFare_04__c;
				LONDiscount.DiscOffPublishFare_05__c = templateLON.DiscOffPublishFare_05__c;
				LONDiscount.DiscOffPublishFare_06__c = templateLON.DiscOffPublishFare_06__c;
				LONDiscount.DiscOffPublishFare_07__c = templateLON.DiscOffPublishFare_07__c;
				LONDiscount.DiscOffPublishFare_08__c = templateLON.DiscOffPublishFare_08__c;
				LONDiscount.DiscOffPublishFare_09__c = templateLON.DiscOffPublishFare_09__c;
				LONDiscount.DiscOffPublishFare_10__c = templateLON.DiscOffPublishFare_10__c;
				LONDiscount.DiscOffPublishFare_11__c = templateLON.DiscOffPublishFare_11__c;
				LONDiscount.DiscOffPublishFare_12__c = templateLON.DiscOffPublishFare_12__c;
				LONDiscount.DiscOffPublishFare_13__c = templateLON.DiscOffPublishFare_13__c;
                LONDiscount.DiscOffPublishFare_14__c = templateLON.DiscOffPublishFare_14__c;
                LONDiscount.DiscOffPublishFare_15__c = templateLON.DiscOffPublishFare_15__c;
                LONDiscount.DiscOffPublishFare_16__c = templateLON.DiscOffPublishFare_16__c;
                LONDiscount.Tier__c = templateLON.Tier__c;
				LONDiscount.Cos_Version__c = templateLON.Cos_Version__c;
			}
			   searchResults1.add(LONDiscount);
            
               List<Contract> contractlist = new List<Contract>();
               Contract contract = new Contract();
               contract = [Select LONDiscountAvailable__c From Contract where id = :a.id ];
               contract.LONDiscountAvailable__c = TRUE ;
               update contract;
		       return null;
		}
		else{


			return ApexPages.currentPage();
		}
    } 
    
    public PageReference remLONDiscounts() 
	{
        International_Discounts_LON__c toDelete = searchResults1[searchResults1.size()-1];

		delete toDelete;
        
        List<Contract> contractlist = new List<Contract>();
        Contract contract = new Contract();
        contract = [Select LONDiscountAvailable__c From Contract where id = :a.id ];
        contract.LONDiscountAvailable__c = false ;
        update contract;

		return ApexPages.currentPage();
    }
    
    public Boolean checkDiscounts()
    {
	
        if(originalContract.VA_VS_Requested_Tier__c == null){
			ApexPages.addMessage( new ApexPages.message( ApexPages.severity.ERROR,
			'A VA/VS Tier has not been Selected in the contract so you cannot add a discount' ));
			return true;
		}

		return false;
    }
}