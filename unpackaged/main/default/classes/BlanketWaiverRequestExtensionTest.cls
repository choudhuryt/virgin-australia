/**
 * @author: Joy S. Marasigan
 * @createdDate: April 18, 2016
 * @description: Test class for BlanketWaiverRequestExtension.cls
 * Please see Case #00101013 for more information regarding the requirements
 **/
 
@isTest
public class BlanketWaiverRequestExtensionTest {
    
    @testSetup static void setUpData(){
        
        Blanket_Waiver_Request_Settings__c bwrSettings = Blanket_Waiver_Request_Settings__c.getOrgDefaults();
        bwrSettings.Name = 'BWR Original Count';
        bwrSettings.BWR_Original_Count__c = 0;
        
        insert bwrSettings;
        
    }
    
    //check records with valid input
    static testMethod void checkValidRecords(){
        
        String bwrOrigCount = String.valueOf(Integer.valueOf(Blanket_Waiver_Request_Settings__c.getValues('BWR Original Count').BWR_Original_Count__c) + 1); 
        
        Test.startTest();
        
        Blanket_Waiver_Request__c newBwr = new Blanket_Waiver_Request__c(
                                                                                Waiver_Type__c = 'Airport related disrupts',
                                                                                Waiver_Group__c = 'Commercial',
                                                                                Business_Justification__c = 'New Record Test',
                                                                                Blanket_Waiver_Start_Date__c = Date.parse('14/04/2016'),
                                                                                Blanket_Waiver_End_Date__c = Date.parse('24/04/2016')
                                                                            );
                                                                        
        ApexPages.StandardController sCon = new ApexPages.StandardController(newBwr);
        BlanketWaiverRequestExtension bwrExt = new BlanketWaiverRequestExtension(sCon);
        
        PageReference savedBwr = bwrExt.saveVersion();
        
        Blanket_Waiver_Request__c allBwr = [SELECT Id, Business_Justification__c, Unique_Waiver_Code__c,
                                                    Submission_Counter__c, Blanket_Waiver_Start_Date__c, Blanket_Waiver_End_Date__c
                                                    FROM Blanket_Waiver_Request__c];
        System.debug(allBwr);
        
        Blanket_Waiver_Request__c existingBwr = [SELECT Id, Business_Justification__c, Unique_Waiver_Code__c,
                                                    Submission_Counter__c, Blanket_Waiver_Start_Date__c, Blanket_Waiver_End_Date__c
                                                    FROM Blanket_Waiver_Request__c
                                                    WHERE (Unique_Waiver_Code__c = 'BW000001')
                                                ];
        
        System.assertEquals(existingBwr.Submission_Counter__c, 1);
        System.assertEquals(existingBwr.Unique_Waiver_Code__c, 'BW' + bwrOrigCount.leftPad(BlanketWaiverRequestExtension.idLength).replace(' ', '0'));
        
        Test.setCurrentPageReference(Page.NewBWRVersion);
        
        ApexPages.currentPage().getParameters().put('id',existingBwr.Id);
        
        Blanket_Waiver_Request__c newBwrVersion = new Blanket_Waiver_Request__c();
      	
        sCon = new ApexPages.StandardController(newBwrVersion);
        bwrExt = new BlanketWaiverRequestExtension(sCon);
        
        bwrExt.bwrNewVersion.Waiver_Type__c = 'Airport related disrupts';
        bwrExt.bwrNewVersion.Waiver_Group__c = 'Commercial';
        bwrExt.bwrNewVersion.Business_Justification__c = 'New Version Test';
        bwrExt.bwrNewVersion.Blanket_Waiver_Start_Date__c = Date.parse('14/04/2016');
        bwrExt.bwrNewVersion.Blanket_Waiver_End_Date__c = Date.parse('24/05/2016');
        
        savedBwr = bwrExt.saveVersion();
        
        Test.stopTest();
        
        Blanket_Waiver_Request__c savedBwrRecord = [SELECT Business_Justification__c, Unique_Waiver_Code__c, Submission_Counter__c,
                                                        Blanket_Waiver_Start_Date__c, Blanket_Waiver_End_Date__c,
                                                    	Replicated_From__c
                                                        FROM Blanket_Waiver_Request__c
                                                        WHERE (Waiver_Type__c = 'Airport related disrupts'
                                                        AND Waiver_Group__c = 'Commercial'
                                                        AND Submission_Counter__c != 1)
                                                    ];
                                                    
        System.assertEquals(savedBwrRecord.Submission_Counter__c, existingBwr.Submission_Counter__c + 1);
        System.assertEquals(savedBwrRecord.Unique_Waiver_Code__c, existingBwr.Unique_Waiver_Code__c);
        System.assertEquals(savedBwrRecord.Replicated_From__c, existingBwr.Id);
        
    }
    
    //check records with invalid input
    static testMethod void checkInvalidRecords(){
        
        String bwrOrigCount = String.valueOf(Integer.valueOf(Blanket_Waiver_Request_Settings__c.getValues('BWR Original Count').BWR_Original_Count__c) + 1); 
        
        Test.startTest();
        
        Blanket_Waiver_Request__c newBwr = new Blanket_Waiver_Request__c(
                                                                                Waiver_Type__c = 'Airport related disrupts',
                                                                                Waiver_Group__c = 'Commercial',
                                                                                Business_Justification__c = 'New Record Test',
                                                                                Blanket_Waiver_Start_Date__c = Date.parse('14/04/2016'),
                                                                                Blanket_Waiver_End_Date__c = Date.parse('04/04/2016')
                                                                            );
                                                                        
        ApexPages.StandardController sCon = new ApexPages.StandardController(newBwr);
        BlanketWaiverRequestExtension bwrExt = new BlanketWaiverRequestExtension(sCon);
        
        PageReference savedBwr = bwrExt.saveVersion();
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        
        if(msgs.size() > 0){
            
            System.assert(msgs[0].getDetail().contains('The Blanket Waiver End Date is not valid'));
            
        }
 
    }
}