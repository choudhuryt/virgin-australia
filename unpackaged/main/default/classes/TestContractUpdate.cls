/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestContractUpdate {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Account accountToTest = new Account();
        CommonObjectsForTest objForTest = new CommonObjectsForTest();
        
        accountToTest= objForTest.CreateAccountObject(0);
        //accountToTest.Velocity_Gold_Status_Match_Available__c = null;
       // accountToTest.Velocity_Platinum_Status_Match_Available__c = null;
        accountToTest.Account_Type__c = 'Corporate';
        insert accountToTest;
        
        Contract contractForTEST = new Contract();
        contractForTEST = objForTest.CreateOldStandardContract(0);
      	contractForTEST.AccountId = accountToTest.id;
      	contractForTEST.Gold__c= 10;
      	contractForTEST.Platinum__c =1;
      	contractForTEST.Pilot_Gold__c =1;
      	contractForTEST.Domestic_Short_Haul_Tier__c ='1';
      	contractForTEST.VA_DL_Requested_Tier__c ='1';
      	contractForTEST.VA_NZ_Requested_Tier__c='1';
      	insert contractForTEST;
      	contractForTEST.Status ='Activated';
      	update contractForTEST;
      	
      	//testing with values
      	 Account accountToTest1 = new Account();
        CommonObjectsForTest objForTest1 = new CommonObjectsForTest();
        
        accountToTest1= objForTest.CreateAccountObject(0);
        accountToTest1.Velocity_Gold_Status_Match_Available__c = 1;
        accountToTest1.Velocity_Platinum_Status_Match_Available__c = 1;
        accountToTest1.Velocity_Pilot_Gold_Available__c =1;
        accountToTest1.Account_Type__c = 'Corporate';
        insert accountToTest1;
        
        Contract contractForTEST1 = new Contract();
        contractForTEST1 = objForTest.CreateOldStandardContract(0);
      	contractForTEST1.AccountId = accountToTest.id;
      	contractForTEST1.Gold__c= 10;
      	contractForTEST1.Platinum__c =1;
      	contractForTEST1.Pilot_Gold__c =1;
      	contractForTEST1.Domestic_Short_Haul_Tier__c ='1';
      	contractForTEST1.VA_DL_Requested_Tier__c ='1';
      	contractForTEST1.VA_NZ_Requested_Tier__c='1';
      	insert contractForTEST1;
      	contractForTEST1.Status ='Activated';
      	update contractForTEST1;
      	
      	 Contract contractForTEST2 = new Contract();
        contractForTEST2 = objForTest.CreateOldStandardContract(0);
      	contractForTEST2.AccountId = accountToTest.id;
      	contractForTEST2.Gold__c= 10;
      	contractForTEST2.Platinum__c =1;
      	contractForTEST2.Pilot_Gold__c =1;
      	contractForTEST2.Domestic_Short_Haul_Tier__c ='1';
      	insert contractForTEST2;
      	contractForTEST2.Status ='Activated';
      	update contractForTEST2;
      	
    }

    
     public static testMethod void testContractCloning() 
    {
        
         User u = TestUtilityClass.createTestUser();
         Account accountToTest = new Account();
         CommonObjectsForTest objForTest = new CommonObjectsForTest();        
         accountToTest= objForTest.CreateAccountObject(0);        
         accountToTest.Account_Type__c = 'Corporate';
         insert accountToTest;
        
         Contract contractForTEST = new Contract();
         contractForTEST = objForTest.CreateOldStandardContract(0);
      	 contractForTEST.AccountId = accountToTest.id;
         contractForTEST.status = 'Draft' ;
         insert contractForTEST;
         contractForTEST.status =  'Activated';
         update contractForTEST;
        
         Contract contractForTEST1 = new Contract();
         contractForTEST1 = objForTest.CreateOldStandardContract(0);
      	 contractForTEST1.AccountId = accountToTest.id;
         contractForTEST1.Parent_Contract__c = contractForTEST.id ;
         contractForTEST.status = 'Draft' ;
         insert contractForTEST1;
         contractForTEST1.status =  'Activated';
         update contractForTEST1;
    }
    
    public static testMethod void testContractMarketingFund() 
    { 
        User u = TestUtilityClass.createTestUser();
         Account accountToTest = new Account();
         CommonObjectsForTest objForTest = new CommonObjectsForTest();        
         accountToTest= objForTest.CreateAccountObject(0);        
         accountToTest.Account_Type__c = 'Corporate';
         insert accountToTest;
        
         Contract contractForTEST = new Contract();
         contractForTEST = objForTest.CreateOldStandardContract(0);
      	 contractForTEST.AccountId = accountToTest.id;
         contractForTEST.status = 'Draft' ;
         contractForTEST.Fixed_Amount__c = 0;
         insert contractForTEST;
         contractForTEST.status =  'Activated';
         contractForTEST.Fixed_Amount__c = 100;
         update contractForTEST;
        
    }
}