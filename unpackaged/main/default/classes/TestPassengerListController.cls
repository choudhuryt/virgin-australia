@isTest
public class TestPassengerListController{
    
    static testMethod void testConstructor() {
        PassengerListController testObject = new PassengerListController();
    }
    
    static testMethod void testInitController() {
		Test.setMock(WebServiceMock.class, new GetTravelBankServiceMockImpl());        
        Test.startTest();
        PassengerListController testObject = new PassengerListController();
        Case tmpCase = new Case(Flight_Number__c = '123', Flight_Date__c = Date.today());
        insert tmpCase;
        
        Reservation__c tmpReservation = new Reservation__c(Case__c = tmpCase.Id,
                                                           Flight_Number__c = tmpCase.Flight_Number__c,
                                                           Flight_Date__c = tmpCase.Flight_Date__c);
        insert tmpReservation;
        
        String caseId = tmpCase.Id;
        testObject.initController(caseId);
		Test.stopTest();
    }
    
    static testMethod void testPreparePassengerList() {
        SalesForceHistoricalDetailModel.GetPassengerManifestRSType response_x = new SalesForceHistoricalDetailModel.GetPassengerManifestRSType();
        response_x.AirlineCode = 'VN1234';
        response_x.FlightDate = '12/12/2012';
        response_x.FlightNumber = '24';

        SalesForceHistoricalDetailModel.FlightManifestType FlightManifest = new SalesForceHistoricalDetailModel.FlightManifestType();
        SalesForceHistoricalDetailModel.PassengerType[] Passenger = new List<SalesForceHistoricalDetailModel.PassengerType>();
        SalesForceHistoricalDetailModel.PassengerType Passenger1 = new SalesForceHistoricalDetailModel.PassengerType();
        Passenger1.PNRLocator = '111';
        Passenger1.PNRCreatedDate = '12/12/2012';
        Passenger1.FirstName = 'Vu';
        Passenger1.Surname = 'holmes';
        Passenger1.Gender = 'M';
        Passenger1.PNROrigin = 'abc';
        Passenger1.PNRDestination = 'bca';
        Passenger1.ServiceStartDate = '2012-12-12';//date.parse('2012-12-12');
        Passenger1.PriorityListFlag = '1';
        Passenger1.CabinCode = '11';
        Passenger1.SeatRowNumber = '12';
        Passenger1.SeatLetter = 'aaa';
        Passenger1.BoardingPassIssuedFlag = 'abc';
        Passenger1.OnboardFlag = 'aaa';
        Passenger1.NoShowFlag = 'aaa';
        Passenger1.InboundConnectionFlag = 'abc';
        Passenger1.OutboundConnectionFlag = 'ccc';
        //------------
        SalesForceHistoricalDetailModel.CheckInType CheckIn = new SalesForceHistoricalDetailModel.CheckInType();
        CheckIn.CheckInDate = '2012-12-12';//date.parse('12/12/2012');
        CheckIn.CheckInTime = string.valueOf(System.now());
        CheckIn.CheckInGroupCode = 'abc';
        CheckIn.CheckInGroupCount = 1;
        CheckIn.RemoteCheckInFlag = 'aaa';
        CheckIn.MobileCheckInFlag = 'aaa';
        CheckIn.KioskCheckInFlag = 'abc';
        Passenger1.CheckIn = CheckIn;
        
        SalesForceHistoricalDetailModel.FrquentTravellerType FrequentTraveller = new SalesForceHistoricalDetailModel.FrquentTravellerType();
        FrequentTraveller.FrequentTravellerNumber = '111';
        FrequentTraveller.FrequentTravellerTier = '1';
        FrequentTraveller.FrequentTravellerVendorCode = '1111';
        Passenger1.FrequentTraveller = FrequentTraveller;
        
        SalesForceHistoricalDetailModel.BaggageType baggage = new SalesForceHistoricalDetailModel.BaggageType();
        baggage.BagCount = 12;
        baggage.TotalBagWeight = 13;
        Passenger1.Baggage = baggage;

        Passenger.add(Passenger1);
        FlightManifest.Passenger = Passenger;
        response_x.FlightManifest = FlightManifest;
        
        PassengerListController testObject = new PassengerListController();
        List<PassengerListController.PassengerBinding> passengerList = testObject.preparePassengerList(response_x);
        System.assertEquals(1, passengerList.size());
        PassengerListController.PassengerBinding passengerBinding = passengerList.get(0);
        System.assertEquals(response_x.AirlineCode, passengerBinding.AirlineCode);
    }
    
    static testMethod void testSearchPassenger() {
        
        //Test.setMock(WebServiceMock.class, new LoyaltyServiceMockImpl());
        Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());	
        //Test.setMock(WebServiceMock.class, new GetTravelBankServiceMockImpl());
        Test.startTest();
        PassengerListController testObject = new PassengerListController();
        Case tmpCase = new Case(Airline_Organisation_Short__c='VN1234',Flight_Number__c='24',
                                Flight_Date__c=Date.today(), 
                                Velocity_Number__c ='2100865670' );
        insert tmpCase;
        //upsert tmpCase;
        testObject.caseRecord = tmpCase;
        
        //Test.setMock(WebServiceMock.class, new PassengerManifestServiceMockImpl());
        testObject.searchPassenger();
        Test.stopTest();
    }

}