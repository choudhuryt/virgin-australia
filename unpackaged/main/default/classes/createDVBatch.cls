/**
 * @description       : Account Update_DV__c Batch
 * UpdatedBy: cloudwerx (Did indentation)
**/
global class createDVBatch implements Database.Batchable<sObject> {
    List<contract_addendum__c> CAListGlobal = new List<contract_addendum__c>();
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
        string BatchLIMIT= Label.FCR_Batch_Limit; 
        String query;
        //query = 'SELECT Id FROM contract WHERE fcr_processed__c = false AND status not in (\'deactivated\',\'rejected\') LIMIT'+BatchLimit;
        //query = 'SELECT AccountId, id FROM contract WHERE Account.recordtype.name in (\'Corporate\', \'Government\') AND status = \'Activated\' AND Account.Owner.Name not in (\'%accelerate%\', \'Justine Flegler\') AND accountid != \'\' AND fcr_processed__c != true LIMIT'+BatchLimit;
        query = 'SELECT id, Update_DV__c, dv_updated__c FROM account WHERE bulk_updated__c = true AND Update_DV__c = false LIMIT '+BatchLimit;
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<account> accountList) {
        try {
            for (integer i = 0; i < accountList.size(); i++) {
                accountList[i].Update_DV__c = true;
            } 
            UPDATE accountList;
        } catch(Exception e) {
            System.debug(e);
        }
    }   
     
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations like sending email
        if(!Test.isRunningTest())  {
            System.scheduleBatch(new createDVBatch(), 'Create DV Batch Job', 1, 1);
        }   
    }
}