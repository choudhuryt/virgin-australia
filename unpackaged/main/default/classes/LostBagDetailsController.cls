public class LostBagDetailsController{

    public String caseId{set;get;}
    public Lost_Baggage__c LostBaggageItem{set;get;}
    public boolean DisplaySaveButton{set;get;}
    public String NeedClosePage{get;set;}
    public String PIR{get;set;}
    public String RecordType{get;set;}
    

    /*Constructor
    Search Lost Baggage based on case id from url parameter
    */
    public LostBagDetailsController(){
        this.caseId = ApexPages.currentPage().getParameters().get('id');
        this.PIR = ApexPages.currentPage().getParameters().get('pir');
        this.RecordType = ApexPages.currentPage().getParameters().get('recordtype');
        
        DisplaySaveButton = false;
        if(ApexPages.currentPage().getParameters().get('isdetail') != null)
        {
            if(ApexPages.currentPage().getParameters().get('isdetail') == 'true')
            {
                DisplaySaveButton = true;
            }
        }

        NeedClosePage = '';

        List<Lost_Baggage__c> listItem = [SELECT Bag_Weight__c,Case__c,
                                          Colour_Type_Code__c,Contents__c,
                                          Cost__c,CreatedById,CreatedDate,
                                          Damage_Details__c,Date_Created__c,
                                          Date_Delivered__c,Date_Received__c,
                                          Destination_Port__c,Fault_Station__c,
                                          First_Name__c,Flight_Details__c,
                                          Id,IsDeleted,LastModifiedById,
                                          LastModifiedDate,
                                          Last_Seen_Port__c,
                                          Name,
                                          Name_Field__c,Notes__c,
                                          Permanent_Address__c,PIR_Number__c,
                                          PNR_Locator__c,
                                          Reason_for_Loss__c,
                                          Route__c,Search_Date_Time__c,
                                          Surname__c,SystemModstamp,
                                          Tag_Numbers__c,
                                          Temporary_Address__c,
                                          TTY_Messages__c FROM
                                          Lost_Baggage__c
                                         WHERE Lost_Baggage__c.PIR_Number__c = :this.PIR and Case__c = :this.caseId];
        if(listItem.size()>0)
        {
            
            this.LostBaggageItem = listItem[0].clone();
            if(this.LostBaggageItem.PIR_Number__c == null)
            {
               this.LostBaggageItem.PIR_Number__c = this.PIR;
            }

        }
        else
        {
            this.GetValueFromWebservice();
        }
    }
    
    
    public void SaveSearchHistory()
    {
        if(this.LostBaggageItem != null)
        {
            this.LostBaggageItem.Search_Date_Time__c = system.now();
            this.LostBaggageItem.Case__c = this.caseId;
            System.debug(this.caseId);
            upsert this.LostBaggageItem ;
            this.LostBaggageItem = [SELECT Bag_Weight__c,Case__c,Colour_Type_Code__c,Contents__c,Cost__c,CreatedById,CreatedDate,Damage_Details__c,Date_Created__c,Date_Delivered__c,Date_Received__c,Destination_Port__c,Fault_Station__c,First_Name__c,Flight_Details__c,Id,IsDeleted,LastModifiedById,LastModifiedDate,Last_Seen_Port__c,Name,Name_Field__c,Notes__c,Permanent_Address__c,PIR_Number__c,PNR_Locator__c,Reason_for_Loss__c,Route__c,Search_Date_Time__c,Surname__c,SystemModstamp,Tag_Numbers__c,Temporary_Address__c,TTY_Messages__c FROM Lost_Baggage__c
                                    ORDER BY CreatedDate desc
                                    limit 1];
            this.NeedClosePage = '1';
        }
    }

    //Get Lost Baggage info from webservice bsed on search condition from the earch page
    private void GetValueFromWebservice()
    {

        SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService services = new SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService();
        // Retrieve Apex Callout options from Custom Settings
        Apex_Callouts__c apexCallouts = Apex_Callouts__c.getValues('LostBaggageDetails');
        if(apexCallouts != null){
            services.endpoint_x = apexCallouts.Endpoint_URL__c;
            services.timeout_x = Integer.valueOf(apexCallouts.Timeout_ms__c);
            services.clientCertName_x = apexCallouts.Certificate__c;
        }

        try { // Added try/catch to distinguish success response and fault response from weservice callouts
            String recordTypeTmp;
            if(this.RecordType != null)
            {
                if (RecordType.indexOf(' ')>-1)
                    recordTypeTmp = this.RecordType.substring(0,this.RecordType.indexof(' '));
                else recordTypeTmp = RecordType;
            }         
            System.debug('HanhLuu::LostBagDetailController::'+ this.PIR + '::'+ recordTypeTmp);
            //SalesForceHistoricalDetailModel.GetLostBaggageRSType response_x = services.GetLostBaggage('ROKDJ10068', 'AHL');
            SalesForceHistoricalDetailModel.GetLostBaggageRSType response_x = services.GetLostBaggage(this.PIR, recordTypeTmp);
    
            SalesForceHistoricalDetailModel.ColourCodesType ColourCodes;
            Lost_Baggage__c item = new Lost_Baggage__c();
            try
            {
                ColourCodes = response_x.ColourCodes;
                SalesForceHistoricalDetailModel.BagColourCodeType[] BagColourCode = ColourCodes.BagColourCode;
                if( BagColourCode != null && BagColourCode.size() >0)
                {
                    item.Colour_Type_Code__c = BagColourCode[0].TypeCode;
                }
            }
            catch (Exception ex)
            {
               //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
            try
            {
                item.Bag_Weight__c = response_x.BagWeight;
            }
            catch (Exception ex)
            {
                   //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
    
            try
            {
                SalesForceHistoricalDetailModel.BaggageCostsType BaggageCosts = response_x.BaggageCosts;
                SalesForceHistoricalDetailModel.BagCostType[] BagCost = BaggageCosts.BagCost;
                if( BagCost != null && BagCost.size() > 0){
                    item.Cost__c = BagCost[0].CostType;
                }            
            }
            catch (Exception ex)
            {
                   //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
            try
            {
                SalesForceHistoricalDetailModel.ContentsType Contents = response_x.Contents;
                SalesForceHistoricalDetailModel.BagContentType[] BagContent = Contents.BagContent;
                if( BagContent != null && BagContent.size() > 0){
                    item.Contents__c = BagContent[0].Content;
                }
            }
            catch(Exception ex)
            {
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
    
            try
            {
                SalesForceHistoricalDetailModel.DamageDetailsType DamageDetails = response_x.DamageDetails;
                SalesForceHistoricalDetailModel.DamageDetailLineType[] DamageDetailLine = DamageDetails.DamageDetailLine;
                if( DamageDetailLine != null && DamageDetailLine.size() > 0 ){
                     item.Damage_Details__c = DamageDetailLine[0].DamageLocation + ' - ' + DamageDetailLine[0].DamageCode;
                }
            }
            catch(Exception ex)
            {
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
    
            try
            {
                SalesForceHistoricalDetailModel.DeliveryDatesType DeliveryDates = response_x.DeliveryDates;
            SalesForceHistoricalDetailModel.DateDeliveredType[] DateDelivered = DeliveryDates.DateDelivered;
            if( DateDelivered != null && DateDelivered.size() > 0){
                    item.Date_Delivered__c =  date.parse(DateDelivered[0].Date_x);
            }
            }
            catch(Exception ex)
            {
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
            try
            {
                item.Date_Created__c = Utilities.convertStringToDate(response_x.DateCreated);
                item.Date_Received__c = Utilities.convertStringToDate(response_x.DateReceived);
                item.Destination_Port__c = response_x.DestinationPort;
                item.Fault_Station__c = response_x.FaultStation;
                item.First_Name__c = response_x.GuestName;
                item.Surname__c = response_x.GuestName;
            }
            catch(Exception ex)
            {
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
            try
            {
                SalesForceHistoricalDetailModel.FlightDetailsType FlightDetails = response_x.FlightDetails;
                SalesForceHistoricalDetailModel.FlightLegDetailType[] FligthLegDetail = FlightDetails.FligthLegDetail;
                if(FligthLegDetail != null && FligthLegDetail.size() > 0){
                    item.Flight_Details__c = FligthLegDetail[0].AirlineCode + ' - ' + FligthLegDetail[0].FlightNumber;
            }
            }
            catch (Exception ex)
            {
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
            try
            {
                SalesForceHistoricalDetailModel.NotesType Notes = response_x.Notes;
                SalesForceHistoricalDetailModel.NoteType[] Note = Notes.Note;
                if( Note != null &&  Note.size() > 0){
                    item.Notes__c = Note[0].NoteText;
                }
            }
             catch (Exception ex)
            {
               // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
            try
            {
                SalesForceHistoricalDetailModel.WorldTracerAddressType PermanentAddress = response_x.PermanentAddress;
                item.Permanent_Address__c = PermanentAddress.AddressLine1 + ' - ' +PermanentAddress.AddressLine2;
                SalesForceHistoricalDetailModel.WorldTracerAddressType TemporaryAddress = response_x.TemporaryAddress;
                item.Temporary_Address__c = TemporaryAddress.AddressLine1 + ' - ' + TemporaryAddress.AddressLine2;
                item.PIR_Number__c = response_x.PIRNumber;
                item.PNR_Locator__c = response_x.PNRLocatorCode;
                item.Reason_for_Loss__c = response_x.ReasonForLossCode;
            }
             catch (Exception ex)
            {
               // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
            try
            {
                SalesForceHistoricalDetailModel.RoutesType Routes = response_x.Routes;
                SalesForceHistoricalDetailModel.RouteType[] Route = Routes.Route;
                if(Route != null && Route.size() > 0){
                    item.Route__c = Route[0].AirportCode;
                }
            }
             catch (Exception ex)
            {
               // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
    
            try
            {
                SalesForceHistoricalDetailModel.BagTagNumbersType BagTagNumbers = response_x.BagTagNumbers;
                SalesForceHistoricalDetailModel.BagTagNumberType[] BagTagNumber = BagTagNumbers.BagTagNumber;
                if( BagTagNumber != null && BagTagNumber.size() > 0){
                    item.Tag_Numbers__c = BagTagNumber[0].TagNumber;
                }
            }
             catch (Exception ex)
            {
               // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
    
            try
            {
                SalesForceHistoricalDetailModel.TTYMessagesType TTYMessages = response_x.TTYMessages;
                SalesForceHistoricalDetailModel.TTYMessageType[] TTYMessage = TTYMessages.TTYMessage;
                if(TTYMessage != null && TTYMessage.size() > 0){
                    item.TTY_Messages__c = TTYMessage[0].TTYMessageText;
            }
            }
            catch (Exception ex)
            {
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
            item.Last_Seen_Port__c = response_x.LastSeenPort;
            item.Case__c = this.caseId;
            if(item.PIR_Number__c == null)
            {
                item.PIR_Number__c = this.PIR;
            }
            this.LostBaggageItem = item;
        }
        catch (System.CalloutException e) {
            String error = e.getMessage();
            if (String.isNotEmpty(error) && String.isNotBlank(error.substringAfter('SOAP Fault:').substringBefore('faultcode'))) {
                error = error.substringAfter('SOAP Fault:').substringBefore('faultcode');
            } else {
                error = 'Communication Error';
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
        }
    }
}