public class PaymentBatch implements Database.Batchable<SObject> {
    public Integer month;
    public Integer year;
    public Date startDate, endDate;

    public PaymentBatch() {
        this(System.now().month(), System.now().year());
    }

    public PaymentBatch(Integer month, Integer year) {
        this.month = month;
        this.year = year;
        this.startDate = Date.newInstance(year - 1, month, 1);
        this.endDate = this.startDate.addYears(1).addDays(-1);
    }

    public Iterable<SObject> start(Database.BatchableContext BC) {
        List<Payments__c> payments = [
                SELECT Id, Paid__c
                FROM Payments__c
                WHERE Contract__r.RecordType.DeveloperName IN ('Accelerate_POS_Rebate', 'NZ_Accelerate_POS_Rebate')
                AND Contract__r.Account.RecordType.DeveloperName IN ('Accelerate', 'SmartFly', 'SmartFlyErr')
                AND Start_Date__c = :startDate
                AND Contract__r.Account.Account_Lifecycle__c = 'Contract'
                AND Contract__r.Status = 'Activated'
        ];
        for (Payments__c p : payments) {
            if (p.Paid__c) {
                throw new UnexpectedException('Batch has payments which are already paid');
            }
        }

        delete payments;

        return (Iterable<SObject>) [
                SELECT Id, AccountId, Account.Corporate_Travel_Bank_Account_Number__c, Account.Corporate_ID__c
                FROM Contract
                WHERE RecordType.DeveloperName IN ('Accelerate_POS_Rebate', 'NZ_Accelerate_POS_Rebate')
                AND Account.RecordType.DeveloperName IN ('Accelerate', 'SmartFly', 'SmartFlyErr')
                AND CALENDAR_MONTH(StartDate) = :month AND CALENDAR_YEAR(StartDate) < :year
                AND Account.Account_Lifecycle__c = 'Contract'
                AND Status = 'Activated'
        ];
    }

    public void execute(Database.BatchableContext BC, List<SObject> sos) {
        Set<Id> ids = new Set<Id>();
        Map<Id, Id> accountMap = new Map<Id, Id>();
        Map<Id, String> rtbNumberMap = new Map<Id, String>();
        Map<Id, String> rtbUserMap = new Map<Id, String>();
        for (SObject so : sos) {
            ids.add((Id) so.get('Id'));
            accountMap.put((Id) so.get('Id'), (Id) so.get('AccountId'));
            rtbNumberMap.put((Id) so.get('AccountId'), (String) so.getSObject('Account').get('Corporate_Travel_Bank_Account_Number__c'));
            rtbUserMap.put((Id) so.get('AccountId'), (String) so.getSObject('Account').get('Corporate_ID__c'));
        }

        List<Spend__c> spends = [
                SELECT Id, Account__c, Account__r.RecordType.DeveloperName, Contract__c, Category__c, Spend__c, Eligible_Spend__c
                FROM Spend__c
                WHERE Contract__c IN :ids
                AND Account__r.RecordType.DeveloperName IN ('Accelerate', 'SmartFly', 'SmartFlyErr')
                AND Spend_Start_Date__c >= :startDate
                AND Spend_Start_Date__c <= :endDate
        ];

        Map<String, Decimal> totals = new Map<String, Decimal>();
        Set<Id> acclerateSet = new Set<Id>();
        for (Spend__c spend : spends) {
            String key1 = spend.Contract__c + '_TE';
            String key2 = spend.Contract__c + '_TEE';
            String key3 = spend.Contract__c + '_' + spend.Category__c;

            if (!totals.containsKey(key1))
                totals.put(key1, 0);
            if (!totals.containsKey(key2))
                totals.put(key2, 0);
            if (!totals.containsKey(key3))
                totals.put(key3, 0);

            totals.put(key1, totals.get(key1) + spend.Spend__c);
            totals.put(key2, totals.get(key2) + spend.Eligible_Spend__c);
            totals.put(key3, totals.get(key3) + spend.Eligible_Spend__c);

            if (spend.Account__r.RecordType.DeveloperName == 'Accelerate')
                acclerateSet.add(spend.Contract__c);
        }

        List<Payments__c> payments = new List<Payments__c>();
        for (Id cid : ids) {
            if (totals.get(cid + '_TE') >= 20000) {
                Payments__c payment = new Payments__c();
                payment.Account__c = accountMap.get(cid);
                payment.Contract__c = cid;
                payment.Start_Date__c = startDate;
                payment.End_Date__c = endDate;
                payment.Reward_Travel_Bank_Number__c = rtbNumberMap.get(cid);
                payment.Reward_Travel_Bank_User_Name__c = rtbUserMap.get(cid);
                payment.Total_Expenditure__c = totals.get(cid + '_TE');
                payment.Total_Eligible_Expenditure__c = totals.get(cid + '_TEE');

                payment.Domestic_Eligible_Expenditure__c = totals.containsKey(cid + '_D') ? totals.get(cid + '_D') : 0;
                payment.Intl_Eligible_Expenditure__c = (totals.containsKey(cid + '_L') ? totals.get(cid + '_L') : 0) + (totals.containsKey(cid + '_S') ? totals.get(cid + '_S') : 0);
                payment.TT_Eligible_Expenditure__c = totals.containsKey(cid + '_T') ? totals.get(cid + '_T') : 0;

                if (payment.Total_Expenditure__c >= 20000 && payment.Total_Expenditure__c <= 49999.99) {
                    payment.Rebate_Percentage__c = 0.02;
                } else if (payment.Total_Expenditure__c >= 50000 && payment.Total_Expenditure__c <= 99999.99) {
                    payment.Rebate_Percentage__c = 0.03;
                } else if (payment.Total_Expenditure__c >= 100000 && payment.Total_Expenditure__c <= 149999.99) {
                    payment.Rebate_Percentage__c = 0.035;
                } else if (payment.Total_Expenditure__c >= 150000 && payment.Total_Expenditure__c <= 199999.99) {
                    payment.Rebate_Percentage__c = 0.04;
                } else if (payment.Total_Expenditure__c >= 200000 && payment.Total_Expenditure__c <= 249999.99) {
                    payment.Rebate_Percentage__c = 0.045;
                } else if (payment.Total_Expenditure__c >= 250000) {
                    payment.Rebate_Percentage__c = 0.05;
                }

                if (acclerateSet.contains(cid)) {
                    payment.Payment_Type__c = 'Sales Payment';
                    payment.GST__c = (payment.Domestic_Eligible_Expenditure__c * payment.Rebate_Percentage__c) / 11;
                    payment.Payment_Earned_On_Domestic__c = (payment.Domestic_Eligible_Expenditure__c * payment.Rebate_Percentage__c) - payment.GST__c;
                    payment.Payment_Earned_On_Intl__c = (payment.Intl_Eligible_Expenditure__c + payment.TT_Eligible_Expenditure__c) * payment.Rebate_Percentage__c;
                    payment.Payment_Amount__c = payment.Payment_Earned_On_Domestic__c + payment.Payment_Earned_On_Intl__c;
                    if ((payment.Payment_Amount__c + payment.GST__c) > 15000) {
                        payment.Payment_Amount__c = 15000 - payment.GST__c;
                    }

                } else {
                    payment.Payment_Type__c = 'SmartFLY Rebate' ;
                    payment.SmartFly_Dom_Eligible__c = totals.containsKey(cid + '_D') ? totals.get(cid + '_D') : 0;
                    payment.SmartFly_TT_Eligible__c = totals.containsKey(cid + '_T') ? totals.get(cid + '_T') : 0;

                    payment.GST__c = (payment.Domestic_Eligible_Expenditure__c * (payment.Rebate_Percentage__c + 0.02)) / 11;
                    payment.Payment_Earned_On_Domestic__c = (payment.Domestic_Eligible_Expenditure__c * (payment.Rebate_Percentage__c + 0.02)) - payment.GST__c;
                    payment.Payment_Earned_On_Intl__c = (payment.Intl_Eligible_Expenditure__c * payment.Rebate_Percentage__c) + (payment.TT_Eligible_Expenditure__c * (payment.Rebate_Percentage__c + 0.02));
                    payment.Payment_Amount__c = payment.Payment_Earned_On_Domestic__c + payment.Payment_Earned_On_Intl__c;
                    payment.SmartFLY_Dom_Rebate__c = payment.SmartFly_Dom_Eligible__c * (payment.Rebate_Percentage__c + 0.02);
                    payment.SmartFLY_TT_Rebate__c = payment.SmartFly_TT_Eligible__c * (payment.Rebate_Percentage__c + 0.02);
                    payment.FCTG_Dom_Rebate_Recovery__c = (payment.SmartFly_Dom_Eligible__c * .02) / 2;
                    payment.FCTG_TT_Rebate_Recovery__c = (payment.SmartFly_TT_Eligible__c * .02) / 2;
                }

                payment.Upsert_ID__c = payment.Account__c + '_' + payment.Contract__c + '_' + payment.Payment_Type__c + '_' + payment.Start_Date__c + '_' + payment.End_Date__c;
                payments.add(payment);
            }
        }

        insert payments;
    }

    public void finish(Database.BatchableContext BC) {
    }
}