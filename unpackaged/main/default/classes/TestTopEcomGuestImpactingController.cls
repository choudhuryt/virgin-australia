/** * File Name      : TestTopEcomGuestImpactingController
* Description        : This Apex Class is the Controller for DomesticDiscount VF Page
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Ed 'the man' Stachyra
* * Date             : 24 May 2013
* * Technical Task ID: 
* * Notes            :  The test class for this file is:  TopEcomGuestImpactingController
* Modification Log =============================================================== 
Ver Date Author Modification --- ---- ------ -------------
* */

@isTest
private class TestTopEcomGuestImpactingController {

    static testMethod void myUnitTest() {
        
        
        TopEcomGuestImpacting__c  topimpacts = new TopEcomGuestImpacting__c();
        topimpacts.EcomGuestItem_1__c = 'xxx';
        topimpacts.ImpactRating1__c = 1;
        topimpacts.exist__c = True;
        
        List<TopEcomGuestImpacting__c> searchResults = new List<TopEcomGuestImpacting__c>();
        
        searchResults.add(topimpacts);
         
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
  	
  			
 		insert account;
 
        Contract contractOld = new Contract();
        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        
        
        insert contractOld;
        Contract contract = new Contract();
		contract.Status = 'Draft';
        contract.AccountId = account.id;
        contract.ContractTerm = 12;
        contract.StartDate = Date.newInstance(2011,01,01);
        contract.Contract_Title__c = 'TEST';
        contract.Contract_Reference__c = 'TEST';
        contract.AccountId = account.id;
        insert contract;
        
        //instantiate the DomesticDiscountController 
        PageReference  ref1 = Page.TopEcomGuestImpacting;
        ref1.getParameters();//.put('Id', contract.Id);
        ApexPages.StandardController conL = new ApexPages.StandardController(contractOld);
        TopEcomGuestImpactingController lController = new TopEcomGuestImpactingController();
        
        //instantiate the DomesticDiscountController 
        ApexPages.StandardController conX = new ApexPages.StandardController(contractOld);
        TopEcomGuestImpactingController xController = new TopEcomGuestImpactingController();
        
        Test.startTest();
        ref1 = xController.initTopEcom();
       
        ref1 = xController.newEcom();
      
        ref1 = xController.save();
        ref1 = xController.qsave();
        
        ref1 = xController.newEcom();
       
        
        ref1 = xController.remEcom();
       
        Test.stopTest();
  
        
        
    }
}