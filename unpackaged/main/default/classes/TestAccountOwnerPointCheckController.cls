@isTest
private class TestAccountOwnerPointCheckController 

{

     static testMethod void TestAccountOwner() 
     {
       User u = TestUtilityClass.createTestUser();
       insert u;
       
      system.runAs(u)
      {
       Test.startTest();   
       Waiver_Point__c wp =   TestUtilityClass.createTestAMLevelWaiverPoint(u.Id);    
       insert wp;           
         
       PageReference pref = Page.AccountManagerPointCheck;
          
       pref.getParameters().put('id', u.id);
       Test.setCurrentPage(pref); 
       
       ApexPages.StandardController conL = new ApexPages.StandardController(u);
       AccountOwnerPointCheckController lController = new AccountOwnerPointCheckController(conL)  ;
       
       pref = lController.Check();
          
       PageReference pref1 = Page.AccountManagerPointCheckForUpdate;
          
       pref1.getParameters().put('id', u.id);
       Test.setCurrentPage(pref1); 
       
       ApexPages.StandardController conL1 = new ApexPages.StandardController(u);
       AccountOwnerPointCheckController lController1 = new AccountOwnerPointCheckController(conL1)  ;
       
       pref1 = lController1.Check();
       pref1 = lController1.UpdateUser();

          
       Test.stopTest();
      }
     }
      
     static testMethod void TestAccountOwnerUpdate() 
     {
       User u = TestUtilityClass.createTestUser();
       insert u;
       
      system.runAs(u)
      {
       Test.startTest();   
      
       PageReference pref3 = Page.AccountManagerWaiverPointUpdate;
          
       pref3.getParameters().put('id', u.id);
       Test.setCurrentPage(pref3); 
        
       ApexPages.StandardController conL2 = new ApexPages.StandardController(u);
          
       AccountManagerPointUpdateController lController2 = new AccountManagerPointUpdateController(conL2)  ;    
          
       pref3 = lController2.initDisc() ;
          
       pref3 = lController2.save();  
       pref3 = lController2.cancel()  ; 
          
       Test.stopTest();
      }
     }
      
    
}