public  with sharing class SQONDSelectionController {
    
    private ApexPages.StandardController controller {get; set;}
    public contract con{get;set;}
    public List <Contract> contracts {get;set;}
    
    
     public SQONDSelectionController (ApexPages.StandardController myController) 
    {
       
              con=(Contract)myController.getrecord();
    }

     public PageReference save(){
     system.debug('Inside save')  ;       
     upsert con;
     PageReference newPage;
     newPage = Page.InternationalDiscountPage_v6;
     newPage.getParameters().put('id', ApexPages.currentPage().getParameters().get('id'));            
     return newPage ; 
     newPage.setRedirect(true);
     return newPage;   
         
     }
    
    public PageReference initDisc() 
    {
    
     try{
   
       contracts=   [select id,Cos_Version__c, EUONDSelected__c , SQSEAONDSelected__c,
                     SQNAONDSelected__c,SQWAAONDSelected__c,SQUSONDSelected__c,
                     SQEUONDSelected1__c,SQEUONDSelected2__c,SQNAONDSelected1__c,
                     SQSEAONDSelected1__c,SQWAAONDSelected1__c
                     from Contract where id =:ApexPages.currentPage().getParameters().get('id')
                    ];
       con = contracts.get(0);
       }catch(Exception e){
         
       }
        return null;  
    }
    
       public String[] EUONDItems { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EUONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.EUONDSelected__c !=null && this.con.EUONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.EUONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.EUONDSelected__c + con.id ); 
        }
       } 
     
        public List<SelectOption> EUONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.EUONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
    
      public String[] EUONDItems1 { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EUONDOptions1;
        for(SelectOption s : sos)
        {
        if (this.con.SQEUONDSelected1__c !=null && this.con.SQEUONDSelected1__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.SQEUONDSelected1__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.SQEUONDSelected1__c + con.id ); 
        }
       } 
     
        public List<SelectOption> EUONDOptions1
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.SQEUONDSelected1__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    public String[] EUONDItems2 { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EUONDOptions2;
        for(SelectOption s : sos)
        {
        if (this.con.SQEUONDSelected2__c !=null && this.con.SQEUONDSelected2__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.SQEUONDSelected2__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.SQEUONDSelected2__c + con.id ); 
        }
       } 
     
        public List<SelectOption> EUONDOptions2
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.SQEUONDSelected2__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
    public String[] SEAONDItems { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.SEAONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.SQSEAONDSelected__c !=null && this.con.SQSEAONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.SQSEAONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.SQSEAONDSelected__c + con.id ); 
        }
       } 
     
        public List<SelectOption> SEAONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.SQSEAONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
     public String[] SEAONDItems1 { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.SEAONDOptions1;
        for(SelectOption s : sos)
        {
        if (this.con.SQSEAONDSelected1__c !=null && this.con.SQSEAONDSelected1__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.SQSEAONDSelected1__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.SQSEAONDSelected1__c + con.id ); 
        }
       } 
     
        public List<SelectOption> SEAONDOptions1
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.SQSEAONDSelected1__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
   public String[] NAONDItems { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.NAONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.SQNAONDSelected__c !=null && this.con.SQNAONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.SQNAONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.SQNAONDSelected__c + con.id ); 
        }
       } 
     
        public List<SelectOption> NAONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.SQNAONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
    
     public String[] NAONDItems1 { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.NAONDOptions1;
        for(SelectOption s : sos)
        {
        if (this.con.SQNAONDSelected1__c !=null && this.con.SQNAONDSelected1__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.SQNAONDSelected1__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.SQNAONDSelected1__c + con.id ); 
        }
       } 
     
        public List<SelectOption> NAONDOptions1
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.SQNAONDSelected1__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
    public String[] WAAONDItems { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.WAAONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.SQWAAONDSelected__c !=null && this.con.SQWAAONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.SQWAAONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.SQWAAONDSelected__c + con.id ); 
        }
       } 
     
        public List<SelectOption> WAAONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.SQWAAONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    public String[] WAAONDItems1 { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.WAAONDOptions1;
        for(SelectOption s : sos)
        {
        if (this.con.SQWAAONDSelected1__c !=null && this.con.SQWAAONDSelected1__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.SQWAAONDSelected1__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.SQWAAONDSelected1__c + con.id ); 
        }
       } 
     
        public List<SelectOption> WAAONDOptions1
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.SQWAAONDSelected1__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
   public String[] USONDItems { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.USONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.SQUSONDSelected__c !=null && this.con.SQUSONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.SQUSONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.SQUSONDSelected__c + con.id ); 
        }
       } 
     
        public List<SelectOption> USONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.SQUSONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    } 
    
}