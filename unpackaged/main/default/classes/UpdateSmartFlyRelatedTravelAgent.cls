/*
 *  Updated By Cloudwerx : Refactored the below code 
 * 
 * 
 */
public class UpdateSmartFlyRelatedTravelAgent {
    @InvocableMethod    
    public static void UpdateRelatedAgentID(List<Id> ContactIds) {      
        
        List<Account> accountsoUpdate = new List<Account>();
        Set<String> contactBookingOfficeNames = new Set<String>();
        
        List<Contact> contactList = [SELECT Id, Booking_Office_Name__c, AccountId, 
                                     Account.Main_Corporate_TMC__c FROM Contact WHERE Id IN :ContactIds];
        
        for (Contact contactObj : contactList) {
            contactBookingOfficeNames.add(contactObj.Booking_Office_Name__c);
        }
        
        Map<Id, Account> rtaMap =  new Map<Id, Account>([SELECT Id FROM Account WHERE FC_Store_Name__c IN :contactBookingOfficeNames]); 
        
        for(Contact contactObj: contactList){    
            Id mainCorporateTMCId = (rtaMap != null && rtaMap.containsKey(contactObj.AccountId)) ? rtaMap.get(contactObj.AccountId)?.Id : null;
            if (mainCorporateTMCId != null) {
                accountsoUpdate.add(new Account(Id = contactObj.AccountId, Main_Corporate_TMC__c = mainCorporateTMCId));
            }
        }   
        UPDATE accountsoUpdate;      
    }
}