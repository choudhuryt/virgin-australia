/** * File Name      : Accelerate Contracts Batch Test Class
* Description        : This Test Apex Class is a Test class that tests a batch class that runs every day to Check an active contract 
					   that has an accelerate record and that has a VA revenue record that has expired, 
					   the Code then creates a new VA record taking the previous spend values and inserts a 
					   new one updates the expired record to invalid status.
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Andy Burgess
* * Date             : 16th May 2012
* * Technical Task ID: 126
* * Notes            : Batch Classes require the start, execute and finish methods and must implement Database.Batchable
                     : To run as a schedule task they also need a Schedule Class that implements Schedulable, this class has
                       a scheduledClass called ScheduleAcceleratorBatch.cls. 
* Modification Log ==================================================​============= 
Ver Date Author Modification --- ---- ------ -------------
* * 19.May.2016     : Warjie Malibago (Accenture CloudFirst); see 190516WBM
* */ 

@isTest
private class TestAccelerateContractsBatch {
    
    //190516WBM Start
    static testMethod void testAccelerateContractBatchPositive(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Test.startTest();
            
            //Create account
            Account acc = TestUtilityClass.createTestAccount();
            insert acc;
            
            //Create contract
            Contract ct = TestUtilityClass.createTestContract(acc.Id);
            insert ct;
            ct.Fixed_Amount__c = 100000;
            ct.Status = 'Activated';
            update ct;
            
            //Create marketing spend down
            Marketing_Spend_Down__c msd = TestUtilityClass.createTestMarketingSpendDown(acc.Id, ct.Id);
            msd.Amount_to_be_Deducted__c = 1000 ; 
            insert msd;
            
            //Create VA revenue
            VA_Revenue__c vr = TestUtilityClass.createTestVARevenue(acc.Id, ct.Id);
            insert vr;  
            
            AccelerateContractBatch testBatch = new AccelerateContractBatch();
            Database.executeBatch(testBatch);
            
            Test.stopTest();
            
            system.assertEquals(2, [SELECT COUNT() FROM VA_Revenue__c WHERE Account__c =: acc.Id]);
        }
    }
    //190516WBM End
}