/** * File Name      : Responsys Batch Test Class
* Description        : This Test Apex Class is a Test class that tests a batch class that runs every day to Check an active contract 
that has an accelerate record and that has a VA revenue record that has expired, 
the Code then creates a new VA record taking the previous spend values and inserts a 
new one updates the expired record to invalid status.
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Edward Stachyra
* * Date             : 23 August 2012
* * Technical Task ID: 
* * Notes            
: Batch Classes require the start, execute and finish methods and must implement Database.Batchable
: To run as a schedule task they also need a Schedule Class that implements Schedulable, this class has
a scheduledClass called ScheduleAcceleratorBatch.cls. 
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
* @UpdatedBy : cloudwerx


* Modification Log ==================================================​============= 
Ver Date Author Modification --- ---- ------ -------------
* */ 

@isTest
private class TestResponsysBatch {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        List<Contact> testContacts = new List<Contact>();
        testContacts.add(TestDataFactory.createTestContact(testAccountObj.Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890'));
        testContacts.add(TestDataFactory.createTestContact(testAccountObj.Id, 'test1@gmail.com', 'Test1', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890'));
        testContacts.add(TestDataFactory.createTestContact(testAccountObj.Id, 'test2@gmail.com', 'Tes2t', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890'));
        INSERT testContacts;
        
        Campaign testCampaignObj = TestDataFactory.createTestCampaign('Test Campaign', Date.today(), Date.today().addDays(10), true);
        INSERT testCampaignObj;
        
        List<CampaignMemberStatus> testCampaignMemberStatuses = new List<CampaignMemberStatus>();
        testCampaignMemberStatuses.add(TestDataFactory.createTestCampaignMemberStatus(testCampaignObj.Id, 'Hard Bounced', false, false, 3));
        testCampaignMemberStatuses.add(TestDataFactory.createTestCampaignMemberStatus(testCampaignObj.Id, 'Complained of spam', false, false, 4));
        testCampaignMemberStatuses.add(TestDataFactory.createTestCampaignMemberStatus(testCampaignObj.Id, 'Opted Out', false, false, 5));
        INSERT testCampaignMemberStatuses;
        
        List<Subscriptions__c> testSubscriptions = new List<Subscriptions__c>();
        testSubscriptions.add(TestDataFactory.createTestSubscription(testContacts[0].Id, false, false));
        testSubscriptions.add(TestDataFactory.createTestSubscription(testContacts[1].Id, false, false));
        testSubscriptions.add(TestDataFactory.createTestSubscription(testContacts[2].Id, false, false));
        INSERT testSubscriptions;
        
        testContacts[2].Subscriptions__c = testSubscriptions[2].Id;
        UPDATE testContacts[2];
        
        List<CampaignMember> testCampaignMembers = new List<CampaignMember>();
        testCampaignMembers.add(TestDataFactory.createTestCampaignMember(testCampaignObj.Id, testContacts[0].Id, 'Hard Bounced', false));
        testCampaignMembers.add(TestDataFactory.createTestCampaignMember(testCampaignObj.Id, testContacts[1].Id, 'Complained of spam', false));
        testCampaignMembers.add(TestDataFactory.createTestCampaignMember(testCampaignObj.Id, testContacts[2].Id, 'Opted Out', false));
        INSERT testCampaignMembers;
    }
    
    @isTest
    private static void testResponsysBatch() {
        Test.startTest();
        Id jobid= Database.executeBatch(new ResponsysBatch() ,5);
        Test.stopTest();
        //Asserts 
        List<Subscriptions__c> testSubscriptions = [SELECT Id, Accelerate_EDM_Opt_Out__c, Accelerate_EDM__c FROM Subscriptions__c];
        system.assertEquals(true, testSubscriptions.size() > 0);
        system.assertEquals(false, testSubscriptions[0].Accelerate_EDM_Opt_Out__c);
        system.assertEquals(false, testSubscriptions[0].Accelerate_EDM__c);
    }
    
    @isTest
    private static void testScheduleResponsysBatch() {
        Test.startTest();
        ScheduleResponsysBatch scheduleResponsysBatchObj = new ScheduleResponsysBatch();
        String cronTime = '0 0 23 * * ?'; 
        system.schedule('scheduleResponsysBatchObj' + System.now(), cronTime, scheduleResponsysBatchObj);
        Test.stopTest();
        //Asserts
        List<Subscriptions__c> testSubscriptions = [SELECT Id, Accelerate_EDM_Opt_Out__c, Accelerate_EDM__c FROM Subscriptions__c];
        system.assertEquals(true, testSubscriptions.size() > 0);
        system.assertEquals(false, testSubscriptions[0].Accelerate_EDM_Opt_Out__c);
        system.assertEquals(false, testSubscriptions[0].Accelerate_EDM__c);
    }
}