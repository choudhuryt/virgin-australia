/**
* @description       : Test class for leadConvertTaskInfoComponentController & leadConvertTaskInfoComponentController
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestLeadConvertTaskRemCompCtrl {
    @isTest
    private static void testleadConvertTaskInfoComponentController() {
        Test.startTest();
        leadConvertTaskInfoComponentController leadConvertTaskInfoComponentControllerObj = new leadConvertTaskInfoComponentController();
        leadConvertTaskInfoComponentControllerObj.reminderTime = String.valueOf(Date.today());
        List<SelectOption> taskStatusOption = leadConvertTaskInfoComponentControllerObj.TaskStatusOption;
        List<SelectOption> taskPriorityOption = leadConvertTaskInfoComponentControllerObj.TaskPriorityOption;
        PageReference dueDateChangedPageReference = leadConvertTaskInfoComponentControllerObj.DueDateChanged();
        Test.stopTest();
        //Asserts
        system.assertEquals(true, taskStatusOption.size() > 0);
        system.assertEquals(true, taskPriorityOption.size() > 0);
        system.assertEquals(null, dueDateChangedPageReference);
    }
    
    @isTest
    private static void testLeadConvertTaskRemindComponentController() {
        Test.startTest();
        leadConvertTaskRemindComponentController leadConvertTaskRemindComponentControllerObj = new leadConvertTaskRemindComponentController();
        leadConvertTaskRemindComponentControllerObj.reminderTime = String.valueOf(Date.today());
        List<SelectOption> reminderTimeOption = leadConvertTaskRemindComponentControllerObj.ReminderTimeOption;
        String disabledActivityDate = leadConvertTaskRemindComponentControllerObj.disabledActivityDate;
        Test.stopTest();
        //Asserts
        system.assertEquals((DateTime.newInstance(system.today(), Time.newInstance(0,0,0,0)).format('M/d/yyyy')).trim(), disabledActivityDate);
        system.assertEquals(true, reminderTimeOption.size() > 0);
        system.assertEquals(true, leadConvertTaskRemindComponentControllerObj.taskID.IsReminderSet);
    }
    
    @isTest
    private static void testLeadConvertTaskRemindComponentControllerWithOtherUser() {
        Profile profileId = [SELECT Id FROM profile WHERE name = 'Standard User'];
        User userObj = TestDataFactory.createTestUser('standt', 'Testing', 'Testing', 'standarduser@testorg.com', 'testUser@testleadconvert.com', 3, 'TestStandardUser', profileId.Id, 'America/Mexico_City', 'es_MX', 'UTF-8', 'es_MX');
        INSERT userObj;
        System.runAs(userObj) {
            Test.startTest();
            leadConvertTaskRemindComponentController leadConvertTaskRemindComponentControllerObj = new leadConvertTaskRemindComponentController();
            leadConvertTaskRemindComponentControllerObj.reminderTime = String.valueOf(Date.today());
            List<SelectOption> reminderTimeOption = leadConvertTaskRemindComponentControllerObj.ReminderTimeOption;
            String disabledActivityDate = leadConvertTaskRemindComponentControllerObj.disabledActivityDate;
            Test.stopTest();
            //Asserts
            system.assertEquals((DateTime.newInstance(system.today(), Time.newInstance(0,0,0,0)).format('M/d/yyyy')).trim(), disabledActivityDate);
            system.assertEquals(true, reminderTimeOption.size() > 0);
            system.assertEquals(true, leadConvertTaskRemindComponentControllerObj.taskID.IsReminderSet);
        }
    }
}