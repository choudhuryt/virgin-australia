@isTest
private class TestLoyaltyService{
    static testMethod void TestMockServices() {
        Test.setMock(WebServiceMock.class, new LoyaltyServiceMockImpl());

        GuestCaseVelocityInfo.SalesforceLoyaltyService services = new GuestCaseVelocityInfo.SalesforceLoyaltyService();
        GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType response_x = services.GetLoyaltyDetails('L111');
        GuestCaseVelocityInfoModel.MemberInformationType memberInformationType = response_x.MemberInformation;
        GuestCaseVelocityInfoModel.PersonType Person = memberInformationType.Person;
        String Surname = Person.Surname;
        System.assertEquals('Vu', Surname);
        List<String> ContactEmail = memberInformationType.ContactEmail;
        System.assertEquals('vuholmes@gmail.com', ContactEmail.get(0));
        GuestCaseVelocityInfoModel.ProfileInformationType profileInformationType = response_x.ProfileInformation;
        System.assertEquals('open', profileInformationType.Status);
    }
    
    static testMethod void TestGuestCaseVelocityInfoUtil() {
        GuestCaseVelocityInfoUtil.AnyElementType anyElementType = new GuestCaseVelocityInfoUtil.AnyElementType();
        GuestCaseVelocityInfoUtil.FaultType faultType = new GuestCaseVelocityInfoUtil.FaultType();
        GuestCaseVelocityInfoUtil.FaultEventType faultEventType = new GuestCaseVelocityInfoUtil.FaultEventType();
        GuestCaseVelocityInfoUtil.PropertySetType propertySetType = new GuestCaseVelocityInfoUtil.PropertySetType();
        GuestCaseVelocityInfoUtil.payload_element payloadElement = new GuestCaseVelocityInfoUtil.payload_element();
    }
    
    static testMethod void TestGuestCaseVelocityInfoModel() {
        GuestCaseVelocityInfoModel.ContactTelephoneType ContactTelephoneType = new GuestCaseVelocityInfoModel.ContactTelephoneType();
        GuestCaseVelocityInfoModel.MemberRelationshipType memberRelationshipType = new GuestCaseVelocityInfoModel.MemberRelationshipType();
        GuestCaseVelocityInfoModel.SeatingPreferenceType seatingPreferenceType = new GuestCaseVelocityInfoModel.SeatingPreferenceType();
        GuestCaseVelocityInfoModel.StateProvType StateProvType = new GuestCaseVelocityInfoModel.StateProvType();
        GuestCaseVelocityInfoModel.IDContactTelephoneType idContactTelephoneType = new GuestCaseVelocityInfoModel.IDContactTelephoneType();
        GuestCaseVelocityInfoModel.LoyaltyMembershipType loyaltyMembershipType = new GuestCaseVelocityInfoModel.LoyaltyMembershipType();
        GuestCaseVelocityInfoModel.PersonType personType = new GuestCaseVelocityInfoModel.PersonType();
        GuestCaseVelocityInfoModel.AddressListType addressListType = new GuestCaseVelocityInfoModel.AddressListType();
        GuestCaseVelocityInfoModel.PreferencesType preferencesType = new GuestCaseVelocityInfoModel.PreferencesType();
        GuestCaseVelocityInfoModel.ParsedTelephoneNumberType parsedTelephoneNumberType = new GuestCaseVelocityInfoModel.ParsedTelephoneNumberType();
        GuestCaseVelocityInfoModel.ProfileInformationType profileInformationType = new GuestCaseVelocityInfoModel.ProfileInformationType();
        GuestCaseVelocityInfoModel.CountryType countryType = new GuestCaseVelocityInfoModel.CountryType();
        GuestCaseVelocityInfoModel.IdentityInformationType identityInformationType = new GuestCaseVelocityInfoModel.IdentityInformationType();
        GuestCaseVelocityInfoModel.IDContactEmailType idContactEmailType = new GuestCaseVelocityInfoModel.IDContactEmailType();
        GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType getLoyaltyDetailsRSType = new GuestCaseVelocityInfoModel.GetLoyaltyDetailsRSType();
        GuestCaseVelocityInfoModel.LoyaltyProgramListType loyaltyProgramListType = new GuestCaseVelocityInfoModel.LoyaltyProgramListType();
        GuestCaseVelocityInfoModel.SecurityInformationType securityInformationType = new GuestCaseVelocityInfoModel.SecurityInformationType();
        GuestCaseVelocityInfoModel.MemberInformationType memberInformationType = new GuestCaseVelocityInfoModel.MemberInformationType();
        GuestCaseVelocityInfoModel.EmploymentInformationType employmentInformationType = new GuestCaseVelocityInfoModel.EmploymentInformationType();
        GuestCaseVelocityInfoModel.GetLoyaltyDetailsRQType getLoyaltyDetailsRQType = new GuestCaseVelocityInfoModel.GetLoyaltyDetailsRQType();
        GuestCaseVelocityInfoModel.LoyaltyProgramType loyaltyProgramType = new GuestCaseVelocityInfoModel.LoyaltyProgramType();
        GuestCaseVelocityInfoModel.InterestType interestType = new GuestCaseVelocityInfoModel.InterestType();
        GuestCaseVelocityInfoModel.ContactEmailType contactEmailType = new GuestCaseVelocityInfoModel.ContactEmailType();
        GuestCaseVelocityInfoModel.LoyaltyMembershipKeyType loyaltyMembershipKeyType = new GuestCaseVelocityInfoModel.LoyaltyMembershipKeyType();
        GuestCaseVelocityInfoModel.InterestListType interestListType = new GuestCaseVelocityInfoModel.InterestListType();
        GuestCaseVelocityInfoModel.CommunicationPreferencesType communicationPreferencesType = new GuestCaseVelocityInfoModel.CommunicationPreferencesType();
        GuestCaseVelocityInfoModel.AddressType addressType = new GuestCaseVelocityInfoModel.AddressType();
        GuestCaseVelocityInfoModel.FullTierLevelType fullTierLevelType = new GuestCaseVelocityInfoModel.FullTierLevelType();
        GuestCaseVelocityInfoModel.ContactEmailListType contactEmailListType = new GuestCaseVelocityInfoModel.ContactEmailListType();
        GuestCaseVelocityInfoModel.UpgradeLoyaltyTierRSType upgradeLoyaltyTierRSType = new GuestCaseVelocityInfoModel.UpgradeLoyaltyTierRSType();
        GuestCaseVelocityInfoModel.UpgradeLoyaltyTierRQType upgradeLoyaltyTierRQType = new GuestCaseVelocityInfoModel.UpgradeLoyaltyTierRQType();
        GuestCaseVelocityInfoModel.TelephoneListType telephoneListType = new GuestCaseVelocityInfoModel.TelephoneListType();
    }
}