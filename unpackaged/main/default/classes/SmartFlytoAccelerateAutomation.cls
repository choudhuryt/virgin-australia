public class SmartFlytoAccelerateAutomation
{
@InvocableMethod
public static void UpdateSmartFlyContractandNotifyKeyContact(List<Id> Accids ) 
{
    List<Contract> conList = new List<Contract>(); 
    List<Contract> lstContractUpdate = new List<Contract>(); 
    List<Account>  lstAccountUpdate = new List<Account>();     
    List<Account> accountlist =  [Select Id , SmartFly_to_Accelerate_Movement_Date__c,
                                  Notify_Key_Contact__c,Reason_For_Termination__c
                                  from Account where id =:accIds and Request_to_Move_to_Accelrate__c = true]; 
    
    List<PartnerNetworkRecordConnection> accsharingconnection = new List<PartnerNetworkRecordConnection>();     
    List<PartnerNetworkRecordConnection> consharingconnection = new List<PartnerNetworkRecordConnection>(); 
    List<Account_Data_Validity__c> smfadvList = new List<Account_Data_Validity__c>();
    
     if( accountlist.size() > 0 )
     {
         accsharingconnection =
                    [SELECT EndDate,Id,LocalRecordId,StartDate,Status FROM PartnerNetworkRecordConnection
                     WHERE LocalRecordId = :accIds
                     and Status = 'Received'
                     and EndDate = null
                    ];      
      
         if (accsharingconnection.size() > 0)
         {
              for(PartnerNetworkRecordConnection  accrecordConn :  accsharingconnection)
              {
                   delete accrecordConn;
               
          }
     
         }
         
         consharingconnection = [SELECT EndDate,Id,LocalRecordId,StartDate,Status
                                            FROM PartnerNetworkRecordConnection
                                            WHERE LocalRecordId  in (SELECT id  FROM Contact  WHERE    AccountId = :accIds and Key_Contact__c = true)                     
                                            and Status = 'Received'
                                            and EndDate = null
                                            ];      
         
          if (consharingconnection.size() > 0)
         {
              for(PartnerNetworkRecordConnection  conrecordConn :  consharingconnection)
              {
                   delete conrecordConn;
               
          }
     
         }
         
           Date dadv= Date.today();
          
           smfadvList = [Select id,To_Date__c From Account_Data_Validity__c where   From_Account__c =:accIds    and Market_Segment__c =  'SmartFly' and  To_Date__c = null ];

           if(smfadvList.size()>0)
         { 
             dadv = date.today().addMonths(1).toStartofMonth().addDays(-1);
             smfadvList[0].To_Date__c = dadv ;
            try 
            {   
             update smfadvList;
             }catch(DmlException e) 
            {
                System.debug('The following exception has occurred: ' + e.getMessage());
             }
            Account_Data_Validity__c advNew = new Account_Data_Validity__c();
            advNew.Account_Owner__c = '00590000000LbNz';
            advNew.Account_Record_Type__c = '01290000000tSR0AAM';
            advNew.From_Account__c = accountlist[0].id ;
            advNew.From_Date__c = dadv.AddDays(1);
            advNew.Market_Segment__c =  'Accelerate';
            advNew.Sales_Matrix_Owner__c = 'Accelerate';            
            advNew.Account_Record_Type_Picklist__c = 'Accelerate' ;
             try 
            {     
             insert advNew;
             }catch(DmlException e) 
            {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }
         } 
         
         conList = [SELECT id,EndDate
                   FROM Contract where
                   accountid = :accIds and 
                   Status = 'Activated' and
                   RecordTypeId = '012900000009HrU' 
                   limit 1] ; 
         
           List<Contact> contactlist =   [SELECT id,AccountId  FROM Contact  WHERE
                                       AccountId = :accIds and
                                       Key_Contact__c = true  and
                                       email <> null and
                                       Status__c = 'Active'
                                       ORDER BY CreatedDate DESC LIMIT 1
                                       ];  
         
         
         if(conlist.size() > 0 )
         {    
                 
           for(Contract c: conlist) 
           {
             if(Date.Today().day() >= 19   )  
             {    
             c.EndDate = date.today().addMonths(1).toStartofMonth().addDays(-1);
             }else
            {
             c.EndDate = date.today().toStartofMonth().addDays(-1);      
            }
             C.Reason_for_Termination__c = accountlist[0].Reason_For_Termination__c ; 
             lstContractUpdate.add(c);  
           }
          try 
          { 
          update lstContractUpdate; 
          }catch(DmlException e) 
          {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }      
         }
    }
    
    if( accountlist.size() > 0 && accountlist[0].Notify_Key_Contact__c  == 'Yes') 
    {
          List<Contact> contactlist =   [SELECT id,AccountId,Email,FirstName  FROM Contact  WHERE
                                       AccountId = :accIds and
                                       Key_Contact__c = true  and
                                       email <> null and
                                       Status__c = 'Active'
                                       ORDER BY CreatedDate DESC LIMIT 1
                                       ]; 
        
         system.debug ('before Contact Details' + contactlist.size());
         if(contactlist.size() > 0 )  
         {
           system.debug ('Contact Details' + contactlist.size() + contactlist[0].email ); 
           for(account acc: accountlist) 
           {
            acc.Accelerate_Key_Contact_Email__c = contactlist[0].email ;
            acc.Accelerate_Key_Contact_First_Name__c =    contactlist[0].FirstName ;
             lstAccountUpdate.add(acc);  
           }
          try 
         { 
          update lstAccountUpdate;
          }catch(DmlException e) 
          {
                System.debug('The following exception has occurred: ' + e.getMessage());
           }    
         } 
    }
    
}

}