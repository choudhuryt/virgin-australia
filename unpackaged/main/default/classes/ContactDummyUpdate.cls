global class ContactDummyUpdate implements Database.Batchable<SObject>,Database.AllowsCallouts {

     global ContactDummyUpdate(){}

     global String gstrQuery = 'Select id , Velocity_Number__c '+
            'FROM Contact where  '+
            ' recordtypeid in ( \'012900000007krgAAA\') ' +
            ' and  Status__c = \'Active\'' +
            ' and  IsAccelerateorSmartflyContact__c = true' +
            ' and  Velocity_Number__c != null ' +
            ' and  Velocity_Number__c != \'----------\'' ;
   
    
     global Database.QueryLocator start(Database.BatchableContext BC)
     {
          System.debug(gstrQuery); 
        return Database.getQueryLocator(gstrQuery);
     }   
    global void execute(Database.BatchableContext BC,List<sObject> scope)
       {

        System.debug('** Starting Batch Process ** ');
           
       List<Contact> contactList = new List <Contact>();
       for (Sobject s : scope)
          {
           Contact c = (Contact)s;
            contactList.add(c);
         }   
      

       for(Integer x =0; x<ContactList.size(); x++)
        {

            Contact contact = contactList.get(x);
           
               if(Test.isRunningTest() )
               {
               }
               else
               { 
                MakeVelocityCallout.apexcallout(contact.id,contact.Velocity_Number__c); 
               } 

         }

        }
   

    //Finishing the batch  (Or though this is not executiong code this methos needs to exsist for the batch class to run!!)  
    global void finish(Database.BatchableContext BC)
    {

    }
}