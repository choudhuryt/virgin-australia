/**
 * Called for a specific Case through the CaseSendEmailWithTemplate VF page.    
 * Redirects to Salesforce's Send Email page with the email template and Contact preselected. 
 * If a template is not specified, the user is prompted to enter one.
 */
public with sharing class CaseSendEmailWithTemplate {
    
    @TestVisible
    private final Case selectedCase;
    public boolean invalidTemplate {get; private set;}
    
    public CaseSendEmailWithTemplate(ApexPages.StandardController stdController) {        
        if (!Test.isRunningTest()) {
            // need access to Contact and Email_Template_Required__c fields 
            // even if we don't get a validation error and display the CaseSendEmailWithTemplate VF page
            stdController.addFields(new List<String>{'Contact', 'Email_Template_Required__c'});    
        }
        this.selectedCase = (Case)stdController.getRecord();
    }
    
    public PageReference init() {
        // ensure a template is selected
        this.invalidTemplate = false;
        
        if (String.isEmpty(selectedCase.Email_Template_Required__c)) {            
            ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR, 
                'This case does not have a template. Select one below.');
                
            ApexPages.addMessage(message);
            this.invalidTemplate = true;
            
            // display the error, prompt the user to select a template
            return null;
        }               
        
        // validate and redirect
        return setTemplateAndSendEmail();
    }
    
    /**
     * Called from this controller, as well as the CaseSendEmailWithTemplate VF page when the user selects a template
     * Validates the template exists, and if so redirects to the send email screen.
     * Otherwise displays an error prompts the user to select a different template.
     */
    public PageReference setTemplateAndSendEmail() {
        // get the template id
        List<EmailTemplate> matchingTemplate = [select Id from EmailTemplate 
                                                where name = :this.selectedCase.Email_Template_Required__c];
        
        if (matchingTemplate.isEmpty()) {
            ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR, 
                'An email template named "'+ this.selectedCase.Email_Template_Required__c +'" does not exist. '
                + 'Please check the Email Template settings in Setup and try again, '
                + 'or choose another template.');
                
            ApexPages.addMessage(message);
            this.invalidTemplate = true;
            
            // display the error
            return null;
        }        
        
        // redirect to the Send Email page                
        return new PageReference(getSendEmailURL(this.selectedCase.Contact.Id, this.selectedCase.Id, matchingTemplate.get(0).Id));
    }
    
    /**
     * Constructs the Send Email URL with the Contact, Case, and template prepopulated 
     */ 
    @TestVisible
    private String getSendEmailURL(Id contactId, Id caseId, Id templateId) {
        String url = '/_ui/core/email/author/EmailAuthor?p2_lkid=' + contactId
                    + '&rtype=003&p3_lkid=' + caseId
                    + '&retURL=%2F' + caseId
                    + '&template_id=' + templateId;        
        return url;
    }
    
}