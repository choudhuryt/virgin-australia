public with sharing class DOMONDSelectionController
{
  private ApexPages.StandardController controller {get; set;}
    public Contract_Addendum__c  contractadd{get;set;}
    public List <Contract_Addendum__c> addlist {get;set;}
    
    
     public DOMONDSelectionController (ApexPages.StandardController myController) 
    {
       
             contractadd=(Contract_Addendum__c)myController.getrecord();
    }
    
    public PageReference initDisc() 
    {
    
     try{
   
           addlist=        [SELECT Contract__c,Cos_Version__c,Domestic_Guideline_Tier__c,Domestic_Requested_Tier__c,                                
                                Red_Circle__c,Regional_Guideline_Tier__c ,Regional_Requested_Tier__c,Status__c,IsEditable__c ,
                                Domestic_OD_Pairs__c ,Regional_OD_Pairs__c,Domestic_Red_Circle_Requested_Tier__c  ,Regional_Red_Circle_Requested_Tier__c                 
                                FROM Contract_Addendum__c 
                                where id =:ApexPages.currentPage().getParameters().get('id')
                               ];
       contractadd =  addlist.get(0);
      }catch(Exception e){
         
       }
          return null;
     }
    
     public PageReference save(){
     system.debug('Inside save')  ;       
     upsert contractadd;
     PageReference newPage;
     newPage = Page.RedCircleDiscounts;
     newPage.getParameters().put('id', ApexPages.currentPage().getParameters().get('id'));       
     newPage.setRedirect(true);
     return newPage;   
         
     }
    
     public String[] REGONDItems { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.REGONDOptions;
        for(SelectOption s : sos)
        {
        if (this.contractadd.Regional_OD_Pairs__c !=null && this.contractadd.Regional_OD_Pairs__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
         contractadd.Regional_OD_Pairs__c = selectedCheckBox;
        system.debug('Selected checkbox' +  contractadd.Regional_OD_Pairs__c +contractadd.id ); 
        }
       } 

     public String[] DOMONDItems { 
        get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.DOMONDOptions;
        for(SelectOption s : sos)
        {
        if (this.contractadd.Domestic_OD_Pairs__c !=null && this.contractadd.Domestic_OD_Pairs__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
         contractadd.Domestic_OD_Pairs__c = selectedCheckBox;
        system.debug('Selected checkbox' +  contractadd.Domestic_OD_Pairs__c +contractadd.id ); 
        }
       } 
    
      public List<SelectOption> REGONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : Contract_Addendum__c.Regional_OD_Pairs__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
     
        public List<SelectOption> DOMONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : Contract_Addendum__c.Domestic_OD_Pairs__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
}