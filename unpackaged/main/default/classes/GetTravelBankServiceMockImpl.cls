@isTest
global class GetTravelBankServiceMockImpl implements WebServiceMock {
   global void doInvoke(Object stub,  
         Object request,  
         Map<String, object> response,  
         String endpoint,  
         String soapAction,  
         String requestName,  
         String responseNS,  
         String responseName,  
         String responseType) {
     //call demo function
     TravelBank.SalesforceTravelBankService sfTravelBankService =  new TravelBank.SalesforceTravelBankService();
     TravelBankModel.GetTravelBankAccountRSType respElement = sfTravelBankService.GetTravelBankDemo();
     //TravelBankModel.UpdateTravelBankAccountBalanceRSType respElement = sfTravelBankService.UpdateTravelBankAccountBalanceDemo();
     
     response.put('response_x', respElement); 
   }
}