/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCosPage {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
  	
  			
 		insert account;
 
        Contract contractOld = new Contract();
        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.Cos_Version__c ='11.2';
        contractOld.va_sq_requested_tier__c='1';
        contractOld.Red_Circle__c =true;
        contractOld.IsTemplate__c =true;
        contractOld.UK_Europe_Revenue_Target__c =1000;
        contractOld.Middle_East_Revenue_Target__c =1000;
        contractOld.Asia_Revenue_Target__c =1000;
        contractOld.Africa_revenue_Target__c =1000;
        
        
        insert contractOld;
        
        Cost_of_Sale__c costOfSale = new Cost_of_Sale__c();
        costOfSale.Contract__c = contractOld.id;
        costOfSale.Domestic_POS__c =1;
        costOfSale.Domestic_Revenue_POS__c =1000;
        insert costOfSale;
        
        PageReference pref = Page.COSPage;
        	pref.getParameters().put('id', contractOld.id);
        	Test.setCurrentPage(pref);
			        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(contractOld);
        COSController lController = new COSController(conL);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conX = new ApexPages.StandardController(contractOld);
        COSController xController = new COSController(conX);
          Test.startTest();
        PageReference  ref1 = Page.COSPage;
       
        
        ref1 = lController.initDisc();
        
        
        ref1 = lController.cancel();
        
        
        test.stopTest();
        
        
    }
}