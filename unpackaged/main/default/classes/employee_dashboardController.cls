public without sharing class employee_dashboardController {
    @AuraEnabled
    public static List<Dashboard> getDashboards() {
        String userId = UserInfo.getUserId();
        User userRecord = [Select Id, Dashboard_Assigned__c from User where Id=: userId];
        String dashboardAssigned = userRecord.Dashboard_Assigned__c;
        List<String> dashboardList = new List<String>();
        if(dashboardAssigned != null) {
            for(String dashboard : dashboardAssigned.split(',')){
                dashboardList.add(dashboard.Trim());
            }
        }
        return [SELECT Id, Title FROM Dashboard WHERE Title=: dashboardList];
    }
}