/* 
 *  Updated by Cloudwerx : Refactored the below code
 * 
 */ 
public class KeyContactUpdate
{   
    
    @InvocableMethod
    public static void UpdateKeyContactOnAccount(List<Id> accIds){
                
        List<Account> accountsToUpdate = new List<Account>();
        
        for(Account acc : [SELECT Id, Accelerate_Key_Contact_Email__c, Accelerate_Key_Contact_First_Name__c , 
                           Accelerate_Key_Contact_Last_Name__c, (SELECT Id, Email, FirstName, LastName FROM Contacts 
                           WHERE Key_Contact__c = true AND email != null AND Status__c = 'Active' 
						   ORDER BY CreatedDate DESC LIMIT 1) FROM Account WHERE id =:accIds]) {
                               
            if(acc.contacts != NULL && acc.contacts.size() > 0 ){
                
                acc.Accelerate_Key_Contact_Email__c =  acc.contacts[0].Email; 
                acc.Accelerate_Key_Contact_First_Name__c =  acc.contacts[0].FirstName;
                acc.Accelerate_Key_Contact_Last_Name__c =  acc.contacts[0].LastName ;
                acc.Accelerate_Key_Contact__c = acc.contacts[0].FirstName+' '+acc.contacts[0].LastName;
                accountsToUpdate.add(acc);  
            }
            
        }
        
        UPDATE accountsToUpdate;        
    }        
}