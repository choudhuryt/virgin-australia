/*
 * Author: Warjie Malibago (Accenture CloudFirst)
 * Date: June 7, 2016
 * Description: Test class for TaskTrigger
*/
@isTest
private class TestTaskTrigger {
    
/*   static testMethod void testImplementFollowingContract(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            insert acc;
            
            Contract cont = TestUtilityClass.createTestContract(acc.Id);
            cont.RecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Industry - Exceptions').getRecordTypeId();
            insert cont;
            
            Task t = TestUtilityClass.createTestTask(cont.Id, 'Please Implement The following Contract', 'Not Started', u.Id);
            insert t;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'INDUSTRY SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t.Id].OwnerId);
        }
    }
    
    static testMethod void testProcessAgentRateWithSSGroup(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            acc.Sales_Support_Group__c = u.Id;
            insert acc;
            
            
            Contract cont = TestUtilityClass.createTestContract(acc.Id);
            cont.RecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Industry - Exceptions').getRecordTypeId();
            insert cont;

            Ticket_Request__c tr = TestUtilityClass.createTestTicketRequest(acc.Id);
            insert tr;
            
            Task t = TestUtilityClass.createTestTask(tr.Id, 'Please process the following Agent Rate Request', 'Not Started', u.Id);
            insert t;
            system.assertEquals(u.Id, [SELECT Sales_Support_Group__c FROM Account WHERE Id =: acc.Id].Sales_Support_Group__c);
        }
    }
    
    static testMethod void testProcessAgentRateWithoutSSGroup(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            acc.Sales_Support_Group__c = null;
            acc.Billing_Country_Code__c = 'AU';
            acc.BillingState = 'NSW';
            insert acc;
            
            Contract cont = TestUtilityClass.createTestContract(acc.Id);
            cont.RecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Industry - Exceptions').getRecordTypeId();
            insert cont;

            Ticket_Request__c tr = TestUtilityClass.createTestTicketRequest(acc.Id);
            insert tr;
            
            Task t = TestUtilityClass.createTestTask(tr.Id, 'Please process the following Agent Rate Request', 'Not Started', u.Id);
            insert t;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'NSW SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t.Id].OwnerId);
            
            acc.BillingState = 'QLD';
            update acc;
            Task t2 = TestUtilityClass.createTestTask(tr.Id, 'Please process the following Agent Rate Request', 'Not Started', u.Id);
            insert t2;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'QLD SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t2.Id].OwnerId);
            
            acc.BillingState = 'WA';
            update acc;
            Task t3 = TestUtilityClass.createTestTask(tr.Id, 'Please process the following Agent Rate Request', 'Not Started', u.Id);
            insert t3;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t3.Id].OwnerId);
            
            acc.BillingState = 'NT';
            update acc;
            Task t4 = TestUtilityClass.createTestTask(tr.Id, 'Please process the following Agent Rate Request', 'Not Started', u.Id);
            insert t4;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t4.Id].OwnerId);
            
            acc.BillingState = 'SA';
            update acc;
            Task t5 = TestUtilityClass.createTestTask(tr.Id, 'Please process the following Agent Rate Request', 'Not Started', u.Id);
            insert t5;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t5.Id].OwnerId);
            
            acc.BillingState = 'VIC';
            update acc;
            Task t6 = TestUtilityClass.createTestTask(tr.Id, 'Please process the following Agent Rate Request', 'Not Started', u.Id);
            insert t6;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'VICTAS SALES SUPPORT COORDINATOR'].Id, [SELECT OwnerId FROM Task WHERE Id =: t6.Id].OwnerId);
            
            acc.BillingState = 'TAS';
            update acc;
            Task t7 = TestUtilityClass.createTestTask(tr.Id, 'Please process the following Agent Rate Request', 'Not Started', u.Id);
            insert t7;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'VICTAS SALES SUPPORT COORDINATOR'].Id, [SELECT OwnerId FROM Task WHERE Id =: t7.Id].OwnerId);
            
            acc.BillingState = 'ACT';
            update acc;
            Task t8 = TestUtilityClass.createTestTask(tr.Id, 'Please process the following Agent Rate Request', 'Not Started', u.Id);
            insert t8;  
            system.assertEquals([SELECT Id, Name FROM User WHERE Name = 'ACTGOV SALES SUPPORT' ORDER BY Id DESC LIMIT 1].Id, [SELECT OwnerId FROM Task WHERE Id =: t8.Id].OwnerId);
            
            acc.Billing_Country_Code__c = 'US';
            acc.Sales_Matrix_Owner__c = 'International - Americas';
            update acc;
            Task t9 = TestUtilityClass.createTestTask(tr.Id, 'Please process the following Agent Rate Request', 'Not Started', u.Id);
            insert t9;  
            system.assertEquals([SELECT Id FROM User WHERE id = '00590000004eCzk'].Id, [SELECT OwnerId FROM Task WHERE Id =: t9.Id].OwnerId);
        }
    }

    static testMethod void testProcessFOCTicketRequestWithSSGroup(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            acc.Sales_Support_Group__c = u.Id;
            insert acc;
            
            Contract cont = TestUtilityClass.createTestContract(acc.Id);
            cont.RecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Industry - Exceptions').getRecordTypeId();
            insert cont;

            Ticket_Request__c tr = TestUtilityClass.createTestTicketRequest(acc.Id);
            insert tr;
            
            Task t = TestUtilityClass.createTestTask(tr.Id, 'Please Process the following FOC Ticket Request', 'Not Started', u.Id);
            insert t;
            system.assertEquals(u.Id, [SELECT Sales_Support_Group__c FROM Account WHERE Id =: acc.Id].Sales_Support_Group__c);
        }
    }
    
    static testMethod void testProcessFOCTicketRequestWithoutSSGroup(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            acc.Sales_Support_Group__c = null;
            acc.Billing_Country_Code__c = 'AU';
            acc.BillingState = 'NSW';
            insert acc;
            
            Contract cont = TestUtilityClass.createTestContract(acc.Id);
            cont.RecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Industry - Exceptions').getRecordTypeId();
            insert cont;

            Ticket_Request__c tr = TestUtilityClass.createTestTicketRequest(acc.Id);
            insert tr;
            
            Task t = TestUtilityClass.createTestTask(tr.Id, 'Please Process the following FOC Ticket Request', 'Not Started', u.Id);
            insert t;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'NSW SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t.Id].OwnerId);
            
            acc.BillingState = 'QLD';
            update acc;
            Task t2 = TestUtilityClass.createTestTask(tr.Id, 'Please Process the following FOC Ticket Request', 'Not Started', u.Id);
            insert t2;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'QLD SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t2.Id].OwnerId);
            
            acc.BillingState = 'WA';
            update acc;
            Task t3 = TestUtilityClass.createTestTask(tr.Id, 'Please Process the following FOC Ticket Request', 'Not Started', u.Id);
            insert t3;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t3.Id].OwnerId);
            
            acc.BillingState = 'NT';
            update acc;
            Task t4 = TestUtilityClass.createTestTask(tr.Id, 'Please Process the following FOC Ticket Request', 'Not Started', u.Id);
            insert t4;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t4.Id].OwnerId);
            
            acc.BillingState = 'SA';
            update acc;
            Task t5 = TestUtilityClass.createTestTask(tr.Id, 'Please Process the following FOC Ticket Request', 'Not Started', u.Id);
            insert t5;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t5.Id].OwnerId);
            
            acc.BillingState = 'VIC';
            update acc;
            Task t6 = TestUtilityClass.createTestTask(tr.Id, 'Please Process the following FOC Ticket Request', 'Not Started', u.Id);
            insert t6;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'VICTAS SALES SUPPORT COORDINATOR'].Id, [SELECT OwnerId FROM Task WHERE Id =: t6.Id].OwnerId);
            
            acc.BillingState = 'TAS';
            update acc;
            Task t7 = TestUtilityClass.createTestTask(tr.Id, 'Please Process the following FOC Ticket Request', 'Not Started', u.Id);
            insert t7;  
            system.assertEquals([SELECT Id FROM User WHERE Name = 'VICTAS SALES SUPPORT COORDINATOR'].Id, [SELECT OwnerId FROM Task WHERE Id =: t7.Id].OwnerId);
            
            acc.BillingState = 'ACT';
            update acc;
            Task t8 = TestUtilityClass.createTestTask(tr.Id, 'Please Process the following FOC Ticket Request', 'Not Started', u.Id);
            insert t8;  
            system.assertEquals([SELECT Id, Name FROM User WHERE Name = 'ACTGOV SALES SUPPORT' ORDER BY Id DESC LIMIT 1].Id, [SELECT OwnerId FROM Task WHERE Id =: t8.Id].OwnerId);
            
            acc.Billing_Country_Code__c = 'US';
            acc.Sales_Matrix_Owner__c = 'International - Americas';
            update acc;
            Task t9 = TestUtilityClass.createTestTask(tr.Id, 'Please Process the following FOC Ticket Request', 'Not Started', u.Id);
            insert t9;  
            system.assertEquals([SELECT Id FROM User WHERE id = '00590000004eCzk'].Id, [SELECT OwnerId FROM Task WHERE Id =: t9.Id].OwnerId);
        }
    }
    
    static testMethod void testWaiverApproved(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            acc.Sales_Support_Group__c = u.Id;
            insert acc;
            
            Waiver_Favour__c wf = TestUtilityClass.createTestWaiverFavour(acc.Id, u.Id);
            insert wf;
            
            Task t = TestUtilityClass.createTestTask(wf.Id, 'Waiver has been approved', 'Not Started', u.Id);
            insert t;
            system.assertEquals(u.Id, [SELECT OwnerId FROM Task WHERE Id =: t.Id].OwnerId);
        }
    }

    static testMethod void testFollowingUpgrade(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            acc.Sales_Support_Group__c = u.Id;
            insert acc;
            
            Contact con = TestUtilityClass.createTestContact(acc.Id);
            insert con;
            
            Upgrade__c up = TestUtilityClass.createTestUpgrade(acc.Id, con.Id);
            insert up;
            
            Task t = TestUtilityClass.createTestTask(up.Id, 'Please process the following Upgrade', 'Not Started', u.Id);
            insert t;
            system.assertEquals(u.Id, [SELECT OwnerId FROM Task WHERE Id =: t.Id].OwnerId);

            Upgrade__c up2 = TestUtilityClass.createTestUpgrade(acc.Id, con.Id);
            up2.Approval_Flight_Status__c = 'Firm';
            insert up2;
            
            Task t2 = TestUtilityClass.createTestTask(up2.Id, 'Please process the following Upgrade', 'Not Started', u.Id);
            insert t2;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'Systems Administrator'].Id, [SELECT OwnerId FROM Task WHERE Id =: t2.Id].OwnerId);
        }
    }
    
    static testMethod void testVSMRequestApprovedWithSSGroup(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            acc.Sales_Support_Group__c = u.Id;
            insert acc;
            
            Velocity_Status_Match__c vsm = TestUtilityClass.createTestVelocityStatusMatch(acc.Id);
            insert vsm;
            
            Task t = TestUtilityClass.createTestTask(vsm.Id, 'Velocity Status Match/Upgrade Request Approved', 'Not Started', u.Id);
            insert t;
            system.assertEquals(u.Id, [SELECT OwnerId FROM Task WHERE Id =: t.Id].OwnerId);
        }
    }
    
    static testMethod void testVSMRequestApprovedWithoutSSGroup(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            acc.Billing_Country_Code__c = 'AU';
            acc.BillingState = 'NSW';
            insert acc;
            
            Velocity_Status_Match__c vsm = TestUtilityClass.createTestVelocityStatusMatch(acc.Id);
            insert vsm;
            
            Task t = TestUtilityClass.createTestTask(vsm.Id, 'Velocity Status Match/Upgrade Request Approved', 'Not Started', u.Id);
            insert t;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'NSW SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t.Id].OwnerId);
            
            acc.BillingState = 'QLD';
            update acc;
            Task t2 = TestUtilityClass.createTestTask(vsm.Id, 'Velocity Status Match/Upgrade Request Approved', 'Not Started', u.Id);
            insert t2;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'QLD SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t2.Id].OwnerId); 
            
            acc.BillingState = 'WA';
            update acc;
            Task t3 = TestUtilityClass.createTestTask(vsm.Id, 'Velocity Status Match/Upgrade Request Approved', 'Not Started', u.Id);
            insert t3;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t3.Id].OwnerId); 
            
            acc.BillingState = 'SA';
            update acc;
            Task t4 = TestUtilityClass.createTestTask(vsm.Id, 'Velocity Status Match/Upgrade Request Approved', 'Not Started', u.Id);
            insert t4;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t4.Id].OwnerId);   
            
            acc.BillingState = 'NT';
            update acc;
            Task t5 = TestUtilityClass.createTestTask(vsm.Id, 'Velocity Status Match/Upgrade Request Approved', 'Not Started', u.Id);
            insert t5;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t5.Id].OwnerId);  
            
            acc.BillingState = 'VIC';
            update acc;
            Task t6 = TestUtilityClass.createTestTask(vsm.Id, 'Velocity Status Match/Upgrade Request Approved', 'Not Started', u.Id);
            insert t6;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'VICTAS SALES SUPPORT COORDINATOR'].Id, [SELECT OwnerId FROM Task WHERE Id =: t6.Id].OwnerId);  
            
            acc.BillingState = 'TAS';
            update acc;
            Task t7 = TestUtilityClass.createTestTask(vsm.Id, 'Velocity Status Match/Upgrade Request Approved', 'Not Started', u.Id);
            insert t7;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'VICTAS SALES SUPPORT COORDINATOR'].Id, [SELECT OwnerId FROM Task WHERE Id =: t7.Id].OwnerId); 
            
            acc.BillingState = 'ACT';
            update acc;
            Task t8 = TestUtilityClass.createTestTask(vsm.Id, 'Velocity Status Match/Upgrade Request Approved', 'Not Started', u.Id);
            insert t8;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'ACTGOV SALES SUPPORT' ORDER BY Id DESC LIMIT 1].Id, [SELECT OwnerId FROM Task WHERE Id =: t8.Id].OwnerId);
            
            acc.Billing_Country_Code__c = 'US';
            acc.Sales_Matrix_Owner__c = 'International - Americas';
            update acc;
            Task t9 = TestUtilityClass.createTestTask(vsm.Id, 'Velocity Status Match/Upgrade Request Approved', 'Not Started', u.Id);
            insert t9;
            system.assertEquals([SELECT Id FROM User WHERE id = '00590000004eCzk'].Id, [SELECT OwnerId FROM Task WHERE Id =: t9.Id].OwnerId);
        }
    }
    
    /*static testMethod void testMSDActioned(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            acc.Billing_Country_Code__c = 'AU';
            acc.BillingState = 'NSW';
            insert acc;
            
            Contract cont = TestUtilityClass.createTestContract(acc.Id);
            insert cont;
            
            Marketing_Spend_Down__c msd = TestUtilityClass.createTestMarketingSpendDown(acc.Id, cont.Id);
            insert msd;
            
            Task t = TestUtilityClass.createTestTask(msd.Id, 'Marketing Spend Down Request to be Actioned', 'Not Started', u.Id);
            insert t;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'NSW SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t.Id].OwnerId);
            
            acc.BillingState = 'QLD';
            update acc;
            Task t1 = TestUtilityClass.createTestTask(msd.Id, 'Marketing Spend Down Request to be Actioned', 'Not Started', u.Id);
            insert t1;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'QLD SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t1.Id].OwnerId); 
            
            acc.BillingState = 'WA';
            update acc;
            Task t2 = TestUtilityClass.createTestTask(msd.Id, 'Marketing Spend Down Request to be Actioned', 'Not Started', u.Id);
            insert t2;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t2.Id].OwnerId);  
            
            acc.BillingState = 'SA';
            update acc;
            Task t3 = TestUtilityClass.createTestTask(msd.Id, 'Marketing Spend Down Request to be Actioned', 'Not Started', u.Id);
            insert t3;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t3.Id].OwnerId); 
            
            acc.BillingState = 'NT';
            update acc;
            Task t4 = TestUtilityClass.createTestTask(msd.Id, 'Marketing Spend Down Request to be Actioned', 'Not Started', u.Id);
            insert t4;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t4.Id].OwnerId); 
            
            acc.BillingState = 'VIC';
            update acc;
            Task t5 = TestUtilityClass.createTestTask(msd.Id, 'Marketing Spend Down Request to be Actioned', 'Not Started', u.Id);
            insert t5;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'VICTAS SALES SUPPORT COORDINATOR'].Id, [SELECT OwnerId FROM Task WHERE Id =: t5.Id].OwnerId); 
            
            acc.BillingState = 'TAS';
            update acc;
            Task t6 = TestUtilityClass.createTestTask(msd.Id, 'Marketing Spend Down Request to be Actioned', 'Not Started', u.Id);
            insert t6;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'VICTAS SALES SUPPORT COORDINATOR'].Id, [SELECT OwnerId FROM Task WHERE Id =: t6.Id].OwnerId); 
            
            acc.BillingState = 'ACT';
            update acc;
            Task t7 = TestUtilityClass.createTestTask(msd.Id, 'Marketing Spend Down Request to be Actioned', 'Not Started', u.Id);
            insert t7;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'ACTGOV SALES SUPPORT' ORDER BY Id DESC LIMIT 1].Id, [SELECT OwnerId FROM Task WHERE Id =: t7.Id].OwnerId); 
            
            acc.Billing_Country_Code__c = 'US';
            update acc;
            Task t8 = TestUtilityClass.createTestTask(msd.Id, 'Marketing Spend Down Request to be Actioned', 'Not Started', u.Id);
            insert t8;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'Jodie Timmins'].Id, [SELECT OwnerId FROM Task WHERE Id =: t8.Id].OwnerId); 
        }
    }
    
    static testMethod void testPOMSD(){
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            acc.Billing_Country_Code__c = 'AU';
            acc.BillingState = 'NSW';
            insert acc;
            
            Contract cont = TestUtilityClass.createTestContract(acc.Id);
            insert cont;
            
            Marketing_Spend_Down__c msd = TestUtilityClass.createTestMarketingSpendDown(acc.Id, cont.Id);
            insert msd;
            
            Task t = TestUtilityClass.createTestTask(msd.Id, 'Please Raise a PO for the Related Marketing Spend Down', 'Not Started', u.Id);
            insert t;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'NSW SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t.Id].OwnerId);
            
            acc.BillingState = 'QLD';
            update acc;
            Task t1 = TestUtilityClass.createTestTask(msd.Id, 'Please Raise a PO for the Related Marketing Spend Down', 'Not Started', u.Id);
            insert t1;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'QLD SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t1.Id].OwnerId); 
            
            acc.BillingState = 'WA';
            update acc;
            Task t2 = TestUtilityClass.createTestTask(msd.Id, 'Please Raise a PO for the Related Marketing Spend Down', 'Not Started', u.Id);
            insert t2;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t2.Id].OwnerId);  
            
            acc.BillingState = 'SA';
            update acc;
            Task t3 = TestUtilityClass.createTestTask(msd.Id, 'Please Raise a PO for the Related Marketing Spend Down', 'Not Started', u.Id);
            insert t3;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t3.Id].OwnerId); 
            
            acc.BillingState = 'NT';
            update acc;
            Task t4 = TestUtilityClass.createTestTask(msd.Id, 'Please Raise a PO for the Related Marketing Spend Down', 'Not Started', u.Id);
            insert t4;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'WANTSA SALES SUPPORT'].Id, [SELECT OwnerId FROM Task WHERE Id =: t4.Id].OwnerId); 
            
            acc.BillingState = 'VIC';
            update acc;
            Task t5 = TestUtilityClass.createTestTask(msd.Id, 'Please Raise a PO for the Related Marketing Spend Down', 'Not Started', u.Id);
            insert t5;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'VICTAS SALES SUPPORT COORDINATOR'].Id, [SELECT OwnerId FROM Task WHERE Id =: t5.Id].OwnerId); 
            
            acc.BillingState = 'TAS';
            update acc;
            Task t6 = TestUtilityClass.createTestTask(msd.Id, 'Please Raise a PO for the Related Marketing Spend Down', 'Not Started', u.Id);
            insert t6;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'VICTAS SALES SUPPORT COORDINATOR'].Id, [SELECT OwnerId FROM Task WHERE Id =: t6.Id].OwnerId); 
            
            acc.BillingState = 'ACT';
            update acc;
            Task t7 = TestUtilityClass.createTestTask(msd.Id, 'Please Raise a PO for the Related Marketing Spend Down', 'Not Started', u.Id);
            insert t7;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'ACTGOV SALES SUPPORT' ORDER BY Id DESC LIMIT 1].Id, [SELECT OwnerId FROM Task WHERE Id =: t7.Id].OwnerId); 
            
            acc.Billing_Country_Code__c = 'US';
            update acc;
            Task t8 = TestUtilityClass.createTestTask(msd.Id, 'Please Raise a PO for the Related Marketing Spend Down', 'Not Started', u.Id);
            insert t8;
            system.assertEquals([SELECT Id FROM User WHERE Name = 'Jodie Timmins'].Id, [SELECT OwnerId FROM Task WHERE Id =: t8.Id].OwnerId); 
            
            acc.Sales_Support_Group__c = u.Id;
            update acc;
            Task t9 = TestUtilityClass.createTestTask(msd.Id, 'Please Raise a PO for the Related Marketing Spend Down', 'Not Started', u.Id);
            insert t9;
            system.assertEquals(u.Id, [SELECT OwnerId FROM Task WHERE Id =: t9.Id].OwnerId);             
        }
    }*/
}