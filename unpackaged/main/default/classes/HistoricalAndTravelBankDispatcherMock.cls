@isTest
global class HistoricalAndTravelBankDispatcherMock implements WebServiceMock{
    global void doInvoke(Object stub,  
             Object request,  
             Map<String, object> response,  
             String endpoint,  
             String soapAction,  
             String requestName,  
             String responseNS,  
             String responseName,  
             String responseType) {
                 System.debug('requestName = ' + requestName);
                //call demo function
                 if (stub instanceof SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService) {
                     new HistoricalDetailsServiceMockImpl().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
                 } else if (stub instanceof TravelBank.SalesforceTravelBankService) {
                     //if ('UpdateTravelBankAccountBalanceRQ'.equals(requestName)) {
                         new TravelBankServiceMockImpl().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
                     //} else if ('GetTravelBankAccountRSType'.equals(requestName)) {
                        //new GetTravelBankServiceMockImpl().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
                     //}
                 } else if (stub instanceof GuestCaseVelocityInfo.SalesforceLoyaltyService) {
                     new LoyaltyServiceMockImpl().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
                 }
    }
}