@isTest
public class TestAssetTriggerHandler 
{
    
    public static testMethod void testadspaceassetcreation ()
    {
	  Test.startTest();
      Account acc = TestUtilityClass.createTestAccount();
      acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('AdSpace').getRecordTypeId();
      insert acc;	
        
	  Opportunity opp = TestUtilityClass.createTestOpportunity(acc.Id); 
      opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('AdSpace').getRecordTypeId();
      insert opp; 
        
      Product2 prod = new Product2(Name = 'IFE: Dedicated Channel', 
                                   Family = '	Inflight Entertainment',
                                   Duration__c= '1 Month'  ,
                                   Rate__c= 4500 );
      insert prod; 
        
       asset a = new Asset (
                  Name = '	IFE Wireless',
                  Opportunity__c = opp.Id,
                  Product2Id = prod.Id,
                  AccountId = acc.Id,
                  Quantity=  1,
                  RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('AdSpace').getRecordTypeId(),
                  Status = 'Offered'
              );
      insert a; 
	
	}

}