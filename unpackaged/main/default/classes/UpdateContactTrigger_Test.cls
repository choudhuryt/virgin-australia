/*
 * Author: Warjie Malibago (Accenture CloudFirst)
 * Date: May 24, 2016
 * Description: Test class for UpdateContact
*/
@isTest
private class UpdateContactTrigger_Test {

	private static testMethod void testContact() {
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            insert acc;
            
            Contact con = TestUtilityClass.createTestContact(acc.Id);
            con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();
            con.Account_ID__c = acc.Id;
            insert con;
            system.assertEquals('Contact', [SELECT Record_Type__c FROM Contact WHERE Id =: con.Id].Record_Type__c);
            
            Test.startTest();
            recursiveTriggerHandler.isGoodForUpdate = TRUE;
            con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GR Contact').getRecordTypeId();
            update con;
            Test.stopTest();
            system.assertEquals('GR Contact', [SELECT Record_Type__c FROM Contact WHERE Id =: con.Id].Record_Type__c);
            
        }
	}
	
	private static testMethod void testGRContact() {
        User u = TestUtilityClass.createTestUser();
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            insert acc;
            
            Contact con = TestUtilityClass.createTestContact(acc.Id);
            con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GR Contact').getRecordTypeId();
            insert con;
            system.assertEquals('GR Contact', [SELECT Record_Type__c FROM Contact WHERE Id =: con.Id].Record_Type__c);
            
            Test.startTest();
            recursiveTriggerHandler.isGoodForUpdate = TRUE;
            con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();
            update con;
            system.assertEquals('Contact', [SELECT Record_Type__c FROM Contact WHERE Id =: con.Id].Record_Type__c);
            Test.stopTest();
        }
	}
}