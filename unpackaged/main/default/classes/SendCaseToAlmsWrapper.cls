/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 22.03.2022
**Description: Wrapper Class for Request body for SendCommsToAlmsWS
*/
public class SendCaseToAlmsWrapper {
    
    public class payload{
        public Data data;
    }
    
    public class data{
        public Source source;
        public Communication communication;
    }
    
    public class source{
        public string srcSystem;
        public string programCode;
        public string comments;
        public string uniqueTransactionId;
        public string communicationId;
        public string communicationStatus;
    }
    
    public class communication{
        public string type;
        public string subject;
        public string content;
        public DateTime triggerDateTime;
        public string direction;       
        public string hasAttachment;
        public Sender sender;
        public Receiver receiver;
    }
   
    public class sender{
        public decimal id;
        public string fullName;
        public string email;
    }
    
    public class receiver{
        public decimal id;
        public string fullName;
        public string email;
    }
}