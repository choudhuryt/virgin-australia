@isTest
global class TravelBankServiceMockImpl implements WebServiceMock {
   global void doInvoke(Object stub,  
         Object request,  
         Map<String, object> response,  
         String endpoint,  
         String soapAction,  
         String requestName,  
         String responseNS,  
         String responseName,  
         String responseType) {
     //call demo function
     TravelBank.SalesforceTravelBankService sfTravelBankService =  new TravelBank.SalesforceTravelBankService();
     TravelBankModel.UpdateTravelBankAccountBalanceRSType respElement = sfTravelBankService.UpdateTravelBankAccountBalanceDemo();
     System.debug('TravelBankServiceMockImpl.doInvoke(), respElement= ' + respElement.AccountNumber);
     response.put('response_x', respElement); 
   }

}