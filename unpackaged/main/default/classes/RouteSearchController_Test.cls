/*
 * Author: Warjie Malibago (Accenture CloudFirst)
 * Date: June 7, 2016
 * Description: Test class for RouteSearchController
*/
@isTest
private class RouteSearchController_Test {

	private static testMethod void test() {
        Airport_Codes__c ac = new Airport_Codes__c(
            Name = 'Test',
            Country__c = 'Afghanistan',
            City__c = 'Bamiyan',
            Airport_Code__c = 'BIN',
            Airport__c = 'Bamiyan'
        );
        insert ac;
        
        RouteSearchController rsc = new RouteSearchController();
        rsc.qry = 'BIN';
	}
}