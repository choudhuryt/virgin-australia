/**
* @description       : FlyPlusToAccelerateAutomation
* @Updated By        : Cloudwerx
**/
public class FlyPlusToAccelerateAutomation {
    
    // @Updated By: Cloudwerx : Here we added code to get record type ids dynamically
    public static Id vaUserId = Utilities.getOwnerId(System.Label.Virgin_Australia_Business_Flyer_User_Id);
    public static Id accountFlyPlusRecordTypeId = Utilities.getRecordTypeId('Account', 'FlyPlus');
    public static Id contractFlyPlusRecordTypeId = Utilities.getRecordTypeId('Contract', 'FlyPlus');
    public static Id accountAccelerateRecordTypeId = Utilities.getRecordTypeId('Account', 'Accelerate');
    public static Id contractAcceleratePOSRebateRecordTypeId = Utilities.getRecordTypeId('Contract', 'Accelerate_POS_Rebate');
    // @Updated By: Cloudwerx : Here we added code to get record type ids dynamically
    @InvocableMethod
    public static void UpdateFlyPlusContractandNotifyKeyContact(List<Id> Accids) {
        List<Contract> contractList = new List<Contract>(); 
        List<Contract> lstContractUpdate = new List<Contract>(); 
        List<Account>  lstAccountUpdate = new List<Account>();     
        
        // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id in WHERE Clause
        List<Account>  flyplusaccountlist =  [SELECT Id , SmartFly_to_Accelerate_Movement_Date__c,
                                              Notify_Key_Contact__c,Reason_For_Termination__c,Business_Number__c
                                              FROM Account WHERE id =:accIds 
                                              AND Request_to_Move_to_Accelrate__c = true AND recordtypeid =:accountFlyPlusRecordTypeId]; 
        
        
        List<PartnerNetworkRecordConnection> accsharingconnection = new List<PartnerNetworkRecordConnection>();     
        List<PartnerNetworkRecordConnection> consharingconnection = new List<PartnerNetworkRecordConnection>(); 
        List<Account_Data_Validity__c> flyplusadvList = new List<Account_Data_Validity__c>();
        
        if(flyplusaccountlist.size() > 0) {
            accsharingconnection = [SELECT EndDate,Id, LocalRecordId, StartDate, Status 
                                    FROM PartnerNetworkRecordConnection 
                                    WHERE LocalRecordId = :accIds AND Status = 'Received' AND EndDate = null];  
            
            if (accsharingconnection.size() > 0) {
                //@updatedBy : cloudwerx : here we removed "DELETE" DML from loop
                DELETE accsharingconnection; 
            }
            
            consharingconnection = [SELECT EndDate,Id,LocalRecordId,StartDate,Status
                                    FROM PartnerNetworkRecordConnection
                                    WHERE LocalRecordId  IN (SELECT Id  FROM Contact WHERE AccountId = :accIds AND Key_Contact__c = true)                     
                                    AND Status = 'Received' AND EndDate = null];      
            
            if (consharingconnection.size() > 0) {
                //@updatedBy : cloudwerx : here we removed "DELETE" DML from loop
                DELETE consharingconnection;           
            }
            
            Date dadv= Date.today();
            flyplusadvList = [SELECT Id, To_Date__c FROM Account_Data_Validity__c 
                              WHERE From_Account__c =:accIds AND Market_Segment__c = 'FlyPlus' AND To_Date__c = null];
            
            if(flyplusadvList.size()>0) { 
                dadv = date.today().addMonths(1).toStartofMonth().addDays(-1);
                flyplusadvList[0].To_Date__c = dadv ;
                try {   
                    UPDATE flyplusadvList;
                } catch(DmlException e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }
                
                Account_Data_Validity__c advNew = new Account_Data_Validity__c();
                // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
                advNew.Account_Owner__c = vaUserId;
                // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id
                advNew.Account_Record_Type__c = accountAccelerateRecordTypeId;
                advNew.From_Account__c = flyplusaccountlist[0].id;
                advNew.From_Date__c = dadv.AddDays(1);
                advNew.Market_Segment__c =  'Accelerate';
                advNew.Sales_Matrix_Owner__c = 'Accelerate';            
                advNew.Account_Record_Type_Picklist__c = 'Accelerate' ;
                try {     
                    INSERT advNew;
                } catch(DmlException e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }
            } 
            
            List<Contract>  Acceleratecontract = new List<Contract>(); 
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id in WHERE Clause
            Acceleratecontract =  [SELECT Id, Status FROM Contract WHERE Accountid =:flyplusaccountlist[0].id  
                                   AND  contract.RecordTypeId =:contractAcceleratePOSRebateRecordTypeId LIMIT 1];
            
            Tourcodes__c ABNCode;
            try{
                ABNCode = (Tourcodes__c) [SELECT Id, Tourcode__c, Tourcode_Purpose__c, Status__c
                                          FROM Tourcodes__c WHERE Account__c =:flyplusaccountlist[0].id
                                          AND Tourcode_Purpose__c ='FlyPlus Tourcode'
                                          AND Tourcode__c = :flyplusaccountlist[0].Business_Number__c];
    
                ABNCode.Tourcode_Purpose__c = 'ABN Number';
                if( Acceleratecontract.size() > 0) {
                    ABNCode.Contract__c = Acceleratecontract[0].id;
                }
            } catch(Exception contactException){
                VirginsutilitiesClass.sendEmailError(contactException);   //************
            }
            
            try {
                UPDATE ABNCode ;
            } catch(Exception e){
                //VirginsutilitiesClass.sendEmailError(e);
            }
            
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id in WHERE Clause
            contractList = [SELECT id,EndDate FROM Contract WHERE
                            accountid = :accIds AND Status = 'Activated' AND
                            RecordTypeId =:contractFlyPlusRecordTypeId LIMIT 1]; 
            
            if(contractlist.size() > 0) {
                for(Contract c: contractlist) {
                    if(Date.Today().day() >= 19)  {    
                        c.EndDate = date.today().addMonths(1).toStartofMonth().addDays(-1);
                    } else {
                        c.EndDate = date.today().toStartofMonth().addDays(-1);      
                    }
                    C.Reason_for_Termination__c = flyplusaccountlist[0].Reason_For_Termination__c ; 
                    lstContractUpdate.add(c);  
                }
                try { 
                    UPDATE lstContractUpdate; 
                } catch(DmlException e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }      
            }
        }
        
        if( flyplusaccountlist.size() > 0 && flyplusaccountlist[0].Notify_Key_Contact__c  == 'Yes') {
            List<Contact> contactlist =   [SELECT id, AccountId, Email, FirstName FROM Contact
                                           WHERE AccountId = :accIds AND Key_Contact__c = true
                                           AND email != null AND Status__c = 'Active' ORDER BY CreatedDate DESC LIMIT 1]; 
            
            if(contactlist.size() > 0) {
                for(account acc: flyplusaccountlist) {
                    acc.Accelerate_Key_Contact_Email__c = contactlist[0].email ;
                    acc.Accelerate_Key_Contact_First_Name__c = contactlist[0].FirstName ;
                    lstAccountUpdate.add(acc);  
                }
                try { 
                    UPDATE lstAccountUpdate;
                } catch(DmlException e)  {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }    
            } 
        }
    }
  }