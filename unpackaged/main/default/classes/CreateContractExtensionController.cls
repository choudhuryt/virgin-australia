public with sharing class CreateContractExtensionController
{
public Contract_Extension__c ce{get;set;}    
public List <Contract_Addendum__c> calist {get;set;}    
public Contract_Addendum__c ca{get;set;}   
List<Contract> affectedContract = new List<Contract>();
List<Contract_Extension__c> ContractExtensionMaxVal = new List<Contract_Extension__c>();
 integer maxvalue ;
public CreateContractExtensionController(ApexPages.StandardController controller)
{
    this.ca= (Contract_Addendum__c)controller.getRecord();    
    ce = new Contract_Extension__c();
}

public PageReference initDisc() 
{
  calist = [SELECT Contract__c,Cos_Version__c,Domestic_Guideline_Tier__c,Domestic_Requested_Tier__c,Is_the_contract_being_extended__c,
                                      Red_Circle__c,Regional_Guideline_Tier__c ,Regional_Requested_Tier__c,Status__c,IsEditable__c
                                      FROM Contract_Addendum__c WHERE
                                       Id =:ApexPages.currentPage().getParameters().get('id')
                                      and Is_the_contract_being_extended__c = 'Yes'];   
    
if (calist.size() > 0 )
{   
  ca = calist.get(0);
    
  affectedContract = [SELECT Id, Max_Extension_Version__c, AccountId  FROM Contract  WHERE
                                           Id =:calist[0].Contract__c ];
    
  List<aggregateResult> maxverlist =[SELECT Max(Extension_Version__c)maxver  
                                                           FROM Contract_Extension__c 
                                                           where Contract__c = :calist[0].Contract__c
                                                           and Status__c not in ('Rejected', 'Deactivated') ]; 
      
      
        if (maxverlist.size() > 0)
     {    
     for(AggregateResult ag :maxverlist)
     {
       maxvalue     =   Integer.valueOf(ag.get('maxver')==null?0:ag.get('maxver'));
     }
     }   
    
  if  (calist.size() > 0 && affectedContract.size() > 0)
  {
      
       ce.Account__c = affectedContract[0].AccountId;
       ce.Contract__c = affectedContract[0].id;
       ce.Extension_Version__c =  maxvalue +1  ;
       ce.Addendum_Extension__c = true;
       ce.Status__c = 'Draft';
  }else
  {
     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'This addendum does not have extension option selected'));
   
  }
    
}    
else
{
     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'This addendum does not have extension option selected'));
}
   return null;        
}    

    
public PageReference save() {
    
    
    upsert  ce;
    ca.Contract_Extension__c = ce.Id ;
    upsert ca;
    
    PageReference pageRef = new PageReference('/'+ca.Id);
    pageRef.setRedirect(true);
    return pageRef;
}
    
public PageReference cancel() {
 
   PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
   thePage.setRedirect(true);
   return thePage;
   
  }
}