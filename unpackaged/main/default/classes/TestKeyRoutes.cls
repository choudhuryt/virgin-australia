@isTest

////////////////////////////////
//  Test method for KeyRoutes
////////////////////////////////

public with sharing class TestKeyRoutes {


    static testMethod void myUnitTest() {
        
        
        
        //////////////////
        //create a user that is both in prod and sandbox to clone from - leads need to be created by a user
        //////////////////
        User initUser = [SELECT Id, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey FROM User WHERE Alias='estac'];
        
         //////////////////
         //Create a User ui
         //////////////////
        User u1 = initUser.clone();
        u1.Alias = 'Joe';
        u1.LastName = 'Smith';
        u1.Email = 'joe@virginaustralia.com';
        u1.Username = 'joe@virginaustralia.com';
        u1.CommunityNickname = 'Joe';           
        insert u1;
        
         
 
        /////////////////////
        // Create a Lead l1
        /////////////////////
         Lead l1 = new Lead();
         l1.OwnerId = u1.Id;     
         l1.LastName = 'test500'; 
         l1.Company = 'TEST'; 
         l1.Lead_Type__c = 'Corporate';
         l1.Target_Markets__c = 'Thailand';
         l1.Estimated_Spend__c = '1m+';
         l1.Travel_Policy__c = 'Flexi Only';
         l1.Booked_Via__c = 'TMC';
         l1.Status = 'New';
         l1.Status = 'Qualified';
         l1.Country = 'AU';
         insert l1;
        
        /////////////////////
        // Create an Account a1
        /////////////////////
         Account a1 = new Account();
         a1.OwnerId = u1.Id;     
         a1.Target_Markets__c = 'Thailand';
         a1.Estimated_Spend__c = '1m+';
         a1.Travel_Policy__c = 'Flexi Only';
         a1.Booked_Via__c = 'TMC';
         a1.Name = 'TEST177';
         a1.BillingCountry = 'AU';
         a1.BillingPostalCode ='4000';
         a1.BillingState ='QLD';
         a1.BillingStreet = 'Test';
         a1.BillingState = 'QLD';
         a1.Billing_Country_Code__c = 'AU';
//MOD-BEGIN CILAG INS CASE# 00129600 09.23.16
         a1.BillingCity = 'MAL';
         a1.Industry_Type__c = 'Engineering';
         a1.Account_Lifecycle__c = 'Offer';
//MOD-END CILAG INS CASE# 00129600 09.23.16
         insert a1; 
         
        ///////////////////
        // Creating Key Domestic Routes for *Lead* via Type(s)
        // getting the ID of the type: Domestic
        ///////////////////
        RecordType rti = [
        select id, name 
        from recordtype 
        where sobjecttype='Key_Routes__c'
        AND DeveloperName = 'Domestic'
        ][0];
        
          ///////////////////
        // Creating Key Routes for *Lead* via Type(s)
        // getting the ID of the type: International
        ///////////////////
        RecordType iLi = [
        select id, name 
        from recordtype 
        where sobjecttype='Key_Routes__c'
        AND DeveloperName = 'International'
        ][0];
        
         ///////////////////
        // Creating Key Domestic Routes for *Account* via Type(s)
        // getting the ID of the type: Domestic
        ///////////////////
        RecordType mti = [
        select id, name 
        from recordtype 
        where sobjecttype='Key_Routes__c'
        AND DeveloperName = 'Domestic'
        ][0];
        
        ///////////////////
        // Creating Key Routes for *Account* via Type(s)
        // getting the ID of the type: International
        ///////////////////
        RecordType iAi = [
        select id, name 
        from recordtype 
        where sobjecttype='Key_Routes__c'
        AND DeveloperName = 'International'
        ][0];


        //Create Domestic Route for a Lead
        Key_Routes__c x1 = new Key_Routes__c();
        x1.Country__c = 'Australia';
        x1.Destination__c = 'Brisbane';
        x1.Destination_Identifier__c = 'BNE';
        x1.Origin__c = 'Sydney';
        x1.Origin_Identifier__c = 'SYD';
        x1.recordTypeId = rti.Id;      //rti
        x1.Lead__c = l1.Id;   
        insert x1; 
        
        //Create International Route for a Lead
        Key_Routes__c iL1 = new Key_Routes__c();
        iL1.Country__c = 'Australia';
        iL1.Destination__c = 'Brisbane';
        iL1.Destination_Identifier__c = 'BNE';
        iL1.Origin__c = 'LosAngeles';
        iL1.Origin_Identifier__c = 'LAX';
        iL1.recordTypeId = iLi.Id;      //iLi International Lead
        iL1.Lead__c = l1.Id;   
        insert iL1;       //was x1
        update iL1;
        
           
        
        
        //Create Domestic Route for an Account
        Key_Routes__c z1 = new Key_Routes__c();
        z1.Country__c = 'Australia';
        z1.Destination__c = 'Brisbane';
        z1.Destination_Identifier__c = 'BNE';
        z1.Origin__c = 'Sydney';
        z1.Origin_Identifier__c = 'SYD';
        z1.recordTypeId = mti.Id;   //mti
        z1.Account__c = a1.Id;  
        insert z1; 
        
        
          //Create International Route for an Account
        Key_Routes__c K1 = new Key_Routes__c();
        K1.Country__c = 'Australia';
        K1.Destination__c = 'Brisbane';
        K1.Destination_Identifier__c = 'BNE';
        K1.Origin__c = 'Los Angeles';
        K1.Origin_Identifier__c = 'LAX';
        K1.recordTypeId = iAi.Id;    
        K1.Account__c = a1.Id;  
        insert K1;
       ///////CB
      
     Test.startTest();
        ///////////

        
        //Instantiate a Ref VisualForce Page
        PageReference  ref1 = Page.routes2; //Lead
        PageReference  ref2 = Page.routes1; //Account
        
        
        //////// TEST LEAD ///////////////////////////////////////////
        
        //Test the controller for each page
        //routes2 for Leads
        Test.setCurrentPage( ref1 );
        ApexPages.currentPage().getParameters().put('id', l1.id );
        
        //instantiate the routeController for Lead
        ApexPages.StandardController conL = new ApexPages.StandardController(l1);
        routeController2 lController = new routeController2(conL);
        
        // Add Key Route Lead
         ref1 = lController.initRoutes();//Lead

         ref1 = lController.quickSaveAll();  //Lead
        // ref1 = lController.remDom();    //Lead
       //  ref1 = lController.remInt();    //Lead

         ref1 = lController.addDom();    //Lead
        
         //Create Domestic Route for a Lead
        Key_Routes__c g1 = new Key_Routes__c();
        g1.Country__c = 'Australia';
        g1.Destination__c = 'Brisbane';
        g1.Destination_Identifier__c = 'BNE';
        g1.Origin__c = 'Sydney';
        g1.Origin_Identifier__c = 'SYD';
        g1.recordTypeId = rti.Id;      //rti
        g1.Lead__c = l1.Id;   
        insert g1; 
         
       ref1 = lController.quickSaveAll();  //Lead
      
        //Create a Bad Domestic Route for a Lead
        Key_Routes__c b1 = new Key_Routes__c();
       // b1.Country__c = 'Australia';
        b1.Destination__c = 'Brisbane';
        b1.Destination_Identifier__c = 'BNE';
        b1.Origin__c = 'Los Angeles';
        b1.Origin_Identifier__c = 'LAX';
        b1.recordTypeId = rti.Id;      //rti
        b1.Lead__c = l1.Id;   
        insert b1;
        lController.domRoute.add(b1);

       //Create International Route for a Lead
        Key_Routes__c iL2 = new Key_Routes__c();
        iL2.Country__c = 'Australia';
        iL2.Destination__c = 'Brisbane';
        iL2.Destination_Identifier__c = 'BNE';
        iL2.Origin__c = 'LosAngeles';
        iL2.Origin_Identifier__c = 'LAX';
        iL2.recordTypeId = iLi.Id;      //iL2 International Lead
        iL2.Lead__c = l1.Id;   
        insert iL2;       //was x1
        update iL2;
        lController.domRoute.add(iL2);
        
        ref1 = lController.saveAll();   //lead
        lController.intRoute.add(iL2); 
         
         //Various for Lead
         
        ref1 = lController.initRoutes();  
        ref1 = lController.quickSaveAll();  //Lead
       
         ref1 = lController.addInt(); //Lead
          //Create a Bad Domestic Route for a Lead
        Key_Routes__c f1 = new Key_Routes__c();
        f1.Country__c = 'Australia';
        f1.Destination__c = 'Brisbane';
        f1.Destination_Identifier__c = 'BNE';
        f1.Origin__c = 'Los Angeles';
        f1.Origin_Identifier__c = 'LAX';
        f1.recordTypeId = rti.Id;      //rti
        f1.Lead__c = l1.Id;   
        insert f1;
        
         ref1 = lController.quickSaveAll();  //Lead
         ref1 = lController.initRoutes();
         
        ref1 = lController.addDom(); //Lead
          //Create a Bad Domestic Route for a Lead
        Key_Routes__c m1 = new Key_Routes__c();
        m1.Country__c = 'Australia';
        m1.Destination__c = 'Brisbane';
        m1.Destination_Identifier__c = 'BNE';
        m1.Origin__c = 'Los Angeles';
        m1.Origin_Identifier__c = 'LAX';
        m1.recordTypeId = rti.Id;      //rti
        m1.Lead__c = l1.Id;   
        insert m1;
        ref1 = lController.saveAll();   //lead
         
         lController.getName();  //Lead
         //
         
        ref1 = lController.saveInt127();
        ref1 = lController.saveDom127();
         
         
         // Remove Key Route Lead
          ref1 = lController.remInt();    //Lead
          ref1 = lController.remDom();    //Lead
          
        
        ////////// TEST ACCOUNT ////////////////////////////////////////////
        //routes1 for Accounts
        Test.setCurrentPage( ref2 );
        ApexPages.currentPage().getParameters().put('id', a1.id );
 
        //instantiate the routeController for Account
        ApexPages.StandardController conA = new ApexPages.StandardController(a1);
        routeController1 aController = new routeController1(conA);
        
        
         //Account add Domestic and International Routes
         ref2 = aController.initRoutes();//Account
         ref2 = aController.quickSaveAll();
         ref2 = aController.addDom();
        //
        Key_Routes__c t1 = new Key_Routes__c();
        t1.Country__c = 'Australia';
        t1.Destination__c = 'Sydney';
        t1.Destination_Identifier__c = 'SYD';
        t1.Origin__c = 'Brisbane';
        t1.Origin_Identifier__c = 'BNE';
        t1.recordTypeId = mti.Id;   //mti
        t1.Account__c = a1.Id;  
        insert t1; 
        
        aController.domRoute.add(t1);
        ref2 = aController.quickSaveAll();  //Account
         
       
         
        ref2 = aController.initRoutes();
         
         
          Key_Routes__c d1 = new Key_Routes__c();
        d1.Country__c = 'Australia';
        d1.Destination__c = 'Sydney';
        d1.Destination_Identifier__c = 'SYD';
        d1.Origin__c = 'Sydney';
        d1.Origin_Identifier__c = 'SYD';
        d1.recordTypeId = iai.Id;   //mti
        d1.Account__c = a1.Id;  
        insert d1; 
        
        ref2 = aController.addInt(); //Account 
         
         
         //d1=(origin[0].Country__c == 'Australia' && destination[0].Country__c == 'Australia');
         aController.intRoute.add(d1);
         aController.domRoute.add(d1);
        
        
         ref2 = aController.saveAll();
         ref2 = aController.initRoutes();//Account //
         aController.getName();         //Account
         
        
        ref2 = aController.addDom(); //Account
//
        
        ref2 = aController.saveInt127();
        ref2 = aController.saveDom127();

        // Remove Key Route Account
        ref2 = aController.remInt();    //Account
        ref2 = aController.remDom();    //Account
   
       
        
        // Stop test
        test.stopTest();
        
    }







}