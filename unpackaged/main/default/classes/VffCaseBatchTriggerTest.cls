/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 30.11.2021
**Description: Test Class to cover Classes-'CaseAttachmentDeletionBatch', 'CaseAttachmentDeletionScheduler' & Triggers - 'CaseResolutionTimeTrigger','CaseCreateOrUpdateContact'
*/
@isTest
public class VffCaseBatchTriggerTest {
    
    static testMethod void batchSuccessScenario(){
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        List<ContentDocumentLink> contentLinkList = new List<ContentDocumentLink>();
        List<EmailMessage> msgList = new List<EmailMessage>();
        List<Attachment> attList = new List<Attachment>();
                        
        for(integer i=0; i<5;i++){
            Case cs = new Case();
        	cs.RecordTypeId = caseRTId;
        	cs.Status = 'Closed';
            caseList.add(cs);
        }
        insert caseList; System.debug('caseList--'+caseList);
                
        Blob bodyBlob = Blob.valueOf('Content of the document for the Test Class for Batch Class');
        ContentVersion cv = new ContentVersion(
            Title = 'Content Document',
            PathonClient = 'Content Document.txt',
            VersionData = bodyBlob,
            origin = 'H'
        );
        insert cv;
        
        ContentVersion cd = [Select Id, ContentDocumentId from ContentVersion where Id =:cv.Id LIMIT 1];
        
        for(integer j=0; j<5; j++){
            ContentDocumentLink cl = new ContentDocumentLink();
            cl.LinkedEntityId = caseList[j].Id;
            cl.contentdocumentid = cd.ContentDocumentId;
            cl.ShareType = 'V';
            contentLinkList.add(cl);
        }
        insert contentLinkList;
        
        for(integer k=0; k<5; k++){
            EmailMessage em = new EmailMessage();
            em.Subject = 'Test Email'+k;
            em.RelatedToId = caseList[k].Id;
            msgList.add(em);
        }
        insert msgList;
        
        for(integer l=0;l<5;l++){
            Attachment att = new Attachment();
            att.Name = 'Email Attachment'+l;
            att.ParentId = msgList[l].Id;
            att.Body = bodyBlob;
            attList.add(att);
        }
        insert attList;
        
        Test.startTest();
        	CaseAttachmentDeletionBatch obj = new CaseAttachmentDeletionBatch();
        	DataBase.executeBatch(obj);
        
        Test.stopTest();
    }
    
    static testMethod void batchSchedulerScenario(){
        Test.startTest();
        CaseAttachmentDeletionScheduler sched = new CaseAttachmentDeletionScheduler();
        String sch = '0 0 23 * * ?';
        System.schedule('Test Status Check', sch, sched);
        Test.stopTest();
    }    
    
    static testMethod void vffTriggerScenarioBeforeUpdate1(){
        
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();        
        List<String> queueList = new List<String>{'Red','Gold','Silver','Platinum','Retro'};
        List<Case> insertCaseList = new List<Case>();
        List<Case> updateCaseList = new List<Case>();
                
        Test.startTest();
        Case trigCase = new Case();
        trigCase.RecordTypeId = caseRTId;
        trigCase.Status = 'New';
        trigCase.SuppliedEmail = 'abc@test.com';
        trigCase.Contact_Email__c = 'abc@test.com';
        trigCase.Velocity_Type__c = 'Gold';
        trigCase.OwnerId = '00G2t000000NTKOEA4';
        insert trigCase;
        
        trigCase.IsStopped = false;
        update trigCase;        
        
        Test.stopTest();
    }
    
    static testMethod void vffTriggerScenarioBeforeUpdate2(){
        
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();        
        List<String> queueList = new List<String>{'Red','Gold','Silver','Platinum','Retro'};
        List<Case> insertCaseList = new List<Case>();
        List<Case> updateCaseList = new List<Case>();
                
        Test.startTest();
        Case trigCase = new Case();
        trigCase.RecordTypeId = caseRTId;
        trigCase.Status = 'New';
        trigCase.SuppliedEmail = 'abc@test.com';
        trigCase.Contact_Email__c = 'abc@test.com';
        trigCase.Velocity_Type__c = 'Platinum';
        trigCase.OwnerId = '00G2t000000NTKOEA4';
        insert trigCase;
        
        trigCase.Velocity_Type__c = 'VIP';
        update trigCase;
        
        Test.stopTest();
    }
    
    static testMethod void vffTriggerScenarioBeforeUpdate3(){
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();        
        List<String> queueList = new List<String>{'Red','Gold','Silver','Platinum','Retro'};
        List<Case> insertCaseList = new List<Case>();
        List<Case> updateCaseList = new List<Case>();
        
        Test.startTest();
        Case trigCase = new Case();
        trigCase.RecordTypeId = caseRTId;
        trigCase.Status = 'Re-Opened';
        trigCase.Case_Re_Opened_Date__c = System.now();
        trigCase.SuppliedEmail = 'abc@test.com';
        trigCase.Contact_Email__c = 'abc@test.com';
        trigCase.Velocity_Type__c = 'Retro';
        trigCase.OwnerId = '0052t000000JttPAAS';
        trigCase.Agent_Assigned_Time__c = System.now();
        trigCase.First_Response__c = false;
        trigCase.IsStopped = true;
        insert trigCase;
        
        trigCase.Velocity_Type__c = 'Gold';
        trigCase.IsStopped = false;
        TrigCase.SLA_Paused_in_Hrs__c = 0.01;
        update trigCase;
        
        trigCase.First_Response__c = true;
        trigCase.First_Response_Time__c = System.now();
        update trigCase;
              
        Test.stopTest();
    }
    
    static testMethod void vffTriggerScenarioAfterUpdate1(){
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();        
        List<String> queueList = new List<String>{'Red','Gold','Silver','Platinum','Retro'};
        List<Case> insertCaseList = new List<Case>();
        List<Case> updateCaseList = new List<Case>();
                
        Test.startTest();
        Case trigCase = new Case();
        trigCase.RecordTypeId = caseRTId;
        trigCase.Status = 'New';
        trigCase.SuppliedEmail = 'abc@test.com';
        trigCase.Contact_Email__c = 'abc@test.com';
        trigCase.Velocity_Type__c = 'Red';
        trigCase.OwnerId = '0052t000000JttPAAS';
        trigCase.Agent_Assigned_Time__c = System.now();
        insert trigCase;
        
        trigCase.First_Response__c = true;
        trigCase.First_Response_Time__c = System.now();
        update trigCase;
        
        trigCase.Status = 'Closed';
        update trigCase;
        
        trigCase.Status = 'Re-Opened';
        update trigCase;
        
        Test.stopTest();
    }
    
    static testMethod void vffTriggerScenarioAfterUpdate2(){
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();        
        List<String> queueList = new List<String>{'Red','Gold','Silver','Platinum','Retro'};
        List<Case> insertCaseList = new List<Case>();
        List<Case> updateCaseList = new List<Case>();
                
        Test.startTest();
        Case trigCase = new Case();
        trigCase.RecordTypeId = caseRTId;
        trigCase.Status = 'New';
        trigCase.SuppliedEmail = 'abc@test.com';
        trigCase.Contact_Email__c = 'abc@test.com';
        trigCase.Velocity_Type__c = 'Red';
        trigCase.OwnerId = '0052t000000JttPAAS';
        trigCase.Agent_Assigned_Time__c = System.now();
        TrigCase.SLA_Paused_in_Hrs__c = 0;
        insert trigCase;
        
        trigCase.Status = 'Closed';
        update trigCase;
        
        Test.stopTest();
    }
}