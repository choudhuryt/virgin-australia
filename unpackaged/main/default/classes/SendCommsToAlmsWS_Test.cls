/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 22.03.2022
**Description: Test Class for SendCommsToAlmsWS, GetAccessToken, SendCasetoAlmsTrigger, SendEmailtoAlmsTrigger, SendChattoAlmsTrigger
*/
@isTest
public class SendCommsToAlmsWS_Test {
    Class SingleRequestMock implements HttpCalloutMock{
        protected Integer code;
        protected String status;
        protected String bodyAsString;
        protected Blob bodyAsBlob;
        protected Map<String, String> responseHeaders;
        
        public SingleRequestMock(Integer code, String status, String body, Map<String, String> responseHeaders){
            this.code = code;
            this.status = status;
            this.bodyAsBlob = null;
            this.bodyAsString = body;
            this.responseHeaders = responseHeaders;
        }
        public SingleRequestMock(Integer code, String status, Blob body, Map<String, String> responseHeaders){
            this.code = code;
            this.status = status;
            this.bodyAsBlob = body;
            this.bodyAsString = null;
            this.responseHeaders = responseHeaders;
        }
        
        public HTTPResponse respond(HTTPRequest req){
            HTTPResponse resp = new HttpResponse();
            resp.setStatusCode(code);
            resp.setStatus(status);
            if(bodyAsBlob != null){
                resp.setBodyAsBlob(bodyAsBlob);
            }else{
                resp.setBody(bodyAsString);
            }
            if(responseHeaders != null){
                for(String key: responseHeaders.keySet()){
                    resp.setHeader(key, responseHeaders.get(key));
                }
            }
            return resp;
        }        
    }
    @isTest
    static void SendCommsToAlmsWS_SuccessScenario1(){
        String body = '{ "data": { "success": "true", "id": "500O000000NcBsfIAF", "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "RED", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "MOBILE", "countryCallingCode": "61", "number": "0478704103", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } }, { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Ok',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'VA Admin');
        insert r;
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Integration Profile'].Id,
            LastName = 'Test User',
            Email = 'test123@abc.com',
            Username = 'test123@abc.com' + System.currentTimeMillis(),
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        List<EmailMessage> emailList=new List<EmailMessage>();
        List<LiveChatTranscript> chatList = new List<LiveChatTranscript>();
        List<ContentDocumentLink> contentLinkList = new List<ContentDocumentLink>();
        List<Attachment> attList = new List<Attachment>();
        List<Id> idList=new List<Id>();
        List<Id> casQList = new List<Id>();
        List<task> taskList = new List<task>();
        List<Comms_Log__c> logList = new List<Comms_Log__c>();
        List<String> newCase = new List<String>();
        List<String> velocityNumber = new List<String>();
        
        Blob bodyBlob = Blob.valueOf('Content of the document for the Test Class');
        
        System.runAs(u){            
            
            Group grp = new Group(Name='VFF Red Queue', type='Queue');
            Insert grp;
            QueuesObject que = new QueueSObject(QueueID = grp.id, SobjectType = 'Case');
            Insert que;
            
            Contact con = new Contact();
            con.FirstName = 'Abc1';
            con.LastName = 'Test';
            con.Email = 'abc1@test.com';
            con.Velocity_Number__c = '0000360491';
            insert con;
            
            ContentVersion cv = new ContentVersion(
                Title = 'Content Document',
                PathonClient = 'Content Document.txt',
                VersionData = bodyBlob,
                origin = 'H'
            );
            insert cv;
            
            ContentVersion cd = [Select Id, ContentDocumentId from ContentVersion where Id =:cv.Id LIMIT 1];
            
            for(integer i=0; i<5;i++){
                Case cs = new Case();
                cs.RecordTypeId = caseRTId;
                cs.Velocity_Number__c = '0000360491';
                if(i==0){
                    cs.ContactId = con.Id;
                    cs.OwnerId = grp.Id;
                    cs.Subject = 'Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A';
                    cs.Description = 'Thank you for submitting your enquiry to Velocity Frequent Flyer, please note reference number #01769645 with our Membership Contact Centre Team as confirmation. We aim to respond to your enquiry within  5 business days. If your matter is urgent and requires an urgent response, please contact us on one of the phone numbers listed below for immediate assistance. Please take this email as confirmation that your enquiry has been received and is in queue.  We ask that you do not submit another enquiry unless it is for a different booking or enquiry other than what has already been submitted. Thank you again for submitting your enquiry with Velocity Frequent Flyer. Kind Regards, Velocity Membership Contact Centre: Australia – 13 18 75    &#124;    New Zealand – 0800 230 875 Everywhere else – +61 2 8667 5924 Membership enquiries: Monday to Friday from 8am to 8pm AEDT Flight redemptions: Monday to Friday from 8am to 8pm AEDT, Saturday and Sunday from 9am to 6pm AEDT ______________________________________________________________________________________________ This message is being sent to you in response to an enquiry you addressed to Velocity Frequent Flyer and is being sent by Velocity Frequent Flyer Pty Limited ACN 601 408 824 of PO Box 1034 Spring Hill Qld 4004.Thank you for submitting your enquiry to Velocity Frequent Flyer, please note reference number #01769645 with our Membership Contact Centre Team as confirmation. We aim to respond to your enquiry within  5 business days. If your matter is urgent and requires an urgent response, please contact us on one of the phone numbers listed below for immediate assistance. Please take this email as confirmation that your enquiry has been received and is in queue.  We ask that you do not submit another enquiry unless it is for a different booking or enquiry other than what has already been submitted. Thank you again for submitting your enquiry with Velocity Frequent Flyer. Kind Regards, Velocity Membership Contact Centre: Australia – 13 18 75    &#124;    New Zealand – 0800 230 875 Everywhere else – +61 2 8667 5924 Membership enquiries: Monday to Friday from 8am to 8pm AEDT Flight redemptions: Monday to Friday from 8am to 8pm AEDT, Saturday and Sunday from 9am to 6pm AEDT ______________________________________________________________________________________________ This message is being sent to you in response to an enquiry you addressed to Velocity Frequent Flyer and is being sent by Velocity Frequent Flyer Pty Limited ACN 601 408 824 of PO Box 1034 Spring Hill Qld 4004.Thank you for submitting your enquiry to Velocity Frequent Flyer, please note reference number #01769645 with our Membership Contact Centre Team as confirmation. We aim to respond to your enquiry within  5 business days. If your matter is urgent and requires an urgent response, please contact us on one of the phone numbers listed below for immediate assistance. Please take this email as confirmation that your enquiry has been received and is in queue.  We ask that you do not submit another enquiry unless it is for a different booking or enquiry other than what has already been submitted. Thank you again for submitting your enquiry with Velocity Frequent Flyer. Kind Regards, Velocity Membership Contact Centre: Australia – 13 18 75    &#124;    New Zealand – 0800 230 875 Everywhere else – +61 2 8667 5924 Membership enquiries: Monday to Friday from 8am to 8pm AEDT Flight redemptions: Monday to Friday from 8am to 8pm AEDT, Saturday and Sunday from 9am to 6pm AEDT ______________________________________________________________________________________________ This message is being sent to you in response to an enquiry you addressed to Velocity Frequent Flyer and is being sent by Velocity Frequent Flyer Pty Limited ACN 601 408 824 of PO Box 1034 Spring Hill Qld 4004.';
                	cs.Origin = 'Email';
                }else{
                    if(i==1){
                        cs.OwnerId = u.Id;
                        cs.Subject = 'Subject Test';
                        cs.Origin = 'Web';
                        cs.SuppliedEmail = 'abc@test.com';
                    }                  
                    cs.Description = 'Test Body';
                }        
                caseList.add(cs);
            }
            insert caseList; 
            newCase.add(caseList[0].Id);
            velocityNumber.add(caseList[0].Velocity_Number__c);
                        
            integer cn =0;
            for (Case cs:caselist){	
                if(cs.OwnerId == grp.Id){
                    casQList.add(cs.Id);
                }
                if(cn == 0){
                    CaseComment caco = new CaseComment();
                    caco.CommentBody = 'Comment A Comment A Comment A Comment A Comment A Comment A Comment A Comment A Comment A Comment A Comment A';
                    caco.ParentId = cs.Id;
                    insert caco;
                }else{
                    CaseComment caco = new CaseComment();
                    caco.CommentBody = 'Comment A';
                    caco.ParentId = cs.Id;
                    insert caco;
                    
                    CaseComment caco1 = new CaseComment();
                    caco1.CommentBody = 'Comment B';
                    caco1.ParentId = cs.Id;
                    insert caco1;
                }                
                cn++;
                idList.add(cs.id);
            }
            
            for(integer j=0; j<3; j++){
                ContentDocumentLink cl = new ContentDocumentLink();
                cl.LinkedEntityId = caseList[j].Id;
                cl.contentdocumentid = cd.ContentDocumentId;
                cl.ShareType = 'V';
                contentLinkList.add(cl);
            }
            insert contentLinkList;
            
            for(integer n=0; n<5; n++){
                EmailMessage em=new EmailMessage();
                if(n<3){
                    em.Incoming = true;
                    em.RelatedToId=casQList[0];
                    em.Subject='Test Email';
                    em.TextBody = 'Test Body for Email';
                }/*else if(n==3){
					em.RelatedToId = ts.Id;
				}*/else if(n==4){
    				em.RelatedToId=caseList[n].Id;
    				em.Subject='Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A';
    				em.TextBody = 'Thank you for submitting your enquiry to Velocity Frequent Flyer, please note reference number #01769645 with our Membership Contact Centre Team as confirmation. We aim to respond to your enquiry within  5 business days. If your matter is urgent and requires an urgent response, please contact us on one of the phone numbers listed below for immediate assistance. Please take this email as confirmation that your enquiry has been received and is in queue.  We ask that you do not submit another enquiry unless it is for a different booking or enquiry other than what has already been submitted. Thank you again for submitting your enquiry with Velocity Frequent Flyer. Kind Regards, Velocity Membership Contact Centre: Australia – 13 18 75    &#124;    New Zealand – 0800 230 875 Everywhere else – +61 2 8667 5924 Membership enquiries: Monday to Friday from 8am to 8pm AEDT Flight redemptions: Monday to Friday from 8am to 8pm AEDT, Saturday and Sunday from 9am to 6pm AEDT ______________________________________________________________________________________________ This message is being sent to you in response to an enquiry you addressed to Velocity Frequent Flyer and is being sent by Velocity Frequent Flyer Pty Limited ACN 601 408 824 of PO Box 1034 Spring Hill Qld 4004.Thank you for submitting your enquiry to Velocity Frequent Flyer, please note reference number #01769645 with our Membership Contact Centre Team as confirmation. We aim to respond to your enquiry within  5 business days. If your matter is urgent and requires an urgent response, please contact us on one of the phone numbers listed below for immediate assistance. Please take this email as confirmation that your enquiry has been received and is in queue.  We ask that you do not submit another enquiry unless it is for a different booking or enquiry other than what has already been submitted. Thank you again for submitting your enquiry with Velocity Frequent Flyer. Kind Regards, Velocity Membership Contact Centre: Australia – 13 18 75    &#124;    New Zealand – 0800 230 875 Everywhere else – +61 2 8667 5924 Membership enquiries: Monday to Friday from 8am to 8pm AEDT Flight redemptions: Monday to Friday from 8am to 8pm AEDT, Saturday and Sunday from 9am to 6pm AEDT ______________________________________________________________________________________________ This message is being sent to you in response to an enquiry you addressed to Velocity Frequent Flyer and is being sent by Velocity Frequent Flyer Pty Limited ACN 601 408 824 of PO Box 1034 Spring Hill Qld 4004.Thank you for submitting your enquiry to Velocity Frequent Flyer, please note reference number #01769645 with our Membership Contact Centre Team as confirmation. We aim to respond to your enquiry within  5 business days. If your matter is urgent and requires an urgent response, please contact us on one of the phone numbers listed below for immediate assistance. Please take this email as confirmation that your enquiry has been received and is in queue.  We ask that you do not submit another enquiry unless it is for a different booking or enquiry other than what has already been submitted. Thank you again for submitting your enquiry with Velocity Frequent Flyer. Kind Regards, Velocity Membership Contact Centre: Australia – 13 18 75    &#124;    New Zealand – 0800 230 875 Everywhere else – +61 2 8667 5924 Membership enquiries: Monday to Friday from 8am to 8pm AEDT Flight redemptions: Monday to Friday from 8am to 8pm AEDT, Saturday and Sunday from 9am to 6pm AEDT ______________________________________________________________________________________________ This message is being sent to you in response to an enquiry you addressed to Velocity Frequent Flyer and is being sent by Velocity Frequent Flyer Pty Limited ACN 601 408 824 of PO Box 1034 Spring Hill Qld 4004.';
				}                                
                emailList.add(em);
            }            
            insert emailList;
            
            LiveChatVisitor lcv = new LiveChatVisitor();
            insert lcv;
            
            for(integer t=0; t<3; t++){
                LiveChatTranscript lct=new LiveChatTranscript();
                if(t==0){
                    //lct.Customer_Name__c = 'Test ABC';
                    lct.Email__c = 'abc@test.com';
                    lct.Subject__c='Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A Subject A';
                    lct.Body = 'Thank you for submitting your enquiry to Velocity Frequent Flyer, please note reference number #01769645 with our Membership Contact Centre Team as confirmation. We aim to respond to your enquiry within  5 business days. If your matter is urgent and requires an urgent response, please contact us on one of the phone numbers listed below for immediate assistance. Please take this email as confirmation that your enquiry has been received and is in queue.  We ask that you do not submit another enquiry unless it is for a different booking or enquiry other than what has already been submitted. Thank you again for submitting your enquiry with Velocity Frequent Flyer. Kind Regards, Velocity Membership Contact Centre: Australia – 13 18 75    &#124;    New Zealand – 0800 230 875 Everywhere else – +61 2 8667 5924 Membership enquiries: Monday to Friday from 8am to 8pm AEDT Flight redemptions: Monday to Friday from 8am to 8pm AEDT, Saturday and Sunday from 9am to 6pm AEDT ______________________________________________________________________________________________ This message is being sent to you in response to an enquiry you addressed to Velocity Frequent Flyer and is being sent by Velocity Frequent Flyer Pty Limited ACN 601 408 824 of PO Box 1034 Spring Hill Qld 4004.Thank you for submitting your enquiry to Velocity Frequent Flyer, please note reference number #01769645 with our Membership Contact Centre Team as confirmation. We aim to respond to your enquiry within  5 business days. If your matter is urgent and requires an urgent response, please contact us on one of the phone numbers listed below for immediate assistance. Please take this email as confirmation that your enquiry has been received and is in queue.  We ask that you do not submit another enquiry unless it is for a different booking or enquiry other than what has already been submitted. Thank you again for submitting your enquiry with Velocity Frequent Flyer. Kind Regards, Velocity Membership Contact Centre: Australia – 13 18 75    &#124;    New Zealand – 0800 230 875 Everywhere else – +61 2 8667 5924 Membership enquiries: Monday to Friday from 8am to 8pm AEDT Flight redemptions: Monday to Friday from 8am to 8pm AEDT, Saturday and Sunday from 9am to 6pm AEDT ______________________________________________________________________________________________ This message is being sent to you in response to an enquiry you addressed to Velocity Frequent Flyer and is being sent by Velocity Frequent Flyer Pty Limited ACN 601 408 824 of PO Box 1034 Spring Hill Qld 4004.Thank you for submitting your enquiry to Velocity Frequent Flyer, please note reference number #01769645 with our Membership Contact Centre Team as confirmation. We aim to respond to your enquiry within  5 business days. If your matter is urgent and requires an urgent response, please contact us on one of the phone numbers listed below for immediate assistance. Please take this email as confirmation that your enquiry has been received and is in queue.  We ask that you do not submit another enquiry unless it is for a different booking or enquiry other than what has already been submitted. Thank you again for submitting your enquiry with Velocity Frequent Flyer. Kind Regards, Velocity Membership Contact Centre: Australia – 13 18 75    &#124;    New Zealand – 0800 230 875 Everywhere else – +61 2 8667 5924 Membership enquiries: Monday to Friday from 8am to 8pm AEDT Flight redemptions: Monday to Friday from 8am to 8pm AEDT, Saturday and Sunday from 9am to 6pm AEDT ______________________________________________________________________________________________ This message is being sent to you in response to an enquiry you addressed to Velocity Frequent Flyer and is being sent by Velocity Frequent Flyer Pty Limited ACN 601 408 824 of PO Box 1034 Spring Hill Qld 4004.';
                }else{
                    lct.Subject__c='Test Email';
                    lct.Body = 'Test Body for Chat';
                }                
                lct.Velocity_Number__c = '0000360491';
                lct.LiveChatVisitorId = lcv.Id;
                chatList.add(lct);
            }                        
            insert chatList;
            
            for(integer a=0; a<3; a++){
                Attachment att = new Attachment();
                if(a==0){
                    att.Name = 'Case Attachment';
                	att.ParentId = caseList[4].Id;
                }else if(a==1){
                    att.Name = 'Email Attachment';
                	att.ParentId = emailList[0].Id;
                }else{
                    att.Name = 'Chat Attachment';
                	att.ParentId = chatList[0].Id;
                }                
                att.Body = bodyBlob;
                attList.add(att);
            }
            insert attList;
            
            Case cas = new Case();
            cas.Id = caseList[0].Id;
            cas.Velocity_Number__c = '0000505115';
            Update cas;
            
            EmailMessage emsg = new EmailMessage();
            emsg.Id = emailList[0].Id;
            //emsg.ParentId = caseList[4].Id;
            Update emsg;
            
            LiveChatTranscript livch = new LiveChatTranscript();
            livch.Id = chatList[1].Id;
            livch.Body = 'Updated Chat transcript';
            livch.Status = 'Completed';
            update livch;
            
            livch.Velocity_Number__c = NULL;
            livch.ContactId = con.Id;
            Update livch;
            
            livch.Velocity_Number__c = NULL;
            livch.CaseId = caseList[0].Id;
            Update livch;
            
            for(integer x=0; x<5; x++){
                Comms_Log__c lg = new Comms_Log__c();
                if(x==1){
                    lg.Type__c = 'Case';
                    lg.Case__c = caseList[0].Id;
                }else if(x==2){
                    lg.Type__c = 'Email';
                    lg.Email__c = emailList[0].Id;
                }else if(x==3){
                    lg.Type__c = 'Chat';
                    lg.LiveChat__c = chatList[0].Id;
                }else{
                    lg.Type__c = 'Contact';
                    lg.Contact__c = con.Id;
                }                                
                lg.Status__c = 'Failed';
                lg.Request_Type__c = 'POST';
                lg.Velocity_Number__c = '0000505115';
                logList.add(lg);
            }
            Insert logList;

            String type = 'Chat';
            
            for(EmailMessage eml: emailList){
                idList.add(eml.Id);
            }
            
            for(LiveChatTranscript lvc: chatList){
                idList.add(lvc.Id);
            }
            
            Test.startTest();
            SendCommsToAlmsWS.sendtoAlms(idList,type);
            SendCommsToAlmsWS.sendNotification();

            Test.stopTest();
        }        
    }
    
    @isTest
    static void SendCommsToAlmsWS_SuccessScenario2(){
        String body = '{ "data": { "success": "true", "id": "500O000000NcBsfIAF", "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "RED", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "MOBILE", "countryCallingCode": "61", "number": "0478704103", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } }, { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Ok',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        List<LiveChatTranscript> chatList = new List<LiveChatTranscript>();
        
        LiveChatVisitor lcv = new LiveChatVisitor();
        insert lcv;
        
        Contact con = new Contact();
        con.FirstName = 'Abc1';
        con.LastName = 'Test';
        con.Email = 'abc1@test.com';
        con.Velocity_Number__c = '0000360491';
        insert con;
        
        LiveChatTranscript lct = new LiveChatTranscript();
        lct.Subject__c='Test Email';
        lct.Body = 'Test Body for Chat';
        lct.Email__c = 'abc@test.com';
        //lct.Velocity_Number__c = '1234567899';
        lct.LiveChatVisitorId = lcv.Id;
        lct.ContactId = con.Id;
        chatList.add(lct);
        insert chatList;
        
        LiveChatTranscript lct1 = new LiveChatTranscript();
        lct1.Id = chatList[0].Id;
        lct1.Status = 'Completed';
        update lct1;
        
        String type = 'Contact Chat';
        
        List<Id> idList=new List<Id>();
        
        idList.add(chatList[0].Id);
        
        Test.startTest();
        SendCommsToAlmsWS.sendtoAlms(idList,type);

        Test.stopTest();
    }
    @isTest
    static void SendCommsToAlmsWS_SuccessScenario3(){
        String body = '{ "data": { "success": "true", "id": "500O000000NcBsfIAF", "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "RED", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "MOBILE", "countryCallingCode": "61", "number": "0478704103", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } }, { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Ok',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'VA Admin');
        insert r;
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Integration Profile'].Id,
            LastName = 'Test User',
            Email = 'test123@abc.com',
            Username = 'test123@abc.com' + System.currentTimeMillis(),
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        List<LiveChatTranscript> chatList = new List<LiveChatTranscript>();
        System.runAs(u){
            
            Case cs = new Case();
            cs.RecordTypeId = caseRTId;
            cs.Velocity_Number__c = '0000360491';
            cs.Subject = 'Subject Test';
            cs.Description = 'Test Body';
            cs.OwnerId = u.Id;
            insert cs;
            
            cs.Velocity_Number__c = '0000505115';
            Update cs;
            
            LiveChatVisitor lcv = new LiveChatVisitor();
            insert lcv;
            
            LiveChatTranscript lct1=new LiveChatTranscript();
            lct1.Subject__c='Test Email1';
            lct1.Body = 'Test Body for Chat';
            lct1.LiveChatVisitorId = lcv.Id;
            lct1.CaseId = cs.Id;
            chatList.add(lct1);
            
            LiveChatTranscript lct2=new LiveChatTranscript();
            //lct2.Subject__c='Test Email2';
            lct2.Body = 'Test Body for Chat';
            lct2.LiveChatVisitorId = lcv.Id;
            //lct2.Customer_Name__c = 'Test 123';
            lct2.Email__c = 'test123@abc.com';
            lct2.CaseId = cs.Id;
            chatList.add(lct2);
            insert chatList;
        }
        
        String type = 'Case Chat';
        
        List<Id> idList=new List<Id>();
        for (LiveChatTranscript ch:chatList)
        {
            idList.add(ch.id);
        }
        
        Test.startTest();
        SendCommsToAlmsWS.sendtoAlms(idList,type);
        Test.stopTest();
    }
    @isTest
    static void SendCommsToAlmsWS_FailureScenario1(){
        String body = '{ "code": 4001, "title": "Validation Error", "detail": "Validation failed for below fields", "description": "description..", "status": 400, "errorFields": [ { "field": "data.source.uniqueTransactionId", "message": "size must be between 2 and 25" } ] }';
        SingleRequestMock fakeResponse = new SingleRequestMock(404,'Not Found',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        List<LiveChatTranscript> chatList = new List<LiveChatTranscript>();
        
        LiveChatVisitor lcv = new LiveChatVisitor();
        insert lcv;
        
        Contact con = new Contact();
        con.FirstName = 'Abc1';
        con.LastName = 'Test';
        con.Email = 'abc1@test.com';
        con.Velocity_Number__c = '0000360491';
        insert con;
        
        LiveChatTranscript lct = new LiveChatTranscript();
        lct.Subject__c='Test Email';
        lct.Body = 'Test Body for Chat';
        //lct.Velocity_Number__c = '1234567899';
        lct.LiveChatVisitorId = lcv.Id;
        lct.ContactId = con.Id;
        chatList.add(lct);
        insert chatList;
        
        String type = 'Contact Chat';
        
        List<Id> idList=new List<Id>();
        
        idList.add(chatList[0].Id);
        
        Test.startTest();
        SendCommsToAlmsWS.sendtoAlms(idList,type);
        Test.stopTest();
    }
    @isTest
    static void SendCommsToAlmsWS_FailureScenario2(){
        String body = '{"data": { "success": "false", "errorDetail": "error details"},"code": 4001, "title": "Validation Error", "detail": "Validation failed for below fields", "status": 400, "errorFields": [ { "field": "data.source.uniqueTransactionId", "message": "size must be between 2 and 25" } ] }';
        SingleRequestMock fakeResponse = new SingleRequestMock(205,'Not Found',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        List<LiveChatTranscript> chatList = new List<LiveChatTranscript>();
        
        LiveChatVisitor lcv = new LiveChatVisitor();
        insert lcv;
        
        Contact con = new Contact();
        con.FirstName = 'Abc1';
        con.LastName = 'Test';
        con.Email = 'abc1@test.com';
        con.Velocity_Number__c = '0000360491';
        insert con;
        
        LiveChatTranscript lct = new LiveChatTranscript();
        lct.Subject__c='Test Email';
        lct.Body = 'Test Body for Chat';
        //lct.Velocity_Number__c = '1234567899';
        lct.LiveChatVisitorId = lcv.Id;
        lct.ContactId = con.Id;
        chatList.add(lct);
        insert chatList;
        
        String type = 'Contact Chat';
        
        List<Id> idList=new List<Id>();
        
        idList.add(chatList[0].Id);
        
        Test.startTest();
        SendCommsToAlmsWS.sendtoAlms(idList,type);
        Test.stopTest();
    }
}