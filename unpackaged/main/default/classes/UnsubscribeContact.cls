public class UnsubscribeContact {

	public String contactID {get; set;}
	//public String  contactID = s.substring(0, Math.min(s.length(), 15)){get; set;};
	
	//public Contact currentContact {get; set;}
	public Subscriptions__c currentContact {get; set;}
	 
	// 1= Trade Release
	// 2 = Business News
	// 3 = Accelerate EDM
	// 4 = Fare Sheets
	// 5 = Invitations
	// 6 = Fare Advice
	// 7 = Xmas Card
	
	public String subscriptionID {get; set;}
	
	//
	public String subscriptionIDN {get; set;}
	//
		
	
	public Boolean hasError {get; set;}
	
    /**
    * Constructor
    */
    public UnsubscribeContact(ApexPages.StandardController myController) {


		hasError = false;
		
		/*
		try {
			if(System.currentPageReference().getParameters().get('cid') == null)
				throw new CustomException('Invalid parameter cid!');
				
	    	List<Contact> contactList = [
	    		SELECT Id, FirstName, LastName 
	    		FROM Contact 
	    		WHERE Id = :System.currentPageReference().getParameters().get('cid')];
	    	if (contactList.size() != 0)
	    		currentContact = contactList[0];
	    	else
	    		throw new CustomException('Contact not found!');
 
 	    }
    	catch(CustomException ex){
    		hasError = true;
			ApexPages.addMessages(ex);
		}  
		*/ 	
    }
    
    public PageReference unsubscribe(){

		try {
			
			
			if(System.currentPageReference().getParameters().get('cid') == null)
				throw new CustomException('Invalid parameter cid!');
	
			if(System.currentPageReference().getParameters().get('t') == null)
				throw new CustomException('Invalid parameter t!');
	
			
	   		contactID = System.currentPageReference().getParameters().get('cid');
	   		
	   		//contactID = contactID.substring(0, Math.min(contactID.length(), 15)); //chopped
	   		subscriptionID = System.currentPageReference().getParameters().get('t');
			
			//List<Contact> contactList = [
			
			
			
			List<Subscriptions__c> contactList = [
				//SELECT Id, FirstName, Trade_Release__c, Business_News__c, 
				SELECT Id, Trade_Release__c, Business_News__c,
				Accelerate_EDM__c, Fare_Sheets__c, Invitations__c,Xmas_Card__c
				//FROM Contact
				FROM Subscriptions__c
				//WHERE Id = :contactID];
				WHERE Contact_ID__c= :contactID];
				
		
			
	    	if (contactList.size() != 0)
	    		currentContact = contactList[0];
	    	else
	    		throw new CustomException('Contact not found!');
	    	
	    	
			if(subscriptionID=='1'){
				currentContact.Trade_Release__c = false;
				subscriptionIDN='Trade Releases';
			}
			else if(subscriptionID=='2'){
				currentContact.Business_News__c = false;
				subscriptionIDN='Business News';
			}
			else if(subscriptionID=='3'){
				currentContact.Accelerate_EDM__c = false;
				subscriptionIDN='accelerate';
			}
			else if(subscriptionID=='4'){
				currentContact.Fare_Sheets__c = false;
				subscriptionIDN='Fare Sheets';
			}
			else if(subscriptionID=='5'){
				currentContact.Invitations__c = false;
				subscriptionIDN='Event Invitations';
			}
            else if(subscriptionID=='6'){
				currentContact.Fare_Advice__c = false;
				subscriptionIDN='Fare Advice';
			}
			else{
				currentContact.Xmas_Card__c = false;
				subscriptionIDN='Xmas Card';
			}
	   	
            system.debug('The current contact' + currentContact + subscriptionIDN  ) ;
	   		update currentContact;
   		}
    	catch(CustomException ex){
            system.debug('Inside error');
    		hasError = true;
			ApexPages.addMessages(ex);
		}   
   		return null;
    } 
    
   
    public String name (){
    	
    	if(subscriptionID=='1'){
			return 'Trade Releases';
		}
		else if(subscriptionID=='2'){
			return 'Business News';
		}
		else if(subscriptionID=='3'){
			return 'Accelerate EDM';
		}
		else if(subscriptionID=='4'){
			return 'Fare Sheets';
		}
		else if(subscriptionID=='5'){
			return 'Invitations';
		}
        else if(subscriptionID=='6'){
			return 'Fare Advice';
		}
		else{
			return 'Xmas Card';
		}
    }

}