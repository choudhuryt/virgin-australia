/*
*	Controller for VF page: FlightCapacity
*/
public class FlightCapacityController{
    public Case caseObj {get; set;}
    public List<FlightCapacity> flightCapacityList {get; set;}
    private SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService detailService;
    private Reservation__c reservationObj;

    public FlightCapacityController(){
        List<Case> caseList = [SELECT Id, Flight_Number__c, Flight_Date__c, Reservation_Number__c FROM Case
                               WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        if (!caseList.isEmpty()){
            caseObj = caseList.get(0);
        }
        
        List<Reservation__c> reserList = [select Id, Case__c, Flight_Number__c, Flight_Date__c, Origin__c, Destination__c,
                                          Ticket_Number__c, Reservation_Date__c, Reservation_Number__c
                                          from Reservation__c where Case__c = :caseObj.Id order by LastModifiedDate DESC limit 1];
        if (!reserList.isEmpty()){
            reservationObj = reserList.get(0);
            
            // Dodgy HACK to be able to use the caseObject on the page as the Reservation object was created with Flight Date as a timestamp
            if (String.isNotBlank(reservationObj.Flight_Number__c) 
                && reservationObj.Flight_Date__c!=null)
            {
                caseObj.Flight_Number__c = reservationObj.Flight_Number__c;
                caseObj.Flight_Date__c = reservationObj.Flight_Date__c.Date();
            }
        } else {
            reservationObj = new Reservation__c();
        }

        detailService = new SalesForceHistoricalDetailService.SalesforceHistoricalDetailsService();

        // Retrieve Apex Callout options from Custom Settings
        Apex_Callouts__c apexCallouts = Apex_Callouts__c.getValues('FlightDetails');
        if(apexCallouts != null){
            detailService.endpoint_x = apexCallouts.Endpoint_URL__c;
            detailService.timeout_x = Integer.valueOf(apexCallouts.Timeout_ms__c);
            detailService.clientCertName_x = apexCallouts.Certificate__c;
        }
    }

    // call webservice to search for a list of flight capacity
    public void searchCapacity(){
        try {
            flightCapacityList = null;
            if (String.isNotBlank(caseObj.Flight_Number__c) && caseObj.Flight_Date__c!=null){
                // break flight number into airline code and 4-digit number (US5764)
                List<String> flightNumber =  Utilities.formatFlightNumber(caseObj.Flight_Number__c);
                caseObj.Flight_Number__c = flightNumber.get(0) + flightNumber.get(1);
                
                SalesForceHistoricalDetailModel.GetFlightDetailsRSType data = 
                    detailService.GetFlightDetails(flightNumber.get(0), flightNumber.get(1),
                                                   Utilities.convertDate2String(caseObj.Flight_Date__c));
                flightCapacityList = mapFlightCapacity(data);

                // Update the search (reservation) object
                if (reservationObj.Flight_Number__c != caseObj.Flight_Number__c 
                    || reservationObj.Flight_Date__c.date() != caseObj.Flight_Date__c)
                {
                    reservationObj.Flight_Date__c = caseObj.Flight_Date__c;
                    reservationObj.Flight_Number__c = caseObj.Flight_Number__c;
                    
                    // link new reservation obj to parent case   
                    if (String.isBlank(reservationObj.Id)){
                        reservationObj.Case__c = caseObj.Id;
                    }
                    upsert reservationObj;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 
                                                               'Search criteria are saved'));
                }
            }
        } catch (System.CalloutException e) {
            String error = e.getMessage();
            if (String.isNotEmpty(error) && String.isNotBlank(error.substringAfter('SOAP Fault:').substringBefore('faultcode'))) {
                error = error.substringAfter('SOAP Fault:').substringBefore('faultcode');
            } else {
                error = 'Communication Error';
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
        }
    }

    // map respose model to a wraper model in this controller to expose to VF page
    private List<FlightCapacity> mapFlightCapacity(SalesForceHistoricalDetailModel.GetFlightDetailsRSType data){
        if (data == null){
            return null;
        }

        List<FlightCapacity> flightCapacityList = new List<FlightCapacity>();

        for (SalesForceHistoricalDetailModel.FlightLegType flightLeg : data.FlightLegs.FlightLeg) {
            for (SalesForceHistoricalDetailModel.CabinSeatingType cabinSeat : flightLeg.FlightSeatingDetails.CabinSeating) {
                FlightCapacity tmpFlight = new FlightCapacity();
                tmpFlight.Origin = flightLeg.Origin;
                tmpFlight.Destination = flightLeg.Destination;
                tmpFlight.EquipmentType = flightLeg.EquipmentType;
                tmpFlight.DepartureDateTime = Utilities.mergeDateTime(flightLeg.DepartureDate, flightLeg.DepartureTime).format();
                tmpFlight.CabinClass = cabinSeat.CabinClass;
                tmpFlight.ActualSeatCapacity = cabinSeat.ActualSeatCapacity;
                tmpFlight.AuthorisedSeatCapacity = cabinSeat.AuthorisedSeatCapacity;
                tmpFlight.SeatsSold = cabinSeat.SeatsSold;
                tmpFlight.AvailableSeats = cabinSeat.AvailableSeats;
                flightCapacityList.add(tmpFlight);
            }
        }

        return flightCapacityList;
    }

    // wrapper model for Flight Capacity
    public class FlightCapacity {
        public String Origin{get;set;}
        public String Destination{get;set;}
        public String EquipmentType{get;set;}
        public String DepartureDateTime{get;set;}
        public String CabinClass{get;set;}
        public Integer ActualSeatCapacity{get;set;}
        public Integer AuthorisedSeatCapacity{get;set;}
        public Integer SeatsSold{get;set;}
        public Integer AvailableSeats{get;set;}
    }
}