/**
* @description       : UpdateMasterCase
* @Updated By        : Cloudwerx
**/
public class UpdateMasterCase {
     @InvocableMethod
     Public static Void GRCaseStatusUpdate( List<Id> CCaseIds) {
         List<Case> lstCaseUpdate = new List<Case>();  								
         List<Case> MasterCaseList = new List<Case>(); 
         List<Case> ChildCaseList = new List<Case>();
         List<CaseComment> masterCommand = new List<CaseComment>();
         
         //@AddedBy: Cloudwerx: Here we are retrieving the GR Case Category record according to custom label id
         List<GR8_Case_Categories__c> caseCategories = [SELECT Id FROM GR8_Case_Categories__c 
                                                        WHERE Id =:System.Label.GR_Case_Category_Id];     
         //@AddedBy: Cloudwerx: Here we are retrieving the GR Case Category record according to custom label id
         ChildCaseList = [SELECT Id, Status, CaseNumber, Description, Origin, MasterRecordId  
                          FROM Case WHERE Id IN: CCaseIds];       
         //@Updated : cloudwerx here we added GR_Case recordtypeId in WHERE condition and removed static name
         MasterCaseList = [SELECT Id, Status, Most_Recent_Comment__c, OwnerId, CaseNumber 
                           FROM Case where id =:ChildCaseList[0].MasterRecordId AND RecordTypeId =:Utilities.getRecordTypeId('Case', 'GR_Case')];
         
         if(MasterCaseList.size() > 0) {     
             CaseComment newCommmand = new CaseComment();
             newCommmand.ParentId  = MasterCaseList[0].Id;
             newCommmand.CommentBody = 'Duplicate Case Number '  + ChildCaseList[0].CaseNumber + ' merged with  Case Number  ' +    MasterCaseList[0].CaseNumber  + ' on ' + system.today().format() ;
             newCommmand.IsPublished = TRUE;
             masterCommand.add(newCommmand) ;
             if(!masterCommand.isEmpty()) {
                 INSERT masterCommand;
             }
             
             if ( ChildCaseList.size() > 0  && (ChildCaseList[0].Origin == 'E2C' ||ChildCaseList[0].Origin =='Email')) {                 
                 try {
                     DELETE ChildCaseList;
                 } catch (DmlException e) {}
             } else if ( ChildCaseList.size() > 0  &&  ( ChildCaseList[0].Origin != 'E2C' &&  ChildCaseList[0].Origin !='Email')) {
                 for(Case c : ChildCaseList) {
                     c.ParentId = MasterCaseList[0].Id;
                     c.OwnerId  =  UserInfo.getUserId();                                       
                     c.Child_Case_Comments__c = 'Duplicate';
                     c.Child_Case_Reason__c  = 'Duplicate';
                     c.Compliance_Status__c = 'Compliance form NOT required';
                     if (caseCategories != null && caseCategories.size() > 0) { c.Category_Number__c = caseCategories[0].Id;}
                     c.Pre_Filter_Primary_Category__c = 'Service Recovery';
                     c.status = 'Closed' ;
                     lstCaseUpdate.add(c);
                 }
                 if(!lstCaseUpdate.isEmpty()) {
                     UPDATE   lstCaseUpdate ;
                 }
             }
         }
     }   
 }