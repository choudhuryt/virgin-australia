/*
* Author: Warjie Malibago (Accenture CloudFirst)
* Date: May 30, 2016
* Description: C# 107599; Caters the URL hacking for Opportunities under Corporate record type.

*  Varsha Patil 2/06/2016 Description: Modified the Proceed method to populate the AccountName/ ParentName on Opportunity when new opportunity is created.
* UpdatedBy : cloudwerx
*/
public class OpportunityRedirectController {
    public Map<String ,String> oppRecMap = new Map<String, String>();
    public String recType {get; set;}
    public Opportunity opp; 
    public Id AccId; 
    public void OpportunityRedirectController(){}
    
    public OpportunityRedirectController(ApexPages.StandardController controller){}
    
    public List<SelectOption> getItems(){
        for(RecordType s: [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Opportunity']){
            oppRecMap.put(s.Name, s.Id);
        }    
        
        List<SelectOption> options = new List<SelectOption>();
        for(String s: oppRecMap.keySet()){
            options.add(new SelectOption(s, s));
        }
        return options;
    }
    // Modified the Proceed method to autopopulate the Account Name on opportunity.
    public PageReference proceed() {
        String accId = ApexPages.currentPage().getParameters().get('accid');       
        PageReference p;
        
        if(accid != null){
            Account a = [SELECT Name,RecordType.Name  From Account WHERE Id = :accId];
            if (recType == 'Corporate') {
                p = Page.NewCorporateOpportunity ;
                p.getParameters().put('id', accId);         
            } else {
                p = new PageReference('/006/e?retURL=%2F006%2Fo&RecordType='+oppRecMap.get(recType)+'&ent=Opportunity&opp4=' +  a.Name);
                p.getParameters().put('nooverride','1');
            }
        } else {
            system.debug('This is the record type' + oppRecMap.get(recType));
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id (Corporate = 'VA_master')
            Id opportunityCorporateRecordTypeId = Utilities.getRecordTypeId('Opportunity', 'Corporate');
            if(oppRecMap.get(recType) == opportunityCorporateRecordTypeId) {
                p = Page.NewCorporateOpportunity ;
            } else {
                p = new PageReference('/006/e?retURL=%2F006%2Fo&RecordType='+oppRecMap.get(recType)+'&ent=Opportunity');
                p.getParameters().put('nooverride','1');
            }
        }
        
        p.setRedirect(true);
        return p;
    }
    
    public PageReference cancel(){
        PageReference p = new PageReference('/006?');
        p.setRedirect(true);
        return p;        
    }
}