public with sharing class AnalysisMasterSelectionController {

	public List<Analysis__c> masterList {get; set;}
	public Opportunity currentOpportunity {get; set;}
	public String selectedAnalysisID {get; set;}
	
    /**
    * Constructor
    */
    public AnalysisMasterSelectionController(ApexPages.StandardController myController) {

    	// Check if there is an opportunity ID in Query String
    	String opptyID = System.currentPageReference().getParameters().get('opptyid');
		if (opptyID != null)
	    	currentOpportunity = [
	    		SELECT Id, Name, AccountId 
	    		FROM Opportunity 
	    		WHERE Id = :opptyID];
	    else 
	    	throw new CustomException('Need an opportunity ID...');

    	String analysisType = System.currentPageReference().getParameters().get('type');
		if (opptyID == null)
	    	analysisType = 'Sales Opportunity Analysis';


		// Get the list of Master and Active Templates\
		masterList = [
    			SELECT Id, Name, Description__c, Active__c, 
    				Is_Master__c, Type__c
    			FROM Analysis__c 		
    			WHERE Is_Master__c = true 
    			AND Active__c = true
    			AND type__c = :analysisType];
    	
    }
    
    public PageReference selectAnalysis() {
    	
    	if (selectedAnalysisID == null) {
    		throw new CustomException('Select a master analysis...');
    		return null;
    	}
    	
    	try {
	    	// Get the data for selected analysis and questions
	    	Analysis__c selectedAnalysis = [
	    			SELECT Id, Name, Comment__c, Account__r.Id, Description__c, 
	    				Is_Master__c, Opportunity__r.Id, Type__c, Status__c, Is_Editable__c,
	    				Show_Rating__c, Show_Rating_2__c, Rating_Title__c, Rating_Title_2__c, 
	    				Range_1_Min__c, Range_1_Max__c, Range_1_Title__c,
	    				Range_2_Min__c, Range_2_Max__c, Range_2_Title__c,
	    				Range_3_Min__c, Range_3_Max__c, Range_3_Title__c,
	    				Range_4_Min__c, Range_4_Max__c, Range_4_Title__c,
	    				Range_5_Min__c, Range_5_Max__c, Range_5_Title__c
	    			FROM Analysis__c 
	    			WHERE Id =:selectedAnalysisID];
	    		if (selectedAnalysis == null)
	    			throw new CustomException('Analysis not found...');
	
				// Get questions for the instance
				List<Analysis_Question__c> questionList = [
					SELECT Id, Analysis__c, Description__c, Has_SWOT__c,
						Range_Min__c, Range_Max__c, Selected_Score__c, Range_Description__c,
						Range_Min_2__c, Range_Max_2__c, Selected_Score_2__c, Range_Description_2__c,
						Comment_Title_1__c, Comment_Title_2__c, Comment_Title_3__c, 
						Answer_1__c, Answer_2__c, Answer_3__c, Title__c, Order__c, 
						(
							SELECT Id, Type__c, Title__c, Description__c, Market__c
							FROM Analysis_SWOT__r
						)
					FROM Analysis_Question__c 
					WHERE Analysis__c=:selectedAnalysis.Id
					ORDER BY Order__c, Name];
	    	
	    	
	    	// Clone the Selected Master Analysis
	    	Analysis__c newAnalysis = selectedAnalysis.clone();
	    	newAnalysis.Is_Master__c = false;
	    	newAnalysis.Opportunity__c = currentOpportunity.Id;
	    	insert newAnalysis;
	    	
	    	List<Analysis_Question__c> newQuestions = new List<Analysis_Question__c>();
			for(Analysis_Question__c a:questionList) {
	    		Analysis_Question__c newQuestion = a.clone();
	    		newQuestion.Analysis__c = newAnalysis.Id;
				newQuestions.add(newQuestion);
	    	}
		    	
		    insert newQuestions;
		    
	    	PageReference thePage = new PageReference('/apex/answerAnalysis?id=' + newAnalysis.Id);
			thePage.setRedirect(true);
			return thePage;
    	}
    	catch(Exception ex) {
    		ApexPages.addMessages(ex);
    		return null;
    	}
    }
 
    public PageReference cancel() {
    	
    	PageReference thePage = new PageReference('/' + currentOpportunity.Id);
		thePage.setRedirect(true);
		return thePage;

    }
        
}