/**
* @description       : FCRCAUtilities
* @Updated By         : Cloudwerx
**/
public class FCRCAUtilities {
    // @UpdateBy : Cloudwerx, Here we are checking the Contract_Addendum__c record is exist or not with system label "FCR_CA_BAKUP" defined Id
    public static List<Contract_Addendum__c> existingContractAddendums = [SELECT Id FROM Contract_Addendum__c WHERE Id =:label.FCR_CA_BAKUP];
    public static void removeDOMRegional(set<id> CASet) {
        list<Proposed_Discount_Tables__c> ProposalTableList = new list<Proposed_Discount_Tables__c>();
        map<id, id> CAIdContractIdMap = new map<id, id>();
        map<id, string> CAIdTierMap = new map<id, string>();
        ProposalTableList = [SELECT id, name, Contract_Addendum__c, Contract__c, Contract_Addendum__r.Domestic_Requested_Tier__c
                             FROM Proposed_Discount_Tables__c 
                             WHERE Contract_Addendum__c IN :CASet AND name IN('DOM Regional', 'DOM Mainline')];
        if ((ProposalTableList != null) && (ProposalTableList.size() > 0)) {
            for (integer count = 0; count < ProposalTableList.size(); count++) {
                ProposalTableList[count].FCR_Contract_Amendment_Back_Up__c = ProposalTableList[count].Contract_Addendum__c;
                ProposalTableList[count].FCR_Contract_Back_Up__c = ProposalTableList[count].Contract__c;
                ProposalTableList[count].FCR_Processed__c = true;
                ProposalTableList[count].FCR_For_Deletion__c = true;
                CAIdContractIdMap.put(ProposalTableList[count].Contract_Addendum__c, ProposalTableList[count].Contract__c);
                CAIdTierMap.put(ProposalTableList[count].Contract_Addendum__c, ProposalTableList[count].Contract_Addendum__r.Domestic_Requested_Tier__c);
                // @UpdateBy : Cloudwerx, Here we are checking the Contract_Addendum__c record is not null then we are updating the Contract_Addendum__c 
                if (existingContractAddendums.size() > 0) { ProposalTableList[count].Contract_Addendum__c = label.FCR_CA_BAKUP; }
                ProposalTableList[count].Contract__c = null;
                ProposalTableList[count].FCR_Remarks__c = 'Old DOM Record marked for Deletion';
            }    
        }
        UPDATE ProposalTableList;
        createDOMRegional(CAIdContractIdMap, CAIdTierMap, CASet);
    }
    
    public static void createDOMRegional(map<id, id> CAIdContractIdMap, map<id, string> CAIdTierMap, set<id> CASet) {
        list<FCR_Proposal_Tier__c> FCRProposalTierList = new list<FCR_Proposal_Tier__c>();
        list<Proposed_Discount_Tables__c> ProposalTableList = new list<Proposed_Discount_Tables__c>();
        FCRProposalTierList = [SELECT Id, Discount_Off_Published_Fare__c, Eligible_Booking_Class__c, Eligible_Fare_Type__c, 
                               Name, Proposal_Table_Name__c, Sort_Order__c,
                               Template_Tier__c, Tier__c, VA_Eligible_Booking_Class__c FROM FCR_Proposal_Tier__c WHERE
                               Proposal_Table_Name__c = 'DOM Mainline'];
        for (id CAId : CASet) {
            if((FCRProposalTierList != null) && (FCRProposalTierList.size() > 0)) {
                for (integer i = 0; i < FCRProposalTierList.size(); i++) {
                    if(CAIdTierMap.get(CAId) == FCRProposalTierList[i].tier__c) {
                        Proposed_Discount_Tables__c ProposalTableRec = new Proposed_Discount_Tables__c();
                        ProposalTableRec.VA_ELIGIBLE_BOOKING_CLASS__c = FCRProposalTierList[i].VA_Eligible_Booking_Class__c;
                        ProposalTableRec.DISCOUNT_OFF_PUBLISHED_FARE__c = FCRProposalTierList[i].Discount_Off_Published_Fare__c;
                        ProposalTableRec.ELIGIBLE_FARE_TYPE__c = FCRProposalTierList[i].Eligible_Fare_Type__c;
                        //ProposalTableRec.Tier__c = FCRProposalTierList[i].Tier__c;
                        ProposalTableRec.Name = FCRProposalTierList[i].Proposal_Table_Name__c;
                        //ProposalTableRec.sort_order__c = FCRProposalTierList[i].sort_order__c;
                        ProposalTableRec.Contract_Addendum__c = CAId;
                        ProposalTableRec.Contract__c = CAIdContractIdMap.get(CAId);
                        ProposalTableRec.FCR_Created__c = true;
                        ProposalTableRec.FCR_Processed__c = true;
                        ProposalTableRec.FCR_Remarks__c = 'DOM Record created as per new Tier Table';
                        ProposalTableList.add(ProposalTableRec);
                    }    
                }    
            }       
        }    
        if (ProposalTableList.size() > 0) {
            INSERT(ProposalTableList);
        }
        //fixISHTT(CAIdContractIdMap.keyset());
        updateCAforProcessed(CASet);
    }    
    
    public static void updateCAforProcessed(set<id> CASet) {
        list<contract_addendum__c> CAList = new list<contract_addendum__c>();
        CAList = [select id from contract_addendum__c where id in :CASet];
        if ((CAList != null) && (CAList.size() > 0)) {
            for (integer i = 0; i < CAList.size(); i++) {
                CAList[i].FCR_Processed__c = true;
            }    
        }
        UPDATE CAList;
    }    
}