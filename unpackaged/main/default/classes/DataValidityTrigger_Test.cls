/*
 * Author: Warjie Malibago (Accenture CloudFirst)
 * Date: May 20, 2016
 * @updatedBy : cloudwerx
*/
@isTest
private class DataValidityTrigger_Test {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('contract', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
    }
    
    @isTest
    private static void testDataValidityForIATAValidityRecordType() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Id contractId = [SELECT Id FROM Contract LIMIT 1]?.Id;
        
        Account testAccountObj1 = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj1;
        
        IATA_Number__c testIATANumberObj = TestDataFactory.createTestIATANumber(testAccountObj1.Id, contractId, '0000795', 'Yes');
        INSERT testIATANumberObj;
        
        Data_Validity__c testDataValidityObj = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false);
        testDataValidityObj.RecordTypeId = DataValidityTriggerHandler.dataValidityIATAValidityRecordTypeId;
        testDataValidityObj.IATA__c = testIATANumberObj.Id;
        INSERT testDataValidityObj;
        
        Test.startTest();
        Data_Validity__c testDataValidityObj1 = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false); 
        testDataValidityObj1.RecordTypeId = DataValidityTriggerHandler.dataValidityIATAValidityRecordTypeId;
        testDataValidityObj1.IATA__c = testIATANumberObj.Id;
        try {
            INSERT testDataValidityObj1;   
        } catch(DmlException dmlExp) {
            system.assertEquals('The Start Date Of the new IATA cannot be before the end date of the previous Data Validity record, the end date of the previous Data Validity record is ' + formateDate(testDataValidityObj.To_Date__c), dmlExp.getDmlMessage(0));
        }
        
        Test.stopTest();
        //Asserts
        IATA_Number__c updatedIATANumberObj = [SELECT Id, Account__c FROM IATA_Number__c LIMIT 1];
        system.assertEquals(accountId, updatedIATANumberObj.Account__c);
    }
    
    @isTest
    private static void testDataValidityForPCCRecordType() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Id contractId = [SELECT Id FROM Contract LIMIT 1]?.Id;
        
        Account testAccountObj1 = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj1;
        
        PCC__c testPCCObj = TestDataFactory.createTestPCC('Test PCC', testAccountObj1.Id, contractId, null, null, 'Yes', Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), 'Galileo');
        INSERT testPCCObj;
        
        Data_Validity__c testDataValidityObj = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false);
        testDataValidityObj.RecordTypeId = DataValidityTriggerHandler.dataValidityPCCRecordTypeId;
        testDataValidityObj.PCC__c = testPCCObj.Id;
        INSERT testDataValidityObj;
        
        Test.startTest();
        Data_Validity__c testDataValidityObj1 = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false); 
        testDataValidityObj1.RecordTypeId = DataValidityTriggerHandler.dataValidityPCCRecordTypeId;
        testDataValidityObj1.PCC__c = testPCCObj.Id;
        try {
            INSERT testDataValidityObj1;   
        } catch(DmlException dmlExp) {
            system.assertEquals('The Start Date Of the new PCC cannot be before the end date of the previous Data Validity record, the end date of the previous Data Validity record is ' + formateDate(testDataValidityObj.To_Date__c), dmlExp.getDmlMessage(0));
        }
        
        Test.stopTest();
        //Asserts
        PCC__c updatedPCCObj = [SELECT Id, Account__c FROM PCC__c LIMIT 1];
        system.assertEquals(accountId, updatedPCCObj.Account__c);
    }
    
    @isTest
    private static void testDataValidityForAGYRecordType() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Id contractId = [SELECT Id FROM Contract LIMIT 1]?.Id;
        
        Account testAccountObj1 = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj1;
        
        AGYLogin__c testAGYLoginObj = TestDataFactory.createTestAGYLogin('AGY' + Math.round((Math.random() * (900000) + 100000)), testAccountObj1.Id, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), 'Yes', 'Test');
        INSERT testAGYLoginObj;
        
        Data_Validity__c testDataValidityObj = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false);
        testDataValidityObj.RecordTypeId = DataValidityTriggerHandler.dataValidityAGYRecordTypeId;
        testDataValidityObj.AGY__c = testAGYLoginObj.Id;
        INSERT testDataValidityObj;
        
        Test.startTest();
        Data_Validity__c testDataValidityObj1 = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false); 
        testDataValidityObj1.RecordTypeId = DataValidityTriggerHandler.dataValidityAGYRecordTypeId;
        testDataValidityObj1.AGY__c = testAGYLoginObj.Id;
        try {
            INSERT testDataValidityObj1;   
        } catch(DmlException dmlExp) {
            system.assertEquals('The Start Date Of the new AGY cannot be before the end date of the previous Data Validity record, the end date of the previous Data Validity record is ' + formateDate(testDataValidityObj.To_Date__c), dmlExp.getDmlMessage(0));
        }
        
        Test.stopTest();
        //Asserts
        AGYLogin__c updatedAGYObj = [SELECT Id, Account__c FROM AGYLogin__c LIMIT 1];
        system.assertEquals(accountId, updatedAGYObj.Account__c);
    }
    
    @isTest
    private static void testDataValidityForTIDSRecordType() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Id contractId = [SELECT Id FROM Contract LIMIT 1]?.Id;
        
        Account testAccountObj1 = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj1;
        
        TIDS__c tesTIDSObj = TestDataFactory.createTestTIDS('TIDS' + Math.round((Math.random() * (9000) + 1000)), testAccountObj1.Id, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), 'Yes');
        INSERT tesTIDSObj;
        
        Data_Validity__c testDataValidityObj = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false);
        testDataValidityObj.RecordTypeId = DataValidityTriggerHandler.dataValidityTIDSRecordTypeId;
        testDataValidityObj.TIDS__c = tesTIDSObj.Id;
        INSERT testDataValidityObj;
        
        Test.startTest();
        Data_Validity__c testDataValidityObj1 = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false); 
        testDataValidityObj1.RecordTypeId = DataValidityTriggerHandler.dataValidityTIDSRecordTypeId;
        testDataValidityObj1.TIDS__c = tesTIDSObj.Id;
        try {
            INSERT testDataValidityObj1;   
        } catch(DmlException dmlExp) {
            system.assertEquals('The Start Date Of the new TIDS cannot be before the end date of the previous Data Validity record, the end date of the previous Data Validity record is ' + formateDate(testDataValidityObj.To_Date__c), dmlExp.getDmlMessage(0));
        }
        
        Test.stopTest();
        //Asserts
        TIDS__c updatedTIDSObj = [SELECT Id, Account__c FROM TIDS__c LIMIT 1];
        system.assertEquals(accountId, updatedTIDSObj.Account__c);
    }
    
    @isTest
    private static void testDataValidityForTourCodeRecordType() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Id contractId = [SELECT Id FROM Contract LIMIT 1]?.Id;
        
        Account testAccountObj1 = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj1;
        
        Tourcodes__c testTourcodesObj = TestDataFactory.createTestTourCodes(testAccountObj1.Id, 'Active', contractId, 'Tactical', 'tourcode' + Math.round((Math.random() * (900000) + 100000)));
        INSERT testTourcodesObj;
        
        Data_Validity__c testDataValidityObj = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false);
        testDataValidityObj.RecordTypeId = DataValidityTriggerHandler.dataValidityTourCodeRecordTypeId;
        testDataValidityObj.Tourcode__c = testTourcodesObj.Id;
        INSERT testDataValidityObj;
        
        Test.startTest();
        Data_Validity__c testDataValidityObj1 = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false); 
        testDataValidityObj1.RecordTypeId = DataValidityTriggerHandler.dataValidityTourCodeRecordTypeId;
        testDataValidityObj1.Tourcode__c = testTourcodesObj.Id;
        try {
            INSERT testDataValidityObj1;   
        } catch(DmlException dmlExp) {
            system.assertEquals('The Start Date Of the new Tourcode cannot be before the end date of the previous Data Validity record, the end date of the previous Data Validity record is ' + formateDate(testDataValidityObj.To_Date__c), dmlExp.getDmlMessage(0));
        }
        
        Test.stopTest();
        //Asserts
        Tourcodes__c updatedTourcodesObj = [SELECT Id, Account__c FROM Tourcodes__c LIMIT 1];
        system.assertEquals(accountId, updatedTourcodesObj.Account__c);
    }
       
    private static String formateDate(Datetime dt) {
        return DateTime.newInstance(dt.year(), dt.month(), dt.day()).format('dd-MM-YYYY');
    }
}