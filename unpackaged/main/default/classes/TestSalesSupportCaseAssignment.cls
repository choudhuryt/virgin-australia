@isTest(SeeAllData=true)
private class TestSalesSupportCaseAssignment
{
public static testMethod void testcontractextnterm() 

{
    Test.startTest();
    List<RecordType> recordTypeImpCase = [Select id From RecordType
											where SobjectType = 'Case'
											and IsActive = true
											and DeveloperName = 'Implementation' LIMIT 1];
    
     Case c1 = new Case(Subject='Contract Extension has been approved',
                        RecordTypeId = recordTypeImpCase[0].id,
                        Description  = 'Account ID:  0016F00001f6puB');
    
    insert c1 ;
    
     List<Id> cid = new List<Id>();        
     cid.add(c1.id);
     SalesSupportCaseAssignment.SalesSupportCaseLink(cid);

    Case c2 = new Case( Subject='Contract Termination has been approved',
                        RecordTypeId = recordTypeImpCase[0].id,
                        Description  = 'Account ID:  0016F00001f6puB');
    
    insert c2 ;
    
     List<Id> c1id = new List<Id>();        
     c1id.add(c2.id);
     SalesSupportCaseAssignment.SalesSupportCaseLink(c1id);
    
    	Test.stopTest();
    
}



public static testMethod void testcontractapproval() 

{
    Test.startTest();
    List<RecordType> recordTypeImpCase = [Select id From RecordType
											where SobjectType = 'Case'
											and IsActive = true
											and DeveloperName = 'Implementation' LIMIT 1];
    
     Case c1 = new Case(Subject='Corporate Contract has been approved',
                        RecordTypeId = recordTypeImpCase[0].id,
                        Description  = 'Account ID:  0016F00001f6puB');
    
    insert c1 ;
    
     List<Id> cid = new List<Id>();        
     cid.add(c1.id);
     SalesSupportCaseAssignment.SalesSupportCaseLink(cid);

    Case c2 = new Case( Subject='Industry Contract has been approved',
                        RecordTypeId = recordTypeImpCase[0].id,
                        Description  = 'Account ID:  0016F00001f6puB Due Date : 19/07/2020');
    
    insert c2 ;
    
     List<Id> c1id = new List<Id>();        
     c1id.add(c2.id);
     SalesSupportCaseAssignment.SalesSupportCaseLink(c1id);
    
    	Test.stopTest();
    
}
public static testMethod void testsalessupportapproval1() 

{
    Test.startTest();
    List<RecordType> recordTypeImpCase = [Select id From RecordType
											where SobjectType = 'Case'
											and IsActive = true
											and DeveloperName = 'State_Sales_Support' LIMIT 1];
    
     Case c1 = new Case(Subject='Please Process the following FOC Ticket Request',
                        RecordTypeId = recordTypeImpCase[0].id,
                        Description  = 'Account ID: 0019000000MYJGU Due Date : 19/07/2020');
    
    insert c1 ;
    
     List<Id> cid = new List<Id>();        
     cid.add(c1.id);
     SalesSupportCaseAssignment.SalesSupportCaseLink(cid);
Test.stopTest();
   
}
    
public static testMethod void testsalessupportapproval3() 

{
    Test.startTest();
    List<RecordType> recordTypeImpCase = [Select id From RecordType
											where SobjectType = 'Case'
											and IsActive = true
											and DeveloperName = 'State_Sales_Support' LIMIT 1];
    
    Case c2 = new Case( Subject='Please process the following Agent Rate Request',
                        RecordTypeId = recordTypeImpCase[0].id,
                        Description  = 'Account ID: 0019000000MYJGU Due Date : 19/07/2020');
    
    insert c2 ;
    
     List<Id> c1id = new List<Id>();        
     c1id.add(c2.id);
     SalesSupportCaseAssignment.SalesSupportCaseLink(c1id);
    
    Test.stopTest();
   
}     

public static testMethod void testsalessupportapproval4() 

{
    Test.startTest();
    List<RecordType> recordTypeImpCase = [Select id From RecordType
											where SobjectType = 'Case'
											and IsActive = true
											and DeveloperName = 'State_Sales_Support' LIMIT 1];
    
    Case c2 = new Case( Subject='Waiver & Favour Request for Account Approved',
                        RecordTypeId = recordTypeImpCase[0].id,
                        Description  = 'Account ID: 0019000000MYJGU Due Date : 19/07/2020');
    
    insert c2 ;
    
     List<Id> c1id = new List<Id>();        
     c1id.add(c2.id);
     SalesSupportCaseAssignment.SalesSupportCaseLink(c1id);
    
    Test.stopTest();
   
}    
    
public static testMethod void testsalessupportapproval2() 

{
    Test.startTest();
    List<RecordType> recordTypeImpCase = [Select id From RecordType
											where SobjectType = 'Case'
											and IsActive = true
											and DeveloperName = 'State_Sales_Support' LIMIT 1];
    
     Case c1 = new Case(Subject='Velocity Status Match/Upgrade Request Approved',
                        RecordTypeId = recordTypeImpCase[0].id,
                        Description  = 'Account ID: 0016F00001gSbuP VSM Record:  a052t000000djkp Due Date : 19/07/2020');
    
    insert c1 ;
    
     List<Id> cid = new List<Id>();        
     cid.add(c1.id);
     SalesSupportCaseAssignment.SalesSupportCaseLink(cid);

    
    
    	Test.stopTest();
    
}  

public static testMethod void testimpementationfarechange() 

{
    Test.startTest();
    List<RecordType> recordTypeImpCase = [Select id From RecordType
											where SobjectType = 'Case'
											and IsActive = true
											and DeveloperName = 'Implementation' LIMIT 1];
    
     Case c1 = new Case(Subject='Corporate Fares - Change 00121604 - Add and Remove PCCs',
                        RecordTypeId = recordTypeImpCase[0].id,
                        Description  = 'This is test');
    
    insert c1 ;
    
     List<Id> cid = new List<Id>();        
     cid.add(c1.id);
     SalesSupportCaseAssignment.SalesSupportCaseLink(cid);

    
    
    	Test.stopTest();
    
}      
    
    
}