@isTest
private class IATAsInContractTest {

     static testMethod void testIATAsInContract()         
     {           
          Account parentacc = TestUtilityClass.createTestAccount();
          insert parentacc ;
          Account acc = TestUtilityClass.createTestAccount();
          acc.parentid = parentacc.Id ;
          insert acc;    
      
          Contract ct = TestUtilityClass.createTestContract(acc.Id);
          Id contractRecType = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Industry - TMC/Corporate Independent').getRecordTypeId();
         //ct.recordtypeid = '012900000007uwJ';
         ct.recordtypeid = contractRecType;
          ct.Status= 'Draft';         
          insert ct;
          ct.Status = 'Activated' ;
          update ct ;
         
          IATA_Number__c ia = TestUtilityClass.createTestIATANumber(acc.Id, ct.Id);
          insert ia;
      
           AGYLogin__c ag = TestUtilityClass.createTestAGYLogin(acc.Id);
           insert ag;
            
           Data_Validity__c dv = TestUtilityClass.createTestDataValidity(acc.Id, ag.Id);
           dv.RecordTypeId = Schema.SObjectType.Data_Validity__c.getRecordTypeInfosByName().get('IATA Validity').getRecordTypeId();
           dv.IATA__c = ia.Id;
           dv.From_Date__c = Date.today();
           dv.To_Date__c = Date.newInstance(2016, 12, 31);
           dv.Soft_Delete__c = FALSE;
           insert dv;
   
            Test.startTest();  
            IATAsInContract testBatch = new IATAsInContract();
            testBatch.emailTo = new String[] {'salesforce.admin@virginaustralia.com'};
            testBatch.query = 'SELECT AccountId, StartDate, EndDate, Status, Account.Account_Type__c, Account.RecordTypeId ' + 
				'FROM Contract ' + 
				'Where RecordType.Name like \'%Industry%\' ' +
				'and Status <> \'Deactivated\' ' +
				'and StartDate >= LAST_YEAR';
         
            Database.executeBatch(testBatch);  
      
           Test.stopTest();
       
     }
}