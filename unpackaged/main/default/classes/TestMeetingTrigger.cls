/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMeetingTrigger {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Account account = new Account();
        CommonObjectsForTest objFortest = new CommonObjectsForTest();
        
        account = objFortest.CreateAccountObject(0);
        
        insert account;
         Contact contact = new Contact();
        contact = objFortest.CreateNewContact(0);
        insert contact;
        
        Meeting__c meeting = new Meeting__c();
        meeting.Subject__c = 'Sales Call';
        meeting.Type__c = 'Ad hoc';
        meeting.Meeting_Objectives__c = 'test';
        meeting.Meeting_Date_And_Time__c =  Date.today();
        meeting.End_Meeting_Date_and_Time__c =  Date.today();
        meeting.Key_Topics__c = 'Test Key Topics';
        meeting.Additional_Meeting_Agenda__c = 'Additional meeting agenda';
        meeting.Account__c = account.id;
         meeting.Sales_Contact__c = contact.id;
        insert meeting;
        
        
        Meeting_Attendee__c attende = new Meeting_Attendee__c();
        attende.RecordTypeId = '012900000007g0DAAQ';
        attende.User__c = '00590000000K0ju';
        attende.Role__c = 'Participant';
        attende.Meeting__c = meeting.id;
        insert attende;
        
        
       
         
        meeting.Additional_Meeting_Agenda__c = '1';
       
        update meeting;
        
        delete meeting;
        
        
        
        
    }
}