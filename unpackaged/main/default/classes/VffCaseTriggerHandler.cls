/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 04.02.2022
**Description: Trigger Helper Class for VffCaseTrigger on Case for VFF
*/
public class VffCaseTriggerHandler {
    //SLA in Hours
    public static integer goldSilverSLA = Integer.valueOf(System.Label.Gold_Silver_SLA_in_Hrs);//18Hrs
    public static integer platinumSLA = Integer.valueOf(System.Label.Platinum_SLA_in_Hrs);//9Hrs
    public static integer redSLA = Integer.valueOf(System.Label.Red_SLA_in_Hrs);//45Hrs
    public static integer retroSLA = Integer.valueOf(System.Label.Retro_SLA_in_Hrs);//27Hrs
    public static integer resolutionSLA = Integer.valueOf(System.Label.Resolution_SLA_in_Hrs);//270Hrs
    
    //public static String vffRTId = System.Label.Case_RT_Velocity_Member_Support;
    public static Id vffRTId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Velocity_Member_Support').getRecordTypeId();
    public static BusinessHours bh = [SELECT Id FROM BusinessHours WHERE Name = 'VFF Business Hours' AND IsActive = true];
    
    public static void TargetDateAfterInsertCase(List<Case> newCaseList){
        //Update Target DateTime for First Resonse SLA and Resolution SLA when Case is Created
        if(newCaseList.size()>0){
            List<Case> cList = [Select Id, Owner.Name, Velocity_Type__c, Queue_Override_Options__c, CreatedDate from Case where Id IN: newCaseList AND RecordTypeId =:vffRTId];
            Datetime fstResTarDate;
            Datetime resSLATarDate;
            List<Case> upCaseList = new List<Case>();
            
            if(cList.size()>0){
                for(Case cas: cList){
                    if(cas.Owner.Name =='VFF Gold Queue' || cas.Velocity_Type__c == 'Gold' || cas.Queue_Override_Options__c == 'Gold' || cas.Owner.Name == 'VFF Silver Queue' || cas.Velocity_Type__c == 'Silver' || cas.Queue_Override_Options__c == 'Silver'){
                        fstResTarDate = BusinessHours.addGmt(bh.id, cas.CreatedDate, goldSilverSLA * 60 * 60 * 1000L);               
                    }else if(cas.Owner.Name =='VFF Platinum Queue' || cas.Velocity_Type__c == 'Platinum' || cas.Queue_Override_Options__c == 'Platinum'){
                        fstResTarDate = BusinessHours.addGmt(bh.id, cas.CreatedDate, platinumSLA * 60 * 60 * 1000L);
                    }else if(cas.Owner.Name =='VFF Red Queue' || cas.Velocity_Type__c == 'Red' || cas.Queue_Override_Options__c == 'Red'){
                        fstResTarDate = BusinessHours.addGmt(bh.id, cas.CreatedDate, redSLA * 60 * 60 * 1000L);
                    }else if(cas.Owner.Name =='VFF Retro Queue' || cas.Velocity_Type__c == 'Retro' || cas.Queue_Override_Options__c == 'Retro'){
                        fstResTarDate = BusinessHours.addGmt(bh.id, cas.CreatedDate, retroSLA * 60 * 60 * 1000L);
                    }
                    if(fstResTarDate != NULL){
                        resSLATarDate = BusinessHours.addGmt(bh.id, cas.CreatedDate, resolutionSLA * 60 * 60 * 1000L);
                        Case newC = new Case();
                        newC.Id = cas.Id;
                        newC.First_Response_Target_Date__c = fstResTarDate;
                        newC.Resolution_SLA_Target_Date__c = resSLATarDate;
                        upCaseList.add(newC);
                    }
                }                
            }System.debug('cList--'+cList+'--upCaseList--'+upCaseList);
            
            if(upCaseList.size()>0){
                update upCaseList;
            }
        }       
    }
    
    public static void BeforeUpdateCase(List<Case> newCaseList, Map<Id, Case> OldCaseMap){
        Datetime fstResTarDate;
        Datetime resSLATarDate;
        Datetime startDate;
        Decimal slaPausedTime;
        Decimal slaPausedHours;
        List<Id> caseAssIdList = new List<Id>();
        for(Case cs: newCaseList){
            if(cs.RecordTypeId == vffRTId){
                //Update Agent Assignment Date if Null
                if(OldCaseMap.get(cs.Id).Agent_Assigned_Time__c == NULL){
                    caseAssIdList.add(cs.Id);
                }
                //Update Target DateTime for First Resonse SLA and Resolution SLA when Case Queue is Updated or Case is Re-Opened
                if((OldCaseMap.get(cs.Id).Case_Queue__c != cs.Case_Queue__c) || (OldCaseMap.get(cs.Id).Status != cs.Status && cs.Status == 'Re-Opened') ){
                    
                    if(OldCaseMap.get(cs.Id).Status != cs.Status && cs.Status == 'Re-Opened'){
                        startDate = System.now();
                        cs.SLA_Paused_in_Hrs__c = 0;
                    }else if(OldCaseMap.get(cs.Id).Case_Queue__c != cs.Case_Queue__c){
                        if(cs.Case_Re_Opened_Date__c != NULL){
                            startDate = cs.Case_Re_Opened_Date__c;
                        }else{
                            startDate = cs.CreatedDate;
                        }                    
                    } System.debug('startDate--'+startDate);
                    
                    if(cs.Case_Queue__c == 'Gold' || cs.Case_Queue__c == 'Silver'){
                        fstResTarDate = BusinessHours.addGmt(bh.id, startDate, goldSilverSLA * 60 * 60 * 1000L);               
                    }else if(cs.Case_Queue__c == 'Platinum'){
                        fstResTarDate = BusinessHours.addGmt(bh.id, startDate, platinumSLA * 60 * 60 * 1000L);
                    }else if(cs.Case_Queue__c == 'Red'){
                        fstResTarDate = BusinessHours.addGmt(bh.id, startDate, redSLA * 60 * 60 * 1000L);
                    }else if(cs.Case_Queue__c == 'Retro'){
                        fstResTarDate = BusinessHours.addGmt(bh.id, startDate, retroSLA * 60 * 60 * 1000L);
                    }
                    if(fstResTarDate != NULL){
                        resSLATarDate = BusinessHours.addGmt(bh.id, startDate, resolutionSLA * 60 * 60 * 1000L);
                    }else{
                        fstResTarDate = NULL;
                        resSLATarDate = NULL;
                    }
                    System.debug('fstResTarDate--'+fstResTarDate+'--resSLATarDate--'+resSLATarDate+'--cs.SLA_Paused_in_Hrs__c--'+cs.SLA_Paused_in_Hrs__c);
                    if(fstResTarDate != NULL && (cs.SLA_Paused_in_Hrs__c != 0 && cs.SLA_Paused_in_Hrs__c != NULL)){
                        Integer pausedMinutes = Integer.valueOf(cs.SLA_Paused_in_Hrs__c*60);
                        cs.First_Response_Target_Date__c = fstResTarDate.addMinutes(pausedMinutes);
                        cs.Resolution_SLA_Target_Date__c = resSLATarDate.addMinutes(pausedMinutes);                    
                    }else{
                        cs.First_Response_Target_Date__c = fstResTarDate;
                        cs.Resolution_SLA_Target_Date__c = resSLATarDate; 
                    }
                }
                //Update SLA Paused Time, Target DateTime for First Resonse SLA and Resolution SLA when SLA is resumed after pausing
                if(cs.IsStopped == false && OldCaseMap.get(cs.Id).IsStopped != cs.IsStopped && cs.StopStartDate != NULL){
                    slaPausedTime = BusinessHours.diff(bh.Id, cs.StopStartDate, System.now());
                    slaPausedHours = slaPausedTime/(60*60*1000);
                    if(cs.SLA_Paused_in_Hrs__c == NULL || cs.SLA_Paused_in_Hrs__c == 0){
                        cs.SLA_Paused_in_Hrs__c = slaPausedHours;
                    }else{
                        cs.SLA_Paused_in_Hrs__c = cs.SLA_Paused_in_Hrs__c + slaPausedHours;
                    }
                    if(slaPausedTime > 0 ){
                        Integer pausedMinutes = Integer.valueOf(slaPausedHours*60);
                        if(cs.First_Response_Target_Date__c != NULL){
                            cs.First_Response_Target_Date__c = cs.First_Response_Target_Date__c.addMinutes(pausedMinutes); System.debug('cs.First_Response_Target_Date__c--'+cs.First_Response_Target_Date__c);
                        }
                        if(cs.Resolution_SLA_Target_Date__c != NULL){
                            cs.Resolution_SLA_Target_Date__c = cs.Resolution_SLA_Target_Date__c.addMinutes(pausedMinutes);
                        }                        
                    }
                }
            }
        }
        //Update Agent Assignment Date if Null
        if(caseAssIdList.size()>0){
            List<CaseHistory> cHistoryList = [SELECT CaseId,CreatedDate,DataType,Field,NewValue,OldValue FROM CaseHistory WHERE CaseId IN: caseAssIdList AND Field = 'Owner'];
            
            System.debug('cHistoryList--'+cHistoryList);
            Map<Id, Datetime> caseAgentAssgMap = new Map<Id, Datetime>();
            
            if(cHistoryList.size()>0){
                
                for(CaseHistory ch: cHistoryList){
                    String oldValue = String.valueof(ch.OldValue);
                    String newValue = String.valueOf(ch.NewValue);
                    if(oldValue.containsIgnoreCase('Queue') && !newValue.containsIgnoreCase('Queue') ){
                        caseAgentAssgMap.put(ch.CaseId, ch.CreatedDate);
                    }
                }
            }
            System.debug('caseAgentAssgMap--'+caseAgentAssgMap);
            for(Case cs: newCaseList){
                if(cs.RecordTypeId == vffRTId && caseAgentAssgMap.containsKey(cs.Id) ){
                    cs.Agent_Assigned_Time__c = caseAgentAssgMap.get(cs.Id);
                }
            }
        }        
    }
    //Update Handling Time by the Agent when Case is Closed
    public static void AfterUpdateCase(List<Case> newCaseList, Map<Id, Case> OldCaseMap){    
        List<Case> upCaseList = new List<Case>();
        decimal handlingTime;
        decimal handlingHours;
        decimal handlingDays;
        decimal handlingMins;
        
        for(Case cs: newCaseList){
            if(cs.RecordTypeId == vffRTId){
                if(cs.Agent_Assigned_Time__c!= NULL && cs.ClosedDate != NULL && OldCaseMap.get(cs.Id).Status != cs.Status){
                    handlingTime = BusinessHours.diff(bh.Id, cs.Agent_Assigned_Time__c, cs.ClosedDate); 
                    Datetime dt= BusinessHours.addGmt (bh.id, cs.Agent_Assigned_Time__c, 23 * 60 * 60 * 1000L);
                    if(cs.SLA_Paused_in_Hrs__c != NULL && cs.SLA_Paused_in_Hrs__c != 0){
                        handlingHours = handlingTime/(60*60*1000) - cs.SLA_Paused_in_Hrs__c;
                    }else{
                        handlingHours = handlingTime/(60*60*1000);
                    }
                    if(handlingHours != 0 && handlingHours != NULL){
                        handlingDays = handlingHours/9;
                    	handlingMins = handlingHours*60;
                        
                        Case upCase = new Case();
                    	upCase.Id = cs.Id;
                    	upCase.Handling_Time_Hrs__c = handlingHours.setScale(2);
                    	upCase.Handling_Time_Days__c = handlingDays.setScale(2);
                    	upCase.Handling_Time_Mins__c = handlingMins.setScale(2);
                    	upCaseList.add(upCase);
                    }                    
                    System.debug('handlingTime--'+handlingTime+'--handlingHours--'+handlingHours+'--handlingDays--'+handlingDays);                                                    
                }
                if(cs.First_Response_Time__c != NULL && OldCaseMap.get(cs.Id).First_Response__c != cs.First_Response__c){
                    if(cs.Case_Re_Opened_Date__c == NULL){
                        handlingTime = BusinessHours.diff(bh.Id, cs.CreatedDate, cs.First_Response_Time__c);
                    }else{
                        handlingTime = BusinessHours.diff(bh.Id, cs.Case_Re_Opened_Date__c, cs.First_Response_Time__c);
                    }
                    
                    if(handlingTime != 0 && handlingTime!=NULL){
                        handlingMins = handlingTime/(60*1000);
                    
                    	Case upCase1 = new case();
                    	upCase1.Id = cs.Id;
                    	upCase1.First_Response_Handling_Time_in_Mins__c = handlingMins.setScale(2);
                    	upCaseList.add(upCase1);
                    }                    
                }
            }
        }
        System.debug('upCaseList---'+upCaseList);
        if(upCaseList.size()>0){
            update upCaseList;
        }
    }
}