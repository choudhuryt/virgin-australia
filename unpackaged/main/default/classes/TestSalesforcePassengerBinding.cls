/**
* @description       : Test class for DomesticAddendumDiscountController
* @Updated By         : Cloudwerx
**/
@isTest
private class TestSalesforcePassengerBinding {
    
    @testSetup static void setup() {      
        Apex_Callouts__c testApexCalloutObj = TestDataFactory.createTestApexCallouts('PassengerDetails', 'https://services-mssl.virginaustralia.com/service/partner/salesforce/1.0/SalesforceLoyalty', 90000, 'client_mssl_virginaustralia2018_com');
        INSERT testApexCalloutObj;
    }
    
    @isTest
    private static void testGetPassengerDetails() {
        Test.setMock(WebServiceMock.class, new PassengerManifestServiceMockImpl());
        Test.startTest();
        SalesforcePassengerBinding testObject = new SalesforcePassengerBinding();
        SalesForceHistoricalDetailModel.GetPassengerManifestRSType response = testObject.getPassengerDetails('VN1234', '24', '12/12/2012');
        Test.stopTest();
        //Asserts
        System.assertEquals('VN1234', response.AirlineCode);
    }
}