public  with sharing class AccountOwnerPointCheckController {
    
         ApexPages.StandardController scontroller {get; set;}    
         public List<Waiver_Point__c> searchResults {get;set;}        
         public Waiver_Point__c usercheck { get; set; }
         public string userId {get; set;}
    
         private User a;
    
     public AccountOwnerPointCheckController(ApexPages.StandardController controller) 
	{
		 sController = controller;
         usercheck = new Waiver_Point__c();
         
    }
    
    
    public PageReference Check()
    {
     
   
        
     searchResults= [select id , Waiver_Points_Allocated__c, Waiver_Points_Used__c,
                     Waiver_Points_Pending_Approval__c,Waiver_Point_Status__c,Waiver_Points_Remaining__c
                     from Waiver_Point__c where Account_Manager__c = :usercheck.Account_Manager__c
                     and status__c = 'Active'
                    ];
        
     if(searchResults.size()==0)
                  {
                   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No waiver points allocated for this user');
                   ApexPages.addMessage(myMsg); 
                  }  
         
     return null ;
     }
  
    public PageReference UpdateUser()
    {
        userId = usercheck.Account_Manager__c;
        PageReference pageRef = Page.AccountManagerWaiverPointUpdate;
        pageRef.getParameters().put('id', userid);  
		pageRef.setRedirect(true);		
		return pageRef;   
    }
}