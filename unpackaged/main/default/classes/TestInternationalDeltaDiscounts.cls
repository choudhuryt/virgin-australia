/**  File Name      : TestInternationalSHDiscountController
* Description        : This Apex Test Class is the Test Class for DomesticDiscountController  
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Andy Burgess
* * Date             : Created 28 February 2013
* * Technical Task ID: 
* * Notes            :  The test class for this file is:  TestDomesticDiscountController
* Modification Log =============================================================== 
Ver Date Author Modification --- ---- ------ -------------
* */
/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
* @updatedBy : cloudwerx
*/
@isTest
private class TestInternationalDeltaDiscounts {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('Test Contract', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj.Cos_Version__c = '13.0';
        testContractObj.Red_Circle__c =true;
        testContractObj.IsTemplate__c =true;
        INSERT testContractObj;
    }
    
    @isTest
    private static void testInternationalDiscountControllerDeltaWithRelatedRecords() {
        Contract testContractObj = [SELECT Id FROM Contract];
        International_Discounts_USA_Canada__c testInternationalDiscountsUSACanadaObj = TestDataFactory.createTestInternationalDiscountsUSACanada(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsUSACanadaObj;
        International_Discounts_DL_CRF__c testInternationalDiscountsDLCRFObj = TestDataFactory.createTestInternationalDiscountsDLCRF(testContractObj.Id, '1', '13.2');
        INSERT testInternationalDiscountsDLCRFObj;
        
        Test.startTest();
        PageReference pageRef = Page.InternationalDiscountDeltaPage;
        pageRef.getParameters().put('id', testContractObj.id);
        Test.setCurrentPage(pageRef);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        InternationalDiscountControllerDelta lController = new InternationalDiscountControllerDelta(conL);
        lController.isCos13_4greater = true;
        PageReference initDiscPageReference = lController.initDisc();
        PageReference savePageReference = lController.save();
        PageReference qsavePageReference = lController.qsave();
        PageReference cancelPageReference = lController.cancel();
        PageReference newInterUSAPageReference = lController.newInterUSA();
        PageReference remUSACANPageReference = lController.remUSACAN();
        PageReference newCRFInterUSAPageReference = lController.newCRFInterUSA();
        test.stopTest();
        //Asserts
        system.assertEquals(true, lController.isCos13greater);
        system.assertEquals(1, lController.usacanFlag);
        system.assertEquals('/' + testContractObj.Id, savePageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountDeltaPage?id=' + testContractObj.Id, qsavePageReference.getUrl());
        system.assertEquals('/' + testContractObj.Id, cancelPageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountDeltaPage?id=' + testContractObj.Id, newInterUSAPageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountDeltaPage?id=' + testContractObj.Id, remUSACANPageReference.getUrl());
        system.assertEquals(null, newCRFInterUSAPageReference);
    }
    
    @isTest
    private static void testInternationalDiscountControllerDeltaWithRelatedRecordsRAMSCAN() {
        Contract testContractObj = [SELECT Id, UpperPerInt__c FROM Contract];
        testContractObj.UpperPerInt__c = 3;
        UPDATE testContractObj;
        
        International_Discounts_USA_Canada__c testInternationalDiscountsUSACanadaObj = TestDataFactory.createTestInternationalDiscountsUSACanada(testContractObj.Id, '1', false, '13.2');
        testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_off_Published_Fare__c = 10;
        testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_off_PublishedFare2__c = 10;
        testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_off_PublishedFare3__c = 10;
        testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_off_PublishedFare4__c = 10;
        testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_off_PublishedFare5__c = 10;
        testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_off_PublishedFare6__c = 10;
        testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_off_PublishedFare7__c = 10;
        testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_off_PublishedFare8__c = 10;
        INSERT testInternationalDiscountsUSACanadaObj;
        International_Discounts_DL_CRF__c testInternationalDiscountsDLCRFObj = TestDataFactory.createTestInternationalDiscountsDLCRF(testContractObj.Id, '1', '13.2');
        INSERT testInternationalDiscountsDLCRFObj;
        
        Test.startTest();
        PageReference pageRef = Page.InternationalDiscountDeltaPage;
        pageRef.getParameters().put('id', testContractObj.id);
        Test.setCurrentPage(pageRef);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        InternationalDiscountControllerDelta lController = new InternationalDiscountControllerDelta(conL);
        lController.isCos13_4greater = true;
        PageReference initDiscPageReference = lController.initDisc();
        PageReference savePageReference = lController.save();
        PageReference qsavePageReference = lController.qsave();
        PageReference cancelPageReference = lController.cancel();
        PageReference newInterUSAPageReference = lController.newInterUSA();
        PageReference newCRFInterUSAPageReference = lController.newCRFInterUSA();
        PageReference remCRFInterUSAUSACANPageReference = lController.remCRFInterUSAUSACAN();
        test.stopTest();
        //Asserts
        system.assertEquals(true, lController.isCos13greater);
        system.assertEquals(1, lController.usacanFlag);
        system.assertEquals('/' + testContractObj.Id, savePageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountDeltaPage?id=' + testContractObj.Id, qsavePageReference.getUrl());
        system.assertEquals('/' + testContractObj.Id, cancelPageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountDeltaPage?id=' + testContractObj.Id, newInterUSAPageReference.getUrl());
        system.assertEquals('/apex/InternationalDiscountDeltaPage?id=' + testContractObj.Id, remCRFInterUSAUSACANPageReference.getUrl());
        system.assertEquals(null, newCRFInterUSAPageReference);
        List<International_Discounts_DL_CRF__c> internationalDiscountsDLCRFResults = lController.searchResultsCRF;
        system.assertEquals('D', internationalDiscountsDLCRFResults[0].Intl_Discount_USA_CAN_EligBk_Class3__c);
        system.assertEquals('C', internationalDiscountsDLCRFResults[0].Intl_Discount_USA_CAN_EligBk_Class2__c);
        system.assertEquals('J', internationalDiscountsDLCRFResults[0].Intl_Discount_USA_CAN_EligBk_Class__c);
        system.assertEquals('Elevate', internationalDiscountsDLCRFResults[0].Intl_Dis_USA_CAN_Eligible_FareType16__c);
        system.assertEquals('USA/Canada/Mexico', internationalDiscountsDLCRFResults[0].Applicable_Routes__c);
    }
    
    @isTest
    private static void testInternationalDiscountControllerDeltaWithApexMessage() {
        Contract testContractObj = [SELECT Id FROM Contract];
        
        Test.startTest();
        PageReference pageRef = Page.InternationalDiscountDeltaPage;
        pageRef.getParameters().put('id', testContractObj.id);
        Test.setCurrentPage(pageRef);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        InternationalDiscountControllerDelta lController = new InternationalDiscountControllerDelta(conL);
        lController.isCos13_4greater = true;
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newInterUSAPageReference = lController.newInterUSA();
        test.stopTest();
        //Asserts
        system.assertEquals(true, lController.isCos13greater);
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('A Delta Tier has not been Selected in the contract so you cannot add a discount', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.ERROR, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testInternationalDiscountControllerDeltaWithCANADAUSDiscount() {
        Contract testContractObj = [SELECT Id, VA_DL_Requested_Tier__c FROM Contract];
        testContractObj.VA_DL_Requested_Tier__c = '1';
        UPDATE testContractObj;
        
        Test.startTest();
        PageReference pageRef = Page.InternationalDiscountDeltaPage;
        pageRef.getParameters().put('id', testContractObj.id);
        Test.setCurrentPage(pageRef);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        InternationalDiscountControllerDelta lController = new InternationalDiscountControllerDelta(conL);
        lController.isCos13_4greater = true;
        PageReference initDiscPageReference = lController.initDisc();
        International_Discounts_USA_Canada__c testInternationalDiscountsUSACanadaObj = TestDataFactory.createTestInternationalDiscountsUSACanada(testContractObj.Id, '1', true, '13.0');
        INSERT testInternationalDiscountsUSACanadaObj;
        PageReference newInterUSAPageReference = lController.newInterUSA();
        test.stopTest();
        //Asserts
        system.assertEquals(true, lController.isCos13greater);
        List<International_Discounts_USA_Canada__c> internationalDiscountsUSACanadaResults = lController.searchResults;
        system.assertEquals(testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_EligBk_Class3__c, internationalDiscountsUSACanadaResults[0].Intl_Discount_USA_CAN_EligBk_Class3__c);
        system.assertEquals(testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_EligBk_Class2__c, internationalDiscountsUSACanadaResults[0].Intl_Discount_USA_CAN_EligBk_Class2__c);
        system.assertEquals(testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_EligBk_Class__c, internationalDiscountsUSACanadaResults[0].Intl_Discount_USA_CAN_EligBk_Class__c);
        system.assertEquals(testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_EligBk_Class__c, internationalDiscountsUSACanadaResults[0].Intl_Dis_USA_CAN_Eligible_FareType13__c);
        system.assertEquals(testInternationalDiscountsUSACanadaObj.Intl_Discount_USA_CAN_EligBk_Class__c, internationalDiscountsUSACanadaResults[0].Applicable_Routes_BNE_SYD_MEL_to__c);
    }
    
    @isTest
    private static void testInternationalDiscountControllerDeltaWithFalseRedCircle() {
        Contract testContractObj = [SELECT Id, VA_DL_Requested_Tier__c, Red_Circle__c FROM Contract];
        testContractObj.VA_DL_Requested_Tier__c = '1';
        testContractObj.Red_Circle__c = false;
        UPDATE testContractObj;
        
        Test.startTest();
        PageReference pageRef = Page.InternationalDiscountDeltaPage;
        pageRef.getParameters().put('id', testContractObj.id);
        Test.setCurrentPage(pageRef);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        InternationalDiscountControllerDelta lController = new InternationalDiscountControllerDelta(conL);
        lController.isCos13_4greater = true;
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newInterUSAPageReference = lController.newInterUSA();
        test.stopTest();
        //Asserts
        system.assertEquals(true, lController.isCos13greater);
        List<International_Discounts_USA_Canada__c> internationalDiscountsUSACanadaResults = lController.searchResults;
        system.assertEquals('D', internationalDiscountsUSACanadaResults[0].Intl_Discount_USA_CAN_EligBk_Class3__c);
        system.assertEquals('C', internationalDiscountsUSACanadaResults[0].Intl_Discount_USA_CAN_EligBk_Class2__c);
        system.assertEquals('J', internationalDiscountsUSACanadaResults[0].Intl_Discount_USA_CAN_EligBk_Class__c);
        system.assertEquals('Elevate', internationalDiscountsUSACanadaResults[0].Intl_Dis_USA_CAN_Eligible_FareType13__c);
        system.assertEquals('USA/Canada/Mexico', internationalDiscountsUSACanadaResults[0].Applicable_Routes_BNE_SYD_MEL_to__c);
    }
    
    @isTest
    private static void testInternationalDiscountControllerDeltaWithFalseCos13greater() {
        Contract testContractObj = [SELECT Id, VA_DL_Requested_Tier__c, Red_Circle__c, Cos_Version__c FROM Contract];
        testContractObj.VA_DL_Requested_Tier__c = '1';
        testContractObj.Red_Circle__c = false;
        testContractObj.Cos_Version__c = '14.2';
        UPDATE testContractObj;
        
        Test.startTest();
        PageReference pageRef = Page.InternationalDiscountDeltaPage;
        pageRef.getParameters().put('id', testContractObj.id);
        Test.setCurrentPage(pageRef);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        InternationalDiscountControllerDelta lController = new InternationalDiscountControllerDelta(conL);
        lController.isCos13_4greater = true;
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newInterUSAPageReference = lController.newInterUSA();
        test.stopTest();
        //Asserts
        List<International_Discounts_USA_Canada__c> internationalDiscountsUSACanadaResults = lController.searchResults;
        system.assertEquals('D/I', internationalDiscountsUSACanadaResults[0].Intl_Discount_USA_CAN_EligBk_Class3__c);
        system.assertEquals('C', internationalDiscountsUSACanadaResults[0].Intl_Discount_USA_CAN_EligBk_Class2__c);
        system.assertEquals('J', internationalDiscountsUSACanadaResults[0].Intl_Discount_USA_CAN_EligBk_Class__c);
        system.assertEquals('Elevate', internationalDiscountsUSACanadaResults[0].Intl_Dis_USA_CAN_Eligible_FareType13__c);
        system.assertEquals('USA/Canada/Mexico', internationalDiscountsUSACanadaResults[0].Applicable_Routes_BNE_SYD_MEL_to__c);
    }
    
    @isTest
    private static void testInternationalDiscountControllerDeltaWithDLCRFRecords() {
        Contract testContractObj = [SELECT Id, NewDLDiscountsActive__c FROM Contract];
        testContractObj.NewDLDiscountsActive__c = false;
        UPDATE testContractObj;
        
        International_Discounts_USA_Canada__c testInternationalDiscountsUSACanadaObj = TestDataFactory.createTestInternationalDiscountsUSACanada(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsUSACanadaObj;
        International_Discounts_DL_CRF__c testInternationalDiscountsDLCRFObj = TestDataFactory.createTestInternationalDiscountsDLCRF(testContractObj.Id, '1', '13.2');
        INSERT testInternationalDiscountsDLCRFObj;
        
        Test.startTest();
        PageReference pageRef = Page.InternationalDiscountDeltaPage;
        pageRef.getParameters().put('id', testContractObj.id);
        Test.setCurrentPage(pageRef);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        InternationalDiscountControllerDelta lController = new InternationalDiscountControllerDelta(conL);
        lController.isCos13_4greater = true;
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newCRFInterUSAPageReference = lController.newCRFInterUSA();
        test.stopTest();
        //Asserts 
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('A Delta Tier has not been Selected in the contract so you cannot add a discount', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.ERROR, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testInternationalDiscountControllerDeltaWithDLCRFApexMessage() {
        Contract testContractObj = [SELECT Id, NewDLDiscountsActive__c FROM Contract];
        testContractObj.NewDLDiscountsActive__c = true;
        UPDATE testContractObj;
        
        International_Discounts_USA_Canada__c testInternationalDiscountsUSACanadaObj = TestDataFactory.createTestInternationalDiscountsUSACanada(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsUSACanadaObj;
        International_Discounts_DL_CRF__c testInternationalDiscountsDLCRFObj = TestDataFactory.createTestInternationalDiscountsDLCRF(testContractObj.Id, '1', '13.2');
        INSERT testInternationalDiscountsDLCRFObj;
        
        Test.startTest();
        PageReference pageRef = Page.InternationalDiscountDeltaPage;
        pageRef.getParameters().put('id', testContractObj.id);
        Test.setCurrentPage(pageRef);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        InternationalDiscountControllerDelta lController = new InternationalDiscountControllerDelta(conL);
        lController.isCos13_4greater = true;
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newCRFInterUSAPageReference = lController.newCRFInterUSA();
        test.stopTest();
        //Asserts 
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('A Delta Tier has not been Selected in the contract so you cannot add a discount', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.ERROR, msg.getSeverity());
        }
    }
}