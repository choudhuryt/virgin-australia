public with sharing class ContractDiscountViewController 
{
 public String CurrentContractId{set;get;}  
 public String CurrentMarket{set;get;} 
 public Boolean dom {get;set;}
 public Boolean reg {get;set;}
 public Boolean ish {get;set;}
 public Boolean tt {get;set;}
 public Boolean hk {get;set;}
 public Boolean china {get;set;}
 public Boolean japan {get;set;}     
 public Boolean london {get;set;} 
 public Boolean na {get;set;}   
 public Boolean sq {get;set;} 
 public Boolean ey {get;set;} 
 public Boolean red {get;set;}    
 public integer ttFlag {get; set;} 
 public integer cnFlag {get; set;}   
 public integer jpFlag {get; set;}  
 public integer hkFlag {get; set;} 
 public integer ishFlag {get; set;} 
 public integer pngFlag {get; set;} 
 public integer lonFlag {get; set;}    
 public integer naFlag {get; set;}   
public integer sqeuFlag {get; set;}    
public integer sqasFlag {get; set;} 
public integer sqafFlag {get; set;} 
public integer sqodFlag {get; set;} 
public integer eyeuFlag {get; set;} 
public integer eyafFlag {get; set;} 
public integer eymeFlag {get; set;} 
public integer redFlag {get; set;}
public integer eyeuz1Flag {get; set;}  
public integer eyeuz2Flag {get; set;} 
public integer eyeuz3Flag {get; set;}    
public integer eymez1Flag {get; set;}   
public integer eymez2Flag {get; set;}      
public integer eymez3Flag {get; set;}   
public integer eymez4Flag {get; set;}        
public List<Proposal_Table__c> domResults {get;set;}
public List<Proposal_Table__c> regResults {get;set;}  
public List<Proposal_Table__c> ishResults {get;set;} 
public List<Proposal_Table__c> ttResults {get;set;} 
public List<Proposal_Table__c> hkResults {get;set;}   
public List<Proposal_Table__c> cnResults {get;set;}  
public List<Proposal_Table__c> jpResults {get;set;}
public List<Proposal_Table__c> jpbResults {get;set;}     
public List<Proposal_Table__c> lonResults {get;set;}  
public List<Proposal_Table__c> naResults {get;set;} 
public List<Proposal_Table__c> eyeuResults {get;set;} 
public List<Proposal_Table__c> eyafResults {get;set;} 
public List<Proposal_Table__c> eymeResults {get;set;}    
public List<Proposal_Table__c> sqeuResults {get;set;}
public List<Proposal_Table__c> sqasResults {get;set;}  
public List<Proposal_Table__c> sqafResults {get;set;}   
public List<Red_Circle_Discounts__c> redResults {get;set;} 
public List<Proposal_Table__c> eyeuz1Results {get;set;}   
public List<Proposal_Table__c> eyeuz2Results {get;set;}  
public List<Proposal_Table__c> eyeuz3Results {get;set;}       
public List<Proposal_Table__c> eymez1Results {get;set;}   
public List<Proposal_Table__c> eymez2Results {get;set;} 
public List<Proposal_Table__c> eymez3Results {get;set;} 
public List<Proposal_Table__c> eymez4Results {get;set;}     
public Contract contractod {get;set;}
public String conname {get;set;}     
public List <Contract> con {get;set;}        
    
  public ContractDiscountViewController() 
    {
  
        
        if(CurrentContractId == null)
		{
			CurrentContractId= ApexPages.currentPage().getParameters().get('cid');
		}
		if(CurrentMarket == null)
		{
			CurrentMarket= ApexPages.currentPage().getParameters().get('Region');
		}
        
    }
   public PageReference initDisc() 
     {  
         
      dom = false;  
      reg = false; 
      tt = false; 
      ish= false; 
      hk = false; 
      china = false;
      japan = false;   
      red = false;   
      ttFlag = 0;
      ishFlag = 0; 
      cnFlag = 0; 
      jpFlag = 0;    
      hkFlag = 0; 
      lonFlag = 0; 
      naFlag = 0; 
      eyeuFlag = 0; 
      eyafFlag = 0;
      eymeFlag = 0 ;   
      sqasFlag = 0;   
      sqeuFlag = 0;  
      sqafFlag = 0;
      sqodflag =0;
      redflag=0;
      eyeuz1Flag = 0;
      eyeuz2Flag = 0; 
      eyeuz3Flag = 0; 
      eymez1Flag = 0 ;  
      eymez2Flag = 0 ; 
      eymez3Flag = 0 ;
      eymez4Flag = 0 ;      
         
      if(CurrentContractId == null)
		{
			CurrentContractId= ApexPages.currentPage().getParameters().get('cid');
		}   
        if(CurrentMarket == null)
		{
			CurrentMarket= ApexPages.currentPage().getParameters().get('Region');
		} 
         
         con =         [Select Name ,Contractnumber from Contract where
                          id = :ApexPages.currentPage().getParameters().get('cid')
                         ];
         
         if(con.size() > 0)
         {
           conname = con[0].name + ' : '  + con[0].contractnumber;  
         }         
          
         
         if (CurrentMarket == 'DOMREG')
         {
           dom = TRUE;    
         } else if (CurrentMarket == 'INTSH')
         {
             
          ish= TRUE;              
         }  else if  (CurrentMarket == 'LONDON')
         {
             
          london= TRUE;              
         }  else if  (CurrentMarket == 'NA')
         {
             
          na= TRUE;              
         } else if  (CurrentMarket == 'EY')
         {
             
          EY = TRUE;              
         } else if  (CurrentMarket == 'SQ')
         {
             
          SQ = TRUE;              
         } 
         
        else if  (CurrentMarket == 'RED')
         {
             
          RED = TRUE;              
         }          
        system.debug( 'The values' +  CurrentContractId + CurrentMarket ) ;
         
        domResults= [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c 
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name like'%DOM%Mainline%'
                       ORDER BY Sort_Order__c
                     ];
         
         regResults= [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c 
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name like '%DOM%Regional%'
                       ORDER BY Sort_Order__c
                     ];
         
         ishResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c 
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name like '%INT%Short%Haul'
                       ORDER BY Sort_Order__c
                     ];
          if(ishResults.size() > 0)
         {
             ishFlag = 1 ;
         }
         
         ttResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c 
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name like '%Trans%Tasman%'
                       and (NOT Name  like '%ex NZ%')
                       ORDER BY Sort_Order__c 
                     ]; 
         if(ttResults.size() > 0)
         {
             ttFlag = 1 ;
         }
         
          hkResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c 
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name like '%Hong%Kong%'
                       ORDER BY Sort_Order__c 
                     ]; 
         if(hkResults.size() > 0)
         {
             hkFlag = 1 ;
         }
         cnResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c 
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name like  '%China%'
                       ORDER BY Sort_Order__c
                     ]; 
                        
         if(cnResults.size() > 0)
         {
             cnFlag = 1 ;
         } 
         jpResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c 
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name =  'Japan'
                       ORDER BY Sort_Order__c
                     ]; 
         
          jpbResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c 
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name =  'Japan JPBeyond'
                       ORDER BY Sort_Order__c
                     ]; 
                        
         if(jpResults.size() > 0)
         {
             jpFlag = 1 ;
         } 
         
         
          lonResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c 
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name like '%London%'
                       ORDER BY Sort_Order__c
                     ]; 
                        
         if(lonResults.size() > 0)
         {
            lonFlag = 1 ;
         } 
         naResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c 
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name like '%North%America%'
                       ORDER BY Sort_Order__c
                     ]; 
                        
         if(naResults.size() > 0)
         {
            naFlag = 1 ;
         } 
         
         eyeuResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name LIKE '%UK/Europe%EY%'
                       AND Zone_Rergion__c =  NULL
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(eyeuResults.size() > 0)
         {
            eyeuFlag = 1 ;
         }


       eyeuz1Results = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                        Contract__c  =:CurrentContractId
                       and Name LIKE '%UK/Europe%EY%'
                       AND Zone_Rergion__c = '1'
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(eyeuz1Results.size() > 0)
         {
            eyeuz1Flag = 1 ;
         }
         
        eyeuz2Results = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                        Contract__c  =:CurrentContractId
                       and Name LIKE '%UK/Europe%EY%'
                       AND Zone_Rergion__c = '2'
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(eyeuz2Results.size() > 0)
         {
            eyeuz2Flag = 1 ;
         }
        
        eyeuz3Results = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                        Contract__c  =:CurrentContractId
                       and Name LIKE '%UK/Europe%EY%'
                       AND Zone_Rergion__c = '3'
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(eyeuz3Results.size() > 0)
         {
            eyeuz3Flag = 1 ;
         }
           
         
          eymeResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name LIKE '%Middle%East%EY%'
                       AND Zone_Rergion__c =  NULL  
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(eymeResults.size() > 0)
         {
            eymeFlag = 1 ;
         }
         
            eymez1Results = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                        Contract__c  =:CurrentContractId
                       and Name LIKE '%Middle%East%EY%'
                       AND Zone_Rergion__c = '1'   
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(eymez1Results.size() > 0)
         {
            eymez1Flag = 1 ;
         }
          
          eymez2Results = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                       Contract__c  =:CurrentContractId
                       and Name LIKE '%Middle%East%EY%'
                       AND Zone_Rergion__c = '2'   
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(eymez2Results.size() > 0)
         {
            eymez2Flag = 1 ;
         }
         
            eymez3Results = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                       Contract__c  =:CurrentContractId
                       and Name LIKE '%Middle%East%EY%'
                       AND Zone_Rergion__c = '3'   
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(eymez3Results.size() > 0)
         {
            eymez3Flag = 1 ;
         }
         
         eymez4Results = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                        Contract__c  =:CurrentContractId
                       and Name LIKE '%Middle%East%EY%'
                       AND Zone_Rergion__c = '4'   
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(eymez4Results.size() > 0)
         {
            eymez4Flag = 1 ;
         } 
         
         
         
         eyafResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name LIKE '%Africa%EY%'
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(eyafResults.size() > 0)
         {
            eyafFlag = 1 ;
         } 
         
          sqeuResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name LIKE '%UK/Europe%SQ%'
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(sqeuResults.size() > 0)
         {
            sqeuFlag = 1 ;
         } 
         
          sqasResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name LIKE '%Asia%SQ%'
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(sqasResults.size() > 0)
         {
            sqasFlag = 1 ;
         } 
         
           sqafResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c
                       FROM Proposal_Table__c WHERE
                       Contract__c =:CurrentContractId
                       and Name LIKE '%Africa%SQ%'
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(sqafResults.size() > 0)
         {
            sqafFlag = 1 ;
         } 
         
         
          redResults = [ SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                       ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                       Partner_Eligible_Booking_Class__c ,Zone_Rergion__c,Fixed_Base_Fare__c
                       FROM Red_Circle_Discounts__c WHERE
                       Contract__c =:CurrentContractId
                       ORDER BY Name, Zone_Rergion__c ,Sort_Order__c ASC NULLS FIRST
                     ]; 
                        
         if(redResults.size() > 0)
         {
            redFlag = 1 ;
         } 
         
        List<Contract> ondResults = [ SELECT SQEUOND__c,SQNAOND__c,SQSEACode__c,SQSEAOND__c,SQWAAOND__c,SQ_Discount_Type__c,
                                             	EUZ1ONDSelected__c,	EUZ2ONDSelected__c,	EUZ3ONDSelected__c,
                                                MEZ1ONDSelected__c,MEZ2ONDSelected__c,MEZ3ONDSelected__c,MEZ4ONDSelected__c FROM Contract
                         WHERE
                         id =:CurrentContractId
                     ]; 
        if(ondResults.size() > 0) 
        {
         contractod = ondResults.get(0);
         if (contractod.SQ_Discount_Type__c == 'Specific OD Pairs')
         {
             sqodflag = 1;
         }
        } 
         
     return null;
     
     }
    
     public PageReference cancel() {
   // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
      PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('cid'));
                  //
                  thePage.setRedirect(true);
                  //
                  return thePage;
   
  
   
  }
     public PageReference cancel1() {
      PageReference newPage;
     newPage = Page.ContractMarketDiscountView;
     newPage.getParameters().put('id', ApexPages.currentPage().getParameters().get('cid')); 
     newPage.setRedirect(true);
     return newPage;  
   
  }
    
}