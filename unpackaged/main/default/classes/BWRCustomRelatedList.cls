/**
 * @author: Joy S. Marasigan
 * @createdDate: April 12, 2016
 * @description: An extension class for the Blanket Waiver Request object to handle the creation of new blanket request versions.
 * Please see Case #00101013 for more information regarding the requirements
 **/

public class BWRCustomRelatedList {
    
    public List<Blanket_Waiver_Request__c> bwrRelatedList {get; set;}
    public Blanket_Waiver_Request__c currentRecord {get; set;}
    
    public BWRCustomRelatedList(ApexPages.StandardController controller){
        //get necessary details for the current record - controller.getRecord() doesn't provide some of the fields
        currentRecord = [SELECT Id, Replicated_From__c, Unique_Waiver_Code__c
                            FROM Blanket_Waiver_Request__c
                            WHERE Id =: controller.getId()
                        ];
                                                
        bwrRelatedList = new List<Blanket_Waiver_Request__c>();
        
        if(!String.isBlank(currentRecord.Unique_Waiver_Code__c)){
            //get all records with the same unique waiver code
            bwrRelatedList = [SELECT Id, Name, Unique_Waiver_Code__c, Waiver_Type__c, Submission_Counter__c,
                                Waiver_Group__c, Blanket_Waiver_Start_Date__c, Replicated_From__c,
                                Blanket_Waiver_End_Date__c, CreatedBy.Name, RecordType.Name
                                FROM Blanket_Waiver_Request__c
                                WHERE (Unique_Waiver_Code__c =: currentRecord.Unique_Waiver_Code__c
                                AND Id !=: currentRecord.Id)
                                ORDER BY Submission_Counter__c
                            ];
        }
        
    }
}