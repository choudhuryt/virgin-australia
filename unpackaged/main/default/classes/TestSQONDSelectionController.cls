/**
* @description       : Test class for SQONDSelectionController
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestSQONDSelectionController {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;  
        
        Contract testContractObj = TestDataFactory.createTestContract('TestContract', testAccountObj.Id, 'Draft', '15', null, true, Date.today());
        testContractObj.VA_SQ_Requested_Tier__c = '5';
        testContractObj.SQ_Discount_Type__c = 'Specific OD Pairs' ;
        testContractObj.Cos_Version__c = '14.4';
        testContractObj.Red_Circle__c = false;
        INSERT testContractObj; 
    }
    
    @isTest
    private static void testSQONDSelectionController() {
        Contract testContractObj = [SELECT Id, EUONDSelected__c, SQNAONDSelected__c, 
                                    SQWAAONDSelected__c, SQSEAONDSelected__c, SQEUONDSelected1__c, 
                                    SQUSONDSelected__c, SQWAAONDSelected1__c, SQEUONDSelected2__c, 
                                    SQSEAONDSelected1__c, SQNAONDSelected1__c FROM Contract];
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        SQONDSelectionController lController = new SQONDSelectionController(conL);
        Test.startTest(); 
        
        PageReference  pageRef = Page.EUONDSelection;
        Test.setCurrentPage(pageRef);
        pageRef = lController.initDisc();
        pageRef = lController.save(); 
        
        lController.EUONDItems = new List<String> {'Adelaide-Amsterdam', 'Perth-Amsterdam'};
        string [] testEUONDItems = lController.EUONDItems;
        
        lController.NAONDItems = new List<String> {'Perth-Guangzhou', 'Brisbane -Chongqing'};
        string [] testNAONDItems = lController.NAONDItems;
        
        lController.WAAONDItems = new List<String> {'Darwin-Ahmedabad', 'Cairns-Bangalore'};
        string [] testWAAONDItems = lController.WAAONDItems;
        
        lController.SEAONDItems = new List<String> {'Cairns-Bandung', 'Adelaide-Kota Kinabalu'};
        string [] testSEAONDItems = lController.SEAONDItems;
        
        lController.EUONDItems1 = new List<String> {'Adelaide-Munich', 'Darwin-Zurich'};
        string [] testEUONDItems1 = lController.EUONDItems1;
        
        lController.EUONDItems2 = new List<String> {'Australia-Frankfurt', 'Australia-Paris'};
        string [] testEUONDItems2 = lController.EUONDItems2;
        
        lController.SEAONDItems1 = new List<String> {'Australia-Bandar Seri Begawan', 'Australia-Kalibo'};
        string [] testSEAONDItems1 = lController.SEAONDItems1;
        
        lController.NAONDItems1 = new List<String> {'Australia-Fukuoka', 'Australia-Hong Kong'};
        string [] testNAONDItems1 = lController.NAONDItems1;
        
        lController.USONDItems = new List<String> {'Brisbane -Houston', 'Adelaide-San Francisco'};
        string [] testUSONDItems = lController.USONDItems;
        
        lController.WAAONDItems1 = new List<String> {'Australia-Ahmedabad', 'Australia-Bangalore'};
        string [] testWAAONDItems1 = lController.WAAONDItems1;
        test.stopTest();
		//Asserts
        System.assertEquals('Adelaide-Amsterdam;Perth-Amsterdam', testContractObj.EUONDSelected__c);
        System.assertEquals('Adelaide-Amsterdam', testEUONDItems[0]);
        System.assertEquals('Perth-Amsterdam', testEUONDItems[1]);
        System.assertEquals('Perth-Guangzhou;Brisbane -Chongqing', testContractObj.SQNAONDSelected__c);
        System.assertEquals('Perth-Guangzhou', testNAONDItems[0]);
        System.assertEquals('Brisbane -Chongqing', testNAONDItems[1]);
        System.assertEquals('Darwin-Ahmedabad;Cairns-Bangalore', testContractObj.SQWAAONDSelected__c);
        System.assertEquals('Darwin-Ahmedabad', testWAAONDItems[0]);
        System.assertEquals('Cairns-Bangalore', testWAAONDItems[1]);
        System.assertEquals('Cairns-Bandung;Adelaide-Kota Kinabalu', testContractObj.SQSEAONDSelected__c);
        System.assertEquals('Cairns-Bandung', testSEAONDItems[0]);
        System.assertEquals('Adelaide-Kota Kinabalu', testSEAONDItems[1]);
        System.assertEquals('Adelaide-Munich;Darwin-Zurich', testContractObj.SQEUONDSelected1__c);
        System.assertEquals('Adelaide-Munich', testEUONDItems1[0]);
        System.assertEquals('Darwin-Zurich', testEUONDItems1[1]);
        System.assertEquals('Australia-Frankfurt;Australia-Paris', testContractObj.SQEUONDSelected2__c);
        System.assertEquals('Australia-Frankfurt', testEUONDItems2[0]);
        System.assertEquals('Australia-Paris', testEUONDItems2[1]);
        System.assertEquals('Australia-Bandar Seri Begawan;Australia-Kalibo', testContractObj.SQSEAONDSelected1__c);
        System.assertEquals('Australia-Bandar Seri Begawan', testSEAONDItems1[0]);
        System.assertEquals('Australia-Kalibo', testSEAONDItems1[1]);
        System.assertEquals('Australia-Fukuoka;Australia-Hong Kong', testContractObj.SQNAONDSelected1__c);
        System.assertEquals('Australia-Fukuoka', testNAONDItems1[0]);
        System.assertEquals('Australia-Hong Kong', testNAONDItems1[1]);
        System.assertEquals('Brisbane -Houston;Adelaide-San Francisco', testContractObj.SQUSONDSelected__c);
        System.assertEquals('Brisbane -Houston', testUSONDItems[0]);
        System.assertEquals('Adelaide-San Francisco', testUSONDItems[1]);
        System.assertEquals('Australia-Ahmedabad;Australia-Bangalore', testContractObj.SQWAAONDSelected1__c);
        System.assertEquals('Australia-Ahmedabad', testWAAONDItems1[0]);
        System.assertEquals('Australia-Bangalore', testWAAONDItems1[1]);
    }
}