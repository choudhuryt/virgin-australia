@isTest
private class TestContractExtensionTerminationTrigger
{
    
     static testMethod void testSetLevel3ManagerContractTermination1()
     {
        User u = TestUtilityClass.createTestUser();     
        insert u;
        
        Profile profileObj = [Select Id, name from profile where name = 'Corporate,Industry and Government Manager'];
        User userObj = [Select Id, name from User where profileId =: profileObj.Id And isActive = True Limit 1];
        //u.contract_approver__c = '00590000000Kyp2';
        u.contract_approver__c = userObj.Id;
        update u;
         
        system.runAs(u){
        Test.startTest();
        Account acc = TestUtilityClass.createTestAccount();
        insert acc;
         
        Contract ct = TestUtilityClass.createTestContract(acc.Id);
        insert ct;
                
        Contract_Termination__c cf = TestUtilityClass.createTestContractTermination(acc.Id, ct.Id);
        insert cf;        
             
        }       
      }

         static testMethod void testSetLevel3ManagerContractExtension1()
     {
        User u = TestUtilityClass.createTestUser();     
        insert u;
        Profile profileObj = [Select Id, name from profile where name = 'Corporate,Industry and Government Manager'];
        User userObj = [Select Id, name from User where profileId =: profileObj.Id And isActive = True Limit 1];
        //u.contract_approver__c = '00590000000Kyp2';
        u.contract_approver__c = userObj.Id;
         
        system.runAs(u){
        Test.startTest();
        Account acc = TestUtilityClass.createTestAccount();
        insert acc;
            
        Contract ct = TestUtilityClass.createTestContract(acc.Id);
        insert ct;
                
        Contract_Extension__c cf = TestUtilityClass.createTestContractExtension(acc.Id, ct.Id);
        insert cf;        
             
        }       
      }
    
    static testMethod void testSetLevel3ManagerContractTermination2()
     {
        User u = TestUtilityClass.createTestUser();     
        insert u;
		Profile profileObj = [Select Id, name from profile where name = 'VISTA Agent'];
        User userObj = [Select Id, name from User where profileId =: profileObj.Id And isActive = True Limit 1];         
        //u.contract_approver__c = '00590000001sIYb';
        u.contract_approver__c = userObj.Id;
        update u;
         
        system.runAs(u){
        Test.startTest();
        Account acc = TestUtilityClass.createTestAccount();
        insert acc;
            
        Contract ct = TestUtilityClass.createTestContract(acc.Id);
        insert ct;
                
        Contract_Termination__c cf = TestUtilityClass.createTestContractTermination(acc.Id, ct.Id);
        insert cf;        
             
        }       
      } 
       static testMethod void testSetLevel3ManagerContractExtension2()
     {
        User u = TestUtilityClass.createTestUser();     
        insert u;
        Profile profileObj = [Select Id, name from profile where name = 'VISTA Agent'];
        User userObj = [Select Id, name from User where profileId =: profileObj.Id And isActive = True Limit 1];         
        //u.contract_approver__c = '00590000001sIYb';
        u.contract_approver__c = userObj.Id;
        update u;
         
        system.runAs(u){
        Test.startTest();
        Account acc = TestUtilityClass.createTestAccount();
        insert acc;
     
            
        Contract ct = TestUtilityClass.createTestContract(acc.Id);
        insert ct;
                
        Contract_Extension__c cf = TestUtilityClass.createTestContractExtension(acc.Id, ct.Id);
        insert cf;        
             
        }       
      }
}