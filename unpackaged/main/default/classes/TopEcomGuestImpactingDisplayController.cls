public with sharing class TopEcomGuestImpactingDisplayController {
	
  //Set&Get 
  public integer TopEcomFlag {get; set;} 
  public List<TopEcomGuestImpacting__c> searchResults {get;set;}
  //public String var1 {get; set;} 

  public PageReference initTopEcom() 
  {
  searchResults= [
                SELECT Impact_Rating_01__c, Impact_Rating_02__c,condition_1__c, CreatedById, CreatedDate,IsDeleted, EcomGuestItem_1__c, 
        		Impact_Rating_03__c,Impact_Rating_04__c,EcomGuestItem_10__c, EcomGuestItem_2__c, EcomGuestItem_3__c, EcomGuestItem_4__c, EcomGuestItem_5__c,
         		Impact_Rating_05__c,Impact_Rating_006__c,EcomGuestItem_6__c, EcomGuestItem_7__c, EcomGuestItem_8__c, EcomGuestItem_9__c, LastModifiedById, 
        		Impact_Rating_07__c,Impact_Rating_08__c,LastModifiedDate, OwnerId, Id, SystemModstamp, Name, exist__c, ImpactRating1__c,ImpactRating2__c,ImpactRating3__c,
        		Impact_Rating_09__c,Impact_Rating_10__c,ImpactRating4__c,ImpactRating5__c,ImpactRating6__c,ImpactRating7__c,ImpactRating8__c,ImpactRating9__c,ImpactRating10__c
				From TopEcomGuestImpacting__c 
            	WHERE   exist__c = True
                   ORDER BY CreatedDate LIMIT 1];
  
                    //
  
				  	//
  
  
  

				if(searchResults.size()>0)
    			 {
     			  TopEcomFlag=1;
      			 }
    
      			return null;
  }
  
  
}