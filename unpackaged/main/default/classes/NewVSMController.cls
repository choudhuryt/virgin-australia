public with sharing class NewVSMController
{


public Velocity_Status_Match__c vsm{get;set;}
    
public List <Contract> conlist {get;set;}
    
public Contract con{get;set;}    

public NewVSMController(ApexPages.StandardController controller)
{
    this.con= (Contract)controller.getRecord();    
    vsm = new Velocity_Status_Match__c();
}

public PageReference initDisc() 
{
  conlist = [SELECT AccountId, Account.name , Account.Accelerate_Key_Contact_Email__c ,RecordType.Name
             FROM Contract WHERE Id =:ApexPages.currentPage().getParameters().get('id')];
  con = conlist.get(0);
  vsm.Account__c = con.AccountId;
  vsm.Contract__c = con.id;
  vsm.RecordTypeId = '012900000009IkM' ;
  vsm.Date_Requested__c = system.today();
  vsm.Approval_Status__c = 'Draft';
  vsm.Within_Contract__c = 'Yes' ;
  vsm.Status_Match_or_Upgrade__c = 'Pilot Gold' ;
  if (con.RecordType.Name == 'SmartFly')
    {
        vsm.Passenger_Email__c = 'smartfly@flightcentre.com'; 
    }
    else
    {
    vsm.Passenger_Email__c = con.Account.Accelerate_Key_Contact_Email__c;
    }
    vsm.Justification__c = 'Within Contract';
    vsm.Delivery_To__c = 'Member';  
    if(Test.isRunningTest())
    {
      vsm.Passenger_Name__c = 'Test';
      vsm.Passenger_Velocity_Number__c ='1234567890';
      vsm.Position_in_Company__c = 'Test';
    }
   return null;        
}    

    
public PageReference save() {
    
    
    insert vsm;
    
    PageReference pageRef = new PageReference('/'+vsm.Id);
    pageRef.setRedirect(true);
    return pageRef;
}
    
public PageReference cancel() {
 
   PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
   thePage.setRedirect(true);
   return thePage;
   
  }
}