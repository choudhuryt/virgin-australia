/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 22.03.2022
**Description: Class to fwtch Access token from KeyCloak
*/
public class GetAccessToken {
    public static string postMethod = System.Label.POST_Method;
    public static string accessEndpoint = System.Label.Access_Token_Endpoint;
    public static string contentType = System.Label.Content_Type;
    public static string accessTokenContentType = System.Label.AccessTokenContentType;
    public static string CLIENT_ID = System.Label.Client_Id;
    public static string CLIENT_SECRET = System.Label.Client_Secret;
    
    public static string getToken(){
        string accessToken;
        
        HttpRequest req = new HttpRequest();
        req.setMethod(postMethod);
        req.setHeader(contentType,accessTokenContentType);
        req.setEndpoint(accessEndpoint);
        req.setBody('grant_type=client_credentials' + '&client_id='+CLIENT_ID + '&client_secret='+CLIENT_SECRET);
        Http http = new Http();
        HTTPResponse response = http.send(req);
        //System.debug('Access Token--'+response.getBody());
        
        if (response.getStatusCode() == 200) {
            JSONParser parser = JSON.createParser(response.getBody()); 
            System.debug('parser--'+parser);
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() =='access_token')){
                    parser.nextToken();
                    accessToken= parser.getText();
                }
            }
        }
        System.debug('accessToken : ' +  accessToken);
        return accessToken;
    }
}