/*
 *	Updated by Cloudwerx : Removed hardcoded RecordTypeIds & UserId references from the code 
 * 
 * 
 */

@isTest
public class TestUpdateSmartflyKeyContact {    
    
    public static testMethod void testSmartflyContactCreate ()
    {
        
        Profile userProfile = [SELECT Id FROM Profile WHERE Name = 'Contract Implementation Support'];
        User userRec = [SELECT Id FROM User WHERE profileId =:userProfile.Id AND IsActive = True LIMIT 1];
        
        Test.startTest();    
        Account acc = new Account();
        CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
        acc = commonObjectsForTest.CreateAccountObject(0);
        acc.RecordTypeid = Utilities.getRecordTypeId('Account', 'SmartFly');
        acc.Sales_Matrix_Owner__c = 'Accelerate';
        acc.OwnerId = Utilities.getOwnerIdByName('Virgin Australia Business Flyer');        
        acc.Business_Number__c ='111';
        acc.Account_Owner__c = Utilities.getOwnerIdByName('Virgin Australia Business Flyer');
        acc.Sales_Support_Group__c = userRec.Id;         
        acc.Billing_Country_Code__c ='AU';
        acc.Market_Segment__c = 'Smartfly'; 
        acc.Total_Domestic_Air_Travel_Expenditure__c =100;
        acc.Lounge__c =true;
        acc.AccountSource = 'Smartfly';      
        insert acc;
        
        Contact newContact2 = new Contact(); 
        newContact2.FirstName ='Andy';
        newContact2.LastName ='C';
        newContact2.AccountId = acc.id;
        newContact2.Email ='testandy123@test.com';
        newContact2.SmartFly_Contact_Status__c = 'Key Contact'; 
        newContact2.SmarftFly_Tourcode__c = 'SMF12345'  ;  
        insert newContact2;    
        
        Contact newContact1 = new Contact(); 
        newContact1.FirstName ='Andy';
        newContact1.LastName ='C';
        newContact1.AccountId = acc.id;
        newContact1.Email ='testandy12344@test.com';
        insert newContact1;
        
        newContact1.SmartFly_Contact_Status__c = 'Key Contact';  
        update newContact1 ;
        
        List<Id> conid = new List<Id>();        
        conid.add(newContact1.Id); 
        
        UpdateSmartflyKeyContact.UpdateSmartflyKeyContact(conid)  ;  
        
        Test.stopTest(); 
        
    }
    
}