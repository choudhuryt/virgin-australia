@isTest
private class CaseSendEmailWithTemplateTest {
    
    @isTest
    public static void test_all() {
        Case testCase = new Case();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
        CaseSendEmailWithTemplate controller = new CaseSendEmailWithTemplate(sc);
        controller.init();
        
        System.assertEquals(true, controller.invalidTemplate);
        
        controller.selectedCase.Email_Template_Required__c = 'test template '+DateTime.now();
        controller.setTemplateAndSendEmail();
        
        System.assertEquals(true, controller.invalidTemplate);
        
        Id testId = '01pN0000000ECbc';
        String sendEmailURL = controller.getSendEmailURL(testId, testId, testId);
        System.assertNotEquals(null, sendEmailURL);
    }
}