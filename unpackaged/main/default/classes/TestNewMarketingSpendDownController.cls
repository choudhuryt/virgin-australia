/**
* @description       : Test class for NewMarketingSpendDownController
* @createdBy         : Cloudwerx
* @Updated By        : Cloudwerx
**/
@isTest
private class TestNewMarketingSpendDownController {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('Account Closed', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        
        Marketing_Spend_Down__c testMarketingSpendDownObj = TestDataFactory.createTestMarketingSpendDown(testAccountObj.Id, testContractObj.Id, 'Entertainment', 'Retail', 'AUD', 20, 'Test Campaign Name', 'Reason For New Version');
        INSERT testMarketingSpendDownObj;
    }
    
    @isTest
    private static void testNewMarketingSpendDownController() {
        Marketing_Spend_Down__c testMarketingSpendDownObj = [SELECT Id, Account__c, Contract__c, Version_Number__c, Global_Sales_Activity__c, 
                                                             Approval_Status__c, Campaign_Detail__c, GST_Inclusive__c, Reason_for_New_Version__c, 
                                                             Amount_to_be_Deducted__c, Currency__c FROM Marketing_Spend_Down__c LIMIT 1];
        Test.startTest();
        PageReference pageRef = Page.NewMarketingSpendDown;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', testMarketingSpendDownObj.Id);
        ApexPages.StandardController conL = new ApexPages.StandardController(testMarketingSpendDownObj);
        NewMarketingSpendDownController newMarketingSpendDownController  = new NewMarketingSpendDownController(conL);
        newMarketingSpendDownController.initDisc();
        PageReference savePageReference = newMarketingSpendDownController.save();
        PageReference cancelPageReference = newMarketingSpendDownController.cancel();
        Test.stopTest();
        // Asserts
        Marketing_Spend_Down__c insertedMarketingSpendDown = [SELECT Id, Account__c, Contract__c, Version_Number__c, Global_Sales_Activity__c, 
                                                              Approval_Status__c, Campaign_Detail__c, GST_Inclusive__c, Reason_for_New_Version__c, 
                                                              Amount_to_be_Deducted__c, Currency__c FROM Marketing_Spend_Down__c 
                                                              WHERE Original_Spend_Down__c =:testMarketingSpendDownObj.Id LIMIT 1];
        // Asserts
        System.assertEquals('/' + insertedMarketingSpendDown.Id, savePageReference.getUrl());
        System.assertEquals(testMarketingSpendDownObj.Account__c, insertedMarketingSpendDown.Account__c);
        System.assertEquals(testMarketingSpendDownObj.Contract__c, insertedMarketingSpendDown.Contract__c);
        System.assertEquals(testMarketingSpendDownObj.Version_Number__c + 1, insertedMarketingSpendDown.Version_Number__c);
        System.assertEquals(testMarketingSpendDownObj.Global_Sales_Activity__c, insertedMarketingSpendDown.Global_Sales_Activity__c);
        System.assertEquals('Draft', insertedMarketingSpendDown.Approval_Status__c);
        System.assertEquals(testMarketingSpendDownObj.Campaign_Detail__c, insertedMarketingSpendDown.Campaign_Detail__c);
        System.assertEquals(testMarketingSpendDownObj.GST_Inclusive__c, insertedMarketingSpendDown.GST_Inclusive__c);
        System.assertEquals(testMarketingSpendDownObj.Reason_for_New_Version__c, insertedMarketingSpendDown.Reason_for_New_Version__c);
        System.assertEquals(testMarketingSpendDownObj.Amount_to_be_Deducted__c, insertedMarketingSpendDown.Amount_to_be_Deducted__c);
        System.assertEquals(testMarketingSpendDownObj.Currency__c, insertedMarketingSpendDown.Currency__c);
        System.assertEquals('/' + testMarketingSpendDownObj.Id, cancelPageReference.getUrl());
    }
}