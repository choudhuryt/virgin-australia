@isTest
private class TestScheduledIATAsInContract {
    
  static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        
	    DateTime currTime = DateTime.now();
        Integer min = currTime.minute();
        Integer hour = currTime.hour();
        String sch;
        
	if(min <= 58)
            sch = '0 '+ (min + 1) + ' ' + hour + ' * * ? '+ currTime.year();
        else          
            sch = '0 0 '+ (hour + 1) + ' * * ? '+ currTime.year();
        
        Test.startTest();
        
    ScheduleIATAsInContract obj = new ScheduleIATAsInContract();  
	IATAsInContract iatasInContract = new IATAsInContract();     
    iatasInContract.emailTo = new String[] {'salesforce.admin@virginaustralia.com'};
    iatasInContract.query =  'SELECT AccountId, StartDate, EndDate, Status, Account.Account_Type__c, Account.RecordTypeId ' + 
				'FROM Contract ' + 
				'Where RecordType.Name like \'%Industry%\' ' +
				'and Status <> \'Deactivated\' ' +
				'and StartDate >= LAST_YEAR';    
	String jobId = system.schedule('test', sch, obj);        
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger where id = :jobId];        
    System.assertEquals(sch, ct.CronExpression);                                      
    database.executeBatch(iatasInContract,1);  
     test.stoptest();
    }   

}