/**
* @description       : Test class for VelocitySectionController
* @Updated By         : Cloudwerx
**/
@isTest
public class TestVelocitySectionController {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;

        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        INSERT testCaseObj;
        
        Velocity__c testVelocityObj = TestDataFactory.createTestVelocity('Test Velocity', testCaseObj.Id, '2100865670', 'Test', 'Test', '1', '12345', 'test@gmail.com', '23', 3);
        INSERT testVelocityObj;
        
        Apex_Callouts__c testApexCalloutObj = TestDataFactory.createTestApexCallouts('VelocityDetails', 'https://services-mssl.virginaustralia.com/service/partner/salesforce/1.0/SalesforceLoyalty', 90000, 'client_mssl_virginaustralia2018_com');
        INSERT testApexCalloutObj;
        
        ALMS_Integration__c testALMSIntegrationObj = TestDataFactory.createTestALMSIntegration('Activate ALMS', false);
        INSERT testALMSIntegrationObj;
    }
    
    @isTest
    private static void testVelocitySectionControllerConstructor() {
        Case testCaseObj = [SELECT Id FROM Case LIMIT 1];	
        Test.startTest();
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "SILVER", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        HTTPCalloutRequestMock fakeResponse = new HTTPCalloutRequestMock(200, 'Ok', body, null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        ApexPages.StandardController sc = new ApexPages.StandardController(testCaseObj);
        VelocitySectionController testObject = new VelocitySectionController(sc);
        Case caseObj = testObject.loadCaseRecord(testCaseObj.Id);
        testObject.reloadVelocitySection();
        Test.stopTest();
        // Asserts
        System.assertEquals(testCaseObj.Id, testObject.caseRecord.Id);
        System.assertEquals(true, caseObj !=null);
    }
    
    @isTest
    private static void testVelocitySectionControllerConstructorWithNoBVelocity() {
        Case testCaseObj = [SELECT Id FROM Case LIMIT 1];	
        DELETE [SELECT Id FROM Velocity__c];
        Test.startTest();
        Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(testCaseObj);
        VelocitySectionController testObject = new VelocitySectionController(sc);
        Case caseObj = testObject.loadCaseRecord(testCaseObj.Id);
        testObject.reloadVelocitySection();
        Test.stopTest();
        // Asserts
        System.assertEquals(testCaseObj.Id, testObject.caseRecord.Id);
        System.assertEquals(true, caseObj !=null);
    }
    
    @isTest
    private static void testVelocitySectionController() {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());
        VelocitySectionController velocitySectionControllerObj = new VelocitySectionController();
        Case caseObj = velocitySectionControllerObj.loadCaseRecord(null);
        Boolean isVelocityBumberLinkedNotLinked = velocitySectionControllerObj.determineToShowVelocityDetailsLink(null);
        velocitySectionControllerObj.reloadVelocitySection();
        Boolean isVelocityBumberLinked = velocitySectionControllerObj.determineToShowVelocityDetailsLink('abc');
        Test.stopTest();
        // Asserts
        System.assertEquals(true, caseObj == null);
        System.assertEquals(true, isVelocityBumberLinked);
        System.assertEquals(false, isVelocityBumberLinkedNotLinked);
    }
}