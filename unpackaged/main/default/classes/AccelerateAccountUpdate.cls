/**
 * @description       : AccelerateAccountUpdate
 * @UpdatedBy : cloudwerx
**/
public class AccelerateAccountUpdate {
    // @Updated By: Cloudwerx Getting the Contract Record type Id  and user Id
    public static Id vaUserId = Utilities.getOwnerId(System.Label.Virgin_Australia_Business_Flyer_User_Id);
    public static Id contractAcceleratePOSRebateRecordTypeId = Utilities.getRecordTypeId('Contract', 'Accelerate_POS_Rebate');
    @InvocableMethod
    public static void ProcessAccelerateAccount(list<Account> acc) {
        Integer ACCELERATE_PILOT_GOLD = 2;  
        Integer wereThereAnyInserErrors = 0;
        Integer howManyrecordsUpdated = 0;
        Contact keyContact = new Contact(); 
        
        try {
            keyContact = [SELECT Id, Key_Contact__c, Email, Subscriptions__c, Name, title, Phone, FirstName, LastName 
                          FROM Contact WHERE AccountId =:acc[0].id LIMIT 1];
            keyContact.Key_Contact__c = true;
            keyContact.OwnerId = vaUserId;
        } catch(Exception contactException){
            VirginsutilitiesClass.sendEmailError(contactException);
        }  
        
        try {
            UPDATE keyContact;
        } catch(Exception e) {
            //VirginsutilitiesClass.sendEmailError(e);
        }
        
        //update Subscription
        try{
            Subscriptions__c tempSub= new Subscriptions__c(); 
            tempSub.Contact_ID__c = keyContact.Id;
            tempSub.Contact_ID__c = tempSub.Contact_ID__c.substring(0, Math.min(tempSub.Contact_ID__c.length(), 15));
            Subscriptions__c subscription = new Subscriptions__c();
            try {
                subscription = (Subscriptions__c) [SELECT Id, Contact_ID__c, Business_News__c, Accelerate_EDM__c 
                                                   FROM Subscriptions__c WHERE Contact_ID__c =:tempSub.Contact_ID__c];
            } catch(Exception se) {
                VirginsutilitiesClass.sendEmailError(se);
            }
            //subscription.Business_News__c =true;
            subscription.Accelerate_EDM__c = true;
            UPDATE subscription;
        } catch(Exception e){
            VirginsutilitiesClass.sendEmailError(e);    
        }      
        
        if(acc != null && acc[0].Lounge__c == true){ 
            if(keyContact.Email <> acc[0].Lounge_Email__c){    
                Contact loungeContact = new Contact();
                loungeContact.FirstName = acc[0].Lounge_First_Name__c ;
                loungeContact.LastName = acc[0].Lounge_Last_Name__c;
                loungeContact.Email = acc[0].Lounge_Email__c;
                loungeContact.AccountId = acc[0].id;
                loungeContact.MailingStreet = acc[0].BillingStreet;
                loungeContact.MailingCity = acc[0].BillingCity;
                loungeContact.MailingPostalCode = acc[0].BillingPostalCode;
                loungeContact.MailingState = acc[0].BillingState;
                loungeContact.OwnerId = vaUserId;
                try {
                    INSERT loungeContact;
                } catch(Exception e){
                    //VirginsutilitiesClass.sendEmailError(e);
                }
            }            
        }
        
        Date d= Date.today();
        //d.toStartOfMonth();
        //Do more stuff here.....
        Contract contract = new Contract();
        contract.AccountId = acc[0]?.id;
        // @Updated By: Cloudwerx Removed hard coded Id 
        contract.RecordTypeId = contractAcceleratePOSRebateRecordTypeId;
        contract.Status = 'Draft';
        contract.Type__c = 'Rebate';
        contract.Contracting_Entity__c = 'Virgin Australia';
        contract.Sales_Basis__c = 'Flown';
        // @Updated By: Cloudwerx Removed hard coded UserId check and getting from custom label
        contract.OwnerId = vaUserId;
        contract.StartDate = d;
        //contract.Ticket_Designator_Out_Count_Out_Payment__c ='';
        contract.Fare_Class_Excluded_for_Payment__c = 'M; S; T; U';
        contract.Carrier_Excluded_For_Payment__c = 'SQ; VX; SA; HA; AB; MI; AZ; VS; HX; AC';
        contract.Carrier_Excluded_For_Calculation__c = 'SQ; VX; SA; HA; AB; MI; AZ; VS; HX; AC';
        
        //pilot gold
        contract.Pilot_Gold__c = ACCELERATE_PILOT_GOLD;
        
        //d.toStartOfMonth();
        try{
            INSERT contract;
        } catch(Exception ex) {
            //VirginsutilitiesClass.sendEmailError(ex);
        }   
        //Add pilot gold if applicable
        
        Contract contractAccelerate = new Contract();
        // @Updated By: Cloudwerx Removed HardCoded Check from Where clause and added recordTypeID that we are getting dynamically
        contractAccelerate = (Contract) [SELECT id, Status, Anniversary_Month__c FROM Contract WHERE Accountid =:acc[0].Id AND  RecordTypeId =:contractAcceleratePOSRebateRecordTypeId LIMIT 1];
        
        SIP_Header__c sipheader = new SIP_Header__c();
        sipheader.Contract__c = contractAccelerate.id;
        sipheader.Description__c = 'Accelerate - All Revenue';
        INSERT sipheader;
        
        // @Updated By: Cloudwerx Code optimisation
        List<SIP_Rebate__c> sipRebateList = new List<SIP_Rebate__c>();
        sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 2, Upper_Percent__c = 2, SIP_Percent__c = 2));
        sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 3, Upper_Percent__c = 3, SIP_Percent__c = 3));
        sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 3.5, Upper_Percent__c = 3.5, SIP_Percent__c = 3.5));
        sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 4, Upper_Percent__c = 4, SIP_Percent__c = 4));
        sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 4.5, Upper_Percent__c = 4.5, SIP_Percent__c = 4.5));
        sipRebateList.add(new SIP_Rebate__c(SIP_Header__c = sipheader.id, Lower_Percent__c = 5, Upper_Percent__c = 5, SIP_Percent__c = 5));
        if(sipRebateList.size() > 0){
            INSERT sipRebateList;
        }
        
        if(acc[0].Pilot_Gold_Email__c != null){
            Velocity_Status_Match__c vsm = new Velocity_Status_Match__c();
            vsm.Account__c = acc[0].id;
            vsm.Approval_Status__c = 'Draft';
            vsm.Date_Requested__c = Date.today();
            vsm.Delivery_To__c = 'Member';
            vsm.Position_in_Company__c = acc[0].Pilot_Gold_Position__c;
            vsm.Passenger_Name__c = acc[0].Pilot_Gold_First_Name__c + ' ' + acc[0].Pilot_Gold_Last_Name__c;
            vsm.Passenger_Velocity_Number__c = acc[0].Pilot_Gold_Velocity_Number__c;
            vsm.Passenger_Email__c = acc[0].Pilot_Gold_Email__c;
            vsm.Within_Contract__c = 'Yes';
            vsm.Contract__c = contract.id;
            vsm.Status_Match_or_Upgrade__c ='Pilot Gold';
            vsm.Justification__c = 'Within Contract';
            try{
                INSERT vsm;
            } catch(Exception e){
                //VirginsutilitiesClass.sendEmailError(e);
            }
        }
        
        if(acc[0].Pilot_Gold_Email_Second_Nominee__c != null){
            Velocity_Status_Match__c vsm1 = new Velocity_Status_Match__c();
            vsm1.Account__c = acc[0].id;
            vsm1.Approval_Status__c = 'Draft';
            vsm1.Date_Requested__c = Date.today();
            vsm1.Delivery_To__c = 'Member';
            vsm1.Position_in_Company__c = acc[0].Pilot_Gold_Position_Second_Nominee__c;
            vsm1.Passenger_Name__c = acc[0].Pilot_Gold_First_Name_Second_Nominee__c  + ' ' + acc[0].Pilot_Gold_last_Name_Second_Nominee__c;
            vsm1.Passenger_Velocity_Number__c = acc[0].Pilot_Gold_Velocity_Gold_Second_Nominee__c;
            vsm1.Passenger_Email__c = acc[0].Pilot_Gold_Email_Second_Nominee__c;
            vsm1.Within_Contract__c = 'Yes';
            vsm1.Contract__c = contract.id;
            vsm1.Justification__c = 'Within Contract';
            vsm1.Status_Match_or_Upgrade__c ='Pilot Gold';
            try{
                INSERT vsm1;
            } catch(Exception e) {
                //VirginsutilitiesClass.sendEmailError(e);
            }
        }   
    }
}