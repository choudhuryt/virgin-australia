/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class TestInternationalDeltaDiscounts_v1 {
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        
        //setup account
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
  	
  			
 		insert account;
 
        Contract contractOld = new Contract();
        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.VA_DL_Requested_Tier__c = '1';
        contractOld.Cos_Version__c = '13.5';
        contractOld.Red_Circle__c =true;
        
        
        insert contractOld;
        
                    International_Discounts_USA_Canada__c deltaDiscount = new International_Discounts_USA_Canada__c();
        			deltaDiscount.Tier__c ='1';
        			deltaDiscount.Is_Template__c =true;
        			deltaDiscount.Cos_Version__c ='13.5';
        			deltaDiscount.Intl_Discount_USA_CAN_Eligible_Fare_Type__c    =    'Business';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType2__c    =    'Business';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType3__c    =    'Business Saver';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType9__c    =	  'Business Saver';
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType14__c	     =	  'Business Tactical';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType4__c    =    'Premuim Economy';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType5__c    =    'Premuim Economy';
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType15__c        =    'Premuim Saver';    
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType6__c    =    'Freedom';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType7__c    =    'Freedom';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareTyp8__c     =    'Freedom';
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType10__c        =    'Freedom';
					deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType11__c        =    'Elevate';
					deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType12__c        =    'Elevate';
					deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType13__c        =    'Elevate';
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType16__c        =    'Elevate';
        
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class__c  =  'J';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class2__c =  'C';            
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class3__c =  'D';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class9__c =  'I';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class14__c = 'I';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class4__c =  'W';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class5__c =  'R';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class15__c = 'O';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class6__c =  'Y';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class7__c =  'B';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class8__c =  'H'; 
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class10__c = 'K';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class11__c = 'L';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class12__c = 'E';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class13__c = 'N';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class16__c = 'V';
        
                    deltaDiscount.Intl_Discount_USA_CAN_off_Published_Fare__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare2__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare3__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare4__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare5__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare6__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare7__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare8__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare9__c =  0;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare10__c =  0;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare11__c=  0;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare12__c=  0;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare13__c=  0;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare14__c=  0;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare15__c=  0;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare15__c=  0;
                    deltaDiscount.International_Discount_USA_CAN_Exists__c = False;
                    deltaDiscount.Applicable_Routes_BNE_SYD_MEL_to__c = 'USA/Canada/Mexico';
                 
                    
                    insert deltaDiscount;
        		    PageReference pref = Page.InternationalDiscountDeltaPage_v1;
        	pref.getParameters().put('id', contractOld.id);
        	Test.setCurrentPage(pref);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(contractOld);
        InternationalDiscountControllerDelta_v1 lController = new InternationalDiscountControllerDelta_v1(conL);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conX = new ApexPages.StandardController(contractOld);
        InternationalDiscountControllerDelta_v1 xController = new InternationalDiscountControllerDelta_v1(conX);
          
          Test.startTest();
        PageReference  ref1 = Page.InternationalDiscountDeltaPage_v1;
        ref1 = lController.initDisc();
        ref1 = lController.newInterUSA();
       
        ref1 = lController.save();
        ref1 = lController.initDisc();
        ref1 = lController.newInterUSA();
        
      	ref1 = lController.qsave();
       
        ref1 = lController.initDisc();
        ref1 = lController.remUSACAN();
        
        
        ref1 = lController.cancel();
        
        contractOld.VA_DL_Requested_Tier__c = '0';
        contractOld.Cos_Version__c = '';
        contractOld.Red_Circle__c =false;
        update contractOld;
        
        ref1 = lController.initDisc();
        ref1 = lController.newInterUSA();
       
        ref1 = lController.save();
        ref1 = lController.initDisc();
        ref1 = lController.newInterUSA();
        ref1 = lController.qsave();
        
        test.stopTest();
        
        
    }
}