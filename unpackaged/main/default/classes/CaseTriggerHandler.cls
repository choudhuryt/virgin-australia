public class CaseTriggerHandler {
    public static Map<String, Schema.RecordTypeInfo> RT_INFO = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName();
    public static Id RT_ACCELERATE = RT_INFO.get('Accelerate').getRecordTypeId();
    public static Id RT_GCCEconomyX = RT_INFO.get('GCC_Economy_X').getRecordTypeId();
    public static Id RT_GCCUpgradeMe = RT_INFO.get('GCC_UpgradeMe').getRecordTypeId();
    public static Id RT_GCCGeneralEnquiries = RT_INFO.get('GCC_General_Enquiries').getRecordTypeId();
    public static Id RT_GCC = RT_INFO.get('GCC').getRecordTypeId();
    public static Id RT_GCCGRCase = RT_INFO.get('GR_Case').getRecordTypeId();
    public static Id RT_GCCSSR = RT_INFO.get('GCC_SSR').getRecordTypeId();
    
    
    public static void AutoCloseSpamCase(List<Case> cases, Map<Id, Case> oldCaseMap) {
        for(Case c: cases) {
            if(RT_ACCELERATE.equals(c.RecordTypeId) && 'SPAM'.equals(c.ACC_Primary_Category__c) && c.ACC_Primary_Category__c != oldCaseMap.get(c.Id).ACC_Primary_Category__c) {
                c.Status = 'Closed';
            }
        }
    }
    
    public static void ReopenCaseCapture(List<Case> caseInTrigger, Map<Id, Case> oldCaseMap) {
        Set<Id> setCaseId = new Set<Id>();
        for (Case caseItr : caseInTrigger) {
            Case oldcase = oldCaseMap.get(caseItr.ID);
            
            system.debug('Old and new values' + caseItr.IsClosed + 'old ' + oldcase.IsClosed);
            
            if (caseItr.Status <> 'Closed' && oldcase.IsClosed) {
                caseItr.Last_Reopened_Date__c = system.today();
                caseItr.Reopened__c = true ;
            }
        }
    }
    
    
    public static void CaseAssignmentCapture(List<Case> caseInTrigger, Map<Id, Case> oldCaseMap) {
        Set<Id> setCaseId = new Set<Id>();
        for (Case caseItr : caseInTrigger) {
            Case oldcase = oldCaseMap.get(caseItr.ID);
            
            if ((caseItr.OwnerId <> oldcase.OwnerId) && (string.valueOf(caseItr.OwnerId).startsWith('005') && (string.valueOf(oldcase.OwnerId).startsWith('00G') || oldcase.OwnerId == '00590000001VJ16'))) {
                caseItr.Date_Assigned_To_Agent__c = system.today();
            }
        }
    }
    //update flight number field with prefix VA
    public static void updateFlightNumber(List<Case> newCaseList, Map<Id, Case> oldCaseMap, boolean isInsert, boolean isUpdate){
        for (Case cases: newCaseList){
            if(cases.RecordTypeId  == RT_GCCGRCase && 
               ((isUpdate && cases.Flight_Number__c != oldCaseMap.get(cases.id).Flight_Number__c && cases.Flight_Number__c <> null) 
                || (isInsert && cases.Flight_Number__c <> null))
              ) {
                  if(!cases.Flight_Number__c.startsWithIgnoreCase('VA')){
                      cases.Flight_Number__c = 'VA'+cases.Flight_Number__c.toUppercase();
                  }
                  else{
                      cases.Flight_Number__c = cases.Flight_Number__c.toUppercase();
                  }                    
              } 
        }
    }
    //update GCC Record Type with GCC General Enquiries-RITM0176588
    public static void updateRecordType(List<Case> newCaseList){
        for (Case cases: newCaseList){
            if(RT_GCCEconomyX.equals(cases.RecordTypeId) || RT_GCCUpgradeMe.equals(cases.RecordTypeId) || RT_GCC.equals(cases.RecordTypeId))
            {
                cases.RecordTypeId = RT_GCCGeneralEnquiries;
            }  
        }                    
    }  
/*------------------------------------------------------------------------
Method Name: attachEmailGCCSSRCase
Author:     Nabeel
Description:   Attach email to SSR (Special Service Request) GCC Web to case https://stage.virginaustralia.io/au/en/forms/special-request-form/. 
Created: 01-02-2022
----------------------------------------------------------------------------*/
    public static void attachEmailGCCSSRCase(List<Case> newTriggerCaseList){
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('GCC SSR').getRecordTypeId();
        Set<Id> CaseIds = new Set<Id>();
        List<Case> newCasesList = new List<Case>();
        for(Case cs: newTriggerCaseList){
            if(cs.RecordTypeId == devRecordTypeId && cs.Origin == 'Web'){
                CaseIds.add(cs.id);
            }
        }
        if(CaseIds.size() > 0){
            newCasesList = [SELECT Id,Virgin_Australia_Booking_Reference__c,Name_Title__c,First_Name__c,Last_Name__c,Contact_Phone__c,Country_Code__c,
                            Email_Address__c,Vision_Impaired_No_Assistance__c,Vision_Impaired_Require_Assistance__c,Vision_Impaired_Service_Dog_in_Cabin__c,
                            Vision_Impaired_Additional_Information__c,Hearing_Impaired_No_Assistance__c,Hearing_Impaired_Require_Assistance__c,
                            Hearing_Impaired_Service_Dog_in_Cabin__c,Hearing_Impaired_Additional_Informatio__c,Can_Use_Stairs_and_Walk_to_Seat__c,
                            Unable_to_Use_Stairs_Can_Walk_to_Seat__c,Immobile_Can_Self_Transfer_to_Seat__c,Immobile_Cannot_Self_Transfer_to_Seat__c,
                            Own_Electric_WC_in_Hold_Dry_Cell__c,Own_Electric_WC_in_Hold_Lithium__c,Upper_Torso_Harness_Required__c,
                            Wheelchair_Required_Onboard__c,Dimensions_of_Mobility_Aid__c,Additional_Information_on_Assistance_Req__c,
                            Immobile_and_Travelling_with_Carer__c,Immobile_and_Providing_Assistance_Person__c,Travelling_with_Carer_Other__c,
                            Providing_Assistance_Person_other__c,Additional_Information_about_Assistance__c,Departure_Assistance_Person_Title__c,
                            Departure_Assistance_Person_First_Name__c, Departure_Assistance_Person_Last_Name__c,Departure_Assistance_Person_Country_Code__c,
                            Departure_Assistance_Person_Phone_Number__c,Arrival_Assistance_Person_Title__c,Arrival_Assistance_Person_First_Name__c,
                            Arrival_Assistance_Person_Last_Name__c, Arrival_Assistance_Person_Country_Code__c,Arrival_Assistance_Person_Phone_Number__c,
                            Carer_Same_virgin_Booking__c,Carer_Virgin_Booking_Reference__c,Carer_Title__c,Carer_First_Name__c,Carer_Last_Name__c,
                            Carer_Country_Code__c,Carer_Phone_Number__c,Medical_Condition_May_require_Clearance__c,Additional_Information_on_Condition__c,
                            Hidden_Disability_Details__c, CPAP_machine__c,Personal_Portable_Oxygen_Concentrator__c,Supplementary_Oxygen_Bottle__c,
                            Other_Powered_Medical_Equipment__c,Additional_Medical_Equipment_Information__c,Other_Assistance_Additional_Information__c
                            FROM Case WHERE Id IN: CaseIds];
        }
        List<EmailMessage> emails = new List<EmailMessage>();
        if(newCasesList.size()>0){
            String tableFirstCol ='</td></tr><tr><td colspan="1" rowspan="1" style="text-align: left; width: 15%;">';
            String tableSecondCol = '</td><td colspan="1" rowspan="1" style="text-align: left; width: 35%;">';
            for(Case cases: newCasesList){
                String htmlBody ='<table align="left" border="1" cellpadding="1" cellspacing="1" style="width: 100%;"><tbody><tr><td colspan="4" rowspan="1" style="background-color: rgb(153, 153, 153);">'
                    + '<p style="text-align: center;">'
                    +'<span style="font-size: 18px;">'
                    +'<strong>Special Service Request Form</strong>'
                    +'</span>'
                    +'</p>'
                    +'</td></tr><tr><td colspan="4" rowspan="1" style="text-align: left; width: 15%; background-color: rgb(204, 204, 204);">'
                    +'<span style="font-size: 14px;"><strong>Passenger Information</strong></span>';
                
                htmlBody += tableFirstCol;
                htmlBody +='<span style="font-size: 12px;"><strong>Virgin Australia Booking Reference</strong></span>';
                htmlBody += tableSecondCol;
                htmlBody += cases.Virgin_Australia_Booking_Reference__c == null ? '': cases.Virgin_Australia_Booking_Reference__c;
                htmlBody += tableFirstCol;
                htmlBody +='<span style="font-size: 12px;"><strong>Title</strong></span>';
                htmlBody += tableSecondCol;
                htmlBody += cases.Name_Title__c == null ? '': cases.Name_Title__c;
                htmlBody += tableFirstCol;
                htmlBody +='<span style="font-size: 12px;"><strong>First Name</strong></span>';
                htmlBody += tableSecondCol;
                htmlBody += cases.First_Name__c == null ? '': cases.First_Name__c;
                htmlBody += tableFirstCol;
                htmlBody +='<span style="font-size: 12px;"><strong>Last Name</strong></span>';
                htmlBody += tableSecondCol;
                htmlBody += cases.Last_Name__c == null ? '': cases.Last_Name__c;
                htmlBody += tableFirstCol;
                htmlBody +='<span style="font-size: 12px;"><strong>Phone Number</strong></span>';
                htmlBody += tableSecondCol;
                htmlBody += cases.Contact_Phone__c == null ? '': cases.Contact_Phone__c;
                htmlBody += tableFirstCol;
                htmlBody +='<span style="font-size: 12px;"><strong>Country Code, Area Code, Number</strong></span>';
                htmlBody += tableSecondCol;
                htmlBody += cases.Country_Code__c == null ? '': cases.Country_Code__c;
                htmlBody += tableFirstCol;
                htmlBody +='<span style="font-size: 12px;"><strong>Email Address</strong></span>';
                htmlBody += tableSecondCol;
                htmlBody += cases.Email_Address__c == null ? '': cases.Email_Address__c;
                
                // Vision impairment details
                if(cases.Vision_Impaired_No_Assistance__c == true || cases.Vision_Impaired_Require_Assistance__c == true || cases.Vision_Impaired_Service_Dog_in_Cabin__c == true
                   || cases.Vision_Impaired_Additional_Information__c != null){
                       htmlBody +='</td></tr><tr><td colspan="2" rowspan="1" style="text-align: left; width: 15%; background-color: rgb(204, 204, 204);">'
                           + '<span style="font-size: 14px;"><strong> Vision impairment details </strong></span>';
                       htmlBody += tableFirstCol;
                       htmlBody += 'I am vision impaired but do not require assistance';
                       htmlBody += tableSecondCol;
                       htmlBody +=  cases.Vision_Impaired_No_Assistance__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'I am vision impaired and require assistance';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Vision_Impaired_Require_Assistance__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'I am travelling with a service dog in the cabin';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Vision_Impaired_Service_Dog_in_Cabin__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Additional Information';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Vision_Impaired_Additional_Information__c == null ? '': cases.Vision_Impaired_Additional_Information__c;
                   }
                // Hearing Impairment Details
                if(cases.Hearing_Impaired_No_Assistance__c == true || cases.Hearing_Impaired_Require_Assistance__c == true || cases.Hearing_Impaired_Service_Dog_in_Cabin__c == true || cases.Hearing_Impaired_Additional_Informatio__c != null){
                       htmlBody +='</td></tr><tr><td colspan="2" rowspan="1" style="text-align: left; width: 15%; background-color: rgb(204, 204, 204);">'
                           + '<span style="font-size: 14px;"><strong> Hearing Impairment Details </strong></span>';
                       htmlBody += tableFirstCol;
                       htmlBody += 'I am hearing impaired but do not require assistance';
                       htmlBody += tableSecondCol;
                       htmlBody +=  cases.Hearing_Impaired_No_Assistance__c == true ? 'Yes': 'NO'; 
                       htmlBody += tableFirstCol;
                       htmlBody += 'I am hearing impaired and require assistance';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Hearing_Impaired_Require_Assistance__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'I am travelling with a service dog in the cabin';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Hearing_Impaired_Service_Dog_in_Cabin__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Additional Information';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Hearing_Impaired_Additional_Informatio__c == null ? '': cases.Hearing_Impaired_Additional_Informatio__c;
                   }
                // Wheelchair Assistance
                if(cases.Can_Use_Stairs_and_Walk_to_Seat__c == true || cases.Unable_to_Use_Stairs_Can_Walk_to_Seat__c == true || cases.Immobile_Can_Self_Transfer_to_Seat__c == true 
                   || cases.Immobile_Cannot_Self_Transfer_to_Seat__c == true || cases.Own_Electric_WC_in_Hold_Dry_Cell__c == true || cases.Own_Electric_WC_in_Hold_Lithium__c == true 
                   || cases.Upper_Torso_Harness_Required__c == true || cases.Wheelchair_Required_Onboard__c == true || cases.Dimensions_of_Mobility_Aid__c != null 
                   || cases.Additional_Information_on_Assistance_Req__c != null){
                       htmlBody +='</td></tr><tr><td colspan="2" rowspan="1" style="text-align: left; width: 15%; background-color: rgb(204, 204, 204);">'
                           + '<span style="font-size: 14px;"><strong> Mobility Impairment Information </strong></span>';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Wheelchair requested as unable to walk long distance but can use stairs and walk to seat';
                       htmlBody += tableSecondCol;
                       htmlBody +=  cases.Can_Use_Stairs_and_Walk_to_Seat__c == true ? 'Yes': 'NO'; 
                       htmlBody += tableFirstCol;
                       htmlBody += 'Wheelchair requested as unable to walk long distance or use stairs but can walk to seat';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Unable_to_Use_Stairs_Can_Walk_to_Seat__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Wheelchair requested as immobile but can self-transfer to seat';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Immobile_Can_Self_Transfer_to_Seat__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Wheelchair requested as immobile and cannot self-transfer to seat';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Immobile_Cannot_Self_Transfer_to_Seat__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Travelling with own electric wheelchair in hold powered by a dry cell battery';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Own_Electric_WC_in_Hold_Dry_Cell__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Travelling with own electric wheelchair in hold powered by a lithium battery';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Own_Electric_WC_in_Hold_Lithium__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Upper torso harness also required';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Upper_Torso_Harness_Required__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Wheelchair is required onboard';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Wheelchair_Required_Onboard__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Dimensions of mobility aid';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Dimensions_of_Mobility_Aid__c == null ? '': cases.Dimensions_of_Mobility_Aid__c;
                       htmlBody += tableFirstCol;
                       htmlBody += 'Additional Information';
                       htmlBody += tableSecondCol; 
                       htmlBody += cases.Additional_Information_on_Assistance_Req__c == null ? '': cases.Additional_Information_on_Assistance_Req__c;
                   }
                
                //  Travelling with a carer or providing an assistance person
                if(cases.Immobile_and_Travelling_with_Carer__c == true || cases.Immobile_and_Providing_Assistance_Person__c == true || cases.Travelling_with_Carer_Other__c == true 
                   || cases.Providing_Assistance_Person_other__c == true || cases.Additional_Information_about_Assistance__c != null){
                       htmlBody +='</td></tr><tr><td colspan="2" rowspan="1" style="text-align: left; width: 15%; background-color: rgb(204, 204, 204);">'
                           + '<span style="font-size: 14px;"><strong> Travelling with a carer or providing an assistance person </strong></span>';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Immobile and travelling with a carer';
                       htmlBody += tableSecondCol;
                       htmlBody +=  cases.Immobile_and_Travelling_with_Carer__c == true ? 'Yes': 'NO'; 
                       htmlBody += tableFirstCol;
                       htmlBody += 'Immobile and providing an assistance person';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Immobile_and_Providing_Assistance_Person__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Travelling with a carer for other reasons';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Travelling_with_Carer_Other__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Providing an assistance person for other reasons';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Providing_Assistance_Person_other__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Additional Information';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Additional_Information_about_Assistance__c == null ? '': cases.Additional_Information_about_Assistance__c;
                       //Carer details
                       if(cases.Immobile_and_Travelling_with_Carer__c == true || cases.Travelling_with_Carer_Other__c == true){
                           htmlBody +='</td></tr><tr><td colspan="2" rowspan="1" style="text-align: left; width: 15%; background-color: rgb(204, 204, 204);">'
                               + '<span style="font-size: 14px;"><strong> Carer details </strong></span>';
                           htmlBody += tableFirstCol;
                           htmlBody += 'Booking reference number';
                           htmlBody += tableSecondCol;
                           htmlBody +=  cases.Carer_Same_virgin_Booking__c == true ? 'Yes': 'NO';
                           htmlBody += tableFirstCol;
                           htmlBody += 'Booking reference number';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Carer_Virgin_Booking_Reference__c == null ? '': cases.Carer_Virgin_Booking_Reference__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'Title';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Carer_Title__c == null ? '': cases.Carer_Title__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'First Name';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Carer_First_Name__c == null ? '': cases.Carer_First_Name__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'Last Name';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Carer_Last_Name__c == null ? '': cases.Carer_Last_Name__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'Country Code';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Carer_Country_Code__c == null ? '': cases.Carer_Country_Code__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'Phone Number';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Carer_Phone_Number__c == null ? '': cases.Carer_Phone_Number__c;
                           
                       }
                       // Assistance Person Details
                       if(cases.Immobile_and_Providing_Assistance_Person__c == true || cases.Providing_Assistance_Person_other__c == true){
                           htmlBody +='</td></tr><tr><td colspan="2" rowspan="1" style="text-align: left; width: 15%; background-color: rgb(204, 204, 204);">'
                               + '<span style="font-size: 14px;"><strong> Assistance Person Details </strong></span>'
                               
                               //Departure assistance person
                               + '</td></tr><tr><th colspan="2" rowspan="1" style="text-align: left;">Departure Assistance Person</th></tr>';
                           htmlBody += tableFirstCol;
                           htmlBody += 'Title';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Departure_Assistance_Person_Title__c == null ? '': cases.Departure_Assistance_Person_Title__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'First Name';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Departure_Assistance_Person_First_Name__c == null ? '': cases.Departure_Assistance_Person_First_Name__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'Last Name';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Departure_Assistance_Person_Last_Name__c == null ? '': cases.Departure_Assistance_Person_Last_Name__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'Country Code';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Departure_Assistance_Person_Country_Code__c == null ? '': cases.Departure_Assistance_Person_Country_Code__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'Phone Number';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Departure_Assistance_Person_Phone_Number__c == null ? '': cases.Departure_Assistance_Person_Phone_Number__c;
                           
                           // Arrival assistance person
                           htmlBody += '</td></tr><tr><th colspan="2" rowspan="1" style="text-align: left;">Arrival Assistance Person</th></tr>';
                           htmlBody += tableFirstCol;
                           htmlBody += 'Title';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Arrival_Assistance_Person_Title__c == null ? '': cases.Arrival_Assistance_Person_Title__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'First Name';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Arrival_Assistance_Person_First_Name__c == null ? '': cases.Arrival_Assistance_Person_First_Name__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'Last Name';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Arrival_Assistance_Person_Last_Name__c == null ? '': cases.Arrival_Assistance_Person_Last_Name__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'Country Code';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Arrival_Assistance_Person_Country_Code__c == null ? '': cases.Arrival_Assistance_Person_Country_Code__c;
                           htmlBody += tableFirstCol;
                           htmlBody += 'Phone Number';
                           htmlBody += tableSecondCol;
                           htmlBody += cases.Arrival_Assistance_Person_Phone_Number__c == null ? '': cases.Arrival_Assistance_Person_Phone_Number__c;                               
                       }
                   }
                
                // Medical Condition Information
                if(cases.Medical_Condition_May_require_Clearance__c == true || cases.Additional_Information_on_Condition__c != null ){
                    htmlBody +='</td></tr><tr><td colspan="2" rowspan="1" style="text-align: left; width: 15%; background-color: rgb(204, 204, 204);">'
                        + '<span style="font-size: 14px;"><strong> Medical condition that may require medical clearance </strong></span>';
                    htmlBody += tableFirstCol;
                    htmlBody += 'Travelling with a medical condition that may require medical clearance';
                    htmlBody += tableSecondCol;
                    htmlBody +=  cases.Medical_Condition_May_require_Clearance__c == true ? 'Yes': 'NO'; 
                    htmlBody += tableFirstCol;
                    htmlBody += 'Additional Information on Condition';
                    htmlBody += tableSecondCol;
                    htmlBody += cases.Additional_Information_on_Condition__c == null ? '': cases.Additional_Information_on_Condition__c;
                }
                // Hidden disability - details
                if(cases.Hidden_Disability_Details__c != null ){
                    htmlBody +='</td></tr><tr><td colspan="2" rowspan="1" style="text-align: left; width: 15%; background-color: rgb(204, 204, 204);">'
                        + '<span style="font-size: 14px;"><strong> Hidden disability - details </strong></span>';
                    htmlBody += tableFirstCol;
                    htmlBody += 'Additional Information about assistance required';
                    htmlBody += tableSecondCol;
                    htmlBody += cases.Hidden_Disability_Details__c == null ? '': cases.Hidden_Disability_Details__c;
                }
                
                //Travelling with powered medical equipment for use on board
                if(cases.CPAP_machine__c == true || cases.Personal_Portable_Oxygen_Concentrator__c == true || cases.Supplementary_Oxygen_Bottle__c == true
                   || cases.Other_Powered_Medical_Equipment__c == true || cases.Additional_Medical_Equipment_Information__c != null){
                       htmlBody +='</td></tr><tr><td colspan="2" rowspan="1" style="text-align: left; width: 15%; background-color: rgb(204, 204, 204);">'
                           + '<span style="font-size: 14px;"><strong> Travelling with powered medical equipment for use on board </strong></span>';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Travelling with a CPAP machine for use on board';
                       htmlBody += tableSecondCol;
                       htmlBody +=  cases.CPAP_machine__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Travelling with a personal portable oxygen concentrator for use onboard';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Personal_Portable_Oxygen_Concentrator__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Travelling with supplementary oxygen bottle';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Supplementary_Oxygen_Bottle__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Travelling with other powered medical equipment for use onboard';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Other_Powered_Medical_Equipment__c == true ? 'Yes': 'NO';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Additional Information';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Additional_Medical_Equipment_Information__c == null ? '': cases.Additional_Medical_Equipment_Information__c;
                   }
                
                //Other assistance additional information
                 if(cases.Other_Assistance_Additional_Information__c != null){
                       htmlBody +='</td></tr><tr><td colspan="2" rowspan="1" style="text-align: left; width: 15%; background-color: rgb(204, 204, 204);">'
                           + '<span style="font-size: 14px;"><strong> Other assistance additional information </strong></span>';
                       htmlBody += tableFirstCol;
                       htmlBody += 'Additional Information';
                       htmlBody += tableSecondCol;
                       htmlBody += cases.Other_Assistance_Additional_Information__c == null ? '': cases.Other_Assistance_Additional_Information__c;
                   }
                htmlBody +='</td></tr></tbody></table>';
                
                EmailMessage emailMsgObj = new EmailMessage();
                emailMsgObj.HtmlBody =  htmlBody;
                //emailMsgObj.TextBody = email.plainTextBody;
                emailMsgObj.FromAddress = 'noreply@virginaustralia.com';                
                emailMsgObj.FromName = cases.First_Name__c+''+cases.Last_Name__c;
                emailMsgObj.Subject = 'SSR'+' '+'-'+' '+cases.Name_Title__c +' '+cases.First_Name__c+' '+cases.Last_Name__c;
                emailMsgObj.Incoming = true;
                emailMsgObj.ParentId = cases.Id;
                
                emails.add(emailMsgObj);
            }
            insert emails;
        }
    }
}