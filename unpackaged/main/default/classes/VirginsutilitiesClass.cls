/** * File Name      : VirginsUtilitiesClass
 * Description        : This is a utilities class that is taking commonly used methods 
					   and turning them into either instance methods or static methods 
					   depending on there function and what they do 
 * * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
 * * @author          : Andy Burgess
 * * Date             : 30th May 2012
 * * Technical Task ID: Internal requirment
 * * Notes            : Please add in the modifications log any new methods added or any 
                       changes made. Original Author is Andy Burgess.
 * Modification Log ==================================================​============= 
Ver  Date       Author        Modification --- ---- ------ -------------
1.01 16/10/2012 Andy Burgess  Added dynamic query call for called get Data
 */
public  class VirginsutilitiesClass {


	/**
	 * Returns true if an email has been successfully sent, or false if it has an error. 
	 * <p>
	 * This method always returns immediately, whether or not it is true or false 
	 *
	 * @param  toemailAddress -  This is the "to" email address of the email being sent for the SingleMessage object this needs to be a string array but the param passed is a  string and then converted.
	 * @param  subject - This is the subject line of the email.
	 * @param  body This is the body of the email
	 * @return boolean (true or false) 
	 * 
	 */

	public static void sendEmailError(Exception e){

		try{

			Messaging.SingleEmailMessage mailErrorvsm = new Messaging.SingleEmailMessage();
			mailErrorvsm.setToAddresses(new String[] {'andrew.burgess@virginaustralia.com','salesforce.admin@virginaustralia.com'});
			mailErrorvsm.setReplyTo('andrew.burgess@VirginAustralia.com');
			mailErrorvsm.setSenderDisplayName('Accelerate Lead Error');
			mailErrorvsm.setSubject('Accelerate Web to Lead has an error');
			mailErrorvsm.setPlainTextBody('Accelerate Web to Lead has an error, The exception is ' + e);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailErrorvsm });
		}catch(Exception contactException){

		}

	}	


	public static Boolean EmailMethod(String toemailAddress,String subject,String body) {

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();	
		if (toemailAddress <> ''){	


			mail.setToAddresses(new String[] {toemailAddress});
			mail.setSubject(subject);
			mail.setPlainTextBody(body);

			try{
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				return true;
			}
			catch (Exception e){
				return false;
			}
		}	
		return false;
	}

	/**
	 * Returns true if an email has been successfully sent, or false if it has an error. 
	 * <p>
	 * This method always returns immediately, the list will either be 0 or more 
	 *
	 * @param  theQuery -  This is the query string that is built from the desired object ie(Select id from account) 
	 *                     this is so that you can return all fields from the calling methid without having to explicitly name them in the query
	 *                     this is a SOQL queries way of doing a select * from query
	 * @return             A List of Sobjects which needs to be cast to the required object  
	 * 
	 */

	public static List<Sobject> getData(String theQuery){

		List<Sobject> s = new List<Sobject>();
		s = Database.query(theQuery);

		return s;
	}


}