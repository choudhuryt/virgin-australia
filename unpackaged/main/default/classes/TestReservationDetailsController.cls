@isTest
public class TestReservationDetailsController{
   static testMethod void ReservationDetailsTestMethod() {
       ReservationDetailsController tmpcontroller = new ReservationDetailsController();
       //Test.setMock(WebServiceMock.class, new ReservationServiceMockImpl2());
       Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());
       
       Case tmp = new Case(Velocity_Number__c='');
       insert tmp;
       
       Reservation__c resTmp = new Reservation__c(Case__c = tmp.Id,
                                                  Reservation_Number__c='',
                                                  Reservation_Date__c=Date.today(),
                                                  Origin__c='SYS',
                                                  Destination__c='MEL');
       insert resTmp;
       
       
       Test.startTest(); 
       //InitReservationInput
       PageReference pr = new PageReference('/apex/ReservationDetails?id='+tmp.Id);
       Test.setCurrentPage(pr);
       tmpcontroller = new ReservationDetailsController();
       tmpcontroller.caseID = tmp.id;
       
       tmpcontroller.InitReservationInput();
       tmpcontroller.ReservationProperties.DepartureDate = System.today().format();
       tmpcontroller.ReservationProperties.ArrivalDate = System.today().format();
           
       tmpcontroller.SearchReservation();
       Test.stopTest(); 
       
    }
    
   static testMethod void nullDepartureDateTestMethod() {
       ReservationDetailsController tmpcontroller = new ReservationDetailsController();
       //Test.setMock(WebServiceMock.class, new ReservationServiceMockImpl2());
       Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());
       
       Case tmp = new Case(Velocity_Number__c='');
       insert tmp;
       
       Reservation__c resTmp = new Reservation__c(Case__c = tmp.Id,
                                                  Reservation_Number__c='',
                                                  Reservation_Date__c=Date.today(),
                                                  Origin__c='SYS',
                                                  Destination__c='MEL');
       insert resTmp;
       
       
       Test.startTest(); 
       //InitReservationInput
       PageReference pr = new PageReference('/apex/ReservationDetails?id='+tmp.Id);
       Test.setCurrentPage(pr);
       tmpcontroller = new ReservationDetailsController();
       tmpcontroller.caseID = tmp.id;
       
       tmpcontroller.InitReservationInput();
       tmpcontroller.ReservationProperties.DepartureDate = '';
       tmpcontroller.ReservationProperties.ArrivalDate = '';
       tmpcontroller.ReservationProperties.LastName = '';
      
       
       tmpcontroller.SearchReservation();
       Test.stopTest(); 
       
    }
    
}