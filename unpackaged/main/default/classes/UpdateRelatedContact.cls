/**
* @description       : UpdateRelatedContact
* @UpdatedBy         : CloudWerx code refactoring
**/
public class UpdateRelatedContact {
    
    @InvocableMethod
    public static void UpdateRelatedSalesContact(List<Id> CaseIds) {
        List<Case> lstCaseUpdate = new List<Case>();         
        List<Contact> conList = new List<Contact>();   
        List<Case> caselist =  [SELECT Id , SuppliedEmail,ContactId FROM Case 
                                WHERE id IN :CaseIds AND ContactId = NULL];
        Set<String> suppliedEmails = new Set<String>();
        for(Case caseObj :caselist) {
            if (String.isNotBlank(caseObj.SuppliedEmail)) {
                suppliedEmails.add(caseObj.SuppliedEmail);   
            }
        }
        //@updatedby : cloudwerx
        Map<String, Id> contactsMap = new Map<String, Id>();
        if(suppliedEmails.size() > 0) { 
            // @Updated By: Cloudwerx removed hard coded Id and using dynamic Id in WHERE Clause
            Id contactRecordTypeId = Utilities.getRecordTypeId('Contact', 'Contact');
            for(Contact conctObj :[SELECT Id, Account_ID__c, Email, Status__c FROM Contact  WHERE Email IN:suppliedEmails AND Status__c = 'Active' AND RecordTypeId =:contactRecordTypeId]) {
                contactsMap.put(conctObj.Email, conctObj.Id);
            }   
        }
        for(Case caseObj: caselist) {
            if(contactsMap !=null && contactsMap.containsKey(caseObj.SuppliedEmail)) {  
                caseObj.ContactId = contactsMap.get(caseObj.SuppliedEmail);
                lstCaseUpdate.add(caseObj);  
            }
        } 
        UPDATE lstCaseUpdate; 
    }
}