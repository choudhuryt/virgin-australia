@isTest
public class TestScheduleContractSpendCalculation
{
    static testMethod void myUnitTest()
    {
        // TO DO: implement unit test
         DateTime currTime = DateTime.now();
         Integer min = currTime.minute();
         Integer hour = currTime.hour();
         String sch;
        
	if(min <= 58)
            sch = '0 '+ (min + 1) + ' ' + hour + ' * * ? '+ currTime.year();
        else          
            sch = '0 0 '+ (hour + 1) + ' * * ? '+ currTime.year();
        
    Test.startTest();
        
	ScheduleContractSpendCalculation obj = new ScheduleContractSpendCalculation();  
	ContractSpendCalculation newContractSpendBatch = new ContractSpendCalculation();          
	String jobId = system.schedule('test', sch, obj); 
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger where id = :jobId];        
    System.assertEquals(sch, ct.CronExpression);                                      
    database.executeBatch(newContractSpendBatch);  
    test.stoptest();
    }

}