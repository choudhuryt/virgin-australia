/**
* @description       : Test class for UpdateRelatedContact
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestRelatedContact {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        testCaseObj.SuppliedEmail = 'suppliedEmail@org.co';
        INSERT testCaseObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'suppliedEmail@org.co', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        testContactObj.RecordTypeId = Utilities.getRecordTypeId('Contact', 'Contact');
        INSERT testContactObj;
    }
    
    @isTest
    private static void testUpdateRelatedContact() {
        Id caseId = [SELECT Id FROM Case LIMIT 1]?.Id;
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.startTest();
        UpdateRelatedContact.UpdateRelatedSalesContact(new List<Id>{caseId});
        Test.stopTest();
        //Asserts
        Case updatedCaseObj = [SELECT Id, ContactId FROM Case WHERE Id =:caseId];
        system.assertEquals(contactId, updatedCaseObj.ContactId);
    }
}