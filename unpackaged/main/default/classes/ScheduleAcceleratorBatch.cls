/** * File Name      : ScheduleAcceleratorBatch implements Schedulable
* Description        : This Apex Class is so that the Accelerate batch class can run
					   new one updates the expired record to invalid status.
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Andy Burgess
* * Date             : 16th May 2012
* * Technical Task ID: 126
* * Notes            : Schedule Classes must implement Schedulable
* Modification Log ==================================================​============= 
Ver Date Author Modification --- ---- ------ -------------
* */ 

global class ScheduleAcceleratorBatch implements Schedulable{

	global void execute(SchedulableContext sc) {
	
	AccelerateContractBatch accelerateContractsBatch = new AccelerateContractBatch();
    Database.executeBatch(accelerateContractsBatch);
	   
	} 


}