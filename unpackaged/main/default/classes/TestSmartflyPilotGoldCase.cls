@isTest
private class  TestSmartflyPilotGoldCase
{
    static testMethod void myUnitTest()
    {
    
    Test.StartTest();	
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrP';
    account.Sales_Matrix_Owner__c = 'Accelerate';
	account.OwnerId = '00590000000LbNz'; 
    account.Business_Number__c = '11111111111';
    insert account;
     
    Case c = new Case() ;
    c.SuppliedEmail='jdoe_test_test@doe.com';
    c.SuppliedName='John Doe';
    c.Velocity_Member__c = false;
    c.Subject='Pilot Gold Request | ABN: 11111111111';
    c.Contact_First_Name__c='Tom';
    c.Contact_Last_name__c = 'Johns';
    insert c;
        
    List<Id> caseid = new List<Id>();        
    caseid.add(c.Id); 
        
    SmartFlyPilotGoldCase.SmartflyPilotGoldLink(caseid)  ;  
        
    Test.stopTest(); 
        
    }

}