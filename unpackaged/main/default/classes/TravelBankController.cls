/*
* Custom controller for VF page TravelBank
*/
public class TravelBankController {
    public Customer_Compensation__c cusCompObj{get;set;}
    private TravelBank.SalesforceTravelBankService travelBankService {get;set;}
    public String resultMessage {get; set;}
    public list<String> lineIds {get; set;}

    public TravelBankController() {
        String compId = ApexPages.currentPage().getParameters().get('compId');
        cusCompObj = [select Id, Name, Parent_Case__c, Status__c, Parent_Case__r.Velocity_Number__c
                      from Customer_Compensation__c where Id = :compId];
		
        // Get selected Compensation Line Ids
        String lineIdString = ApexPages.currentPage().getParameters().get('lineIds');
        lineIds = lineIdString.split(',');
        
        travelBankService = new TravelBank.SalesforceTravelBankService();
        // Retrieve Apex Callout options from Custom Settings
        Apex_Callouts__c apexCallouts = Apex_Callouts__c.getValues('TravelBankDetails');
        if (apexCallouts != null) {
            travelBankService.endpoint_x = apexCallouts.Endpoint_URL__c;
            travelBankService.timeout_x = Integer.valueOf(apexCallouts.Timeout_ms__c);
            travelBankService.clientCertName_x = apexCallouts.Certificate__c;
        }
    }

    /*
        This function is called when VF page TravelBank is loaded.
        It call webservice to request update for payment status.
    */
    public void updateTravelBankAccount() {
        // only proceed if Customer_Compensation__c status is Approved
        if(cusCompObj.Status__c == 'Approved') {
            resultMessage = '';
            
            // get Velocity_Number__c in parent Case__c
            String vNum = cusCompObj.Parent_Case__r.Velocity_Number__c;
            // get latest Velocity__c which has Velocity_Number__c match with Velocity_Number__c in Case__c
            List<Velocity__c> velocityList = [select Id, Velocity_Number__c, Travel_Bank_Number__c
                                              from Velocity__c
                                              where Case__c = :cusCompObj.Parent_Case__c 
                                              and Velocity_Number__c != null
                                              and Travel_Bank_Number__c != null
                                              order by LastModifiedDate DESC];
            
            if (velocityList.size()>0) {
                Velocity__c velocity = velocityList.get(0);
                // get all children Compensation_Line__c
                List<Compensation_Line__c> lineList= [select Id, Name, Type__c, Category__c, Status__c, 
                                                      Notes__c, Currency_Ea__c, Units__c, Credit_Code__c
                                                      from Compensation_Line__c
                                                      where Parent_Customer_Compensation__c = :cusCompObj.Id
                                                      and Id in :lineIds];
                
                for (Compensation_Line__c line:lineList){
                    try {
                        // proceed if the Line has type=Credit, category=Travel Bank, status=Unpaid
                        if (line.Type__c=='Credit' && line.Category__c=='Travel Bank'){
                            if (line.Status__c == 'Unpaid'){
                                TravelBankModel.UpdateTravelBankAccountBalanceRSType response =
                                    travelBankService.UpdateTravelBankAccountBalance(
                                        velocity.Velocity_Number__c,
                                        velocity.Travel_Bank_Number__c+'', 		// convert this param to String
                                        line.Currency_Ea__c, line.Units__c,
                                        line.Credit_Code__c.subString(0,4));	// code is the first 4 digits of picklist string
                                
                                
                                // sucess payment has reponse status=000000
                                if (response.Status == '000000'){
                                    line.Status__c = 'Paid';
                                    line.Notes__c = 'Payment Number: ' + response.PaymentReference;
                                    
                                    resultMessage = 'Payment ('+line.Name+') has been completed';
                                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, resultMessage));
                                } else {
                                    resultMessage = 'Payment ('+line.Name+') failed';
                                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, resultMessage));
                                }
                            } else {
                                resultMessage = 'Compensation Line ('+line.Name+') already paid, please select and unpaid compensation line';
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, resultMessage));
                            }
                        } else {
                            resultMessage = 'Compensation Line ('+line.Name+') is not in Travel Bank category';
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, resultMessage));
                        }
                    } catch (Exception ex) {
                        ApexPages.addMessages(ex);
                    }
                }
                update lineList;
            } else {
                resultMessage = 'Velocity may not have velocity number or travel bank number. Check search results of case velocity.';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, resultMessage));
            }
        } else {
            resultMessage = 'Compensation ('+cusCompObj.Name+') is not in Approved status';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, resultMessage));
        }
    }
}