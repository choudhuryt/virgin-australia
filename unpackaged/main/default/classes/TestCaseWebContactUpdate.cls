/**
* @description       : Test class for CaseWebContactUpdate
* @createdBy         : Cloudwerx
* @Updated By        : Cloudwerx
**/
@isTest
private class TestCaseWebContactUpdate {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '', 'Active', 'First Nominee, Second Nominee', '');
        INSERT testContactObj;
    }
    
    @isTest
    private static void testCaseWebContactUpdateForSuppliedEmail() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Contact testContactObj = [SELECT Id, Email FROM Contact LIMIT 1];
        Test.startTest();
        Case testCaseObj = TestDataFactory.createTestCase(accountId, 'Test Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        testCaseObj.ContactId = testContactObj.Id;
        testCaseObj.SuppliedEmail = testContactObj.Email;
        INSERT testCaseObj;
        Test.stopTest();
        //Asserts
        Contact updatedContactObj = [SELECT Id, Velocity_Number__c, Velocity_member__c FROM Contact];
        system.assertEquals(testCaseObj.Velocity_Number__c, updatedContactObj.Velocity_Number__c);
        system.assertEquals(true, updatedContactObj.Velocity_member__c);
    }
    
    @isTest
    private static void testCaseWebContactUpdateWithPhone() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Contact testContactObj = [SELECT Id, Email FROM Contact LIMIT 1];
        Test.startTest();
        Case testCaseObj = TestDataFactory.createTestCase(accountId, 'Test Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        testCaseObj.ContactId = testContactObj.Id;
        testCaseObj.SuppliedEmail = testContactObj.Email;
        testCaseObj.Contact_Phone__c = '01234';
        INSERT testCaseObj;
        Test.stopTest();
        //Asserts
        Contact updatedContactObj = [SELECT Id, phone FROM Contact];
        system.assertEquals(testCaseObj.Contact_Phone__c, updatedContactObj.phone);
    }
    
    @isTest
    private static void testCaseWebContactUpdateWithMobilePhone() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Contact testContactObj = [SELECT Id, Email FROM Contact LIMIT 1];
        Test.startTest();
        Case testCaseObj = TestDataFactory.createTestCase(accountId, 'Test Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        testCaseObj.ContactId = testContactObj.Id;
        testCaseObj.SuppliedEmail = testContactObj.Email;
        testCaseObj.Contact_Mobile__c = '01234';
        INSERT testCaseObj;
        Test.stopTest();
        //Asserts
        Contact updatedContactObj = [SELECT Id, mobilePhone FROM Contact];
        system.assertEquals(testCaseObj.Contact_Mobile__c, updatedContactObj.mobilePhone);
    }
    
    @isTest
    private static void testCaseWebContactUpdateWithTravelAgent() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Contact testContactObj = [SELECT Id, Email FROM Contact LIMIT 1];
        Test.startTest();
        Case testCaseObj = TestDataFactory.createTestCase(accountId, 'Test Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        testCaseObj.ContactId = testContactObj.Id;
        testCaseObj.SuppliedEmail = testContactObj.Email;
        testCaseObj.Travel_Agent_ID__c = 'ABCID';
        INSERT testCaseObj;
        Test.stopTest();
        //Asserts
        Contact updatedContactObj = [SELECT Id, travel_agent_id__c, I_am_a_travel_agent__c FROM Contact];
        system.assertEquals(testCaseObj.travel_agent_id__c, updatedContactObj.Travel_Agent_ID__c);
        system.assertEquals(true, updatedContactObj.I_am_a_travel_agent__c);
    }
    
    @isTest
    private static void testCaseWebContactUpdateWithAdditionalDetails() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Contact testContactObj = [SELECT Id, Email FROM Contact LIMIT 1];
        Test.startTest();
        Case testCaseObj = TestDataFactory.createTestCase(accountId, 'Test Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        testCaseObj.ContactId = testContactObj.Id;
        testCaseObj.SuppliedEmail = testContactObj.Email;
        testCaseObj.Additional_Details__c = 'I am a travel Agent';
        INSERT testCaseObj;
        Test.stopTest();
        //Asserts
        Contact updatedContactObj = [SELECT Id, I_am_a_travel_agent__c FROM Contact];
        system.assertEquals(true, updatedContactObj.I_am_a_travel_agent__c);
    }
    
    @isTest
    private static void testCaseWebContactUpdateWithVelocityType() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Contact testContactObj = [SELECT Id, Email FROM Contact LIMIT 1];
        Test.startTest();
        Case testCaseObj = TestDataFactory.createTestCase(accountId, 'Test Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        testCaseObj.ContactId = testContactObj.Id;
        testCaseObj.SuppliedEmail = testContactObj.Email;
        testCaseObj.Velocity_Type__c = 'Gold';
        INSERT testCaseObj;
        Test.stopTest();
        //Asserts
        Contact updatedContactObj = [SELECT Id, Velocity_Status__c FROM Contact];
        system.assertEquals(testCaseObj.Velocity_Type__c, updatedContactObj.Velocity_Status__c);
    }
    
     @isTest
    private static void testCaseWebContactUpdateWithContactEmail() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Contact testContactObj = [SELECT Id, Email FROM Contact LIMIT 1];
        Test.startTest();
        
        Entitlement testEntitlementObj = TestDataFactory.createTestEntitlement(accountId, 'Entitlement', 'Phone Support');
        INSERT testEntitlementObj;
        
        GR8_Case_Categories__c testGR8CaseCategoryObj = TestDataFactory.createTestGRCaseCategories(true, true, 'Test', true, Date.today(), false, 'Boarding');
        testGR8CaseCategoryObj.RecordTypeId = Utilities.getRecordTypeId('GR8_Case_Categories__c', 'GR_Live');
        INSERT testGR8CaseCategoryObj;
        
        Id caseRecordTypeId = Utilities.getRecordTypeId('Case', 'GR_Case');
        Case testCaseObj = TestDataFactory.createTestCase(accountId, 'Test Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        testCaseObj.Category_Number__c = testGR8CaseCategoryObj.Id;
        testCaseObj.Airline_Organisation_Short__c = 'VAA';
        testCaseObj.Type = 'Complaint';
        testCaseObj.Pre_Filter_Primary_Category__c = 'Boarding';
        testCaseObj.RecordTypeId = caseRecordTypeId;
        testCaseObj.EntitlementId = testEntitlementObj.Id;
        testCaseObj.Velocity_Type__c = 'Gold';
        testCaseObj.Contact_Email__c = testContactObj.Email;
        INSERT testCaseObj;
        Test.stopTest();
        //Asserts
        Contact updatedContactObj = [SELECT Id, Velocity_Status__c FROM Contact];
        system.assertEquals(testCaseObj.Velocity_Type__c, updatedContactObj.Velocity_Status__c);
    }
}