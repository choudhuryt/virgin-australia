public class AssetTriggerHandler
{

     public static void  CalculateOpportuntiyValue (Boolean isDelete, List<Asset> assetInTrigger, List<Asset> assetOldInTrigger)
    {
      Set<id> Aids  = new Set<id>()  ;     
       List<Opportunity> lstOPUpdate = new List<Opportunity>();         
        
      if(isDelete)
     {
       for( Asset assetOld: assetOldInTrigger)
      {            
            aids.add(assetOld.id) ;         
      }  
     }else  
     {     
      for( Asset assetdata: assetInTrigger ) 
     { 
     aids.add(assetdata.id) ;         
     } 
    } 
  
       List<Asset> assetList =  [select id ,Commercial_Value__c, Offer_Price__c,Opportunity__c
                                      FROM Asset  where 
                                      id = :aids and
                                      Opportunity__c <> NULL
                                      and RecordTypeId in (Select id  from RecordType
                                      where SobjectType = 'Asset'
									  and IsActive = true 
                                      and DeveloperName in('Adspace')) ]; 
        
       List<Opportunity> opList =  [select id ,Total_Commercial_Value__c,Total_Investment_Payable__c
                                        from Opportunity
                                        where id =  : assetList[0].Opportunity__c ];  
        if (assetList.size() > 0  && opList.size() > 0 )
        {
            
            
              AggregateResult[] totalcompvalue =
                                              [SELECT SUM(Commercial_Value__c) sumcomm, SUM(Offer_Price__c) sumoff
                                               FROM Asset
                                               where  Status in ('Offered' )
                                               and    Opportunity__c= : assetList[0].Opportunity__c
                                               and RecordTypeId in (Select id  from RecordType
                                               where SobjectType = 'Asset'
                                               and IsActive = true 
                                               and DeveloperName in ('AdSpace'))
                                              ];
            
             for(Opportunity op: oplist)
           {  
             op.Total_Commercial_Value__c = (decimal)totalcompvalue[0].get('sumcomm')==null?0:(decimal)totalcompvalue[0].get('sumcomm');  
             op.Total_Investment_Payable__c = (decimal)totalcompvalue[0].get('sumoff')==null?0:(decimal)totalcompvalue[0].get('sumoff');
          
             lstOpUpdate.add(op)  ;  
           } 
          update  lstOPUpdate ;    
        }
        
        
        
    }      
}