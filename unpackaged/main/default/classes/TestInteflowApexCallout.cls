/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * @UpdatedBy         : CloudWerx
 */

@isTest  
private class TestInteflowApexCallout {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;  
    }
    
    @isTest
    private static void testInteflowCallout(){
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Test.setMock(HttpCalloutMock.class, getStaticResourceCalloutMock());
        test.startTest();
        Inteflowapexcallout.invokeExternalWs(accountId, accountId, '49113973087', 'JEMINEX LIMITED', 'www.jeminex.com ', 'Level 20 68 Pitt Street','Sydney','NSW', '2000','OTHER' ,'Smitha','Nair','Test Manager','smitha.nair@virginaustralia.com','054554354' , 'DIRECTB2B','TEST');
        test.stopTest(); 
        Account updatedAccountObj = [SELECT Id, Inteflow_Request__c, Inteflow_Response__c FROM Account LIMIT 1];
        system.assertEquals(true, String.isNotBlank(updatedAccountObj.Inteflow_Request__c));
        system.assertEquals(true, String.isNotBlank(updatedAccountObj.Inteflow_Response__c));
    }  
    
    @isTest
    private static void testInteflowCalloutWith13Chars(){
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Test.setMock(HttpCalloutMock.class, getStaticResourceCalloutMock());
        test.startTest();
        Inteflowapexcallout.invokeExternalWs(accountId, accountId, '4911397308730', 'JEMINEX LIMITED', 'www.jeminex.com ', 'Level 20 68 Pitt Street','Sydney','NSW', '2000','OTHER' ,'Smitha','Nair','Test Manager','smitha.nair@virginaustralia.com','054554354' , 'DIRECTB2B','TEST');
        test.stopTest(); 
        Account updatedAccountObj = [SELECT Id, Inteflow_Request__c, Inteflow_Response__c FROM Account LIMIT 1];
        system.assertEquals(true, String.isNotBlank(updatedAccountObj.Inteflow_Request__c));
        system.assertEquals(true, String.isNotBlank(updatedAccountObj.Inteflow_Response__c));
    }
    
    @isTest
    private static void testInteflowCalloutMoreThan13(){
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Test.setMock(HttpCalloutMock.class, getStaticResourceCalloutMock());
        test.startTest();
        Inteflowapexcallout.invokeExternalWs(accountId, accountId, '491139730871', 'JEMINEX LIMITED', 'www.jeminex.com ', 'Level 20 68 Pitt Street','Sydney','NSW', '2000','OTHER' ,'Smitha','Nair','Test Manager','smitha.nair@virginaustralia.com','054554354' , 'DIRECTB2B','TEST');
        test.stopTest(); 
        Account updatedAccountObj = [SELECT Id, Inteflow_Request__c, Inteflow_Response__c FROM Account LIMIT 1];
        system.assertEquals(true, String.isNotBlank(updatedAccountObj.Inteflow_Request__c));
        system.assertEquals(true, String.isNotBlank(updatedAccountObj.Inteflow_Response__c));
    }
    
    @isTest
    private static void testInteflowCalloutException(){
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Test.setMock(HttpCalloutMock.class, new UnauthorizedEndpointResponse());
        test.startTest();
        Inteflowapexcallout.invokeExternalWs(accountId, accountId, '4911397308730', 'JEMINEX LIMITED', 'www.jeminex.com ', 'Level 20 68 Pitt Street','Sydney','NSW', '2000','OTHER' ,'Smitha','Nair','Test Manager','smitha.nair@virginaustralia.com','054554354' , 'DIRECTB2B','TEST');
        test.stopTest(); 
    }
    
    
    private static StaticResourceCalloutMock getStaticResourceCalloutMock() {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TestInteflowResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml; charset=utf-8');	
        return mock;
    }
    
    @TestVisible class UnauthorizedEndpointResponse implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            CalloutException e = (CalloutException)CalloutException.class.newInstance();
            e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings.');
            throw e;
        }
    }
}