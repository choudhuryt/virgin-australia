/**
 * @description       : Test class for AccountTMCHierarchy
 * @Updated By        : Cloudwerx
**/
@isTest
private class testAccountTMCHierarchy {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;  
        
        Account testAccountObj1 = TestDataFactory.createTestAccount('TEST1', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        testAccountObj1.Main_Corporate_TMC__c = testAccountObj.Id;
        INSERT testAccountObj1;     
    }
    
    @isTest
    private static void testAccountTMCHierarchy(){
        Account accObj = [SELECT Id FROM Account WHERE Main_Corporate_TMC__c != null];
        
        UPDATE accObj;
        test.startTest(); 
        PageReference AccountTMCHierarchy = Page.AccountTMCHierarchy;
        Test.setCurrentPage( AccountTMCHierarchy );
        ApexPages.currentPage().getParameters().put('id', accObj.Id);
        // Instanciate Controller
        AccountTMCHierarchy controller = new AccountTMCHierarchy();
        
        // Call Methodes for top account
        controller.setcurrentId(null);
        AccountTMCHierarchy.ObjectStructureMap[] objectStructureMap = new AccountTMCHierarchy.ObjectStructureMap[]{};
        objectStructureMap = controller.getObjectStructure();
        // Asserts
        System.Assert(objectStructureMap.size() > 0, 'Test failed at Top account, no Id' );
       
        //Call ObjectStructureMap methodes
        objectStructureMap[0].setnodeId('1234567890');
        objectStructureMap[0].setlevelFlag(true);
        objectStructureMap[0].setlcloseFlag(false);
        objectStructureMap[0].setnodeType('parent');
        objectStructureMap[0].setcurrentNode(false);
        objectStructureMap[0].setaccount(accObj);
        objectStructureMap[0].getIndentation();
        
        String nodeId       = objectStructureMap[0].getnodeId();
        Boolean[] levelFlag = objectStructureMap[0].getlevelFlag();
        Boolean[] closeFlag = objectStructureMap[0].getcloseFlag();
        String nodeType     = objectStructureMap[0].getnodeType();
        Boolean currentName = objectStructureMap[0].getcurrentNode();
        Account smbAccount  = objectStructureMap[0].getaccount();
        test.stopTest();
    }
}