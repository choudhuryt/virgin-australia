@isTest
private class TestSmartflyAccelerateAutomation 
{
    
    static testMethod void myUnitTest()
    {
    
       Test.StartTest();	
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrPAAU';
    account.Sales_Matrix_Owner__c = 'Accelerate';
    account.Market_Segment__c = 'SmartFly';
	account.OwnerId = '00590000000LbNz'; 
    insert account;   
        
        
    Contact newContact1 = new Contact(); 
	newContact1.FirstName ='Andy';
	newContact1.LastName ='C';
    newContact1.Status__c = 'Active';  
	newContact1.AccountId = account.id;
    newContact1.Key_Contact__c = true;
	newContact1.Email ='test@gmail.com';
	newContact1.SmartFly_Contact_Status__c = 'Key Contact';
    insert    newContact1 ;     
        
    Contract contract = New Contract();
    contract.name ='PUTCONTRACTNAMEHERE';
    contract.AccountId =account.id;
    contract.Status ='Draft';
    contract.StartDate = Date.newInstance(2018,01,01);
    contract.RecordTypeId ='012900000009HrU'; 
    insert contract;
    
    contract.Status ='Activated';
    update contract;
        
    account.Request_to_Move_to_Accelrate__c =  true ;
    account.Notify_Key_Contact__c = 'Yes';
    account.SmartFly_to_Accelerate_Movement_Date__c =  Date.newInstance(2010,01,01);   
    update account ;
    }

}