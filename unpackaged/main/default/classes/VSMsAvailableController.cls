/** 
 *  File Name       : VSMsAvailableController 
 *
 *  Description     : 
 *                      
 *  Copyright       : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
 *  @author         : Steve Kerr
 *  Date            : 20th October 2015
 *
 *  Notes           : 
 *  UpdatedBy : cloudwerx
 **/ 
public with sharing class VSMsAvailableController {
    
    //@UpdatedBy : Cloudwerx added @TestVisible to increase coverage to 100%
    @testvisible private ApexPages.StandardController controller {get; set;}
    public List<VSM_Utills.VSM_Totals> totals {get;set;}
    public String message {get;set;}
    private Account a;
    
    // Constructor
    public VSMsAvailableController(ApexPages.StandardController myController) {
        a = (Account)myController.getrecord();
        totals = new List<VSM_Utills.VSM_Totals>();
        // Get all active contracts
        List<Contract> contracts = [SELECT Id, Silver__c, Pilot_Gold__c, Gold__c, Platinum__c, ContractNumber
                                    FROM Contract WHERE AccountId = :a.id AND (EndDate >= today or EndDate=null)
                                    AND Status in ('Activated') ORDER BY EndDate DESC];
        
        for(Contract contract : contracts) {
            VSM_Utills.VSM_Totals tots = VSM_Utills.getVSM_Totals(contract.Id);
            totals.add(tots);            
        }
        
        if(contracts.size() == 0) {
            message = '***   No active contracts are available.   ***';
        }
    }
    
    //pre-processing prior to Page load
    public PageReference initDisc() {
        return null;
    }
}