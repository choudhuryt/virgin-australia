@isTest
private class TestInternationalLONDiscountController
{
    
     static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        
        //setup account
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
  	
  			
 		insert account;
 
        Contract contractOld = new Contract();
        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.VA_VS_Requested_Tier__c = '1';
        contractOld.Cos_Version__c = '15.0';
        contractOld.Red_Circle__c =true;
        
        
        insert contractOld;
        
        			International_Discounts_LON__c LONDiscount = new International_Discounts_LON__c();
        			LONDiscount.Tier__c ='1';
        			LONDiscount.Is_Template__c =true;
        			LONDiscount.Cos_Version__c ='15.0';
        			
                    
                LONDiscount.DiscOffPublishFare_01__c = 10;
				LONDiscount.DiscOffPublishFare_02__c = 10;
				LONDiscount.DiscOffPublishFare_03__c = 10;
				LONDiscount.DiscOffPublishFare_04__c = 10;
				LONDiscount.DiscOffPublishFare_05__c = 10;
				LONDiscount.DiscOffPublishFare_06__c = 10;
				LONDiscount.DiscOffPublishFare_07__c = 10;
				LONDiscount.DiscOffPublishFare_08__c = 10;
				LONDiscount.DiscOffPublishFare_09__c = 10;
				LONDiscount.DiscOffPublishFare_10__c = 10;
				LONDiscount.DiscOffPublishFare_11__c = 10;
				LONDiscount.DiscOffPublishFare_12__c = 10;
				LONDiscount.DiscOffPublishFare_13__c= 10;				
				LONDiscount.DiscOffPublishFare_14__c = 10;
                LONDiscount.DiscOffPublishFare_15__c = 10;
                LONDiscount.DiscOffPublishFare_16__c = 10;
                    insert LONDiscount;
        		
       
       		PageReference pref = Page.InternationalLONDiscountPage;
        	pref.getParameters().put('id', contractOld.id);
        	Test.setCurrentPage(pref);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(contractOld);
        InternationalLONDiscountController lController = new InternationalLONDiscountController(conL);
        
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conX = new ApexPages.StandardController(contractOld);
        InternationalLONDiscountController xController = new InternationalLONDiscountController(conX);
          
          Test.startTest();
        PageReference  ref1 = Page.InternationalLONDiscountPage;
        ref1 = lController.initDisc();
        ref1 = lController.newLONDiscounts();
       
        ref1 = lController.save();
        ref1 = lController.initDisc();
        ref1 = lController.newLONDiscounts();
        
      	ref1 = lController.qsave();
       
        ref1 = lController.initDisc();
        ref1 = lController.remLONDiscounts();
        
        
        ref1 = lController.cancel();
        
        contractOld.VA_VS_Requested_Tier__c = '0';
        contractOld.Cos_Version__c = '';
        contractOld.Red_Circle__c =false;
        update contractOld;
        
        ref1 = lController.initDisc();
        ref1 = lController.newLONDiscounts();
       
        ref1 = lController.save();
        ref1 = lController.initDisc();
        ref1 = lController.newLONDiscounts();
        ref1 = lController.qsave();
        
        test.stopTest();
        
        
    }

}