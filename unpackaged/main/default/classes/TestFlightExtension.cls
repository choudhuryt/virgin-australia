/**
* @description       : Test class for FlightExtension
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestFlightExtension {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        INSERT testCaseObj;
    }
    
    @isTest
    private static void testFlightExtensionLoadReservation() {
        Case testCaseObj = [SELECT Id FROM Case LIMIT 1];
        Reservation__c testReservationObj = TestDataFactory.createTestReservation(testCaseObj.Id, 'R123', Date.today(), 'SYS', 'MEL');
        INSERT testReservationObj;
        Test.startTest();
        PageReference pageRef = Page.FlightSection;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testCaseObj);
        FlightExtension flightExtensionController  = new FlightExtension(conL);
        flightExtensionController.LoadReservation();
        Test.stopTest();
        System.assertEquals(testReservationObj.Id, flightExtensionController.reserObj.Id);
        System.assertEquals(testReservationObj.Destination__c, flightExtensionController.reserObj.Destination__c);
    }
    
    @isTest
    private static void testFlightExtensionNoReservation() {
        Case testCaseObj = [SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        PageReference pageRef = Page.FlightSection;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testCaseObj);
        FlightExtension flightExtensionController  = new FlightExtension(conL);
        flightExtensionController.LoadReservation();
        Test.stopTest();
        System.assertEquals(new Reservation__c(), flightExtensionController.reserObj);
    }
}