public  with sharing class EUEYONDSelectionController {

 //standard controller and search results
    private ApexPages.StandardController controller {get; set;}
    public contract con{get;set;}
    public Boolean EUZone1 {get;set;}
    public Boolean EUZone2 {get;set;}
    public Boolean EUZone3 {get;set;}
    public Boolean EUZone4 {get;set;}
    public Boolean EUZone5 {get;set;}
    public Boolean EUZone6 {get;set;}
    public Boolean EUZone7 {get;set;}
    public Boolean EUZone8 {get;set;}
    public Boolean MEZone1 {get;set;}
    public Boolean MEZone2 {get;set;}
    public Boolean MEZone3 {get;set;}
    public Boolean MEZone4 {get;set;}
    public Boolean MEZone5 {get;set;}
    public Boolean MEZone6 {get;set;}
    public Boolean AFZone1 {get;set;}
    public Boolean AFZone2 {get;set;}
    public List <Contract> contracts {get;set;}
    public Boolean isTemplateDiscount {get;set;}
    private Contract a;
    private boolean greaterthat14_4;
// Constructor
    public EUEYONDSelectionController (ApexPages.StandardController myController) 
    {
       
              con=(Contract)myController.getrecord();
    }
       //pre-processing prior to page load
    public PageReference initDisc() 
    {
    
     try{
   
       contracts=
                  [select id,VA_EY_Requested_Tier__c,Cos_Version__c, VA_EY_EU_Zone__c, VA_EY_ME_Zone__c,VA_EY_AF_Zone__c,
                  EUEYZ1ONDSelected__c,EUEYZ2ONDSelected__c,EUEYZ3ONDSelected__c,EUEYZ4ONDSelected__c,
                  EUEYZ5ONDSelected__c,EUEYZ6ONDSelected__c,EUEYZ7ONDSelected__c,EUEYZ8ONDSelected__c,
                  MEEYZ1ONDSelected__c,MEEYZ2ONDSelected__c,MEEYZ3ONDSelected__c,MEEYZ4ONDSelected__c,
                  MEEYZ5ONDSelected__c,MEEYZ6ONDSelected__c,AFEYZ1ONDSelected__c,AFEYZ2ONDSelected__c
                  from Contract where id =:ApexPages.currentPage().getParameters().get('id')];
       con = contracts.get(0);
         
       greaterthat14_4 = false ;
  
       if (con.Cos_Version__c == '15.0' ) 
       {
          greaterthat14_4 = true ;
       }else
       {
           greaterthat14_4 = false ;
       }  
           
       
       if(con.VA_EY_EU_Zone__c != null) 
        {
         EUZone1=con.VA_EY_EU_Zone__c.contains('1');  
         EUZone2=con.VA_EY_EU_Zone__c.contains('2');  
         EUZone3=con.VA_EY_EU_Zone__c.contains('3');  
      //   EUZone4=con.VA_EY_EU_Zone__c.contains('4');
      //   EUZone5=con.VA_EY_EU_Zone__c.contains('5');
      //   EUZone6=con.VA_EY_EU_Zone__c.contains('6');
      //   EUZone7=con.VA_EY_EU_Zone__c.contains('7');
      //   EUZone8=con.VA_EY_EU_Zone__c.contains('8');   
        }      
        if(con.VA_EY_ME_Zone__c != null) 
        {
         MEZone1=con.VA_EY_ME_Zone__c.contains('1');  
         MEZone2=con.VA_EY_ME_Zone__c.contains('2');  
         MEZone3=con.VA_EY_ME_Zone__c.contains('3');  
         MEZone4=con.VA_EY_ME_Zone__c.contains('4');
      //   MEZone5=con.VA_EY_EU_Zone__c.contains('5');
      //   MEZone6=con.VA_EY_EU_Zone__c.contains('6'); 
        }     
         
         if(con.VA_EY_AF_Zone__c != null) 
        {
         AFZone1=con.VA_EY_AF_Zone__c.contains('1');  
         AFZone2=con.VA_EY_AF_Zone__c.contains('2');           
        }      
       }catch(Exception e){
         
       }
        return null;
}
    
    
    
    
     public PageReference save(){
      system.debug('Inside save')  ;       
      upsert con;
      PageReference newPage;
      newPage = Page.EY_Discount_page_v5 ;
      newPage.getParameters().put('id', ApexPages.currentPage().getParameters().get('id'));            
      return newPage ; 
      newPage.setRedirect(true);
      return newPage;   
         
     }
    
      public PageReference meafsave(){
      system.debug('Inside save')  ;       
      upsert con;
          
      PageReference newPage;
        if(Test.isRunningTest() ) 
         {
            greaterthat14_4 = true; 
         }
       if(greaterthat14_4)
       {
         newPage = Page.EY_MEAFDiscount_page_v6 ;      
       }
       else
       {
         newPage = Page.EY_MEAFDiscount_page_v5 ;      
       }
      newPage.getParameters().put('id', ApexPages.currentPage().getParameters().get('id'));            
      return newPage ; 
      newPage.setRedirect(true);
      return newPage;   
       }    
        
    //get the multi-select pick list values for Zone1
    //
    public List<SelectOption> EYEUZ1ONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.EUEYZ1ONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
     public String[] EYEUZ1ONDItems { 
     get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EYEUZ1ONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.EUEYZ1ONDSelected__c !=null && this.con.EUEYZ1ONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.EUEYZ1ONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.EUEYZ1ONDSelected__c + con.id ); 
        }
       } 
     
        public List<SelectOption> EYEUZ2ONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.EUEYZ2ONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
     public String[] EYEUZ2ONDItems { 
     get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EYEUZ2ONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.EUEYZ2ONDSelected__c !=null && this.con.EUEYZ2ONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.EUEYZ2ONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.EUEYZ2ONDSelected__c + con.id ); 
        }
       }
    
    
     public List<SelectOption> EYEUZ3ONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.EUEYZ3ONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
     public String[] EYEUZ3ONDItems { 
     get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EYEUZ3ONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.EUEYZ3ONDSelected__c !=null && this.con.EUEYZ3ONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.EUEYZ3ONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.EUEYZ3ONDSelected__c + con.id ); 
        }
       }
    
   
    
     public List<SelectOption> EYMEZ1ONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.MEEYZ1ONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
     public String[] EYMEZ1ONDItems { 
     get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EYMEZ1ONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.MEEYZ1ONDSelected__c !=null && this.con.MEEYZ1ONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.MEEYZ1ONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.MEEYZ1ONDSelected__c + con.id ); 
        }
       }
    
       public List<SelectOption> EYMEZ2ONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.MEEYZ2ONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
     public String[] EYMEZ2ONDItems { 
     get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EYMEZ2ONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.MEEYZ2ONDSelected__c !=null && this.con.MEEYZ2ONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.MEEYZ2ONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.MEEYZ2ONDSelected__c + con.id ); 
        } 
       }

     public List<SelectOption> EYMEZ3ONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.MEEYZ3ONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
     public String[] EYMEZ3ONDItems { 
     get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EYMEZ3ONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.MEEYZ3ONDSelected__c !=null && this.con.MEEYZ3ONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.MEEYZ3ONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.MEEYZ3ONDSelected__c + con.id ); 
        } 
       }

    public List<SelectOption> EYMEZ4ONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.MEEYZ4ONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
     public String[] EYMEZ4ONDItems { 
     get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EYMEZ4ONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.MEEYZ4ONDSelected__c !=null && this.con.MEEYZ4ONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.MEEYZ4ONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.MEEYZ4ONDSelected__c + con.id ); 
        } 
       }
    
    
     public List<SelectOption> EYAFZ1ONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.AFEYZ1ONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
     public String[] EYAFZ1ONDItems { 
     get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EYAFZ1ONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.AFEYZ1ONDSelected__c !=null && this.con.AFEYZ1ONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.AFEYZ1ONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.AFEYZ1ONDSelected__c + con.id ); 
        }
       }
    
     public List<SelectOption> EYAFZ2ONDOptions
     {
      get {
       List<SelectOption> options = new List<SelectOption>();
       for( Schema.PicklistEntry f : contract.AFEYZ2ONDSelected__c.getDescribe().getPicklistValues())
       {
         options.add(new SelectOption(f.getValue(), f.getLabel()));
        } 
       return options;
        }  
     set;
    }
    
     public String[] EYAFZ2ONDItems { 
     get {
        String[] selected = new List<String>();
        List<SelectOption> sos = this.EYAFZ2ONDOptions;
        for(SelectOption s : sos)
        {
        if (this.con.AFEYZ2ONDSelected__c !=null && this.con.AFEYZ2ONDSelected__c.contains(s.getValue()))
           selected.add(s.getValue());
        }
        return selected;
        }
        public set 
        {
         String selectedCheckBox = '';
         for(String s : value) {
          if (selectedCheckBox == '') 
           selectedCheckBox += s;
         else selectedCheckBox += ';' + s;
          }
        con.AFEYZ2ONDSelected__c = selectedCheckBox;
        system.debug('Selected checkbox' + con.AFEYZ2ONDSelected__c + con.id ); 
        }
       }
    
   }