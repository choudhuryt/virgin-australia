@isTest
public class TestDomesticAddendumDiscountControllerv1 {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        
        Contract_Addendum__c testContractAddendumObj = TestDataFactory.createTestContractAddendum(testContractObj.Id, '15', 'Tier 1', 'Tier 1', true, 'SYDMEL', 'PERKTH', 'Draft');
        INSERT testContractAddendumObj;
        testContractAddendumObj.Airline__c = 'UA;VA;QR;SQ';
        UPDATE testContractAddendumObj;
        
        Opportunity testOpportunityObj = TestDataFactory.createTestOpportunity('testOpp1', 'Sales Opportunity Analysis', testAccountObj.Id, Date.today(), 1.00);
        INSERT testOpportunityObj; 
        
        List<Proposal_Table__c> testProposalTables = new List<Proposal_Table__c>();
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'DOM Mainline'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'DOM Regional'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'INT Short Haul'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'International Short Haul'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'Trans Tasman'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'North America'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'Africa'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'Middle East QR'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'UK/Europe QR'));
        testProposalTables.add(TestDataFactory.createTestProposalTable(testOpportunityObj.Id, testContractObj.Id, 'Tier 1', 'UK/Europe SQ'));
        INSERT testProposalTables;
    }
    
    @isTest
    private static void testDomAddController_v1() {
        Contract_Addendum__c testContractAddendumObj = [SELECT Id FROM Contract_Addendum__c];
        
        List<Proposed_Discount_Tables__c> testProposedDiscountTables = new List<Proposed_Discount_Tables__c>();
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('DOM Mainline', testContractAddendumObj.Id, 10, 'Business', 'J'));
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('DOM Regional', testContractAddendumObj.Id, 10, 'Business', 'D'));
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('INT Short Haul', testContractAddendumObj.Id, 10, 'Business', 'A'));
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('International Short Haul', testContractAddendumObj.Id, 10, 'Business', 'A'));
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('Trans Tasman', testContractAddendumObj.Id, 10, 'Business', 'B'));
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('North America', testContractAddendumObj.Id, 10, 'Business', 'C'));
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('Africa', testContractAddendumObj.Id, 10, 'Business', 'D'));
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('Middle East QR', testContractAddendumObj.Id, 10, 'Business', 'E'));
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('UK/Europe QR', testContractAddendumObj.Id, 10, 'Business', 'F'));
        testProposedDiscountTables.add(TestDataFactory.createTestProposedDiscountTables('UK/Europe SQ', testContractAddendumObj.Id, 10, 'Business', 'G'));
        
        INSERT testProposedDiscountTables;
        
        Test.startTest();
        PageReference pageRef = Page.DomesticAddendumDiscount_v1;
        pageRef.getParameters().put('id', testContractAddendumObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractAddendumObj);
        DomesticAddendumDiscountController_v1 lController = new DomesticAddendumDiscountController_v1(conL);        
        PageReference initDiscPageReference = lController.initDisc();
        PageReference savePageReference = lController.save();
        PageReference qSavePageReference = lController.qsave();
        PageReference newDomPageReference = lController.newDom();
        PageReference newRegPageReference = lController.newReg();
        PageReference newTransPageReference = lController.newTrans();
        PageReference newIntPageReference = lController.newInt();
        PageReference newNAPageReference = lController.newNA();
        PageReference newMEPageReference = lController.newME();
        PageReference newAfPageReference = lController.newaf();
        PageReference newUK_QRPageReference = lController.newUK_QR();
        PageReference newUK_SQPageReference = lController.newUK_SQ();
        PageReference remDomPageReference = lController.remDom();
        PageReference remRegPageReference = lController.remReg();
        PageReference remTransPageReference = lController.remTrans();
        PageReference remIntPageReference = lController.remInt();
        PageReference remNAPageReference = lController.remNA();
        PageReference remMEPageReference = lController.remME();
        PageReference remAfPageReference = lController.remAf();
        PageReference remUK_QRPageReference = lController.remUK_QR();
        PageReference remUK_SQPageReference = lController.remUK_SQ();
        PageReference cancelPageReference = lController.cancel();
        Test.stopTest();
        //Asserts
        system.assertEquals(null, initDiscPageReference);
        system.assertEquals('/' + testContractAddendumObj.Id, savePageReference.getUrl());
        system.assertEquals('/apex/DomesticAddendumDiscount_v1?id=' + testContractAddendumObj.Id, qSavePageReference.getUrl());
        system.assertEquals('/apex/DomesticAddendumDiscount_v1?id=' + testContractAddendumObj.Id, newDomPageReference.getUrl());
        system.assertEquals('/apex/DomesticAddendumDiscount_v1?id=' + testContractAddendumObj.Id, newRegPageReference.getUrl());
        system.assertEquals('/apex/DomesticAddendumDiscount_v1?id=' + testContractAddendumObj.Id, remDomPageReference.getUrl());
        system.assertEquals('/apex/DomesticAddendumDiscount_v1?id=' + testContractAddendumObj.Id, remRegPageReference.getUrl());
        system.assertEquals('/' + testContractAddendumObj.Id, cancelPageReference.getUrl());
    }
    
    @isTest
    private static void testDomAddController_v1WithoutPropDiscounts() {
        Contract_Addendum__c testContractAddendumObj = [SELECT Id FROM Contract_Addendum__c];
        testContractAddendumObj.Trans_Tasman_Requested_Tier__c = 'Tier 1';
        testContractAddendumObj.INT_short_Haul_Requested_Tier__c = 'Tier 1';
        testContractAddendumObj.North_America_Requested_Tier__c = 'Tier 1';
        testContractAddendumObj.Africa_requested_Tier__c = 'Tier 1';
        testContractAddendumObj.Middle_East_Requested_Tier__c = 'Tier 1';
        testContractAddendumObj.UK_Europe_Requested_Tier_QR__c = 'Tier 1';
        testContractAddendumObj.UK_Europe_Requested_Tier_SQ__c = 'Tier 1';
        update testContractAddendumObj;
              
        Test.startTest();
        PageReference pageRef = Page.DomesticAddendumDiscount_v1;
        pageRef.getParameters().put('id', testContractAddendumObj.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractAddendumObj);
        DomesticAddendumDiscountController_v1 lController = new DomesticAddendumDiscountController_v1(conL);        
        PageReference initDiscPageReference = lController.initDisc();
        PageReference savePageReference = lController.save();
        PageReference qSavePageReference = lController.qsave();
        PageReference newDomPageReference = lController.newDom();
        PageReference newRegPageReference = lController.newReg();
        PageReference newTransPageReference = lController.newTrans();
        PageReference newIntPageReference = lController.newInt();
        PageReference newNAPageReference = lController.newNA();
        PageReference newMEPageReference = lController.newME();
        PageReference newAfPageReference = lController.newaf();
        PageReference newUK_QRPageReference = lController.newUK_QR();
        PageReference newUK_SQPageReference = lController.newUK_SQ();
        

        Test.stopTest();
        //Asserts
        system.assertEquals(null, initDiscPageReference);
        system.assertEquals('/' + testContractAddendumObj.Id, savePageReference.getUrl());
        
    }
    
}