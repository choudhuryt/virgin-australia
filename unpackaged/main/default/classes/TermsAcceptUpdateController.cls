public class TermsAcceptUpdateController
{
  public List<Account> acct {get;set;} 
  public List<Account_Self_Update__c> accupdate {get;set;}   
  public Account accountdetail{get;set;} 
  public Account_Self_Update__c newAcctSelfUpdate {get;set;}
  private Account a;  
  public ApexPages.StandardController mycontroller;  
  public  string recId{get; set;}
  public boolean Validforupdate{get;set;}
    
   public TermsAcceptUpdateController(ApexPages.StandardController stdController) 
    {
        Validforupdate = true ;
        this.mycontroller = stdController;
        a=(Account)myController.getrecord();
        recId = ApexPages.currentPage().getParameters().get('Id');  
      
    }    
    
    public PageReference Checkifvalid()
    {
       acct=[select  Agreed_To_Terms_And_Conditions__c,
                      Agree_To_Use_The_Booking_Code__c,
                      Authorisation_Rep_Name__c,
                      Authorisation_Rep_Date__c,
                      Authorisation_Rep_Position__c,Authorised_Contact__c
                      from Account where
                      id =:ApexPages.currentPage().getParameters().get('id')];
        accountdetail = acct.get(0);   
        
        
        
        newAcctSelfUpdate = new Account_Self_Update__c();      
        
        accupdate= [select Agreed_To_Terms_And_Conditions__c,
                      Agree_To_Use_The_Booking_Code__c,
                      Authorisation_Rep_Name__c,
                      Authorisation_Rep_Date__c,
                      Authorisation_Rep_Position__c,Authorised_Contact__c
                      from Account_Self_Update__c where 
                      Account__c=:ApexPages.currentPage().getParameters().get('id')
                      and  terms_ConditionsUpdated__c = true
                    ];
        
        if (accupdate.size() > 0)
        {
            Validforupdate = false ;
        }
        system.debug( 'Check validity' + Validforupdate );
        
        if(Validforupdate == false)
        {
           system.debug( 'Inside redirect' + Validforupdate ); 
           return Page.TermsAcceptConfirmation;
        }else 
        {
            system.debug( 'outside redirect' + Validforupdate ); 
			return null;
		}
		
	}


    
     public PageReference clickYes()
     {
        system.debug( 'Inside submit' + acct.size() + accountdetail.Agreed_To_Terms_And_Conditions__c) ; 
         
		if(acct.size() > 0 )
        {            
          if (newAcctSelfUpdate.Agreed_To_Terms_And_Conditions__c )
          {  
             newAcctSelfUpdate.Account__c = accountdetail.Id;
             newAcctSelfUpdate.Terms_ConditionsUpdated__c = true ; 
             newAcctSelfUpdate.Authorisation_Rep_Date__c = system.today() ;
             insert newAcctSelfUpdate;
             return Page.TermsAcceptConfirmation;
			//return new PageReference('/' + this.acct.Id);
		  }else
          {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check all agreement checkboxes before submitting.'));    
          return null;
          }
        }
       else 
        {
			return null;
		}
	 } 
    
   
}