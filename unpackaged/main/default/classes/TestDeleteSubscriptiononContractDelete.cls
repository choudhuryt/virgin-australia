/** 
* * File Name        : TestDeleteSubscriptionContractdelete
* * Description      : This Test Class test the deletion of a subcscription when a contact is deleted
					   The Test Class is created to test if the trigger is bulkified also, 	 
                        
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Andy Burgess
* * Date             : 25 June 2012
* * Technical Task ID: 
* * Notes            :   

====================================================================================

* * Modification Log *  *

Ver Date Author Modification
 
***/

@isTest
public with sharing class TestDeleteSubscriptiononContractDelete {

public static testMethod void deleteContacttesttrigger(){

	
	List<Contact> ListFailContact = new List<Contact>();
	
	//Creation of Contacts in a 
	//for loop is done to test that the trigger is bulkified
	//Common objects for test returns contact(s)
	for (Integer i =1; i <4 ;i ++)
	{
	Contact newContact = new Contact();
	CommonObjectsForTest  commonObj = new CommonObjectsForTest(); 
	newContact = commonObj.CreateNewContact(i);
	ListFailContact.add(newContact);
	System.Debug('!!!!!!!!!!!! contact ' + newContact.FirstName );
	
	}
	
	
	insert ListFailContact;
	
	
	
	delete ListFailContact;
	
}

}