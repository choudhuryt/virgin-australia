/**
* @description       : Test class for EUEYONDSelectionController
* @UpdatedBy         : CloudWerx
**/
@isTest
public class TestEUEYONDSelectionController {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;  
        
        Contract testContractObj = TestDataFactory.createTestContract('TestContract', testAccountObj.Id, 'Draft', '15', null, true, Date.today());
        testContractObj.Cos_Version__c = '14.4';
        testContractObj.Red_Circle__c = false;
        testContractObj.VA_EY_Requested_Tier__c = '1';
        testContractObj.VA_EY_EU_Zone__c = '1;2;3' ;
        testContractObj.VA_EY_ME_Zone__c = '1;2;3;4' ;
        testContractObj.VA_EY_AF_Zone__c = '1;2' ;
        INSERT testContractObj; 
    }
    
    @isTest 
    private static void testEUEYONDSelectionController() {
        Contract testContractObj = [SELECT Id, Cos_Version__c, Red_Circle__c, VA_EY_Requested_Tier__c,
                                    VA_EY_EU_Zone__c, VA_EY_ME_Zone__c, VA_EY_AF_Zone__c FROM Contract];
        
        Test.startTest();
        EUEYONDSelectionController lController = getEUEYONDSelectionControllerObj(testContractObj);
        lController.EUZone4 = true;
        lController.EUZone5 = true;
        lController.EUZone6 = true;
        lController.EUZone7 = true;
        lController.MEZone5 = true;
        lController.MEZone6 = true;
        lController.isTemplateDiscount = true;
        PageReference initDiscPageReference = lController.initDisc();
        PageReference savePageReference = lController.save(); 
        PageReference meafsaveReference = lController.meafsave();  
        
        lController.EYEUZ1ONDItems = new List<String> {'Adelaide-London', 'Sydney-London'};
        string [] testEYEUZ1ONDItems = lController.EYEUZ1ONDItems;
        
        lController.EYEUZ2ONDItems = new List<String> {'Sydney-Astana', 'Brisbane-Barcelona'};
        string [] testEYEUZ2ONDItems = lController.EYEUZ2ONDItems;
        
        lController.EYEUZ3ONDItems = new List<String> {'Perth-Dublin', 'Perth-Madrid'};
        string [] testEYEUZ3ONDItems = lController.EYEUZ3ONDItems;
        
        lController.EYMEZ1ONDItems = new List<String> {'Sydney-Al Ain', 'Melbourne-Dubai'};
        string [] testEYMEZ1ONDItems = lController.EYMEZ1ONDItems;
        
        lController.EYMEZ2ONDItems = new List<String> {'Adelaide-Amman', 'Adelaide-Khartoum'};
        string [] testEYMEZ2ONDItems = lController.EYMEZ2ONDItems;
        
        lController.EYMEZ3ONDItems = new List<String> {'Perth-Abu Dhabi', 'Perth-Dubai'};
        string [] testEYMEZ3ONDItems = lController.EYMEZ3ONDItems;
        
        lController.EYMEZ4ONDItems = new List<String> {'Perth-Dammam', 'Perth-Riyadh'};
        string [] testEYMEZ4ONDItems = lController.EYMEZ4ONDItems;
        
        lController.EYAFZ1ONDItems = new List<String> {'Brisbane-Johannesburg', 'Sydney-Mahe Island'};
        string [] testEYAFZ1ONDItems = lController.EYAFZ1ONDItems;
        
        lController.EYAFZ2ONDItems = new List<String> {'Perth-Nairobi', 'Perth-Dar es Salaam'};
        string [] testEYAFZ2ONDItems = lController.EYAFZ2ONDItems;
        test.stopTest();
        //Asserts
        System.assertEquals('Adelaide-London', testEYEUZ1ONDItems[0]);
        System.assertEquals('Sydney-London', testEYEUZ1ONDItems[1]);
        System.assertEquals('Sydney-Astana', testEYEUZ2ONDItems[0]);
        System.assertEquals('Brisbane-Barcelona', testEYEUZ2ONDItems[1]);        
        System.assertEquals('Perth-Dublin', testEYEUZ3ONDItems[0]);
        System.assertEquals('Perth-Madrid', testEYEUZ3ONDItems[1]);
        System.assertEquals('Sydney-Al Ain', testEYMEZ1ONDItems[0]);
        System.assertEquals('Melbourne-Dubai', testEYMEZ1ONDItems[1]);
        System.assertEquals('Adelaide-Amman', testEYMEZ2ONDItems[0]);
        System.assertEquals('Adelaide-Khartoum', testEYMEZ2ONDItems[1]);
        System.assertEquals('Perth-Abu Dhabi', testEYMEZ3ONDItems[0]);
        System.assertEquals('Perth-Dubai', testEYMEZ3ONDItems[1]);
        System.assertEquals('Perth-Dammam', testEYMEZ4ONDItems[0]);
        System.assertEquals('Perth-Riyadh', testEYMEZ4ONDItems[1]);
        System.assertEquals('Brisbane-Johannesburg', testEYAFZ1ONDItems[0]);
        System.assertEquals('Sydney-Mahe Island', testEYAFZ1ONDItems[1]);
        System.assertEquals('Perth-Dar es Salaam', testEYAFZ2ONDItems[0]);
        System.assertEquals('Perth-Nairobi', testEYAFZ2ONDItems[1]);
    }
    
    
    @isTest 
    private static void testEUEYONDSelectionControllerCISVERSION15() {
        Contract testContractObj = [SELECT Id, Cos_Version__c FROM Contract];
        testContractObj.Cos_Version__c = '15.0';
        UPDATE testContractObj;
        
        Test.startTest();
        EUEYONDSelectionController lController = getEUEYONDSelectionControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        test.stopTest();
        //Asserts
        System.assertEquals(null, initDiscPageReference);
    }
    
    private static EUEYONDSelectionController getEUEYONDSelectionControllerObj(Contract testContractObj) {
        PageReference pageRef = Page.EUEYONDSelection;
        pageRef.getParameters().put('id', testContractObj.id);
        Test.setCurrentPage(pageRef);
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        return new EUEYONDSelectionController(conL);
    }
}