/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * @updatedBy : cloudwerx
 */
@isTest
private class SIPInputUnitTest {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('Account Closed', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('Test Contract', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        
        SIP_Selection__c testSIPSelectionObj = TestDataFactory.createTestSIPSelection('Wholesale', 'Industry', 1, 'SIPSelection_' + Datetime.now());
        INSERT testSIPSelectionObj;
        
        SIP_Option__c testSIPOptionObj = TestDataFactory.createTestSIPOption('Umbrella Demo Description', '1', testSIPSelectionObj.Id, 1, 'SIPOption_' + Datetime.now());
        INSERT testSIPOptionObj;
    }
    
    @isTest
    private static void testSIPInputControllerWithNoContractId() {
        Contract testContractObj = [SELECT Id FROM Contract];
        SIP_Option__c testSIPOptionObj = [SELECT Id FROM SIP_Option__c];
        Test.startTest();
        SIPInputController sipInputControllerCtrl  = new SIPInputController();
        PageReference checkExistedPageRefernce = sipInputControllerCtrl.CheckExisted();
        PageReference cancelPageReference = sipInputControllerCtrl.Cancel();
        PageReference saveAndClosePageReference = sipInputControllerCtrl.SaveAndClose();
        Test.stopTest();
        //Asserts
        System.assertEquals('/home/home.jsp', checkExistedPageRefernce.getUrl());
        System.assertEquals('/', cancelPageReference.getUrl());
    }
    
    @isTest
    private static void testSIPInputControllerWithProperIds() {
        Contract testContractObj = [SELECT Id FROM Contract];
        SIP_Option__c testSIPOptionObj = [SELECT Id FROM SIP_Option__c];
        
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'SIP Header Description', null, 2000);
        testSIPHeaderObj.IsTemplate__c = true;
        INSERT testSIPHeaderObj;
        
        SIP_Rebate__c testSipRebateObj = TestDataFactory.createTestSIPRebate(testContractObj.Id, testSIPHeaderObj.Id, 20, 30, 10); 
        INSERT testSipRebateObj;
        
        SIP_Option_Join__c testSIPOptionJoin = TestDataFactory.createTestSIPOptionJoin(testSIPHeaderObj.Id, testSIPOptionObj.Id);
        INSERT testSIPOptionJoin;
        
        Test.startTest();
        PageReference pageRef = Page.SIPTableSelector;
        pageRef.getParameters().put('cid', testContractObj?.id);
        pageRef.getParameters().put('sipid', testSIPOptionObj?.id);
        pageRef.getParameters().put('currentID', testSIPHeaderObj.Description__c);
        Test.setCurrentPage(pageRef);
        SIPInputController sipInputControllerCtrl  = new SIPInputController();
        sipInputControllerCtrl.SIPOptionTitle = 'd';
        sipInputControllerCtrl.DeleteId = new List<String>{'d'};
        PageReference checkExistedPageRefernce = sipInputControllerCtrl.CheckExisted();
        PageReference cancelPageReference = sipInputControllerCtrl.Cancel();
        List<SIPInputController.SIPHeaderWrapper> sipHeaderWrapper = sipInputControllerCtrl.GetSIPOptionList();
        PageReference saveAndClosePageReference = sipInputControllerCtrl.SaveAndClose();
        PageReference savePageReference = sipInputControllerCtrl.Save();
        PageReference removeHeaderReference = sipInputControllerCtrl.RemoveHeader();
        Test.stopTest();
        //Asserts
        System.assertEquals('/' + testContractObj.Id, cancelPageReference.getUrl());
        System.assertEquals('block', sipInputControllerCtrl.ShowOptionTitle);
        System.assertEquals(true, sipInputControllerCtrl.Umbrella);
        System.assertEquals('/apex/SIPTableSelector?cid=' + testContractObj.Id + '&sipid=' + EncodingUtil.urlEncode(testSIPOptionObj.Id, 'UTF-8'), sipInputControllerCtrl.GoBackLink);
    }
    
    @isTest
    private static void testSIPInputControllerWithDomesticSIPOption() {
        Contract testContractObj = [SELECT Id FROM Contract];
        SIP_Option__c testSIPOptionObj = [SELECT Id, Description__c FROM SIP_Option__c];
        testSIPOptionObj.Description__c = 'Domestic Revenue';
        UPDATE testSIPOptionObj;
        
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'SIP Header Description', null, 2000);
        testSIPHeaderObj.IsTemplate__c = true;
        INSERT testSIPHeaderObj;
        
        SIP_Rebate__c testSipRebateObj = TestDataFactory.createTestSIPRebate(testContractObj.Id, testSIPHeaderObj.Id, 20, 30, 10); 
        INSERT testSipRebateObj;
        
        SIP_Option_Join__c testSIPOptionJoin = TestDataFactory.createTestSIPOptionJoin(testSIPHeaderObj.Id, testSIPOptionObj.Id);
        INSERT testSIPOptionJoin;
        
        Test.startTest();
        SIPInputController sipInputControllerCtrl  = getSIPInputControllerObj(testContractObj, testSIPOptionObj);
        List<SIPInputController.SIPHeaderWrapper> sipHeaderWrapper = sipInputControllerCtrl.GetSIPOptionList();
        PageReference saveAndClosePageReference = sipInputControllerCtrl.SaveAndClose();
        PageReference savePageReference = sipInputControllerCtrl.Save();
        Test.stopTest();
        //Asserts
        System.assertEquals(true, sipInputControllerCtrl.Domestic);
    }
    
    @isTest
    private static void testSIPInputControllerWithInternationalSIPOption() {
        Contract testContractObj = [SELECT Id, Estimated_Short_Haul_Revenue__c, Estimated_Long_Haul_Revenue__c,
                                    Short_Haul_LY__c, Long_Haul_LY__c FROM Contract];
        testContractObj.Estimated_Short_Haul_Revenue__c = 10;
        testContractObj.Estimated_Long_Haul_Revenue__c = 10;
        testContractObj.Short_Haul_LY__c = 10;
        testContractObj.Long_Haul_LY__c = 10;
        UPDATE testContractObj;
        
        SIP_Option__c testSIPOptionObj = [SELECT Id, Description__c FROM SIP_Option__c];
        testSIPOptionObj.Description__c = 'International Revenue';
        UPDATE testSIPOptionObj;
        
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'SIP Header Description', 20, 2);
        testSIPHeaderObj.IsTemplate__c = true;
        INSERT testSIPHeaderObj;
        
        SIP_Rebate__c testSipRebateObj = TestDataFactory.createTestSIPRebate(testContractObj.Id, testSIPHeaderObj.Id, 20, 900, 10); 
        INSERT testSipRebateObj;
        
        SIP_Option_Join__c testSIPOptionJoin = TestDataFactory.createTestSIPOptionJoin(testSIPHeaderObj.Id, testSIPOptionObj.Id);
        INSERT testSIPOptionJoin;
        
        Test.startTest();
        SIPInputController sipInputControllerCtrl  = getSIPInputControllerObj(testContractObj, testSIPOptionObj);
        List<SIPInputController.SIPHeaderWrapper> sipHeaderWrapper = sipInputControllerCtrl.GetSIPOptionList();
        PageReference saveAndClosePageReference = sipInputControllerCtrl.SaveAndClose();
        PageReference savePageReference = sipInputControllerCtrl.Save();
        Test.stopTest();
        //Asserts
        System.assertEquals(true, sipInputControllerCtrl.International);
    }
    
    @isTest
    private static void testSIPInputControllerWithKeyMarketSIPOption() {
        Contract testContractObj = [SELECT Id FROM Contract];
        SIP_Option__c testSIPOptionObj = [SELECT Id, Description__c FROM SIP_Option__c];
        testSIPOptionObj.Description__c = 'Key Market Revenue';
        UPDATE testSIPOptionObj;
        
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'SIP Header Description', null, 2000);
        testSIPHeaderObj.IsTemplate__c = true;
        testSIPHeaderObj.Key_Market_Revenue__c = 10;
        INSERT testSIPHeaderObj;
        
        SIP_Rebate__c testSipRebateObj = TestDataFactory.createTestSIPRebate(testContractObj.Id, testSIPHeaderObj.Id, 20, 30, 10); 
        testSipRebateObj.SIP_Percent__c = 10;
        INSERT testSipRebateObj;
        
        SIP_Option_Join__c testSIPOptionJoin = TestDataFactory.createTestSIPOptionJoin(testSIPHeaderObj.Id, testSIPOptionObj.Id);
        INSERT testSIPOptionJoin;
        
        Test.startTest();
        SIPInputController sipInputControllerCtrl  = getSIPInputControllerObj(testContractObj, testSIPOptionObj);
        List<SIPInputController.SIPHeaderWrapper> sipHeaderWrapper = sipInputControllerCtrl.GetSIPOptionList();
        PageReference saveAndClosePageReference = sipInputControllerCtrl.SaveAndClose();
        PageReference savePageReference = sipInputControllerCtrl.Save();
        Test.stopTest();
        //Asserts
        System.assertEquals(true, sipInputControllerCtrl.KeyMarket);
    }
    
    @isTest
    private static void testSIPInputControllerWithInternationalSIPOptionApexMessage() {
        Contract testContractObj = [SELECT Id, Estimated_Short_Haul_Revenue__c, Estimated_Long_Haul_Revenue__c,
                                    Short_Haul_LY__c, Long_Haul_LY__c FROM Contract];
        testContractObj.Estimated_Short_Haul_Revenue__c = 10;
        testContractObj.Estimated_Long_Haul_Revenue__c = 10;
        testContractObj.Short_Haul_LY__c = 10;
        testContractObj.Long_Haul_LY__c = 10;
        UPDATE testContractObj;
        
        SIP_Option__c testSIPOptionObj = [SELECT Id, Description__c FROM SIP_Option__c];
        testSIPOptionObj.Description__c = 'International Revenue';
        UPDATE testSIPOptionObj;
        
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'SIP Header Description', null, null);
        testSIPHeaderObj.IsTemplate__c = true;
        INSERT testSIPHeaderObj;
        
        SIP_Header__c testSIPHeaderObj1 = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'SIP Header Description', 10, null);
        testSIPHeaderObj1.IsTemplate__c = true;
        INSERT testSIPHeaderObj1;
        
        SIP_Rebate__c testSipRebateObj = TestDataFactory.createTestSIPRebate(testContractObj.Id, testSIPHeaderObj.Id, 20, 10, 10); 
        INSERT testSipRebateObj;
        
        SIP_Option_Join__c testSIPOptionJoin = TestDataFactory.createTestSIPOptionJoin(testSIPHeaderObj.Id, testSIPOptionObj.Id);
        INSERT testSIPOptionJoin;
        
        Test.startTest();
        SIPInputController sipInputControllerCtrl  = getSIPInputControllerObj(testContractObj, testSIPOptionObj);
        List<SIPInputController.SIPHeaderWrapper> sipHeaderWrapper = sipInputControllerCtrl.GetSIPOptionList();
        PageReference saveAndClosePageReference = sipInputControllerCtrl.SaveAndClose();
        PageReference savePageReference = sipInputControllerCtrl.Save();
        Test.stopTest();
        //Asserts
        System.assertEquals(true, sipInputControllerCtrl.International);
        System.assertEquals('SIP Header Description at SIP Header section 1 at Level 1 Upper% must be greater than Lower%', ApexPages.getMessages()[0].getSummary());
        System.assertEquals(ApexPages.SEVERITY.ERROR, ApexPages.getMessages()[0].getSeverity());
    }
    
    @isTest
    private static void testSIPInputControllerWithKeyMarketSIPOption1() {
        Contract testContractObj = [SELECT Id FROM Contract];
        SIP_Option__c testSIPOptionObj = [SELECT Id, Description__c FROM SIP_Option__c];
        testSIPOptionObj.Description__c = 'Key Market Revenue';
        UPDATE testSIPOptionObj;
        
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'Key Market Revenue', null, 2000);
        testSIPHeaderObj.IsTemplate__c = true;
        testSIPHeaderObj.Key_Market_Revenue__c = 10;
        INSERT testSIPHeaderObj;
        
        SIP_Rebate__c testSipRebateObj = TestDataFactory.createTestSIPRebate(testContractObj.Id, testSIPHeaderObj.Id, 20, 30, 10); 
        testSipRebateObj.SIP_Percent__c = 10;
        INSERT testSipRebateObj;
        
        SIP_Option_Join__c testSIPOptionJoin = TestDataFactory.createTestSIPOptionJoin(testSIPHeaderObj.Id, testSIPOptionObj.Id);
        INSERT testSIPOptionJoin;
        
        Test.startTest();
        SIPInputController sipInputControllerCtrl  = getSIPInputControllerObj(testContractObj, testSIPOptionObj);
        List<SIPInputController.SIPHeaderWrapper> sipHeaderWrapper = sipInputControllerCtrl.GetSIPOptionList();
        PageReference saveAndClosePageReference = sipInputControllerCtrl.SaveAndClose();
        PageReference savePageReference = sipInputControllerCtrl.Save();
        Test.stopTest();
        //Asserts
        System.assertEquals(true, sipInputControllerCtrl.KeyMarket);
    }
    
    @isTest
    private static void testSIPInputControllerWithContractIds() {
        Contract testContractObj = [SELECT Id FROM Contract];
        Test.startTest();
        SIPInputController sipInputControllerCtrl  = getSIPInputControllerObj(testContractObj, new SIP_Option__c());
        PageReference checkExistedPageRefernce = sipInputControllerCtrl.CheckExisted();
        PageReference cancelPageReference = sipInputControllerCtrl.Cancel();
        PageReference saveAndClosePageReference = sipInputControllerCtrl.SaveAndClose();
        PageReference goBackPageReference = sipInputControllerCtrl.GoBack();
        Test.stopTest();
        //Asserts
        System.assertEquals('/' + testContractObj.Id, cancelPageReference.getUrl());
        System.assertEquals('block', sipInputControllerCtrl.ShowOptionTitle);
        System.assertEquals('/apex/SIPTableSelector?cid=' + testContractObj.Id, checkExistedPageRefernce.getUrl());
    }
    
    @isTest
    private static void testSIPInputControllerWithSIPHeader() {
        Contract testContractObj = [SELECT Id FROM Contract];
        
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'SIP Header Description', null, 2000);
        testSIPHeaderObj.IsTemplate__c = true;
        INSERT testSIPHeaderObj;
        
        SIP_Rebate__c testSipRebateObj = TestDataFactory.createTestSIPRebate(testContractObj.Id, testSIPHeaderObj.Id, 20, 30, 10); 
        INSERT testSipRebateObj;
        
        Test.startTest();
        SIPInputController sipInputControllerCtrl  = getSIPInputControllerObj(testContractObj, new SIP_Option__c());
        sipInputControllerCtrl.SIPHeaderDelete = new List<SIP_Header__c>{testSIPHeaderObj};
        List<SIPInputController.SIPHeaderWrapper> sipHeaderWrapper = sipInputControllerCtrl.GetExistedSIPHeaderList(new List<SIP_Header__c>{testSIPHeaderObj});
        sipInputControllerCtrl.DeleteHeader();
        Test.stopTest();
        //Asserts
        List<SIP_Header__c> sipHeaders = [SELECT Id FROM SIP_Header__c];
        System.assertEquals(true, sipHeaders.isEmpty());
        System.assertEquals(null, sipInputControllerCtrl.SIPHeaderDelete);
    }
    
    private static SIPInputController getSIPInputControllerObj(Contract testContractObj, SIP_Option__c testSIPOptionObj) {
        PageReference pageRef = Page.SIPTableSelector;
        pageRef.getParameters().put('cid', testContractObj?.id);
        pageRef.getParameters().put('sipid', testSIPOptionObj?.id);
        Test.setCurrentPage(pageRef);
        return new SIPInputController();
    }
}