public class MarketingSpendDownTriggerHandler 
{

     public static void  AggregateMarketingSpenddown (Boolean isDelete, List<Marketing_Spend_Down__c> msdInTrigger, List<Marketing_Spend_Down__c> msdOldInTrigger)
    {
      Set<Id> contractIds  = new Set<Id>();
      List<Contract> lstConUpdate = new List<Contract>();  
        
   if(isDelete)
   {
    for( Marketing_Spend_Down__c msdOld: msdOldInTrigger)
    { 
     if (msdOld.Contract__c !=  NULL && msdOld.Approval_Status__c == 'Funds/Tickets Allocated' )
     {    
     contractIds.add(msdOld.Contract__c);
     } 
    }
   }
   else
   {    
   for( Marketing_Spend_Down__c msddata: msdInTrigger ) 
   { 
     if (msddata.Contract__c !=  NULL &&  msddata.Approval_Status__c == 'Funds/Tickets Allocated')
     {    
      contractIds.add(msddata.Contract__c);
     }    
   } 
  }      
  
        
    List<Contract> conList =  [select id ,Fixed_Amount__c,Marketing_Fund_Amount_Allocated__c
                                from  Contract where id =:contractIds] ;
        
     if (conList.size() > 0)
  {    
      
    AggregateResult[] msddeducted = [ SELECT SUM(Amount_to_be_Deducted__c) dedsum FROM Marketing_Spend_Down__c
                                       where Contract__c  = :contractIds  and Approval_Status__c in ('Funds/Tickets Allocated' )
                                    ];
      
     for(Contract con: conlist)
       { 
           con.Marketing_Fund_Amount_Allocated__c = (decimal)msddeducted[0].get('dedsum')==null?0:(decimal)msddeducted[0].get('dedsum');  
           lstConUpdate.add(con)  ;   
        } 
      
      update  lstConUpdate ; 
          
  }   
        
 }
  
}