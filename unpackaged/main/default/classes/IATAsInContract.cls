/** 
 *  File Name		: IATAsInContract 
 *
 *  Description		: This batch class updates the IATAs in Contract object.  This custom object
 * 					  is used to store all the IATA numbers that apply to a contract.  When a 
 * 					  deal/contract is made with an account, this also applies to all the child
 *					  accounts.  We need to collect all the IATA numbers of the child accounts
 *					  and then link them to each contract.  
 *					    
 *  Copyright		: © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
 *  @author			: Steve Kerr
 *  Date			: 22nd June 2015
 *
 *  Notes			: 
 *
 **/ 

global class IATAsInContract implements Database.Batchable<sObject>, Database.Stateful {

	global String query;
	global String[] emailTo;
	global String errors1;
	global String summary;
	global Map<String, List<Account>> parentAccountMap;
	global Map<String, String> processedAccounts;

	/**
	 * Constructor
	 **/
	global IATAsInContract(){
		errors1 = '';
		summary = '';
		parentAccountMap = getparentAccountMap(); 
		processedAccounts = new Map<String, String>();    
	}

	global Database.querylocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	/**
	 * Process a batch of Accounts
	 **/
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		//List<Contract> contracts = new List<Contract>();

		// Get a list of contracts
		for(sObject s : scope) {
			Contract c = (Contract)s;

			// Check if we have already processed this account
			if(!processedAccounts.containsKey(c.AccountId)){
				processedAccounts.put(c.AccountId, c.AccountId);
				updateIATAs(c.AccountId);
			} 
		}
	}


	/**
	 * This method is only called by the batch constructor.  It queries all
	 * Industry accounts and builds a map of parendID to list of child 
	 * accounts.  This map is then used when exploring the account hierarchy.
	 *  
	 * The reason this data needs to be cached like this, and not looked up by
	 * SOQL as required, is that some account hierarchies are over 500 deep.  
	 * This means that if you run SOQL for all the children (one at a time) you
	 * will exceed the 200 SOQL queries governor limit – even running as a
	 * batch.
	 *
	 * Note: We don't include accounts with no parent.
	 *
	 **/
	private Map<String, List<Account>> getparentAccountMap(){

		Map<String, List<Account>> parentToAccountList = new Map<String, List<Account>>();

		// Get all accounts with a parent
		List<Account> childAccs = [
		                           SELECT Id,Name,ParentId 
		                           FROM Account 
		                           Where Account_Lifecycle__c <> 'Inactive'
		                           and ParentId <> null
		                           ORDER BY ParentId ASC];

		// Build the map
		String currentParentId = 'firstpass';
		List<Account> currentChildAccs;
		for(Account accChild : childAccs) {

			// Check if there are more children for the current parent.
			if(currentParentId.equals(accChild.ParentId)){
				currentChildAccs.add(accChild);
			} else {
				// we have moved onto a new parent
				currentParentId = accChild.ParentId;
				currentChildAccs = new List<Account>();
				currentChildAccs.add(accChild);
				parentToAccountList.put(currentParentId, currentChildAccs);
			}
		}
		return parentToAccountList;
	} 


	/**
	 * Processes an account
	 *
	 * Returns and erros as a string
	 *
	 **/
	private void updateIATAs(String accId) {

		// Fetch the account object	
		Account acc = [Select Name from Account where id = :accId];

		// Info
		summary += '\n\nAccount:  ' + acc.Name;

		// Get a list of all children 
		List<Account> childAccs = new List<Account>();
		getChildAccounts(acc, childAccs);

		// Info
		integer noChildren = childAccs.size() - 1;
		summary +=  '\n' + noChildren + ', Child Accounts:'; 

		// Get a list of IATA numbers that relate to these accounts
		List<IATA_Number__c> IATAs = getRelatedIATAs(childAccs);

		// Add/remove IATAs in contract, based of the discovered list of related IATAs
		correctIATAsInContract(acc, IATAs);
	}


	/**
	 * Recursively follows the account hierarchy down.
	 * The end result is that the list "accs" gets filled with all children.
	 **/
	private void getChildAccounts(Account acc, List<Account> accs){
		//Add self to the list
		accs.add(acc);

		// Get a list of children
		//List<Account> childAccs = [select Name from Account where ParentId = :acc.id and Account_Lifecycle__c <> 'Inactive'];
		//parentAccountMap
		List<Account> childAccs = new List<Account>();
		if(parentAccountMap.containsKey(acc.id)){
			childAccs = parentAccountMap.get(acc.id);
		} 

		if(childAccs.size() == 0){
			// Do nothing, we are already added
		} else {
			for(Account accChild : childAccs) {
				getChildAccounts(accChild, accs);
			}			
		}
	}


	/**
	 * Take a list of accounts as input.  
	 * Returns a list of related IATAs
	 *
	 * Note: Data validity is also taken into account.
	 **/
	private List<IATA_Number__c> getRelatedIATAs(List<Account> accs) {

		// Make a list of Account Ids - from the incoming list of accounts
		List<id> accIds = new List<id>();
		for(Account acc : accs) {
			accIds.add(acc.id);
		}

		// Get the base list of IATAs - that relate to the incoming list of Accounts
		List<IATA_Number__c> IAIAs = [
		                              SELECT Id, IATA_Number__c, Account__c
		                              FROM IATA_Number__c 
		                              where Account__c in: accIds 
		                              and IATA_Number__c <> null];

		// Info                              
		summary += '\n' + IAIAs.size() + ', Related IATAs: ';

		// Make a list of IATA Ids - we will use this to search for soft-deleted IATAs
		List<id> IATA_Ids = new List<id>();
		for(IATA_Number__c IATA : IAIAs) {
			IATA_Ids.add(IATA.id);
		}

		// Check if any of these IATAs have been soft-deleted
		List<Data_Validity__c> IAIAsSoftDeleted = [
		                                           SELECT From_Account__c,From_Date__c,IATA__c,Name,Soft_Delete__c,To_Date__c 
		                                           FROM Data_Validity__c
		                                           where IATA__c in : IATA_Ids
		                                           and RecordType.Name like '%IATA Validity%'
		                                           and Soft_Delete__c = true];

		// Info                              
		summary += '\n' + IAIAsSoftDeleted.size() + ', IATAs (soft deleted): ';

		// Build a map of the soft deleted IATAs (IDs)
		Map<String, Data_Validity__c> IATA_Ids_softDeleted = new Map<String, Data_Validity__c>();
		for(Data_Validity__c delIATA : IAIAsSoftDeleted) {
			IATA_Ids_softDeleted.put(delIATA.IATA__c, delIATA);
		}

		// make a new list - without the soft-deleted IATAs
		List<IATA_Number__c> IAIAs2 = new List<IATA_Number__c>(); 
		for(IATA_Number__c IATA : IAIAs) {
			if(!IATA_Ids_softDeleted.containsKey(IATA.id)){
				IAIAs2.add(IATA);
			}
		}

		// Return the filtered list
		return IAIAs2;
	}

	/**
	 * Add/remove as required, based on the incoming list.
	 **/
	private void correctIATAsInContract(Account acc, List<IATA_Number__c> relatedIATAs){

		// Build a map of IATA numbers to their parent Accounts
		Map<String, IATA_Number__c> IATAsExistingMap1 = new Map<String, IATA_Number__c>();
		for(IATA_Number__c IATA_incoming : relatedIATAs) {
			IATAsExistingMap1.put(IATA_incoming.IATA_Number__c, IATA_incoming);
		}

		// Get a list of current contracts
		List<Contract> contracts = [
		                            SELECT AccountId, StartDate, EndDate, Status, ContractNumber
		                            FROM Contract
		                            where StartDate >= LAST_YEAR 
		                            and AccountId = :acc.id
		                            and Status <> 'Deactivated'
		                            ];
                          
		summary += '\nContracts processed:';

		/* For each contract, correct the list of IATAs */
		for(Contract contract : contracts) {

			// Sometimes this fails due to missing data 
			try{
				summary += '\n      ' + contract.ContractNumber + ',  ' +  contract.StartDate.format() + ' -> ' + contract.EndDate.format() + ', ' + contract.Status;
			}catch(Exception e){
				summary += '\n ** ERROR: Missing contract data **';
			}

			List<IATAs_in_Contract__c> IATAsToDelete =  new List<IATAs_in_Contract__c>();
			List<IATAs_in_Contract__c> IATAsToAdd =  new List<IATAs_in_Contract__c>();

			// Get a set of the existing IATA numbers attached to the contract
			List<IATAs_in_Contract__c> IATAsExisting = [
			                                            SELECT Id, Contract__c,Name, IATA_Travel_Agent__c 
			                                            FROM IATAs_in_Contract__c
			                                            where Contract__c = :contract.id];

			// Build a map of the existing IATAs, if a duplicate is found, delete it
			Map<String, IATAs_in_Contract__c> IATAsExistingMap = new Map<String, IATAs_in_Contract__c>();
			for(IATAs_in_Contract__c IAIA : IATAsExisting) {
				// Check if this IATA is still current
				if(!IATAsExistingMap1.containsKey(IAIA.Name)){
					IATAsToDelete.add(IAIA);
				} else {
					// Check if we already have this IATA
					if(IATAsExistingMap.containsKey(IAIA.Name)){
						IATAsToDelete.add(IAIA);
					} else {
						IATAsExistingMap.put(IAIA.Name, IAIA);
					}
				}
			}		

			// Update the exist list of IATAs with the new filtered version
			IATAsExisting = IATAsExistingMap.values();

			// Update the existing IATAs in Contract
			for(IATAs_in_Contract__c IAIA : IATAsExisting) {

				//IAIA.Exception__c = '';
				IAIA.Contract__c = contract.id;

				// update the patent account (travel agent)
				if(IATAsExistingMap1.containsKey(IAIA.Name)){
					IAIA.IATA_Travel_Agent__c = (Id)IATAsExistingMap1.get(IAIA.Name).Account__c ;
				}
			}

			// Add in the missing ones
			for(IATA_Number__c IATA_incoming : relatedIATAs) {
				if(!IATAsExistingMap.containsKey(IATA_incoming.IATA_Number__c)){
					IATAs_in_Contract__c IATAInContract = new IATAs_in_Contract__c();
					IATAInContract.Name = IATA_incoming.IATA_Number__c;
					IATAInContract.Contract__c = contract.id;
					IATAInContract.IATA_Travel_Agent__c = (Id)IATA_incoming.Account__c ;
					IATAsToAdd.add(IATAInContract);
				}
			}	


			/* Process the updates and inserts */

			// Update existing IATAs
			try{
				update IATAsExisting;
			}catch(Exception e){
				errors1 += '\nERROR updating Account:' + acc.Name;
			}

			// add new IATAs
			try{
				insert IATAsToAdd;
			}catch(Exception e){
				errors1 += '\nERROR adding new IATA for Account:' + acc.Name;
			}

			// Delete duplicate IATAs
			try{
				delete  IATAsToDelete;
			}catch(Exception e){
				errors1 += '\nERROR deleating duplicates for Account:' + acc.Name;
			}
		}
	}

	global void finish(Database.BatchableContext BC) {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(emailTo);
		mail.setReplyTo('salesforce.admin@virginaustralia.com');
		mail.setSenderDisplayName('Batch Processing');
		mail.setSubject('IATA in contract - Batch Process Completed');
		mail.setPlainTextBody(
				'Batch Process has completed' + '\n\n' +
				'Errors (if any):\n' +
				errors1 + '\n\n' + 
				'Processing output:\n' + summary + 
				'\n\n' 
				);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}