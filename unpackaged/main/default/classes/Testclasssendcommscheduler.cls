/*
**Created By: Naveen V(naveenkumar.vankadhara@virginaustralia.com)
**Created Date: 19.05.2022
**Description: Test Class for SendCommsToAlmsSchedular
*/
@IsTest
public class Testclasssendcommscheduler {
    
    @isTest
    public static void testcommsschedular()
    {
        String CRON_EXP = '0 0 13 ? * FRI';
        
        Test.startTest();
        
        String jobId = System.schedule('Send Comms Schedular New', CRON_EXP, new SendCommsToAlmsSchedular());
        
        Test.stopTest();
    }
}