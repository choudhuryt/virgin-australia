/*
* 	Updated by : Cloudwerx
*	Removed hardcoded references for owner/user ID used in the code below by querying the required 	
* 	user record ID
* 
* 
*/


global class AccelerateAccountBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    
    // Sets the number of Pilot Gold VSMs a new auto-created Accelerate contract will get.
    //private static Integer ACCELERATE_PILOT_GOLD = 2;
    private static Integer ACCELERATE_PILOT_GOLD = integer.valueOf(Label.ACCELERATE_PILOT_GOLD_COUNT);
    Id accountId = null;
    
    global AccelerateAccountBatch() {
    }
    
    global AccelerateAccountBatch(Id accountId) {
        this.accountId = accountId;
    }
    
    global Integer howManyrecordsUpdated = 0;
    String CreditType;
    String BusinessActivity;
    
    //Loading and running the query string
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Id ownerId = System.Label.Virgin_Australia_Business_Flyer_User_Id;
        String gstrQuery = 'SELECT Name,Id,Business_Number__c,InteflowIndustryCode__c,Business_Activity__c,Website,BillingState,Account_Manager_Email_Address__c,Account_Lifecycle__c,Sales_Support_Group__c,Sales_Matrix_Owner__c,OwnerId,Account_Owner__c,Velocity_Pilot_Gold_Available__c,Lounge_Last_Name__c,BillingStreet,BillingCity,Lounge_First_Name__c,Lounge_Email__c,BillingPostalCode,Pilot_Gold_Position__c,Pilot_Gold_First_Name__c,Pilot_Gold_Velocity_Number__c,Pilot_Gold_Email__c,Pilot_Gold_Last_Name__c,Pilot_Gold_Position_Second_Nominee__c,Pilot_Gold_First_Name_Second_Nominee__c,Pilot_Gold_last_Name_Second_Nominee__c,Pilot_Gold_Velocity_Gold_Second_Nominee__c,Pilot_Gold_Email_Second_Nominee__c FROM Account WHERE Accelerate_Batch_Update__c = FALSE AND Account_Lifecycle__c  <> \'Inactive\'  AND   OwnerId =:ownerId  AND Market_Segment__c IN (\'Accelerate\' , \'NZ Accelerate\') AND  (Potential_Duplicate__c = FALSE OR (Potential_Duplicate__c = TRUE AND Force_Processing__c = TRUE))';
        gstrQuery = (this.accountId != null)? gstrQuery + ' AND Id = \'' + accountId + '\'': gstrQuery;
        return Database.getQueryLocator(gstrQuery);
    }
    
    //Running The execute Method
    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        Set<String> accountIds = new Set<String>();
        Set<String> contractIds = new Set<String>();
        List <String> emailAccountNumbers = new List<String>() ;
        List<Account> accountList = new List <Account>();
        String accountname;
        
        User OwnerRec = [SELECT Id, Name FROM User WHERE FirstName = 'Virgin Australia' AND 
                         LastName = 'Business Flyer'];
        
        for (SObject s : scope) {
            Account a = (Account) s;
            accountList.add(a);
            
        }
        
        List<Account> accountListToUpdate = new List<Account>();
        for (Integer x = 0; x < accountList.size(); x++) {
            Account account = new Account();
            account = accountList.get(x);
            Contact keyContact = new Contact();
            accountname = account.Name;                       
            
            try {
                keyContact = (Contact) [SELECT Id,Key_Contact__c,Email,Subscriptions__c,Name,Title,Phone,FirstName,LastName FROM Contact WHERE AccountId = :account.Id LIMIT 1];
                keyContact.Key_Contact__c = true;
                keyContact.RecordTypeId = Utilities.getRecordTypeId('Contact', 'Contact');                    
                //keyContact.OwnerId = '00590000000LbNz';
                keyContact.OwnerId = OwnerRec.Id;
            } catch (Exception contactException) {
                VirginsutilitiesClass.sendEmailError(contactException);
            }
            
            if (account.Website == null) {
                account.Website = 'no';
            }
            if (account.Account_Manager_Email_Address__c != null)
                
            {
                CreditType = 'TMCGDS' ;
                
            } else {
                CreditType = 'DIRECTB2B' ;
                
            }
            
            if (account.Business_Activity__c != null) {
                BusinessActivity = account.Business_Activity__c.replaceAll('&', '&amp;') ;
            } else {
                BusinessActivity = '  ';
            }
            
            if (accountname != null && account.Name.contains('&')) {
                accountname = account.Name.replaceAll('&', '&amp;') ;
            }
            if (keyContact.Title != null && keyContact.Title.contains('&')) {
                keyContact.Title = keyContact.Title.replaceAll('&', '&amp;') ;
            }
            if (account.BillingStreet != null && account.BillingStreet.contains('&')) {
                account.BillingStreet = account.BillingStreet.replaceAll('&', '&amp;') ;
            }
            if (account.Website != null && account.Website.contains('&')) {
                account.Website = account.Website.replaceAll('&', '&amp;') ;
            }
            
            try {
                update keyContact;
            } catch (Exception e) {
                //VirginsutilitiesClass.sendEmailError(e);
            }
            //update Subscription
            try {
                Subscriptions__c tempSub = new Subscriptions__c();
                tempSub.Contact_ID__c = keyContact.Id;
                tempSub.Contact_ID__c = tempSub.Contact_ID__c.substring(0, Math.min(tempSub.Contact_ID__c.length(), 15));
                
                Subscriptions__c subscription = new Subscriptions__c();
                try {
                    subscription = (Subscriptions__c) [SELECT Id,Contact_ID__c,Business_News__c,Accelerate_EDM__c FROM Subscriptions__c WHERE Contact_ID__c = :tempSub.Contact_ID__c];
                } catch (Exception se) {
                    VirginsutilitiesClass.sendEmailError(se);
                }
                //subscription.Business_News__c =true;
                subscription.Accelerate_EDM__c = true;
                
                update subscription;
            } catch (Exception e) {
                VirginsutilitiesClass.sendEmailError(e);
            }
            //Adding a task for Sales support adding a Lounge Scheme Cordinator 
            account.Sales_Matrix_Owner__c = 'Accelerate';
            //account.OwnerId = '00590000000LbNz';
            account.OwnerId = OwnerRec.Id;
            //account.Account_Owner__c = '00590000000LbNz';
            account.Account_Owner__c = OwnerRec.Id;
            account.Velocity_Pilot_Gold_Available__c = 2;
            //account.Sales_Support_Group__c = '00590000000LbNz';
            account.Sales_Support_Group__c = OwnerRec.Id;
            account.RecordTypeId = Utilities.getRecordTypeId('Account', 'Accelerate');                
            if (account.Business_Number__c.length() == 13) {
                account.Billing_Country_Code__c = 'NZ';
                account.Market_Segment__c = 'NZ Accelerate';
            } else {
                account.Billing_Country_Code__c = 'AU';
                account.Market_Segment__c = 'Accelerate';
            }
            account.Accelerate_Batch_Update__c = true;
            //account.Industry_Type__c = account.Industry;
            account.Accelerate_Key_Contact__c = keyContact.FirstName + ' ' + keyContact.LastName;
            account.Accelerate_Key_Contact_First_Name__c = keyContact.FirstName ;
            account.Accelerate_Key_Contact_Last_Name__c = keyContact.LastName;
            account.Accelerate_Key_Contact_Position__c = keyContact.Title;
            account.Accelerate_Key_Contact_Phone__c = keyContact.Phone;
            account.Accelerate_Key_Contact_Email__c = keyContact.Email;
            // Decomm Inteflow Integration, Account_Lifecycle__c='Contract'
            if (account.Account_Lifecycle__c == 'Opportunity') {
                accountIds.add(account.Id);
            }
            
            try {
                update account;
                emailAccountNumbers.add(account.Name);
                
            } catch (Exception e) {
                //VirginsutilitiesClass.sendEmailError(e);
            }
            
            if (account.Lounge__c = true) {
                
                if (keyContact.Email <> account.Lounge_Email__c) {
                    Contact loungeContact = new Contact();
                    loungeContact.FirstName = account.Lounge_First_Name__c ;
                    loungeContact.LastName = account.Lounge_Last_Name__c;
                    loungeContact.Email = account.Lounge_Email__c;
                    loungeContact.AccountId = account.Id;
                    loungeContact.MailingStreet = account.BillingStreet;
                    loungeContact.MailingCity = account.BillingCity;
                    loungeContact.MailingPostalCode = account.BillingPostalCode;
                    loungeContact.MailingState = account.BillingState;
                    //loungeContact.OwnerId = '00590000000LbNz';
                    loungeContact.OwnerId = OwnerRec.Id;
                    try {
                        insert loungeContact;
                    } catch (Exception e) {
                        //VirginsutilitiesClass.sendEmailError(e);
                    }
                }
                
            }
            
            //Do more stuff here.....            
            Contract contract = new Contract();
            contract.AccountId = account.Id;
            if (account.Business_Number__c.length() == 13) {
                contract.RecordTypeId = Utilities.getRecordTypeId('Contract', 'NZ_Accelerate_POS_Rebate');                
            } else {
                contract.RecordTypeId = Utilities.getRecordTypeId('Contract', 'Accelerate_POS_Rebate');                    
            }
            // Decomm Inteflow Integration, contract.Status = 'Activated'
            // Checking for existing active contract
            Id contractId;
            List<contract> existingContract = [SELECT id FROM contract WHERE AccountId =:account.Id AND Status = 'Activated' LIMIT 1];
            if(existingContract.isEmpty()) {                
                contract.Status = 'Draft';
                contract.Type__c = 'Rebate';
                contract.Contracting_Entity__c = 'Virgin Australia';
                contract.Sales_Basis__c = 'Flown';
                //contract.OwnerId = '00590000000LbNz';
                contract.OwnerId = OwnerRec.Id;
                contract.StartDate = Date.today();
                //pilot gold
                contract.Pilot_Gold__c = ACCELERATE_PILOT_GOLD;
                insert contract;
                
                contractId = contract.Id;
            }
            else{
                if (existingContract.size()>0){
                    contractId = existingContract[0].id;
                } 
            }
            contractIds.add(contractId);
            
            List<SIP_Rebate__c> sipRebateList = new List<SIP_Rebate__c>();
            
            SIP_Header__c sipheader = new SIP_Header__c();
            sipheader.Contract__c = contractId;
            sipheader.Description__c = 'Accelerate - All Revenue';
            insert sipheader;
            
            SIP_Rebate__c sipRebateLevel1 = new SIP_Rebate__c();
            sipRebateLevel1.SIP_Header__c = sipheader.Id;
            sipRebateLevel1.Lower_Percent__c = 2;
            sipRebateLevel1.Upper_Percent__c = 2;
            sipRebateLevel1.SIP_Percent__c = 2;
            
            sipRebateList.add(sipRebateLevel1);
            
            SIP_Rebate__c sipRebateLevel2 = new SIP_Rebate__c();
            sipRebateLevel2.SIP_Header__c = sipheader.Id;
            sipRebateLevel2.Lower_Percent__c = 3;
            sipRebateLevel2.Upper_Percent__c = 3;
            sipRebateLevel2.SIP_Percent__c = 3;
            sipRebateList.add(sipRebateLevel2);
            
            SIP_Rebate__c sipRebateLevel3 = new SIP_Rebate__c();
            sipRebateLevel3.SIP_Header__c = sipheader.Id;
            sipRebateLevel3.Lower_Percent__c = 3.5;
            sipRebateLevel3.Upper_Percent__c = 3.5;
            sipRebateLevel3.SIP_Percent__c = 3.5;
            sipRebateList.add(sipRebateLevel3);
            
            SIP_Rebate__c sipRebateLevel4 = new SIP_Rebate__c();
            sipRebateLevel4.SIP_Header__c = sipheader.Id;
            sipRebateLevel4.Lower_Percent__c = 4;
            sipRebateLevel4.Upper_Percent__c = 4;
            sipRebateLevel4.SIP_Percent__c = 4;
            sipRebateList.add(sipRebateLevel4);
            
            SIP_Rebate__c sipRebateLevel5 = new SIP_Rebate__c();
            sipRebateLevel5.SIP_Header__c = sipheader.Id;
            sipRebateLevel5.Lower_Percent__c = 4.5;
            sipRebateLevel5.Upper_Percent__c = 4.5;
            sipRebateLevel5.SIP_Percent__c = 4.5;
            sipRebateList.add(sipRebateLevel5);
            
            
            SIP_Rebate__c sipRebateLevel6 = new SIP_Rebate__c();
            sipRebateLevel6.SIP_Header__c = sipheader.Id;
            sipRebateLevel6.Lower_Percent__c = 5;
            sipRebateLevel6.Upper_Percent__c = 5;
            sipRebateLevel6.SIP_Percent__c = 5;
            sipRebateList.add(sipRebateLevel6);
            
            if (sipRebateList.size() > 0) {
                insert sipRebateList;
            }
            
            
            //if(account.Pilot_Gold_Email__c != null && !account.Pilot_Gold_Email__c.equals('')){
            if (account.Pilot_Gold_Email__c != null) {
                Velocity_Status_Match__c vsm = new Velocity_Status_Match__c();
                vsm.Account__c = account.Id;
                vsm.Approval_Status__c = 'Draft';
                vsm.Date_Requested__c = Date.today();
                vsm.Delivery_To__c = 'Member';
                vsm.Position_in_Company__c = account.Pilot_Gold_Position__c;
                vsm.Passenger_Name__c = account.Pilot_Gold_First_Name__c + ' ' + account.Pilot_Gold_Last_Name__c;
                vsm.Passenger_Velocity_Number__c = account.Pilot_Gold_Velocity_Number__c;
                vsm.Passenger_Email__c = account.Pilot_Gold_Email__c;
                vsm.Within_Contract__c = 'Yes';
                vsm.Contract__c = contractId;
                vsm.Status_Match_or_Upgrade__c = 'Pilot Gold';
                vsm.Justification__c = 'Within Contract';
                try {
                    insert vsm;
                } catch (Exception e) {
                    //VirginsutilitiesClass.sendEmailError(e);
                }
            }
            //}
            //if(account.Pilot_Gold_Email_Second_Nominee__c != null && !account.Pilot_Gold_Email_Second_Nominee__c.equals('') ){
            if (account.Pilot_Gold_Email_Second_Nominee__c != null) {
                Velocity_Status_Match__c vsm1 = new Velocity_Status_Match__c();
                vsm1.Account__c = account.Id;
                vsm1.Approval_Status__c = 'Draft';
                vsm1.Date_Requested__c = Date.today();
                vsm1.Delivery_To__c = 'Member';
                vsm1.Position_in_Company__c = account.Pilot_Gold_Position_Second_Nominee__c;
                vsm1.Passenger_Name__c = account.Pilot_Gold_First_Name_Second_Nominee__c + ' ' + account.Pilot_Gold_last_Name_Second_Nominee__c;
                vsm1.Passenger_Velocity_Number__c = account.Pilot_Gold_Velocity_Gold_Second_Nominee__c;
                vsm1.Passenger_Email__c = account.Pilot_Gold_Email_Second_Nominee__c;
                vsm1.Within_Contract__c = 'Yes';
                vsm1.Contract__c = contractId;
                vsm1.Justification__c = 'Within Contract';
                vsm1.Status_Match_or_Upgrade__c = 'Pilot Gold';
                try {
                    insert vsm1;
                } catch (Exception e) {
                    //VirginsutilitiesClass.sendEmailError(e);
                }
            }
            //}
            //  }   
            
            
        }
        Messaging.SingleEmailMessage mailfinish = new Messaging.SingleEmailMessage();
        mailfinish.setToAddresses(new String[]{
            'salesforce.admin@virginaustralia.com'
                });
        mailfinish.setReplyTo('batch@VirginAustralia.com');
        mailfinish.setSenderDisplayName('VirginAustraliaAccelerate');
        mailfinish.setSubject('Accelerate Account batch has finished');
        mailfinish.setPlainTextBody('Hi Accelerate Team,\n\nThe new accounts recieved via the application form are as follows \n\n' + emailAccountNumbers);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
            mailfinish
                });
        
        System.enqueueJob(new AsyncActivateAccelerateAccount(accountIds, contractIds));
        
    }
    
    //Finishing the batch  (Or though this is not executiong code this methos needs to exsist for the batch class to run!!)  
    global void finish(Database.BatchableContext BC) {
        
        
    }
    
}