/**
* @description       : Test class for MakeVelocityCallout
* @UpdatedBy         : CloudWerx
**/
@isTest
public with sharing class TestMakeVelocityCallout {
    
    private static String velocityNumber = 'L111345678';
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('FlyPlus', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        testContactObj.Velocity_Number__c = velocityNumber;
        INSERT testContactObj;
        
        Apex_Callouts__c testApexCalloutObj = TestDataFactory.createTestApexCallouts('VelocityDetails', 'https://services-mssl.virginaustralia.com/service/partner/salesforce/1.0/SalesforceLoyalty', 90000, 'client_mssl_virginaustralia2018_com');
        INSERT testApexCalloutObj;
    }
    
    @isTest
    private static void testMakeVelocityCalloutStatusRed() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new VelocitySnapshot_Test.WebServiceMockImplR());
        Test.startTest();
        MakeVelocityCallout.apexcallout(contactId, velocityNumber);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, Velocity_Status__c FROM Contact LIMIT 1];
        system.assertEquals('Red', updatedContact.Velocity_Status__c);
    }
    
    @isTest
    private static void testMakeVelocityCalloutVelocityStatusSilver() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new VelocitySnapshot_Test.WebServiceMockImplS());
        Test.startTest();
        MakeVelocityCallout.apexcallout(contactId, velocityNumber);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, Velocity_Status__c FROM Contact LIMIT 1];
        system.assertEquals('Silver', updatedContact.Velocity_Status__c);
    }
    
    @isTest
    private static void testVelocityApexCalloutContactVelocityStatusGold() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new VelocitySnapshot_Test.WebServiceMockImplG());
        Test.startTest();
        MakeVelocityCallout.apexcallout(contactId, velocityNumber);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, Velocity_Status__c FROM Contact LIMIT 1];
        system.assertEquals('Gold', updatedContact.Velocity_Status__c);
    }
    
    @isTest
    private static void testMakeVelocityCalloutVelocityStatusVIP() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new VelocitySnapshot_Test.WebServiceMockImplV());
        Test.startTest();
        MakeVelocityCallout.apexcallout(contactId, velocityNumber);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, Velocity_Status__c FROM Contact LIMIT 1];
        system.assertEquals('VIP', updatedContact.Velocity_Status__c);
    }
    
    @isTest
    private static void testMakeVelocityCalloutVelocityStatusPlatinum() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new VelocitySnapshot_Test.WebServiceMockImplP());
        Test.startTest();
        MakeVelocityCallout.apexcallout(contactId, velocityNumber);
        Test.stopTest();
        Contact updatedContact = [SELECT Id, Velocity_Status__c FROM Contact LIMIT 1];
        system.assertEquals('Platinum', updatedContact.Velocity_Status__c);
    }
    
    @isTest
    private static void testMakeVelocityCalloutException() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Test.setMock(WebServiceMock.class, new TestVelocityApexCallOut.WebServiceMockImplException());
        Test.startTest();
        MakeVelocityCallout.apexcallout(contactId, velocityNumber);   
        Test.stopTest();
    }
}