/**
* @description       : Test class for UpdateRelatedTravelAgent
* @createdBy         : Cloudwerx
* @Updated By        : Cloudwerx
**/
@isTest
private class TestUpdateRelatedTravelAgent {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Account testAccountObj1 = TestDataFactory.createTestAccount('Related Travel Agent', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj1;
        
        Case testCaseObj = TestDataFactory.createTestCase(testAccountObj.Id, 'Velocity Case', '2100865670', 'movementType', UserInfo.getUserId(), 'newMarketSegment', 'ACT', Date.today(), 'Both');
        testCaseObj.IATA_From_Web__c = '0000795';
        INSERT testCaseObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('contract', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
    }
    
    @isTest
    private static void testUpdateRelatedTravelAgentIATA() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Id casesId = [SELECT Id FROM Case LIMIT 1]?.Id;
        Id contractId = [SELECT Id FROM Contract LIMIT 1]?.Id;
        
        IATA_Number__c testIATANumberObj = TestDataFactory.createTestIATANumber(accountId, contractId, '0000795', 'Yes');
        INSERT testIATANumberObj;
        
        Data_Validity__c testDataValidityObj = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false); 
        testDataValidityObj.RecordTypeId = Utilities.getRecordTypeId('Data_Validity__c', 'IATA_Validity');
        testDataValidityObj.IATA__c = testIATANumberObj.Id;
        INSERT testDataValidityObj;
        
        Test.startTest();
        UpdateRelatedTravelAgent.UpdateRelatedAccount(new List<Id>{casesId});
        Test.stopTest();
        // Asserts
        case updatedCaseObj = [SELECT Id, Related_Travel_Agent_Account__c FROM Case
                               WHERE Id =:casesId];
        // Asserts
        System.assertEquals(accountId, updatedCaseObj.Related_Travel_Agent_Account__c);
    }
    
    @isTest
    private static void testUpdateRelatedTravelAgentPCC() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Case testCaseObj = [SELECT Id, PCC_From_Web__c FROM Case LIMIT 1];
        Id contractId = [SELECT Id FROM Contract LIMIT 1]?.Id;
        
        PCC__c testPCCObj = TestDataFactory.createTestPCC('Test PCC', accountId, contractId, null, null, 'Yes', Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), 'Galileo');
        INSERT testPCCObj;
        
        testCaseObj.PCC_From_Web__c = testPCCObj.Name;
        UPDATE testCaseObj;
        
        Data_Validity__c testDataValidityObj = TestDataFactory.createTestDataValidity(accountId, Date.newInstance(2016, 01, 01), Date.newInstance(2016, 11, 31), false); 
        testDataValidityObj.RecordTypeId = Utilities.getRecordTypeId('Data_Validity__c', 'PCC');
        testDataValidityObj.PCC__c = testPCCObj.Id;
        INSERT testDataValidityObj;
        
        Test.startTest();
        UpdateRelatedTravelAgent.UpdateRelatedAccount(new List<Id>{testCaseObj.Id});
        Test.stopTest();
        // Asserts
        case updatedCaseObj = [SELECT Id, Related_Travel_Agent_Account__c FROM Case
                               WHERE Id =:testCaseObj.Id];
        // Asserts
        System.assertEquals(accountId, updatedCaseObj.Related_Travel_Agent_Account__c);
    }
}