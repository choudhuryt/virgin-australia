/**
 * @description       : Test class for NewVersionController
 * @UpdateBy          : Cloudwerx
**/
@isTest
private class TestNewVersionController  {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('Account Closed', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        testContractObj.Status = 'Activated';
        UPDATE testContractObj;
    }
    
    @isTest
    private static void testDraftedContract(){
        DELETE [SELECT Id FROM Contract];
        Account testAccountObj = [SELECT Id FROM Account LIMIT 1];
        // Switch to test context
        Test.startTest();
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        // setup a reference to the page the controller is expecting with the parameters
        PageReference pref = Page.New_Version_Page;
        pref.getParameters().put('contractId', testContractObj.id);
        Test.setCurrentPage(pref);
        // Construct the controllers
        ApexPages.StandardController con = new ApexPages.StandardController(testContractObj);
        NewVersionController nvc = new NewVersionController(con);
        nvc.actionButtonYes();
        nvc.actionButtonNo();
        Contract oldContract = nvc.getOldContract();   
        Test.stopTest();
        //Asserts
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
    }
    
    @isTest
    private static void testActivatedContract(){
        Contract testContractObj = [SELECT Id, Status FROM Contract];
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'SIP Header Description', 1000, 2000);
        INSERT testSIPHeaderObj;
        
        SIP_Header__c testSIPHeaderObj1 = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'SIP Header Description', 1000, 2000);
        testSIPHeaderObj1.SIP_Header_Parent__c = testSIPHeaderObj.Id;
        INSERT testSIPHeaderObj1;
        
        EY_Discount_Africa__c testEYDiscountAfricaObj = TestDataFactory.createTestEYDiscountAfrica(testContractObj.Id, '1', false, '13.2');
        INSERT testEYDiscountAfricaObj;
        
        EY_Discount_Middle_East__c testEYDiscountMiddleEastObj = TestDataFactory.createTestEYDiscountMiddleEast(testContractObj.Id, '1', false, '13.2');
        INSERT testEYDiscountMiddleEastObj;
        
        EY_Discount__c testEYDiscountObj = TestDataFactory.createTestEYDiscount(testContractObj.Id, '1', false, '13.2');
        INSERT testEYDiscountObj;
        
        International_Discounts_USA_Canada__c testInternationalDiscountsUSACanadaObj = TestDataFactory.createTestInternationalDiscountsUSACanada(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsUSACanadaObj;
        
        International_Discounts_UK_EUROPE__c testInternationalDiscountsUKEUROPEObj = TestDataFactory.createTestInternationalDiscountsUKEUROPE(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsUKEUROPEObj;
        
        International_Discounts_Middle_East__c testInternationalDiscountsMiddleEastObj = TestDataFactory.createTestInternationalDiscountsMiddleEast(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsMiddleEastObj;
        
        International_Discounts_Asia__c testInternationalDiscountsAsiaObj = TestDataFactory.createTestInternationalDiscountsAsia(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsAsiaObj;
        
        International_Discounts_AUS_NZ__c testInternationalDiscountsAUSNZObj = TestDataFactory.createTestInternationalDiscountsAUSNZ(testContractObj.Id);
        INSERT testInternationalDiscountsAUSNZObj;
        
        International_Discounts_HK__c testInternationalDiscountsHKObj = TestDataFactory.createTestInternationalDiscountsHK(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsHKObj;
        
        Trans_Tasman_Discounts__c testTransTasmanDiscountsObj = TestDataFactory.createTestTransTasmanDiscounts(testContractObj.Id, '1', false, '13.2');
        INSERT testTransTasmanDiscountsObj;
        
        Intra_WA_Discounts__c testIntraWADiscountsObj = TestDataFactory.createTestIntraWADiscounts(testContractObj.Id, '1', false, '13.2');
        INSERT testIntraWADiscountsObj;
        
        International_Disc_SH__c testInternationalDiscSHObj = TestDataFactory.createTestInternationalDiscSH(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscSHObj;
        
        Domestic_Discounts__c testDomesticDoscountsObj = TestDataFactory.createTestDomesticDiscounts(testContractObj.Id, '1', false, '13.2');
        INSERT testDomesticDoscountsObj;
        
        International_City_Pairs__c testInternationalCityPairsObj = TestDataFactory.createTestInternationalCityPairs(testContractObj.Id, 'AAN;BSR', 'Africa');
        INSERT testInternationalCityPairsObj;
        
        List<SIP_Rebate__c> sipRebates = new List<SIP_Rebate__c>();
        for (Integer i = 0; i< 3; i++) {
            sipRebates.add(TestDataFactory.createTestSIPRebate(testContractObj.Id, testSIPHeaderObj.Id, 1+i, 2+i, 3+i));
        }
        INSERT sipRebates;
        
        // setup a reference to the page the controller is expecting with the parameters
        PageReference pref = Page.New_Version_Page;
        pref.getParameters().put('contractId', testContractObj.id);
        Test.setCurrentPage(pref);
        
        // Switch to test context
        Test.startTest();
        // Construct the controllers
        ApexPages.StandardController con = new ApexPages.StandardController(testContractObj);
        NewVersionController nvc = new NewVersionController(con);
        nvc.actionButtonYes();
        nvc.actionButtonNo();
        Contract oldContract = nvc.getOldContract();   
        Test.stopTest();
        //Asserts
		List<International_City_Pairs__c> intCityPairs = [SELECT Id, International_City_Pairs_Parent__c 
                                                          FROM International_City_Pairs__c 
                                                          WHERE International_City_Pairs_Parent__c =: testInternationalCityPairsObj.Id];
        System.assertEquals(testInternationalCityPairsObj.Id, intCityPairs[0].International_City_Pairs_Parent__c);
    }
}