/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 28.10.2021
**Description: Schedular Class for the batch class 'CaseAttachmentDeletionBatch'
*/
global class CaseAttachmentDeletionScheduler implements Schedulable{
    global void execute(SchedulableContext sc){
        CaseAttachmentDeletionBatch caseAttach = new CaseAttachmentDeletionBatch();
        database.executeBatch(caseAttach);
    }
}