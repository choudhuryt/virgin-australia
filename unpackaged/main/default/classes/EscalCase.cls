public class EscalCase {
    //Apex properties or variables

   
    public Id caseid { get; set; }
    public Case cas { get; set; }
    List<Case> lstCaseUpdate = new List<Case>();  
    
    //constructor to get the Case record
    public EscalCase(ApexPages.StandardController controller)
    {
     cas =  (Case) controller.getRecord();
        
    }

    //Method that can is called from the Visual Force page action attribute
    public PageReference caseEscalation() {
        
   
    caseid = cas.Id;
    List<Case> caselist = [SELECT id, CaseNumber,Status,OwnerId,Priority,Queue_Override_Options__c ,Allocation_Priority__c  FROM Case WHERE id =:caseid];
    system.debug('The case list' +caselist )  ; 
    for(Case c : caselist)
    {
        c.Status = 'New';
        c.OwnerId = '00590000001VJ16';
        c.Priority = 'Escalated back to GR - Sitel';
        c.Queue_Override_Options__c = 'Brisbane Team Queue';
        c.Allocation_Priority__c = 'Amber';
        lstCaseUpdate.add(c);
      }
        
         system.debug('The case update list' +lstCaseUpdate )    ; 
      if(!lstCaseUpdate.isEmpty())
           {
                     update   lstCaseUpdate ;
           }
    if (caselist.size() > 0)
    {
        CaseComment a = new CaseComment(ParentId = caseid, CommentBody = 'Escalated to BNE Team' + ' on ' + system.today().format() ,IsPublished = TRUE);
        insert a;
    }     
       
       PageReference pageRef = new PageReference('/'+Caseid);
        pageRef.setRedirect(true);
       return pageRef; //Returns to the case page
    }
}