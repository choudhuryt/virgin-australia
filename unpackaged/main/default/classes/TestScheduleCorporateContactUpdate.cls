@isTest
public class TestScheduleCorporateContactUpdate 
{
static testMethod void myUnitTest()
    {
        // TO DO: implement unit test
         DateTime currTime = DateTime.now();
         Integer min = currTime.minute();
         Integer hour = currTime.hour();
         String sch;
        
	if(min <= 58)
            sch = '0 '+ (min + 1) + ' ' + hour + ' * * ? '+ currTime.year();
        else          
            sch = '0 0 '+ (hour + 1) + ' * * ? '+ currTime.year();
        
    Test.startTest();
        
	ScheduleCorporateContactVelocityUpdate obj = new ScheduleCorporateContactVelocityUpdate();  
	CorporateContactVelocityStatusUpdate newContactUpdate = new CorporateContactVelocityStatusUpdate();          
	String jobId = system.schedule('test', sch, obj); 
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger where id = :jobId];        
    System.assertEquals(sch, ct.CronExpression);                                      
    database.executeBatch(newContactUpdate);  
    test.stoptest();
    }
}