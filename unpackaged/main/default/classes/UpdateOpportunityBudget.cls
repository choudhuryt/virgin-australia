public class UpdateOpportunityBudget 
{
 @InvocableMethod 
public static void UpdateVelocityOpportunityBudeget(List<Id> oppids  ) 
{
  List<Opportunity> lstOppUpdate = new List<Opportunity>(); 
  List<Opportunity> lstOpp = [ SELECT Id, CloseDate FROM Opportunity WHERE Id =:oppids]; 
  List<Budget__c> lstbudget = [ SELECT Id ,Start_Date__c,End_Date__c from Budget__c 
                              where  Start_Date__c <= :lstOpp[0].CloseDate
                              and End_Date__c >= :lstOpp[0].CloseDate
                              and Type__c = 'Velocity'
                              limit 1
                              ];
    
  system.debug('The test values' + lstbudget +  lstOpp )  ; 
    
 if (lstbudget.size() > 0 )
 {
          for(Opportunity o: lstOpp)
      {
          o.Budget__c = lstbudget[0].id ;
          lstOppUpdate.add(o);      
      } 
     
   update lstOppUpdate;    
 }
     
    
}

}