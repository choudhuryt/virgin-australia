/**
* @description       : Test class for CaseUtilities
* @Updated By        : Cloudwerx
**/
@isTest
private class TestFlyPlusToAccelerateAutomation {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('FlyPlus', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        testAccountObj.AccountSource = 'FlyPlus';
        testAccountObj.Business_Number__c = '123';
        testAccountObj.RecordTypeId = FlyPlusToAccelerateAutomation.accountFlyPlusRecordTypeId;
        testAccountObj.Request_to_Move_to_Accelrate__c = true;
        testAccountObj.Notify_Key_Contact__c = 'Yes';
        INSERT testAccountObj;
        
        User u = TestUtilityClass.createTestUser();
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Integration Profile' LIMIT 1].Id;
        INSERT u;
        
        system.runAs(u){
            Account testAccountObj2 = TestDataFactory.createTestAccount('FlyPlustestAccountDataValidity', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
            testAccountObj.AccountSource = 'FlyPlus';
            testAccountObj.Business_Number__c = 'ABC1';
            testAccountObj.RecordTypeId = FlyPlusToAccelerateAutomation.accountFlyPlusRecordTypeId;
            testAccountObj.Request_to_Move_to_Accelrate__c = true;
            testAccountObj.Notify_Key_Contact__c = 'Yes';
            INSERT testAccountObj2;
            
            Account testAccountObj1 = TestDataFactory.createTestAccount('testAccountDataValidity1', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
            testAccountObj.AccountSource = 'FlyPlus';
            testAccountObj.Business_Number__c = 'ABC11';
            testAccountObj.RecordTypeId = FlyPlusToAccelerateAutomation.accountFlyPlusRecordTypeId;
            testAccountObj.Request_to_Move_to_Accelrate__c = true;
            testAccountObj.Notify_Key_Contact__c = 'Yes';
            INSERT testAccountObj1;
            
            Account_Data_Validity__c testAccountDataValidityObj = TestUtilityClass.createAccountDataValidity(testAccountObj2.Id, FlyPlusToAccelerateAutomation.vaUserId, testAccountObj1.Id);
            testAccountDataValidityObj.To_Date__c = null;
            testAccountDataValidityObj.Market_Segment__c = 'FlyPlus';
            testAccountDataValidityObj.Account_Record_Type__c = Utilities.getRecordTypeId('Account', 'Corporate');
            INSERT testAccountDataValidityObj;
        }
    }
    
    @isTest 
    private static void testFlyPlusToAccelerateAutomation() {
        List<Account> testAccounts = [SELECT Id, Business_Number__c FROM Account];
        List<Id> accountIds = new List<Id>(new Map<Id,Account>(testAccounts).keySet());
        testAccounts[0].Business_Number__c = 'ABC';
        UPDATE testAccounts[0];
        
        Test.startTest();
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccounts[0].Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj.RecordTypeId = FlyPlusToAccelerateAutomation.contractFlyPlusRecordTypeId;
        INSERT testContractObj;
        testContractObj.Status = 'Activated';
        UPDATE testContractObj;
        
        Contract testContractObj1 = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccounts[0].Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj1.RecordTypeId = FlyPlusToAccelerateAutomation.contractAcceleratePOSRebateRecordTypeId;
        INSERT testContractObj1;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccounts[0].Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        testContactObj.Status__c = 'Active';
        testContactObj.Key_Contact__c = true;
        INSERT testContactObj;
        FlyPlusToAccelerateAutomation.UpdateFlyPlusContractandNotifyKeyContact(accountIds);
        Test.stopTest();
        //Asserts
        List<Account_Data_Validity__c> accountDataValidities = [SELECT Id, Account_Record_Type__c, Account_Owner__c,
                                                                Market_Segment__c, Sales_Matrix_Owner__c, Account_Record_Type_Picklist__c 
                                                                FROM Account_Data_Validity__c WHERE From_Account__c =:testAccounts[0].Id];
        System.assertEquals(2, accountDataValidities.size());
        System.assertEquals(FlyPlusToAccelerateAutomation.accountAccelerateRecordTypeId, accountDataValidities[1].Account_Record_Type__c);
        System.assertEquals(FlyPlusToAccelerateAutomation.vaUserId, accountDataValidities[1].Account_Owner__c);
        System.assertEquals(FlyPlusToAccelerateAutomation.vaUserId, accountDataValidities[1].Account_Owner__c);
        System.assertEquals('Accelerate', accountDataValidities[1].Market_Segment__c);
        System.assertEquals('Accelerate', accountDataValidities[1].Sales_Matrix_Owner__c);
        System.assertEquals('Accelerate', accountDataValidities[1].Account_Record_Type_Picklist__c);
        //Asserts
        List<Contract> contracts = [SELECT Id FROM Contract];
        Tourcodes__c testTourcodesObj = [SELECT Id, Contract__c FROM Tourcodes__c LIMIT 1];
        System.assertEquals(contracts[1].Id, testTourcodesObj.Contract__c);
    }
    
    @isTest 
    private static void testFlyPlusToAccelerateAutomationNoTourcodes() {
        List<Account> testAccounts = [SELECT Id, Business_Number__c FROM Account];
        List<Id> accountIds = new List<Id>(new Map<Id,Account>(testAccounts).keySet());
        Test.startTest();
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccounts[0].Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj.RecordTypeId = FlyPlusToAccelerateAutomation.contractFlyPlusRecordTypeId;
        INSERT testContractObj;
        testContractObj.Status = 'Activated';
        UPDATE testContractObj;
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccounts[0].Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        testContactObj.Status__c = 'Active';
        testContactObj.Key_Contact__c = true;
        INSERT testContactObj;
        FlyPlusToAccelerateAutomation.UpdateFlyPlusContractandNotifyKeyContact(accountIds);
        Test.stopTest();
        //Asserts
        List<Account_Data_Validity__c> accountDataValidities = [SELECT Id, Account_Record_Type__c, Account_Owner__c,
                                                                Market_Segment__c, Sales_Matrix_Owner__c, Account_Record_Type_Picklist__c 
                                                                FROM Account_Data_Validity__c WHERE From_Account__c =:testAccounts[0].Id];
        System.assertEquals(2, accountDataValidities.size());
        System.assertEquals(FlyPlusToAccelerateAutomation.accountAccelerateRecordTypeId, accountDataValidities[1].Account_Record_Type__c);
        System.assertEquals(FlyPlusToAccelerateAutomation.vaUserId, accountDataValidities[1].Account_Owner__c);
        System.assertEquals(FlyPlusToAccelerateAutomation.vaUserId, accountDataValidities[1].Account_Owner__c);
        System.assertEquals('Accelerate', accountDataValidities[1].Market_Segment__c);
        System.assertEquals('Accelerate', accountDataValidities[1].Sales_Matrix_Owner__c);
        System.assertEquals('Accelerate', accountDataValidities[1].Account_Record_Type_Picklist__c);
    }
}