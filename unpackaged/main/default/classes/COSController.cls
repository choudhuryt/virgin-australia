public with sharing class COSController {

 private ApexPages.StandardController controller {get; set;}
   public List<Domestic_Discounts__c> searchResults {get;set;}
   public List<Intra_WA_Discounts__c> searchResults1 {get;set;}
   public List <Contract> contracts {get;set;}
   public Contract originalContract{get;set;}
   public Cost_of_Sale__c costOfSale{get;set;}

   private Contract a;
  
    // Constructor
    
    public COSController(ApexPages.StandardController myController) 
    {
        a=(Contract)myController.getrecord();
     	
    }
 
    //pre-processing prior to Page load
     public PageReference initDisc() 
     {
     	
     	contracts=[select Ticket_Fund__c,Fixed_Amount__c,id,Cos_Version__c,Domestic_Short_Haul_Tier__c,Red_Circle__c,IsTemplate__c,Saver_Discounts__c,Domestic_TT_VA_Market_Share_Target__c,Virgin_Australia_Amount__c,Domestic_and_Trans_Tasman_VA_Rev_Target__c from Contract where id =:a.id];
   	    originalContract = contracts.get(0);
   	    costOfSale = (Cost_of_Sale__c)[select Grand_Total_Percent__c,Grand_Total__c,Value_Add_Total_Percent__c,Value_Add_Total__c,VSM_Acceleration_Cost__c,Velocity_Acceleration_Percentage__c,Ticket_Fund_Percentage__c,Marketing_Fund_Percentage__c,Cost_Of_SaleRevenue_Figure_Trans__c,Cost_Of_SaleRevenue_Figure_int__c,Cost_Of_SaleRevenue_Figure_Domestic__c,Marketing_Fund__c,Ticket_Fund__c,Trans_Tasman_Revenue_POS__c,Trans_Tasman_POS__c,Domestic_POS__c,Domestic_Revenue_POS__c,Contract__c,Total_Revenue_POS__c,Total_POS__c,International_Revenue_POS__c,International_POS__c from Cost_of_Sale__c where Contract__c=:a.id ];
   	    
   	    return null;
     	
     }
     
      public PageReference cancel() {
   // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
   PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
                  //
                  thePage.setRedirect(true);
                  //
                  return thePage;
   
  }


}