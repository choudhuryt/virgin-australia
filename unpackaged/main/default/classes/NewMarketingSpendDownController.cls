public with sharing class NewMarketingSpendDownController {

public Integer versionum {get;set;}
public Marketing_Spend_Down__c msd{get;set;}
    
public List <Marketing_Spend_Down__c> mktspdn {get;set;}
    
public Marketing_Spend_Down__c msdold{get;set;}    

public NewMarketingSpendDownController(ApexPages.StandardController controller)
{
    this.msdold= (Marketing_Spend_Down__c)controller.getRecord();    
    msd = new Marketing_Spend_Down__c();
    versionum = 0 ;
}

public PageReference initDisc() 
{
   mktspdn = [SELECT Account__c,Contract__c,Global_Sales_Activity__c,
                     Campaign_Detail__c,Currency__c,Name,
                     GST_Inclusive__c,Reason_for_New_Version__c,
                     Next_Version__c,Version_Number__c,Amount_to_be_Deducted__c
              FROM Marketing_Spend_Down__c 
              where id =:ApexPages.currentPage().getParameters().get('id')];
    
   msdold = mktspdn.get(0);
   versionum =  msdold.Version_Number__c.intValue() + 1;
   return null;        
}    

    
public PageReference save() {
    
    msd.Account__c = msdold.Account__c;
    msd.Contract__c = msdold.Contract__c;
    msd.Version_Number__c = versionum ;
    msd.Global_Sales_Activity__c = msdold.Global_Sales_Activity__c;
    msd.Approval_Status__c = 'Draft';
    msd.Campaign_Detail__c = msdold.Campaign_Detail__c;
    msd.GST_Inclusive__c = msdold.GST_Inclusive__c;
    msd.Reason_for_New_Version__c = msdold.Reason_for_New_Version__c;
    msd.Amount_to_be_Deducted__c = msdold.Amount_to_be_Deducted__c;
    msd.Currency__c = msdold.Currency__c;
    msd.Original_Spend_Down__c = msdold.Id ;
    insert msd;
    
    PageReference pageRef = new PageReference('/'+msd.Id);
    pageRef.setRedirect(true);
    return pageRef;
}
    
public PageReference cancel() {
 
   PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
   thePage.setRedirect(true);
   return thePage;
   
  }
}