public with sharing class SIPInputEditController {
	public List<SIPHeaderWrapper> SIPHeaderWrapperListObject{get;set;}
	public String ContractID{get; set;}
	public List<SIPHeaderWrapper> GetSIPHeaderList()
	{
		if(SIPHeaderWrapperListObject == null)
		{
			List<SIPHeaderWrapper> SIPHeaderWrapperList = new List<SIPHeaderWrapper>();
			if(contractId == null)
			{
				contractId = ApexPages.currentPage().getParameters().get('cid');
			}
			List<SIP_Header__c> SIPHeaderList  = [Select s.Type__c, s.SystemModstamp, s.SIP_Percentage__c, s.SIP_Calculation__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsTemplate__c, s.IsDeleted, s.Id, s.Full_Last_Year_Actual_Revenue__c, s.Est_Current_Full_Year_Revenue__c, s.Description__c, s.CreatedDate, s.CreatedById, s.Contract__c From SIP_Header__c s where s.Contract__c =:contractId];
			system.debug('SIPHeaderList=' + SIPHeaderList);
			for(SIP_Header__c SIPHeaderItem :  SIPHeaderList)
			{
				String headId= SIPHeaderItem.Id;
				SIPHeaderWrapper SIPHeaderWrapperItem = new SIPHeaderWrapper();
				SIPHeaderWrapperItem.SIPHeaderItem = SIPHeaderItem;
				list<SIP_Rebate__c> SIPRebateList = [Select s.Upper_Percent__c, s.Tier__c, s.SystemModstamp, s.SIP_Rebate_Parent__c, s.SIP_Percent__c, s.SIP_Header__c, s.Name, s.Lower_Percent__c, s.LastModifiedDate, s.LastModifiedById, s.JH_Tier__c, s.IsDeleted, s.Id, s.Current_Tier__c, s.CreatedDate, s.CreatedById, s.Contract_Growth__c, s.Calculated_Upper_Amount__c, s.Calculated_Lower_Amount__c From SIP_Rebate__c s where s.SIP_Header__c =:headId];
				system.debug('SIPRebateList=' + SIPRebateList);
				SIPHeaderWrapperItem.SIPRebateList = SIPRebateList;
				SIPHeaderWrapperList.add(SIPHeaderWrapperItem); 
			}
			 SIPHeaderWrapperListObject = SIPHeaderWrapperList;
		}
		return SIPHeaderWrapperListObject;
	}
	public PageReference Save()
	{
		return null;
	}
	public PageReference SaveAndClose()
	{
		return null;
	}
	public class SIPHeaderWrapper
	{
		public SIP_Header__c SIPHeaderItem {set; get;}
		public List<SIP_Rebate__c> SIPRebateList{set; get;}
		public SIPHeaderWrapper(){}
		
	}
}