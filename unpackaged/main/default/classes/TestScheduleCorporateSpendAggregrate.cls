/**
* @description       : Test class for ScheduleCorporateSpendAggregate 
* @updatedBy         : Cloudwerx
**/
@isTest
private class TestScheduleCorporateSpendAggregrate {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        testAccountObj.RecordTypeId = Utilities.getRecordTypeId('Account', 'Corporate');
        testAccountObj.Account_Lifecycle__c = 'Contract';
        INSERT testAccountObj;
        
        Opportunity testOpportunityObj = TestDataFactory.createTestOpportunity('testOpp1', 'Sales Opportunity Analysis', testAccountObj.Id, Date.today(), 1.00);
        INSERT testOpportunityObj; 
        
        Contract testContractObj = TestDataFactory.createTestContract('TestContract', testAccountObj.Id, 'Draft', '15', testOpportunityObj.Id, true, Date.today());
        INSERT testContractObj;
        testContractObj.Status = 'Activated';
        UPDATE testContractObj;
        
        List<Market__c> testMarkets = new List<Market__c>();
        testMarkets.add(TestDataFactory.createTestMarket('DOM Mainline', testOpportunityObj.Id, testContractObj.Id, 'DOM(Mainline)', 'Tier 1', 'Tier 1', 1, 1, 'UA', 1000));
        testMarkets.add(TestDataFactory.createTestMarket('Japan', testOpportunityObj.Id, testContractObj.Id, 'DOM(Regional)', 'Tier 1', 'Tier 1', 1, 1, 'UA', 2000));
        INSERT testMarkets;
    }
    
    @isTest
    private static void testScheduleAccelerateAccountBatch() {
        Test.startTest();
        ScheduleCorporateSpendAggregate scheduleCorporateSpendAggregateObj = new ScheduleCorporateSpendAggregate();
        String cronTime = '0 0 23 * * ?'; 
        system.schedule('ScheduleCorporateSpendAggregate' + System.now(), cronTime, scheduleCorporateSpendAggregateObj);
        Test.stopTest();
    }
}