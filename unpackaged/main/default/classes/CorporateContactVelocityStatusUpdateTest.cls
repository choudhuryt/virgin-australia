/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 23.05.2022
**Description: Test Class for VelocitySnapshot
*/
@isTest
public class CorporateContactVelocityStatusUpdateTest
{
    //REST Mock
    Class SingleRequestMock implements HttpCalloutMock{
        protected Integer code;
        protected String status;
        protected String bodyAsString;
        protected Blob bodyAsBlob;
        protected Map<String, String> responseHeaders;
        
        public SingleRequestMock(Integer code, String status, String body, Map<String, String> responseHeaders){
            this.code = code;
            this.status = status;
            this.bodyAsBlob = null;
            this.bodyAsString = body;
            this.responseHeaders = responseHeaders;
        }
        public SingleRequestMock(Integer code, String status, Blob body, Map<String, String> responseHeaders){
            this.code = code;
            this.status = status;
            this.bodyAsBlob = body;
            this.bodyAsString = null;
            this.responseHeaders = responseHeaders;
        }
        
        public HTTPResponse respond(HTTPRequest req){
            CalloutException e;
            HTTPResponse resp = new HttpResponse();
            if(code != NULL){
                resp.setStatusCode(code);
            }else{
                e = (CalloutException)CalloutException.class.newInstance();
                e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings.');
                //throw e;
            }
            
            resp.setStatus(status);
            if(bodyAsBlob != null){
                resp.setBodyAsBlob(bodyAsBlob);
            }else if(e != NULL){
                resp.setBody(String.valueOf(e));
            }else{
                resp.setBody(bodyAsString);
            }
            if(responseHeaders != null){
                for(String key: responseHeaders.keySet()){
                    resp.setHeader(key, responseHeaders.get(key));
                }
            }
            
            return resp;
        }        
    }
    static testMethod void myUnitTestRed()
    {
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "RED", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Ok',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        Account account = new Account();
        CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
        account = commonObjectsForTest.CreateAccountObject(0);  
        account.RecordTypeId ='01290000000tSR2';
        account.Sales_Matrix_Owner__c = 'VIC';
        account.Market_Segment__c = 'Mid Market' ;
        insert account;  
        
        Contact contactTest = new Contact();
        contactTest.LastName = 'TestLast Contact';
        contactTest.FirstName  = 'TestFirst Contact';
        contactTest.Key_Contact__c  = TRUE;
        contactTest.Email = 'testContact@yahoo.com';        
        contactTest.Title = 'Test Title';
        contactTest.Phone = '123456';
        contactTest.Velocity_Number__c = '2105327372';
        contactTest.RecordTypeId = '012900000007krgAAA';
        contactTest.AccountId = account.Id;
        Insert contactTest;
        
        Test.StartTest();
        
        CorporateContactVelocityStatusUpdate newContactUpdate = new CorporateContactVelocityStatusUpdate();
        
        Database.executeBatch( newContactUpdate);
        
        Test.StopTest();   
        
    }
    static testMethod void myUnitTestSilver()
    {
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "SILVER", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Ok',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        Account account = new Account();
        CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
        account = commonObjectsForTest.CreateAccountObject(0);  
        account.RecordTypeId ='01290000000tSR2';
        account.Sales_Matrix_Owner__c = 'VIC';
        account.Market_Segment__c = 'Mid Market' ;
        insert account;  
        
        Contact contactTest = new Contact();
        contactTest.LastName = 'TestLast Contact';
        contactTest.FirstName  = 'TestFirst Contact';
        contactTest.Key_Contact__c  = TRUE;
        contactTest.Email = 'testContact@yahoo.com';        
        contactTest.Title = 'Test Title';
        contactTest.Phone = '123456';
        contactTest.Velocity_Number__c = '2105327372';
        contactTest.RecordTypeId = '012900000007krgAAA';
        contactTest.AccountId = account.Id;
        insert contactTest;
        
        Test.StartTest();        
        CorporateContactVelocityStatusUpdate newContactUpdate = new CorporateContactVelocityStatusUpdate();        
        Database.executeBatch( newContactUpdate);        
        Test.StopTest();   
        
    }
    static testMethod void myUnitTestGold()
    {
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "GOLD", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Ok',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        Account account = new Account();
        CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
        account = commonObjectsForTest.CreateAccountObject(0);  
        account.RecordTypeId ='01290000000tSR2';
        account.Sales_Matrix_Owner__c = 'VIC';
        account.Market_Segment__c = 'Mid Market' ;
        insert account;  
        
        Contact contactTest = new Contact();
        contactTest.LastName = 'TestLast Contact';
        contactTest.FirstName  = 'TestFirst Contact';
        contactTest.Key_Contact__c  = TRUE;
        contactTest.Email = 'testContact@yahoo.com';        
        contactTest.Title = 'Test Title';
        contactTest.Phone = '123456';
        contactTest.Velocity_Number__c = '2105327372';
        contactTest.RecordTypeId = '012900000007krgAAA';
        contactTest.AccountId = account.Id;
        insert contactTest;
        
        Test.StartTest();        
        CorporateContactVelocityStatusUpdate newContactUpdate = new CorporateContactVelocityStatusUpdate();        
        Database.executeBatch( newContactUpdate);        
        Test.StopTest();   
        
    }
    static testMethod void myUnitTestPlatinum()
    {
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "Platinum", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Ok',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        Account account = new Account();
        CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
        account = commonObjectsForTest.CreateAccountObject(0);  
        account.RecordTypeId ='01290000000tSR2';
        account.Sales_Matrix_Owner__c = 'VIC';
        account.Market_Segment__c = 'Mid Market' ;
        insert account;  
        
        Contact contactTest = new Contact();
        contactTest.LastName = 'TestLast Contact';
        contactTest.FirstName  = 'TestFirst Contact';
        contactTest.Key_Contact__c  = TRUE;
        contactTest.Email = 'testContact@yahoo.com';        
        contactTest.Title = 'Test Title';
        contactTest.Phone = '123456';
        contactTest.Velocity_Number__c = '2105327372';
        contactTest.RecordTypeId = '012900000007krgAAA';
        contactTest.AccountId = account.Id;
        insert contactTest;
        
        Test.StartTest();        
        CorporateContactVelocityStatusUpdate newContactUpdate = new CorporateContactVelocityStatusUpdate();        
        Database.executeBatch( newContactUpdate);        
        Test.StopTest();   
        
    }
    static testMethod void myUnitTestVip()
    {
        String body = '{ "data": { "channel": "AGENT_UI", "lastModifiedAt": "2022-04-12T17:21:36.12Z", "subType": "INDIVIDUAL", "membershipId": "0000360491", "pointBalance": 610, "pointBalanceExpiryDate": "2024-02-29", "statusCredit": 200, "eligibleSector": 0, "status": { "main": "OPEN", "effectiveAt": "2021-07-19", "sub": { "accountIdentifier": { "activity": "ACTIVE", "merge": "NEUTRAL", "fraudSuspicion": "NEUTRAL", "billing": "INACTIVE", "completeness": { "percentage": 100 }, "accountLoginStatus": "UNLOCKED" }, "characteristicIdentifier": { "lifeCycle": { "isDeceased": false, "ageGroup": "ADULT" } } } }, "enrolmentSource": "MCC", "enrolmentDate": "2021-07-19", "mainTier": { "tierType": "MAIN", "level": "VIP", "code": "SR", "label": "STANDARD RED", "validityStartDate": "2021-07-18T14:00:00Z" }, "individual": { "identity": { "firstName": "Tessssaaasting", "lastName": "Tesssaaasaassat", "title": "MR", "suffix": "Jr", "preferredFirstName": "Tessssaaasasasating", "birthDate": "1933-10-09", "gender": "MALE", "employments": [ { "company": "Virgin", "title": "Engineer" } ] }, "consents": [ { "frequency": "Daily", "status": "NOT_GRANTED", "to": "VELOCITY", "via": "POST", "for": "FUEL PARTNER OFFERS" } ], "preferences": [ { "category": "REFERENTIAL_DATA", "subCategory": "CURRENCY", "value": "AUD" } ], "fulfillmentDetail": {}, "contact": { "emails": [ { "category": "PERSONAL", "address": "jorge.test03-nosec@virginaustralia.com", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ], "phones": [ { "category": "PERSONAL", "deviceType": "LANDLINE", "countryCallingCode": "61", "areaCode": "02", "number": "987654356", "contactValidity": { "isMain": false, "isValid": true, "isConfirmed": false } } ], "addresses": [ { "category": "BUSINESS", "lines": [ "Test Line 1", "Test Line 2", null ], "postalCode": "1234", "countryCode": "AU", "cityName": "Windsor", "stateCode": "QLD", "contactValidity": { "isMain": true, "isValid": true, "isConfirmed": false } } ] } }, "enrolmentDetail": { "referringMemberId": "1026372134", "promoCode": "AFLLIONS" } } }';
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Ok',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        Account account = new Account();
        CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
        account = commonObjectsForTest.CreateAccountObject(0);  
        account.RecordTypeId ='01290000000tSR2';
        account.Sales_Matrix_Owner__c = 'VIC';
        account.Market_Segment__c = 'Mid Market' ;
        insert account;  
        
        Contact contactTest = new Contact();
        contactTest.LastName = 'TestLast Contact';
        contactTest.FirstName  = 'TestFirst Contact';
        contactTest.Key_Contact__c  = TRUE;
        contactTest.Email = 'testContact@yahoo.com';        
        contactTest.Title = 'Test Title';
        contactTest.Phone = '123456';
        contactTest.Velocity_Number__c = '2105327372';
        contactTest.RecordTypeId = '012900000007krgAAA';
        contactTest.AccountId = account.Id;
        insert contactTest;
        
        Test.StartTest();        
        CorporateContactVelocityStatusUpdate newContactUpdate = new CorporateContactVelocityStatusUpdate();        
        Database.executeBatch( newContactUpdate);        
        Test.StopTest();   
        
    }
    static testMethod void myUnitTestFailure()
    {
        String body= '{ "code": 37105, "title": "Not Found", "description": "No matching member found.", "status": 404 }';
        SingleRequestMock fakeResponse = new SingleRequestMock(404,'Not Found',body,null);
        System.Test.setMock(HttpCalloutMock.Class, fakeResponse);
        
        Account account = new Account();
        CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
        account = commonObjectsForTest.CreateAccountObject(0);  
        account.RecordTypeId ='01290000000tSR2';
        account.Sales_Matrix_Owner__c = 'VIC';
        account.Market_Segment__c = 'Mid Market' ;
        insert account;  
        
        Contact contactTest = new Contact();
        contactTest.LastName = 'TestLast Contact';
        contactTest.FirstName  = 'TestFirst Contact';
        contactTest.Key_Contact__c  = TRUE;
        contactTest.Email = 'testContact@yahoo.com';        
        contactTest.Title = 'Test Title';
        contactTest.Phone = '123456';
        contactTest.Velocity_Number__c = '2105327372';
        contactTest.RecordTypeId = '012900000007krgAAA';
        contactTest.AccountId = account.Id;
        insert contactTest;
        
        Test.StartTest();        
        CorporateContactVelocityStatusUpdate newContactUpdate = new CorporateContactVelocityStatusUpdate();        
        Database.executeBatch( newContactUpdate);        
        Test.StopTest();   
        
    }
}