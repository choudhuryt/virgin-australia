/*
*   Custom controller for VF page: ReservationSection
*/
public class ReservationController{
    public Case caseObj{get;set;}
    public Reservation__c reserObj{get; set;}

    // list data to export to tables on VF page
    public List<FlightSegment__c> flightSegmentList {get; set;}
    public List<Passenger__c> passengerList {get; set;}
    public List<SpecialServiceRequest__c> ssrList {get; set;}
    public List<Other_Service_Information__c> osiList {get; set;}

    // map of Id and object to link SSR, OSI to Passenger and FlightSegment when persisting data to Salesforce
    public Map<String, FlightSegment__c> flightSegmentMap {get; set;}
    public Map<String, Passenger__c> passengerMap {get; set;}
    public Map<String, SpecialServiceRequest__c> ssrMap {get; set;}
    public Map<String, Other_Service_Information__c> osiMap {get; set;}

    // keep the response model of OSI and SSR in order to map the ID to parent Passenger/FlightSegment
    public List<GuestCaseReservationInfoModel.OSIType> osiModel {get; set;}
    public List<GuestCaseReservationInfoModel.SpecialServiceRequestType> ssrModel {get; set;}

    public String reservationPhone {get; set;}
    public boolean isSaved {get; set;}
    private boolean isNew;
    public String resTemp {get; set;}

    public ReservationController() {
        isNew = false;
        isSaved = false;
        String caseId = ApexPages.currentPage().getParameters().get('id');
        String PNR = ApexPages.currentPage().getParameters().get('pnr');
        String PNRDate = ApexPages.currentPage().getParameters().get('date');

        try {
            caseObj = [SELECT Id, CaseNumber, Ticket_number__c, Reservation_Number__c, Reservation_Date__c,
                       Flight_Number__c, Flight_Date__c, Origin__c, Destination__c, Contact_First_Name__c,
                       Contact_Last_Name__c
                       FROM Case WHERE Id = :caseId];
            reserObj = [select Id, Case__c, Flight_Number__c, Flight_Date__c, Origin__c, Destination__c,
                        Ticket_Number__c, Reservation_Date__c, Reservation_Number__c
                        from Reservation__c where Case__c = :caseObj.Id limit 1];
        } catch (Exception ex){
            System.debug('EXCEPTION ReservationController() ' + ex);
            reserObj = null;
        }

        if (reserObj != null) {
            if (reserObj.Reservation_Number__c == PNR) {
                flightSegmentList = getFlightSegmentList(reserObj);
                passengerList = getPassengerList(reserObj);
                ssrList = getSSRList(reserObj);
                osiList = getOSIList(reserObj);
            }

            // begin DE4911 bug fixing - Charleston Telles
            // it it has search criteria saved but it hasn't reservation saved
            if (flightSegmentList==null || flightSegmentList.size() == 0)
                searchReservation(PNR, Utilities.convert2Date(PNRDate));
        } else if (!String.isEmpty(PNR)){
            reserObj = new Reservation__c();
            searchReservation(PNR, Utilities.convert2Date(PNRDate));
            // end DE4911 bug fixing
        } else {
            caseObj = new Case();
            reserObj = new Reservation__c();
        }
        
        System.debug('RESERVATION LOADED' + reserObj);
    }
    
    
    private void searchReservation(String PNR, Date PNRDate){       
        isNew = true; // if service returns error, check later
        GuestCaseReservationInfo.SalesforceReservationService service = new GuestCaseReservationInfo.SalesforceReservationService();
        try{
        // Retrieve Apex Callout options from Custom Settings
        Apex_Callouts__c apexCallouts = Apex_Callouts__c.getValues('ReservationGet');
        if(apexCallouts != null){
            service.endpoint_x = apexCallouts.Endpoint_URL__c;
            service.timeout_x = Integer.valueOf(apexCallouts.Timeout_ms__c);
            service.clientCertName_x = apexCallouts.Certificate__c;
        }
        System.debug('CALLOUT PNR: ' + PNR + ' DATE: ' + PNRDate);
        
        GuestCaseReservationInfoModel.ReservationType reservation = service.GetReservationDetails(PNR, PNRDate);

        System.debug('RESERVATION FROM WS: ' + reservation);
        // ---------- Map response to Salesforce objects to display to VF page -----------
        //reserObj = new Reservation__c(); // moved to constructor DE4911 bug fixing
        reserObj.Reservation_Number__c = PNR;
        reserObj.Reservation_Date__c = reservation.ReservationDate;

        flightSegmentMap = new Map<String, FlightSegment__c>();
        List<GuestCaseReservationInfoModel.FlightSegmentType> flightList = reservation.Itinerary.FlightSegments.FlightSegment;
        System.debug('Flight List size: ' + flightList.size());
        if (flightList != null) {
            for (GuestCaseReservationInfoModel.FlightSegmentType flightSegInfo: flightList){
                FlightSegment__c flightSeg = new FlightSegment__c();

                flightSeg.Departure_Date_and_Time__c = Utilities.convert2DateTime(flightSegInfo.DepartureDateTime);
                flightSeg.Departure_Airport_Code__c = flightSegInfo.DepartureAirportCode;
                flightSeg.Departure_Airport_Name__c = flightSegInfo.DepartureAirportName;

                flightSeg.Arrival_Date_and_Time__c = Utilities.convert2DateTime(flightSegInfo.ArrivalDateTime);
                flightSeg.Arrival_Airport_Code__c = flightSegInfo.ArrivalAirportCode;
                flightSeg.Arrival_Airport_Name__c = flightSegInfo.ArrivalAirportName;

                flightSeg.AOC_Operating_Company_Name__c = flightSegInfo.AocOperatingCompanyName;
                flightSeg.Fare_Class__c = flightSegInfo.FareClass;
                flightSeg.Booked_Cabin__c = flightSegInfo.BookedCabin;
                flightSeg.Partner_Record_Locator__c = flightSegInfo.PartnerRecordLocator;
                flightSeg.Operating_Carrier_Fare_Class__c = flightSegInfo.OperatingCarrierFareClass;
                flightSeg.Marketing_Carrier_Fare_Class__c = flightSegInfo.MarketingCarrierFareClass;

                flightSeg.Marketing_Airline__c = flightSegInfo.MarketingAirline.AirlineCode + ' - '
                    + flightSegInfo.MarketingAirline.FlightNumber;
                flightSeg.Operating_Airline__c = flightSegInfo.OperatingAirline.AirlineCode + ' - '
                    + flightSegInfo.OperatingAirline.FlightNumber;
                flightSeg.Equipment_Type__c = flightSegInfo.EquipmentType.EquipmentTypeCode;

                flightSegmentMap.put(flightSegInfo.ID, flightSeg);
            }
        }
        flightSegmentList = null;
        flightSegmentList = flightSegmentMap.values();
        System.debug('POPULATED FLIGHT: ' + flightSegmentList);

        /* --------- Passenger/Infant ------------*/
        passengerMap = new Map<String, Passenger__c>();

        List<GuestCaseReservationInfoModel.PassengerType> passList = reservation.Passengers.Passenger;
        if (passList != null) {
            for (GuestCaseReservationInfoModel.PassengerType passInfo: passList){
                Passenger__c passenger = new Passenger__c();
                passenger.Title__c = passInfo.PassengerName.Title;
                passenger.Given_Name__c = passInfo.PassengerName.GivenName;
                passenger.Surname__c = passInfo.PassengerName.Surname;
                passenger.Date_of_Birth__c = Utilities.convert2Date(passInfo.PassengerName.DateOfBirth);
                passenger.Infant__c = false;    // true for infantInfo only
                try {
                    passenger.Telephone_Number__c = passInfo.PassengerContactInfo.PassengerContactTelephone.UnparsedTelephoneNumber;
                } catch (Exception ex){}
                try {
                    passenger.Email_Address__c = passInfo.PassengerContactInfo.PassengerContactEmail;
                } catch (Exception ex){}
                try {
                    passenger.Program_Identifier__c = passInfo.LoyaltyMemberships.LoyaltyMembership.ProgramID;
                    passenger.Loyalty_Membership_ID__c = passInfo.LoyaltyMemberships.LoyaltyMembership.MembershipID;
                    passenger.Tier__c = passInfo.LoyaltyMemberships.LoyaltyMembership.Tier;
                } catch (Exception ex){}
                passengerMap.put(passInfo.ID, passenger);
            }
        }

        List<GuestCaseReservationInfoModel.InfantType> infantList = reservation.Passengers.Infant;
        if (infantList != null) {
            for (GuestCaseReservationInfoModel.InfantType infantInfo: infantList){
                Passenger__c passenger = new Passenger__c();
                passenger.Title__c = infantInfo.Title;
                passenger.Given_Name__c = infantInfo.GivenName;
                passenger.Surname__c = infantInfo.Surname;
                passenger.Gender__c = infantInfo.Gender;
                passenger.Date_of_Birth__c = Utilities.convert2Date(infantInfo.DateOfBirth);
                passenger.Infant__c = true; // true for infantInfo only
                passengerMap.put(infantInfo.ID, passenger);
            }
        }

        passengerList = passengerMap.values();
        System.debug('POPULATED PASSENGER: ' + passengerList);

        // get passenger phone to display on reservation detail page
        try {
            for (String phone : reservation.ContactInformation.TelephoneNumber){
                if (String.isNotBlank(phone)){
                    reservationPhone = phone;
                    break;
                }
            }
        } catch (Exception ex){
            reservationPhone = '';
        }
        updateReservationPhone(passengerList);

        /* ----------- Special Service Request ----------*/
        ssrMap = new Map<String, SpecialServiceRequest__c>();
        try {
        ssrModel = reservation.SpecialRequestDetails.SpecialServiceRequests.SpecialServiceRequest;
        } catch (Exception ex){}
        if (ssrModel != null){
            for (GuestCaseReservationInfoModel.SpecialServiceRequestType ssrInfo:ssrModel){
                SpecialServiceRequest__c ssr = new SpecialServiceRequest__c();
                ssr.SSR_Code__c = ssrInfo.SSRCode;
                ssr.Text__c = ssrInfo.Text;
                ssrMap.put(ssrInfo.ID, ssr);
            }
        }
        ssrList = ssrMap.values();
        System.debug('POPULATED SSR: ' + ssrList);

        /* ----------- Other Service Information -----------*/
        osiMap = new Map<String, Other_Service_Information__c>();
        try {
            osiModel = reservation.SpecialRequestDetails.OtherServiceInformation.OSI;
        } catch (Exception ex) {}
        if (osiModel != null) {
            for (GuestCaseReservationInfoModel.OSIType osiInfo:osiModel){
                Other_Service_Information__c osi = new Other_Service_Information__c();
                osi.OSI_Text__c = osiInfo.OSIText;
                osiMap.put(osiInfo.ID, osi);
            }
        }
        osiList = osiMap.values();
        System.debug('POPULATED OSI: ' + osiList);
        
        System.debug('POPULATED RESERVATION: ' + reserObj);
        resTemp = reserObj.Reservation_Number__c;
        }catch (Exception ex) {
            System.debug('EXCEPTION: ' + ex);
        }
    }

    // persist SObjects to database.
    public void saveReservation(){
        System.debug('ReservationController::saveReservation::init::');

        System.Savepoint sp = Database.setSavepoint();  // restore if one of objects cannot be saved
        
        System.debug('saveReservation::after_init');

        ID oldReservationId = null;
        try {
            // extract info about origin, dest, flightNumber, flightDate to persist to Reservation object
            String origin = ApexPages.currentPage().getParameters().get('origin');
            if (!String.isEmpty(origin)) reserObj.Origin__c = origin;

            String dest = ApexPages.currentPage().getParameters().get('dest');
            if (!String.isEmpty(dest)) reserObj.Destination__c = dest;

            String flightNum = ApexPages.currentPage().getParameters().get('flightnumber');
            if (!String.isEmpty(flightNum)) reserObj.Flight_Number__c = flightNum;

            String flightDate = ApexPages.currentPage().getParameters().get('flightdate');
            if (!String.isEmpty(flightDate)) reserObj.Flight_Date__c = Utilities.convert2DateTime(flightDate);
            
            System.debug('saveReservation::begin::'+reserObj);

            // insert to database if no Reservation exist yet
            if (isNew){
                // delete old record
                if (String.isNotBlank(reserObj.Id)) {
                    System.debug('saveReservation::deleteReserObj::'+reserObj);
                    oldReservationId = reserObj.Id;
                    delete reserObj;
                    reserObj.Id = null;
                }
                // link to parent Case
                if (String.isBlank(reserObj.Case__c)) {
                    reserObj.Case__c = caseObj.Id;
                }
                System.debug('saveReservation::insertResObj::'+reserObj);
                insert reserObj;

                // insert Flight Segment, this should never be empty
                for (FlightSegment__c flightSeg: flightSegmentMap.values()) {
                    flightSeg.Reservation__c = reserObj.Id;
                }
                insert flightSegmentMap.values();
                System.debug('saveReservation::insertFlightSegment::'+flightSegmentMap.values());

                // insert Passenger List, this should never be empty
                for (Passenger__c passenger: passengerMap.values()){
                    passenger.Reservation__c = reserObj.Id;
                }
                insert passengerMap.values();
                System.debug('saveReservation::insertPassengerList::'+passengerMap.values());

                if (ssrModel != null) {
                    //DE4965 De-reference Null Object Error. Handling for SSRs w out Flight Segment association
                    for (GuestCaseReservationInfoModel.SpecialServiceRequestType ssrInfo:ssrModel){
                        SpecialServiceRequest__c ssr = ssrMap.get(ssrInfo.ID);

                        FlightSegment__c flight = flightSegmentMap.get(ssrInfo.FlightSegmentAssociationID);
                        // if FlightSegmentAssociationID not exist or not match, get the first element
                        if (flight==null) {
                            flight = flightSegmentMap.values().get(0);
                        }
                        ssr.Flight_Segment__c = flight.Id;
                        
                        // PassengerAssociationID is not madatory, it can be omit in lookup relationship
                        Passenger__c passenger = passengerMap.get(ssrInfo.PassengerAssociationID);
                        if (passenger != null) {
                            ssr.Passenger__c = passenger.Id;
                        }
                    }
                    System.debug('saveReservation::insertSSR::'+ssrMap.values());
                    insert ssrMap.values();
                }

                if (osiModel != null) {
                    for (GuestCaseReservationInfoModel.OSIType osiInfo:osiModel){
                        Other_Service_Information__c osi = osiMap.get(osiInfo.ID);
                        Passenger__c passenger = passengerMap.get(osiInfo.PassengerAssociationID);
                        
                        // if passengerAssociationID not exist or not match, get the first element
                        if (passenger == null) {    
                            passenger = passengerMap.values().get(0);
                        }
                        osi.Passengers__c = passenger.Id;
                    }
                    System.debug('saveReservation::insertOSI::'+osiMap.values());
                    insert osiMap.values();
                }
            } else {
                // if Reservation objects already exist, update Origin, Destination, FlightNum, FlightDate
                System.debug('saveReservation::updateResObj::'+reserObj);
                update(reserObj);
            }
            
            // delete old reservation object
            
            isSaved = true; // save successfully
            System.debug('saveReservation::end');
        } catch (Exception ex){
            // rollback on errors
            Database.rollback(sp);
            reserObj.Id = oldReservationId;
            isSaved = false;
            ApexPages.addMessages(ex);
            System.debug('saveReservation::Exception::'+ex);
        }
    }

    private List<FlightSegment__c> getFlightSegmentList(Reservation__c reserObj) {
        List<FlightSegment__c> retValues = [select Id, Marketing_Airline__c, Operating_Airline__c, Departure_Date_and_Time__c,
                                            Arrival_Date_and_Time__c, Equipment_Type__c, Fare_Class__c,
                                            Booked_Cabin__c, AOC_Operating_Company_Name__c, Partner_Record_Locator__c,
                                            Departure_Airport_Code__c, Arrival_Airport_Code__c
                                            from FlightSegment__c where Reservation__c = :reserObj.Id];
        return retValues;
    }

    private List<Passenger__c> getPassengerList(Reservation__c reserObj) {
         List<Passenger__c> retValues = [select Id, Title__c, Given_Name__c, Surname__c, Gender__c,
                                            Date_of_Birth__c, Infant__c, Telephone_Number__c, Email_Address__c
                                            from Passenger__c where Reservation__c = :reserObj.Id];

        updateReservationPhone(retValues);
        return retValues;
    }

    private List<SpecialServiceRequest__c> getSSRList(Reservation__c reserObj) {
         List<SpecialServiceRequest__c> ssrList = [select Id, SSR_Code__c, Text__c from SpecialServiceRequest__c
                                                   where Flight_Segment__r.Reservation__c = :reserObj.Id];
        return ssrList;
    }

    private List<Other_Service_Information__c> getOSIList(Reservation__c reserObj) {
        osiList = [select Id, OSI_Text__c from Other_Service_Information__c
                   where Passengers__r.Reservation__c = :reserObj.Id];
        return osiList;
    }

    // generate Phone number to display on reservation detail page
    // only display if passenger GivenName = case FirstName and passenger Surname = case LastName
    // reservation phone will be the phone of that passenger
    private void updateReservationPhone (List<Passenger__c> passengerList) {
        for(Passenger__c passenger: passengerList) {
            if(String.IsNotBlank(passenger.Telephone_Number__c)) {
                   reservationPhone = passenger.Telephone_Number__c;
                   break;
            }
        }
        // if there are no phone numbers in Passenger list
        // and there is a phone number in ContractInformation
        // then add the phone number to the first passenger
        if (String.isNotBlank(reservationPhone))
            passengerList.get(0).Telephone_Number__c = reservationPhone;
    }
}