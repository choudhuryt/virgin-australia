@isTest(SeeAllData=true)
public class TestSetCaseParentRecordOwner {

    public static testMethod void CaseParentRecordOwner() 
    {
    	// This code runs as the "Corporate and Government User" user
        User u1;

        try
        {
	         u1 = [select Id from User WHERE IsActive=True AND Profile.Name = 'Integration Profile'  LIMIT 1];
        } 
        catch (QueryException qe)
        {
			System.debug(qe);
        }
										    	
	    Test.startTest();	

	    List<Case> newCases = new List<Case>();
	    for (Integer i = 0; i<1; i++) 
	    {
	            Case c1 = new Case(AccountId='0019000000AOTqQ' ,
                                    RecordTypeid = '0126F0000012HR6',
	            					Status = 'New',
	                                Priority='Low',
	                                NEW_Parent_Account1__c='00190000009QFgy' );
	            newCases.add(c1);
	   }
	  
          for (Integer i = 0; i<1; i++) 
	    {
	            Case c2 = new Case(AccountId='0019000001SbDjf' ,
                                    RecordTypeid = '0126F0000012HR6',
	            					Status = 'New',
	                                Priority='Low',
	                                NEW_Parent_Account1__c='00190000009QFgy' );
	            newCases.add(c2);
	   }
        
	   System.runas(u1)
       {
       		try
       		{
	        	insert newCases;
       		}
       		catch(Exception e)
       		{
       			System.debug(e);
       		}
	   }
	   
	   Test.stopTest();
	   
	   
    }
  
    

}