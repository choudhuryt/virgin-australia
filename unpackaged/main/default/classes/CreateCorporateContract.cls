public class CreateCorporateContract {
    
    @InvocableMethod
public static void CreateRelatedContract(List<Id> OppIds  )   
    {
        
         List<Opportunity> lstOppUpdate = new List<Opportunity>(); 
         ID c ;
        
         List<Opportunity> opplist =  [SELECT AccountId,Contract_Record_Type__c,Charter_Spend__c,ContractId,
                                       Contract_Type__c,Cos_Version__c,exNZ__c,ex_DPS__c,ExJP__c ,ex_Pacific_PNG__c, 
                                       Group_Spend__c,Id,OwnerId,Existing_SF_Contract__c ,Proposed_Contract_Duration__c,
                                       Proposed_Contract_End_Date__c,Proposed_Contract_Start_Date__c,
                                       RecordTypeId,StageName,Type,Name,EYEUZ1__c,EYEUZ2__c,EYEUZ3__c,
                                       EYMEZ1__c,EYMEZ2__c,EYMEZ3__c,EYMEZ4__c,SQEUOND__c,
                                       SQNAOND__c,SQSEAOND__c,SQWAAOND__c,SQ_Discount_Type__c , B2B_Request__c ,
                                       Objectives__c ,Description,Business_Justification__c,PRISM_Id__c,
                                       PRISM_Participant__c
                                       FROM Opportunity where id =:OppIds]; 
        
      
        
    

        if(opplist.size() > 0)
        {
          Contract contract = new Contract();
          contract.AccountId = opplist[0].AccountId ;
          contract.Opportunity__c   = opplist[0].Id ;
          contract.Name =  opplist[0].Name;
          contract.RecordTypeId = opplist[0].Contract_Record_Type__c;
          contract.Status = 'Draft';
          contract.OwnerId = opplist[0].OwnerId;
          contract.StartDate = opplist[0].Proposed_Contract_Start_Date__c ;
          contract.EndDate = opplist[0].Proposed_Contract_End_Date__c ;
          contract.Cos_Version__c =  opplist[0].Cos_Version__c ;
          contract.exNZ__c = opplist[0].exNZ__c ; 
          contract.exJP__c = opplist[0].exJP__c ;   
          contract.Is_PNG_DPS_PAC_Required__c = opplist[0].ex_DPS__c;
          contract.Is_PNG_DPS_PAC_Required__c = opplist[0].ex_Pacific_PNG__c ;
            If(opplist[0].PRISM_Participant__c == true)
            {
             contract.PRISM_Participant__c = 'Yes' ;  
            }                
            
            If(opplist[0].Type == 'Acquisition') {
                contract.Contract_Type__c ='New';
            } else{
                  contract.Contract_Type__c = opplist[0].Type;
                
            }
        
          contract.Contract_Version__c = 1  ; 
          contract.EUZ1ONDSelected__c  = opplist[0].EYEUZ1__c;
          contract.EUZ2ONDSelected__c = opplist[0].EYEUZ2__c;
          contract.EUZ3ONDSelected__c = opplist[0].EYEUZ3__c;
          contract.MEZ1ONDSelected__c = opplist[0].EYMEZ1__c;
          contract.MEZ2ONDSelected__c = opplist[0].EYMEZ2__c;
          contract.MEZ3ONDSelected__c = opplist[0].EYMEZ3__c;
          contract.MEZ4ONDSelected__c = opplist[0].EYMEZ4__c;
          contract.SQEUOND__c = opplist[0].SQEUOND__c;
          contract.SQNAOND__c = opplist[0].SQNAOND__c;
          contract.SQSEAOND__c = opplist[0].SQSEAOND__c;
          contract.SQWAAOND__c = opplist[0].SQWAAOND__c;
          contract.SQ_Discount_Type__c = opplist[0].SQ_Discount_Type__c ;   
          contract.B2B_Request__c = opplist[0].B2B_Request__c ; 
          contract.Other_Details__c = 	opplist[0].Objectives__c + '\r\n' +	opplist[0].Description + '\r\n' +
                                        opplist[0].Business_Justification__c ; 
           
            if(opplist[0].SQWAAOND__c <> null)
            {
             contract.SQWAADiscountAvailable__c = true;   
            }
             if(opplist[0].SQNAOND__c <> null)
            {
             contract.SQNADiscountAvailable__c = true;   
            }  
             if(opplist[0].SQSEAOND__c <> null)
            {
             contract.SQSEADiscountAvailable__c = true;   
            } 
            
            if(opplist.size() > 0 &&  opplist[0].Existing_SF_Contract__c <> null && opplist[0].Type == 'Revision' )
          {
              List<Contract> oldcon =  [SELECT Id,Status,Enddate
                                       FROM Contract where
                                       id =:opplist[0].Existing_SF_Contract__c
                                       and status in ('Activated','Expired')
                                      ]; 
            
            if(oldcon.size() > 0)
             {
               contract.Parent_Contract__c = opplist[0].Existing_SF_Contract__c ;
                 
              }
         }    
            
          try{
                insert contract;
             }
            catch(Exception ex)
             {
                 
                 
             }         
        }
        
        
            
        
           
        

    }
    
    
    
}