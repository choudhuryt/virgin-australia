@isTest
private class TestOpportunityProposalDetails
{
    
    static testMethod void myUnitTestOpp() 
    {
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';
        
        insert account;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.StageName = 'Sales Opportunity Analysis'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1; 
        
        Contract con = new Contract();
        con = commx.CreateOldStandardContract(0);
        con.AccountId = account.id;
        con.Opportunity__c = opp1.id ;
        insert con;
        
        
        
        Market__c market1 = new Market__c();
        market1.Opportunity__c = opp1.id ;
        market1.Contract__c = con.id ;
        market1.Name = 'DOM Mainline';
        
        Additional_Benefits__c addben1 = new  Additional_Benefits__c();
        addben1.Opportunity__c = opp1.id ;
        addben1.Contract__c = con.id ; 
        addben1.Name = opp1.Name ;
        
        Proposal_Table__c  prop1 = new  Proposal_Table__c();
        prop1.Opportunity__c = opp1.id ;
        prop1.Contract__c = con.id ;
        prop1.Name = 'DOM Mainline'; 
        
        Test.StartTest(); 
        
        PageReference pageRef = Page.OpportunityProposalDetails; 
        pageRef.getParameters().put('id', opp1.Id);
        Test.setCurrentPage(pageRef);
        OpportunityProposalDetailsController lController = new OpportunityProposalDetailsController();
        lController.initDisc();
        lController.DOM();   
        lController.INSH(); 
        lController.NA(); 
        lController.LON(); 
        lController.EY();    
        lController.SQ(); 
        lController.UA();
        lController.QR();
        lController.RED(); 
        lController.Cancel(); 
        
        Test.StopTest();
        
    }
    
    
    static testMethod void myUnitTestContract() 
    {
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.GDS_User__c =true;
        account.BillingCountry ='AU';
        
        insert account;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.StageName = 'Sales Opportunity Analysis'; 
        opp1.AccountId = account.id ;
        opp1.CloseDate = Date.today();
        opp1.Amount = 1000.00;
        insert opp1; 
        
        Contract con = new Contract();
        con = commx.CreateOldStandardContract(0);
        con.AccountId = account.id;
        con.Opportunity__c = opp1.id ;
        insert con;
                        
        Market__c market1 = new Market__c();
        market1.Opportunity__c = opp1.id ;
        market1.Contract__c = con.id ;
        market1.Name = 'DOM Mainline';
        
        Additional_Benefits__c addben1 = new  Additional_Benefits__c();
        addben1.Opportunity__c = opp1.id ;
        addben1.Contract__c = con.id ; 
        addben1.Name = opp1.Name ;
        
        Proposal_Table__c  prop1 = new  Proposal_Table__c();
        prop1.Opportunity__c = opp1.id ;
        prop1.Contract__c = con.id ;
        prop1.Name = 'DOM Mainline'; 
        
        Test.StartTest(); 
        
        PageReference pageRef = Page.OpportunityProposalDetails; 
        pageRef.getParameters().put('id', con.Id);
        Test.setCurrentPage(pageRef);
        ContractMarketDiscountViewController lController = new ContractMarketDiscountViewController();
        lController.initDisc();
        lController.DOM();   
        lController.INSH(); 
        lController.NA(); 
        lController.LON(); 
        lController.EY();    
        lController.SQ(); 
        lController.RED(); 
        lController.Cancel(); 
        
        Test.StopTest();
        
        
        
        
    }
}