/*
* 	Updated by Cloudwerx : Removed hardcoded references from the Other required fields
* 	Also, added additional field data to this class
* 
*
* 
*/

@isTest
private class AccountMasterTriggerTest 
{
    
    static testMethod void testforAccelerateInsert()
    {
        
        Id accRecTypeId = Utilities.getRecordTypeId('Account','Accelerate');            
        Id contactRecTypeId = Utilities.getRecordTypeId('Contact','Contact');            
        
        Contact billingContact = new Contact(lastName = 'Test',
                                             RecordTypeId = contactRecTypeId);
        insert billingContact;
        
        Account parentAcc = TestUtilityClass.createTestParentAccount();
        insert parentAcc;               
        
        Account accOne = new Account();
        accOne.name = 'Test Acc';
        //acc.RecordTypeId = '0126F0000012ziNQAQ';
        accOne.RecordTypeId = accRecTypeId;
        accOne.Waiver_Parent_Account__c = parentAcc.Id;
        accOne.Main_Corporate_TMC__c = parentAcc.Id;
        accOne.Waiver_Points_Allocated__c = 1000;
        accOne.Waiver_Points_Used__c = 10;        
        accOne.Account_Lifecycle__c = 'Opportunity';
        //acc.Nominated_Billing_Contact__c = '0032t000007vFH4AAM';
        accOne.Nominated_Billing_Contact__c = billingContact.Id;
        accOne.Industry_Type__c = 'AVI';
        accOne.Empower_Password__c = '12332';
        accOne.Business_Number__c = '897986';
        accOne.PRISM_ID__c = '5434';
        accOne.Administrator_Contact_Email__c = 'test@test.com';
        accOne.Website = 'http://virginaustralia--devsfrem.sandbox.lightning.force.com/lightning/r/Account/';
        insert accOne;
        
        Account accTwo = new Account();
        accTwo.name = 'Test Account';        
        accTwo.RecordTypeId = accRecTypeId;
        accTwo.Waiver_Parent_Account__c = parentAcc.Id;
        accTwo.Main_Corporate_TMC__c = parentAcc.Id;
        accTwo.Waiver_Points_Allocated__c = 10000;
        accTwo.Waiver_Points_Used__c = 100;        
        accTwo.Account_Lifecycle__c = 'Inactive';        
        accTwo.Nominated_Billing_Contact__c = billingContact.Id;
        accTwo.Industry_Type__c = 'AVI';
        accTwo.Empower_Password__c = '123324';
        accTwo.Business_Number__c = '8979862';
        accTwo.PRISM_ID__c = '54345';
        accTwo.Administrator_Contact_Email__c = 'testing.123@test.com';
        accTwo.Website = 'http://virginaustralia--devsfrem.sandbox.lightning.force.com/lightning/r/Account/31231';
        accTwo.Agreed_To_Terms_And_Conditions__c = true;
        accTwo.Accelerate_Batch_Update__c = false;
        insert accTwo;
        
        Test.startTest();
        
        Contract contractObj = TestUtilityClass.createTestContract(accOne.Id);
        insert contractObj; 
        
        Contact relatedContact = new Contact(lastName = 'Testing',
                                             AccountId = accOne.Id,
                                             SmartFly_Contact_Status__c = 'Key Contact',
                                             RecordTypeId = contactRecTypeId);
        insert relatedContact;
        
        List<Account> accList = new List<Account>();
        accList.add(accOne);
        accList.add(accTwo);
        
        AccountTriggerHandler.markDuplicates(accList);
        
        Test.stopTest();
    }
    
    static testMethod void testforAccelerateUpdate()
    {
        
        Id accRecTypeId = Utilities.getRecordTypeId('Account','Accelerate');            
        Id contactRecTypeId = Utilities.getRecordTypeId('Contact','Contact');            
        
        Contact billingContact = new Contact(lastName = 'Test',
                                             RecordTypeId = contactRecTypeId);
        insert billingContact;
        
        Account parentAcc = TestUtilityClass.createTestParentAccount();
        insert parentAcc;
        
        account acc = new account();
        acc.name = 'Test Acc';        
        acc.RecordTypeId = accRecTypeId;
        acc.Waiver_Parent_Account__c = parentAcc.Id;
        acc.Main_Corporate_TMC__c = parentAcc.Id;
        acc.Waiver_Points_Allocated__c = 1000;
        acc.Waiver_Points_Used__c = 10;        
        acc.Account_Lifecycle__c = 'Opportunity';        
        acc.Nominated_Billing_Contact__c = billingContact.Id;
        acc.Industry_Type__c = 'AVI';
        acc.Empower_Password__c = '12332';        
        acc.PRISM_ID__c = '5434';
        acc.Administrator_Contact_Email__c = 'test@test.com';
        acc.Website = 'http://virginaustralia--devsfrem.sandbox.lightning.force.com/lightning/r/Account/';
        insert acc;                        
        
        Test.startTest();
        
        Contract contractObj = TestUtilityClass.createTestContract(acc.Id);
        insert contractObj; 
        
        Contact relatedContact = new Contact(lastName = 'Testing',
                                             AccountId = acc.Id,
                                             SmartFly_Contact_Status__c = 'Key Contact',
                                             RecordTypeId = contactRecTypeId);        
        insert relatedContact;
        
        List<Account> accList = new List<Account>();
        accList.add(acc);        
        AccountTriggerHandler.markDuplicates(accList);                
        
        User userObj = [Select id,Level__c From User Where Id =: UserInfo.getUserId()];
        userObj.level__c = 3;
        update userObj;
        
        Account accToUpdate = new Account();
        accToUpdate.Id = acc.Id;        
        accToUpdate.Account_Lifecycle__c = 'Inactive';
        update accToUpdate;
        
        Test.stopTest();
    }
    
    static testMethod void testforSmartFly()
    {
        
        Id accRecTypeId = Utilities.getRecordTypeId('Account','SmartFly');            
        Id contactRecTypeId = Utilities.getRecordTypeId('Contact','Contact');            
        Id contractRecTypeSmartFlyId = Utilities.getRecordTypeId('Contract','SmartFly');            
        
        Contact billingContact = new Contact(lastName = 'Test',
                                             RecordTypeId = contactRecTypeId);
        insert billingContact;
        
        Account parentAcc = TestUtilityClass.createTestParentAccount();
        insert parentAcc;
        
        account acc = new account();
        acc.name = 'Test Acc';
        //acc.RecordTypeId = '0126F0000012ziNQAQ';
        acc.RecordTypeId = accRecTypeId;
        acc.Waiver_Parent_Account__c = parentAcc.Id;
        acc.Main_Corporate_TMC__c = parentAcc.Id;
        acc.Waiver_Points_Allocated__c = 1000;
        acc.Waiver_Points_Used__c = 10;        
        acc.Account_Lifecycle__c = 'Opportunity';
        //acc.Nominated_Billing_Contact__c = '0032t000007vFH4AAM';
        acc.Nominated_Billing_Contact__c = billingContact.Id;
        acc.Industry_Type__c = 'AVI';
        acc.Empower_Password__c = '12332';
        acc.Business_Number__c = '897986';
        acc.Administrator_Contact_Email__c = 'test@test.com';
        acc.Website = 'http://virginaustralia--devsfrem.sandbox.lightning.force.com/lightning/r/Account/';
        insert acc;      
        
        Test.startTest();
        
        Contract contractObj = TestUtilityClass.createTestContract(acc.Id);
        contractObj.RecordTypeId = contractRecTypeSmartFlyId;
        contractObj.Type__c = 'Rebate';
        contractObj.AccountId = acc.Id;
        insert contractObj; 
        
        Contact relatedContact = new Contact(lastName = 'Testing',
                                             AccountId = acc.Id,
                                             SmartFly_Contact_Status__c = 'Key Contact',
                                             RecordTypeId = contactRecTypeId);
        insert relatedContact;                
        
        Account accToUpdate = new Account();
        accToUpdate.Id = acc.Id;
        accToUpdate.Business_Number__c = '222';
        update accToUpdate;
        
        Test.stopTest();
    }
    
    
    static testMethod void testforFlyPlus()
    {
        
        Id accRecTypeId = Utilities.getRecordTypeId('Account','FlyPlus');            
        Id contactRecTypeId = Utilities.getRecordTypeId('Contact','Contact');            
        Id contractRecTypeFlyPlusId = Utilities.getRecordTypeId('Contract','FlyPlus');            
        
        Contact billingContact = new Contact(lastName = 'Test',
                                             RecordTypeId = contactRecTypeId);
        insert billingContact;
        
        Account parentAcc = TestUtilityClass.createTestParentAccount();
        insert parentAcc;
        
        account acc = new account();
        acc.name = 'Test Acc';
        //acc.RecordTypeId = '0126F0000012ziNQAQ';
        acc.RecordTypeId = accRecTypeId;
        acc.Waiver_Parent_Account__c = parentAcc.Id;
        acc.Main_Corporate_TMC__c = parentAcc.Id;
        acc.Waiver_Points_Allocated__c = 1000;
        acc.Waiver_Points_Used__c = 10;        
        acc.Account_Lifecycle__c = 'Opportunity';
        //acc.Nominated_Billing_Contact__c = '0032t000007vFH4AAM';
        acc.Nominated_Billing_Contact__c = billingContact.Id;
        acc.Industry_Type__c = 'AVI';
        acc.Empower_Password__c = '12332';
        acc.Business_Number__c = '897986';
        acc.Administrator_Contact_Email__c = 'test@test.com';
        acc.Website = 'http://virginaustralia--devsfrem.sandbox.lightning.force.com/lightning/r/Account/';
        insert acc;
        
        Test.startTest();
        
        Contract contractObj = TestUtilityClass.createTestContract(acc.Id);
        contractObj.RecordTypeId = contractRecTypeFlyPlusId;
        insert contractObj; 
        
        Contact relatedContact = new Contact(lastName = 'Testing',
                                             AccountId = acc.Id,
                                             SmartFly_Contact_Status__c = 'Key Contact',
                                             RecordTypeId = contactRecTypeId);
        insert relatedContact;               
        
        Account accToUpdate = new Account();
        accToUpdate.Id = acc.Id;
        accToUpdate.Business_Number__c = '3322';
        update accToUpdate;
        
        Test.stopTest();
    }
    
}