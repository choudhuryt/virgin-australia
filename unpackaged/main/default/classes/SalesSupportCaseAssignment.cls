public class SalesSupportCaseAssignment
{
@InvocableMethod
    public static void SalesSupportCaseLink( List<Id> CaseIds )
  {
      string AccountIDName ;
      string ContractNum ;
      string VSMIdName;
      string Duedate ;
      List<Case> caseUpdate = new List<Case>(); 
      List<Account> accList = new List<Account>(); 
      List<Contract> conList = new List<Contract>(); 
      List<Velocity_Status_Match__c> vsmList = new List<Velocity_Status_Match__c>(); 
      List<Case> vsmcaselist = new List<Case>();
      List<Case> wfcaselist  = new List<Case>();
      List<Case> agentratecaselist  = new List<Case>();
      List<Case> foccaselist  = new List<Case>();
      List<Case> conextncaselist  = new List<Case>();
      List<Case> contermcaselist  = new List<Case>();
      List<Case> indcontractcaseList = new List<Case>();
      List<Case> corpcontractcaseList = new List<Case>();
      List<Case> corpfarechangecaseList = new List<Case>();
      
       corpcontractcaseList    =  [select id ,Subject,accountid,	Category__c,Case_Due_Date__c,
                          Sub_Category__c,Query_Type__c,Description  
                          from case 
                          where id=:caseIds and subject like '%Corporate Contract%has been approved%' 
                         ];
      
      indcontractcaseList    =  [select id ,Subject,accountid,	Category__c,Case_Due_Date__c,
                          Sub_Category__c,Query_Type__c,Description  
                          from case 
                          where id=:caseIds and subject like '%Industry Contract%has been approved%' 
                         ];
      
       vsmcaselist    =  [select id ,Subject,accountid,	Category__c,Case_Due_Date__c,
                          Sub_Category__c,Query_Type__c,Description  
                          from case 
                          where id=:caseIds and subject like '%Velocity Status Match/Upgrade Request Approved%' 
                         ];
      
      foccaselist    =  [select id ,Subject,accountid,	Category__c,Case_Due_Date__c,
                          Sub_Category__c,Query_Type__c,Description  
                          from case 
                          where id=:caseIds and subject like '%Please Process the following FOC Ticket Request%' 
                         ];
      
      wfcaselist    =  [select id ,Subject,accountid,	Category__c,Case_Due_Date__c,
                          Sub_Category__c,Query_Type__c,Description  
                          from case 
                          where id=:caseIds and subject like '%Waiver & Favour Request for%Approved%' 
                         ];
      agentratecaselist    =  [select id ,Subject,accountid,	Category__c,Case_Due_Date__c,
                          Sub_Category__c,Query_Type__c,Description  
                          from case 
                          where id=:caseIds and subject like '%Please process the following Agent Rate Request%' 
                         ]; 
      
      conextncaselist    =  [select id ,Subject,accountid,	Category__c,Case_Due_Date__c,
                          Sub_Category__c,Query_Type__c,Description  
                          from case 
                          where id=:caseIds and subject like '%Contract Extension%has been approved%' 
                         ]; 
      
     corpfarechangecaseList   =  [select id ,Subject,accountid,	Category__c,Case_Due_Date__c,
                          Sub_Category__c,Query_Type__c,Description  
                          from case 
                          where id=:caseIds and subject like 'Corporate Fares%' 
                         ]; 



      
       if (vsmcaselist.size() > 0)
     {
         
       AccountIDName =    vsmcaselist[0].Description.substringAfter('Account ID: ').substring(1,16).replaceAll( '\\s+', '');
         
        VSMIDName =   vsmcaselist[0].Description.substringAfter('VSM Record: ').substring(0,15).replaceAll( '\\s+', '');
      
       Duedate =    vsmcaselist[0].Description.substringAfter('Due Date : ').substring(0,10).replaceAll( '\\s+', '');
         
         
       system.debug('This is the description and id' + AccountIDName+ Duedate  + VSMIDName ) ;   
       accList  =  [select id 
                    from Account 
                    where  id = : AccountIDName  limit 1
                   ]; 
         
       vsmlist =[select id 
                    from Velocity_Status_Match__c 
                    where  id = : VSMIDName  limit 1
                   ]; 
       for(Case c: vsmcaselist)
       { 
       c.Category__c = 'Approval Task' ; 
       c.Sub_Category__c =  'Velocity Upgrade' ;   
       c.Query_Type__c = 'Internal';
       c.Case_Due_Date__c =  date.parse(Duedate) ;  
        
       if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
           
       }
           
       if(vsmlist.size() > 0)
       {
          c.Velocity_Status_Upgrade__c =  vsmList[0].id ;
       }
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     }    
      
       
       if (wfcaselist.size() > 0)
     {
         
       AccountIDName =    wfcaselist[0].Description.substringAfter('Account ID: ').substring(0,16).replaceAll( '\\s+', '');
      
       Duedate =    wfcaselist[0].Description.substringAfter('Due Date : ').substring(0,10).replaceAll( '\\s+', '');
       system.debug('This is the description and id' + AccountIDName+ Duedate  ) ;   
       accList  =  [select id 
                    from Account 
                    where  id = : AccountIDName  limit 1
                   ]; 
         
       for(Case c: wfcaselist)
       { 
       c.Category__c = 'Approval Task' ; 
       c.Sub_Category__c =  'Waiver Request' ;   
       c.Query_Type__c = 'Internal';
       c.Case_Due_Date__c =  date.parse(Duedate) ;   
       if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
           
       }
       
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     }    
      
          if (foccaselist.size() > 0)
     {
         
       AccountIDName =    foccaselist[0].Description.substringAfter('Account ID: ').substring(0,16).replaceAll( '\\s+', '');
      
       Duedate =    foccaselist[0].Description.substringAfter('Due Date : ').substring(0,10).replaceAll( '\\s+', '');
       system.debug('This is the description and id' + AccountIDName+ Duedate  ) ;   
       accList  =  [select id 
                    from Account 
                    where  id = : AccountIDName  limit 1
                   ]; 
         
       for(Case c: foccaselist)
       { 
       c.Category__c = 'Approval Task' ; 
       c.Sub_Category__c =  'FOC Request' ;   
       c.Query_Type__c = 'Internal';
       c.Case_Due_Date__c =  date.parse(Duedate) ;   
       if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
           
       }
       
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     }   
      
      
        if (agentratecaselist.size() > 0)
     {
         
       AccountIDName =    agentratecaselist[0].Description.substringAfter('Account ID: ').substring(0,16).replaceAll( '\\s+', '');
      
       Duedate =    agentratecaselist[0].Description.substringAfter('Due Date : ').substring(0,10).replaceAll( '\\s+', '');
       system.debug('This is the description and id' + AccountIDName+ Duedate  ) ;   
       accList  =  [select id 
                    from Account 
                    where  id = : AccountIDName  limit 1
                   ]; 
         
       for(Case c: agentratecaselist)
       { 
       c.Category__c = 'Approval Task' ; 
       c.Sub_Category__c =  'AgentRate Request' ;   
       c.Query_Type__c = 'Internal';
       c.Case_Due_Date__c =  date.parse(Duedate) ;   
       if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
           
       }
       
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     } 
      
      
       if (conextncaselist.size() > 0)
     {
         
       AccountIDName =    conextncaselist[0].Description.substringAfter('Account ID: ').substring(0,16).replaceAll( '\\s+', '');
      
       
       system.debug('This is the description and id' + AccountIDName  ) ;   
       
       accList  =  [select id 
                    from Account 
                    where  id = : AccountIDName  limit 1
                   ]; 
         
       for(Case c: conextncaselist)
       { 
       c.Category__c = 'Extension/Termination' ; 
       c.Sub_Category__c =  'Extension' ;   
       c.Query_Type__c = 'Internal';
             if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
           
       }
       
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     }  
      
      
        if (contermcaselist.size() > 0)
     {
         
       AccountIDName =    contermcaselist[0].Description.substringAfter('Account ID: ').substring(0,16).replaceAll( '\\s+', '');
      
       
       system.debug('This is the description and id' + AccountIDName  ) ;   
       
       accList  =  [select id 
                    from Account 
                    where  id = : AccountIDName  limit 1
                   ]; 
         
       for(Case c: contermcaselist)
       { 
       c.Category__c = 'Extension/Termination' ; 
       c.Sub_Category__c =  'Termination' ;   
       c.Query_Type__c = 'Internal';
             if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
           
       }
       
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     }    
       if (corpcontractcaseList.size() > 0 )
     {
         
       AccountIDName =    corpcontractcaseList[0].Description.substringAfter('Account ID: ').substring(0,16).replaceAll( '\\s+', '');
      
       
       system.debug('This is the description and id' + AccountIDName  ) ;   
       
       accList  =  [select id 
                    from Account 
                    where  id = : AccountIDName  limit 1
                   ]; 
         
       for(Case c: corpcontractcaseList)
       { 
       c.Category__c = 'Contract Implementation' ;       
       c.Query_Type__c = 'Internal';
             if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
           
       }
       
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     }    
     if (indcontractcaseList.size() > 0 )
     {
         
       AccountIDName =    indcontractcaseList[0].Description.substringAfter('Account ID: ').substring(0,16).replaceAll( '\\s+', '');
      
              system.debug('This is the description and id' + AccountIDName  ) ;   
       
       accList  =  [select id 
                    from Account 
                    where  id = : AccountIDName  limit 1
                   ]; 
         
       for(Case c: indcontractcaseList)
       { 
       c.Category__c = 'Contract Implementation' ;       
       c.Query_Type__c = 'Internal';
             if(accList.size() > 0) 
       {
            c.accountid = accList[0].id ;
           
       }
       
         caseUpdate.add(c);  
       }
        update caseUpdate;       
     }    
      
      if (corpfarechangecaseList.size() > 0)
     {
        Integer intIndex = corpfarechangecaseList[0].subject.indexOf('00'); 
        ContractNum =  corpfarechangecaseList[0].subject.substring( intIndex, intIndex+8 );
        system.debug('This is contract number' + ContractNum  );
        conList  =     [SELECT id, AccountId FROM Contract WHERE ContractNumber =: ContractNum  limit 1];
         
         system.debug('This is contract number' + ContractNum  + 'The list' + conList  );
        system.debug( conlist);      
        for(Case c: corpfarechangecaseList)
        { 
           c.Category__c = 'Faresheet' ;       
           c.Query_Type__c = 'Internal';
          if(conList.size() > 0) 
           {
               c.accountid  = conList[0].AccountId ;
            }
          caseUpdate.add(c);      
         }
           update caseUpdate;  
     }
  } 
    
}