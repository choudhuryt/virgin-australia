/*
* Author: Warjie Malibago (Accenture CloudFirst)
* Date: 31st May, 2016
* Description: C# 107599; Test class for OpportunityRedirectController
* Revision history:
* 05.OCT.2016 - Michi Magsarili - Added additional field values to the Account object (00130081 fix)
* @UpdateBy : cloudwerx
*/
@isTest
private class OpportunityRedirectController_Test {
    
    public static String opportunityVelocityRecordTypeId = Utilities.getRecordTypeId('Opportunity', 'Velocity');
    public static String opportunityCorporateRecordTypeId = Utilities.getRecordTypeId('Opportunity', 'Corporate');
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;  
        
        Opportunity testOpportunityObj = TestDataFactory.createTestOpportunity('testOpp1', 'Sales Opportunity Analysis', testAccountObj.Id, Date.today(), 1.00);
        INSERT testOpportunityObj; 
    }
    
    @isTest
    private static void testOpportunityRedirectController() {
        Opportunity testOpp = [SELECT Id FROM Opportunity];
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Test.startTest();
        ApexPages.standardController sc = new ApexPages.standardController(testOpp);
        OpportunityRedirectController opportunityRedirectCtrlObj = new OpportunityRedirectController(sc);
        PageReference pageRef = Page.OpportunityRedirect;
        pageRef.getParameters().put('006/', 'e?');
        pageRef.getParameters().put('&nooverride=', '0');
        pageRef.getParameters().put('&RecordType=', opportunityCorporateRecordTypeId);
        pageRef.getParameters().put('&ent=', 'Opportunity');
        pageRef.getParameters().put('&opp11=', 'Open');
        pageRef.getParameters().put('accid', accountId);
        pageRef.getParameters().put('&00N90000003ypnf=', 'Planning');
        Test.setCurrentPage(pageRef);
        
        opportunityRedirectCtrlObj.OpportunityRedirectController();
        opportunityRedirectCtrlObj.recType = 'Corporate';
        List<SelectOption> recordTypeOptions = opportunityRedirectCtrlObj.getItems();
        PageReference proceedPageReference = opportunityRedirectCtrlObj.proceed();
        PageReference cancelPageReference = opportunityRedirectCtrlObj.cancel();
        Test.stopTest();
        //Asserts
        system.assertEquals(opportunityCorporateRecordTypeId, opportunityRedirectCtrlObj.oppRecMap.get(opportunityRedirectCtrlObj.recType));
        system.assertEquals('/apex/newcorporateopportunity?id='+accountId, proceedPageReference.getUrl());
        system.assertEquals('/006', cancelPageReference.getUrl());
        List<RecordType> oppRecordTypes = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity'];
        system.assertEquals(oppRecordTypes.size(), recordTypeOptions.size());
    }
    
    @isTest
    private static void testOpportunityRedirectControllerWithVelocityId() {
        Opportunity testOpp = [SELECT Id FROM Opportunity];
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        
        Test.startTest();
        ApexPages.standardController sc = new ApexPages.standardController(testOpp);
        OpportunityRedirectController opportunityRedirectCtrlObj = new OpportunityRedirectController(sc);
        PageReference pageRef = Page.OpportunityRedirect;
        pageRef.getParameters().put('006/', 'e?');
        pageRef.getParameters().put('&nooverride=', '0');
        pageRef.getParameters().put('&RecordType=', opportunityVelocityRecordTypeId);
        pageRef.getParameters().put('&ent=', 'Opportunity');
        pageRef.getParameters().put('&opp11=', 'Open');
        pageRef.getParameters().put('accid', accountId);
        pageRef.getParameters().put('&00N90000003ypnf=', 'Planning');
        Test.setCurrentPage(pageRef);
        
        opportunityRedirectCtrlObj.OpportunityRedirectController();
        opportunityRedirectCtrlObj.recType = 'Velocity';
        List<SelectOption> recordTypeOptions = opportunityRedirectCtrlObj.getItems();
        PageReference proceedPageReference = opportunityRedirectCtrlObj.proceed();
        PageReference cancelPageReference = opportunityRedirectCtrlObj.cancel();
        Test.stopTest();
        //Asserts
        system.assertEquals(opportunityVelocityRecordTypeId, opportunityRedirectCtrlObj.oppRecMap.get(opportunityRedirectCtrlObj.recType));
        system.assertEquals('/006/e?ent=Opportunity&nooverride=1&opp4=TEST177&RecordType=' + opportunityVelocityRecordTypeId + '&retURL=%2F006%2Fo', proceedPageReference.getUrl());
        system.assertEquals('/006', cancelPageReference.getUrl());
        List<RecordType> oppRecordTypes = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity'];
        system.assertEquals(oppRecordTypes.size(), recordTypeOptions.size());
    }
    
    @isTest
    private static void testOpportunityRedirectControllerWithNoAccount() {
        Opportunity testOpp = [SELECT Id, RecordTypeId FROM Opportunity];
        testOpp.RecordTypeId = opportunityCorporateRecordTypeId;
        UPDATE testOpp;
        Test.startTest();
        ApexPages.standardController sc = new ApexPages.standardController(testOpp);
        OpportunityRedirectController opportunityRedirectCtrlObj = new OpportunityRedirectController(sc);
        PageReference pageRef = Page.OpportunityRedirect;
        pageRef.getParameters().put('006/', 'e?');
        pageRef.getParameters().put('&nooverride=', '0');
        pageRef.getParameters().put('&RecordType=', opportunityCorporateRecordTypeId);
        pageRef.getParameters().put('&ent=', 'Opportunity');
        pageRef.getParameters().put('&opp11=', 'Open');
        pageRef.getParameters().put('&00N90000003ypnf=', 'Planning');
        Test.setCurrentPage(pageRef);
        
        opportunityRedirectCtrlObj.OpportunityRedirectController();
        opportunityRedirectCtrlObj.recType = 'Corporate';
        List<SelectOption> recordTypeOptions = opportunityRedirectCtrlObj.getItems();
        PageReference proceedPageReference = opportunityRedirectCtrlObj.proceed();
        PageReference cancelPageReference = opportunityRedirectCtrlObj.cancel();
        Test.stopTest();
        //Asserts
        system.assertEquals(opportunityCorporateRecordTypeId, opportunityRedirectCtrlObj.oppRecMap.get(opportunityRedirectCtrlObj.recType));
        system.assertEquals('/apex/newcorporateopportunity', proceedPageReference.getUrl());
        system.assertEquals('/006', cancelPageReference.getUrl());
        List<RecordType> oppRecordTypes = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity'];
        system.assertEquals(oppRecordTypes.size(), recordTypeOptions.size());
    }
    
    @isTest
    private static void testOpportunityRedirectControllerWithNoAccountOtherType() {
        Opportunity testOpp = [SELECT Id FROM Opportunity];
        Test.startTest();
        ApexPages.standardController sc = new ApexPages.standardController(testOpp);
        OpportunityRedirectController opportunityRedirectCtrlObj = new OpportunityRedirectController(sc);
        PageReference pageRef = Page.OpportunityRedirect;
        pageRef.getParameters().put('006/', 'e?');
        pageRef.getParameters().put('&nooverride=', '0');
        pageRef.getParameters().put('&RecordType=', opportunityVelocityRecordTypeId);
        pageRef.getParameters().put('&ent=', 'Opportunity');
        pageRef.getParameters().put('&opp11=', 'Open');
        pageRef.getParameters().put('&00N90000003ypnf=', 'Planning');
        Test.setCurrentPage(pageRef);
        
        opportunityRedirectCtrlObj.OpportunityRedirectController();
        opportunityRedirectCtrlObj.recType = 'Velocity';
        List<SelectOption> recordTypeOptions = opportunityRedirectCtrlObj.getItems();
        PageReference proceedPageReference = opportunityRedirectCtrlObj.proceed();
        PageReference cancelPageReference = opportunityRedirectCtrlObj.cancel();
        Test.stopTest();
        //Asserts
        system.assertEquals(opportunityVelocityRecordTypeId, opportunityRedirectCtrlObj.oppRecMap.get(opportunityRedirectCtrlObj.recType));
        system.assertEquals('/006/e?ent=Opportunity&nooverride=1&RecordType=' + opportunityVelocityRecordTypeId + '&retURL=%2F006%2Fo', proceedPageReference.getUrl());
        system.assertEquals('/006', cancelPageReference.getUrl());
        List<RecordType> oppRecordTypes = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity'];
        system.assertEquals(oppRecordTypes.size(), recordTypeOptions.size());
    }
}