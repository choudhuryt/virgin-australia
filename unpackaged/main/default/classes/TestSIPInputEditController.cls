/**
* @description       : Test class for SIPInputEditController
* @createdBy         : Cloudwerx
* @Updated By        : Cloudwerx
**/
@isTest
private class TestSIPInputEditController {
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('Account Closed', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        
        SIP_Header__c testSIPHeaderObj = TestDataFactory.createTestSIPHeader(testContractObj.Id, 'SIP Header Description', 1000, 2000);
        INSERT testSIPHeaderObj;
    }
    
    @isTest
    private static void testSIPInputEditController() {
        Contract testContractObj = [SELECT Id FROM Contract LIMIT 1];
        Test.startTest();
        ApexPages.currentPage().getParameters().put('cid', testContractObj.Id);
        SIPInputEditController sioInputEditController  = new SIPInputEditController();
        List<SIPInputEditController.SIPHeaderWrapper> sipHeaderWrappers = sioInputEditController.GetSIPHeaderList();
        PageReference savePageReference = sioInputEditController.Save();
        PageReference saveAndClosePageReference = sioInputEditController.SaveAndClose();
        Test.stopTest();
        // Asserts
        System.assertEquals(null, savePageReference);
        System.assertEquals(null, saveAndClosePageReference);
        System.assertEquals(true, sipHeaderWrappers[0].SIPHeaderItem != null);
        System.assertEquals([SELECT Id FROM SIP_Header__c]?.Id, sipHeaderWrappers[0].SIPHeaderItem.Id);
    }
}