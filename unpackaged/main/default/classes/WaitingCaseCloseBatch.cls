global class WaitingCaseCloseBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        Date expDate = System.today().addDays(-15);
        Date tExpDate = System.today().addDays(0);
        String cStatus = 'Waiting for the Member';
        String rtName = 'Velocity Member Support';
        String query;
        System.debug('expDate--'+expDate);
        if(Test.isRunningTest()){
            query = 'Select Id,Status from Case where Waiting_Member_Status_Time__c >:tExpDate AND RecordType.Name =:rtName AND Status =:cStatus';
        }else{
            query = 'Select Id,Status,CaseNumber,Waiting_Member_Status_Time__c from Case where Waiting_Member_Status_Time__c <=:expDate AND RecordType.Name =:rtName AND Status =:cStatus';
        }        
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Case> caseList){
        System.debug('caseList--'+caseList);
        List<Case> upCaseList = new List<Case>();
        List<CaseComment> commentList = new List<CaseComment>();
        
        for(Case cs: caseList){
            Case cas = new Case();
            cas.Id = cs.Id;
            cas.Status = 'Closed';
            cas.Comments__c = 'Closed by the system due to no response from customer.';
            upCaseList.add(cas);
            
            CaseComment comm = new CaseComment();
            comm.ParentId = cs.Id;
            comm.CommentBody = 'Closed by the system due to no response from customer.';
            commentList.add(comm);
        }
        System.debug('upCaseList--'+upCaseList);
        System.debug('commentList--'+commentList);
        
        if(upCaseList.size()>0){
            update upCaseList;
        }
        if(commentList.size()>0){
            insert commentList;
        }
    }
    global void finish(Database.BatchableContext BC){
        
    }
}