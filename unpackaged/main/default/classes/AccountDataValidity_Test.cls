/*
* Author: Warjie Malibago (Accenture CloudFirst)
* Date: June 2, 2016
* Description: C# 108018 | June - Increase Overall Test Coverage
* 
* Updated by cloudwerx : changed profile name VA System Administrator to Integration Profile at line 13
*/
@isTest
private class AccountDataValidity_Test {
    
    private static testMethod void testAfterInsert() {
        User u = TestUtilityClass.createTestUser();
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Integration Profile' LIMIT 1].Id;
        insert u;
        
        system.runAs(u){
            Account acc = TestUtilityClass.createTestAccount();
            insert acc;
            
            Account acc2 = TestUtilityClass.createTestAccount();
            insert acc2;
            
            User u2 = TestUtilityClass.createTestUser(); 
            u2.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Industry Sales Manager' LIMIT 1].Id;
            insert u2;
            
            //Test.startTest();
            Account_Data_Validity__c adv = TestUtilityClass.createAccountDataValidity(acc.Id, u2.Id, acc2.Id);
            adv.From_Date__c = System.today()-10;
            adv.To_Date__c = System.today();
            insert adv;
            
            Account_Data_Validity__c adv1 = TestUtilityClass.createAccountDataValidity(acc.Id, u2.Id, acc2.Id);
            adv1.From_Date__c = System.today()-1;
            insert adv1;
            
            system.assertEquals(1, [SELECT COUNT() FROM Account WHERE Id =: adv.From_Account__c]);
            //Test.stopTest();
        }
    }
    
}