/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
* @updatedBy : cloudwerx
*/
@isTest
private class SIPTableSelectorControllerUnitTest {
    
    @testSetup static void setup() {
        Id contractSmartFlyRecordTypeId = Utilities.getRecordTypeId('Contract', 'SmartFly');
        Account testAccountObj = TestDataFactory.createTestAccount('Account Closed', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('Test Contract', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj.RecordTypeId = contractSmartFlyRecordTypeId;
        testContractObj.Type__c = 'Rebate';
        INSERT testContractObj;
        
        List<SIP_Header__c> testSIPHeaders = new List<SIP_Header__c>();
        testSIPHeaders.add(TestDataFactory.createTestSIPHeader(testContractObj.Id, 'All Revenue', 1000, 2000));
        testSIPHeaders.add(TestDataFactory.createTestSIPHeader(testContractObj.Id, 'Domestic Revenue', 1000, 2000));
        testSIPHeaders.add(TestDataFactory.createTestSIPHeader(testContractObj.Id, 'International Revenue', 1000, 2000));
        testSIPHeaders.add(TestDataFactory.createTestSIPHeader(testContractObj.Id, 'Umbrella Revenue', 1000, 2000));
        testSIPHeaders.add(TestDataFactory.createTestSIPHeader(testContractObj.Id, 'Key Market Revenue', 1000, 2000));
        INSERT testSIPHeaders;
        
        SIP_Selection__c testSIPSelectionObj = TestDataFactory.createTestSIPSelection('Rebate', 'SmartFly', 1, 'SIPSelection_' + Datetime.now());
        INSERT testSIPSelectionObj;
        
        SIP_Option__c testSIPOptionObj = TestDataFactory.createTestSIPOption('All Revenue', '1', testSIPSelectionObj.Id, 1, 'AllSIPOption_' + Datetime.now());
        INSERT testSIPOptionObj;
    }
    
    @isTest
    private static void testSIPTableSelectorControllerAllRevenue() {
        Contract testContractObj = [SELECT Id FROM Contract];
        SIP_Option__c testSIPOptionObj = [SELECT Id FROM SIP_Option__c];
        Test.startTest();
        SIPTableSelectorController sipTableSelectorObj = getSIPTableSelectorControllerObj(testContractObj, testSIPOptionObj);
        PageReference checkExistedPageReference = sipTableSelectorObj.CheckExisted();
        PageReference cancelPageReference = sipTableSelectorObj.Cancel();
        List<SIPTableSelectorController.SIPSelectionWrapper> sipSelectionWrapperObj = sipTableSelectorObj.getSIPOption();
        PageReference nextPageReference = sipTableSelectorObj.Next();
        String contractType = sipTableSelectorObj.GetContractType();
        String contactId = sipTableSelectorObj.GetContractID();
        Test.stopTest();
        //Asserts
        system.assertEquals('/' + testContractObj.Id, cancelPageReference.getUrl());
        System.assertEquals('There are existing Domestic,International ,Umbrella and Key Market revenue SIP tables for this contract.If you want to add new tables, please delete the existing ones  and if you want to edit the existing tables, please click on the Edit SIP Table button', ApexPages.getMessages()[0].getSummary());
        System.assertEquals(ApexPages.SEVERITY.ERROR, ApexPages.getMessages()[0].getSeverity());
    }
    
    @isTest
    private static void testSIPTableSelectorControllerDomRevenue() {
        Contract testContractObj = [SELECT Id FROM Contract];
        SIP_Option__c testSIPOptionObj = [SELECT Id, Description__c FROM SIP_Option__c];
        testSIPOptionObj.Description__c = 'Domestic Revenue';
        UPDATE testSIPOptionObj;
        
        Test.startTest();
        SIPTableSelectorController sipTableSelectorObj = getSIPTableSelectorControllerObj(testContractObj, testSIPOptionObj);
        List<SIPTableSelectorController.SIPSelectionWrapperDom> sipSelectionWrapperDomObj = sipTableSelectorObj.getSIPOptionDom();
        PageReference nextPageReference = sipTableSelectorObj.Next();
        Test.stopTest();
        //Asserts
        system.assertEquals(true, sipSelectionWrapperDomObj.size() > 0);
        system.assertEquals(true, sipSelectionWrapperDomObj[0].selecteddom);
        system.assertEquals(true, sipSelectionWrapperDomObj[0].sip_optiondom != null);
    }
    
    @isTest
    private static void testSIPTableSelectorControllerInternationalRevenue() {
        Contract testContractObj = [SELECT Id FROM Contract];
        SIP_Option__c testSIPOptionObj = [SELECT Id, Description__c FROM SIP_Option__c];
        testSIPOptionObj.Description__c = 'International Revenue';
        UPDATE testSIPOptionObj;
        
        Test.startTest();
        SIPTableSelectorController sipTableSelectorObj = getSIPTableSelectorControllerObj(testContractObj, testSIPOptionObj);
        List<SIPTableSelectorController.SIPSelectionWrapperInt> sipSelectionWrapperIntObj = sipTableSelectorObj.getSIPOptionInt();
        PageReference nextPageReference = sipTableSelectorObj.Next();
        Test.stopTest();
        //Asserts
        system.assertEquals(true, sipSelectionWrapperIntObj.size() > 0);
        system.assertEquals(true, sipSelectionWrapperIntObj[0].selectedint);
        system.assertEquals(true, sipSelectionWrapperIntObj[0].sip_optionint != null);
    }
    
    @isTest
    private static void testSIPTableSelectorControllerUmbRevenue() {
        Contract testContractObj = [SELECT Id FROM Contract];
        SIP_Option__c testSIPOptionObj = [SELECT Id, Description__c FROM SIP_Option__c];
        testSIPOptionObj.Description__c = 'Umbrella Revenue';
        UPDATE testSIPOptionObj;
        
        Test.startTest();
        SIPTableSelectorController sipTableSelectorObj = getSIPTableSelectorControllerObj(testContractObj, testSIPOptionObj);
        List<SIPTableSelectorController.SIPSelectionWrapperUmb> sipSelectionWrapperUmbObj = sipTableSelectorObj.getSIPOptionUmb();
        PageReference nextPageReference = sipTableSelectorObj.Next();
        Test.stopTest();
        //Asserts
        system.assertEquals(true, sipSelectionWrapperUmbObj.size() > 0);
        system.assertEquals(true, sipSelectionWrapperUmbObj[0].selectedumb);
        system.assertEquals(true, sipSelectionWrapperUmbObj[0].sip_optionumb != null);
    }
    
    @isTest
    private static void testSIPTableSelectorControllerKeyRevenue() {
        Contract testContractObj = [SELECT Id FROM Contract];
        SIP_Option__c testSIPOptionObj = [SELECT Id, Description__c FROM SIP_Option__c];
        testSIPOptionObj.Description__c = 'Key Market Revenue';
        UPDATE testSIPOptionObj;
        
        Test.startTest();
        SIPTableSelectorController sipTableSelectorObj = getSIPTableSelectorControllerObj(testContractObj, testSIPOptionObj);
        List<SIPTableSelectorController.SIPSelectionWrapperAdd> sipSelectionWrapperAddObj = sipTableSelectorObj.getSIPOptionAdd();
        PageReference nextPageReference = sipTableSelectorObj.Next();
        Test.stopTest();
        //Asserts
        system.assertEquals(true, sipSelectionWrapperAddObj.size() > 0);
        system.assertEquals(true, sipSelectionWrapperAddObj[0].selectedadd);
        system.assertEquals(true, sipSelectionWrapperAddObj[0].sip_optionadd != null);
    }
    
    @isTest
    private static void testSIPTableSelectorControllerWithNoId() {
        Test.startTest();
        SIPTableSelectorController sipTableSelectorObj = new SIPTableSelectorController();
        PageReference checkExistedPageReference = sipTableSelectorObj.CheckExisted();
        PageReference cancelPageReference = sipTableSelectorObj.Cancel();
        PageReference nextPageReference = sipTableSelectorObj.Next();
        Test.stopTest();
        //Asserts
        system.assertEquals('/', cancelPageReference.getUrl());
        System.assertEquals('Please select one SIP Table!', ApexPages.getMessages()[0].getSummary());
        System.assertEquals(ApexPages.SEVERITY.ERROR, ApexPages.getMessages()[0].getSeverity());
    }
    
    private static SIPTableSelectorController getSIPTableSelectorControllerObj(Contract testContractObj, SIP_Option__c testSIPOptionObj) {
        PageReference pageRef = Page.SIPTableSelector;
        pageRef.getParameters().put('cid', testContractObj.id);
        pageRef.getParameters().put('sipid', testSIPOptionObj.id);
        Test.setCurrentPage(pageRef);
        return new SIPTableSelectorController();
    }
}