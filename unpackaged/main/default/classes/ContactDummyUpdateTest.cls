@isTest
public class ContactDummyUpdateTest
{
static testMethod void myUnitTest()
{
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='012900000009HrP';
    account.Sales_Matrix_Owner__c = 'Accelerate';
	account.OwnerId = '00590000000LbNz'; 
    insert account;  
    
    Contact contactTest = new Contact();
    contactTest.LastName = 'TestLast Contact';
    contactTest.FirstName  = 'TestFirst Contact';
    contactTest.Key_Contact__c  = TRUE;
    contactTest.Email = 'testContact@yahoo.com';        
    contactTest.Title = 'Test Title';
    contactTest.Phone = '123456';
    contactTest.Velocity_Number__c = '2105327372';
    contactTest.AccountId = account.Id;
    insert contactTest;
    
     Test.StartTest();

     ContactDummyUpdate newContactUpdate = new ContactDummyUpdate();
     	
     Database.executeBatch( newContactUpdate);
	    
	 Test.StopTest();   
        
    }

}