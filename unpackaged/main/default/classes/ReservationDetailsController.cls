public class ReservationDetailsController{

    public List<ReservationSearchResult> Results{set;get;}
    public ReservationProperty ReservationProperties { set;get; }
    public String caseId {get; set;}

    //Constructor
    public ReservationDetailsController()
    {
        caseId = ApexPages.currentPage().getParameters().get('id');
        ReservationProperties = new ReservationProperty();
        if(caseId != null)
        {
            InitReservationInput();
        }
    }

    /*
    Initiate existed values for textbox on the search page
    */
    public void InitReservationInput(){
        if(caseId != null)
        {
            Case ca = [SELECT Reservation_Number__c, Reservation_Date__c, Origin__c, Destination__c, 
                       Velocity_Number__c, Velocity_Points_Credit__c,
                       Contact_First_Name__c, Contact_Last_Name__c, Airline_Organisation_Short__c
                       FROM Case WHERE Id = :caseId LIMIT 1];
            
            // populate search criteria from Case
            ReservationProperties.FirstName = ca.Contact_First_Name__c;
            ReservationProperties.LastName = ca.Contact_Last_Name__c;
            ReservationProperties.VelocityNumber = ca.Velocity_Number__c;
            try {
                ReservationProperties.VelocityCode = ca.Airline_Organisation_Short__c.substring(0,2);
            } catch (Exception ex){}
            
            // populate search criteria from Reservation obj
            List<Reservation__c> reList = [SELECT Destination__c,
                                 Flight_Date__c,Flight_Number__c,Id,IsDeleted,                             
                                 Name,Name_Field__c,
                                 Origin__c,Reservation_Date__c,
                                 Reservation_Number__c,
                                 Ticket_Number__c 
                                 FROM Reservation__c
                                 WHERE Case__c =:caseId LIMIT 1];
            
            if(reList == null || reList.size() == 0 || ca == null){
                return;
            }
            
            Reservation__c re = reList[0];
            if(re.Reservation_Number__c != null){
                ReservationProperties.ReservationNumber = re.Reservation_Number__c;
            }
            if(re.Reservation_Date__c != null){
                ReservationProperties.ReservationDate =re.Reservation_Date__c.format();
            }
            if(re.Origin__c != null){
                ReservationProperties.Origin = re.Origin__c;
            }
            if(re.Destination__c != null){
                 ReservationProperties.Destination = re.Destination__c;
            }
        }
    }
    
    /*
    validate the input data from ReservationDetail form
    if neither Reservation Number nor Velocity ID have been provided then Departure date is mandatory or Arrival date is mandatory
    */
  	private boolean validateInputFormValues(){
        String error = '';
        if (String.isBlank(ReservationProperties.VelocityNumber) && String.isBlank(ReservationProperties.VelocityCode) ){
            if (String.isBlank(ReservationProperties.ReservationNumber) 
                && String.isBlank(ReservationProperties.DepartureDate) 
                && String.isBlank(ReservationProperties.ArrivalDate)
                && String.isBlank(ReservationProperties.LastName)) {
                    error = 'If neither Reservation Number nor Velocity number nor Last Name have been provided then Departure or Arrival date is mandatory';
                }
        } else if (String.isBlank(ReservationProperties.VelocityNumber) || String.isBlank(ReservationProperties.VelocityCode)){
            error = 'Both velocity code and velocity number are mandatory if one of them has been provided';
        }
        
        if (error!='') {
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
            return false;
        } else return true;
	}    
    /*
    Search Reservation based on the creteria on the search page and then calls out webservice to get suitable values
    */
    public void SearchReservation()
    {

        // Reset search result before a new search
        Results = null;
        GuestCaseReservationInfo.SalesforceReservationService SalesforceReservationService =
            new GuestCaseReservationInfo.SalesforceReservationService();
		if (validateInputFormValues()){
	        // Retrieve Apex Callout options from Custom Settings
	        Apex_Callouts__c apexCallouts = Apex_Callouts__c.getValues('ReservationFind');
	        if(apexCallouts != null){
	            SalesforceReservationService.endpoint_x = apexCallouts.Endpoint_URL__c;
	            SalesforceReservationService.timeout_x = Integer.valueOf(apexCallouts.Timeout_ms__c);
	            SalesforceReservationService.clientCertName_x = apexCallouts.Certificate__c;
	        }
	
	        GuestCaseReservationInfoModel.FindPassengerNameType PassengerName = new GuestCaseReservationInfoModel.FindPassengerNameType();
	      		    
	      	PassengerName.GivenName = null;	        
	        if(ReservationProperties.FirstName != '')
	        {
	        	PassengerName.GivenName = ReservationProperties.FirstName;
	        }
			
			
			PassengerName.Surname = null;
	        if(ReservationProperties.LastName != '')
	        {
	            PassengerName.Surname = ReservationProperties.LastName;
	        }
	        
	        if((PassengerName.GivenName==null)&&(PassengerName.Surname==null)){
	        	PassengerName = null;
	        }
	        
	        GuestCaseReservationInfoModel.LoyaltyType Loyalty = new GuestCaseReservationInfoModel.LoyaltyType();
	        Loyalty.ProgramCode = null;
	        if(ReservationProperties.VelocityCode !='')
	        {
	        	Loyalty.ProgramCode = ReservationProperties.VelocityCode;
	        }
	       
	        
	        Loyalty.LoyaltyMembershipID = null;
	        if(ReservationProperties.VelocityNumber !='')
	        {
	            Loyalty.LoyaltyMembershipID = ReservationProperties.VelocityNumber;
	        }
			      
	        if(Loyalty.ProgramCode == null && Loyalty.LoyaltyMembershipID ==null )
	        {
	        	Loyalty = null;
	        }
	              
	        GuestCaseReservationInfoModel.FlightSegmentCriteriaType FlightSegmentCriteria = new GuestCaseReservationInfoModel.FlightSegmentCriteriaType();
	        
	        FlightSegmentCriteria.DepartureAirport = null;
	        if(ReservationProperties.Origin !='')
	        {
	        	FlightSegmentCriteria.DepartureAirport = ReservationProperties.Origin;
	        }
	        
			FlightSegmentCriteria.ArrivalAirport = null;
	        if(ReservationProperties.Destination!='')
	        {
	        	FlightSegmentCriteria.ArrivalAirport = ReservationProperties.Destination;
			}
	
			
			FlightSegmentCriteria.DepartureDate = null;
	        GuestCaseReservationInfoModel.DateCriterionType DateTmp = new GuestCaseReservationInfoModel.DateCriterionType();
	        if(ReservationProperties.DepartureDate!= null && ReservationProperties.DepartureDate != '')
	        {
	            try
	            {
	            	//DateTmp.Is = Date.parse(ReservationProperties.DepartureDate);
	            	// convert from date format to datetime format
	        		DateTmp.After = null;
	            	DateTmp.Before = null;        		
	            	System.debug('hanhluu::set DepartureDate::start::' + ReservationProperties.DepartureDate + '00:00 AM');
	        		DateTmp.Is = Utilities.convert2DateTimeString(ReservationProperties.DepartureDate);
	        		
	        		System.debug('hanhluu::set DepartureDate::end::' + DateTmp.Is);
	                FlightSegmentCriteria.DepartureDate = DateTmp;
	            }
	            catch (Exception ex)
	            {
	                //FlightSegmentCriteria.DepartureDate = null;
	             	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Departure Date'));   
	            }
	                
	        }
	        
	        FlightSegmentCriteria.ArrivalDate = null;
	        DateTmp = new GuestCaseReservationInfoModel.DateCriterionType();        
	        if(ReservationProperties.ArrivalDate!=null && ReservationProperties.ArrivalDate != '')
	        {
	            try
	            {
	        		//DateTmp.Is = date.parse(ReservationProperties.ArrivalDate);
	        		// convert from date format to datetime format
	        		DateTmp.After = null;
	            	DateTmp.Before = null;        		
	            	//Datetime dt = DateTime.parse(ReservationProperties.ArrivalDate);
	        		DateTmp.Is = Utilities.convert2DateTimeString(ReservationProperties.ArrivalDate);
	        		System.debug('hanhluu::set ArrivalDate::' + DateTmp.Is);
	                FlightSegmentCriteria.ArrivalDate = DateTmp;
	            }
	            catch(Exception ex)
	            {
	                //FlightSegmentCriteria.ArrivalDate = null;
	                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Arrival Date'));
	            }               
	        }     
	        
	        if ((FlightSegmentCriteria.ArrivalDate==null)
	        	&&(FlightSegmentCriteria.DepartureDate==null)
	        	&&(FlightSegmentCriteria.ArrivalAirport==null)
	        	&&(FlightSegmentCriteria.DepartureAirport ==null)){
	        	FlightSegmentCriteria = null;
	        }
	        
	       
	        String PNRLocator =null;
	        if(ReservationProperties.ReservationNumber != '')
	        {
	         	PNRLocator =ReservationProperties.ReservationNumber;
	        }
	        
	        try {
	           System.debug('hanhluu::GuestCaseReservationInfo.FindReservationDetails::ReservationDetailsController:start');
	           
	           GuestCaseReservationInfoModel.FindReservationDetailsRSType FindReservationDetailsRSType = SalesforceReservationService.FindReservationDetails(PassengerName, Loyalty, FlightSegmentCriteria, PNRLocator);
				
			if ((FindReservationDetailsRSType== null)||(FindReservationDetailsRSType.Reservations==null)||(FindReservationDetailsRSType.Reservations.FindReservation==null)){
				Results =null;
		        String error = 'There is no return data';  
		        System.debug('HanhLuu::No result::' +  error);
	            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));								
			}		 
			else{	 
		           GuestCaseReservationInfoModel.FindReservationType[] FindReservationType =
		                FindReservationDetailsRSType.Reservations.FindReservation;
		                
		    	   System.debug('hanhluu::GuestCaseReservationInfo.FindReservationDetails::ReservationDetailsController:end' + FindReservationType.size());
		    	       	   
		           List<ReservationSearchResult> tmpList = new List<ReservationSearchResult>();
		           ReservationSearchResult item = new ReservationSearchResult();
		    
		    
		           List<GuestCaseReservationInfoModel.FindPassengerType> FindPassengerType;
		           List<GuestCaseReservationInfoModel.FlightSegmentType> FlightSegmentType;
		    
		    		if (FindReservationType!=null && FindReservationType.size()>0){
			            for(GuestCaseReservationInfoModel.FindReservationType tempItem:FindReservationType){
			    			// Charleston Telles - bug fixing
			    			// if we don't instantiate a new object before adding to collection
			    			// all itens in the collection will have the value
			    			item = new ReservationSearchResult();
			    			// end bug fixing
			    			
			                FindPassengerType = tempItem.Passengers.Passenger;
			                FlightSegmentType = tempItem.FoundFlightSegments.FlightSegmentList;
			                item.PNRLocator = tempItem.PNRLocator;
			                if(tempItem.PNRCreationDate != null){
			                    item.PNRCreationDate = String.valueOf(tempItem.PNRCreationDate);
			                }
			    			
			                if((FindPassengerType != null) && (FindPassengerType.size()>0)){
			                	System.debug('HanhLuu::FindPassengerType::' +  FindPassengerType.size());
			                	System.debug('HanhLuu::FindPassengerType::Surname' +  FindPassengerType.get(0).Surname);
			                	if (FindPassengerType.get(0)!=null){
				                    item.FirstName = FindPassengerType.get(0).Surname;
				                    item.LastName = FindPassengerType.get(0).GivenName;
			                	}else{
			                		item.FirstName = '<no value>';
			                		item.LastName = '<no value>';
			                	}
			                }else{
		                		System.debug('HanhLuu::FindPassengerType::novalue');
		                		item.FirstName = '<no value>';
		                		item.LastName = '<no value>';
			                }
			                item.DepartureDateAndTime = (FlightSegmentType!=null)?FlightSegmentType.get(0).DepartureDateTime:null;
				            item.MarketingAirlineCode = (FlightSegmentType!=null)?FlightSegmentType.get(0).MarketingAirline.AirlineCode:'<no value>';
			                item.OperatingAirlineCode = (FlightSegmentType!=null)?FlightSegmentType.get(0).OperatingAirline.AirlineCode:'<no value>';
			
			                item.DeparturePort = (FlightSegmentType!=null)?FlightSegmentType.get(0).DepartureAirportCode:'<no value>';
			                item.ArrivalDateAndTime  = (FlightSegmentType!=null)?FlightSegmentType.get(0).ArrivalDateTime:'<no value>';
			                item.ArrivalPort = (FlightSegmentType!=null)?FlightSegmentType.get(0).ArrivalAirportCode:'<no value>';
			                tmpList.add(item);
			                
			            }
		    		}
		    		Results = tmpList;
				}
		        
	        } catch (System.CalloutException e) {
                System.debug('SonPham>>>>>>>>exception>>>>' + e);
	            String error = e.getMessage();
	            if (String.isNotEmpty(error) && String.isNotBlank(error.substringAfter('SOAP Fault:').substringBefore('faultcode'))) {
	                error = error.substringAfter('SOAP Fault:').substringBefore('faultcode');
	            } else {
	                error = 'Communication Error';
	            }
	            System.debug('HanhLuu::CalloutException::' +  e.getMessage());
	            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
	        }catch (Exception ex){
		        String error = ex.getMessage();  
		        System.debug('HanhLuu::CalloutException2::' +  ex.getMessage());
	            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
		        	
	        }
		}
    }

    public class ReservationSearchResult
    {
        public ReservationSearchResult()
        {
        }
        public String PNRLocator
        {
            set;get;
        }
        public String PNRCreationDate
        {
            set;get;
        }
        public String FirstName
        {
            set;get;
        }
         public String LastName
        {
            set;get;
        }
        public String DepartureDateAndTime
        {
            set;get;
        }
        public String DeparturePort
        {
            set;get;
        }
        public String ArrivalDateAndTime
        {
            set;get;
        }
        public String ArrivalPort
        {
            set;get;
        }
        public String MarketingAirlineCode
        {
            set;get;
        }
        public String OperatingAirlineCode
        {
            set;get;
        }
        public String EquipmentType
        {
            set;get;
        }
        public String FareClass
        {
            set;get;
        }
    }

    public class ReservationProperty
    {
        public ReservationProperty()
        {
        }
        public String ReservationNumber
        {
            set;get;
        }
        public String ReservationDate
        {
            set;get;
        }
        public String Origin
        {
            set;get;
        }
        public String Destination
        {
            set;get;
        }
        public String ArrivalDate
        {
            set;get;
        }
        public String DepartureDate
        {
            set;get;
        }
        public String FirstName
        {
            set;get;
        }
        public String LastName
        {
            set;get;
        }
        public String VelocityCode
        {
            set;get;
        }
        public String VelocityNumber
        {
            set;get;
        }
    }
}