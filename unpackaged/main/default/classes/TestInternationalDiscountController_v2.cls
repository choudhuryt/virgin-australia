@isTest
private class TestInternationalDiscountController_v2 {

    static testMethod void TestTemplateTrue() {
        // TO DO: implement unit test
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        insert account;
  			
  		Contract contractOld = new Contract();
        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.VA_EY_Requested_Tier__c ='1';
        contractOld.Cos_Version__c = '11.2';
        contractold.IsTemplate__c =true;
        contractOld.Red_Circle__c = false;
        
        
        insert contractOld;
  		
  		
  		Cost_of_Sale__c costOfSale = new Cost_of_Sale__c();
       	costOfSale.Contract__c = contractOld.id;
       	insert costOfSale;
       	
        	Contract_Calculation_Discount_Levels__c discountlevel = new Contract_Calculation_Discount_Levels__c();
      	discountlevel.Business_Perentage_C_Class__c = 2.2;
      	discountlevel.Business_Perentage_J_Class__c = 2.2;
      	discountlevel.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel.Carrier__c = 'EY';
      	discountlevel.Category__c ='International Africa';
      	discountlevel.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel.Premium_Economy_W_Class__c =5.0;
      	discountlevel.Premium_Saver_R_Class__c = 3.0;
      	discountlevel.Saver_Percentage_E_Class__c =2.0;
      	discountlevel.Saver_Percentage_N_Class__c =3.0;
      	discountlevel.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel;
       
       
       	Contract_Calculation_Discount_Levels__c discountlevel1 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel1.Business_Perentage_C_Class__c = 2.2;
      	discountlevel1.Business_Perentage_J_Class__c = 2.2;
      	discountlevel1.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel1.Carrier__c = 'EY';
      	discountlevel1.Category__c ='International Asia';
      	discountlevel1.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel1.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel1.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel1.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel1.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel1.Premium_Economy_W_Class__c =5.0;
      	discountlevel1.Premium_Saver_R_Class__c = 3.0;
      	discountlevel1.Saver_Percentage_E_Class__c =2.0;
      	discountlevel1.Saver_Percentage_N_Class__c =3.0;
      	discountlevel1.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel1.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel1.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel1;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel2 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel2.Business_Perentage_C_Class__c = 2.2;
      	discountlevel2.Business_Perentage_J_Class__c = 2.2;
      	discountlevel2.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel2.Carrier__c = 'EY';
      	discountlevel2.Category__c ='International Middle East';
      	discountlevel2.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel2.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel2.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel2.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel2.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel2.Premium_Economy_W_Class__c =5.0;
      	discountlevel2.Premium_Saver_R_Class__c = 3.0;
      	discountlevel2.Saver_Percentage_E_Class__c =2.0;
      	discountlevel2.Saver_Percentage_N_Class__c =3.0;
      	discountlevel2.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel2.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel2.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel2;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel3 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel3.Business_Perentage_C_Class__c = 2.2;
      	discountlevel3.Business_Perentage_J_Class__c = 2.2;
      	discountlevel3.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel3.Carrier__c = 'EY';
      	discountlevel3.Category__c ='International UK/Europe';
      	discountlevel3.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel3.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel3.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel3.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel3.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel3.Premium_Economy_W_Class__c =5.0;
      	discountlevel3.Premium_Saver_R_Class__c = 3.0;
      	discountlevel3.Saver_Percentage_E_Class__c =2.0;
      	discountlevel3.Saver_Percentage_N_Class__c =3.0;
      	discountlevel3.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel3.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel3.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel3;
  			
  		system.assert(contractOld !=null);

 		 PageReference pref = Page.EY_Discount_Page;
        	pref.getParameters().put('id', contractOld.id);
        	Test.setCurrentPage(pref);
			        
        //instantiate the InternationalDiscountController_v2 
        ApexPages.StandardController conL = new ApexPages.StandardController(contractOld);
        InternationalDiscountController_v2 lController = new InternationalDiscountController_v2(conL);
        
        
          Test.startTest();
         
        //PageReference  ref1 = Page.EY_Discount_Page;
        
        pref = lController.initDisc();       
        
        pref = lController.newInterUKEUR();
        pref = lController.newMiddleE();
        pref = lController.newAsia();
        pref = lController.newAfrica();
        
        pref = lController.save();
        pref = lController.initDisc();
        
        pref = lController.qsave();
        pref = lController.initDisc();

        pref = lController.remUKEUR();  
        pref = lController.remMiddleE();
        pref = lController.remAsia();
        pref = lController.remAfrica();
        
        pref = lController.cancel();

      
        test.stopTest();
        
			
    }

    static testMethod void TestTemplatefalse() {
        // TO DO: implement unit test
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        insert account;
  			
  		Contract contractOld = new Contract();
        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.VA_EY_Requested_Tier__c ='1';
        contractOld.Cos_Version__c = '11.2';
        contractold.IsTemplate__c =false;
        contractOld.Red_Circle__c = true;
        insert contractOld;

  		
  		Cost_of_Sale__c costOfSale = new Cost_of_Sale__c();
       	costOfSale.Contract__c = contractOld.id;
       	insert costOfSale;
       	
        	Contract_Calculation_Discount_Levels__c discountlevel = new Contract_Calculation_Discount_Levels__c();
      	discountlevel.Business_Perentage_C_Class__c = 2.2;
      	discountlevel.Business_Perentage_J_Class__c = 2.2;
      	discountlevel.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel.Carrier__c = 'EY';
      	discountlevel.Category__c ='International Africa';
      	discountlevel.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel.Premium_Economy_W_Class__c =5.0;
      	discountlevel.Premium_Saver_R_Class__c = 3.0;
      	discountlevel.Saver_Percentage_E_Class__c =2.0;
      	discountlevel.Saver_Percentage_N_Class__c =3.0;
      	discountlevel.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel;
       
       
       	Contract_Calculation_Discount_Levels__c discountlevel1 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel1.Business_Perentage_C_Class__c = 2.2;
      	discountlevel1.Business_Perentage_J_Class__c = 2.2;
      	discountlevel1.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel1.Carrier__c = 'EY';
      	discountlevel1.Category__c ='International Asia';
      	discountlevel1.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel1.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel1.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel1.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel1.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel1.Premium_Economy_W_Class__c =5.0;
      	discountlevel1.Premium_Saver_R_Class__c = 3.0;
      	discountlevel1.Saver_Percentage_E_Class__c =2.0;
      	discountlevel1.Saver_Percentage_N_Class__c =3.0;
      	discountlevel1.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel1.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel1.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel1;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel2 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel2.Business_Perentage_C_Class__c = 2.2;
      	discountlevel2.Business_Perentage_J_Class__c = 2.2;
      	discountlevel2.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel2.Carrier__c = 'EY';
      	discountlevel2.Category__c ='International Middle East';
      	discountlevel2.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel2.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel2.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel2.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel2.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel2.Premium_Economy_W_Class__c =5.0;
      	discountlevel2.Premium_Saver_R_Class__c = 3.0;
      	discountlevel2.Saver_Percentage_E_Class__c =2.0;
      	discountlevel2.Saver_Percentage_N_Class__c =3.0;
      	discountlevel2.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel2.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel2.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel2;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel3 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel3.Business_Perentage_C_Class__c = 2.2;
      	discountlevel3.Business_Perentage_J_Class__c = 2.2;
      	discountlevel3.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel3.Carrier__c = 'EY';
      	discountlevel3.Category__c ='International UK/Europe';
      	discountlevel3.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel3.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel3.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel3.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel3.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel3.Premium_Economy_W_Class__c =5.0;
      	discountlevel3.Premium_Saver_R_Class__c = 3.0;
      	discountlevel3.Saver_Percentage_E_Class__c =2.0;
      	discountlevel3.Saver_Percentage_N_Class__c =3.0;
      	discountlevel3.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel3.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel3.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel3;
  			
  		system.assert(contractOld !=null);
                   
 		 PageReference pref = Page.EY_Discount_Page;
        	pref.getParameters().put('id', contractOld.id);
        	Test.setCurrentPage(pref);
			        
        //instantiate the InternationalDiscountController_v2 
        ApexPages.StandardController conL = new ApexPages.StandardController(contractOld);
        InternationalDiscountController_v2 lController = new InternationalDiscountController_v2(conL);
        
        
          Test.startTest();
         
        //PageReference  ref1 = Page.EY_Discount_Page;
        
        pref = lController.initDisc();       
        
        pref = lController.newInterUKEUR();
        pref = lController.newMiddleE();
        pref = lController.newAsia();
        pref = lController.newAfrica();
        
        pref = lController.save();
        pref = lController.initDisc();
        
        pref = lController.qsave();
        pref = lController.initDisc();

        /*pref = lController.remUKEUR();  
        pref = lController.remMiddleE();
        pref = lController.remAsia();
        pref = lController.remAfrica();*/
        
        pref = lController.cancel();

      
        test.stopTest();
        
			
    }

    static testMethod void TestDiscount() {
        // TO DO: implement unit test
        
        Account account 				= new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        insert account;
  			
  		Contract contractOld = new Contract();
        
        contractOld = commx.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.VA_EY_Requested_Tier__c ='1';
        contractOld.Cos_Version__c = '11.2';
        contractold.IsTemplate__c =true;
        contractOld.Red_Circle__c = false;
        
        
        insert contractOld;
  		
  		
  		Cost_of_Sale__c costOfSale = new Cost_of_Sale__c();
       	costOfSale.Contract__c = contractOld.id;
       	insert costOfSale;
       	
        	Contract_Calculation_Discount_Levels__c discountlevel = new Contract_Calculation_Discount_Levels__c();
      	discountlevel.Business_Perentage_C_Class__c = 2.2;
      	discountlevel.Business_Perentage_J_Class__c = 2.2;
      	discountlevel.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel.Carrier__c = 'EY';
      	discountlevel.Category__c ='International Africa';
      	discountlevel.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel.Premium_Economy_W_Class__c =5.0;
      	discountlevel.Premium_Saver_R_Class__c = 3.0;
      	discountlevel.Saver_Percentage_E_Class__c =2.0;
      	discountlevel.Saver_Percentage_N_Class__c =3.0;
      	discountlevel.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel;
       
       
       	Contract_Calculation_Discount_Levels__c discountlevel1 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel1.Business_Perentage_C_Class__c = 2.2;
      	discountlevel1.Business_Perentage_J_Class__c = 2.2;
      	discountlevel1.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel1.Carrier__c = 'EY';
      	discountlevel1.Category__c ='International Asia';
      	discountlevel1.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel1.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel1.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel1.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel1.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel1.Premium_Economy_W_Class__c =5.0;
      	discountlevel1.Premium_Saver_R_Class__c = 3.0;
      	discountlevel1.Saver_Percentage_E_Class__c =2.0;
      	discountlevel1.Saver_Percentage_N_Class__c =3.0;
      	discountlevel1.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel1.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel1.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel1;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel2 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel2.Business_Perentage_C_Class__c = 2.2;
      	discountlevel2.Business_Perentage_J_Class__c = 2.2;
      	discountlevel2.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel2.Carrier__c = 'EY';
      	discountlevel2.Category__c ='International Middle East';
      	discountlevel2.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel2.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel2.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel2.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel2.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel2.Premium_Economy_W_Class__c =5.0;
      	discountlevel2.Premium_Saver_R_Class__c = 3.0;
      	discountlevel2.Saver_Percentage_E_Class__c =2.0;
      	discountlevel2.Saver_Percentage_N_Class__c =3.0;
      	discountlevel2.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel2.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel2.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel2;
      	
      	Contract_Calculation_Discount_Levels__c discountlevel3 = new Contract_Calculation_Discount_Levels__c();
      	discountlevel3.Business_Perentage_C_Class__c = 2.2;
      	discountlevel3.Business_Perentage_J_Class__c = 2.2;
      	discountlevel3.Business_Saver_Perentage_D_Class__c = 2.1;
      	discountlevel3.Carrier__c = 'EY';
      	discountlevel3.Category__c ='International UK/Europe';
      	discountlevel3.Flexi_Percentage_B_Class__c = 2.2;
      	discountlevel3.Flexi_Percentage_H_Class__c =1.0;
      	discountlevel3.Flexi_Percentage_K_Class__c =5.0;
      	discountlevel3.Flexi_Percentage_L_Class__c =5.0;
      	discountlevel3.Flexi_Percentage_Y_Class__c =1.0;
      	discountlevel3.Premium_Economy_W_Class__c =5.0;
      	discountlevel3.Premium_Saver_R_Class__c = 3.0;
      	discountlevel3.Saver_Percentage_E_Class__c =2.0;
      	discountlevel3.Saver_Percentage_N_Class__c =3.0;
      	discountlevel3.Saver_Percentage_Q_Class__c= 3.0;
      	discountlevel3.Saver_Percentage_T_Class__c = 2.0;
      	discountlevel3.Saver_Percentage_V_Class__c =1.0;
      	
      	insert discountlevel3;
  			
  		system.assert(contractOld !=null);

  		EY_Discount__c EYdiscountUKEUR =   new EY_Discount__c();
            EYdiscountUKEUR.DiscOffPublishFare_001__c =  10;
            EYdiscountUKEUR.DiscOffPublishFare_002__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_003__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_004__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_005__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_006__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_007__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_008__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_009__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_010__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_011__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_012__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_013__c = 10;
            EYdiscountUKEUR.DiscOffPublishFare_014__c = 10;

            EYdiscountUKEUR.IsTemplateUKEUR__c = true;
            EYdiscountUKEUR.Cos_Version_c__c='11.2';
            EYdiscountUKEUR.Tier__c='1';
        insert EYdiscountUKEUR;
  		system.assert(EYdiscountUKEUR !=null);
  				
  		EY_Discount_Middle_East__c EYdiscountME = new EY_Discount_Middle_East__c();
            EYdiscountME.DiscOffPublishFare_001__c =  10;
            EYdiscountME.DiscOffPublishFare_002__c = 10;
            EYdiscountME.DiscOffPublishFare_003__c = 10;
            EYdiscountME.DiscOffPublishFare_004__c = 10;
            EYdiscountME.DiscOffPublishFare_005__c = 10;
            EYdiscountME.DiscOffPublishFare_006__c = 10;
            EYdiscountME.DiscOffPublishFare_007__c = 10;
            EYdiscountME.DiscOffPublishFare_008__c = 10;
            EYdiscountME.DiscOffPublishFare_009__c = 10;
            EYdiscountME.DiscOffPublishFare_010__c = 10;
            EYdiscountME.DiscOffPublishFare_011__c = 10;
            EYdiscountME.DiscOffPublishFare_012__c = 10;
            EYdiscountME.DiscOffPublishFare_013__c = 10;
            EYdiscountME.DiscOffPublishFare_014__c = 10;
                   
            EYdiscountME.Tier__c = '1';
            EYdiscountME.Cos_Version__c='11.2';
        insert EYdiscountME;
        system.assert(EYdiscountME !=null);
					
					
		EY_Discount_Africa__c EYdiscountAFRICA =   new EY_Discount_Africa__c();
            EYdiscountAFRICA.DiscOffPublishFare_001__c =  10;
            EYdiscountAFRICA.DiscOffPublishFare_002__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_003__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_004__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_005__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_006__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_007__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_008__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_009__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_010__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_011__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_012__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_013__c = 10;
            EYdiscountAFRICA.DiscOffPublishFare_014__c = 10;
            EYdiscountAFRICA.Cos_Version__c ='11.2';
            EYdiscountAFRICA.Tier__c = '1';
            EYdiscountAFRICA.IsTemplateAfrica__c = true;
        insert EYdiscountAFRICA;  
        system.assert(EYdiscountAFRICA !=null);	
                   
 		 PageReference pref = Page.EY_Discount_Page;
        	pref.getParameters().put('id', contractOld.id);
        	Test.setCurrentPage(pref);
			        
        //instantiate the InternationalDiscountController_v2 
        ApexPages.StandardController conL = new ApexPages.StandardController(contractOld);
        InternationalDiscountController_v2 lController = new InternationalDiscountController_v2(conL);
        
        
          Test.startTest();
         
        //PageReference  ref1 = Page.EY_Discount_Page;
        
        pref = lController.initDisc();       
        
        pref = lController.newInterUKEUR();
        pref = lController.newMiddleE();
        pref = lController.newAsia();
        pref = lController.newAfrica();
        
        pref = lController.save();
        pref = lController.initDisc();
        
        pref = lController.qsave();
        pref = lController.initDisc();

        pref = lController.remUKEUR();  
        pref = lController.remMiddleE();
        pref = lController.remAsia();
        pref = lController.remAfrica();
        
        pref = lController.cancel();

      
        test.stopTest();
        
			
    }    
}