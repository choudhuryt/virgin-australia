@isTest
private class TestAccountOwnerBulkUpdate
{
static testMethod void myUnitTest()
    {
    
    Test.StartTest();
        
        Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        account.Sales_Matrix_Owner__c = 'VIC';
        account.Market_Segment__c = 'Large Market';
	    account.OwnerId = '00590000000K0ju'; 
        account.Account_Lifecycle__c = 'Contract';
  		account.RecordTypeId = '01290000000tSR2';
        account.Nominated_Account_Owner__c = '0056F000005xwcl' ;
 		insert account;
        
      
        
        List<Id> accid = new List<Id>();        
        accid.add(account.Id);
       AccountOwnerBulkUpdate.OwnerUpdateForAccount(accid) ;
        
        
       Test.StopTest(); 
    }
}