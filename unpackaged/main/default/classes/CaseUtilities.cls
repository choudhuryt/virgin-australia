global class CaseUtilities {
webservice static String convertToSolution(ID id) {
   Case cs =
        [SELECT cs.Subject, cs.Description FROM Case cs
          WHERE cs.ID = :id ];
          
  Solution[] solution =
        [SELECT s.SolutionName, s.SolutionNote  
          FROM Solution s
          WHERE s.CaseID__c = :id ];
          
        if(solution.size() == 0){
        Solution sl = new Solution();
        sl.SolutionName = cs.Subject;
        sl.SolutionNote = cs.Description;
        sl.CaseID__c = id;
        insert sl;
        
        Attachment[] attList = [select id, name, body from Attachment where ParentId = :id];
        Attachment[] insertAttList = new Attachment[]{};
 
         for(Attachment a: attList)
         {
               Attachment att = new Attachment(name = a.name, body = a.body, parentid = sl.Id);
               insertAttList.add(att);
         }
       if(insertAttList.size() > 0)
       {
            insert insertAttList;
       }
        
        return 'Case has been converted to Solution successfully.';
        } else{
      
      return 'Case is already converted to Solution successfully.';
        }
}
}