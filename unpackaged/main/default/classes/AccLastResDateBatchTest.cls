@isTest
public class AccLastResDateBatchTest {
	static testMethod void batchSuccessScenario(){
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Accelerate').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        List<EmailMessage> msgList = new List<EmailMessage>();
        
        for(integer i=0; i<10;i++){
            Case cs = new Case();            
        	cs.RecordTypeId = caseRTId;
        	cs.Status = 'New';
            caseList.add(cs);
        }
        insert caseList;
        System.debug('caseListTest--'+caseList);
        
        for(integer j=0; j<5; j++){
            EmailMessage msg = new EmailMessage();
            if(j<4){
                msg.RelatedToId = caseList[1].Id;
            }else{
                msg.RelatedToId = caseList[j].Id;
            }            
            msg.Incoming = true;
            msgList.add(msg);
        }
        insert msgList;
        System.debug('msgList--'+msgList);
        
        List<EmailMessage> mailList = [Select Id,RelatedToId,CreatedDate from EmailMessage where RelatedToId IN:caseList AND Incoming =:true];
        System.debug('mailList--'+mailList);
        
        Test.startTest();
        
        AccLastResDateBatch obj = new AccLastResDateBatch();
        DataBase.executeBatch(obj);
        
        Test.stopTest();
    }
}