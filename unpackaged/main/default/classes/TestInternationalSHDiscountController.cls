/**  File Name      : TestInternationalSHDiscountController
* Description        : This Apex Test Class is the Test Class for DomesticDiscountController  
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Andy Burgess
* * Date             : Created 28 February 2013
* * Technical Task ID: 
* * Notes            :  The test class for this file is:  TestDomesticDiscountController
* Modification Log =============================================================== 
Ver Date Author Modification --- ---- ------ -------------
* */
/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
* @updatedBy : cloudwerx
*/
@isTest
private class TestInternationalSHDiscountController {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        Contract testContractObj = TestDataFactory.createTestContract('Test Contract', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        testContractObj.Cos_Version__c = '13.0';
        testContractObj.Red_Circle__c = true;
        testContractObj.IsTemplate__c = true;
        INSERT testContractObj;
    }
    
    @isTest
    private static void testInternationalSHDiscountControllerWithRelatedRecords() {
        Contract testContractObj = [SELECT Id FROM Contract];
        
        Trans_Tasman_Discounts__c testTransTasmanDiscountsObj = TestDataFactory.createTestTransTasmanDiscounts(testContractObj.Id, '1', false, '13.2');
        INSERT testTransTasmanDiscountsObj;
        
        International_Disc_SH__c testInternationalDiscSHObj = TestDataFactory.createTestInternationalDiscSH(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscSHObj;
        
        SH_Discount_Pacific_PNG__c testSHDiscountPacificPNGObj = TestDataFactory.createTestSHDiscountPacificPNG(testContractObj.Id, '1', false, '13.2');
        INSERT testSHDiscountPacificPNGObj;
        
        International_Discounts_HK__c testInternationalDiscountsHKObj = TestDataFactory.createTestInternationalDiscountsHK(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsHKObj;
        
        International_Discounts_China__c testInternationalDiscountsChinaObj = TestDataFactory.createTestInternationalDiscountsChina(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsChinaObj;
        
        International_Discounts_NZ__c testInternationalDiscountsNZObj = TestDataFactory.createTestInternationalDiscountsNZ(testContractObj.Id, '1', false, '13.2');
        INSERT testInternationalDiscountsNZObj;
        
        Test.startTest();
        InternationalSHDiscountController lController = getInternationalSHDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference savePageReference = lController.save();
        PageReference qsavePageReference = lController.qsave();
        PageReference cancelPageReference = lController.cancel();
        PageReference newDomPageReference = lController.newDom();
        PageReference remDomPageReference = lController.remDom();
        PageReference newPNGPageReference = lController.newPNG();
        PageReference remPNGPageReference = lController.remPNG();
        PageReference newTransPageReference = lController.newTrans();
        PageReference remTransPageReference = lController.remTrans();
        PageReference newNZPageReference = lController.newNZ();
        PageReference remNZPageReference = lController.remNZ();
        PageReference newHKPageReference = lController.newHK();
        PageReference remHKPageReference = lController.remHK();
        PageReference newCNPageReference = lController.newCN();
        PageReference remCNPageReference = lController.remCN();
        test.stopTest();
        //Asserts
        system.assertEquals(true, lController.redCircle);
        system.assertEquals(true, lController.isGoPlus);
        system.assertEquals(1, lController.SHFlag);
        system.assertEquals(1, lController.transFlag);
        system.assertEquals(1, lController.pngFlag);
        system.assertEquals(1, lController.hkFlag);
        system.assertEquals(1, lController.cnFlag);
        system.assertEquals('/' + testContractObj.Id, savePageReference.getUrl());
        system.assertEquals('/' + testContractObj.Id, qsavePageReference.getUrl());
        system.assertEquals('/' + testContractObj.Id, cancelPageReference.getUrl());
    }
    
    @isTest
    private static void testInternationalSHDiscountControllerWithNewDom() {
        Contract testContractObj = [SELECT Id FROM Contract];
        Test.startTest();
        InternationalSHDiscountController lController = getInternationalSHDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newDomPageReference = lController.newDom();
        test.stopTest();
        //Asserts
        List<International_Disc_SH__c> internationalDiscSHResults = lController.searchResults;
        system.assertEquals('Business', internationalDiscSHResults[0].IntlDisc_SH_EligibleFareType__c);
        system.assertEquals('Business Saver', internationalDiscSHResults[0].IntlDisc_SH_EligibleFareType3__c);
        system.assertEquals('J', internationalDiscSHResults[0].IntlDisc_SH_EligibleBookClass__c);
        system.assertEquals('N/V', internationalDiscSHResults[0].IntlDisc_SH_EligibleBookClass14__c);
    }
    
    @isTest
    private static void testInternationalSHDiscountControllerWithNewPNG() {
        Contract testContractObj = [SELECT Id FROM Contract];
        Test.startTest();
        InternationalSHDiscountController lController = getInternationalSHDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newPNGPageReference = lController.newPNG();
        test.stopTest();
        //Asserts
        List<SH_Discount_Pacific_PNG__c> shDiscountPacificPNGResults = lController.searchResults3;
        system.assertEquals(0, shDiscountPacificPNGResults[0].DiscOffPublishFare_001__c);
        system.assertEquals(0, shDiscountPacificPNGResults[0].DiscOffPublishFare_002__c);
        system.assertEquals(0, shDiscountPacificPNGResults[0].DiscOffPublishFare_013__c);
    }
    
    @isTest
    private static void testInternationalSHDiscountControllerWithNewTransApexMessage() {
        Contract testContractObj = [SELECT Id FROM Contract];
        Test.startTest();
        InternationalSHDiscountController lController = getInternationalSHDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newTransPageReference = lController.newTrans();
        test.stopTest();
        //Asserts
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('A VA/NZ Tier has not been Selected in the contract so you cannot add a discount', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.ERROR, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testInternationalSHDiscountControllerWithContractFieldValues() {
        Contract testContractObj = [SELECT Id, VA_NZ_Requested_Tier__c, VA_HK_Requested_Tier__c, VA_China_Requested_Tier__c FROM Contract];
        testContractObj.VA_NZ_Requested_Tier__c = '1';
        testContractObj.VA_HK_Requested_Tier__c = '1';
        testContractObj.VA_China_Requested_Tier__c = '1';
        UPDATE testContractObj;
        Test.startTest();
        InternationalSHDiscountController lController = getInternationalSHDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newTransPageReference = lController.newTrans();
        PageReference newNZPageReference = lController.newNZ();
        PageReference newHKPageReference = lController.newHK();
        PageReference newCNPageReference = lController.newCN();
        test.stopTest();
        //Asserts
        List<Trans_Tasman_Discounts__c> transTasmanDiscountsResults = lController.searchResults2;
        system.assertEquals('Getaway', transTasmanDiscountsResults[0].Trans_Tasman_Eligible_Fare_Type6__c);
        system.assertEquals('Business Saver', transTasmanDiscountsResults[0].Trans_Tasman_Eligible_Fare_Type14__c);
        system.assertEquals('E/N/V', transTasmanDiscountsResults[0].Trans_Tasman_Eligible_Booking_Class6__c);
        List<International_Discounts_NZ__c> internationalDiscountsNZResults = lController.searchResults6;
        system.assertEquals(10, internationalDiscountsNZResults[0].DiscOffPublishFare_01__c);
        system.assertEquals(10, internationalDiscountsNZResults[0].DiscOffPublishFare_08__c);
        system.assertEquals(10, internationalDiscountsNZResults[0].DiscOffPublishFare_14__c);
        List<International_Discounts_HK__c> internationalDiscountsHKResults = lController.searchResults4;
        system.assertEquals(0, internationalDiscountsHKResults[0].IntlDisc_HK_DiscOffPublishFare_01__c);
        system.assertEquals(0, internationalDiscountsHKResults[0].IntlDisc_HK_DiscOffPublishFare_08__c);
        system.assertEquals(0, internationalDiscountsHKResults[0].IntlDisc_HK_DiscOffPublishFare_09__c);
        List<International_Discounts_China__c> internationalDiscountsChinaResults = lController.searchResults5;
        system.assertEquals(0, internationalDiscountsChinaResults[0].IntlDisc_DiscOffPublishFare_01__c);
        system.assertEquals(0, internationalDiscountsChinaResults[0].IntlDisc_DiscOffPublishFare_10__c);
        system.assertEquals(0, internationalDiscountsChinaResults[0].IntlDisc_DiscOffPublishFare_13__c);
    }
    
    @isTest
    private static void testInternationalSHDiscountControllerWithNewNZApexMessage() {
        Contract testContractObj = [SELECT Id FROM Contract];
        Test.startTest();
        InternationalSHDiscountController lController = getInternationalSHDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newNZPageReference = lController.newNZ();
        test.stopTest();
        //Asserts
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('A VA/NZ Tier has not been Selected in the contract so you cannot add a discount', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.ERROR, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testInternationalSHDiscountControllerWithNewHKApexMessage() {
        Contract testContractObj = [SELECT Id FROM Contract];
        Test.startTest();
        InternationalSHDiscountController lController = getInternationalSHDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newHKPageReference = lController.newHK();
        test.stopTest();
        //Asserts
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('A VA/HK Tier has not been Selected in the contract so you cannot add a discount', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.ERROR, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testInternationalSHDiscountControllerWithNewCNApexMessage() {
        Contract testContractObj = [SELECT Id FROM Contract];
        Test.startTest();
        InternationalSHDiscountController lController = getInternationalSHDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        PageReference newCNPageReference = lController.newCN();
        test.stopTest();
        //Asserts
        for(ApexPages.Message msg :ApexPages.getMessages()) {
            System.assertEquals('A VA/China Tier has not been Selected in the contract so you cannot add a discount', msg.getSummary());
            System.assertEquals(ApexPages.SEVERITY.ERROR, msg.getSeverity());
        }
    }
    
    @isTest
    private static void testInternationalSHDiscountControllerWithProperRecords() {
        Contract testContractObj = [SELECT Id, VA_ISH_Requested_Tier__c, Cos_Version__c, 
                                    PNG_PAC_Tier__c, Red_Circle__c, VA_NZ_Requested_Tier__c, 
                                    VA_HK_Requested_Tier__c, VA_China_Requested_Tier__c 
                                    FROM Contract];
        testContractObj.VA_ISH_Requested_Tier__c = '1';
        testContractObj.Cos_Version__c = '16.0';
        testContractObj.VA_NZ_Requested_Tier__c = '1';
        testContractObj.Red_Circle__c = false;
        testContractObj.VA_HK_Requested_Tier__c = '1';
        testContractObj.VA_China_Requested_Tier__c = '1';
        UPDATE testContractObj;
        
        Test.startTest();
        InternationalSHDiscountController lController = getInternationalSHDiscountControllerObj(testContractObj);
        PageReference initDiscPageReference = lController.initDisc();
        
        International_Disc_SH__c testInternationalDiscSHObj = TestDataFactory.createTestInternationalDiscSH(testContractObj.Id, '1', true, '16.0');
        INSERT testInternationalDiscSHObj;
        PageReference newDomPageReference = lController.newDom();
        
        SH_Discount_Pacific_PNG__c testSHDiscountPacificPNGObj = TestDataFactory.createTestSHDiscountPacificPNG(testContractObj.Id, '0', true, '16.0');
        INSERT testSHDiscountPacificPNGObj;
        PageReference newPNGPageReference = lController.newPNG();
        
        Trans_Tasman_Discounts__c testTransTasmanDiscountsObj = TestDataFactory.createTestTransTasmanDiscounts(testContractObj.Id, '1', true, '16.0');
        INSERT testTransTasmanDiscountsObj;
        PageReference newTransPageReference = lController.newTrans();
        
        International_Discounts_NZ__c testInternationalDiscountsNZObj = TestDataFactory.createTestInternationalDiscountsNZ(testContractObj.Id, '1', true, '16.0');
        INSERT testInternationalDiscountsNZObj;
        PageReference newNZPageReference = lController.newNZ();
        
        International_Discounts_HK__c testInternationalDiscountsHKObj = TestDataFactory.createTestInternationalDiscountsHK(testContractObj.Id, '1', true, '16.0');
        INSERT testInternationalDiscountsHKObj;
        PageReference newHKPageReference = lController.newHK();
        
        International_Discounts_China__c testInternationalDiscountsChinaObj = TestDataFactory.createTestInternationalDiscountsChina(testContractObj.Id, '1', true, '16.0');
        INSERT testInternationalDiscountsChinaObj;
        PageReference newCNPageReference = lController.newCN();
        test.stopTest();
        //Asserts
        List<International_Disc_SH__c> internationalDiscSHResults = lController.searchResults;
        system.assertEquals(testInternationalDiscSHObj.IntlDisc_SH_EligibleFareType__c, internationalDiscSHResults[0].IntlDisc_SH_EligibleFareType__c);
        system.assertEquals(testInternationalDiscSHObj.IntlDisc_SH_EligibleFareType3__c, internationalDiscSHResults[0].IntlDisc_SH_EligibleFareType3__c);
        system.assertEquals(testInternationalDiscSHObj.IntlDisc_SH_EligibleBookClass__c, internationalDiscSHResults[0].IntlDisc_SH_EligibleBookClass__c);
        system.assertEquals(testInternationalDiscSHObj.IntlDisc_SH_EligibleBookClass14__c, internationalDiscSHResults[0].IntlDisc_SH_EligibleBookClass14__c);
    }
    
    private static InternationalSHDiscountController getInternationalSHDiscountControllerObj(Contract testContractObj) {
        PageReference pageRef = Page.InternationalSHDiscountPage;
        pageRef.getParameters().put('id', testContractObj.id);
        Test.setCurrentPage(pageRef);
        //instantiate the InternationalDiscountController 
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractObj);
        return new InternationalSHDiscountController(conL);
    }
}