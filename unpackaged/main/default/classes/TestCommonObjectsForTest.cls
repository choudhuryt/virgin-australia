/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCommonObjectsForTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
    CommonObjectsForTest commonObjForTest = new CommonObjectsForTest();
    		
    		User user = new User();
    		user = commonObjForTest.CreateNewUser(0);
    		insert user;
    		
    		List<User> userList = new List<User>();
  			for (Integer i =1; i < 3; i++){
  			User user1 	= new User();
			user1 = commonObjForTest.CreateNewUser(i);
  			userList.add(user1);
  			
  			}
  			insert userList;
     
        
         Account account = new Account();
  			
         account = commonObjForTest.CreateAccountObject(0);
  		 insert account;
  			
  			//testing bulkified mode
  	    	List<Account> accountList = new List<Account>();
  			for (Integer i =1; i < 3; i++)
  			{
  			Account account1 	= new Account();
			account1 = commonObjForTest.CreateAccountObject(i);
  			accountList.add(account1);
  			
  			}
  			insert accountList;
  			
  		Contact newContact = new Contact();
  		newContact = commonObjForTest.CreateNewContact(0);
  		 insert newContact;
  		 
  		 //testing bulkified mode
  		 List <Contact> contactList = new List<Contact>();
  		 for (Integer i =1; i < 3; i ++)
  		 {
  		 
  		 Contact newContact1 = new Contact();
  		 newContact1 = commonObjForTest.CreateNewContact(i);
  		 contactList.add(newContact1);
       	 }
  		 	
  		 insert contactList;	
  		
  		Lead newLead = new Lead();
  		
  		newLead=commonObjForTest.CreateNewlead(0);
  		newLead.OwnerId = user.Id;
  		insert newLead;
  		
  		List<Lead> leadList = new List<Lead>();
  		
  		for (Integer i = 1; i <3; i ++)
  		{
  			Lead leadUPD = new lead();
  			leadUPD =commonObjForTest.CreateNewlead(i);
  			leadUPD.OwnerId = user.Id;
  			leadList.add(leadUPD);
  		}
  		insert leadList;
  		
  		
  		Opportunity  opportunity = new Opportunity();
  		
  		opportunity = commonObjForTest.CreateNewOpportunity(0);
        opportunity.AccountId = account.Id ;
  		insert opportunity;
  		
  		List<Opportunity> opplist = new List<Opportunity>();
  		
  		for (Integer i = 1 ; i < 3; i++)
  		{
  		Opportunity  opportunityBulk = new Opportunity();          
  		opportunityBulk = commonObjForTest.CreateNewOpportunity(i);
        opportunityBulk.AccountId =  account.Id ; 
  		opplist.add(opportunityBulk);
  	
  		}
  		insert opplist;
  			
        Contract contractOld = new Contract();
        
        contractOld = commonObjForTest.CreateOldStandardContract(0);
        contractOld.AccountId = account.id;
        contractOld.Status = 'Draft';    
        insert contractOld;
        contractOld.Status = 'Activated';  
        contractOld.Fixed_Amount__c = 100000;
        update contractOld;
        
  			
  		//testing bulkified mode 
  			List<Contract> contractList = new List<Contract>();
  			for (Integer i =1; i < 3; i++){
  			Contract contractOld1 	= new Contract();
			contractOld1 = commonObjForTest.CreateOldStandardContract(i);
  			contractList.add(contractOld1);
  			
  			}
  			
  			
  			for (Integer i = 0 ; i <2; i ++){
  			Contract contractupdate = new Contract();
  			
  			contractupdate = contractList.get(i);
  			contractupdate.AccountId =account.Id;
  			contractList.set(i, contractupdate);	
  				
  			}
  			
  			insert contractList;
    
    
        Velocity_Status_Match__c VSM = new Velocity_Status_Match__c();
        VSM = commonObjForTest.CreateVelocityStatusmatch(0);
        account.Velocity_Platinum_Status_Match_Available__c = 10;
        update account;
        
        VSM.Account__c =account.Id;
        
        VSM.Contract__c = contractOld.Id;
         //Added for failing test classes start 28/11/2019
        VSM.PA_Email__c = 'test@test.com.au';
     	VSM.PA_name__c = 'Sarita Gaur';
     	VSM.PA_Number__c = '1013143410';
     
       
        //Added for failing test classes stop 28/11/2019
        
        insert VSM;
         
        //testing bulkified mode 
  			List<Velocity_Status_Match__c> vsmList = new List<Velocity_Status_Match__c>();
  			for (Integer i =1; i < 3; i++){
  			Velocity_Status_Match__c vsmObj 	= new Velocity_Status_Match__c();
			vsmObj = commonObjForTest.CreateVelocityStatusmatch(i);
  			vsmList.add(vsmObj);
  			
  			}
  			
  			for (Integer becauseiCan = 0; becauseiCan <2; becauseiCan ++){
  			
  			Velocity_Status_Match__c VSMupdated = new Velocity_Status_Match__c();
  			VSMupdated = vsmList.get(becauseiCan);
  			VSMupdated.Account__c = account.Id;
  			VSMupdated.Contract__c = contractOld.Id;
            //Added for failing test classes start 28/11/2019
            VSMupdated.PA_Email__c = 'test@test.com.au';
            VSMupdated.PA_name__c = 'Sarita Gaur';
            VSMupdated.PA_Number__c = '1013143410';
    		//Added for failing test classes stop 28/11/2019
  			vsmList.set(becauseiCan,VSMupdated);
  			}
  			
  			
  			insert vsmList;
        
        
        Marketing_Spend_Down__c marketingSpendDown = new Marketing_Spend_Down__c();
        marketingSpendDown = commonObjForTest.CreatemarketingSpendDown(0);
        marketingSpendDown.Account__c =account.Id;
        marketingSpendDown.Contract__c = contractOld.Id;
        marketingSpendDown.Amount_to_be_Deducted__c = 1000 ; 
        insert marketingSpendDown;
        
        
        //testing bulkified mode 
  			List<Marketing_Spend_Down__c> mSpendList = new List<Marketing_Spend_Down__c>();
  			for (Integer i =1; i < 3; i++){
  			Marketing_Spend_Down__c mspend 	= new Marketing_Spend_Down__c();
			mspend = commonObjForTest.CreatemarketingSpendDown(i);
  			mSpendList.add(mspend);
  			
  			}
  			
  			for(Integer i =0; i <2; i ++){
  			Marketing_Spend_Down__c mspendUPD 	= new Marketing_Spend_Down__c();
  			mspendUPD = mSpendList.get(i);
  			mspendUPD.Account__c =account.Id;
  			mspendUPD.Contract__c = contractOld.Id;
  			mSpendList.set(i,mspendUPD);
  			}
  			insert mSpendList;
    
    	/*	SIP_Header__c sipheader = new SIP_Header__c();
    		sipheader = commonObjForTest.CreateNewSIPHeader(0);
    		sipheader.Contract__c = contractOld.Id;
    		
    		insert sipheader;
    		
    	
    		
    		SIP_Rebate__c sippRebate = new SIP_Rebate__c();
    		sippRebate = commonObjForTest.CreatenewSIPRebate(0);
    		sippRebate.Contract_Growth__c = contractOld.id;
    		sippRebate.SIP_Header__c = sipheader.id;
    		insert sippRebate;
    		
    		PCC__c PCC1 = new PCC__c();
    		PCC1 = commonObjForTest.CreatePCC(0);
    		PCC1.Contract__c = contractOld.Id;
    		//PCC1.IATA_PCC__c = 'a0090000002LajsAAC';
    		pcc1.Office_Store_Name__c ='abs';
    		insert PCC1;
    		
    		
    		
    		International_City_Pairs__c icp = new International_City_Pairs__c();
    		icp = commonObjForTest.CreateNewInternationalPairs(0);
    		icp.Contract__c = contractOld.id;
    		
    		insert icp;
    		
    		List<International_City_Pairs__c> icpList = new List<International_City_Pairs__c>();
    		for (Integer i = 0; i < 3; i ++){
    			International_City_Pairs__c icpUPD = new International_City_Pairs__c();
    			icpUPD = commonObjForTest.CreateNewInternationalPairs(i);
    			icpUPD.Contract__c = contractOld.id;
    			icpList.add(icpUPD);
    		}
    		insert icpList;
    
     PRISM_Key_Routes__c pkr1 = new PRISM_Key_Routes__c();
     pkr1 = commonObjForTest.CreatePrismKeyRoutes(0);
     pkr1.Account__c = account.id;
     insert pkr1;
     
     List <PRISM_Key_Routes__c> prismKeyRoutesList = new List<PRISM_Key_Routes__c>();
     for (Integer i = 0 ; i < 3; i++){
     	PRISM_Key_Routes__c pkr1UPD = new PRISM_Key_Routes__c();
     	pkr1UPD = commonObjForTest.CreatePrismKeyRoutes(i);
     	pkr1UPD.Account__c =account.id;
     	prismKeyRoutesList.add(pkr1UPD);
     	
     }insert prismKeyRoutesList;*/
    
    
    }
}