/**
* @description       : Test class for UpdateInactiveAccountValidity
* @CreatedBy         : CloudWerx
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestUpdateInactiveAccountValidity {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;
        
        User u = TestUtilityClass.createTestUser();
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Sales Super User' LIMIT 1].Id;
        INSERT u;
        
        system.runAs(u){
            Account testAccountObj1 = TestDataFactory.createTestAccount('testAccountDataValidity', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
            INSERT testAccountObj1;
            
            Account_Data_Validity__c testAccountDataValidityObj = TestUtilityClass.createAccountDataValidity(testAccountObj.Id, u.Id, testAccountObj1.Id);
            testAccountDataValidityObj.To_Date__c = null;
            testAccountDataValidityObj.Soft_Delete__c = false;
            testAccountDataValidityObj.Account_Record_Type__c = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Corporate').getRecordTypeId();
            INSERT testAccountDataValidityObj;
        }
        
        Contract testContractObj = TestDataFactory.createTestContract('PUTCONTRACTNAMEHERE', testAccountObj.Id, 'Draft', '15', null, true, Date.newInstance(2018,01,01));
        INSERT testContractObj;
        testContractObj.status = 'Activated';
        UPDATE testContractObj;
        
        Opportunity testOpportunityObj = TestDataFactory.createTestOpportunity('testOpp1', 'Open', testAccountObj.Id, Date.today(), 1.00);
        INSERT testOpportunityObj; 
        
        Contact testContactObj = TestDataFactory.createTestContact(testAccountObj.Id, 'test@gmail.com', 'Test', 'Contact', 'Manager', '1234567890', 'Active', 'First Nominee, Second Nominee', '1234567890');
        INSERT testContactObj;
    }
    
    @isTest
    private static void testUpdateInactiveAccount() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Test.startTest();
        UpdateInactiveAccountValidity.UpdateTheAccount(accountId);
        Test.stopTest();
        Account_Data_Validity__c updatedAccountDataValidityObj = [SELECT Id, To_Date__c FROM Account_Data_Validity__c LIMIT 1];
        system.assertEquals(Date.today(), updatedAccountDataValidityObj.To_Date__c);
    }
    
    @isTest
    private static void testUpdateInactiveContract() {
        Id accountId = [SELECT Id FROM Account LIMIT 1]?.Id;
        Test.startTest();
        UpdateInactiveAccountValidity.UpdateTheContract(accountId);
        Test.stopTest();
        Opportunity updatedOpportunityObj = [SELECT Id, Status__c, StageName, Lost_Reason__c FROM Opportunity LIMIT 1];
        system.assertEquals('Account Closed', updatedOpportunityObj.Status__c);
        system.assertEquals('Closed Lost', updatedOpportunityObj.StageName);
        system.assertEquals('Account Closed', updatedOpportunityObj.Lost_Reason__c);
        
        Contact updatedContactObj = [SELECT Id, status__c FROM Contact LIMIT 1];
        system.assertEquals('Inactive', updatedContactObj.status__c);
 
        Contract updatedContractObj = [SELECT Id, Status, Reason_for_Termination__c, EndDate FROM Contract LIMIT 1];
        system.assertEquals('Terminated', updatedContractObj.Status);
        system.assertEquals('Closed Account', updatedContractObj.Reason_for_Termination__c);
        system.assertEquals(Date.today(), updatedContractObj.EndDate);
    }
}