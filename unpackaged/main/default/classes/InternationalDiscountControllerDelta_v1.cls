public class InternationalDiscountControllerDelta_v1 {
     //standard controller and search results
     private ApexPages.StandardController controller {get; set;}
     public List<International_Discounts_USA_Canada__c> searchResults {get;set;}    
     public List <Contract> contracts {get;set;}
     public Contract originalContract{get;set;}
 	 public Boolean redCircle {get;set;}
     //flags
     public integer usacanFlag {get; set;} 
     public Boolean isTemplateDiscount {get;set;}
     public Boolean isCos13greater {get;set;}
     public Boolean isCos13_4greater {get;set;}
     public Boolean isCos14_4lesser{get;set;}
     //contract instance
     private Contract a;

   
    // Constructor
    
    public InternationalDiscountControllerDelta_v1(ApexPages.StandardController myController) 
    {
        a=(Contract)myController.getrecord();
        usacanFlag=0;
        
    }
 
    //pre-processing prior to page load
     public PageReference initDisc() 
     {
   
   	contracts=[select id,Cos_Version__c,VA_DL_Requested_Tier__c,Red_Circle__c,IsTemplate__c from Contract where id =:ApexPages.currentPage().getParameters().get('id')];
   	originalContract = contracts.get(0);
   	isTemplateDiscount = originalContract.IsTemplate__c;
         
         
   	
   	if(originalContract.Cos_Version__c != null)
    {
        
       if(originalContract.Cos_Version__c.startsWith('13') || originalContract.Cos_Version__c.startsWith('14')  )
       {
   			isCos13greater = true;
   	   }
       
      isCos14_4lesser = false;
        
      if( originalContract.Cos_Version__c.startsWith('14.3')  )
       {
   			isCos14_4lesser = true;
   	   }
        
    }
   	
       
         
     searchResults= [
         SELECT Applicable_Routes_BNE_SYD_MEL_to__c, CreatedById, CreatedDate, IsDeleted,
         Int_DiscountUSACANLookupToContract__c, International_Discount_USA_CAN_Exists__c, Name, 
         Intl_Discount_USA_CAN_EligBk_Class__c, Intl_Discount_USA_CAN_EligBk_Class2__c, 
         Intl_Discount_USA_CAN_EligBk_Class3__c, Intl_Discount_USA_CAN_EligBk_Class4__c, 
         Intl_Discount_USA_CAN_EligBk_Class5__c, Intl_Discount_USA_CAN_EligBk_Class6__c, 
         Intl_Discount_USA_CAN_EligBk_Class7__c, Intl_Discount_USA_CAN_Eligible_Fare_Type__c, 
         Intl_Discount_USA_CAN_Eligible_FareType2__c, Intl_Discount_USA_CAN_Eligible_FareType3__c, 
         Intl_Discount_USA_CAN_Eligible_FareType4__c,Intl_Discount_USA_CAN_Eligible_FareType5__c,
         Intl_Discount_USA_CAN_Eligible_FareType6__c,Intl_Discount_USA_CAN_Eligible_FareType7__c,
         Intl_Discount_USA_CAN_Eligible_FareTyp8__c,
         Intl_Discount_USA_CAN_off_Published_Fare__c, Intl_Discount_USA_CAN_EligBk_Class8__c,
         Intl_Discount_USA_CAN_off_PublishedFare2__c, Intl_Discount_USA_CAN_off_PublishedFare3__c, 
         Intl_Discount_USA_CAN_off_PublishedFare4__c, Intl_Discount_USA_CAN_off_PublishedFare5__c, 
         Intl_Discount_USA_CAN_off_PublishedFare6__c, Intl_Discount_USA_CAN_off_PublishedFare7__c,
         Intl_Discount_USA_CAN_EligBk_Class9__c,Intl_Discount_USA_CAN_EligBk_Class10__c,
		 Intl_Discount_USA_CAN_EligBk_Class11__c,Intl_Discount_USA_CAN_EligBk_Class12__c,
		 Intl_Discount_USA_CAN_Eligible_FareType9__c,Intl_Discount_USA_CAN_off_PublishedFare9__c,
		 Intl_Dis_USA_CAN_off_PublishedFare10__c,Intl_Dis_USA_CAN_off_PublishedFare11__c,Intl_Dis_USA_CAN_off_PublishedFare12__c,
		 Intl_Dis_USA_CAN_Eligible_FareType10__c,Intl_Dis_USA_CAN_Eligible_FareType13__c,Intl_Dis_USA_CAN_Eligible_FareType14__c,Intl_Dis_USA_CAN_Eligible_FareType15__c,Intl_Discount_USA_CAN_EligBk_Class13__c,
		 Intl_Discount_USA_CAN_EligBk_Class14__c,Intl_Discount_USA_CAN_EligBk_Class15__c,Intl_Dis_USA_CAN_off_PublishedFare13__c,Intl_Dis_USA_CAN_off_PublishedFare14__c,
		 Intl_Dis_USA_CAN_off_PublishedFare15__c,Intl_Dis_USA_CAN_off_PublishedFare16__c,
		 Intl_Dis_USA_CAN_Eligible_FareType11__c,Intl_Dis_USA_CAN_Eligible_FareType12__c,
         Intl_Dis_USA_CAN_off_PublishedFare17__c,Intl_Dis_USA_CAN_off_PublishedFare18__c,
         Intl_Discount_USA_CAN_EligBk_Class18__c,Intl_Discount_USA_CAN_EligBk_Class17__c,
         Intl_Dis_USA_CAN_Eligible_FareType18__c,Intl_Dis_USA_CAN_Eligible_FareType17__c,
         LastModifiedById, LastModifiedDate, OwnerId, Id, SystemModstamp,Intl_Discount_USA_CAN_off_PublishedFare8__c,Cos_Version__c,Tier__c,Is_Template__c 
         FROM International_Discounts_USA_Canada__c
         where Int_DiscountUSACANLookupToContract__r.id = :ApexPages.currentPage().getParameters().get('id')
         and Status__c = 'Active'
         ORDER BY CreatedDate];
         
         system.debug('Final page load' + searchResults  );
         
      if(searchResults.size()>0)
      {
        usacanFlag=1;
      }
      
       return null;
      }
          
        
    
    
 
  // fired when the save records button is clicked
  public PageReference save() {
   // try {
      upsert searchResults;

      
      UpdateContractPercentagesForContractCalculation();
      
   // } Catch (DMLException e) {
   //   ApexPages.addMessages(e);
   //   return null;
   //}
 
    //return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
    PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
                  //
                  thePage.setRedirect(true);
                  //
                  return thePage;
  }
  
  // fired when the save records button is clicked
  public PageReference qsave() {
   // try {
   
      system.debug('This is the value to be upserted' +  searchResults );
      upsert searchResults;
    
      
      UpdateContractPercentagesForContractCalculation();
      
   // } Catch (DMLException e) {
   //   ApexPages.addMessages(e);
   //   return null;
   //}
 
    //return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
    
    PageReference thePage = new PageReference('/apex/InternationalDiscountDeltaPage_v1?id=' + a.Id);
    thePage.setRedirect(true);
    return thePage;
      
  }
  
  
 
  // takes user back to main record
  public PageReference cancel() {
   // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
   PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
                  //
                  thePage.setRedirect(true);
                  //
                  return thePage;
   
  }
  
 
  public PageReference newInterUSA() 
  {
    
  
   if (searchResults.size()<1)
      {
      	 
      	 			International_Discounts_USA_Canada__c deltaDiscount = new International_Discounts_USA_Canada__c();
      	 			International_Discounts_USA_Canada__c deltaTemplateDiscount = new International_Discounts_USA_Canada__c();
                  	List<International_Discounts_USA_Canada__c>	deltaTemplateDiscountList = new List<International_Discounts_USA_Canada__c>();
                  
                  	Boolean isTemplate =false;
                  	redCircle = false;
       				
       				
       	if(originalContract.VA_DL_Requested_Tier__c == null){
       		ApexPages.addMessage( new ApexPages.message( ApexPages.severity.ERROR,
                                    'A Delta Tier has not been Selected in the contract so you cannot add a discount' ));
                                    return null;
       	}
    	
       	deltaTemplateDiscountList =
       	[
       	SELECT Applicable_Routes_BNE_SYD_MEL_to__c, CreatedById, CreatedDate, IsDeleted,
         Int_DiscountUSACANLookupToContract__c, International_Discount_USA_CAN_Exists__c, Name, 
         Intl_Discount_USA_CAN_EligBk_Class__c, Intl_Discount_USA_CAN_EligBk_Class2__c, 
         Intl_Discount_USA_CAN_EligBk_Class3__c, Intl_Discount_USA_CAN_EligBk_Class4__c, 
         Intl_Discount_USA_CAN_EligBk_Class5__c, Intl_Discount_USA_CAN_EligBk_Class6__c, 
         Intl_Discount_USA_CAN_EligBk_Class7__c, Intl_Discount_USA_CAN_Eligible_Fare_Type__c, 
         Intl_Discount_USA_CAN_Eligible_FareType2__c, Intl_Discount_USA_CAN_Eligible_FareType3__c, 
         Intl_Discount_USA_CAN_Eligible_FareType4__c,Intl_Discount_USA_CAN_Eligible_FareType5__c,
         Intl_Discount_USA_CAN_Eligible_FareType6__c,Intl_Discount_USA_CAN_Eligible_FareType7__c,
         Intl_Discount_USA_CAN_Eligible_FareTyp8__c,
         Intl_Discount_USA_CAN_off_Published_Fare__c, Intl_Discount_USA_CAN_EligBk_Class8__c,
         Intl_Discount_USA_CAN_off_PublishedFare2__c, Intl_Discount_USA_CAN_off_PublishedFare3__c, 
         Intl_Discount_USA_CAN_off_PublishedFare4__c, Intl_Discount_USA_CAN_off_PublishedFare5__c, 
         Intl_Discount_USA_CAN_off_PublishedFare6__c, Intl_Discount_USA_CAN_off_PublishedFare7__c,
         Intl_Discount_USA_CAN_EligBk_Class9__c,Intl_Discount_USA_CAN_EligBk_Class10__c,
		 Intl_Discount_USA_CAN_EligBk_Class11__c,Intl_Discount_USA_CAN_EligBk_Class12__c,
		 Intl_Discount_USA_CAN_Eligible_FareType9__c,Intl_Discount_USA_CAN_off_PublishedFare9__c,
		 Intl_Dis_USA_CAN_off_PublishedFare10__c,Intl_Dis_USA_CAN_off_PublishedFare11__c,Intl_Dis_USA_CAN_off_PublishedFare12__c,
		 Intl_Dis_USA_CAN_Eligible_FareType10__c,Intl_Dis_USA_CAN_Eligible_FareType13__c,Intl_Dis_USA_CAN_Eligible_FareType14__c,Intl_Dis_USA_CAN_Eligible_FareType15__c,Intl_Discount_USA_CAN_EligBk_Class13__c,
		 Intl_Discount_USA_CAN_EligBk_Class14__c,Intl_Discount_USA_CAN_EligBk_Class15__c,Intl_Dis_USA_CAN_off_PublishedFare13__c,Intl_Dis_USA_CAN_off_PublishedFare14__c,
		 Intl_Dis_USA_CAN_off_PublishedFare15__c,Intl_Dis_USA_CAN_off_PublishedFare16__c,Intl_Discount_USA_CAN_EligBk_Class16__c, 
		 Intl_Dis_USA_CAN_Eligible_FareType11__c,Intl_Dis_USA_CAN_Eligible_FareType12__c,Intl_Dis_USA_CAN_Eligible_FareType16__c,
         Intl_Dis_USA_CAN_off_PublishedFare17__c,Intl_Dis_USA_CAN_off_PublishedFare18__c,
         Intl_Discount_USA_CAN_EligBk_Class18__c,Intl_Discount_USA_CAN_EligBk_Class17__c,
         Intl_Dis_USA_CAN_Eligible_FareType18__c,Intl_Dis_USA_CAN_Eligible_FareType17__c,   
          LastModifiedById, LastModifiedDate, OwnerId, Id, SystemModstamp,Intl_Discount_USA_CAN_off_PublishedFare8__c,Cos_Version__c,Tier__c,Is_Template__c 
          FROM International_Discounts_USA_Canada__c
          where Is_Template__c = true and Tier__c = :originalContract.VA_DL_Requested_Tier__c and  Cos_Version__c = :originalContract.Cos_Version__c
         ];
                
           if (deltaTemplateDiscountList.size() > 0){
           	
           	deltaTemplateDiscount = deltaTemplateDiscountList.get(0);
           	isTemplate=false;
           
           }else{
           	
           	isTemplate=true;
           }
       if(originalContract.Red_Circle__c ==true){
       	redCircle = true;
       	
       }else{
       	redCircle = false;
       }
       		
            
                    deltaDiscount.Int_DiscountUSACANLookupToContract__c = ApexPages.currentPage().getParameters().get('id');
                    
                     if(isTemplate == true){    
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_Fare_Type__c    =    'Business';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType2__c    =    'Business';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType3__c    =    'Business Saver';
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType17__c	     =	  'Business Tactical';     
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType9__c    =	  'Business Saver';                         
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType14__c	     =	  'Business Tactical';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType4__c    =    'Premuim Economy';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType5__c    =    'Premuim Economy';
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType15__c        =    'Premuim Saver';
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType18__c        =    'Premuim Tactical';     
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType6__c    =    'Freedom';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType7__c    =    'Freedom';
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareTyp8__c     =    'Freedom';
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType10__c        =    'Freedom';
					deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType11__c        =    'Elevate';
					deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType12__c        =    'Elevate';
					deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType13__c        =    'Elevate';
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType16__c        =    'Elevate';
					
                    
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class__c  =  'J';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class2__c =  'C';            
                    
                    if(isCos13greater ==true){
                    	deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class3__c =  'D';                        
                    }else{
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class3__c =  'D/I';
                    }
                     deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class17__c = 'D*';    
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class9__c =  'I';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class14__c = 'I*';     
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class4__c =  'W';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class5__c =  'R';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class15__c = 'O'; 
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class18__c = 'O*';         
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class6__c =  'Y';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class7__c =  'B';
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class8__c =  'H'; 
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class10__c = 'K';
					deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class11__c = 'L';
					deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class12__c = 'E';
					deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class13__c = 'N';
					deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class16__c = 'V';
                    
                    deltaDiscount.Intl_Discount_USA_CAN_off_Published_Fare__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare2__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare3__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare9__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare4__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare5__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare6__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare7__c =  0;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare8__c =  0;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare10__c     =  0;
					deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare11__c     =  0;
					deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare12__c     =  0;
					deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare13__c     =  0;
					deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare17__c     =  0;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare18__c     =  0;
                    deltaDiscount.International_Discount_USA_CAN_Exists__c = true;
                    deltaDiscount.Status__c = 'Active'  ;  
                    deltaDiscount.Applicable_Routes_BNE_SYD_MEL_to__c = 'USA/Canada/Mexico';
                     }
                   else
                   
                   {
                     	
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_Fare_Type__c    =    deltaTemplateDiscount.Intl_Discount_USA_CAN_Eligible_Fare_Type__c;
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType2__c    =    deltaTemplateDiscount.Intl_Discount_USA_CAN_Eligible_FareType2__c;
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType3__c    =    deltaTemplateDiscount.Intl_Discount_USA_CAN_Eligible_FareType3__c;
                    
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType9__c    =    deltaTemplateDiscount.Intl_Discount_USA_CAN_Eligible_FareType9__c;
                    
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType4__c    =    deltaTemplateDiscount.Intl_Discount_USA_CAN_Eligible_FareType4__c;
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType5__c    =    deltaTemplateDiscount.Intl_Discount_USA_CAN_Eligible_FareType5__c;
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType6__c    =    deltaTemplateDiscount.Intl_Discount_USA_CAN_Eligible_FareType6__c;
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareType7__c    =   deltaTemplateDiscount.Intl_Discount_USA_CAN_Eligible_FareType7__c;
                    deltaDiscount.Intl_Discount_USA_CAN_Eligible_FareTyp8__c    =    deltaTemplateDiscount.Intl_Discount_USA_CAN_Eligible_FareTyp8__c;
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType10__c    =    deltaTemplateDiscount.Intl_Dis_USA_CAN_Eligible_FareType10__c;
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType11__c    =    deltaTemplateDiscount.Intl_Dis_USA_CAN_Eligible_FareType11__c;
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType12__c    =    deltaTemplateDiscount.Intl_Dis_USA_CAN_Eligible_FareType12__c;
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType13__c    =    deltaTemplateDiscount.Intl_Dis_USA_CAN_Eligible_FareType13__c;
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType14__c    =    deltaTemplateDiscount.Intl_Dis_USA_CAN_Eligible_FareType14__c;
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType15__c    =    deltaTemplateDiscount.Intl_Dis_USA_CAN_Eligible_FareType15__c;
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType16__c    =    deltaTemplateDiscount.Intl_Dis_USA_CAN_Eligible_FareType16__c;                    
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType17__c    =    deltaTemplateDiscount.Intl_Dis_USA_CAN_Eligible_FareType17__c;  
                    deltaDiscount.Intl_Dis_USA_CAN_Eligible_FareType18__c    =    deltaTemplateDiscount.Intl_Dis_USA_CAN_Eligible_FareType18__c;  
                       
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class__c  =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class2__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class2__c;            
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class3__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class3__c;
                    
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class9__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class9__c;
                    
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class4__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class4__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class5__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class5__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class6__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class6__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class7__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class7__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class8__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class8__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class10__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class10__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class11__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class11__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class12__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class12__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class13__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class13__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class14__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class14__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class15__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class15__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class16__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class16__c; 
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class17__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class17__c;
                    deltaDiscount.Intl_Discount_USA_CAN_EligBk_Class18__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_EligBk_Class18__c; 
                       
                    deltaDiscount.Intl_Discount_USA_CAN_off_Published_Fare__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_off_Published_Fare__c;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare2__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_off_PublishedFare2__c;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare3__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_off_PublishedFare3__c;
                    
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare9__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_off_PublishedFare9__c;
                    
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare4__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_off_PublishedFare4__c;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare5__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_off_PublishedFare5__c;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare6__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_off_PublishedFare6__c;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare7__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_off_PublishedFare7__c;
                    deltaDiscount.Intl_Discount_USA_CAN_off_PublishedFare8__c =  deltaTemplateDiscount.Intl_Discount_USA_CAN_off_PublishedFare8__c;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare10__c =  deltaTemplateDiscount.Intl_Dis_USA_CAN_off_PublishedFare10__c;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare11__c =  deltaTemplateDiscount.Intl_Dis_USA_CAN_off_PublishedFare11__c;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare12__c =  deltaTemplateDiscount.Intl_Dis_USA_CAN_off_PublishedFare12__c;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare13__c =  deltaTemplateDiscount.Intl_Dis_USA_CAN_off_PublishedFare13__c;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare14__c =  deltaTemplateDiscount.Intl_Dis_USA_CAN_off_PublishedFare14__c;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare15__c =  deltaTemplateDiscount.Intl_Dis_USA_CAN_off_PublishedFare15__c;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare16__c =  deltaTemplateDiscount.Intl_Dis_USA_CAN_off_PublishedFare16__c;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare17__c =  deltaTemplateDiscount.Intl_Dis_USA_CAN_off_PublishedFare17__c;
                    deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare18__c =  deltaTemplateDiscount.Intl_Dis_USA_CAN_off_PublishedFare18__c;
                    deltaDiscount.International_Discount_USA_CAN_Exists__c = true;
                    deltaDiscount.Applicable_Routes_BNE_SYD_MEL_to__c = 'USA/Canada/Mexico' ;
                    deltaDiscount.Status__c = 'Active';
                     }
                       system.debug ('This is a check' +  deltaDiscount.Intl_Dis_USA_CAN_off_PublishedFare16__c    )   ; 
                   searchResults.add(deltaDiscount);  
                        
                  
                    return null;
                   }
    else{
  
             PageReference thePage = new PageReference('/apex/InternationalDiscountDeltaPage_v1?id=' + a.Id);
             thePage.setRedirect(true);
             return thePage;
         }
      
  }
  
 
  
  public PageReference remUSACAN() 
  {
        International_Discounts_USA_Canada__c toDelete = searchResults[searchResults.size()-1];
            
        delete toDelete;
      
        List<Contract> contractlist = new List<Contract>();
        Contract contract = new Contract();
        contract = [Select DLDiscountAvailable__c From Contract where id = :a.id ];
        contract.DLDiscountAvailable__c = false ;
        update contract;
        
         PageReference thePage = new PageReference('/apex/InternationalDiscountDeltaPage_v1?id=' + a.Id);
                  
                  thePage.setRedirect(true);
                  
                  return thePage;
  }
  
    

   

      
       
  public Boolean UpdateContractPercentagesForContractCalculation(){
    
    
    List <Decimal> sortOrder = new List<Decimal>();
        if(searchResults != null){
        if(searchResults.size()>0){
      International_Discounts_USA_Canada__c intUSACan = new International_Discounts_USA_Canada__c();        
      intUSACan = searchResults.get(0);
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_Published_Fare__c); 
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare2__c);
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare3__c); 
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare4__c);
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare5__c); 
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare6__c); 
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare8__c);
      sortOrder.add(intUSACan.Intl_Discount_USA_CAN_off_PublishedFare7__c);
        }
    }
       
 
      if(sortOrder.size() > 0){
  
      
      sortOrder.sort();
    
    Integer i = sortOrder.size();
    Decimal d = sortorder.get(i -1);
    Boolean dl= true;      
    
    Contract contract = new Contract();
    
    contract = [Select UpperPerInt__c, DLDiscountAvailable__c  From Contract where id = :a.id ];
    if(contract.UpperPerInt__c <> null){
    contract.UpperPerInt__c = contract.UpperPerInt__c + d;	
    }else{
    	contract.UpperPerInt__c =  d;
    }
    contract.DLDiscountAvailable__c = dl;
          
    update contract;
          
      }
    return true;
    
  }
 

}