@isTest
public class WaiverTriggerHandlerTest {
    
@isTest static void callheadofficewaiverpointcalculation() 
   {
      User u = TestUtilityClass.createTestUser();
      insert u;
        
     system.runAs(u)
      {
        Test.startTest();
        Account acc = TestUtilityClass.createTestAccount();      
        insert acc;     
       
        Waiver_Point__c wp1 =   TestUtilityClass.createTestHeadOfficeLevelWaiverPoint(acc.Id) ; 
        insert wp1;
          
        Waiver_Favour__c w1  = TestUtilityClass.createTestHeadOfficePointWaiverFavour(acc.Id, u.Id);          
        insert w1; 
          
        Waiver_Ticket_Details__c wt1 = TestUtilityClass.createTestWaiverTicket(w1.Id)  ;
        insert wt1; 
      }
       
   }  
    
   
@isTest static void calltravelbankcompensationwaiver() 
   {
      User u = TestUtilityClass.createTestUser();
      insert u;
      system.runAs(u)
      { 
      Test.startTest();
      Account acc = TestUtilityClass.createTestAccount();      
      insert acc; 
          
       Waiver_Favour__c w1  = TestUtilityClass.createTestTravelBankCompensationWaiver(acc.Id, u.Id);          
       insert w1; 
          
        Waiver_Point__c wp1 =   TestUtilityClass.createTestTravelBankWaiverPoint(acc.Id, u.Id); 
        insert wp1;
          
       w1.Status__c = 'Pending Approval';
       update w1; 
          
      }
   }       
    
 
   
   @isTest static void callstorelevelwaiverpointcalculation() 
   {
      User u = TestUtilityClass.createTestUser();
      insert u;
        
     system.runAs(u)
      {
        Test.startTest();
        Account acc = TestUtilityClass.createTestAccount();         
        insert acc; 
        Account acc1 = TestUtilityClass.createTestAccount();
        insert acc1; 
        acc1.IsWavierParentAccount__c = true ;  
        update acc1 ;
        Waiver_Point__c wp2 =   TestUtilityClass.createTestStoreLevelWaiverPoint(acc.Id, acc1.Id);       
        insert wp2;     
        acc.Waiver_Parent_Account__c = acc1.Id;               
        update acc ;   
        Waiver_Favour__c w3  = TestUtilityClass.createTestStoreLevelPointWaiverFavour(acc.Id, u.Id); 
        w3.Notify_Account_Owner__c= false ; 
        insert w3;     
        Waiver_Ticket_Details__c wt3 = TestUtilityClass.createTestWaiverTicket(w3.Id)  ;  
        insert wt3;             
      }
   }   
     
    
    @isTest static void callaccountmanagerwaiverpointcalculation() 
   {
      User u = TestUtilityClass.createTestUser();     
      insert u;
     
       
      system.runAs(u)
      {
        Test.startTest();
        Account acc = TestUtilityClass.createTestAccount();
        insert acc; 
          
        Waiver_Point__c wp3 =   TestUtilityClass.createTestAMLevelWaiverPoint(u.Id);    
        insert wp3;  
        
        Waiver_Favour__c w1  = TestUtilityClass.createAccountManagerPointWaiverFavour(acc.Id, u.Id);        
        insert w1;     
        Waiver_Ticket_Details__c wt1 = TestUtilityClass.createTestWaiverTicket(w1.Id)  ;  
        insert wt1;              
      }
   }
    
   @isTest static void callcommecialadhocapproval() 
   {
      User u = TestUtilityClass.createTestUser();     
      insert u;
     
       
      system.runAs(u)
      {
        Test.startTest();
        Account acc = TestUtilityClass.createTestAccount();
        insert acc; 
          
        Waiver_Favour__c w1 =   TestUtilityClass.createCommecialAdhocWaiver(acc.Id, u.Id);    
        insert w1;  
         
        Waiver_Ticket_Details__c wt1 = TestUtilityClass.createTestWaiverTicket(w1.Id)  ;  
        insert wt1; 
        
          Approval.ProcessSubmitRequest req1 =  new Approval.ProcessSubmitRequest();
          req1.setComments('Submitting request for approval.');
          req1.setObjectId(w1.id);

        // Submit on behalf of a specific submitter
          req1.setSubmitterId(u.Id); 

        // Submit the record to specific process and skip the criteria evaluation
        req1.setProcessDefinitionNameOrId('Waiver_Approval_Process_VISTA_Commercial');
        req1.setSkipEntryCriteria(true);

        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
          
        w1.Approval_Comment_Check__c = 'Requested' ;
        update w1;  
          
      }
   }  

   }