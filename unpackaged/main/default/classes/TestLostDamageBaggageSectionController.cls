@isTest(SeeAllData=true)
public class TestLostDamageBaggageSectionController{
    static testMethod void testNewCase(){
        Test.setMock(WebServiceMock.class, new LostBaggageMockImpl());
        
        Case caseTmp = new Case();
        insert caseTmp;
        ApexPages.StandardController sc = new ApexPages.standardController(caseTmp);
        LostDamagedBaggagedSectionController e = new LostDamagedBaggagedSectionController(sc);
        
        Test.startTest();
        PageReference opptyPage = new PageReference('/apex/LostBagDetails?id='+caseTmp.Id);
        Test.setCurrentPage(opptyPage);
        LostBagDetailsController de = new LostBagDetailsController();
        de.SaveSearchHistory();
        Test.stopTest();
        
        List<Lost_Baggage__c> lostBaggageItem = [SELECT Id 
                                                 FROM Lost_Baggage__c
                                                 WHERE Case__c = :caseTmp.Id];
        System.assert(lostBaggageItem.size()>0);
    }
}