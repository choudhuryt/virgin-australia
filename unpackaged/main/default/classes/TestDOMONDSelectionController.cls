/**
* @description       : Test class for DOMONDSelectionController
* @UpdatedBy         : CloudWerx
**/
@isTest
private class TestDOMONDSelectionController {
    
    @testSetup static void setup() {
        Account testAccountObj = TestDataFactory.createTestAccount('TEST177', 10, 'AU', '1 Main St.', 'VA', '12345', 'Anytown', 'TMC', 'Thailand', '1m+', 'Flexi Only', true, 10, 10, 10, 1);
        INSERT testAccountObj;  
        
        Contract testContractObj = TestDataFactory.createTestContract('TestContract', testAccountObj.Id, 'Draft', '15', null, true, Date.today());
        testContractObj.Cos_Version__c ='16.0';
        testContractObj.Red_Circle__c = true;
        INSERT testContractObj; 
        
        Contract_Addendum__c testContractAddendumObj = TestDataFactory.createTestContractAddendum(testContractObj.Id, '16.0', 'Tier 1', 'Tier 1', true, 'SYDMEL', 'PERKTH', 'Draft');
        INSERT testContractAddendumObj;
    }
    
    @isTest
    private static void testSQONDSelectionController() {
        Contract_Addendum__c testContractAddendumObj = [SELECT Id, Regional_OD_Pairs__c, Domestic_OD_Pairs__c FROM Contract_Addendum__c];
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractAddendumObj);
        DOMONDSelectionController lController = new DOMONDSelectionController(conL);
        Test.startTest(); 
        
        PageReference  pageRef = Page.REGONDSelection;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', testContractAddendumObj.Id);
        pageRef = lController.initDisc();
        pageRef = lController.save(); 
        
        lController.REGONDItems = new List<String> {'ABXSYD', 'BNECNJ'};
            string [] testREGONDItems = lController.REGONDItems;
        
        lController.DOMONDItems = new List<String> {'ADLBNE', 'ADLDRW'};
            string [] testDOMONDItems = lController.DOMONDItems;
        test.stopTest();
        //Asserts
        System.assertEquals('PERKTH', testContractAddendumObj.Regional_OD_Pairs__c);
        System.assertEquals('ABXSYD', testREGONDItems[0]);
        System.assertEquals('BNECNJ', testREGONDItems[1]);
        System.assertEquals('SYDMEL', testContractAddendumObj.Domestic_OD_Pairs__c);
        System.assertEquals('ADLBNE', testDOMONDItems[0]);
        System.assertEquals('ADLDRW', testDOMONDItems[1]);
    }
    
    @isTest
    private static void testSQONDSelectionControllerException() {
        Contract_Addendum__c testContractAddendumObj = [SELECT Id FROM Contract_Addendum__c];
        ApexPages.StandardController conL = new ApexPages.StandardController(testContractAddendumObj);
        DOMONDSelectionController lController = new DOMONDSelectionController(conL);
        Test.startTest(); 
        PageReference  pageRef = Page.REGONDSelection;
        Test.setCurrentPage(pageRef);
        pageRef = lController.initDisc();
        test.stopTest();
        //Asserts
        System.assertEquals(null, pageRef);
    }
}