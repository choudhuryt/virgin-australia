@isTest
private class TestMaretingSpendDownHandler
{
    
    static testMethod void testNewVersionPage_whenUserChoosesYes_AndStatusIsActivated() 
    {
        //setup account
            Account account                 = new Account();
            CommonObjectsForTest commonObjForTest = new CommonObjectsForTest();
            account = commonObjForTest.CreateAccountObject(0);
            insert account;
 
            Contract contractOld = new Contract();
        
            contractOld = commonObjForTest.CreateOldStandardContract(0);
            contractOld.AccountId = account.id;
            contractOld.Fixed_Amount__c = 100000;
            contractOld.Status = 'Draft';
            insert contractOld;
            
            contractOld.Status = 'Activated';
            update contractOld ;
        
            Marketing_Spend_Down__c marketingSpendDown = new Marketing_Spend_Down__c();
            marketingSpendDown = commonObjForTest.CreatemarketingSpendDown(0);
            marketingSpendDown.Account__c =account.Id;
            marketingSpendDown.Contract__c = contractOld.Id;
            marketingSpendDown.Amount_to_be_Deducted__c = 1000 ; 
            marketingSpendDown.Approval_Status__c = 'Draft';
           insert marketingSpendDown;
        
           marketingSpendDown.Approval_Status__c = 'Funds/Tickets Allocated';
           update marketingSpendDown ;
    }

}