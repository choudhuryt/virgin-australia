public class Inteflowapexcallout {
    
 
    public  static void invokeExternalWs(ID AccId,string Accountid,string ABN_ACN, string AccountName,
                                      string Website,string BillingStreet,string BillingCity,
                                      String BillingState, String BillingPostcode,String IndustryCode,                                         
                                      string KeycontactFirstName,string KeycontactLastName,
                                      string KeyContactTitle,string KeyContactEmail,string KeyContactPhone,string credittype,string BusinessActivity){
    string request_message;
   
     // Instantiate a new http object
   
                                          
    
                                          
     if (ABN_ACN.length() == 11 )  
     {
        request_message = 
                    '<?xml version="1.0" encoding="utf-8"?>' +
                    '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
                    '<soap:Body>'+
                    '<ExecuteRequest xmlns="http://decisionintellect.com/inteport/">' +
                    '<_sRequestXML><![CDATA[' +
                    '<inteflow>' +
                    '<request cd_type="submit">' +
                    '<user>' +
                    '<id_oper>INTERFACE</id_oper>'+
                    //'<tx_password>8Ann9Uzbp1sfZDqjdijILZFPXABbyts1J7QVr2qQ/gE=uk2lHKeWxtosn2AYbbiSiw==</tx_password>'+
                    //'<tx_password>jcqzaczGSDZXdKZtmLNkxrJllyXKtZoGpO3Wpdet3u4=4scI5VL4jYVM1p7ohBMlvw==</tx_password>'+
                    '<tx_password>3K6ltqx/ZrwFbv9J7GqVd1JaGTQD6mba2VV8GBXE8CDUMaeFj0QXAG++n6arEvM0</tx_password>'+
                    '<fg_encryption>2</fg_encryption>'+
                    '</user>'+
                    '<cd_service>SUBMIT</cd_service>'+
                    '<id_company>VIRGINAIR</id_company>'+
                    '<id_merchant>VIRGINAIR</id_merchant>' +
                    '<cd_security>IFE</cd_security>'+
                    '<id_channel>ACCELERATE</id_channel>'+
                    '<cd_product>IFE</cd_product>'+
                    '<cd_country>61</cd_country>' +
                    '</request>' +
                    '<application_details>' +
                    '<nm_schema_translation>' + 'Integate Default Response Translation.xslt' + '</nm_schema_translation>' +
                    '<id_reference_internal>' +
                     Accountid.trim() +
                    '</id_reference_internal>' +
                    '<no_sequence>1</no_sequence>' +
                    '<id_product_credit>AUS</id_product_credit>' +
                    '<id_flow>VirginAirCommercial</id_flow>' +
                    '<id_merchant_submit>VIRGINAIR</id_merchant_submit>'+
                    '<id_operator_submit>INTERFACE</id_operator_submit>' +
                    '<id_merchant_contact>VIRGINAIR</id_merchant_contact>' +
                    '<id_operator_contact>INTERFACE</id_operator_contact>' +
                    '</application_details>'+
                    '<service>' +
                    '<application>' +
                    '<tx_environment_url>' + 'https://www.inteflow.com.au/Integate' + '</tx_environment_url>'+
                    '<finance>'+
                    '<cd_type_finance>ConFinance</cd_type_finance>' +
                    '<am_finance>' + '0' + '</am_finance>'+
                    '</finance>' +
                    '<applicant fg_new="0" cd_type="Principal">'+
                    '<company>' +
                    '<cd_type_entity>' + 'LTD' + '</cd_type_entity>' +
                    '<tx_company_abn>' + ABN_ACN.trim() + '</tx_company_abn>' +                    
                    '<nm_company_legal>' + AccountName.trim() + '</nm_company_legal>' +
                    '<nm_company_trading>' + AccountName.trim() + '</nm_company_trading>' +  
                    '<cd_type_industry>' + IndustryCode.trim() + '</cd_type_industry>' +  
                    '</company>'+
                    '<address>' +
                    '<registered_address>' +
                    '<tx_unformatted_address>' + 
                     BillingStreet.replace(',', ' ')  + ' ' + BillingCity.trim() + ' ' + BillingState.trim() + ' ' +BillingPostcode.trim() +
                    '</tx_unformatted_address>' +
                    '</registered_address>' +
                    '</address>' +
                    '<contact>' +
                    '<other_phone>' +
                    '<tx_country>' + '61' + '</tx_country>' +
                    '<tx_number>' + KeyContactPhone.trim() + '</tx_number>' +
                    '<cd_type>' + 'Mobile' + '</cd_type>' +
                    '</other_phone>' +
                    '</contact>'+
                    '<contact>' +
                    '<email>'+
                    '<cd_type>Primary</cd_type>' +
                    '<tx_address>' + KeyContactEmail.trim() + '</tx_address>'+ 
                    '</email>'+
                    '</contact>'+ 
                    '<reference>'+
                    '<nm_position>' + KeyContactTitle.trim() + '</nm_position>' +
                    '<nm_firstname>'+ KeycontactFirstName.trim() + '</nm_firstname>' +
                    '<nm_surname>'  + KeycontactLastName.trim() + '</nm_surname>' + 
                    '</reference>'  +
                    '</applicant>'  +
                    '<virgin_application>' +
                    '<cd_account_type>' +
                     'ACCELERATE' +
                    '</cd_account_type>' + 
                    '<cd_credit_type>' +
                     credittype.trim() +
                    '</cd_credit_type>' + 
                    '<tx_website>' +
                     website.trim()  +
                    '</tx_website>' +
                    '<tx_industry_business_activities>' +
                    '<tx_value>'+
                     BusinessActivity.trim()  +
                    '</tx_value>' + 
                    '</tx_industry_business_activities>' + 
                    '<fg_abr_search_required>1</fg_abr_search_required>' +
                    '</virgin_application>' +
                    '</application>'+
                    '</service>' +
                    '</inteflow>]]>' +
                    '</_sRequestXML>'+
                    '</ExecuteRequest></soap:Body></soap:Envelope>';         
     }  else if (ABN_ACN.length() == 13 ) 
     {
                    request_message = 
                   '<?xml version="1.0" encoding="utf-8"?>' +
                    '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
                    '<soap:Body>'+
                    '<ExecuteRequest xmlns="http://decisionintellect.com/inteport/">' +
                    '<_sRequestXML><![CDATA[' +
                    '<inteflow>' +
                    '<request cd_type="submit">' +
                    '<user>' +
                    '<id_oper>INTERFACE</id_oper>'+
                    //'<tx_password>8Ann9Uzbp1sfZDqjdijILZFPXABbyts1J7QVr2qQ/gE=uk2lHKeWxtosn2AYbbiSiw==</tx_password>'+
                    //'<tx_password>jcqzaczGSDZXdKZtmLNkxrJllyXKtZoGpO3Wpdet3u4=4scI5VL4jYVM1p7ohBMlvw==</tx_password>'+
                    '<tx_password>3K6ltqx/ZrwFbv9J7GqVd1JaGTQD6mba2VV8GBXE8CDUMaeFj0QXAG++n6arEvM0</tx_password>'+
                    '<fg_encryption>2</fg_encryption>'+
                    '</user>'+
                    '<cd_service>SUBMIT</cd_service>'+
                    '<id_company>VIRGINAIR</id_company>'+
                    '<id_merchant>VIRGINAIR</id_merchant>' +
                    '<cd_security>IFE</cd_security>'+
                    '<id_channel>ACCELERATE</id_channel>'+
                    '<cd_product>IFE</cd_product>'+
                    '<cd_country>64</cd_country>' +
                    '</request>' +
                    '<application_details>' +
                    '<nm_schema_translation>' + 'Integate Default Response Translation.xslt' + '</nm_schema_translation>' +
                    '<id_reference_internal>' +
                     Accountid.trim() +
                    '</id_reference_internal>' +
                    '<no_sequence>1</no_sequence>' +
                    '<id_product_credit>NZ</id_product_credit>' +
                    '<id_flow>VirginAirCommercial</id_flow>' +
                    '<id_merchant_submit>VIRGINAIR</id_merchant_submit>'+
                    '<id_operator_submit>INTERFACE</id_operator_submit>' +
                    '<id_merchant_contact>VIRGINAIR</id_merchant_contact>' +
                    '<id_operator_contact>INTERFACE</id_operator_contact>' +
                    '</application_details>'+
                    '<service>' +
                    '<application>' +
                   '<tx_environment_url>' + 'https://www.inteflow.com.au/Integate' + '</tx_environment_url>'+
                    '<finance>'+
                    '<cd_type_finance>ConFinance</cd_type_finance>' +
                    '<am_finance>' + '0' + '</am_finance>'+
                    '</finance>' +
                    '<applicant fg_new="0" cd_type="Principal">'+
                    '<company>' +
                    '<cd_type_entity>' + 'LTD' + '</cd_type_entity>' +
                    '<tx_company_number>' + ABN_ACN.trim() + '</tx_company_number>' +                    
                    '<nm_company_legal>' + AccountName.trim() + '</nm_company_legal>' +
                    '<nm_company_trading>' + AccountName.trim() + '</nm_company_trading>' +  
                    '<cd_type_industry>' + IndustryCode.trim() + '</cd_type_industry>' +  
                    '</company>'+
                    '<address>' +
                    '<registered_address>' +
                    '<tx_unformatted_address>' + 
                     BillingStreet.replace(',', ' ')  + ' ' + BillingCity.trim() + ' ' + BillingState.trim() + ' ' +BillingPostcode.trim() +
                    '</tx_unformatted_address>' +
                    '</registered_address>' +
                    '</address>' +
                    '<contact>' +
                    '<other_phone>' +
                    '<tx_country>' + '64' + '</tx_country>' +
                    '<tx_number>' + KeyContactPhone.trim() + '</tx_number>' +
                    '<cd_type>' + 'Mobile' + '</cd_type>' +
                    '</other_phone>' +
                    '</contact>'+
                    '<contact>' +
                    '<email>'+
                    '<cd_type>Primary</cd_type>' +
                    '<tx_address>' + KeyContactEmail.trim() + '</tx_address>'+ 
                    '</email>'+
                    '</contact>'+ 
                    '<reference>'+
                    '<nm_position>' + KeyContactTitle.trim() + '</nm_position>' +
                    '<nm_firstname>'+ KeycontactFirstName.trim() + '</nm_firstname>' +
                    '<nm_surname>'  + KeycontactLastName.trim() + '</nm_surname>' + 
                    '</reference>'  +
                    '</applicant>'  +
                    '<virgin_application>' +
                    '<cd_account_type>' +
                     'ACCELERATE' +
                    '</cd_account_type>' + 
                    '<cd_credit_type>' +
                     credittype.trim() +
                    '</cd_credit_type>' + 
                    '<tx_website>' +
                     website.trim()  +
                    '</tx_website>' +
                    '<tx_industry_business_activities>' +
                    '<tx_value>'+
                     BusinessActivity.trim()  +
                    '</tx_value>'+ 
                    '</tx_industry_business_activities>' +     
                    '<fg_abr_search_required>1</fg_abr_search_required>' +
                    '</virgin_application>' +
                    '</application>'+
                    '</service>' +
                    '</inteflow>]]>' +
                    '</_sRequestXML>'+
                    '</ExecuteRequest></soap:Body></soap:Envelope>';
     }
     else 
     {
         request_message = 
                    '<?xml version="1.0" encoding="utf-8"?>' +
                    '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
                    '<soap:Body>'+
                    '<ExecuteRequest xmlns="http://decisionintellect.com/inteport/">' +
                    '<_sRequestXML><![CDATA[' +
                    '<inteflow>' +
                    '<request cd_type="submit">' +
                    '<user>' +
                    '<id_oper>INTERFACE</id_oper>'+
                    //'<tx_password>8Ann9Uzbp1sfZDqjdijILZFPXABbyts1J7QVr2qQ/gE=uk2lHKeWxtosn2AYbbiSiw==</tx_password>'+
                    '<tx_password>3K6ltqx/ZrwFbv9J7GqVd1JaGTQD6mba2VV8GBXE8CDUMaeFj0QXAG++n6arEvM0</tx_password>' +   
                    '<fg_encryption>2</fg_encryption>'+
                    '</user>'+
                    '<cd_service>SUBMIT</cd_service>'+
                    '<id_company>VIRGINAIR</id_company>'+
                    '<id_merchant>VIRGINAIR</id_merchant>' +
                    '<cd_security>IFE</cd_security>'+
                    '<id_channel>ACCELERATE</id_channel>'+
                    '<cd_product>IFE</cd_product>'+
                    '<cd_country>61</cd_country>' +
                    '</request>' +
                    '<application_details>' +
                    '<nm_schema_translation>' + 'Integate Default Response Translation.xslt' + '</nm_schema_translation>' +
                    '<id_reference_internal>' +
                     Accountid.trim() +
                    '</id_reference_internal>' +
                    '<no_sequence>1</no_sequence>' +
                    '<id_product_credit>AUS</id_product_credit>' +
                    '<id_flow>VirginAirCommercial</id_flow>' +
                    '<id_merchant_submit>VIRGINAIR</id_merchant_submit>'+
                    '<id_operator_submit>INTERFACE</id_operator_submit>' +
                    '<id_merchant_contact>VIRGINAIR</id_merchant_contact>' +
                    '<id_operator_contact>INTERFACE</id_operator_contact>' +
                    '</application_details>'+
                    '<service>' +
                    '<application>' +
                    '<tx_environment_url>' + 'https://www.inteflow.com.au/Integate' + '</tx_environment_url>'+
                    '<finance>'+
                    '<cd_type_finance>ConFinance</cd_type_finance>' +
                    '<am_finance>' + '0' + '</am_finance>'+
                    '</finance>' +
                    '<applicant fg_new="0" cd_type="Principal">'+
                    '<company>' +
                    '<cd_type_entity>' + 'LTD' + '</cd_type_entity>' +
                    '<tx_company_acn>' + ABN_ACN.trim() + '</tx_company_acn>' +                    
                    '<nm_company_legal>' + AccountName.trim() + '</nm_company_legal>' +
                    '<nm_company_trading>' + AccountName.trim() + '</nm_company_trading>' +  
                    '<cd_type_industry>' + IndustryCode.trim() + '</cd_type_industry>' + 
                    '</company>'+
                    '<address>' +
                    '<registered_address>' +
                    '<tx_unformatted_address>' + 
                     BillingStreet.replace(',', ' ')  + ' ' + BillingCity.trim() + ' ' + BillingState.trim() + ' ' +BillingPostcode.trim() +
                    '</tx_unformatted_address>' +
                    '</registered_address>' +
                    '</address>' +
                    '<contact>' +
                    '<other_phone>' +
                    '<tx_country>' + '61' + '</tx_country>' +
                    '<tx_number>' + KeyContactPhone.trim() + '</tx_number>' +
                    '<cd_type>' + 'Mobile' + '</cd_type>' +
                    '</other_phone>' +
                    '</contact>'+
                    '<contact>' +
                    '<email>'+
                    '<cd_type>Primary</cd_type>' +
                    '<tx_address>' + KeyContactEmail.trim() + '</tx_address>'+ 
                    '</email>'+
                    '</contact>'+ 
                    '<reference>'+
                    '<nm_position>' + KeyContactTitle.trim() + '</nm_position>' +
                    '<nm_firstname>'+ KeycontactFirstName.trim() + '</nm_firstname>' +
                    '<nm_surname>'  + KeycontactLastName.trim() + '</nm_surname>' + 
                    '</reference>'  +
                    '</applicant>'  +
                    '<virgin_application>' +
                    '<cd_account_type>' +
                     'ACCELERATE' +
                    '</cd_account_type>' + 
                    '<cd_credit_type>' +
                     credittype.trim() +
                    '</cd_credit_type>' + 
                    '<tx_website>' +
                     website.trim()  +
                    '</tx_website>' +
                    '<tx_industry_business_activities>' +
                    '<tx_value>'+
                     BusinessActivity.trim()  +
                    '</tx_value>'+ 
                    '</tx_industry_business_activities>' +     
                    '<fg_abr_search_required>1</fg_abr_search_required>' +
                    '</virgin_application>' +
                    '</application>'+
                    '</service>' +
                    '</inteflow>]]>' +
                    '</_sRequestXML>'+
                    '</ExecuteRequest></soap:Body></soap:Envelope>';   
     }
                    
                   String resValue =''; 
                   String errValue ='';                       
                   boolean check = false;                       
                   Http http = new Http();                       
                   HttpRequest req = new HttpRequest();        
                   req.setMethod('POST');  
                 // req.setEndpoint('https://test.inteflow.com.au/Inteport/DecisionGateway.asmx');
                 // req.setHeader('Host','test.inteflow.com.au');
                   req.setEndpoint('https://www.inteflow.com.au/inteport/decisiongateway.asmx');
                   req.setHeader('Host','www.inteflow.com.au');                       
                   req.setHeader('Content-Type', 'text/xml; charset=utf-8');
                   req.setHeader('Content-Length','5000');                  
                   req.setHeader('SOAPAction', 'http://decisionintellect.com/inteport/ExecuteRequest');                   
                   req.setBody(request_message); 
                   req.setTimeout(120000) ;          
                   HTTPResponse res;
                                         
                                
             try{
                  res = http.send(req);
                  resValue = res.getBody()  ;
               
                } catch(System.CalloutException e) {
                  System.debug('Callout error: '+ e);
                  Messaging.SingleEmailMessage mailfinish = new Messaging.SingleEmailMessage();
                  mailfinish.setToAddresses(new String[] {'salesforce.admin@virginaustralia.com','smitha.nair@virginaustralia.com'});
                  mailfinish.setReplyTo('batch@VirginAustralia.com');
                  mailfinish.setSenderDisplayName('VirginAustraliaAccelerate');
                  mailfinish.setSubject('Upload to inteflow failed');
                  mailfinish.setPlainTextBody('Hi Accelerate Team,\n\nThe upload to inteflow has failed for \n\n' + AccountName + e );
                  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailfinish });  
             }
                                          
           List<Account> lstAccUpdate = new List<Account>();    
           List<Account> acclist =  [Select Id from Account where id = :AccId]; 
                                   
            if (acclist.size() > 0)
            {    
             for(Account acc: acclist)
             { 
             acc.Inteflow_Request__c  =  request_message ;
             acc.Inteflow_Response__c =  resValue; 
             lstAccUpdate.add(acc);    
             }
              
             update lstAccUpdate ;
           }
  }

}