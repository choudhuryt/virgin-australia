@isTest
public class AccelerateSpendAggregateTest
{
static testMethod void myUnitTest() {
    	
    Date d= Date.today();
    Account account = new Account();
    CommonObjectsForTest commonObjectsForTest = new CommonObjectsForTest();
    account = commonObjectsForTest.CreateAccountObject(0);  
    account.RecordTypeId ='01290000000tSR0';
    account.Sales_Matrix_Owner__c = 'Accelerate';
    account.Business_Number__c = '123123213213';
	account.OwnerId = '00590000000LbNz'; 
    insert account;   
    
    Contract contract = New Contract();
    contract.name ='PUTCONTRACTNAMEHERE';
    contract.AccountId =account.id;
    contract.Status ='Draft';
    contract.StartDate = Date.newInstance(2017,01,01);
    contract.RecordTypeId ='012900000007oTO'; 
    insert contract;
    
    contract.Status ='Activated';
    update contract;
    
       Spend__c sp3 = TestUtilityClass.createTestSpend(account.Id, contract.Id);         
    insert sp3; 
    sp3.Year__c= d.year();  
    sp3.Month__c =  2 ;  
    update sp3; 
    
    Spend__c sp = TestUtilityClass.createTestSpend(account.Id, contract.Id);         
    insert sp; 
    sp.Year__c= d.year() ;
    sp.Month__c =  2 ;  
    update sp; 
     
       
     Spend__c sp1 = TestUtilityClass.createTestSpend(account.Id, contract.Id);         
     insert sp1; 
     sp1.Year__c= d.year() ;
     sp1.Month__c = 3 ;  
     update sp1;
    
    Account smfaccount = new Account();
    CommonObjectsForTest commonObjectsForTest1 = new CommonObjectsForTest();
    smfaccount = commonObjectsForTest1.CreateAccountObject(0);  
    smfaccount.RecordTypeId ='012900000009HrP';
    smfaccount.Sales_Matrix_Owner__c = 'Accelerate';
	smfaccount.OwnerId = '00590000000LbNz'; 
    insert smfaccount;   
    
    Contract smfcontract = New Contract();
    smfcontract.name ='SMFPUTCONTRACTNAMEHERE';
    smfcontract.AccountId =account.id;
    smfcontract.Status ='Draft';
    smfcontract.StartDate = Date.newInstance(2018,01,01);
    smfcontract.RecordTypeId ='012900000009HrU'; 
    insert smfcontract;
    
    smfcontract.Status ='Activated';
    update smfcontract;
    
    Spend__c smfsp = TestUtilityClass.createTestSpend(smfaccount.Id, smfcontract.Id);         
    insert smfsp; 
    smfsp.Year__c= d.year() ; 
    smfsp.Month__c =   12 ;  
    update smfsp; 
    
    
     Spend__c smfsp1 = TestUtilityClass.createTestSpend(account.Id, contract.Id);         
     insert smfsp1; 
     smfsp1.Year__c= d.year() ; 
     smfsp1.Month__c =   1 ;  
     update smfsp1;
    
     Spend__c smfsp2 = TestUtilityClass.createTestSpend(account.Id, contract.Id);         
     insert smfsp2; 
     smfsp1.Year__c= d.year() ;
     smfsp1.Month__c =   3 ;  
     update smfsp1;
    
    Test.StartTest();

        AccelerateSpendAggregate newAccountSpend = new AccelerateSpendAggregate();
     	
     	Database.executeBatch( newAccountSpend);
	    
	    Test.StopTest();   
        System.AssertEquals(database.countquery('SELECT COUNT()'+' FROM Account where recordtypeid in ( \'01290000000tSR0AAM\' , 	\'012900000009HrPAAU\') '), 2);
    }
    
    
    
    
}