public with sharing class RedCircleDiscountController
{
    
  private ApexPages.StandardController controller {get; set;}

   public List<Proposed_Red_Tables__c> searchResults {get;set;}
   public List<Proposed_Red_Tables__c>  searchResults1 {get;set;}   
   public List <Contract_Addendum__c> contractadd {get;set;}
   public Contract_Addendum__c conlist{get;set;}
   public Boolean isTemplate {get;set;}
   public Boolean isTemplateDiscount {get;set;}
   public Boolean iseditable {get;set;}
   public Boolean isdraft {get;set;}
   public integer domFlag {get; set;} 
   public integer regFlag {get; set;}
   private Contract_Addendum__c  a;
   public string recId{get; set;}
  
 public RedCircleDiscountController(ApexPages.StandardController myController) 
    {
        a=(Contract_Addendum__c)myController.getrecord();
        domFlag=0;
        regFlag=0;
        recId = ApexPages.currentPage().getParameters().get('Id');
      //  transFlag=0;
        //Id conid = 
    }
    


public PageReference initDisc() 
{
    
    
      
      contractadd=[SELECT Contract__c,Cos_Version__c,Domestic_Guideline_Tier__c,
                                      Domestic_Requested_Tier__c, Red_Circle__c,Regional_Guideline_Tier__c ,
                                      Regional_Requested_Tier__c,Status__c,IsEditable__c ,Domestic_OD_Pairs__c,Regional_OD_Pairs__c ,
                                      Domestic_Red_Circle_Requested_Tier__c ,Regional_Red_Circle_Requested_Tier__c
                                      FROM Contract_Addendum__c 
                           where id =:ApexPages.currentPage().getParameters().get('id')];
    
      conlist = contractadd.get(0);
    
     if(conlist.IsEditable__c ==true  )
       {
        iseditable = true;
         searchResults= [SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,
                                          Id,Name,VA_Eligible_Booking_Class__c,Applicable_Routes__c,Fixed_Base_Fare__c,
                                          Domestic_Red_Circle_Requested_Tier__c
                                          FROM Proposed_Red_Tables__c
                                          where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')
                                          and  Name = 'DOM Mainline'
                                          and Saved__c = false
                                          ORDER BY Sort_Order__c];
    
   
      
      if(searchResults.size()>0)
      {
        domFlag=1;
      }
     
       searchResults1=
           [
                SELECT Contract_Addendum__c,Contract__c,Discount_Off_Published_Fare__c,Eligible_Fare_Type__c,Id,Name,
                   VA_Eligible_Booking_Class__c,Applicable_Routes__c,Fixed_Base_Fare__c 
                   FROM Proposed_Red_Tables__c
                   where Contract_Addendum__c = :ApexPages.currentPage().getParameters().get('id')
                   and  Name = 'DOM Regional'
                   and Saved__c = false
                   ORDER BY Sort_Order__c
         ];
      if(searchResults1.size()>0)
      {
        regFlag=1;
      } 
      return null;   
        
       }else{
        iseditable = false;
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'This addendum does not have red circle selected or is not in Draft status'));
           return null;   
       }
    
     
    
     
  
   }
    
    
    public PageReference qsave() {
     if(searchResults.size() > 0) 
     {
         upsert searchResults;
     }
      if(searchResults1.size() > 0) 
     { 
      upsert searchResults1; 
      }   
      RefreshODPairValue();
      PageReference thePage = new PageReference('/apex/RedCircleDiscounts?id=' + a.Id);
      thePage.setRedirect(true);
      return thePage;
    }
    
   public PageReference save() {
   
     if(!searchResults.isEmpty())
     {
         upsert searchResults;
     }
      if(searchResults1.size() > 0) 
     { 
      upsert searchResults1; 
      }   
      RefreshODPairValue();
      PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id')); 
      thePage.setRedirect(true);
      return thePage;
  }
    
     public  void  RefreshODPairValue()
     {
          List<Contract_Addendum__c> caddList = new List<Contract_Addendum__c>();
         Contract_Addendum__c ca = new Contract_Addendum__c();
         caddlist=[SELECT Contract__c,Cos_Version__c,Domestic_Guideline_Tier__c,Domestic_Requested_Tier__c,
                                      Red_Circle__c,Regional_Guideline_Tier__c ,Regional_Requested_Tier__c,Status__c,
                                      IsEditable__c ,Domestic_OD_Pairs__c,Domestic_Red_Circle_Requested_Tier__c,
                                      Regional_Red_Circle_Requested_Tier__c
                                      FROM Contract_Addendum__c 
                           where id =:ApexPages.currentPage().getParameters().get('id')];
         if(caddList.size() > 0 )
         {
             ca = caddList.get(0);
             ca.Domestic_OD_Pairs__c ='';
             ca.Regional_OD_Pairs__c ='';
             ca.Domestic_Red_Circle_Requested_Tier__c = '';
             ca.Regional_Red_Circle_Requested_Tier__c='';
             
         }
          update ca ;
     }
    
    
    
    
     public PageReference newDOMOND() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.DOMONDSelection ;
        NewPage.getParameters().put('id', recid);            
        return newPage ;
    } 
    
       public PageReference newRegOND() 
    {   
        system.debug('Inside the new section');
        PageReference newPage;
        newPage = Page.regONDSelection ;
        NewPage.getParameters().put('id', recid);            
        return newPage ;
    }
    
   public PageReference newDom() 
  {
      
   if(conlist.Domestic_Red_Circle_Requested_Tier__c==null ||  conlist.Domestic_OD_Pairs__c == null)
   {
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Guideline Tier and Domestic OD pairs must be selected before inserting discounts'));
           return null;  
   }
   
   
        List<Red_Circle_Discounts__c> templateRedCircleDOMDiscountList = new List<Red_Circle_Discounts__c>(); 

        templateRedCircleDOMDiscountList =
              [
               SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
               ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
               Partner_Eligible_Booking_Class__c 
               FROM Red_Circle_Discounts__c WHERE
               Name ='DOM Mainline'
               AND IsTemplate__c = true
               AND Tier__c = : conlist.Domestic_Red_Circle_Requested_Tier__c
               AND Cos_Version__c = :conlist.Cos_Version__c
               ORDER BY Sort_Order__c
             ];
        
        
        if(templateRedCircleDOMDiscountList.size() > 0 )
            {
               
               for(Red_Circle_Discounts__c  tempdisc : templateRedCircleDOMDiscountList) 
               {   
               Proposed_Red_Tables__c newprop = new Proposed_Red_Tables__c();     
               newprop.Contract__c = conlist.Contract__c ;
               newprop.Contract_Addendum__c = conlist.id;
               newprop.Name = tempdisc.Name;
               newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
               newprop.Fixed_Base_Fare__c = 0;     
               newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
               newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
               newprop.Applicable_Routes__c =  conlist.Domestic_OD_Pairs__c;
               newprop.Saved__c = true;
                searchResults.add(newprop);    
               }               
            }
         
                return null;
  
  }
    
    
    public PageReference newReg() 
  {
      
   if(conlist.Regional_Red_Circle_Requested_Tier__c== null ||  conlist.Regional_OD_Pairs__c == null)
   {
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Guideline Tier and Regional OD pairs must be selected before inserting discounts'));
           return null;  
   }
  
   
    
         List<Red_Circle_Discounts__c> templateRedCircleRegDiscountList = new List<Red_Circle_Discounts__c>(); 

         templateRedCircleRegDiscountList =
              [
               SELECT Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
               ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
               Partner_Eligible_Booking_Class__c 
               FROM  Red_Circle_Discounts__c WHERE
               Name ='DOM Regional'
               AND IsTemplate__c = true
               AND Tier__c = : conlist.Regional_Red_Circle_Requested_Tier__c
               AND Cos_Version__c = :conlist.Cos_Version__c
               ORDER BY Sort_Order__c
             ];
        
        
        if( templateRedCircleRegDiscountList.size() > 0 )
            {
               
               for( Red_Circle_Discounts__c  tempdisc :  templateRedCircleRegDiscountList) 
               {   
               Proposed_Red_Tables__c newprop = new Proposed_Red_Tables__c();      
               newprop.Contract__c = conlist.Contract__c ;
               newprop.Contract_Addendum__c = conlist.id;
               newprop.Name = tempdisc.Name;
               newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
               newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
               newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c ; 
               newprop.Applicable_Routes__c =  conlist.Regional_OD_Pairs__c ;
               newprop.Saved__c = true;
                searchResults1.add(newprop);    
               }               
            }
         
                return null;
    
  }
    
     public PageReference remdom()
     {
        Proposed_Red_Tables__c toDelete = searchResults[searchResults.size()-1];            
         delete toDelete;        
         PageReference thePage = new PageReference('/apex/RedCircleDiscounts?id=' + a.Id);
         thePage.setRedirect(true);
         return thePage;
    }
    
    
     public PageReference remreg()
     {
        Proposed_Red_Tables__c toDelete = searchResults1[searchResults1.size()-1];            
         delete toDelete;        
         PageReference thePage = new PageReference('/apex/RedCircleDiscounts?id=' + a.Id);
         thePage.setRedirect(true);
         return thePage;
    }
     public PageReference cancel() {
   // return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
   PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
                  //
                  thePage.setRedirect(true);
                  //
                  return thePage;
     }
    
    
      
    

}