@isTest
public class TestReservationService{
	static testMethod void TestMockServices(){
        Test.setMock(WebServiceMock.class, new ReservationServiceMockImpl());
        
        GuestCaseReservationInfo.SalesforceReservationService services = new GuestCaseReservationInfo.SalesforceReservationService();
       	GuestCaseReservationInfoModel.ReservationType response_x = new GuestCaseReservationInfoModel.ReservationType();
        response_x = services.GetReservationDetails('111', date.parse('12/12/2012'));

        List<GuestCaseReservationInfoModel.PassengerType> passengerList = response_x.Passengers.Passenger;
        System.assert(passengerList.size()>0);
        
        List<GuestCaseReservationInfoModel.FlightSegmentType> flightList = response_x.Itinerary.FlightSegments.FlightSegment;
        System.assert(flightList.size()>0);
    }
    
    static testMethod void TestMockServices2(){
        Test.setMock(WebServiceMock.class, new ReservationServiceMockImpl2());
        
        GuestCaseReservationInfoModel.FindPassengerNameType Passenger = new GuestCaseReservationInfoModel.FindPassengerNameType();
        Passenger.GivenName = 'vuholmes';
        GuestCaseReservationInfoModel.LoyaltyType Loyalty = new GuestCaseReservationInfoModel.LoyaltyType();
        Loyalty.LoyaltyMembershipID = '111';
        GuestCaseReservationInfoModel.FlightSegmentCriteriaType FlightSegment = new GuestCaseReservationInfoModel.FlightSegmentCriteriaType();
        FlightSegment.ArrivalAirport = '11';
        String PNRLocator = '111';
        
        GuestCaseReservationInfo.SalesforceReservationService services = new GuestCaseReservationInfo.SalesforceReservationService();
        GuestCaseReservationInfoModel.FindReservationDetailsRSType response_x = 
            services.FindReservationDetails(Passenger, Loyalty, FlightSegment, PNRLocator);
        System.assertEquals('1', response_x.TotalNumberOfResults);
    }
    
    static testMethod void testModels(){
        GuestCaseReservationInfoModel.PassengerFare_element pfE = new GuestCaseReservationInfoModel.PassengerFare_element();
        GuestCaseReservationInfoModel.ReservationQuoteType rqT = new GuestCaseReservationInfoModel.ReservationQuoteType();
        GuestCaseReservationInfoModel.PassengerLoyaltyMembershipListType plmLT = new GuestCaseReservationInfoModel.PassengerLoyaltyMembershipListType();
        GuestCaseReservationInfoModel.GrandTotalType gtT = new GuestCaseReservationInfoModel.GrandTotalType();
        GuestCaseReservationInfoModel.FormOfPaymentType fopT = new GuestCaseReservationInfoModel.FormOfPaymentType();
        GuestCaseReservationInfoModel.SeatingPreferenceType sprT = new GuestCaseReservationInfoModel.SeatingPreferenceType();
        GuestCaseReservationInfoModel.CommentType cT = new GuestCaseReservationInfoModel.CommentType();
        GuestCaseReservationInfoModel.OtherServiceInformationType osiT = new GuestCaseReservationInfoModel.OtherServiceInformationType();
        GuestCaseReservationInfoModel.FindReservationType frT = new GuestCaseReservationInfoModel.FindReservationType();
        GuestCaseReservationInfoModel.IDContactTelephoneType idctT = new GuestCaseReservationInfoModel.IDContactTelephoneType();
        GuestCaseReservationInfoModel.StateProvType spT = new GuestCaseReservationInfoModel.StateProvType();
        GuestCaseReservationInfoModel.SecureDocumentType sdT = new GuestCaseReservationInfoModel.SecureDocumentType();
        GuestCaseReservationInfoModel.PassengerLoyaltyMembershipType plmT = new GuestCaseReservationInfoModel.PassengerLoyaltyMembershipType();
        GuestCaseReservationInfoModel.CreditCardType ccT = new GuestCaseReservationInfoModel.CreditCardType();
        GuestCaseReservationInfoModel.PassengersType pT = new GuestCaseReservationInfoModel.PassengersType();
        GuestCaseReservationInfoModel.CommentListType clT = new GuestCaseReservationInfoModel.CommentListType();
		GuestCaseReservationInfoModel.ReservationEntity rE = new GuestCaseReservationInfoModel.ReservationEntity();
        GuestCaseReservationInfoModel.TaxBreakdownType tbT = new GuestCaseReservationInfoModel.TaxBreakdownType();
        GuestCaseReservationInfoModel.IDContactEmailType idceT = new GuestCaseReservationInfoModel.IDContactEmailType();
        GuestCaseReservationInfoModel.PassengerListType plT = new GuestCaseReservationInfoModel.PassengerListType();
        GuestCaseReservationInfoModel.MemberInformationType miT = new GuestCaseReservationInfoModel.MemberInformationType();
        GuestCaseReservationInfoModel.SecurityInformationType siT = new GuestCaseReservationInfoModel.SecurityInformationType();
        GuestCaseReservationInfoModel.SabreAgencyAccountType saaT = new GuestCaseReservationInfoModel.SabreAgencyAccountType();
        GuestCaseReservationInfoModel.ReservationType rT = new GuestCaseReservationInfoModel.ReservationType();
        GuestCaseReservationInfoModel.TicketingInfo_element tiE = new GuestCaseReservationInfoModel.TicketingInfo_element();
        GuestCaseReservationInfoModel.LoyaltyProgramType lpT = new GuestCaseReservationInfoModel.LoyaltyProgramType();
        GuestCaseReservationInfoModel.FareDifferenceBreakdownType fdbT = new GuestCaseReservationInfoModel.FareDifferenceBreakdownType();
        GuestCaseReservationInfoModel.InterestType iT = new GuestCaseReservationInfoModel.InterestType();
        GuestCaseReservationInfoModel.ReservationQuoteChangeType rqcT = new GuestCaseReservationInfoModel.ReservationQuoteChangeType();
        GuestCaseReservationInfoModel.InterestListType ilT = new GuestCaseReservationInfoModel.InterestListType();
        GuestCaseReservationInfoModel.LoyaltyMembershipKeyType lmkT = new GuestCaseReservationInfoModel.LoyaltyMembershipKeyType();
        GuestCaseReservationInfoModel.RefundCreditCardType rccT = new GuestCaseReservationInfoModel.RefundCreditCardType();
        GuestCaseReservationInfoModel.OSIType oT = new GuestCaseReservationInfoModel.OSIType();
        GuestCaseReservationInfoModel.RefundDifferenceBreakdown_element rdbE = new GuestCaseReservationInfoModel.RefundDifferenceBreakdown_element();
        GuestCaseReservationInfoModel.ConnectionIdType cidT = new GuestCaseReservationInfoModel.ConnectionIdType();
        GuestCaseReservationInfoModel.TicketCouponType tcT = new GuestCaseReservationInfoModel.TicketCouponType();
        GuestCaseReservationInfoModel.FullTierLevelType ftlT = new GuestCaseReservationInfoModel.FullTierLevelType();
        GuestCaseReservationInfoModel.DateTimeStampIDGroupType dtsidgT = new GuestCaseReservationInfoModel.DateTimeStampIDGroupType();
        GuestCaseReservationInfoModel.MessageType mT = new GuestCaseReservationInfoModel.MessageType();
        GuestCaseReservationInfoModel.TelephoneListType tlT = new GuestCaseReservationInfoModel.TelephoneListType();
        GuestCaseReservationInfoModel.FlightSegmentPriceDetail_element fspeE = new GuestCaseReservationInfoModel.FlightSegmentPriceDetail_element();
        GuestCaseReservationInfoModel.TransactionResponseHeaderType trhT = new GuestCaseReservationInfoModel.TransactionResponseHeaderType();
        GuestCaseReservationInfoModel.ETicketType etT = new GuestCaseReservationInfoModel.ETicketType();
        
        GuestCaseReservationInfoModel.EMDCouponListType emdclT = new GuestCaseReservationInfoModel.EMDCouponListType();
        GuestCaseReservationInfoModel.EquipmentTypeType eqtT = new GuestCaseReservationInfoModel.EquipmentTypeType();
        GuestCaseReservationInfoModel.InfantType infT = new GuestCaseReservationInfoModel.InfantType();
        GuestCaseReservationInfoModel.Portion_element pE = new GuestCaseReservationInfoModel.Portion_element();
        GuestCaseReservationInfoModel.FlightSegmentCriteriaType fscT = new GuestCaseReservationInfoModel.FlightSegmentCriteriaType();
        GuestCaseReservationInfoModel.StandAloneAncillaryType staaT = new GuestCaseReservationInfoModel.StandAloneAncillaryType();
        GuestCaseReservationInfoModel.FindPassengerNameType fpnT = new GuestCaseReservationInfoModel.FindPassengerNameType();
        GuestCaseReservationInfoModel.FoundFlightSegmentListType ffslT = new GuestCaseReservationInfoModel.FoundFlightSegmentListType();
        GuestCaseReservationInfoModel.PassengerType passT = new GuestCaseReservationInfoModel.PassengerType();
        GuestCaseReservationInfoModel.LoyaltyMembershipType lmT = new GuestCaseReservationInfoModel.LoyaltyMembershipType();
        GuestCaseReservationInfoModel.AddressListType alT = new GuestCaseReservationInfoModel.AddressListType();
        GuestCaseReservationInfoModel.OACType oacT = new GuestCaseReservationInfoModel.OACType();
        GuestCaseReservationInfoModel.AncillariesPriceSummaryType apsT = new GuestCaseReservationInfoModel.AncillariesPriceSummaryType();
        GuestCaseReservationInfoModel.SpecialServiceRequestType ssrT = new GuestCaseReservationInfoModel.SpecialServiceRequestType();
        GuestCaseReservationInfoModel.FeeListType flT = new GuestCaseReservationInfoModel.FeeListType();
        GuestCaseReservationInfoModel.PrinterType prT = new GuestCaseReservationInfoModel.PrinterType();
        GuestCaseReservationInfoModel.Portions_element portE = new GuestCaseReservationInfoModel.Portions_element();
        GuestCaseReservationInfoModel.WarningListType wlT = new GuestCaseReservationInfoModel.WarningListType();
        GuestCaseReservationInfoModel.BrandedFareType bfT = new GuestCaseReservationInfoModel.BrandedFareType();
        GuestCaseReservationInfoModel.WarningExtensionType weT = new GuestCaseReservationInfoModel.WarningExtensionType();
        GuestCaseReservationInfoModel.TaxesType tT = new GuestCaseReservationInfoModel.TaxesType();
        GuestCaseReservationInfoModel.CardholderNameType cnT = new GuestCaseReservationInfoModel.CardholderNameType();
        GuestCaseReservationInfoModel.CCAddressGroup_element ccagE = new GuestCaseReservationInfoModel.CCAddressGroup_element();
        GuestCaseReservationInfoModel.LocalCardType lcT = new GuestCaseReservationInfoModel.LocalCardType();
        GuestCaseReservationInfoModel.AncillaryListType anclT = new GuestCaseReservationInfoModel.AncillaryListType();
        GuestCaseReservationInfoModel.EmploymentInformationType eiT = new GuestCaseReservationInfoModel.EmploymentInformationType();
        GuestCaseReservationInfoModel.FlightSegmentPriceDetails_element fspdE = new GuestCaseReservationInfoModel.FlightSegmentPriceDetails_element();
        GuestCaseReservationInfoModel.TransactionRequestHeaderType tranhT = new GuestCaseReservationInfoModel.TransactionRequestHeaderType();
        GuestCaseReservationInfoModel.ChangePriceQuoteType cpqT = new GuestCaseReservationInfoModel.ChangePriceQuoteType();
        GuestCaseReservationInfoModel.GetReservationDetailsRQType grdrqT = new GuestCaseReservationInfoModel.GetReservationDetailsRQType();
        GuestCaseReservationInfoModel.ContactEmailType ceT = new GuestCaseReservationInfoModel.ContactEmailType();
        GuestCaseReservationInfoModel.TicketCouponListType tclT = new GuestCaseReservationInfoModel.TicketCouponListType();
        GuestCaseReservationInfoModel.SpecialServiceRequestListType ssrlT = new GuestCaseReservationInfoModel.SpecialServiceRequestListType();
        GuestCaseReservationInfoModel.FindReservationsType fresT = new GuestCaseReservationInfoModel.FindReservationsType();
        GuestCaseReservationInfoModel.ContactInformationType ciT = new GuestCaseReservationInfoModel.ContactInformationType();
        GuestCaseReservationInfoModel.ContactEmailListType celT = new GuestCaseReservationInfoModel.ContactEmailListType();
        GuestCaseReservationInfoModel.OtherAmountType oaT = new GuestCaseReservationInfoModel.OtherAmountType();
        GuestCaseReservationInfoModel.ItineraryType iteT = new GuestCaseReservationInfoModel.ItineraryType();
        GuestCaseReservationInfoModel.SurchargeType sT = new GuestCaseReservationInfoModel.SurchargeType();
        GuestCaseReservationInfoModel.StopInformationType stopiT = new GuestCaseReservationInfoModel.StopInformationType();
        GuestCaseReservationInfoModel.TransactionContextHeader tcH = new GuestCaseReservationInfoModel.TransactionContextHeader();
        GuestCaseReservationInfoModel.AmountsType at = new GuestCaseReservationInfoModel.AmountsType();
        GuestCaseReservationInfoModel.PriceQuoteType pqT = new GuestCaseReservationInfoModel.PriceQuoteType();
        GuestCaseReservationInfoModel.OperatingAirlineType oqT = new GuestCaseReservationInfoModel.OperatingAirlineType();
        GuestCaseReservationInfoModel.SessionContextType scT = new GuestCaseReservationInfoModel.SessionContextType();
        GuestCaseReservationInfoModel.ReservationQuoteRefundType rqrT = new GuestCaseReservationInfoModel.ReservationQuoteRefundType();
        GuestCaseReservationInfoModel.EMDType emdT = new GuestCaseReservationInfoModel.EMDType();
        GuestCaseReservationInfoModel.DateCriterionType drT = new GuestCaseReservationInfoModel.DateCriterionType();
        GuestCaseReservationInfoModel.BrandedFareListType bflT = new GuestCaseReservationInfoModel.BrandedFareListType();
        GuestCaseReservationInfoModel.MarketingAirlineType maT = new GuestCaseReservationInfoModel.MarketingAirlineType();
        GuestCaseReservationInfoModel.RefundFormOfPaymentType rfopT = new GuestCaseReservationInfoModel.RefundFormOfPaymentType();
        GuestCaseReservationInfoModel.LoyaltyType lT = new GuestCaseReservationInfoModel.LoyaltyType();
        GuestCaseReservationInfoModel.BaggageDataType bdT = new GuestCaseReservationInfoModel.BaggageDataType();
        GuestCaseReservationInfoModel.RefundFareSummaryType rfsT = new GuestCaseReservationInfoModel.RefundFareSummaryType();
        GuestCaseReservationInfoModel.AncillaryType ancT = new GuestCaseReservationInfoModel.AncillaryType();
        GuestCaseReservationInfoModel.SabreUserType sabT = new GuestCaseReservationInfoModel.SabreUserType();
        GuestCaseReservationInfoModel.PreferencesType preT = new GuestCaseReservationInfoModel.PreferencesType();
        GuestCaseReservationInfoModel.Extension_element eE = new GuestCaseReservationInfoModel.Extension_element();
        GuestCaseReservationInfoModel.FindReservationDetailsRQType frdT = new GuestCaseReservationInfoModel.FindReservationDetailsRQType();
        GuestCaseReservationInfoModel.ParsedTelephoneNumberType ptnT = new GuestCaseReservationInfoModel.ParsedTelephoneNumberType();
        GuestCaseReservationInfoModel.EMDCouponType emdcT = new GuestCaseReservationInfoModel.EMDCouponType();
        GuestCaseReservationInfoModel.TransactionFaultHeaderType tfhT = new GuestCaseReservationInfoModel.TransactionFaultHeaderType();
        GuestCaseReservationInfoModel.ETicketListType etlT = new GuestCaseReservationInfoModel.ETicketListType();
        GuestCaseReservationInfoModel.DutyCodeListType dclT = new GuestCaseReservationInfoModel.DutyCodeListType();
        GuestCaseReservationInfoModel.QuoteCreditCardType qccT = new GuestCaseReservationInfoModel.QuoteCreditCardType();
        GuestCaseReservationInfoModel.LoyaltyProgramListType lplT = new GuestCaseReservationInfoModel.LoyaltyProgramListType();
        GuestCaseReservationInfoModel.FareSummaryType fsT = new GuestCaseReservationInfoModel.FareSummaryType();
        GuestCaseReservationInfoModel.AncillaryPriceType apT = new GuestCaseReservationInfoModel.AncillaryPriceType();
        GuestCaseReservationInfoModel.FareBreakdownType fbT = new GuestCaseReservationInfoModel.FareBreakdownType();
        GuestCaseReservationInfoModel.RefundLocalCardType rlcT = new GuestCaseReservationInfoModel.RefundLocalCardType();
        GuestCaseReservationInfoModel.SpecialRequestDetailsType srdT = new GuestCaseReservationInfoModel.SpecialRequestDetailsType();
        GuestCaseReservationInfoModel.Segment_element sE = new GuestCaseReservationInfoModel.Segment_element();
        GuestCaseReservationInfoModel.BaggageAllowance_element baE = new GuestCaseReservationInfoModel.BaggageAllowance_element();
        GuestCaseReservationInfoModel.FlightAssociatedAncillaryType faaT = new GuestCaseReservationInfoModel.FlightAssociatedAncillaryType();
        GuestCaseReservationInfoModel.GetReservationDetailsRSType grdrsT = new GuestCaseReservationInfoModel.GetReservationDetailsRSType();
        GuestCaseReservationInfoModel.RefundFareDifferenceBreakdownType rfdbT = new GuestCaseReservationInfoModel.RefundFareDifferenceBreakdownType();
        GuestCaseReservationInfoModel.TicketingInformation_element tickiE = new GuestCaseReservationInfoModel.TicketingInformation_element();
        GuestCaseReservationInfoModel.CommunicationPreferencesType cpT = new GuestCaseReservationInfoModel.CommunicationPreferencesType();
        GuestCaseReservationInfoModel.FlightSegmentType flsegT = new GuestCaseReservationInfoModel.FlightSegmentType();
        GuestCaseReservationInfoModel.StopInformationListType stopilT = new GuestCaseReservationInfoModel.StopInformationListType();
        GuestCaseReservationInfoModel.TaxType taxT = new GuestCaseReservationInfoModel.TaxType();
        GuestCaseReservationInfoModel.TaxListType taxlT = new GuestCaseReservationInfoModel.TaxListType();
        GuestCaseReservationInfoModel.EMDListType emdlT = new GuestCaseReservationInfoModel.EMDListType();
        GuestCaseReservationInfoModel.AgencyCreditType acT = new GuestCaseReservationInfoModel.AgencyCreditType();
        GuestCaseReservationInfoModel.ContactTelephoneType ctT = new GuestCaseReservationInfoModel.ContactTelephoneType();
        GuestCaseReservationInfoModel.ValidityDates_element vdE = new GuestCaseReservationInfoModel.ValidityDates_element();
        GuestCaseReservationInfoModel.MemberRelationshipType mrT = new GuestCaseReservationInfoModel.MemberRelationshipType();
        GuestCaseReservationInfoModel.SurchargeListType slT = new GuestCaseReservationInfoModel.SurchargeListType();
        GuestCaseReservationInfoModel.RefundPriceQuoteType rpqT = new GuestCaseReservationInfoModel.RefundPriceQuoteType();
        GuestCaseReservationInfoModel.TotalWeight_element twE = new GuestCaseReservationInfoModel.TotalWeight_element();
        GuestCaseReservationInfoModel.TravelBankType trabT = new GuestCaseReservationInfoModel.TravelBankType();
        GuestCaseReservationInfoModel.FeeType fT = new GuestCaseReservationInfoModel.FeeType();
        GuestCaseReservationInfoModel.ContextExtensionType conextT = new GuestCaseReservationInfoModel.ContextExtensionType();
        GuestCaseReservationInfoModel.AmountType amountT = new GuestCaseReservationInfoModel.AmountType();
        GuestCaseReservationInfoModel.PersonType personT = new GuestCaseReservationInfoModel.PersonType();
        GuestCaseReservationInfoModel.PassengerContactInfo_element pciE = new GuestCaseReservationInfoModel.PassengerContactInfo_element();
        GuestCaseReservationInfoModel.FareType fareT = new GuestCaseReservationInfoModel.FareType();
        GuestCaseReservationInfoModel.MaxWeightPerPiece_element mwppE = new GuestCaseReservationInfoModel.MaxWeightPerPiece_element();
        GuestCaseReservationInfoModel.RefundTravelBankAccountType rtbaT = new GuestCaseReservationInfoModel.RefundTravelBankAccountType();
        GuestCaseReservationInfoModel.FindPassengerType fpT = new GuestCaseReservationInfoModel.FindPassengerType();
        GuestCaseReservationInfoModel.ProfileInformationType piT = new GuestCaseReservationInfoModel.ProfileInformationType();
        GuestCaseReservationInfoModel.Refund_element refundE = new GuestCaseReservationInfoModel.Refund_element();
        GuestCaseReservationInfoModel.CountryType counT = new GuestCaseReservationInfoModel.CountryType();
        GuestCaseReservationInfoModel.IdentityInformationType iiT = new GuestCaseReservationInfoModel.IdentityInformationType();
        GuestCaseReservationInfoModel.Endorsements_element endorE = new GuestCaseReservationInfoModel.Endorsements_element();
        GuestCaseReservationInfoModel.PassengerName_element pnE = new GuestCaseReservationInfoModel.PassengerName_element();
        GuestCaseReservationInfoModel.APISDocumentType apidT = new GuestCaseReservationInfoModel.APISDocumentType();
        GuestCaseReservationInfoModel.WarningType wT = new GuestCaseReservationInfoModel.WarningType();
        GuestCaseReservationInfoModel.FindReservationDetailsRSType frdrsT = new GuestCaseReservationInfoModel.FindReservationDetailsRSType();
        GuestCaseReservationInfoModel.FlightSegmentListType fslT = new GuestCaseReservationInfoModel.FlightSegmentListType();
        GuestCaseReservationInfoModel.DutyCodeType dcT = new GuestCaseReservationInfoModel.DutyCodeType();
        GuestCaseReservationInfoModel.AddressType addT = new GuestCaseReservationInfoModel.AddressType();
        GuestCaseReservationInfoModel.QuoteFormOfPaymentType qfopT = new GuestCaseReservationInfoModel.QuoteFormOfPaymentType();
    }
    
    static testMethod void testGuestCaseReservationInfoUti() {
        GuestCaseReservationInfoUti.AnyElementType anyElementType = new GuestCaseReservationInfoUti.AnyElementType();
        GuestCaseReservationInfoUti.FaultType faultType = new GuestCaseReservationInfoUti.FaultType();
        GuestCaseReservationInfoUti.FaultEventType faultEventType = new GuestCaseReservationInfoUti.FaultEventType();
        GuestCaseReservationInfoUti.PropertySetType propertySetType = new GuestCaseReservationInfoUti.PropertySetType();
        GuestCaseReservationInfoUti.payload_element payloadElement = new GuestCaseReservationInfoUti.payload_element();
    }
}