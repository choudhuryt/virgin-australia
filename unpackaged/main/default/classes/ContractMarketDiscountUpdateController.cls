/**
 * @description       : ContractMarketDiscountUpdateController Apex
 * UpdatedBy: cloudwerx (Did indentation)
**/
public with sharing class ContractMarketDiscountUpdateController {
    
  String inputValue; 
  public String CurrentContractId{set;get;}   
  public List<Market__c> vamarketdata {get;set;}
  public Market__c addmarket {get;set;}   
  public List <Contract> contracts {get;set;}
  public Boolean add {get;set;} 
  public Boolean conavailable {get;set;} 
  public Contract originalContract{get;set;}
  
  public String paramValue {
      get;
      // *** setter is NOT being called ***
      set {
          paramValue = value;
      }
  }    
  
  public ContractMarketDiscountUpdateController() {
      if(CurrentContractId == null){
          CurrentContractId= ApexPages.currentPage().getParameters().get('id');
      }
  }
  
  public PageReference initDisc() {  
      if(CurrentContractId == null) {
          CurrentContractId= ApexPages.currentPage().getParameters().get('id');
      }    
      
      contracts=[SELECT Id,Cos_Version__c,Status FROM Contract WHERE id =:ApexPages.currentPage().getParameters().get('id') AND status = 'Draft'] ;
      add = false;  
      conavailable = false;  
      if(contracts.size() > 0 ) {
          originalContract = contracts.get(0);
          addmarket  = new Market__c();  
          conavailable = true;  
          vamarketdata = [SELECT Carrier__c,Comments__c,Contract__c,Guideline_Tier__c,
              Id,Name,Requested_Tier__c,Revenue_M_S__c,VA_Revenue_Commitment__c,
              VA_Serviceable_Routes_p_a__c,Market__c
              FROM Market__c WHERE Contract__c =:ApexPages.currentPage().getParameters().get('id')
              AND Carrier__c = 'VA' AND Name not in ('China' , 'HK Hong Kong')];
          return null;
      } else {
          ApexPages.addMessage( new ApexPages.message( ApexPages.severity.ERROR,
                                                      'Only Draft contracts can be updated' ));
          return null;   
      }
  }     
  
  public PageReference AddMarket() {
      add  = true ;
      addmarket.Contract__c = ApexPages.currentPage().getParameters().get('id');
      return null;
  } 
  
  public PageReference Save() {   
      List<Market__c> CheckMarketList = new List<Market__c>();  
      CheckMarketList =[SELECT Id,Name,Market__c FROM Market__c 
                        WHERE Market__c =:addmarket.Market_Name__c
                        AND Contract__c =:ApexPages.currentPage().getParameters().get('id') AND Carrier__c = 'VA'];
      if(CheckMarketList.size() > 0) {
          ApexPages.addMessage( new ApexPages.message( ApexPages.severity.ERROR,
                                                      'This Market is already available on the contract.' ));
          return null;
      } else {
          switch on addmarket.Market_Name__c {
              when 'DOM(Mainline)' {
                  addmarket.Name = 'DOM Mainline';
              } when 'DOM(Regional)' {
                  addmarket.Name = 'DOM Regional';
              } when 'Hong Kong' {
                  addmarket.Name = 'HK Hong Kong';
              } when 'China' {
                  addmarket.Name = 'China';
              } when 'Japan' {
                  addmarket.Name = 'Japan';
              } when 'Trans Tasman (TT)' {
                  addmarket.Name = 'Trans Tasman TT';
              } when 'INT. Short-Haul (excl. TT)' {
                  addmarket.Name = 'INT Short Haul';
              } when 'London' {
                  addmarket.Name = 'London';
              } when else {
                  System.debug('none of the above');
              }
          }
          addmarket.Carrier__c = 'VA';
          addmarket.Requested_Tier__c = addmarket.Tier__c  ;
          
          if(addmarket.Revenue_M_S__c > 0 ) {   
              addmarket.VA_Revenue_Commitment__c = 	addmarket.VA_Serviceable_Routes_p_a__c * addmarket.Revenue_M_S__c /100 ;
          }
          if(addmarket.Requested_Tier__c <> NULL  && addmarket.Name <> NULL) {
              List<Proposal_Table__c> templateDomesticDiscountList = new List<Proposal_Table__c>();  
              templateDomesticDiscountList = [SELECT Applicable_Routes__c,Name,DISCOUNT_OFF_PUBLISHED_FARE__c,
                  ELIGIBLE_FARE_TYPE__c,Id,VA_ELIGIBLE_BOOKING_CLASS__c,
                  Partner_Eligible_Booking_Class__c 
                  FROM Proposal_Table__c 
                  WHERE Name =:addmarket.Name AND IsTemplate__c = true AND Tier__c = : addmarket.Requested_Tier__c
                  AND Cos_Version__c = :originalContract.Cos_Version__c ORDER BY CreatedDate ];    
              if(templateDomesticDiscountList.size() > 0 ) {
                  List<Proposal_Table__c> discList = new List<Proposal_Table__c >();
                  for(Proposal_Table__c  tempdisc : templateDomesticDiscountList) {
                      Proposal_Table__c newprop = new Proposal_Table__c();    
                      newprop.Contract__c = originalContract.Id ;
                      newprop.Name = tempdisc.Name;
                      newprop.DISCOUNT_OFF_PUBLISHED_FARE__c = tempdisc.DISCOUNT_OFF_PUBLISHED_FARE__c;
                      newprop.ELIGIBLE_FARE_TYPE__c = tempdisc.ELIGIBLE_FARE_TYPE__c  ;
                      newprop.VA_ELIGIBLE_BOOKING_CLASS__c  = tempdisc.VA_ELIGIBLE_BOOKING_CLASS__c  ;
                      newprop.Partner_Eligible_Booking_Class__c   = tempdisc.Partner_Eligible_Booking_Class__c  ;
                      discList.add(newprop);
                  }
                  
                  INSERT addmarket; 
                  INSERT  discList;     
              } else {
                  ApexPages.addMessage( new ApexPages.message( ApexPages.severity.ERROR,
                                                              'There are no template disccounts available for this market' ));
                  return null; 
              }
              
          }    
          
          add = false ;
          PageReference thePage = new PageReference('/apex/ContractMarketDiscountUpdate?id=' + CurrentContractId);
          thePage.setRedirect(true);
          return thePage;
      }        
  } 
  
  public PageReference Cancel() { 
      add = false ;
      PageReference thePage = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
      thePage.setRedirect(true);
      return thePage; 
  } 
  
  public PageReference DeleteId() {
      Market__c[] delmarket = [SELECT Id, Name FROM Market__c  WHERE id = :this.paramValue]; 
      Proposal_Table__c[]  deldiscount = [SELECT Id, Name FROM  Proposal_Table__c  
                                          WHERE  Name= :delmarket[0].Name AND Contract__c = :originalContract.Id] ; 
      try {
          DELETE delmarket;
          DELETE deldiscount ; 
      } catch (DmlException e) {
          // Process exception here
      }
      PageReference thePage = new PageReference('/apex/ContractMarketDiscountUpdate?id=' + CurrentContractId);
      thePage.setRedirect(true);
      return thePage;
  }    
}