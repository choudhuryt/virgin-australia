@isTest
public class TestPrismContractDomDataController 
{
    
   static testMethod void myUnitamount()
    {
        // TO DO: implement unit test
        
        
        //setup account
         Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        insert account;
 
        Contract contractrec= new Contract();        
        contractrec = commx.CreateOldStandardContract(0);
        contractrec.AccountId = account.id;
        contractrec.RecordTypeid = '012900000007LFZAA2';
        contractrec.Cos_Version__c = '14.4';
        insert contractrec ;
            
        Contract_Performance__c cp = TestUtilityClass.createTestContractPerformanceAmount(contractrec.id);          
        insert cp;      
        
      
        
        Test.startTest();
        
        PageReference  ref1 = Page.PrismContractWithDomData ;        
        ref1.getParameters().put('cid', contractrec.id );  
        ref1.getParameters().put('cpid',cp.id);   
        Test.setCurrentPage(ref1);         
        PrismContractDomDataController lController = new PrismContractDomDataController() ;      
        lController.initDisc(); 
        lController.getData() ;
        lController.cancel() ;
        lController.cancel1() ;
        Test.StopTest();
    }

    
     static testMethod void myUnitShare()
    {
        // TO DO: implement unit test
        
        
        //setup account
         Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        insert account;
 
        Contract contractrec= new Contract();        
        contractrec = commx.CreateOldStandardContract(0);
        contractrec.AccountId = account.id;
        contractrec.RecordTypeid = '012900000007LFZAA2';
        contractrec.Cos_Version__c = '14.4';
        insert contractrec ;
            
        Contract_Performance__c cp = TestUtilityClass.createTestContractPerformanceShare(contractrec.id);          
        insert cp;      
        
      
        
        Test.startTest();
        
        PageReference  ref1 = Page.PrismContractWithDomData ;        
        ref1.getParameters().put('cid', contractrec.id );  
        ref1.getParameters().put('cpid',cp.id);   
        Test.setCurrentPage(ref1);         
        PrismContractDomDataController lController = new PrismContractDomDataController() ;      
        lController.initDisc(); 
        lController.getData() ;
        lController.cancel() ;
        lController.cancel1() ;
        Test.StopTest();
    }

         static testMethod void myUnitNoReq()
    {
        // TO DO: implement unit test
        
        
        //setup account
         Account account = new Account();
        CommonObjectsForTest commx = new CommonObjectsForTest();
        account = commx.CreateAccountObject(0);
        insert account;
 
        Contract contractrec= new Contract();        
        contractrec = commx.CreateOldStandardContract(0);
        contractrec.AccountId = account.id;
        contractrec.RecordTypeid = '012900000007LFZAA2';
        contractrec.Cos_Version__c = '14.4';
        insert contractrec ;
            
        Contract_Performance__c cp = TestUtilityClass.createTestContractPerformanceNoReq(contractrec.id);          
        insert cp;      
        
      
        
        Test.startTest();
        
        PageReference  ref1 = Page.PrismContractWithDomData ;        
        ref1.getParameters().put('cid', contractrec.id );  
        ref1.getParameters().put('cpid',cp.id);   
        Test.setCurrentPage(ref1);         
        PrismContractDomDataController lController = new PrismContractDomDataController() ;      
        lController.initDisc(); 
        lController.getData() ;
        lController.cancel() ;
        lController.cancel1() ;
        Test.StopTest();
    }
    
}