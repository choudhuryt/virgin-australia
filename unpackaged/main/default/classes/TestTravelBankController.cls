/*
* Test class for TravelBankController
*/
@isTest
private class TestTravelBankController {
    
    static testMethod void testUpdateTravelBankAccountSuccessful() {
        // create mock service

        //Test.setMock(WebServiceMock.class, new HistoricalAndTravelBankDispatcherMock()); <-- deprecated, should be removed
        Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());
        
        // prepare sample data
        Case tmpCase = new Case(Velocity_Number__c='V1');
        insert tmpCase;
        
        Velocity__c tmpVelo = new Velocity__c(Velocity_Number__c = 'V1', Travel_Bank_Number__c = '1234567', Case__c = tmpCase.Id);
        insert tmpVelo;
        
        Customer_Compensation__c tmpCusComp = new Customer_Compensation__c(Status__c = 'Submitted', Parent_Case__c = tmpCase.Id, Department_Revenue_Code__c = 'CO - Compensation');
        insert tmpCusComp;
        String cusCompId = tmpCusComp.Id;
        
        Compensation_Line__c tmpCompLine1 = new Compensation_Line__c(Parent_Customer_Compensation__c = tmpCusComp.Id, Category__c = 'Travel Bank', Type__c = 'Credit', Currency_Ea__c = 'AUD', Status__c = 'Unpaid', Credit_Code__c='VIMC');
        insert tmpCompLine1;
        
        Compensation_Line__c tmpCompLine2 = new Compensation_Line__c(Parent_Customer_Compensation__c = tmpCusComp.Id, Category__c = 'Travel Bank', Type__c = 'Credit', Currency_Ea__c = 'USD', Status__c = 'Unpaid', Credit_Code__c='VACO');
        insert tmpCompLine2;
        
        Compensation_Line__c tmpCompLine3 = new Compensation_Line__c(Parent_Customer_Compensation__c = tmpCusComp.Id, Category__c = 'Refund', Type__c = 'Credit', Currency_Ea__c = 'AUD', Status__c = 'Unpaid', Credit_Code__c='VMCO');
        insert tmpCompLine3;
        
        Compensation_Line__c tmpCompLine4 = new Compensation_Line__c(Parent_Customer_Compensation__c = tmpCusComp.Id, Category__c = 'Travel Bank', Type__c = 'Credit', Currency_Ea__c = 'AUD', Status__c = 'Paid', Credit_Code__c='VMCO');
        insert tmpCompLine4;
        
        tmpCusComp.Status__c = 'Approved';
        update tmpCusComp;
        
        // create page + controller + call update function
        Test.startTest();
        
        PageReference travelBankPage = new PageReference('/apex/TravelBank?compId='+tmpCusComp.Id+'&lineIds='+tmpCompLine1.Id+','+tmpCompLine2.Id+','+tmpCompLine3.Id+','+tmpCompLine4.Id);
        Test.setCurrentPage(travelBankPage);
        
        TravelBankController travelBankExtCon = new TravelBankController();
        travelBankExtCon.updateTravelBankAccount();
        Test.stopTest();

        
        // verify data. Only Compensation_Line__c type Travel Bank, status Unpaid is updated
        Compensation_Line__c comtCompLine =  [select Id, Status__c, Notes__c from Compensation_Line__c
                                              where Id = :tmpCompLine1.Id limit 1];
        System.assert(comtCompLine.Status__c == 'Paid');
        System.assert(!String.isEmpty(comtCompLine.Notes__c));
        
        comtCompLine =  [select Id, Status__c, Notes__c from Compensation_Line__c
                         where Id = :tmpCompLine2.Id limit 1];
        System.assert(comtCompLine.Status__c == 'Paid');
        System.assert(!String.isEmpty(comtCompLine.Notes__c));
        
        comtCompLine =  [select Id, Status__c, Notes__c from Compensation_Line__c
                         where Id = :tmpCompLine3.Id limit 1];
        System.assert(comtCompLine.Status__c == 'Unpaid');
        System.assert(String.isEmpty(comtCompLine.Notes__c));
    }
    
    static testMethod void testUpdateTravelBankAccountFail() {
        Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());
        
        // prepare sample data
        Case tmpCase = new Case(Velocity_Number__c='V1');
        insert tmpCase;
        
        Velocity__c tmpVelo = new Velocity__c(Velocity_Number__c = 'V1', Case__c = tmpCase.Id);
        insert tmpVelo;
        
        Customer_Compensation__c tmpCusComp = new Customer_Compensation__c(Status__c = 'Submitted', Parent_Case__c = tmpCase.Id, Department_Revenue_Code__c = 'CO - Compensation');
        insert tmpCusComp;
        String cusCompId = tmpCusComp.Id;
        
        // create page + controller + call update function
        Test.startTest();
        
        PageReference travelBankPage = new PageReference('/apex/TravelBank?compId='+tmpCusComp.Id+'&lineIds=');
        Test.setCurrentPage(travelBankPage);
        
        TravelBankController travelBankExtCon = new TravelBankController();
        travelBankExtCon.updateTravelBankAccount();
        Test.stopTest();
        
    }
    
    static testMethod void testUpdateTravelBankAccountFail2() {
        Test.setMock(WebServiceMock.class, new VelocityAndTravelBankDispatcherMock());
        
        // prepare sample data
        Case tmpCase = new Case(Velocity_Number__c='V1');
        insert tmpCase;
        
        Velocity__c tmpVelo = new Velocity__c(Velocity_Number__c = 'V1', Case__c = tmpCase.Id);
        insert tmpVelo;
        
        Customer_Compensation__c tmpCusComp = new Customer_Compensation__c(Status__c = 'Approved', Parent_Case__c = tmpCase.Id, Department_Revenue_Code__c = 'CO - Compensation');
        insert tmpCusComp;
        String cusCompId = tmpCusComp.Id;
        
        // create page + controller + call update function
        Test.startTest();
        
        PageReference travelBankPage = new PageReference('/apex/TravelBank?compId='+tmpCusComp.Id+'&lineIds=');
        Test.setCurrentPage(travelBankPage);
        
        TravelBankController travelBankExtCon = new TravelBankController();
        travelBankExtCon.updateTravelBankAccount();
        Test.stopTest();
        
    }
    
    static testMethod void testModels() {
        TravelBank.HeaderRSType hrs = new TravelBank.HeaderRSType();
        TravelBank.HeaderRQType hrq = new TravelBank.HeaderRQType();
        TravelBankUtil.AnyElementType aeT = new TravelBankUtil.AnyElementType();
        TravelBankUtil.FaultType fT = new TravelBankUtil.FaultType();
        TravelBankUtil.FaultEventType feT = new TravelBankUtil.FaultEventType();
        TravelBankUtil.PropertySetType psT = new TravelBankUtil.PropertySetType();
        TravelBankUtil.payload_element pE = new TravelBankUtil.payload_element();
    }
}