trigger UpdateEnhancementNumber on Case (before insert) 
{
    
  RecordType rc = [Select id, Name From RecordType 
										where SobjectType = 'Case'
										and IsActive = true
										and DeveloperName = 'Salesforce_Enhancement_Request' LIMIT 1];  
    
  list<Case> c= [SELECT Id,Enhancement_ID__c FROM Case where Enhancement_ID__c!=:null 
                 and RecordTypeId = :rc.Id order by Enhancement_ID__c desc limit 1];
    
  if (c.size() > 0 )  
  {    
  decimal maxlead = c[0].Enhancement_ID__c;  
    
  for (Case cs:Trigger.new)
  {
     if (cs.RecordTypeId == rc.Id )
     {
      cs.Enhancement_ID__c = Integer.valueOf(maxlead)+1;    
     }
    
   }
  }
}