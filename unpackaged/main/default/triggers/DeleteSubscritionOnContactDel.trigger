/** * File Name      : DeleteSubscritionOnContactDel
* Description        : This Apex trigger will delete a subscription everytime 
*                      a contact is deleted
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Edward Stachyra
* * Date             : 12 June 2012
* * Technical Task ID: 
* * Notes            : 
* Modification Log ==================================================​============= 
Ver  Date     Author        Modification --- ---- ------ -------------
1.01 13/06/12 Andy Burgess  Modified delete so that the subscription list looked at the 
                            map to do a bulkified update and not hit govenour limits * */ 


trigger DeleteSubscritionOnContactDel on Contact (before delete) {

//trigger CustomObjectDelete on Contact (before delete) {
   Set<Id> subscriptionIds = new Set<Id>();
   
   for (Contact c : Trigger.old) 
   {
      subscriptionIds.add(c.Subscriptions__c);
      
   }





List <Subscriptions__c> iterList = new list<Subscriptions__c>([select  Id, Name from Subscriptions__c where id in: subscriptionIds]);
//for (Subscriptions__c iter : iterList){
        
         delete iterList;
     // }
}