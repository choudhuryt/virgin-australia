// Tquila ANZ - Trigger on Case to update the Account life Cycle to opprotunity when a reply has been received by the client
// Ref VACR01
Trigger updateAccountLC on Case (after update) {
    list<Account> accList = new List<Account>();
    for (Case c : Trigger.new) {
        Account acc = new Account();
        
        if(c.status == 'Working' && c.ACC_Secondary_Category__c == 'Received Further Info' && c.subject.contains('FOLLOWUP#[') == true){ 
            acc.id = c.AccountId;
            acc.Account_Lifecycle__c = 'Opportunity';
            accList.add(acc);
        }
        
    }
         
        system.debug('***account size: ' + accList.size());
        if(accList.size() > 0){
            update accList;
        }
}