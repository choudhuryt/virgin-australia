trigger SetCaseParentrecordOwner on Case (before insert, before update) {
    
    Set<Id> accIDs  = new Set<Id>();
    Set<Id> parentaccIDs  = new Set<Id>();
    List<Case> affectedCase = new List<Case>();
    for(Case c: Trigger.New)
    {
        if (c.AccountId != NULL &&  c.RecordTypeId == '0126F0000012HR6')
            
        {
            accIDs.add(c.AccountId);
            parentaccIDs.add(c.NEW_Parent_Account1__c);
            affectedCase.add(c);
            
        }   
    }
 

 List <Account> acclist = [SELECT Id, Ultimate_Parent__c, ParentId  FROM Account WHERE Id = :accIDs];
 if   (acclist.size() > 0 )
 {
 String ultimateparent =    acclist[0].Ultimate_Parent__c;
 string parentaccount =     acclist[0].ParentId ;
     
 List  <Account>  newparentlist  = [SELECT Id, OwnerId FROM Account WHERE Id   = :parentaccIDs]; 
 List <Account> currentparentlist    = [SELECT Id, OwnerId FROM Account WHERE Id = :ultimateparent.substring(10,25)];
     
 List<Case> CaseToUpdate = new List<Case>();
     
 system.debug('The parent id '  + ultimateparent + currentparentlist[0].OwnerId );   
    
 for(Case cs : affectedCase)
    {   
     if (currentparentlist.size()   > 0 &&  parentaccount != NULL  )
     
    {
     cs.Parent_Account_Owner__c = currentparentlist[0].OwnerId;
     
    }else  if (parentaccount  == null  &&  newparentlist.size() > 0  ) 
    {
     cs.Parent_Account_Owner__c = newparentlist[0].OwnerId;
     
    }     
         
         
     caseToUpdate.add(cs);    
    }   
 }
}