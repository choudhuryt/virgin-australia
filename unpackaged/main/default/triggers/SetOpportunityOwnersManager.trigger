trigger SetOpportunityOwnersManager on Opportunity (before insert, before update) {

/*
A trigger to set the Opportunity owner's manager when the Opportunity owner changes
*/

	Set<Id> ownerIDs  = new Set<Id>();
    Set<Id> level3IDs = new Set<Id>();
	List<Opportunity> affectedOpportunity = new List<Opportunity>();
	Boolean ownerChanged=false;
	
	// Step 1: Only retain the new Opportunity and the existing Opportunity with a new Owner (change owner) 
	// or if the Close date is updated
    for(Opportunity l: Trigger.New)
    {
			affectedOpportunity.add(l);
			ownerIDs.add(l.OwnerId);		
		
    }
    
    // Step 2: Build a map of User ID-Contract Approver ID of affected Opportunity
    
    List <User> u = [SELECT Id, contract_approver__r.Id, contract_approver__r.Level__c, contract_approver__r.ManagerId
                  FROM User WHERE Id In :ownerIDs];
    
    // Stp 3: Now, assign the new Opportunity Owner for the affected Opportunity
    // by using the Contract Approver of the new Opportunity Owner
    List<Opportunity> OpportunityToUpdate = new List<Opportunity>();
    for(Opportunity l : affectedOpportunity)
    {
        if (u.size() > 0)
        {    
    	l.Opportunity_Owner_Manager__c = u[0].contract_approver__r.Id;
        if  ( u[0].contract_approver__r.Level__c  > 2 )
        {
         l.Level3_Manager__c    = u[0].contract_approver__r.Id; 
        }else
        {
         l.Level3_Manager__c    = u[0].contract_approver__r.ManagerId;    
        }
        
        }    
    	OpportunityToUpdate.add(l); 
        
    }
    
    //update opportunityToUpdate;
    
}