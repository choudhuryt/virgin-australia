trigger CreateAccelerateAccountFromLead on Lead (after insert) {
    system.debug('inside CreateAccelerateAccountFromLead');
    for (Lead lead : Trigger.new) {
        if ('Web'.equals(lead.LeadSource) && lead.Agreed_To_Terms_And_Conditions__c && lead.Business_Number__c != null) {
            /*if (lead.Account_Manager_Email_Address__c != null) {
                Messaging.SingleEmailMessage mailTMC = new Messaging.SingleEmailMessage();
                mailTMC.setToAddresses(new String[]{
                        lead.Account_Manager_Email_Address__c
                });
                mailTMC.setBccAddresses(new String[]{
                        'salesforce.admin@virginaustralia.com'
                });
                mailTMC.setReplyTo('VirginAustralia.accelerate@virginaustralia.com');
                mailTMC.setSenderDisplayName('Virgin Australia Accelerate Team');

                mailTMC.setSubject('Request for Virgin Australia Main Agency login code');
                mailTMC.setHtmlBody('<html><body>Dear ' + lead.Account_Manager_First_Name__c + '<br/><br/>Your client ' + lead.FirstName + ' ' + lead.LastName + ' from ' + lead.Company + ', ' + ' has just submitted an application to join the Virgin Australia accelerate program.<br/><br/>In order for us to process this application, could you please provide Virgin Australia your IATA / TIDS number, and also any PCCs (Pseudo City Code).<br/><br/>If you are a non IATA travel agency (i.e You do not self ticket), please provide your Virgin Australia main agency code.<br/><br/>To book and ticket Virgin Australia flights, all Travel Agents and Travel Management Companies are required to register with Virgin Australia. To register a travel agent account, please review the travel agent information <a href=\'https://www.virginaustralia.com/au/en/bookings/agents-corporate-bookings/agents-registration/information-for-all-travel-agents/australian-travel-agent-account/\'>here</a> and complete the travel agent registration form.<br/><br/>If you have any further questions, please do not hesitate to contact us.<br/><br/>Kind Regards,<br/><br/>Virgin Australia accelerate<br/>T 1300 246 498<br/>E accelerate@virginaustralia.com<br/><br/>Virgin Australia Airlines Pty Ltd (Virgin Australia) | Velocity | Blue Holidays<br/><br/>Please consider the environment before printing this email.</body></html>');

                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                        mailTMC
                });

            }*/

            // Needs to be optimised
            if ((label.Allow_Lead_Conversion).equalsignorecase('true'))
            {
                System.enqueueJob(new AccelerateWebToLead(lead.Id));
            }    
            
        }
    }
}