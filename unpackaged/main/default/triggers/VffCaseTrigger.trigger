/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 22.10.2021
**Description: Trigger to update the handling time for the Cases when it's 'Closed'
*/
trigger VffCaseTrigger on Case (after insert, before update, after update) {
    
    if(Trigger.isAfter && Trigger.isInsert){
        VffCaseTriggerHandler.TargetDateAfterInsertCase(Trigger.New);
    }
    
    if(Trigger.isBefore && Trigger.isUpdate){
        VffCaseTriggerHandler.BeforeUpdateCase(Trigger.New, Trigger.oldMap);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate){
        VffCaseTriggerHandler.AfterUpdateCase(Trigger.New, Trigger.oldMap);
    }
}