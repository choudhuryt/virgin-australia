/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 22.03.2022
**Description: Trigger to call 'SendCommsToAlmsWS' Class if Velocity number is tagged to a new Case or is updated in an existing Case
* updatedBy : cloudwerx
*/
trigger SendCasetoAlmsTrigger on Case (after insert, after update) {
    
    List<Id> idList = new List<Id>();
    List<Id> relatedMailList = new List<Id>();
    //@updateBy : cloudwerx here added check to allow TestSendCasetoAlmsTrigger to execute insert and update conditions
    if(Test.isRunningTest() || System.Label.POST_ALMS_Integration == 'Active'){
        if(Trigger.isInsert){
            for(Case cs: Trigger.New){
                System.debug('Velocity Number Insert--'+cs.Velocity_Number__c);
                if(cs.Velocity_Number__c != NULL){
                    idList.add(cs.Id);
                    System.debug('Case Insert Id--'+idList);
                }
            }
            //SendToAlmsWS.sendCase(caseList, Trigger.newMap);
        } else if(Trigger.isUpdate) {
            for(Case cs: Trigger.New){
                System.debug('Velocity Number Update --'+Trigger.OldMap.get(cs.Id).Velocity_Number__c+' -- '+ cs.Velocity_Number__c);
                if(Trigger.OldMap.get(cs.Id).Velocity_Number__c != cs.Velocity_Number__c && cs.Velocity_Number__c != NULL){
                    idList.add(cs.Id);
                    System.debug('Case Update Id'+idList);
                }
            }
        }       
    } 
    
    if(idList.size() > 0) {
        List<EmailMessage> mailList = [SELECT Id FROM EmailMessage WHERE RelatedToId IN :idList];
        for(EmailMessage em: mailList){
            idList.add(em.Id);
        } 
        SendCommsToAlmsWS.sendtoAlms(idList, NULL);
    }
}