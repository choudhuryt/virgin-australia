/**
 * @description       : ContractTrigger
 * @CreatedBy         : CloudeWerx
 * @UpdatedBy         : CloudeWerx
**/
trigger ContractTrigger on Contract (before insert, after insert, before update, after update) {
    if(!ContractTriggerHandler.preventCalling) {
        if(Trigger.isBefore) {
            ContractTriggerHandler.contractBeforeActivity(Trigger.new, Trigger.oldMap, Trigger.isInsert);
        } else if (Trigger.isAfter) {
            if(Trigger.isUpdate) {
                ContractTriggerHandler.contractAfterUpdateActivity(Trigger.New, Trigger.oldMap);
            } else if(Trigger.isInsert) {
                ContractTriggerHandler.contractAfterInsertActivity(Trigger.New);
            }
        }
    }
}