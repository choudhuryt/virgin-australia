trigger UpdateContactTrigger on Contact (after insert, after update)
{
    
for(Contact cnt:Trigger.new)
{
  if(System.IsBatch() == false )
  {  
   SYSTEM.DEBUG(recursiveTriggerHandler.isGoodForUpdate + cnt.Velocity_Number__c + cnt.id)  ;
   if(recursiveTriggerHandler.isGoodForUpdate && cnt.Velocity_Number__c <> NULL )
   { 
    MakeVelocityCallout.apexcallout(cnt.id,cnt.Velocity_Number__c);
    recursiveTriggerHandler.isGoodForUpdate = FALSE;
   }  
  }     
}
}