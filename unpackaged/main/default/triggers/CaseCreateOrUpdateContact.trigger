/*
**Updated By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Updated Date: 18.11.2021
**Description: Updated to include Case Record Type 'Velocity Member Support'
*/
trigger CaseCreateOrUpdateContact on Case (before insert) 
{
    Id caseVelocityMemberSupportId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Velocity Member Support').getRecordTypeId();
    //first get the record type id
    List<RecordType> recordTypeGRCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
                                         r.Description, r.BusinessProcessId From RecordType r
                                         where SobjectType = 'Case'
                                         and IsActive = true
                                         and DeveloperName = 'GR_Case' LIMIT 1];
    
    List<RecordType> recordTypeVelCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
                                          r.Description, r.BusinessProcessId From RecordType r
                                          where SobjectType = 'Case'
                                          and IsActive = true
                                          and DeveloperName = 'Velocity_Case_Record_Type' LIMIT 1];	
    
    List<RecordType> recordTypePRVCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
                                          r.Description, r.BusinessProcessId From RecordType r
                                          where SobjectType = 'Case'
                                          and IsActive = true
                                          and DeveloperName = 'Privacy_Case_Record_Type' LIMIT 1];	
    
    //22.11.2021_Tapashree: Added for Case Record Type 'Velocity Member Support'
    List<RecordType> recordTypeVFFCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
                                          r.Description, r.BusinessProcessId From RecordType r
                                          where SobjectType = 'Case'
                                          and IsActive = true
                                          and DeveloperName = 'Velocity_Member_Support' LIMIT 1];
    
    List<RecordType> recordTypeGRContact = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
                                            r.Description, r.BusinessProcessId From RecordType r
                                            where SobjectType = 'Contact'
                                            and IsActive = true
                                            and DeveloperName = 'GR_Contact' LIMIT 1];
    
    //stop if no GR Record types for contacts or case found
    //22.11.2021_Tapashree: Added for Case Record Type 'Velocity Member Support'
    if(recordTypeGRCase.size() <= 0 || recordTypeVelCase.size() <= 0 || recordTypePRVCase.size() <= 0 || recordTypeGRContact.size() <= 0 || recordTypeVFFCase.size() <=0)
        return;
    
    List<String> emailAddresses = new List<String>();   
    
    //First exclude any cases where the contact is set  
    for (Case caseObj:Trigger.new) 
    {     	 
        //web cases
        //22.11.2021_Tapashree: Added for Case Record Type 'Velocity Member Support'
        if( caseObj.ContactId==null && 
           caseObj.SuppliedEmail != null && 
           caseObj.SuppliedEmail.trim()!='' &&
           ( caseObj.recordTypeId ==  recordTypeGRCase[0].Id || caseObj.recordTypeId == recordTypeVelCase[0].id || caseObj.recordTypeId == recordTypePRVCase[0].id || caseObj.recordTypeId == recordTypeVFFCase[0].id)
           //DONE - Cases created off webform are not populating recordtypeid now. Need to add this back in when form is sorted.
          )  
        {  
            emailAddresses.add(caseObj.SuppliedEmail.trim());  
        }  
        
        //manually created cases
        //22.11.2021_Tapashree: Added for Case Record Type 'Velocity Member Support'
        else if( caseObj.ContactId==null && 
                caseObj.SuppliedEmail==null &&  
                caseObj.Contact_Email__c !=null &&
                caseObj.Contact_Email__c.trim() !='' &&
                (caseObj.recordTypeId ==  recordTypeGRCase[0].Id || caseObj.recordTypeId == recordTypeVelCase[0].id || caseObj.recordTypeId == recordTypePRVCase[0].id || caseObj.recordTypeId == recordTypeVFFCase[0].id) 
               )
        {  
            emailAddresses.add(caseObj.Contact_Email__c.trim());  
        }
        
    }  
    
    //Find any matching contacts as per email address
    List<Contact> listContacts = [Select Id, name, phone, mobilePhone, Email, Velocity_Number__c, 
                                  Velocity_Member__c, Velocity_Status__c, Travel_Agent_ID__c, 
                                  I_am_a_Travel_Agent__c From Contact 
                                  where name != '' 
                                  and Email in :emailAddresses];
    
    Map<String, Contact> takenEmailsMap = new Map<String, Contact>();
    for (Contact c:listContacts) 
    {  
        //assumes no duplicate contacts 
        takenEmailsMap.put(c.Email.trim(), c);
    }  
    
    Map<String,Contact> emailToContactMap = new Map<String,Contact>();  
    List<Case> casesToUpdate = new List<Case>();  
    
    for (Case caseObj:Trigger.new)
    {       	    	
        //create contacts from web form cases where contact does not exist
        if( caseObj.ContactId==null &&  
           caseObj.SuppliedEmail!=null &&  
           caseObj.SuppliedEmail.trim()!='' &&  
           !takenEmailsMap.containsKey(caseObj.SuppliedEmail.trim()) &&
           caseObj.RecordTypeId != caseVelocityMemberSupportId)  
        {  
            //The case was created with a null contact  
            //Let's make a contact for it  
            //no current validation on phone/mobile numbers
            Contact cont = new Contact(FirstName=caseObj.Contact_First_Name__c,  
                                       LastName=caseObj.Contact_Last_Name__c, 
                                       phone=caseObj.Contact_Phone__c,
                                       mobilePhone=caseObj.Contact_Mobile__c,
                                       Email=caseObj.SuppliedEmail.trim(),
                                       Velocity_Member__c = caseObj.Velocity_Member__c,
                                       Velocity_Number__c = caseObj.Velocity_Number__c,
                                       travel_agent_id__c = caseObj.Travel_Agent_ID__c,
                                       I_am_a_travel_agent__c = caseObj.Travel_Agent_ID__c != null,
                                       RecordTypeId = recordTypeGRContact[0].id );  
            emailToContactMap.put(caseObj.SuppliedEmail.trim(), cont);  
            casesToUpdate.add(caseObj);  
        }  
        //web case and contact exists in system but not referenced in case record
        else if( caseObj.ContactId == null &&  
                caseObj.SuppliedEmail != null &&  
                caseObj.SuppliedEmail.trim() != '' &&
                takenEmailsMap.containsKey(caseObj.SuppliedEmail.trim()) )  
        {
            casesToUpdate.add(caseObj);    
        }
        //create contacts from manually created cases where contact details supplied in case and Case lookup not populated
        //and contact does not exist
        else if( caseObj.ContactId==null &&  
                caseObj.SuppliedEmail==null &&  
                caseObj.Contact_Email__c !=null &&
                caseObj.Contact_Email__c.trim() !='' &&  
                !takenEmailsMap.containsKey(caseObj.Contact_Email__c.trim()) )  
        {  
            //The case was created with a null contact  
            //Let's make a contact for it  
            //no current validation on phone/mobile numbers
            Contact cont = new Contact(FirstName=caseObj.Contact_First_Name__c,  
                                       LastName=caseObj.Contact_Last_Name__c,  
                                       phone=caseObj.Contact_Phone__c,
                                       mobilePhone=caseObj.Contact_Mobile__c,
                                       Email=caseObj.Contact_Email__c.trim(),
                                       Velocity_Member__c = caseObj.Velocity_Member__c,
                                       Velocity_Number__c = caseObj.Velocity_Number__c,
                                       Velocity_status__c = caseObj.Velocity_type__c,
                                       travel_agent_id__c = caseObj.Travel_Agent_ID__c,
                                       //I_am_a_travel_agent__c = String.valueOf(caseObj.Additional_Details__c).contains('I am a travel Agent'),                                     
                                       RecordTypeId = recordTypeGRContact[0].id );  
            if( caseObj.Additional_Details__c != null && caseObj.Additional_Details__c != '')
            {
                cont.I_am_a_travel_agent__c = String.valueOf(caseObj.Additional_Details__c).contains('I am a travel Agent');
            }
            emailToContactMap.put(caseObj.Contact_Email__c.trim(), cont);  
            casesToUpdate.add(caseObj);  
        } 
        
        //manual case and contact exists in system but not referenced in case record
        else if( caseObj.ContactId==null &&  
                caseObj.SuppliedEmail==null &&  
                caseObj.Contact_Email__c !=null &&
                caseObj.Contact_Email__c.trim() !='' &&  
                takenEmailsMap.containsKey(caseObj.Contact_Email__c.trim()) )  
        {  
            casesToUpdate.add(caseObj);  
        }          
    }  
    
    List<Contact> newContacts = emailToContactMap.values();  
    
    //can put this is try-catch if needed for validation rules etc
    if( newContacts.size()>0 )
    {
        try{
            insert newContacts;
        }
        Catch(Exception ex){
        }
    }
    
    for (Case caseObj:casesToUpdate) 
    {  
        Contact currContact = null; 
        if(caseObj.SuppliedEmail != null && caseObj.SuppliedEmail.trim() != ''
           && !takenEmailsMap.containsKey(caseObj.SuppliedEmail.trim()))
        {  
            //new contact web case
            currContact = emailToContactMap.get(caseObj.SuppliedEmail.trim());
        }
        else if(caseObj.SuppliedEmail != null && caseObj.SuppliedEmail.trim() != ''
                && takenEmailsMap.containsKey(caseObj.SuppliedEmail.trim()))
        {
            //existing contact web case
            currContact = takenEmailsMap.get(caseObj.SuppliedEmail.trim());
        }
        else if(caseObj.Contact_Email__c != null && caseObj.Contact_Email__c.trim() != ''
                && !takenEmailsMap.containsKey(caseObj.Contact_Email__c.trim()) 
                && caseObj.SuppliedEmail == null)
        {
            //new contact manual case
            currContact = emailToContactMap.get(caseObj.Contact_Email__c.trim());
        }
        else if(caseObj.Contact_Email__c != null && caseObj.Contact_Email__c.trim() != ''
                && takenEmailsMap.containsKey(caseObj.Contact_Email__c.trim()) 
                && caseObj.SuppliedEmail == null)
        {
            //existing contact manual case
            currContact = takenEmailsMap.get(caseObj.Contact_Email__c.trim());
        }
        // By passing Velocity Member Support records from assigning contacts
        if( currContact != null && caseObj.RecordTypeId != caseVelocityMemberSupportId)
        {
            caseObj.ContactId = currContact.Id;
        }  
    }  
    
}