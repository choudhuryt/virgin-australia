trigger ContractExtensionTrigger on Contract_Extension__c (before insert, before update) 
{
   if( Trigger.isBefore && ( Trigger.isInsert || Trigger.isUpdate ) )
   {
   ContractExtensionTriggerHandler.setLevel3Manager( Trigger.isInsert, Trigger.new, Trigger.oldMap  );
   }
   if( Trigger.isBefore &&  Trigger.isInsert ) 
   {
   ContractExtensionTriggerHandler.setMaxExtensionVersion( Trigger.isInsert, Trigger.new, Trigger.oldMap  );  
   }
}