trigger ContractAddendumMasterTrigger on Contract_Addendum__c (after insert,after update,after delete, before insert, before update)
{ 
    if( Trigger.isAfter && Trigger.isUpdate )
    
    {
       ContractAddendumMasterTriggerHandler.UpdateDiscountOnContract( Trigger.isInsert,  Trigger.new, Trigger.oldMap); 
       ContractAddendumMasterTriggerHandler.SumbitExtensionForApproval( Trigger.isInsert,  Trigger.new, Trigger.oldMap);  
     }
       
    
}