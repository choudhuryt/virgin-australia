/** 
* * File Name        : ContractFulfilmentTrigger
* * Description      : This Apex Trigger will copy values to the Contract and the account object so that 
                     : Conga can print out information on the printed contract 
                        
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Andy Burgess
* * Date             : 7 March 2013
* * Technical Task ID: Corporate Contract brand Realignment (Remedy Number:HD0000000495937)
* * Notes            : The test class for this file is: TestContractFulfilmentTrigger 

====================================================================================

* * Modification Log *  *

Ver Date Author Modification
 
***/

trigger ContractFulfilmentTrigger on Contract_Fulfilment__c (before insert, before update)
{
    
    Set<Id> accIds  = new Set<Id>(); 
    Set<Id> tourcodeIds  = new Set<Id>(); 
    Set<Id> contractIds  = new Set<Id>(); 
    Set<Id> cfids =  new Set<Id>(); 
    Set<String> tname = new Set<String>(); 
    String tourcode;
        
    for(Contract_Fulfilment__c contractFulfilmentItr: Trigger.New) 
    { 
        system.debug('This is the tourcode:' + contractFulfilmentItr.Tour_Code__c );
        if (contractFulfilmentItr.Tour_Code__c != null)
        {
            accIds.add(contractFulfilmentItr.Account__c) ;
            tname.add(contractFulfilmentItr.Tour_Code__c) ;
            contractids.add(contractFulfilmentItr.Contract__c) ;
            cfids.add(contractFulfilmentItr.Id);
            tourcode = contractFulfilmentItr.Tour_Code__c;
        }

    }
    system.debug('The account and contract id' + accIds +contractids );
    List<Contract_Fulfilment__c> cfList =  [select id , Contract__c,Account__c, Tour_Code__c from Contract_Fulfilment__c where Account__c =:accIds and  Contract__c = :contractids];
    List<Tourcodes__c> tourcodeList = [select id,Tourcode__c,Status__c,tourcode_purpose__c, Account__c,Contract__c from Tourcodes__c where Account__c =:accIds and Tourcode__c =:tname  ];
   system.debug('This is the cflist:' + cfList + tourcodeList );
            
     if(tourcodeList.size()<1 && cfList.size() > 0)
         {
             
                Tourcodes__c tourcodetoinsert = new  Tourcodes__c();
                tourcodetoinsert.Account__c = cfList[0].Account__c;
                tourcodetoinsert.Tourcode__c = tourcode;
                tourcodetoinsert.tourcode_purpose__c = 'Corp Tourcode';
                tourcodetoinsert.Status__c = 'Active';
                tourcodetoinsert.Contract__c =  cfList[0].Contract__c;
                insert tourcodetoinsert;
       }
               
    
            
   
}