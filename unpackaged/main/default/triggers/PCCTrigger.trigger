trigger PCCTrigger on PCC__c (after insert) {


//check pcc name and gds for insertion

for(PCC__c pcc : Trigger.new){

List<Data_Validity__c> dataValidityList = new List<Data_Validity__c>();
	
	List<PCC__c> pccList = new List<PCC__c>();
	
	pccList= [select id,IATA_PCC_GDS__c,Name,Account__c from PCC__c where name =: pcc.name ];
	
	
	for(integer x =0;x < pccList.size();x ++ ){
		
		PCC__c exsitingPCC = new PCC__c();
		exsitingPCC = pccList.get(x);
		
		if(exsitingPCC.id <> pcc.id ){
		if(exsitingPCC.IATA_PCC_GDS__c == pcc.IATA_PCC_GDS__c ){
			
			trigger.new[0].name.addError('You have entered in a PCC that is already in the system with the same GDS, you cannot do this');
			
		}
		}
		
		
		
	}
	
	dataValidityList = [select id from Data_Validity__c where pcc__c =:pcc.id];
	
	if(dataValidityList.size()>0){
		//do stuff
		
		
	}else{
		Date d = Date.newinstance(2001, 12, 31);
		Data_Validity__c dataValidity = new Data_Validity__c();
		
		dataValidity.From_Account__c = pcc.account__c;
		dataValidity.From_Date__c = d;
		dataValidity.PCC__c = pcc.id;
		dataValidity.RecordTypeid = '012900000009YBZAA2';
		
		insert dataValidity;
	}

}





}