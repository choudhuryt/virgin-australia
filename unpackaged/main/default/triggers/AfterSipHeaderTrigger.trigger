trigger AfterSipHeaderTrigger on SIP_Header__c (after update)
{
if( Trigger.isAfter && Trigger.isUpdate)
       {
          SipHeaderTriggerHandler.updatesippercentage( Trigger.new); 
       }
}