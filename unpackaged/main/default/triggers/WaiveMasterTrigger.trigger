trigger WaiveMasterTrigger on Waiver_Favour__c (after insert,after update,after delete, before insert, before update)

{
if( (Trigger.isAfter && ( Trigger.isInsert || Trigger.isUpdate )) ||  (Trigger.isAfter &&  Trigger.isDelete) )
   {
        WaiverTriggerHandler.CalculateAccountLevelPoints( Trigger.isDelete, Trigger.new, Trigger.old ); 
        WaiverTriggerHandler.CalculateAccountManagerPoints( Trigger.isDelete, Trigger.new, Trigger.old );
        WaiverTriggerHandler.CalculateTravelBankCompensation(Trigger.isDelete, Trigger.new, Trigger.old );   
   }  
 if( Trigger.isBefore && ( Trigger.isInsert || Trigger.isUpdate  || Trigger.isDelete)  )  
    {
     WaiverTriggerHandler.L3ManagerAllocation(Trigger.new); 
      
    }
    
    
}