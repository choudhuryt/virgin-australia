/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 22.10.2021
**Description: Trigger to update the handling time for the Cases when it's 'Closed'
*/
trigger CaseResolutionTimeTrigger on Case (after update) {
    
    BusinessHours bh = [SELECT Id FROM BusinessHours WHERE Name = 'VFF Business Hours' AND IsActive = true];
    List<case> upCaseList = new List<Case>();
    decimal handlingTime;
    decimal handlingHours;
    decimal handlingDays;
    decimal handlingMins;
    
    for(Case cs: trigger.new){
               
        System.debug(Trigger.oldMap.get(cs.Id).First_Response__c+'--cs.First_Response_Completed__c--'+cs.First_Response__c+'--First_Response_Time__c--'+cs.First_Response_Time__c);
        System.debug(Trigger.oldMap.get(cs.Id).Status+'--Status--'+cs.Status+'--ClosedDate--'+cs.ClosedDate+'--Created/ReOpened Time--'+cs.CreatedDate);
        if(cs.ClosedDate != NULL && Trigger.oldMap.get(cs.Id).Status != cs.Status){
            if(cs.Case_Re_Opened_Date__c == NULL){
                handlingTime = BusinessHours.diff(bh.Id, cs.CreatedDate, cs.ClosedDate);
            }else{
                handlingTime = BusinessHours.diff(bh.Id, cs.Case_Re_Opened_Date__c, cs.ClosedDate);
            }
            handlingHours = handlingTime/(60*60*1000);
            handlingDays = handlingHours/9;
            handlingMins = handlingHours*60;
            System.debug('handlingTime--'+handlingTime+'--handlingHours--'+handlingHours+'--handlingDays--'+handlingDays);            
            
            Case upCase = new Case();
            upCase.Id = cs.Id;
            upCase.Handling_Time_Hrs__c = handlingHours.setScale(2);
            upCase.Handling_Time_Days__c = handlingDays.setScale(2);
            upCase.Handling_Time_Mins__c = handlingMins.setScale(2);
            upCaseList.add(upCase);
        }
        if(cs.First_Response_Time__c != NULL && Trigger.oldMap.get(cs.Id).First_Response__c != cs.First_Response__c){
            if(cs.Case_Re_Opened_Date__c == NULL){
                handlingTime = BusinessHours.diff(bh.Id, cs.CreatedDate, cs.First_Response_Time__c);
            }else{
                handlingTime = BusinessHours.diff(bh.Id, cs.Case_Re_Opened_Date__c, cs.First_Response_Time__c);
            }
            handlingMins = handlingTime/(60*1000);
            
            Case upCase1 = new case();
            upCase1.Id = cs.Id;
            upCase1.First_Response_Handling_Time_in_Mins__c = handlingMins.setScale(2);
            upCaseList.add(upCase1);
        }
    }
  System.debug('upCaseList---'+upCaseList);
    if(upCaseList.size()>0){
        update upCaseList;
    }
}