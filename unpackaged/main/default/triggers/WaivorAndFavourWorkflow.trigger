/**
 * @description       : Trigger for Waiver_Favour__c
 * UpdatedBy : cloudwerx (Removed SOQL queries from loop and use handler to implement the logic rather than in trigger itself)
**/
trigger WaivorAndFavourWorkflow on Waiver_Favour__c (before insert, before update) {
    WaivorAndFavourWorkflowHandler.waiverFavourActivity(trigger.new);
}