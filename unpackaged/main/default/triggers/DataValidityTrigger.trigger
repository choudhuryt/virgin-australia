trigger DataValidityTrigger on Data_Validity__c (before insert, before update) {
    //@UpdateBy: Cloudwerx refactoring and moved the logic into handler
    DataValidityTriggerHandler.dataValidityActivity(Trigger.New);
}