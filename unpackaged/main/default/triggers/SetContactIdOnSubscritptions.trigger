/** * File Name      : SetContactIdOnSubscritptions
* Description        : This Apex Trigger will make a Subscription every time a newContact is inserted
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Ed Stachyra
* * Date             : 31 May 2012
* * Technical Task ID: 
* * Notes            : Mod1 The test class for this file is:  TestSetContactIdOnSubscritptions
* Modification Log =============================================================== 
Ver Date Author Modification --- ---- ------ -------------
* */

//
//  The test class for this file is:  SetContactIdOnSubscritptionsTest
//
trigger SetContactIdOnSubscritptions on Contact ( after insert,after update)
{
    Set<ID> ls = new Set<ID> ();
    for(Contact l:Trigger.new)
    {
        if(l.Subscriptions__c == null)
        {
            ls.add(l.id);  
            
        }   
    }
    List<RecordType> ContactList = [select IsActive, BusinessProcessId, CreatedById, CreatedDate, Description, 
                                    LastModifiedById, LastModifiedDate, Name, NamespacePrefix, Id, DeveloperName, 
                                    SobjectType, SystemModstamp FROM RecordType where name='Contact' AND sobjecttype='Contact'];
    //Catches all contacts// List<Contact> lis=[Select AFL__c, Accelerate_EDM__c, AccountId, Allowed_to_Receive_Gifts__c, Animals__c, Art_and_Museum__c, AssistantName, AssistantPhone, Birthdate, Boat_Yacht_Racing__c, Fax, Business_News__c, Phone, Car_Car_Racing__c, Charities__c, Competitor_Sales_Rep__c, Description, Id, ConvertedFromLead__c, Corporate_TMC_Contact__c, CreatedById, CreatedDate, Cricket__c, IsDeleted, Department, Email, EmailBouncedDate, EmailBouncedReason, Fare_Sheets__c, Fashion_and_Design__c, FirstName, Food_and_Wine__c, Name, Golf__c, HomePhone, Home_and_Garden__c, Horse_Racing__c, Influence_Ranking__c, Invitations__c, Xmas_Card__c ,Job_Function__c, Key_Contact__c, LastActivityDate, LastModifiedById, LastModifiedDate, LastName, LastCURequestDate, LastCUUpdateDate, LeadSource, Live_Entertainment__c, MailingCity, MailingCountry, MailingState, MailingStreet, MailingPostalCode, MasterRecordId, Middle_Name__c, MobilePhone, NBL__c, NRL__c, Opera__c, OtherCity, OtherCountry, OtherPhone, OtherState, OtherStreet, OtherPostalCode, OwnerId, Preferred_AFL_Club__c, Preferred_Charities__c, Preferred_Cricket_Club__c, Preferred_NBL_Team__c, Preferred_NRL_Club__c, Preferred_Rugby_Union_Club__c, Preferred_Soccer_Club__c, RecordTypeId, Release_Communication__c, ReportsToId, Role__c, Rugby_Union__c, Salutation, Soccer__c, Status__c, Subscriptions__c, SystemModstamp, Tennis__c, Theatre__c, Title, Trade_Release__c, Travel_Policy__c, Velocity_Expiry_Date__c, Velocity_Member__c, Velocity_Number__c, Velocity_Status__c from Contact where id in:ls];
    
    List<Contact> lis=[Select AFL__c, Accelerate_EDM__c, AccountId, Business_News__c, Phone, Competitor_Sales_Rep__c, 
                       Description, Id, ConvertedFromLead__c, Corporate_TMC_Contact__c, CreatedById, CreatedDate, IsDeleted, 
                       Department, Email, Fare_Sheets__c, FirstName, Name, HomePhone, Influence_Ranking__c, Invitations__c, 
                       Xmas_Card__c ,Job_Function__c, Key_Contact__c, LastActivityDate, LastModifiedById, LastModifiedDate, 
                       LastName, LastCURequestDate, LastCUUpdateDate, LeadSource, MailingCity, MailingCountry, MailingState, 
                       MailingStreet, MailingPostalCode, MasterRecordId, Middle_Name__c, MobilePhone, OtherCity, OtherCountry, 
                       OwnerId, RecordTypeId,Release_Communication__c, ReportsToId, Role__c, Salutation,Status__c, Subscriptions__c, 
                       SystemModstamp, Title, Trade_Release__c, Travel_Policy__c, Velocity_Expiry_Date__c, Velocity_Member__c, 
                       Velocity_Number__c, Velocity_Status__c from Contact where id in:ls
                       AND (Contact.RecordTypeId IN :ContactList OR (LeadSource = 'Web' and Account.Business_Number__c != NULL))];
    
    List<Subscriptions__c> lstSub = new List<Subscriptions__c>();
    
    //List<Subscriptions__c> existingSub = [Select Accelerate_EDM__c, Accelerate_EDM_Opt_Out__c, Business_News__c, Business_News_Opt_Out__c, Contact_ID__c, CreatedById, CreatedDate, IsDeleted, Fare_Sheets__c, Fare_Sheets_Opt_Out__c, Invitations__c, Invitations_Opt_Out__c,Xmas_Card__c,Xmas_Card_Opt_Out__c, LastModifiedById, LastModifiedDate, OwnerId, Id, Name, SystemModstamp, Trade_Release__c, Trade_Release_Opt_Out__c  from Subscriptions__c];
    /////List<Subscriptions__c> existingSub = [Select Contact_ID__c from Subscriptions__c where IsDeleted = false];
    List<RecordType> Z = new List<RecordType>();
    String string1;  //for Contact ID to look for Dup
    String string2;  //to use as flag
    String string3;  //for Last Name of Contact  
    String string4;  //for First Name of Contact
    String string5;  //the concat Name (Last + First + email)
    String string6;  //Contact email
    String string7;  //minimized string for contact name
    
    for(Contact co:lis)
    { 
        Subscriptions__c tempSub= new Subscriptions__c(); 
        tempSub.Contact_ID__c=co.Id;
        tempSub.Contact_ID__c = tempSub.Contact_ID__c.substring(0, Math.min(tempSub.Contact_ID__c.length(), 15)); //chopped 
        string1 = tempSub.Contact_ID__c;    //used below to look for dup
        tempSub.Trade_Release__c = co.Trade_Release__c;
        tempSub.Business_News__c = co.Business_News__c; 
        tempSub.Accelerate_EDM__c = co.Accelerate_EDM__c;
        tempSub.Fare_Sheets__c = co.Fare_Sheets__c;
        tempSub.Invitations__c = co.Invitations__c;
        tempSub.Xmas_Card__c   = co.Xmas_Card__c ;
        string3 = co.LastName;
        string4 = co.FirstName;
        string6 = co.Email;
        // string6 string6.substring(0, Math.min(string6.length(), 40));
        string7 = (string3+ ',' + string4); //added this one to fix the issue
        string7 = string7.substring(0, Math.min(string7.length(), 40)); //chopped first + last name
        // string5 = (string3+ ',' + string4 + ','+ '('+ string6 +')');
        string5 = (string7 + '('+ string6 +')'); //added this to fix
        
        if(string5.length()>80)
        {
            string5 = string5.substring(0, Math.min(string5.length(), 80));
        }
        
        tempSub.Name=string5;
        
        lstSub.add(tempSub); 
        //tempSub.Accelerate_EDM__c =True;
        system.debug('running sucessfully' +tempSub);
        
    }
    
    if(lstSub.size() > 0 )
        
        //cycle through the the existing Subscriptions looking for a duplicate
        //   for (Subscriptions__c d : existingSub) 
        //   {
        //      if (d.Contact_ID__c == string1) //looking for a duplicate subscription
        //       {
        //       string2='1';  //set a flag if its a duplicate subscription
        //       }
        //   }
        //   if (string2!='1')   //if no duplicates then insert
        //   {
        insert lstSub;
    //   }
    
}