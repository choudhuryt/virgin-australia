/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 22.03.2022
**Description: Trigger to mask sensitive data in Subject and Body of Chat and if Velocity Number is updated, call 'SendCommsToAlmsWS' Class
* UpdatedBy : cloudwerx
*/
trigger SendChattoAlmsTrigger on LiveChatTranscript (before update, after update) {
    //@UpdatedBy : cloudwerx moved logic to handler
    SendChattoAlmsTriggerHandler.liveChatTranscriptUpdateActivities(Trigger.New, Trigger.OldMap, Trigger.isBefore, Trigger.isAfter);
}