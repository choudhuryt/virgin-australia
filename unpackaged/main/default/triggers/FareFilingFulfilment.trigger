trigger FareFilingFulfilment  on Contract_Fulfilment__c (AFTER insert,AFTER update) {

 Set<Id> RecIDs  = new Set<Id>();
 Set<Id> ContractIDs  = new Set<Id>();
 String Amadeuspcclist = 'None'  ;   
 string Sabrepcclist = 'None' ;    
 string Abacuspcclist = 'None' ;    
 string Galileopcclist = 'None' ; 
 string Worldpcclist = 'None' ;  
 string Travelskypcclist = 'None' ; 
 string Otherpcclist = 'None' ;   
    
 List<Contract> ConToUpdate = new List<Contract>();

    for(Contract_Fulfilment__c cf: Trigger.New)
    {
    			RecIDs.add(cf.Id);
                ContractIDs.add(cf.Contract__c);
	}
		
  List <Contract_Fulfilment__c> c = [SELECT PCC_s__c ,GDS_System__c  FROM Contract_Fulfilment__c 
                                     WHERE Contract__c = :ContractIDs
                                     and  Status__c != 'Inactive' 
                                     ORDER by GDS_Code__c];
    
   List <Contract> affcon =  [SELECT id , List_of_1A_Pccs__c , List_of_1E_Pccs__c,
                                   List_of_1G_Pccs__c, List_of_1P_Pccs__c,
                                   List_of_1S_Pccs__c,List_of_all_GDS_Codes__c,
                                   List_of_B1_Pccs__c,List_of_Other_Pccs__c
                             from Contract where id =   :ContractIDs];

   for(Contract con: affcon)
    {
        
        if (c.size() > 0)
            
            
        { 
                        
            for (Integer i = 0; i < c.size(); i++) 
            {
                     
              if (c[i].GDS_System__c == 'Abacus') 
              {
                 
               Abacuspcclist =    Abacuspcclist +  ',' +  c[i].PCC_s__c  ;
              }     
              
              if (c[i].GDS_System__c == 'Sabre') 
              {
              Sabrepcclist =   Sabrepcclist  + ','  +  c[i].PCC_s__c  ;
              }  
               
               if (c[i].GDS_System__c == 'Amadeus') 
              {
              Amadeuspcclist  =   Amadeuspcclist   + ','  +  c[i].PCC_s__c  ;
              }
                 if (c[i].GDS_System__c == 'Galileo') 
              {
              Galileopcclist =   Galileopcclist  + ','  +  c[i].PCC_s__c  ;
              }
                 if (c[i].GDS_System__c == 'Worldspan') 
              {
             Worldpcclist  =   Worldpcclist  + ','  +  c[i].PCC_s__c  ;
              }
                 if (c[i].GDS_System__c == 'Travelsky') 
              {
              Travelskypcclist =   Travelskypcclist + ','  +  c[i].PCC_s__c  ;
              }                
             
              if (c[i].GDS_System__c == 'Apollo' ) 
              {
              Otherpcclist =   Otherpcclist  + ','  +  c[i].PCC_s__c  ;
              }
                
              }

        }
        
        IF( Abacuspcclist <> NULL)
        {    
      con.List_of_B1_Pccs__c =  Abacuspcclist.replaceFirst('None,', '') ;
        }
        IF( Sabrepcclist <> NULL)
        {     
        con.List_of_1S_Pccs__c =  Sabrepcclist.replaceFirst('None,', '') ;
        }
         IF( Galileopcclist <> NULL)
        {  
        con.List_of_1G_Pccs__c =  Galileopcclist.replaceFirst('None,', '') ;
        }
         IF( Amadeuspcclist <> NULL)
         {     
        con.List_of_1A_Pccs__c =  Amadeuspcclist.replaceFirst('None,', '') ;
         }
        IF( Worldpcclist<> NULL)
         { 
        con.List_of_1P_Pccs__c =  Worldpcclist.replaceFirst('None,', '') ;
         }
        IF( Travelskypcclist<> NULL)
         {
        con.List_of_1E_Pccs__c =  Travelskypcclist.replaceFirst('None,', '') ; 
         }
        IF( Otherpcclist<> NULL)
         {
        con.List_of_Other_Pccs__c =  Otherpcclist .replaceFirst('None,', '') ;
         }
        ConToUpdate.add(con); 
    } 
    
    update ConToUpdate ;
}