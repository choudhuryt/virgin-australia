/*
 *  Updated by Cloudwerx : Moved the trigger logic to AccountDataValidityHandler class
 * 
 */ 
trigger AccountDataValidity on Account_Data_Validity__c (after insert, after update) {
    AccountDataValidityHandler.accountDataValidityActivity(Trigger.New, Trigger.isUpdate);
}