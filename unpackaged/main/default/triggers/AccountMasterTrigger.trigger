trigger AccountMasterTrigger on Account ( before insert, before update, after update, after insert  )
{
    if( Trigger.isAfter && Trigger.isInsert )
    {
        AccountTriggerHandler.createDataValidity( Trigger.new );
        AccountTriggerHandler.setrelatedtmcparent(  Trigger.new, Trigger.oldMap ); 
        
    }
    else if( Trigger.isAfter && ( Trigger.isUpdate ) )
    {   
        AccountTriggerHandler.setWaiverParentPoints( Trigger.isInsert, Trigger.new, Trigger.oldMap );
        AccountTriggerHandler.setrelatedtmcparent(  Trigger.new, Trigger.oldMap );
        
    }       
    else if( Trigger.isBefore && ( Trigger.isInsert || Trigger.isUpdate ) )
    {
        AccountTriggerHandler.setAccountOwner( Trigger.isInsert, Trigger.new, Trigger.oldMap );
        AccountTriggerHandler.updateEmpowerPassword( Trigger.isInsert, Trigger.new, Trigger.oldMap );
        AccountTriggerHandler.updateoldABN( Trigger.isInsert,  Trigger.new, Trigger.oldMap );
        
        // Comment to note by Cwerx : as MarkDuplicates method is present in the handler
        
        if( Trigger.isInsert )
        {
            AccountTriggerHandler.markDuplicatesV2( Trigger.New ); 
        }
        
    }
    
    public static void sum(integer a,integer b)
    {
        integer c = a + b;
    }    
}