/** * File Name      : ResolvePrismKeyRoutes
* *Description       : This Apex Trigger will make update the PRISM Key Routes
* *                    with the City and Airport Code in the Custom Settings.
* *                    this action is performed prior on memory to the update or insert
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Steven Kerr & Ed Stachyra
* * Date             : 29 May 2012
* * Technical Task ID: 
* * Notes            :  The test class for this file is: TestResolvePrismKeyRoutes  
* Modification Log =============================================================== 
Ver Date Author Modification --- ---- ------ -------------
* */

trigger ResolvePrismKeyRoutes on PRISM_Key_Routes__c (before insert, before update) 
  {

    String string3;   //Origin_Full__c
    String string4;   //Destination_Full__c
    
    //a map here to the custom setting of all airport codes and cities
    Map<String ,Airport_Codes__c > mc = new Map<String ,Airport_Codes__c>();  
    
    //cycle through custom settings to build map
    for (Airport_Codes__c a: [SELECT Airport__c, Airport_Code__c, City__c, Country__c, CreatedById, CreatedDate, IsDeleted, LastModifiedById, LastModifiedDate, SetupOwnerId, Name, Id, SystemModstamp FROM Airport_Codes__c ])
      {
             mc.put(a.Airport_Code__c, a);
      }
    Set<String> mcs = new Set<String>();
    mcs.addAll(mc.keySet());
    
     //the 'for loop' for this trigger to cycle though
      for( PRISM_Key_Routes__c c : Trigger.new) 
      {
        
            //match up the custom setting 3 letter ID with Airport Code
            Airport_Codes__c tempInstance = mc.get(c.Origin__c);
            Airport_Codes__c tempInstance2 = mc.get(c.Destination__c);
        
        
         boolean fire = false;      
         if (Trigger.isInsert) 
         {
                fire = true;
         }  
         else if (Trigger.isUpdate) 
         {
            if(Trigger.oldMap.get(c.ID).Origin__c != c.Origin__c) 
            {
                fire = true;
            }
            
            if(Trigger.oldMap.get(c.ID).Destination__c != c.Destination__c) 
            {
                fire = true;
            }
            
            if(tempInstance != null){
                if(Trigger.oldMap.get(c.ID).Origin_Full__c != tempInstance.Name){
                    fire = true;
                }
            }
            
            if(tempInstance2 != null){
                if(Trigger.oldMap.get(c.ID).Destination_Full__c != tempInstance2.Name){
                    fire = true;
                }
            }           
            
         }
            
            // Check if we need to processes this record    
            if (fire) {
            
         
           //Origin to resolve
           if (tempInstance != null) 
            {
              string3 = tempInstance.Name;
              c.Origin_Full__c = string3;
              System.debug('--setting origin__c---');
             } else {
                c.Origin_Full__c = '';
             }
               //Destination to resolve
            if (tempInstance2 != null) 
            {
              string4 = tempInstance2.Name;
              c.Destination_Full__c = string4;
              System.debug('--setting destination__c---');
            } else {
                c.Destination_Full__c = '';
            }

            
            }
         }
    
  }