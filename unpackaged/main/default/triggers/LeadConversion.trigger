trigger LeadConversion on Lead (after update) {
    List<ID> cids = new List<ID> {};
        List<Contact> contactlist = new List<Contact>();
    
    for(Lead led: Trigger.New)
    { 
        if (Trigger.old[0].isConverted == false && Trigger.new[0].isConverted == true && Trigger.new[0].leadsource == 'GSO') 
        {             
            cids.add(led.ConvertedContactId);          
        } 
    }
    
    for(Contact con: [select id from contact where id in : cids]) {
        
        contactlist.add(con);
    }   
    
    if(contactlist.size()>0)
    {
        delete contactlist;
    }
    
}