/** 
 *  File Name       : VSM_Trigger 
 *
 *  Description     : This trigger makes sure there is enough VSMs remaining on a contract 
 *                    before it is created.  
 *                      
 *  Copyright       : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
 *  @author         : Steve Kerr
 *  Date            : 22nd October 2015
 *
 *  Notes           : 
 *
 **/ 
trigger VSM_Trigger on Velocity_Status_Match__c (before insert) {
	// There could be many VSMs, related to many different contracts.  We only want to
	// tally them up once.  From there we just reduce the totals as we go.
	Map<Id, VSM_Utills.VSM_Totals> VSMTotals = new Map<Id, VSM_Utills.VSM_Totals>();
	Id VSMRecordTypeId = Schema.SObjectType.Velocity_Status_Match__c.getRecordTypeInfosByName().get('Status Match').getRecordTypeId();
	for(Velocity_Status_Match__c vsm : Trigger.new){

		// If this VSM is "Out of contract", then we can skip validation.
		if(vsm.Within_Contract__c == 'Yes' &&  vsm.RecordTypeID != VSMRecordTypeId){
			
			VSM_Utills.VSM_Totals thisTotal;  // Stores the totals for this VSMs parent Contract.

			// Check if we already have the VSM tally for this Contract
			if(VSMTotals.containsKey(vsm.Contract__c)){
				thisTotal = VSMTotals.get(vsm.Contract__c); 
			} else {
				// We don't have it.  Let's get it and store it for next time.
				thisTotal = VSM_Utills.getVSM_Totals(vsm.Contract__c);
				VSMTotals.put(vsm.Contract__c, thisTotal);
			}

			String tier = vsm.Status_Match_or_Upgrade__c;

			// Now we have the relevent totals, let's see if there is enough remaining.
			Integer remiaining = thisTotal.getRemaining(tier);
			if(remiaining > 0){
				// we have enough, now record this VSM to update the totals.
				thisTotal.incrementAllocated(tier);
			} else {
				// There is not enough, throw an error.
				
				// unless this is a test
				if(Test.isRunningTest()){
					
				} else {
					vsm.addError('There are no '+tier+' upgrades available to allocate, ' + 
					'You must submit this upgrade out of contract to be approved');					
				}
			}

		}
	} 
}