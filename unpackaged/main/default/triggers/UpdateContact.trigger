/*
 * HISTORY
 * 
 * 24.May.2016  Warjie Malibago, Irish Ghen Mozo (Accenture CloudFirst)  C# 106979 | Duplicate Management Issue; see 240516WBMIGM
*/
trigger UpdateContact on Contact (before insert, before update) {
    for(Contact contact : Trigger.new){
        if(Trigger.isInsert){
            if(contact.Account_ID__c !=null ){
                PartnerNetworkRecordConnection pnrc= new PartnerNetworkRecordConnection();
                List<PartnerNetworkRecordConnection> pnrList = new List<PartnerNetworkRecordConnection>();
                system.debug( 'Smartfly account id' + contact.Account_ID__c );
                pnrList= [select PartnerRecordId,ParentRecordId,LocalRecordId from PartnerNetworkRecordConnection where PartnerRecordId =:contact.Account_ID__c];
                if(pnrList.size()>0){
                    pnrc = pnrList.get(0);
                    system.debug( 'Parent account id' + pnrc.LocalRecordId );
                    contact.AccountId = pnrc.LocalRecordId;
                }
            }
        }
        //240516WBMIGM Start
        String recConId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();
        String recGRId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GR Contact').getRecordTypeId();
        if(Trigger.isInsert){
            for(Contact con: Trigger.New){
                system.debug('**Contact id: ' + con.Id + ' - ' + con.AccountId);
                if(con.RecordTypeId == recConId)
                    con.Record_Type__c = 'Contact';
                if(con.RecordTypeId == recGRId)
                    con.Record_Type__c = 'GR Contact';
            }
            recursiveTriggerHandler.isGoodForUpdate = FALSE;
        }
        
        if(Trigger.isUpdate){
            system.debug('**isGoodForUpdate? : ' + recursiveTriggerHandler.isGoodForUpdate);
            if(recursiveTriggerHandler.isGoodForUpdate){ //this is implemented to prevent recursive trigger
                for(Contact con: Trigger.New){
                    con.Record_Type__c = (con.RecordTypeId != null ? (con.RecordTypeId == recGRId ? 'GR Contact' : 'Contact') : 'Contact');
                    system.debug('**Record Type: ' + con.Record_Type__c + ' - ' + con.Id);
                }
                recursiveTriggerHandler.isGoodForUpdate = FALSE;
            }
        }
        //240516WBMIGM End
    }
}