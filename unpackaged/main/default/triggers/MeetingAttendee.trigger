trigger MeetingAttendee on Meeting_Attendee__c (after insert, after update) {



	if(Trigger.isInsert){
	
 	for(Meeting_Attendee__c meetingAttendee : Trigger.new){
 	
 	//if(meetingAttendee.RecordTypeId =='012900000007g0IAAQ'){
 		
 	//List<Meeting__c> meetingOldList = new List<Meeting__c>();
 	//List<Meeting__c> meetingNewList = new List<Meeting__c>();
 	
 	//Meeting__c meetingOld = new Meeting__c();
 	
 	 //	Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Meeting__c.fields.getMap();
	//	List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
		
	//	String theQuery = 'SELECT ';
	//	for(Schema.SObjectField s : fldObjMapValues)
	//	{
	//	   String theName = s.getDescribe().getName();
	//	   theQuery += theName + ',';
	//	}
		
		// Trim last comma
	//	theQuery = theQuery.subString(0, theQuery.length() - 1);
		
		// Finalize query string
	//	theQuery += ' FROM Meeting__c WHERE Id = \'' + meetingAttendee.Meeting__c + '\'';
		
		// Make your dynamic call
	//	meetingOld = (Meeting__c)Database.query(theQuery);
 		
 		
 //		Contact contact = new contact();
 //		contact =(Contact) [Select id,email from Contact where id=:meetingattendee.Contact__c];
 		
 		
 		
//String attachmentName = 'Meeting.ics';
//Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage(); 
//Now we have to set the value to Mail message properties 
//Note Please change it to correct mail-id to use this in your application 

//msg.setSenderDisplayName('Andy Burgess'); 
//msg.setToAddresses(new String[] {contact.Email}); 
//msg.setReplyTo('andrew.burgess@virginaustralia.com');
// it is optional, only if required 
//msg.setSubject('Appointment'); 
//msg.setPlainTextBody(meetingold.Additional_Meeting_Agenda__c); 
// Create the attachment
//CommonObjectsForTest comObj = new CommonObjectsForTest();
//Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment(); 
//efa.setFileName(attachmentName ); 

//String [] icsTemplate = new List<String> 
//{'BEGIN:VCALENDAR', 'PRODID:-//Schedule a Meeting', 'VERSION:2.0', 'METHOD:REQUEST', 'BEGIN:VEVENT', 'DTSTART: ' + String.valueof(meetingold.Meeting_Date_And_Time__c), 'DTSTAMP: '+ String.valueof(DateTime.Now()), 'DTEND: ' + String.valueof(meetingold.End_Meeting_Date_and_Time__c), 'LOCATION: ' + meetingold.Location__c, 'UID: ' + String.valueOf(Crypto.getRandomLong()), 'DESCRIPTION: ' + meetingold.Additional_Meeting_Agenda__c, 'X-ALT-DESC;FMTTYPE=text/html: ' + meetingold.Additional_Meeting_Agenda__c, 'SUMMARY: ' + 'Sales Call with Andy', 'ORGANIZER:MAILTO: ' + 'andrew.burgess@virginaustralia.com', 'ATTENDEE;CN="' + 'Andy' + '";RSVP=TRUE:mailto: ' + contact.Email, 'BEGIN:VALARM', 'TRIGGER:-PT15M', 'ACTION:DISPLAY', 'DESCRIPTION:Reminder', 'END:VALARM', 'END:VEVENT', 'END:VCALENDAR' }; 
//String attachment = String.join(icsTemplate, '\n'); 

//Blob b = Blob.valueof(attachment);
//efa.setBody(b);
//msg.setFileAttachments(new Messaging.EmailFileAttachment[] {efa}); 
// Send the email you have created. 
//Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg }); 
 		
 		
 //	}
 	
 	if( meetingAttendee.RecordTypeId =='012900000007g0DAAQ'){
 	List<Meeting__c> meetingOldList = new List<Meeting__c>();
 	List<Meeting__c> meetingNewList = new List<Meeting__c>();
 	
 	Meeting__c meetingOld = new Meeting__c();
 	Meeting__c meetingNew = new Meeting__c();
 	
 	 	Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Meeting__c.fields.getMap();
		List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
		
		String theQuery = 'SELECT ';
		for(Schema.SObjectField s : fldObjMapValues)
		{
		   String theName = s.getDescribe().getName();
		   theQuery += theName + ',';
		}
		
		// Trim last comma
		theQuery = theQuery.subString(0, theQuery.length() - 1);
		
		// Finalize query string
		theQuery += ' FROM Meeting__c WHERE Id = \'' + meetingAttendee.Meeting__c + '\'';
		
		// Make your dynamic call
		meetingOld = (Meeting__c)Database.query(theQuery);
    
 	
 	//meetingOldList = [select id,Account__c,Additional_Meeting_Agenda__c,Agent_Client_Business_Update__c,
 	//Client_Discount__c, from Meeting__c where id =:meetingAttendee.Meeting__c];
 	
 	//if(meetingOldList.size()>0){
 		
 		//meetingOld = meetingOldList.get(0);
 		meetingNew = meetingOld.clone(false, true, true, true);
 		meetingNew.Ownerid = meetingAttendee.User__c;
 		meetingNew.Account__c = meetingOld.Account__c;
 		meetingNew.Parent_Meeting__c = meetingold.id;
 		insert meetingNew;
 	
 	}
 	
 	
 			
 	}
	}

		 
}