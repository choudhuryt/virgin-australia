/*
**Created By: Tapashree Roy Choudhury(tapashree.choudhury@virginaustralia.com)
**Created Date: 22.03.2022
**Description: Trigger to call 'SendCommsToAlmsWS' incase the mail parent is Case/Contact with a Velocity Number tagged to it
* UpdatedBy : Cloudwerx
*/
trigger SendEmailtoAlmsTrigger on EmailMessage (after insert, after update) {
	SendEmailtoAlmsTriggerHandler.emailMessageActivities(Trigger.New, Trigger.isInsert, Trigger.isUpdate);
}