trigger SetLevel3Manager_ContractTermination on Contract_Termination__c (before insert, before update)

{
    
    Set<Id> ownerIDs  = new Set<Id>();
    Set<Id> level3IDs = new Set<Id>();
    
    List<Contract_Termination__c> affectedCt = new List<Contract_Termination__c>();
    
     for(Contract_Termination__c ct: Trigger.New)
    {
        affectedct.add(ct);
        ownerIDs.add(ct.OwnerID__c);
    }
    
    system.debug('The owner id is ' +ownerIDs  + affectedct );
    
    List <User> u = [SELECT Id, contract_approver__r.Id, contract_approver__r.Level__c, contract_approver__r.ManagerId
                  FROM User WHERE Id In :ownerIDs];
    
    List<Contract_Termination__c> ctToUpdate = new List<Contract_Termination__c>();
    
    
     for(Contract_Termination__c ct : affectedct)
    {
        if (u.size() > 0)
        {    
    	
            
        if  ( u[0].contract_approver__r.Level__c  > 2 )
        {
         ct.Level3_Manager__c    = u[0].contract_approver__r.Id; 
        }
            else
        {
         ct.Level3_Manager__c    = u[0].contract_approver__r.ManagerId;    
        }
        
        }    
    	CtToUpdate.add(ct); 
        
    }
}