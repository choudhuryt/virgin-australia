trigger CaseWebContactUpdate on Case (after insert) 
{
	//first get the record type id
	List<RecordType> recordTypeGRCase = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Case'
										and IsActive = true
										and DeveloperName = 'GR_Case' LIMIT 1];
										
	List<RecordType> recordTypeGRContact = [Select r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, 
											r.Description, r.BusinessProcessId From RecordType r
										where SobjectType = 'Contact'
										and IsActive = true
										and DeveloperName = 'GR_Contact' LIMIT 1];
	
	//stop if no GR Record types for contacts or case found
	if(recordTypeGRCase.size() <= 0 || recordTypeGRContact.size() <= 0)
		return;
	
     List<String> emailAddresses = new List<String>();  
     
	//find cases where contact is set
     for( Case caseObj:Trigger.new )  
     {  
         //web cases
         if( caseObj.ContactId!=null && 
            caseObj.SuppliedEmail != null && 
            caseObj.SuppliedEmail.trim()!='' //&&
            //caseObj.recordTypeId == recordTypeGRCase[0].Id
            //TODO - Cases created off webform are not populating recordtypeid now. Need to add this back in when form is sorted.
           )  
         {  
             emailAddresses.add(caseObj.SuppliedEmail.trim());  
         }  
         //manually created cases
         else if( caseObj.ContactId!=null && 
                 caseObj.SuppliedEmail==null &&  
                 caseObj.Contact_Email__c !=null &&
                 caseObj.Contact_Email__c.trim() !='' &&
                 caseObj.recordTypeId == recordTypeGRCase[0].Id
                )  
         {  
             emailAddresses.add(caseObj.Contact_Email__c.trim());  
         }  
         
     }  

     //Find any matching contacts as per email address
     List<Contact> listContacts = [Select Id, name, phone, mobilePhone, Email, Velocity_Number__c, 
     								Velocity_Member__c, Velocity_Status__c, Travel_Agent_ID__c, 
     								I_am_a_Travel_Agent__c From Contact 
     							where name != '' 
     							and Email in :emailAddresses];
     							
     List<Contact> contactsToUpdate = new List<Contact>();			  
     Map<String, Contact> existingEmailAddressMap = new Map<String, Contact>();
     Map<Id, String> existingContactIdMap = new Map<Id, String>();
     
     //add existing contact emails and Ids
     //assumes no duplicate contacts 
     for (Contact c:listContacts) 
     {  
         existingEmailAddressMap.put(c.Email.trim(), c);
         existingContactIdMap.put(c.Id, c.Email.trim());
     }  
     
     for (Case caseObj:Trigger.new)
     {       	   	 
         //check if case has contact referenced and contact exists 
         if( caseObj.ContactId != null &&  
             existingContactIdMap.containsKey(caseObj.ContactId) )  
         {
              
              //update existing fields which are currently blank but provided in the case  	      	
              Contact thisContact = existingEmailAddressMap.get(existingContactIdMap.get(caseObj.ContactId));
              
              Boolean updateContact = false;
              
              if( (thisContact.phone == null || thisContact.phone == '') 
              	&& (caseObj.Contact_Phone__c != null && caseObj.Contact_Phone__c != ''))
              {
              		thisContact.phone = caseObj.Contact_Phone__c;
              		updateContact = true;
              }
              if( (thisContact.mobilePhone == null || thisContact.mobilePhone == '') 
              	&& (caseObj.Contact_Mobile__c != null && caseObj.Contact_Mobile__c != ''))
              {
              		thisContact.mobilePhone = caseObj.Contact_Mobile__c;
              		updateContact = true;
              }              
              if( (thisContact.Velocity_Number__c == null || thisContact.Velocity_Number__c == '') 
              	&& (caseObj.Velocity_Number__c != null && caseObj.Velocity_Number__c != '' && caseObj.Velocity_Number__c.length()==10))
              {
              		thisContact.Velocity_Number__c = caseObj.Velocity_Number__c;
              		thisContact.Velocity_member__c = true;
              		updateContact = true;
              }       
              if( (thisContact.travel_agent_id__c == null || thisContact.travel_agent_id__c == '') 
              	&& (caseObj.Travel_Agent_ID__c != null && caseObj.Travel_Agent_ID__c != ''))
              {
              		thisContact.travel_agent_id__c = caseObj.Travel_Agent_ID__c;
              		thisContact.I_am_a_travel_agent__c = true;
              		updateContact = true;
              }   
              if(caseObj.Additional_Details__c != null && caseObj.Additional_Details__c != '' 
              	&& String.valueOf(caseObj.Additional_Details__c).contains('I am a travel Agent')
              	&& !thisContact.I_am_a_travel_agent__c)
              {
              		thisContact.I_am_a_travel_agent__c = true;
              		updateContact = true;
              }    
              if( (thisContact.Velocity_Status__c == null || thisContact.Velocity_Status__c == '') 
              	&& (caseObj.Velocity_Type__c != null && caseObj.Velocity_Type__c != ''))
              {
              		thisContact.Velocity_Status__c = caseObj.Velocity_Type__c;
              		updateContact = true;
              }   
                              	
              if(updateContact)
              {
              		System.debug('SGX1 - Updates needed. Adding contact ' + thisContact.id+ ' ' + thisContact.name);
              		contactsToUpdate.add(thisContact);
              }
         }
         
     }  

     
     System.debug('SGX1 - Contacts to be updated ' + contactsToUpdate.size());
     if( contactsToUpdate.size() > 0 )
     {
     	System.debug('SGX1 - Contacts updated ' + contactsToUpdate.size());
     	update contactsToUpdate;
     }  
    // Chandra Kemoji added below IF statement.
    if(trigger.isInsert && trigger.isAfter)
       {    
          CaseSetAccLifeCycle.CaseSetAccLifeCycle1(trigger.new);
       }
}