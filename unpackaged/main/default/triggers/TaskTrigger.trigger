/*
 * 
 * HISTORY
 * 18.May.2016  Warjie Malibago CloudFirst  Added filter in TaskTrigger to assign the task to Sales Support Group if it is filled out; see 18.May.2016 WBM
*/
trigger TaskTrigger on Task (before insert) {

   /* List<Task> TaskList = new List<Task>();
    Map<Id, Task> mapTasks = new Map<Id, Task>();
    Set<Id> tIds = new Set<Id>();

    for(Task t : Trigger.new){
         
         if( t.Subject =='Please Implement The following Contract' && t.Status =='Not Started' )
         {
        
               mapTasks.put(t.WhatId, t);
               tIds.add(t.WhatId);
               List<Contract> conList =[Select id,RecordTypeId  from Contract Where Id IN : tIds ];
        
               for(Contract con : conList)
               {          
                RecordType recordType = new RecordType();          
                recordType = (RecordType) [select id,DeveloperName from RecordType where id =:con.RecordTypeId];          
                if (recordType.DeveloperName.Contains('Industry'))
                {            
                t.OwnerId ='00590000001XiTY';
                }
                 
              }
        
         }
        
        if( t.Subject =='Please process the following Agent Rate Request' && t.Status =='Not Started' )
         {
            
                //Add the task to the Map and Set
                mapTasks.put(t.WhatId, t);
                tIds.add(t.WhatId);
               // System.debug('!!!!!!!!!!!! I am In' + String.valueOf(t.WhatId));
            
    
                List<Ticket_Request__c> VList =[Select id,Ticket_Request__c from Ticket_Request__c Where Id IN : tIds ];
    
                for(Ticket_Request__c vsm : VList)
                {
        
        
               Account account  = [select id,Billing_Country_Code__c,BillingState,Sales_Support_Group__c,
                                   SalesSupportGroup_For_Trigger__c , SalesSupportGroup_For_TR__c from Account
                                   where id = : vsm.Ticket_Request__c ];
                    
               system.debug('The new feild' + account.SalesSupportGroup_For_Trigger__c ) ;
               if(account.Sales_Support_Group__c <> null ){
               t.OwnerId = account.Sales_Support_Group__c;
               return;
               }
        
               if(account.Sales_Support_Group__c == null)
               {
                 t.OwnerId =  account.SalesSupportGroup_For_TR__c ;
                  return; 
               }
              }//for loop for Ticket Request
         }
       
    
         if( t.Subject =='Please Process the following FOC Ticket Request' && t.Status =='Not Started' )
         {
            
                //Add the task to the Map and Set
                mapTasks.put(t.WhatId, t);
                tIds.add(t.WhatId);
               // System.debug('!!!!!!!!!!!! I am In' + String.valueOf(t.WhatId));
            
    
               List<Ticket_Request__c> VList =[Select id,Ticket_Request__c from Ticket_Request__c Where Id IN : tIds ];
  
               for(Ticket_Request__c vsm : VList){
    
   
                Account account  = [select id,Billing_Country_Code__c,BillingState,
                                    Sales_Support_Group__c,SalesSupportGroup_For_Trigger__c,
                                    SalesSupportGroup_For_TR__c
                                    from Account where id = : vsm.Ticket_Request__c ];
                   
                system.debug('The new feild' + account.SalesSupportGroup_For_Trigger__c ) ;
                   
                if(account.Sales_Support_Group__c <> null ){
                t.OwnerId = account.Sales_Support_Group__c;
                return;
                }
      
               if(account.Sales_Support_Group__c == null)
               {
               t.OwnerId =  account.SalesSupportGroup_For_TR__c ;
                return; 
              }
      
   
              }//for loop for Ticket Request
           }
    
    
     if( t.Subject =='Waiver has been approved' && t.Status =='Not Started' || t.Subject =='Travel Bank Compensation Waiver Approved' && t.Status =='Not Started'|| t.Subject =='ADM waiver has been approved' && t.Status =='Not Started' || t.Subject =='Reissued Ticket Required on Waiver' && t.Status =='Not Started'){
            
                //Add the task to the Map and Set
                mapTasks.put(t.WhatId, t);
                tIds.add(t.WhatId);
               // System.debug('!!!!!!!!!!!! I am In' + String.valueOf(t.WhatId));
            
    
            List<Waiver_Favour__c> wList =[Select id,CreatedByid from Waiver_Favour__c Where Id IN : tIds ];
  
            for(Waiver_Favour__c Waiver : wList){ 
    
             t.OwnerId = Waiver.CreatedByid;
    
                 }//for loop for Ticket Request
            }
  
    
    if(t.Subject =='Please process the following Upgrade' && t.Status =='Not Started'  ){
      
          mapTasks.put(t.WhatId, t);
                tIds.add(t.WhatId);
      
      List<Upgrade__c> UList =[Select id,Account__c,Approval_Flight_Status__c,contact__c,PNR__c,Date_Of_Departure__c  from Upgrade__c Where Id IN : tIds ];
  
      for(Upgrade__c upgrade : UList){
        
      if(upgrade.Approval_Flight_Status__c =='Space Available'){
        
      }else{
        
        
  Contact c = new Contact();
  c = (Contact) [select Name From Contact where id =:upgrade.Contact__c ];
  Account a = new Account();
  a = (Account) [select Name,Account_Owner__c From Account where id =: upgrade.Account__c];
  User u = new User();
  u = (User) [select name from User where id =:a.Account_Owner__c];      
  Messaging.SingleEmailMessage mailErrorvsm = new Messaging.SingleEmailMessage();
   mailErrorvsm.setToAddresses(new String[] {'peta.clarke@virginaustralia.com'});
  mailErrorvsm.setReplyTo('peta.clarke@virginaustralia.com');
  mailErrorvsm.setSenderDisplayName('Firm Upgrade Request');
  mailErrorvsm.setSubject('Firm Upgrade');
  mailErrorvsm.setPlainTextBody('Please process the following Firm upgrade as it has CCO approval. ' +  '\r\n' +  '\r\n' + 'Account ' + a.Name + '\r\n' + 'Passenger ' + c.name + '\r\n' + 'PNR ' + upgrade.PNR__c + '\r\n' + 'Departure Date ' + upgrade.Date_Of_Departure__c + '\r\n' + '\r\n' + 'Thanks ' + '\r\n' + u.Name );
  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailErrorvsm });
      t.status ='Completed';
      t.OwnerId ='00590000000K0ju';
      
        
      }
    
      }
      
      
    }
    
    
          if( t.Subject =='Velocity Status Match/Upgrade Request Approved' && t.Status =='Not Started' ){
            
                //Add the task to the Map and Set
                mapTasks.put(t.WhatId, t);
                tIds.add(t.WhatId);
               // System.debug('!!!!!!!!!!!! I am In' + String.valueOf(t.WhatId));
            
    
   List<Velocity_Status_Match__c> VList =[Select id,Account__c from Velocity_Status_Match__c Where Id IN : tIds ];
  
  for(Velocity_Status_Match__c vsm : VList){
    
    //System.debug('!!!!!!!!!!!! I am In the vsm list' + vsm.Account__c);
    Account account  = [select id,Billing_Country_Code__c,BillingState,Sales_Support_Group__c,
                        SalesSupportGroup_For_Trigger__c,SalesSupportGroup_VSM__c
                        from Account where id = : vsm.Account__c ];
    
    if(account.Sales_Support_Group__c <> null ){
      t.OwnerId = account.Sales_Support_Group__c;
      return;
    }
    if(account.Sales_Support_Group__c == null)
    {
      t.OwnerId =  account.SalesSupportGroup_VSM__c ;
      return; 
    }
    
  }//for loop for VSM
            }//close first if for VSM task
            
 if( t.Subject =='Please raise a PO for the related Marketing Spend Down and input Purchase Order Number in the Marketing Spend Down. Do NOT submit for approval.' && t.Status =='Not Started' ){
            
                //Add the task to the Map and Set
                mapTasks.put(t.WhatId, t);
                tIds.add(t.WhatId);
               // System.debug('!!!!!!!!!!!! I am In' + String.valueOf(t.WhatId));
            
    
   List<Marketing_Spend_Down__c> MList =[Select id,Account__c from Marketing_Spend_Down__c Where Id IN : tIds ];
  
  for(Marketing_Spend_Down__c msd : MList){
    
    //System.debug('!!!!!!!!!!!! I am In the vsm list' + vsm.Account__c);
    Account account  = [select id,Billing_Country_Code__c,BillingState,
                        BillingCountry,Region__c,SalesSupportGroup_MS_PO__c
                        from Account where id = : msd.Account__c ];
     
    
      t.OwnerId =  account.SalesSupportGroup_MS_PO__c ;
      return; 
  
      
  }//second for loop for spend down
            }//close second if
            
  if( t.Subject =='Submit Purchase Order for Approval' && t.Status =='Not Started' ){
            
                //Add the task to the Map and Set
                mapTasks.put(t.WhatId, t);
                tIds.add(t.WhatId);
               // System.debug('!!!!!!!!!!!! I am In' + String.valueOf(t.WhatId));
            
    
   List<Marketing_Spend_Down__c> MList =[Select id,Account__c from Marketing_Spend_Down__c Where Id IN : tIds ];
  
  for(Marketing_Spend_Down__c msd : MList){
    
    //System.debug('!!!!!!!!!!!! I am In the vsm list' + vsm.Account__c);
    Account account  = [select id,Billing_Country_Code__c,BillingState, 
                        Region__c,BillingCountry, SalesSupportGroup_MS_PO__c
                        from Account where id = : msd.Account__c ];
     
     
      t.OwnerId = account.SalesSupportGroup_MS_PO__c;
      return;
      
     
  
      
  }//second for loop for spend down
            }//close second if  
        
        
        
    }//end for loop for task trigger*/
}