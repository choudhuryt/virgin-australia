trigger MeetingTrigger on Meeting__c (after insert,after update,before delete) {
    
    String businessUpdate = '';
    String additionalAgenda ='';
    
    if(Trigger.isBefore){
        if(Trigger.isDelete){
            
            List<Meeting__c> meetingOldList = new List<Meeting__c>();
            
            for (Meeting__c MeetingUpdate : Trigger.old){	
                meetingOldList =[select id from Meeting__c where Parent_Meeting__c =:MeetingUpdate.id  ];
                if(meetingOldList.size() > 0){
                    delete meetingOldList;
                }
            }
        } 
    }
    if (Trigger.isUpdate){
        
        for(Meeting__c MeetingUpdate : Trigger.new){
            
            String firstName = '';
            String surname = '';
            Contact contactForMeeting = new Contact();
            if(MeetingUpdate.Sales_Contact__c != null){
                contactForMeeting =(Contact)[select id,FirstName,LastName From Contact where id =:MeetingUpdate.Sales_Contact__c ];
                firstName = contactForMeeting.FirstName;
                surname=  contactForMeeting.LastName;
            }
            List<Account> accountList1 = new List<Account>();
            accountList1 = [select Name,BillingStreet,BillingCity,BillingState,BillingPostalCode from Account where id =:MeetingUpdate.Account__c ];
            Account a1 = new Account();
            a1= accountList1.get(0);
            
            List<Event> eventList = new List<Event>();
            Event e = New Event();
            eventList = [select id From Event where whatid =:MeetingUpdate.id ];
            if(eventList.size()>0){
                //e=[select id From Event where whatid =:MeetingUpdate.id ];
                e = eventList.get(0);
                if(MeetingUpdate.VA_Business_Update__c == null){
                    businessUpdate ='';
                }else{
                    businessUpdate = MeetingUpdate.VA_Business_Update__c;
                }
                
                if(MeetingUpdate.Additional_Meeting_Agenda__c == null){
                    additionalAgenda ='';
                }else{
                    additionalAgenda = 'Meeting Agenda ' + '\r\n'+  '\r\n'+ MeetingUpdate.Additional_Meeting_Agenda__c;
                    additionalAgenda =  additionalAgenda.replaceAll('<[/a-zAZ0-9]*>',',');
                    additionalAgenda =  additionalAgenda.replaceAll(',,',',');
                    additionalAgenda =  additionalAgenda.removeEnd(',');
                    additionalAgenda =  additionalAgenda.replaceFirst(',',' ');
                }
                e.Subject = a1.Name +' ' +  MeetingUpdate.Subject__c + ' with ' + firstName + ' ' + surname;
                //e.Subject = a1.Name +' ' +  MeetingUpdate.Subject__c + ' with ' + contactForMeeting.FirstName + ' ' + contactForMeeting.LastName;
                if(MeetingUpdate.Location__c ==null){
                    e.Location = a1.BillingStreet + ', ' +   a1.BillingCity + ', ' + a1.BillingState + ', ' + a1.BillingPostalCode ;	
                }else{
                    e.Location = MeetingUpdate.Location__c;
                }
                e.StartDateTime = MeetingUpdate.Meeting_Date_And_Time__c;
                e.EndDateTime = MeetingUpdate.End_Meeting_Date_and_Time__c;
                e.Description = additionalAgenda +  '\r\n';
                additionalAgenda = additionalAgenda.replace('<div>', '');
                additionalAgenda = additionalAgenda.replace('</div>', '');
                //e.Description = additionalAgenda +  '\r\n' + 'Meeting Objectives' + '\r\n' + MeetingUpdate.Meeting_Objectives__c;
                e.WhatId = MeetingUpdate.id;
                e.WhoId = MeetingUpdate.Sales_Contact__c;
                e.OwnerId = MeetingUpdate.OwnerId;
                
                update e;
            }
            List<Meeting__c> meetingChildren = new List<Meeting__c>();
            meetingChildren = [select id,Parent_Meeting__c from Meeting__c where Parent_Meeting__c =:MeetingUpdate.id ]; 
            List<Meeting__c> meetingChildrenUpdate = new List<Meeting__c>();
            
            for(Integer x =0; x < meetingChildren.size(); x ++){
                
                Meeting__c childMeetingToUpdate = new Meeting__c();
                childMeetingToUpdate = meetingChildren.get(x);
                childMeetingToUpdate.Key_Topics__c = MeetingUpdate.Key_Topics__c;
                childMeetingToUpdate.End_Meeting_Date_and_Time__c =MeetingUpdate.End_Meeting_Date_and_Time__c;
                childMeetingToUpdate.Meeting_Date_And_Time__c = MeetingUpdate.Meeting_Date_And_Time__c;
                childMeetingToUpdate.Key_Topics__c = MeetingUpdate.Key_Topics__c;
                childMeetingToUpdate.VA_Business_Update__c = MeetingUpdate.VA_Business_Update__c;
                childMeetingToUpdate.Additional_Meeting_Agenda__c = MeetingUpdate.Additional_Meeting_Agenda__c;
                childMeetingToUpdate.location__c = MeetingUpdate.location__c;
                meetingChildrenUpdate.add(childMeetingToUpdate); 
            }
            
            List<Event> ChildEventList = new List<Event>();
            for(Integer x =0; x< meetingChildren.size();x++){
                if(meetingChildren.size()>0){
                    Meeting__c meetingChild = new Meeting__c();
                    meetingChild = meetingChildren.get(x);
                    //Event ChildEvent = New Event();
                    //ChildEvent = [select id From Event where whatid =:meetingChild.id ];
					Event[] ChildEvent = [select id From Event where whatid =:meetingChild.id ];
                    if (ChildEvent.size() > 0){
                        ChildEvent[0].Subject = a1.Name +' ' +  MeetingUpdate.Subject__c + ' with ' + contactForMeeting.FirstName + ' ' + contactForMeeting.LastName;
                        if(MeetingUpdate.Location__c ==null){
                            ChildEvent[0].Location = a1.BillingStreet + ', ' +   a1.BillingCity + ', ' + a1.BillingState + ', ' + a1.BillingPostalCode ;
                        }else{
                            ChildEvent[0].Location = MeetingUpdate.Location__c;
                        }
                        ChildEvent[0].StartDateTime = MeetingUpdate.Meeting_Date_And_Time__c;
                        ChildEvent[0].EndDateTime = MeetingUpdate.End_Meeting_Date_and_Time__c;
                        ChildEvent[0].Description = additionalAgenda;
                        ChildEvent[0].WhoId = MeetingUpdate.Sales_Contact__c;
                        //ChildEvent.OwnerId = MeetingUpdate.OwnerId;
                        ChildEventList.add(ChildEvent[0]);
                    } 
                }
                update ChildEventList;
            }
            if(meetingChildrenUpdate.size()>0){
                update 	meetingChildrenUpdate;
            }
            
        }
    }
    if(Trigger.isInsert){
        
        for(Meeting__c Meeting : Trigger.new){
            
            List<Account> accountList = new List<Account>();
            
            String firstName = '';
            String surname = '';
            String salesContactEmail ='';
            if(Meeting.Sales_Contact__c != null){
                Contact contactForMeeting = new Contact();
                contactForMeeting =(Contact)[select id,FirstName,LastName,Email From Contact where id =:Meeting.Sales_Contact__c ];
                firstName = contactForMeeting.FirstName;
                surname=  contactForMeeting.LastName;
                salesContactEmail = contactForMeeting.Email;
            }
            
            accountList = [select Name,BillingStreet,BillingCity,BillingState,BillingPostalCode from Account where id =:meeting.Account__c ];
            
            if (accountList.size() > 0){
                Account a = new Account();
                a= accountList.get(0);
                if(meeting.VA_Business_Update__c == null){
                    businessUpdate ='';
                }else{
                    businessUpdate = meeting.VA_Business_Update__c;
                }
                
                if(meeting.Additional_Meeting_Agenda__c == null){
                    additionalAgenda ='';
                }else{
                    additionalAgenda = 'Meeting Agenda ' + '\r\n'+ '\r\n'+ meeting.Additional_Meeting_Agenda__c;
                    additionalAgenda =  additionalAgenda.replaceAll('<[/a-zAZ0-9]*>',',');
                    additionalAgenda =  additionalAgenda.replaceAll(',,',',');
                    additionalAgenda =  additionalAgenda.removeEnd(',');
                    additionalAgenda =  additionalAgenda.replaceFirst(',',' ');
                    
                }
                system.debug ('The meeting agenda is'  + additionalAgenda  );
                
                Event event = new Event();
                
                event.Subject = a.Name +' ' +  meeting.Subject__c + ' with ' + firstName + ' ' + surname;
                if(meeting.location__c == null){
                    event.Location = a.BillingStreet + ', ' +   a.BillingCity + ', ' + a.BillingState + ', ' + a.BillingPostalCode ;
                }else{
                    event.Location = meeting.location__c;	
                }
                event.StartDateTime = meeting.Meeting_Date_And_Time__c;
                event.EndDateTime = meeting.End_Meeting_Date_and_Time__c;
                event.Description =  additionalAgenda;
                //event.Description =  additionalAgenda +  '\r\n' + 'Meeting Objectives \r\n' + meeting.Meeting_Objectives__c;
                event.WhatId = Meeting.id;
                event.WhoId = Meeting.Sales_Contact__c;
                event.OwnerId = Meeting.OwnerId;
                
                insert event;
                
                /*	EventRelation er = new  EventRelation();
er.RelationId = Meeting.id;
er.RelationId = Meeting.Sales_Contact__c;
er.EventId = event.id;
er.Status ='New';
insert er;*/   
                /////
                String txtInvite = '';
                txtInvite += 'BEGIN:VCALENDAR\n';
                txtInvite += 'PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN\n';
                txtInvite += 'VERSION:2.0\n';
                txtInvite += 'METHOD:PUBLISH\n';
                txtInvite += 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n';
                txtInvite += 'BEGIN:VEVENT\n';
                txtInvite += 'CLASS:PUBLIC\n';
                txtInvite += 'CREATED:20091026T203709Z\n';
                txtInvite += 'DTEND:20131028T010000Z\n';
                txtInvite += 'DTSTAMP:20131026T203709Z\n';
                txtInvite += 'DTSTART:20131028T000000Z\n';
                txtInvite += 'LAST-MODIFIED:20131026T203709Z\n';
                txtInvite += 'LOCATION:' + event.Location + '\n';
                txtInvite += 'PRIORITY:5\n';
                txtInvite += 'SEQUENCE:0\n';
                txtInvite += 'SUMMARY: This is a test';
                txtInvite += 'LANGUAGE=en-us:Meeting\n';
                txtInvite += 'TRANSP:OPAQUE\n';
                txtInvite += 'UID:4036587160834EA4AE7848CBD028D1D200000000000000000000000000000000\n';
                txtInvite += 'X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN"><HTML><HEAD><META NAME="Generator" CONTENT="MS Exchange Server version 08.00.0681.000"><TITLE></TITLE></HEAD><BODY><!-- Converted from text/plain format --></BODY></HTML>\n';
                txtInvite += 'X-MICROSOFT-CDO-BUSYSTATUS:BUSY\n';
                txtInvite += 'X-MICROSOFT-CDO-IMPORTANCE:1\n';
                txtInvite += 'END:VEVENT\n';
                txtInvite += 'END:VCALENDAR';
                
                String attachmentName = 'Meeting.ics';
                Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage(); 
                //Now we have to set the value to Mail message properties 
                
                msg.setToAddresses(new String[] {salesContactEmail}); 
                // it is optional, only if required 
                msg.setSubject('Meeting'); 
                msg.setPlainTextBody(event.description); 
                // Create the attachment
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment(); 
                efa.setFileName(attachmentName ); 
                
                Blob b = Blob.valueof(txtInvite);
                efa.setBody(b);
                msg.setFileAttachments(new Messaging.EmailFileAttachment[] {efa}); 
                // Send the email you have created. 
                //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });   
            }  
        }
    }
}