trigger LeadMasterTrigger on Lead (before insert, before update) {
    if (trigger.isBefore && (trigger.isInsert || trigger.isUpdate)) {
        Boolean hasChatLead = false;
        for(Lead ld : Trigger.new){
            if(ld.Company == 'GCC Webchat'){
                hasChatLead = true;
            }
        }
        
        if(hasChatLead) {
            User adminUser = [SELECT Id FROM User WHERE Alias = 'SADMIN' LIMIT 1];
            for(Lead ld : Trigger.new) {
                if(ld.Company == 'GCC Webchat') {
                    ld.OwnerId = adminUser.Id;
                }
            }
        }
    }
}