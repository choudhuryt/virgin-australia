trigger VelocityClubMembership on Velocity_Status_Match__c (before insert, before update) {

  for(Velocity_Status_Match__c vsmClub:Trigger.new){
		

		if(vsmClub.RecordTypeId != null && vsmClub.RecordTypeId.equals('012900000009IauAAE')){
			if(vsmClub.Approval_Status__c.equals('Draft')){
			List<Contact> contact = new List<Contact>();
			
			contact = [select id,Salutation, name,AssistantName,AssistantPhone,Assistant_Email__c,Phone,Email,Title,Velocity_Member__c,Velocity_Number__c,Velocity_Status__c from contact where id=:vsmClub.Contact__c];
			
			for (Integer x =0; x < contact.size(); x++){
				
				Contact contactDetails = new Contact();
				contactDetails = contact.get(x);
				vsmClub.Passenger_Email__c = contactDetails.Email;
				vsmClub.Passenger_Name__c = contactDetails.Name;
				vsmClub.PA_Email__c = contactDetails.Assistant_Email__c;
				vsmClub.PA_name__c = contactDetails.AssistantName;
				vsmClub.Position_in_Company__c = contactDetails.Title;
				vsmClub.Passenger_Velocity_Number__c = contactDetails.Velocity_Number__c;
				//vsmClub.Status_Match_or_Upgrade__c = contactDetails.Velocity_Status__c;
				vsmClub.PA_Number__c = contactDetails.AssistantPhone;
				vsmClub.Passenger_Phone_Number__c = contactDetails.Phone;
				vsmClub.Salutation__c = contactDetails.Salutation;				 
			}
			
			/*List<Account> accountList = new List<Account>();
			accountList =[select id,Estimated_Domestic_Revenue__c,Estimated_International_Revenue__c from Account where id=:vsmClub.Account__c];
			for (Integer i = 0; i < accountList.size(); i ++ ){
				Account acc = new Account();
				acc = accountList.get(i);
				vsmClub.Total_Domestic_Expenditure__c = String.valueOf(acc.Estimated_Domestic_Revenue__c);
				break;		
			}
			*/
			
			}
		}

}
}