/** * File Name      : SetLookuponContactforSubscritpions
* Description        : This Apex Trigger will make a lookup to Subscription on the Contact every time a new Subscription is inserted
* * Copyright        : © Virgin Australia Airlines Pty Ltd ABN 36 090 670 965 
* * @author          : Ed 'the man' Stachyra
* * Date             : 23 May 2012
* * Technical Task ID: 
* * Notes            :  The test class for this file is:  TestSetContactIdOnSubscritptions
* Modification Log =============================================================== 
Ver Date Author Modification --- ---- ------ -------------
* 
* HISTORY
* May 27, 2016  Warjie Malibago (Accenture CloudFirst)  minimize contacts to update because the trigger updates all contacts with the criteria in the query; see 270516WBM
*/
//  The test class for this file is:  TestSetContactIdOnSubscritptions
//

trigger SetLookuponContactforSubscritpions on Subscriptions__c (after insert) {
   
    List<Contact> conToUpdate = new List<Contact>(); //270516WBM
    Set<ID> subid = new Set<ID> ();
    Set<ID> conid = new Set<ID> ();
    for(Subscriptions__c s : Trigger.new)
    {
       subid.add(s.id); 
       conid.add(s.Contact_ID__c) ;
    }
    List<Subscriptions__c> sub = [select Id,Contact_ID__c from Subscriptions__c where  id = :subid ];
    List<Contact> con = [select Id from Contact where Subscriptions__c ='' and IsDeleted = false and id = :conid ];
    for(Contact c : con)
    {
            if (c.Id == sub[0].Contact_ID__c){ //if the Contact ID is same as Subscription Contact_ID__c
                c.Subscriptions__c = sub[0].id;
                conToUpdate.add(c); //270516WBM
            }
        }
    update conToUpdate; //270516WBM
}