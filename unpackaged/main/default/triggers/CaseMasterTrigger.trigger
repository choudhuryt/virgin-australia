trigger CaseMasterTrigger on Case ( before insert, before update, after update, after insert) {
    if (Trigger.isBefore && Trigger.isUpdate) {
        CaseTriggerHandler.AutoCloseSpamCase(Trigger.new, Trigger.oldMap);
        CaseTriggerHandler.ReopenCaseCapture(Trigger.new, Trigger.oldMap);
        CaseTriggerHandler.CaseAssignmentCapture(Trigger.new, Trigger.oldMap);
        CaseTriggerHandler.updateFlightNumber(Trigger.new, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate);
    }
    if (Trigger.isBefore && Trigger.isInsert) {
        CaseTriggerHandler.updateFlightNumber(Trigger.new, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate);
        CaseTriggerHandler.updateRecordType(Trigger.new);
    }
    if (Trigger.isAfter && Trigger.isInsert) {
        CaseTriggerHandler.attachEmailGCCSSRCase(Trigger.new); 
    }
    
}