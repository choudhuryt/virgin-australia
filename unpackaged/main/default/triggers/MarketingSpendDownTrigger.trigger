trigger MarketingSpendDownTrigger on Marketing_Spend_Down__c (after insert,after update,after delete)
{
 if( Trigger.isAfter && ( Trigger.isInsert || Trigger.isUpdate  || Trigger.isDelete)  )  
    {
     MarketingSpendDownTriggerHandler.AggregateMarketingSpenddown(Trigger.isDelete, Trigger.new, Trigger.old);       
    }
}