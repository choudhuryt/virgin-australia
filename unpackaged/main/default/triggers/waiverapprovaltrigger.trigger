trigger waiverapprovaltrigger on Waiver_Favour__c (AFTER update) {

    system.debug('Inside the waiver approval trigger' + triggerupdatehelper.hasAlreadyupdatedtrigger() )  ;
    
//if (!triggerupdatehelper.hasAlreadyupdatedtrigger()) {
     
//system.debug('Inside the waiver approval trigger')  ;
    
Map<Id, Waiver_Favour__c> waiverapproved =   new Map<Id, Waiver_Favour__c>{};
List<Waiver_Favour__c> wfUpdate = new List<Waiver_Favour__c>(); 

 for(Waiver_Favour__c wf: trigger.new)
  {
    system.debug('Approval flag' +  wf.Approval_Comment_Check__c)  ;
    if ( wf.Approval_Comment_Check__c == 'Requested'  )
    {    
       waiverapproved.put(wf.Id, wf);    
      
    }
  }   
  if (!waiverapproved.isEmpty()) 
  {   
      List<Waiver_Favour__c> wflist =  [Select Id, Business_Justification__c from Waiver_Favour__c where id IN :waiverapproved.keySet()  ];     
      List<Id> processInstanceIds = new List<Id>{};
          
      for (Waiver_Favour__c waf : [SELECT (SELECT ID
                                              FROM ProcessInstances
                                              ORDER BY CreatedDate DESC
                                              LIMIT 1)
                                      FROM Waiver_Favour__c
                                      WHERE ID IN :waiverapproved.keySet()])
       {
        processInstanceIds.add(waf.ProcessInstances[0].Id);
       }   
        
        for (ProcessInstance pi : [SELECT TargetObjectId,
                                   (SELECT Id, StepStatus, Comments 
                                    FROM Steps
                                    ORDER BY CreatedDate DESC
                                    LIMIT 1 )
                               FROM ProcessInstance
                               WHERE Id IN :processInstanceIds
                               ORDER BY CreatedDate DESC])
       { 
          system.debug('This is the comment' + pi.Steps[0].Comments + processInstanceIds   )   ;
           
           if ((pi.Steps[0].Comments == null || 
           pi.Steps[0].Comments.trim().length() == 0))
           {
             waiverapproved.get(pi.TargetObjectId).addError(
             'Comments not provided : Please provide the business justification for your approval');
             
            }
            
           if (pi.Steps[0].Comments != null)
           {   
              for(Waiver_Favour__c updwf: wflist) 
             {
               updwf.Business_Justification__c = pi.Steps[0].Comments; 
               updwf.Approval_Comment_Check__c  = null ;    
               wfUpdate.add(updwf);   
             }  
           }    
       }
     
     triggerupdatehelper.settriggerupdated();

     update wfUpdate;
 } 
//}
}