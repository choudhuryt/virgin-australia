trigger WaiverTicketDetailsTrigger on Waiver_Ticket_Details__c (before insert, before update) {

List<Task>taskList = new List<Task>();
for(Waiver_Ticket_Details__c waiverTicketDetail: Trigger.New) {

//add a task for the Smartfly Tourcode for Sales Support
	if(waiverTicketDetail.Send_Task__c ==true){
	Date d = Date.today();
	Waiver_Favour__c waiverAndFavour = [select id,Account__c,createdbyid from Waiver_Favour__c where id =: waiverTicketDetail.Waiver_Favour__c];
	
	
	Task task = new task();
	task.WhatId = waiverAndFavour.Account__c;
	task.OwnerId = waiverAndFavour.createdbyid;
	task.Subject = 'Reissued Ticket Required on Waiver';
	task.Status = 'Not Started';
	task.Priority = 'Normal';
	task.Notify_Creator__c = true;
	task.ActivityDate = d;
	taskList.add(task);
	
	}
}

if(taskList.size() > 0 ){
	try{
	insert taskList;
	}catch(exception e){
		VirginsutilitiesClass.sendEmailError(e);
	}
}

}