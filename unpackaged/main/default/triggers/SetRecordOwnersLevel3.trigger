trigger SetRecordOwnersLevel3 on Global_Sales_Activity__c (before insert, before update) {
    
    
    Set<Id> ownerIDs  = new Set<Id>();
    Set<Id> level3IDs = new Set<Id>();
	List<Global_Sales_Activity__c> affectedGSA = new List<Global_Sales_Activity__c>();
    List<Global_Sales_Activity__c> GSAToUpdate = new List<Global_Sales_Activity__c>();
    
    for(Global_Sales_Activity__c g: Trigger.New)
    {
    			ownerIDs.add(g.OwnerId);
                affectedGSA.add(g) ;
	}
		
    List <User> u = [SELECT Id, contract_approver__r.Id, contract_approver__r.Level__c, contract_approver__r.ManagerId
                  FROM User WHERE Id In :ownerIDs];
    system.debug('The userid' + ownerIDs + u ) ;
    
    for(Global_Sales_Activity__c gs : affectedGSA)
    {
        if (u.size() > 0)
        {    
    	
        if  ( u[0].contract_approver__r.Level__c  > 2 )
        {
         gs.Associated_Level3__c    = u[0].contract_approver__r.Id; 
        }else
        {
         gs.Associated_Level3__c   = u[0].contract_approver__r.ManagerId;    
        }
        
        }  
        system.debug('The level3'  +  gs.Associated_Level3__c );
    	GSAToUpdate.add(gs); 
        
    }
}