trigger UpdatePIRNumber on Case (after insert, after update)
{
    LIST<String> myCaseId = new LIST<String>();
    LIST<String> myVelocityNumber = new LIST<String>();
    List <Case> caselist = new LIST<Case>();
    
    //Commented by tapashree.choudhury@virginaustralia.com to avoid 101 Soql error, added inside AfterInsert loop and AfterUpdate loop
    /*for (Case  c : [Select Id from Case where Id in :Trigger.new and recordtypeid <> '0126F000001MHEV' ]) { myCaseId.add(c.Id); } 
    for (Case  c : [Select Velocity_Number__c from Case where Id in :Trigger.new and recordtypeid <> '0126F000001MHEV'  ]) { myVelocityNumber.add(c.Velocity_Number__c); } 
    
    caselist = [SELECT Id   FROM Case WHERE Id = :myCaseId];
        
    if (caselist.size() > 0)
    {*/
    if(Trigger.isInsert)
    {
        if(Trigger.isAfter)
        {
            for (Case  c : [Select Id from Case where Id in :Trigger.new and recordtypeid <> '0126F000001MHEV' ]) { myCaseId.add(c.Id); } 
    		for (Case  c : [Select Velocity_Number__c from Case where Id in :Trigger.new and recordtypeid <> '0126F000001MHEV'  ]) { myVelocityNumber.add(c.Velocity_Number__c); } 
    
    		caselist = [SELECT Id   FROM Case WHERE Id = :myCaseId];
            
            //avoid recursive callout
            ProcessorControl.inFutureContext = true;
            //Call Function from controller to get Velicity details and then update these values to velocity object
            if (caselist.size() > 0){
                TriggerUtility.UpdateVelocity(myCaseId, myVelocityNumber);
            }
            
        }
    }
    else if(Trigger.isUpdate)
    {
         if (Trigger.isAfter) {
            if (!ProcessorControl.inFutureContext) {
                //avoid recursive callout
                ProcessorControl.inFutureContext = true;
                
                for (Case  c : [Select Id from Case where Id in :Trigger.new and recordtypeid <> '0126F000001MHEV' ]) { myCaseId.add(c.Id); } 
    			for (Case  c : [Select Velocity_Number__c from Case where Id in :Trigger.new and recordtypeid <> '0126F000001MHEV'  ]) { myVelocityNumber.add(c.Velocity_Number__c); } 
    
    			caselist = [SELECT Id   FROM Case WHERE Id = :myCaseId];
                
                
                if (caselist.size() > 0){
                    for(Id caseId : Trigger.newMap.keyset()) {
                        String newVelocity = Trigger.newMap.get(caseId).Velocity_Number__c;
                        String oldVelocity = Trigger.oldMap.get(caseId).Velocity_Number__c;
                        if (String.isNotBlank(newVelocity) && !newVelocity.equals(oldVelocity)) {
                            TriggerUtility.UpdateVelocity(new List<String>{caseId}, new List<String>{newVelocity});
                        }
                    }
                }
                
            } else {
                ProcessorControl.inFutureContext = false;
            }
        }
    }
        
    //}
}